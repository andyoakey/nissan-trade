﻿Imports System.Data
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class RewardProcessing201504

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSQL As String, dsResults As DataSet, dsBD4 As DataSet
    Dim htIn As New Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dDate As Date
        Me.Title = "qubeDATA PARTS - Reward Processing 2015 Q4"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), Me.Title, Me.Title)
            dDate = DateAdd(DateInterval.Day, -1, Now)
            While dDate.DayOfWeek = DayOfWeek.Saturday Or dDate.DayOfWeek = DayOfWeek.Sunday Or IsBankHoliday(dDate)
                dDate = DateAdd(DateInterval.Day, -1, dDate)
            End While
            txtDate.Text = Format(dDate, "dd-MMM-yyyy")
            Session("ExportPressed") = 0
        End If
    End Sub

    Sub LoadOrders()

        Dim dDate As Date
        Dim sDate As String

        lblError.Text = ""
        sDate = ""
        gridOrders.DataSource = Nothing
        gridOrders.DataBind()

        If Not IsDate(txtDate.Text) Then
            lblError.Text = "Not a valid date"
        Else
            dDate = CDate(txtDate.Text)
            If dDate.DayOfWeek = DayOfWeek.Saturday Or dDate.DayOfWeek = DayOfWeek.Sunday Or IsBankHoliday(dDate) Then
                lblError.Text = "Not a valid date"
            Else
                sDate = Format(dDate, "dd-MMM-yyyy")
            End If
        End If

        If sDate <> "" Then
            htIn.Clear()
            htIn.Add("@dDate", sDate)
            htIn.Add("@nProcess", 0)
            dsResults = da.Read(sErr, "p_Campaigns_201504_RewardExtract", htIn)
            gridOrders.DataSource = dsResults.Tables(0)
            gridOrders.DataBind()
        End If

    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Call LoadOrders()
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click

        Dim sFileName As String = "Rewards2015_Q4_" & Format(Now, "ddMMMyyyy") & ".xls"
        Dim i As Integer

        lblError.Text = ""
        Session("ExportPressed") = 1

        Call LoadOrders()

        For i = 3 To 7
            gridOrders.Columns(i).Visible = True
        Next
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsToResponse(New XlsExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        For i = 3 To 7
            gridOrders.Columns(i).Visible = False
        Next
        For i = 0 To gridOrders.Columns.Count - 1
            gridOrders.Columns(i).VisibleIndex = i
        Next

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        If Session("ExportPressed") = 0 Then
            lblError.Text = "WARNING: The Export button has not been pressed."
        End If
        txtDate.Enabled = False
        btnRefresh.Visible = False
        btnProcess.Visible = False
        btnExportToExcel.Visible = False
        btnOK.Visible = True
        btnCancel.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtDate.Enabled = True
        btnRefresh.Visible = True
        btnProcess.Visible = True
        btnExportToExcel.Visible = True
        btnOK.Visible = False
        btnCancel.Visible = False
        Session("ExportPressed") = 0
        lblError.Text = ""
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click

        Dim dDate As Date
        Dim sDate As String

        lblError.Text = ""
        sDate = ""
        gridOrders.DataSource = Nothing
        gridOrders.DataBind()

        If Not IsDate(txtDate.Text) Then
            lblError.Text = "Not a valid date"
        Else
            dDate = CDate(txtDate.Text)
            If dDate.DayOfWeek = DayOfWeek.Saturday Or dDate.DayOfWeek = DayOfWeek.Sunday Or IsBankHoliday(dDate) Then
                lblError.Text = "Not a valid date"
            Else
                sDate = Format(dDate, "dd-MMM-yyyy")
            End If
        End If

        If sDate <> "" Then

            htIn.Clear()
            htIn.Add("@dDate", sDate)
            htIn.Add("@nProcess", 1)
            dsResults = da.Read(sErr, "p_Campaigns_201504_RewardExtract", htIn)

            txtDate.Enabled = True
            btnRefresh.Visible = True
            btnProcess.Visible = True
            btnOK.Visible = False
            btnCancel.Visible = False
            Session("ExportPressed") = 0
            lblError.Text = ""

            Call LoadOrders()

        End If

    End Sub

    Function IsBankHoliday(ByVal dDate As Date) As Boolean

        Dim sDate As String = Format(dDate, "dd-MMM-yyyy")
        Dim sStr As String = "SELECT COUNT(*) AS RecCount FROM Toyota_PublicHolidays WHERE HolidayDate = '" & sDate & "'"
        Dim nCount As Integer = GetDataLong(sStr)
        IsBankHoliday = (nCount > 0)

    End Function

End Class
