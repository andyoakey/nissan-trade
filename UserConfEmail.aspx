﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserConfEmail.aspx.vb" Inherits="UserConfEmail"  MasterPageFile="~/MasterPage.master"%>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
        
        <div id="divPageTitle" style="position:relative; left:5px">
            <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat="server" 
                Text="Password Confirmation Email" />
        </div>
                
        <div id="div2" style="position:relative; left:5px; top:30px ">
            <dx:ASPxLabel
                ID="lblEmailAddress" 
                runat="server"/>
        </div>

        <br />
        <br />

        <table>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnSend" runat="server" Text="OK" Width="100px" /> 
                </td>

                <td>
                    <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="100px"/>
                </td>

            </tr>
        </table>

    </div>

</asp:Content>