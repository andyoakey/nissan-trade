﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System

Partial Class DataSubmission

    Inherits System.Web.UI.Page

    Dim dsSubmissions As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim nTotalSale As Double
    Dim nTotalCost As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Data Checker"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Session("MonthFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O'")
        End If

        Call LoadDataChecker()

        If Not IsPostBack Then
            Call GetMonthsDevX(ddlMonth)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Data Submissions", "Viewing Data Submissions")
        End If

    End Sub

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Session("MonthFrom") = ddlMonth.Value
    End Sub

    Protected Sub gridSubmissions_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridSubmissions.DataBound

        Dim nDay As Integer
        Dim nDays As Integer
        Dim i As Integer
        Dim sThisCaption As String

        nDays = GetDaysInMonth(Session("MonthFrom"))

        For i = 3 To gridSubmissions.Columns.Count - 1
            nDay = Mid(gridSubmissions.Columns(i).Name, 2)
            If nDay > nDays Then
                gridSubmissions.Columns(i).Visible = False
            Else
                gridSubmissions.Columns(i).Visible = True
                sThisCaption = GetDayCaption(Session("MonthFrom"), nDay)
                gridSubmissions.Columns(i).Caption = sThisCaption
                If Left(sThisCaption, 1) = "S" Then
                    gridSubmissions.Columns(i).HeaderStyle.ForeColor = Drawing.Color.Yellow
                Else
                    gridSubmissions.Columns(i).HeaderStyle.ForeColor = Drawing.Color.White
                End If
            End If
        Next

    End Sub

    Protected Sub gridSubmissions_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSubmissions.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0
        Dim nTradeValue As Decimal = 0
        Dim sDay As String
        Dim sTField As String
        'Dim sTPSM As String

        '      sTPSM = e.GetValue("Area")

        If e.DataColumn.FieldName = "Centre" Or e.DataColumn.FieldName = "CentreName" Or e.DataColumn.FieldName = "DMS" Then
            'e.Cell.ToolTip = sTPSM
            Exit Sub
        End If

        sDay = Mid(e.DataColumn.FieldName, 2)
        sTField = "t" & sDay

        If IsDBNull(e.CellValue) = True Then
            nThisValue = 0
        Else
            nThisValue = CDec(e.CellValue)
        End If

        If IsDBNull(e.GetValue(sTField)) = True Then
            nTradeValue = 0
        Else
            nTradeValue = CDec(e.GetValue(sTField))
        End If

        e.Cell.ForeColor = Drawing.Color.White
        If nThisValue <> 0 Then
            e.Cell.BackColor = Drawing.Color.Green
            e.Cell.ForeColor = Drawing.Color.Green
            e.Cell.ToolTip = Format(nTradeValue, "£#,##0") & " Trade Sales"
        Else
            e.Cell.BackColor = Drawing.Color.Red
            e.Cell.ForeColor = Drawing.Color.Red
            e.Cell.ToolTip = "No data received"
        End If

    End Sub

    Sub LoadDataChecker()

        Dim htin As New Hashtable

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nMonth", Session("MonthFrom"))

        dsSubmissions = da.Read(sErrorMessage, "p_Full_Transaction_Grid", htin)
        gridSubmissions.DataSource = dsSubmissions.Tables(0)
        gridSubmissions.DataBind()

        'If dsSubmissions.Tables(0).Rows.Count < 10 Then
        '    gridSubmissions.Settings.ShowVerticalScrollBar = False
        'Else
        '    gridSubmissions.Settings.ShowVerticalScrollBar = True
        'End If


    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub
End Class
