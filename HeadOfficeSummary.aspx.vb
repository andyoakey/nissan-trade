﻿
Imports System.Drawing
Imports DevExpress.XtraPrinting

Partial Class HeadOfficeSummary
    Inherits System.Web.UI.Page
    Dim sThisMonthName As String
    Dim bInApril As Boolean = vbFalse

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        If Page.IsPostBack = False Then
            Session("ProductReportType") = 1
            Session("nDataPeriod") = GetDataLong("SELECT min(Id) FROM DataPeriods WHERE PeriodStatus = 'O'")

            Session("ReportType") = 1
            Session("everpresent_flag") = 0
            Session("showlastyear_flag") = 0
        End If

        ' Check current Month isnt April - this buggers the report up for this fin year ( no completed months)
        Dim sCheckMonth As String = GetDataString("Select LEFT(textperiodname,3) from dataperiods where id = " & Session("nDataPeriod"))
        If sCheckMonth = "Apr" Then
            bInApril = vbTrue
            Session("nDataPeriod") = Session("nDataPeriod") - 1
        End If

        sThisMonthName = Trim(GetDataString("Select textperiodname from dataperiods where id = " & Session("nDataPeriod")))

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        divGrid1.Visible = False
        divGrid2.Visible = False
        divGrid3.Visible = False
        divGrid4.Visible = False
        divGrid5.Visible = False


        'gridPerformanceTotal.Visible = False
        'gridPerformanceTotal2.Visible = False
        'lblCostNotes.Visible = False
        'lblCostNotes2.Visible = False
        'lblCostNotes3.Visible = False

        'gridPerformanceRMEM.Visible = False
        'gridPerformanceRMEM2.Visible = False
        'gridMargin.Visible = False
        'rblProductReportType.Visible = False
        'gridProduct.Visible = False
        'gridProduct2.Visible = False
        'gridActiveCustomers.Visible = False

        If Session("ReportType") = 1 Then
            divGrid1.Visible = True
            ' gridPerformanceTotal.Visible = True
            'gridPerformanceTotal2.Visible = True
            'lblCostNotes.Visible = True
        End If

        If Session("ReportType") = 2 Then
            divGrid2.Visible = True
            '    gridPerformanceRMEM.Visible = True
            '   gridPerformanceRMEM2.Visible = True
            '  lblCostNotes2.Visible = True
        End If

        If Session("ReportType") = 3 Then
            divGrid3.Visible = True
            '     gridMargin.Visible = True
        End If

        If Session("ReportType") = 4 Then
            divGrid4.Visible = True
        End If

        If Session("ReportType") = 5 Then
            divGrid5.Visible = True
            'gridActiveCustomers.Visible = True
        End If

        Call columntext(gridPerformanceTotal)
        Call columntext(gridPerformanceTotal2)
        Call columntext(gridPerformanceRMEM)
        Call columntext(gridPerformanceRMEM2)
        Call columntext(gridMargin)
        Call columntext(gridProduct)
        Call columntext(gridProduct2)
        Call columntext(gridActiveCustomers)

    End Sub

    Sub columntext(grid As DevExpress.Web.ASPxGridView)

        Dim sThisFinYear As String = GetDataString("Select financialyear from dataperiods where id = " & Session("nDataPeriod"))
        Dim xYear As String = " " & Mid(sThisFinYear, 3, 2)
        Dim yYear As String = " " & Right(Trim(sThisFinYear), 2)

        With grid

            Select Case grid.ID
                Case "gridPerformanceTotal"
                    .Columns("rowtext").Caption = "Invoiced Sales Against Target"
                Case "gridPerformanceRMEM"
                    .Columns("rowtext").Caption = "Invoiced Sales Against Target"
                Case "gridPerformanceTotal2"
                    .Columns("rowtext").Caption = "Dealer Cost Of Sales*"
                Case "gridPerformanceRMEM2"
                    .Columns("rowtext").Caption = "Dealer Cost Of Sales*"
                Case "gridProduct"
                    .Columns("rowtext").Caption = "Invoiced Sales By Product"
                Case "gridProduct2"
                    .Columns("rowtext").Caption = "Dealer Cost Of Sales*"
                Case "Else"
                    .Columns("rowtext").Caption = tabReport.ActiveTab.Text
            End Select

            .Columns("apr_data").Caption = "Apr" & xYear
            .Columns("may_data").Caption = "May" & xYear
            .Columns("jun_data").Caption = "Jun" & xYear
            .Columns("jul_data").Caption = "Jul" & xYear
            .Columns("aug_data").Caption = "Aug" & xYear
            .Columns("sep_data").Caption = "Sep" & xYear
            .Columns("oct_data").Caption = "Oct" & xYear
            .Columns("nov_data").Caption = "Nov" & xYear
            .Columns("dec_data").Caption = "Dec" & xYear
            .Columns("jan_data").Caption = "Jan" & yYear
            .Columns("feb_data").Caption = "Feb" & yYear
            .Columns("mar_data").Caption = "Mar" & yYear

            If Session("nDataPeriod") = GetDataLong("Select min(Id) FROM DataPeriods WHERE PeriodStatus = 'O'") Then
                '  only do this if we are looking at the current year
                Select Case Left(sThisMonthName, 3)
                    Case "Apr"
                        .Columns("apr_data").HeaderStyle.Font.Italic = True
                    Case "May"
                        .Columns("may_data").HeaderStyle.Font.Italic = True
                    Case "Jun"
                        .Columns("jun_data").HeaderStyle.Font.Italic = True
                    Case "Jul"
                        .Columns("jul_data").HeaderStyle.Font.Italic = True
                    Case "Aug"
                        .Columns("aug_data").HeaderStyle.Font.Italic = True
                    Case "Sep"
                        .Columns("sep_data").HeaderStyle.Font.Italic = True
                    Case "Oct"
                        .Columns("oct_data").HeaderStyle.Font.Italic = True
                    Case "Nov"
                        .Columns("nov_data").HeaderStyle.Font.Italic = True
                    Case "Dec"
                        .Columns("dec_data").HeaderStyle.Font.Italic = True
                    Case "Jan"
                        .Columns("jan_data").HeaderStyle.Font.Italic = True
                    Case "Feb"
                        .Columns("feb_data").HeaderStyle.Font.Italic = True
                    Case "Mar"
                        .Columns("mar_data").HeaderStyle.Font.Italic = True
                End Select
                If Session("ReportType") = 5 Then
                    .Columns("tot_data").Caption = "YTD Average Completed months only”
                Else
                    .Columns("tot_data").Caption = "YTD Completed months only"
                End If
            End If




        End With

        grid.DataBind()

    End Sub

    Protected Sub gridActiveCustomers_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridActiveCustomers.HtmlDataCellPrepared



        If e.CellValue Is Nothing Then
        Else

            'If Session("nDataPeriod") = GetDataLong("SELECT min(Id) FROM DataPeriods WHERE PeriodStatus = 'O'") Then
            '    'only do this if we are looking at the current year
            '    If bInApril = vbFalse Then
            '        If UCase(Mid(e.DataColumn.FieldName, 1, 3)) = UCase(Mid(sThisMonthName, 1, 3)) Then
            '            e.Cell.Font.Italic = True
            '            e.Cell.ForeColor = Color.SlateGray
            '        End If
            '    End If
            'End If

            If e.DataColumn.FieldName <> "id" And e.DataColumn.FieldName <> "rowtext" Then

                If e.CellValue = 0 Then
                    e.Cell.Text = ""
                Else
                    If e.GetValue("id") = 1 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 2 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 3 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 4 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 5 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 6 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 7 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 8 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 9 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 10 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 11 Then
                        e.Cell.Text = Format(e.CellValue, "##,##0")
                    End If

                    If e.GetValue("id") = 12 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 13 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 14 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 15 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                    If e.GetValue("id") = 16 Then
                        e.Cell.Text = Format(e.CellValue, "£##,##0")
                    End If

                    If e.GetValue("id") = 17 Then
                        e.Cell.Text = Format(e.CellValue, "£##,##0")
                    End If

                    If e.GetValue("id") = 18 Then
                        e.Cell.Text = Format(e.CellValue, "£##,##0")
                    End If

                    If e.GetValue("id") = 19 Then
                        e.Cell.Text = Format(e.CellValue, "£##,##0")
                    End If

                    If e.GetValue("id") = 20 Then
                        e.Cell.Text = Format(e.CellValue, "£##,##0")
                    End If

                    If e.GetValue("id") = 21 Then
                        e.Cell.Text = Format(e.CellValue * 100, "###.00") & "%"
                    End If

                End If
            End If
        End If
    End Sub

    Protected Sub gridPerformanceTotal_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles _
        gridPerformanceTotal.HtmlDataCellPrepared,
        gridPerformanceTotal2.HtmlDataCellPrepared,
        gridPerformanceRMEM.HtmlDataCellPrepared,
        gridPerformanceRMEM2.HtmlDataCellPrepared,
        gridMargin.HtmlDataCellPrepared,
        gridProduct.HtmlDataCellPrepared,
        gridProduct2.HtmlDataCellPrepared

        If e.DataColumn.FieldName <> "rowtext" Then

            If e.CellValue Is Nothing Or e.CellValue = 0 Then
            Else

                'If bInApril = vbFalse Then
                '    If UCase(Mid(e.DataColumn.FieldName, 1, 3)) = UCase(Mid(sThisMonthName, 1, 3)) Then
                '        e.Cell.Font.Italic = True
                '        e.Cell.ForeColor = Color.SlateGray
                '        e.Cell.Font.Size = 7
                '    End If
                'End If

                Dim bPoundColumn As Integer = InStr(e.GetValue("rowtext"), "£")
                        Dim bVsColumn As Integer = InStr(e.GetValue("rowtext"), "vs")

                        If bVsColumn > 0 Then
                            If e.CellValue > 0 Then
                                e.Cell.BackColor = Color.Green
                                e.Cell.ForeColor = Color.White
                            End If
                            If e.CellValue = 0 Then
                                e.Cell.BackColor = Color.White
                            End If
                            If e.CellValue < 0 Then
                                e.Cell.BackColor = Color.Red
                                e.Cell.ForeColor = Color.White
                            End If
                        End If

                        If bPoundColumn > 1 Then
                            'should be ok already
                        Else
                            e.Cell.Text = Format(e.CellValue * 100, "##0.00") & "%"
                        End If
                    End If
                End If

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "HeadOfficeSummary"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim linkResults1a As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1a)
        Dim linkResults2 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter2)
        Dim linkResults2a As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter2a)
        Dim linkResults3 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter3)
        Dim linkResults4 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter4)
        Dim linkResults4a As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter4a)
        Dim linkResults5 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter5)
        Dim headercell1 As New Link()
        Dim headercell1a As New Link()
        Dim headercell2 As New Link()
        Dim headercell2a As New Link()
        Dim headercell3 As New Link()
        Dim headercell4 As New Link()
        Dim headercell4a As New Link()
        Dim headercell5 As New Link()
        Dim blankcell As New Link()


        AddHandler headercell1.CreateDetailArea, AddressOf createheadercell1
        AddHandler headercell1a.CreateDetailArea, AddressOf createheadercell1a
        AddHandler headercell2.CreateDetailArea, AddressOf createheadercell2
        AddHandler headercell2a.CreateDetailArea, AddressOf createheadercell2a
        AddHandler headercell3.CreateDetailArea, AddressOf createheadercell3
        AddHandler headercell4.CreateDetailArea, AddressOf createheadercell4
        AddHandler headercell4a.CreateDetailArea, AddressOf createheadercell4a
        AddHandler headercell5.CreateDetailArea, AddressOf createheadercell5
        AddHandler blankcell.CreateDetailArea, AddressOf createblankcell

        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {headercell1, linkResults1, blankcell, blankcell, headercell1a, linkResults1a, blankcell, blankcell, headercell2, linkResults2, blankcell, blankcell, headercell2a, linkResults2a, blankcell, blankcell, headercell3, linkResults3, blankcell, blankcell, headercell4, linkResults4, blankcell, blankcell, headercell4a, linkResults4a, blankcell, blankcell, headercell5, linkResults5})

        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub
    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles _
        ASPxGridViewExporter1.RenderBrick,
        ASPxGridViewExporter1a.RenderBrick,
        ASPxGridViewExporter2.RenderBrick,
        ASPxGridViewExporter2a.RenderBrick,
        ASPxGridViewExporter3.RenderBrick,
        ASPxGridViewExporter4.RenderBrick,
        ASPxGridViewExporter4a.RenderBrick,
        ASPxGridViewExporter5.RenderBrick

        Call GlobalRenderBrick(e)


        If e.Value Is Nothing Then
        Else
            If e.Column.Name <> "id" And e.Column.Name <> "rowtext" Then
                If e.Value = "0" Then
                    e.TextValue = ""
                    e.BrickStyle.ForeColor = Color.Transparent
                Else
                    If InStr(e.GetValue("rowtext"), "%") > 0 Then
                        e.TextValueFormatString = "0.00%"
                    Else
                        If InStr(e.GetValue("rowtext"), "£") > 0 Then
                            e.TextValueFormatString = "£##,##0"
                        Else
                            e.TextValueFormatString = "##,##0"
                        End If
                    End If
                End If

                Dim bVsColumn As Integer = InStr(e.GetValue("rowtext"), "vs")

                If bVsColumn > 0 Then
                    If e.Value > 0 Then
                        e.BrickStyle.BackColor = Color.Green
                        e.BrickStyle.ForeColor = Color.White
                    End If
                    If e.Value = 0 Then
                        e.BrickStyle.BackColor = Color.White
                        e.BrickStyle.ForeColor = Color.White
                    End If
                    If e.Value < 0 Then
                        e.BrickStyle.BackColor = Color.Red
                        e.BrickStyle.ForeColor = Color.White
                    End If
                End If



            End If
        End If


    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub tabReport_ActiveTabChanged(ByVal source As Object, ByVal e As DevExpress.Web.TabControlEventArgs) Handles tabReport.ActiveTabChanged
        Session("ReportType") = e.Tab.Index + 1
    End Sub

    Private Sub chkLikeForLike_ValueChanged(sender As Object, e As EventArgs) Handles chkLikeForLike.ValueChanged
        If chkLikeForLike.Checked Then
            Session("everpresent_flag") = 1
        Else
            Session("everpresent_flag") = 0
        End If
    End Sub

    Private Sub chkLastYear_ValueChanged(sender As Object, e As EventArgs) Handles chkLastYear.ValueChanged
        If chkLastYear.Checked Then
            Session("showlastyear_flag") = 1
        Else
            Session("showlastyear_flag") = 0
        End If
    End Sub

    Public Sub createblankcell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.White)
        brick.Text = " "
        brick.Rect = New RectangleF(0, 0, 2000, 20)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell1(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = tabReport.Tabs(0).Text
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell1a(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "Dealer Purchases"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell2(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = tabReport.Tabs(1).Text
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell2a(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "Dealer Purchases"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell3(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = tabReport.Tabs(2).Text
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell4(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = tabReport.Tabs(3).Text
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell4a(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "Dealer Purchases By Product"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell5(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = tabReport.Tabs(4).Text
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Protected Sub cboFY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFY.SelectedIndexChanged
        Dim sThisFY As String = cboFY.Value
        Session("nDataPeriod") = GetDataLong("Select max(ID) from dataperiods where financialyear = '" & sThisFY & "' and periodstatus = 'C'")
    End Sub

End Class





