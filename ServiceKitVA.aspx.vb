﻿Imports System.Data
Imports DevExpress.XtraCharts
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ServiceKitVA

    Inherits System.Web.UI.Page

    Dim dsGraph As DataSet
    Dim dsSummary As DataSet
    Dim dsBreakdown As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim htin As New Hashtable
    Dim htinBreakdown As New Hashtable
    Dim htinSummary As New Hashtable
    Dim nThisYear As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site -VA Service Kit Year on Year Performance"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        nThisYear = GetDataDecimal("Select year(startdate) from dataperiods where id = (Select  min(id) from dataperiods where periodstatus = 'O')")

    End Sub

    Sub ChartLoad()

        Dim i As Integer
        Dim nThisValue As Integer

        While WebChartControl1.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End While

        Dim series1 As New Series
        Dim view1 As New SideBySideBarSeriesView

        series1.ArgumentScaleType = ScaleType.Qualitative

        series1.View = view1
        series1.LegendText = Trim(Str(nThisYear - 1))

        Dim series2 As New Series
        Dim view2 As New SideBySideBarSeriesView

        series2.ArgumentScaleType = ScaleType.Qualitative
        series2.View = view2
        series2.LegendText = Trim(Str(nThisYear))

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nYear", nThisYear)
        htin.Add("@nType", 1)

        dsGraph = da.Read(sErrorMessage, "p_ServiceKitDataVA", htin)
        With dsGraph.Tables(0).Rows(0)

            For i = 1 To 12
                nThisValue = Val(.Item("LastYear_" & Trim(Str(i))))
                series1.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
                nThisValue = Val(.Item("ThisYear_" & Trim(Str(i))))
                series2.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
            Next i

        End With

        WebChartControl1.Series.Add(series1)
        WebChartControl1.Series.Add(series2)

        Dim diagram As XYDiagram = CType(WebChartControl1.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Month"

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Total Kits"

        htinSummary.Add("@sSelectionLevel", Session("SelectionLevel"))
        htinSummary.Add("@sSelectionId", Session("SelectionId"))
        htinSummary.Add("@nYear", nThisYear)
        dsSummary = da.Read(sErrorMessage, "sp_VAServiceKitData_Summary", htinSummary)
        gridTotals.DataSource = dsSummary.Tables(0)
        gridTotals.DataBind()

        htinBreakdown.Add("@sSelectionLevel", Session("SelectionLevel"))
        htinBreakdown.Add("@sSelectionId", Session("SelectionId"))
        htinBreakdown.Add("@nYear", nThisYear)
        htinBreakdown.Add("@nType", 0)
        dsBreakdown = da.Read(sErrorMessage, "sp_ServiceKitDataGrid_VA", htinBreakdown)
        gridServiceKits.DataSource = dsBreakdown.Tables(0)
        gridServiceKits.DataBind()

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Call ChartLoad()

        gridServiceKits.Columns("LastYear_1").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_1").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_2").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_2").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_3").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_3").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_4").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_4").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_5").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_5").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_6").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_6").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_7").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_7").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_8").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_8").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_9").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_9").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_10").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_10").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_11").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_11").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_12").Caption = nThisYear - 1 - 2000
        gridServiceKits.Columns("ThisYear_12").Caption = nThisYear - 2000

        gridServiceKits.Columns("LastYear_1").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_1").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_2").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_2").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_3").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_3").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_4").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_4").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_5").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_5").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_6").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_6").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_7").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_7").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_8").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_8").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_9").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_9").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_10").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_10").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_11").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_11").ToolTip = "Service Kits Sold in this month in " & nThisYear

        gridServiceKits.Columns("LastYear_12").ToolTip = "Service Kits Sold in this month in " & nThisYear - 1
        gridServiceKits.Columns("ThisYear_12").ToolTip = "Service Kits Sold in this month in " & nThisYear


        gridServiceKits.Columns("ThisYear_1diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_2diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_3diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_4diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_5diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_6diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_7diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_8diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_9diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_10diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_11diff").ToolTip = "Uplift from last year"
        gridServiceKits.Columns("ThisYear_12diff").ToolTip = "Uplift from last year"

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub

    Protected Sub gridServiceKits_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridServiceKits.HtmlDataCellPrepared

        If InStr(e.DataColumn.FieldName, "diff") > 0 Then
            e.Cell.ForeColor = System.Drawing.Color.White
            If e.CellValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.DarkGreen
            End If
            If e.CellValue < 0 Then
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        Else
            If InStr(e.DataColumn.FieldName, "Year") > 0 Then
                If e.CellValue = 0 Then
                    e.Cell.ForeColor = System.Drawing.Color.White
                    e.Cell.BackColor = System.Drawing.Color.White
                End If
            End If
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)

        If e.RowType = GridViewRowType.Data Then
            If InStr(e.Column.Name, "diff") > 0 Then
                e.BrickStyle.ForeColor = System.Drawing.Color.White
                If e.Value >= 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.DarkGreen
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                End If
            End If
        End If
    End Sub

    Protected Sub gridInvoices_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    '    Protected Sub btnExcel_Click()
    '
    '   ASPxGridViewExporter1.FileName = "Service Kit Performance Year On Year"
    '  ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    'End Sub
    Protected Sub ExportToExcel()


        gridServiceKits.DataBind()
        Dim sFileName As String = "VAServiceKitPerformance"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)

    End Sub

End Class

