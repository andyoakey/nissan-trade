﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ExcludedSalesSummary

    Inherits System.Web.UI.Page

    Dim Total_i As Decimal = 0
    Dim Total_x As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Excluded Sales Summary"
    End Sub

    Protected Sub dsDealerBonus_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsExcluded.Init
        dsExcluded.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Not IsPostBack Then
            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
            Session("AnalysisType") = 1
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
            Session("MonthTo") = Session("MonthFrom")
            Session("FocusType") = 0
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Excluded Sales Summary", "Excluded Sales Summary")
        End If

    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "Excluded Sales Summary"
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub gridExcluded_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridExcluded.CustomSummaryCalculate

        '        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
        '       Total_i = 0
        '      Total_x = 0
        '     End If

        ''If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
        'Total_i = Total_i + e.GetValue("salevalue_i")
        'Total_x = Total_x + e.GetValue("salevalue_x")
        'End If
        '
        'If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
        '   If Total_i <> 0 And Total_x <> 0 Then
        'e.TotalValue = (100 - (Total_x / Total_i) * 100) / 100
        'Else
        'e.TotalValue = 0
        'End If
        'End If

    End Sub



End Class


