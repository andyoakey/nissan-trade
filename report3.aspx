﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report3.aspx.vb" Inherits="report3" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
        
        <table style="width: 100%;">
            <tr>
                <td width="260px">
                    <dx:ASPxLabel 
                        ID="lblPageTitle" 
                        Text="Reports"
                        Font-Size="Larger"
                        runat="server">
                    </dx:ASPxLabel>
                </td>
                <td style="width: calc(100% - 260px);">
                    <dx:ASPxLabel 
                        ID="lblSubTitle" 
                        Text="Service Kit Year on Year Performance"
                        Font-Size="Larger"
                        runat="server">
                    </dx:ASPxLabel>   
                </td>
            </tr>
            <tr>
                <td width="260px">
                    <dx:ASPxButton 
                         BackColor="White" 
                       ID="btn1" 
                        runat="server" 
                        Style="left: 0px; position: absolute; top: 30px; z-index: 2;"
                        EnableDefaultAppearance="false" 
                        Text="" 
                        CssClass="special" 
                        Border-BorderStyle="None" 
                        HorizontalPosition="True"
                        Image-Url="~/Images2014/ProductSalesAnalysis.png" 
                        Image-UrlHottracked="~/Images2014/ProductSalesAnalysis_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White" 
                        ID="btn2" 
                        runat="server" 
                        Style="left: 128px; position: absolute; top: 30px; z-index: 2;"
                        EnableDefaultAppearance="false"
                        Text="" 
                        CssClass="special" 
                        Visible="True" 
                        Border-BorderStyle="None" 
                        Image-Url="~/Images2014/pfc.png"
                        Image-UrlHottracked="~/Images2014/pfc-hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White" 
                        ID="btn3" 
                        runat="server" 
                        Style="left: 0px; position: absolute; top: 160px; z-index: 2;"
                        EnableDefaultAppearance="false" 
                        Text="" 
                        CssClass="special" 
                        Visible="True"
                        Border-BorderStyle="None" 
                        Image-Url="~/Images2014/BestSellingParts.png" 
                        Image-UrlHottracked="~/Images2014/BestSellingParts_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White" 
                        ID="btn4" 
                        runat="server" 
                        Style="left: 128px; position: absolute; top: 160px; z-index: 2;"
                        EnableDefaultAppearance="false" 
                        Text="" 
                        CssClass="special" 
                        Border-BorderStyle="None" 
                        Image-Url="~/Images2014/SalesComparison.png" 
                        Image-UrlHottracked="~/Images2014/SalesComparison_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton ID="btn5" 
                        BackColor="White" 
                        runat="server" 
                        Style="left: 0px; position: absolute; top: 280px; z-index: 2;"
                        EnableDefaultAppearance="false" 
                        Text="" 
                        CssClass="special" 
                        Visible="True"
                        Border-BorderStyle="None" 
                        Image-Url="~/Images2014/ServiceKitSales.png" 
                        Image-UrlHottracked="~/Images2014/ServiceKitSales_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton ID="btn6" 
                        BackColor="White" 
                        runat="server" 
                        Style="left: 128px; position: absolute; top: 280px; z-index: 2;"
                        EnableDefaultAppearance="false" 
                        Text="" 
                        CssClass="special" 
                        Visible="True"
                        Border-BorderStyle="None" 
                        Image-Url="~/Images2014/ServiceKitSalesVA.png" 
                        Image-UrlHottracked="~/Images2014/ServiceKitSalesVA_Hover.png">
                    </dx:ASPxButton>
        
                    <dx:ASPxButton ID="btn7" 
                         BackColor="White" 
                         runat="server" 
                         Style="left: 0px; position: absolute; top: 410px; z-index: 2;"
                         EnableDefaultAppearance="false" 
                         Text=""
                         CssClass="special"
                         Visible="True"
                         Border-BorderStyle="None" 
                         Image-Url="~/Images2014/CoreStockSales.png" 
                         Image-UrlHottracked="~/Images2014/CoreStockSales_Hover.png"> 
                    </dx:ASPxButton>
                
                    <dx:ASPxButton ID="btn8" 
                         BackColor="White" 
                         runat="server" 
                         Style="left: 128px; position: absolute; top: 410px; z-index: 2;"
                         EnableDefaultAppearance="false" 
                         Text=""
                         CssClass="special"
                         Visible="True"
                         Border-BorderStyle="None" 
                         Image-Url="~/Images2014/VASales.png" 
                         Image-UrlHottracked="~/Images2014/VASales_Hover.png"> 
                    </dx:ASPxButton>
                
                    <dx:ASPxButton ID="btn9" 
                         BackColor="White" 
                         runat="server" 
                         Style="left: 0px; position: absolute; top: 540px; z-index: 2;"
                         EnableDefaultAppearance="false" 
                         Text=""
                         CssClass="special"
                         Visible="True"
                         Border-BorderStyle="None" 
                         Image-Url="~/Images2014/RemanSales.png" 
                         Image-UrlHottracked="~/Images2014/RemanSales_Hover.png"> 
                    </dx:ASPxButton>
                
                
                </td>
                <td style="width: calc(100% - 260px);">
                    <dxchartsui:WebChartControl 
                        ID="WebChartControl1" 
                        visible="False"
                        runat="server"
                        Width="1010px" 
                        Height="400px"		
                        CssClass="graph_respond"	 
                        ClientInstanceName="WebChartControl1" 
                        PaletteName="Palette 2" 
                        CrosshairEnabled="True" AppearanceNameSerializable="Gray" PaletteBaseColorNumber="1">
                            <emptycharttext font="Calibri, 12pt" />
                            <smallcharttext font="Calibri, 12pt" />
                            <diagramserializable>
                                <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                    </axisy>
                                    <margins bottom="0" left="0" right="0" />
                                </cc1:XYDiagram>
                            </diagramserializable>
                            <legend alignmenthorizontal="Right" alignmentvertical="TopOutside" direction="LeftToRight" equallyspaceditems="False" font="Calibri, 12pt" horizontalindent="3" markersize="30, 14" textoffset="0" verticalindent="0">
                                <margins bottom="0" left="0" right="0" top="0" />
                                <border visibility="True" />
                                <shadow visible="True" />
                            </legend>
                            <seriesserializable>
                                <cc1:Series LabelsVisibility="True" Name="Series 1">
                                    <labelserializable>
                                        <cc1:SideBySideBarSeriesLabel Antialiasing="True" BackColor="Transparent" Font="Calibri, 10pt" LineVisibility="False" Position="Top" TextColor="Black" LineColor="255, 255, 255">
                                            <shadow color="255, 255, 255" />
                                            <border color="255, 0, 0" visibility="True" />
                                        </cc1:SideBySideBarSeriesLabel>
                                    </labelserializable>
                                </cc1:Series>
                                <cc1:Series LabelsVisibility="True" Name="Series 2">
                                    <labelserializable>
                                        <cc1:SideBySideBarSeriesLabel Antialiasing="True" BackColor="Transparent" Font="Calibri, 10pt" LineColor="0, 0, 192" LineVisibility="False" Position="Top" TextColor="0, 0, 192">
                                            <border color="255, 0, 0" Visibility="True" />
                                        </cc1:SideBySideBarSeriesLabel>
                                    </labelserializable>
                                </cc1:Series>
                            </seriesserializable>
                            <titles>
                                <cc1:ChartTitle Font="Calibri, 14pt" Text="Service Kit Sales" />
                            </titles>
                            <palettewrappers>
                                <dxchartsui:PaletteWrapper Name="Palette 1" ScaleMode="Repeat">
                                    <palette>
                                        <cc1:PaletteEntry />
                                    </palette>
                                </dxchartsui:PaletteWrapper>
                                <dxchartsui:PaletteWrapper Name="Palette 2" ScaleMode="Repeat">
                                    <palette>
                                        <cc1:PaletteEntry Color="58, 142, 192" Color2="58, 142, 192" />
                                    </palette>
                                </dxchartsui:PaletteWrapper>
                            </palettewrappers>
                    </dxchartsui:WebChartControl>            
                </td>
            </tr>
        </table>
    </div>  
</asp:Content>

