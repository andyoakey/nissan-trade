var ValueChain = {

    init: function () {
		var self = this;
			
        $.extend(self, {
			rememberMe: $('.remember-me, .remember-me.on'),
			closeOverlay: $('.close-modal'),
			overlayTriggers: $('.overlay'),
			overlayContainer: $('.overlay-container')
		});
		
		self.overlayTriggers.bind('click', function(e){
			e.preventDefault;
			var displayContent = $(this).attr('data-modal');
			
			self.addOverlay(displayContent);
			self.formElements();
		});
		
		$(document).on('click', '.close-modal', function(e) {
			e.preventDefault;
			
			$('.overlay-container').fadeOut('slow', function(){
				$('.overlay-container').remove();
			});
		});
		
		self.rememberMe.bind('click', function(e){
			e.preventDefault;
			var button = $(this).find('span');
			
			if (button.hasClass('off')){
				button.animate({left: '1px'}, 100, function(){
					button.removeClass('off').addClass('on');
					$(this).parent().addClass('on');
					$('#rememberMe').prop('checked', true );
				});
			}
			else {
				button.animate({left: '60px'}, 100, function(){
					button.removeClass('on').addClass('off');
					$(this).parent().removeClass('on');
					$('#rememberMe').prop('checked', false );
				});
			}
		})
		
		//Client Side validation for Login Button
		$('#login').bind('click',function(e){
			e.preventDefault();
			var username = $('.username');
			var password  = $('.password');
			var errorRow = $('.username').parent();
			
			if(username.text() == 'Username'){
				errorRow.addClass('error');
			}
			else{
				//form submit action
			}
		});
		
		$(window).resize(function(){
			var windowHeight = $(window).height();
			
			$('.overlay-container').css('height',windowHeight);
			
			self.positionContent();
		
		});
		
		//form focus removal
		self.formElements();
	},
	formElements: function(){
		var self = this;
		var el = $('input[type="text"], input[type="password"], textarea');
		
		el.focus(function(e) {
			if(e.target.value == 'Password'){
					e.target.type = 'password';
					e.target.value = '';
				}
			else if (e.target.value == e.target.defaultValue)
				e.target.value = '';
		});
		el.blur(function(e) {
			//if(e.target.name == 'password'){
			//	e.target.type = 'text';
			//	e.target.value = e.target.defaultValue;
			//
			//else } Caused issues with iPad 
			if (e.target.value == '')
				e.target.value = e.target.defaultValue;
		});
		
		container = $('select, input, textarea');  
		container.each(function (index, element) {

			var object = $(element);
			if (object.is('input[type=radio]')) {
				self.styleRadio(object);
			}
			else if (object.is('input[type=checkbox]')) {
				self.styleCheckbox(object);
			}
			else if(object.is('select')){
				self.styleSelect(object);
			}
			else if (object.is('input[type=file]')) {
				self.styleUploadFields(object);
			}
		});
	},
	
	styleCheckbox: function (object) {
		// Create a dummy checkbox button for styling
		if(!object.parent('div').hasClass('check-replacement')){
			object.wrap('<div class="check-replacement">');

			// Check if checkbox is selected and add selected state to dummy checkbox if needed
			if (object.is(':checked')) {
				object.parent().addClass('selected');
			}
		
			// Bind an event to the form element so that we can add/remove selected state to our dummy checkbox		
			object.bind({
				'change': function () {
					if ($(this).is(':checked')) {
						object.parent().addClass('selected');
					}
					else {
						object.parent().removeClass('selected');
					}
				}
			});
		
			// Hide the real Checkbox
			object.css({
				'opacity': '0',
				'filter': 'alpha(opacity=0)'
			});
		}
	},
	
	styleSelect: function (object) {

		// Create a dummy select box for styling
		if(!object.parent('div').hasClass('select-replacement')){
			object.wrap('<div class="select-replacement">');
		
			// Insert a div into our dummy select box where we will display the currently selected item
			object.parent().prepend('<div>' + object.find(':selected').html() + '</div>');
			
			// Bind an event to the form element so that we can updated the selected item on our dummy selectbox
			object.bind({
				'change': function () {
					selectValue = object.find(':selected').html();
					object.parent().find('div').html(selectValue);
				},
				'focus': function () {
					object.parent().addClass('focus');
				},
				'blur': function () {
					$('.focus').removeClass('focus');
				}
			});
	
			// Capture use of keys on the dummy selectboxes to make them accessible
			object.keydown(function (e) {
				switch (e.keyCode) {
					// User pressed "up"                
					case 38:
						var selectedIndex = object.find(':selected').index();
	
						if (selectedIndex <= 0) {
							selectedIndex;
						}
						else {
							selectedIndex = selectedIndex - 1;
						}
	
						var selectValue = object.find('option').eq(selectedIndex).html();
						object.parent().find('div').html(selectValue);
						break;
	
					// User pressed "down"
					case 40:
						var selectedIndex = object.find(':selected').index();
						var totalItems = object.find('option').length - 1;
	
						if (selectedIndex >= totalItems) {
							selectedIndex;
						}
						else {
							selectedIndex = selectedIndex + 1;
						}
	
						var selectValue = object.find('option').eq(selectedIndex).html();
						object.parent().find('div').html(selectValue);
						break;
				}
			});
	
			// Hide the real select box
			object.css({
				'opacity': '0',
				'filter': 'alpha(opacity=0)'
			});
		}
	},
		
	addOverlay: function(content){
		var self = this;
		var overlay = '<div class="overlay-container"><div class="container"></div></div>'
		var windowHeight = $(window).height();
		
		if(content == 'feedback'){
			$('footer').before(overlay)
			self.feedbackForm(content);
		}
		else{
			$('body').append(overlay)
		}
		
		$('.overlay-container').fadeIn('slow', function(){
			self.modal(content);
		}).css('height', windowHeight);
	},
	
	feedbackForm: function(content){
		var self = this;
		
		var feedback = $('.feedback')
		
		if(feedback.parent().hasClass('active')){
			feedback.parent().removeClass('active')
			$('.overlay-container').fadeOut('slow', function(){
				$('.overlay-container').delay(1000).remove();
			});
		}
		else{
			$('.overlay-container .container').remove();
			feedback.parent().addClass('active')
		}
	},
	modal: function(content){
		var self = this;
		
		overlay = $('.overlay-container .container');
		var modal = $('.modal');
				
		modal.each(function(){	

			var modalContent = $(this).html();
		
			if($(this).attr('id') == content){
				overlay.append(modalContent)
			}
		});
		
		self.positionContent()
	},
	positionContent: function(){
		var self = this;
		
		var windowHeight = $(window).height();
		var modalContent = $('.overlay-container .container');
		var modalHeight = $('.overlay-container .container').height();
		var xPosition = (windowHeight-modalHeight) / 2;	 
		
		if(modalHeight > windowHeight){
			modalContent.css('margin-top', '10px');
		}else{
			modalContent.css('margin-top', xPosition);
		}
		

	}
}
