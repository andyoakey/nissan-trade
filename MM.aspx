﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="mm.aspx.vb" Inherits="MM" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <dx:ASPxPopupControl 
        ID="PopFilter" 
        ClientInstanceName="PopFilter" 
        Width="1200px"
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Select filters and press 'Go'"
        Modal="False" 
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">

        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
                <table id="trFilter" width="100%">
    
                    <tr>
                        <td align="left" valign="middle" style="width:25%" >
                             <dx:ASPxLabel 
                                ID="lblFilter1" 
                                runat ="server" 
                                Text="Postcode Districts" />
                       </td>
                       
                        <td align="left" valign="middle" style="width:25%; margin-left:10px">
                            
                              <dx:ASPxLabel 
                                ID="lblFilter2" 
                                runat ="server" 
                                Text="Business Type" />
                              
                                </td>
               
                        <td align="left" valign="middle" style="width:25%; margin-left:10px">
                              <dx:ASPxLabel 
                                ID="ASPxLabel2" 
                                runat ="server" 
                                Text="Customer Type" />
                              
                        </td>
                        
                        <td align="left" valign="middle" style="width:25%; margin-left:10px">
                           
                              <dx:ASPxCheckBox
                                 ID="chkSpendFilters" 
                                 runat="server" 
                                 AutoPostBack="true" 
                                 Text="Add Spend Filters" 
                                 TextAlign="right" 
                                 Checked="false" />
                  
                            </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            
                            <dx:ASPxListBox  
                                ID="ddlPostCodes" 
                                ClientInstanceName="ddlPostCodes" 
                                runat="server" 
                                TextField="PostCodeDistrict" 
                                ValueField="PostCodeDistrict"
                                Width="300px"
                                Height="500px"
                                Border-BorderStyle="None"
                                Tooltip="Select postcodes by using [Ctrl] and the mouse"
                                SelectionMode="CheckColumn">
                            </dx:ASPxListBox>
                            
                            <br />
                      
                              <dx:ASPxCheckBox
                                 ID="chkOutOfTerr" 
                                 runat="server" 
                                 visible ="false"
                                 AutoPostBack="true" 
                                 Text="Include out of Territory ?" 
                                 TextAlign="right" 
                                 Checked="false" />
                        
                        </td>

                        <td align="left" valign="top" style="margin-left:10px">
                                               <dx:ASPxCheckBoxList
                                                   id="chkBusType"
                                                   runat ="server"
                                                   Border-BorderStyle="None">
                                                    <Items>
                                                        <dx:ListEditItem Text="Bodyshop" Value="1"/>
                                                        <dx:ListEditItem Text="Breakdown Recovery Services" Value="2"/>
                                                        <dx:ListEditItem Text="Car Dealer New/Used" Value="3" />
                                                        <dx:ListEditItem Text="Independent Motor Trader" Value="4" />
                                                        <dx:ListEditItem Text="Mechanical Specialist" Value="5" />
                                                        <dx:ListEditItem Text="Motor Factor" Value="6" />
                                                        <dx:ListEditItem Text="Parts Supplier" Value="7" />
                                                        <dx:ListEditItem Text="Taxi & Private Hire" Value="8" />
                                                        <dx:ListEditItem Text="Windscreens" Value="9" />
                                                        <dx:ListEditItem Text="Other" Value="99" />
                                                    </Items>
                                               </dx:ASPxCheckBoxList>
                                     </td>
                
                        <td align="left" valign="top" style="margin-left:10px; margin-bottom:25px">
           
                                     <dx:ASPxCheckBoxList
                                id ="chkFilterCustType" 
                                Border-BorderStyle="None"
                                runat="server">
                               <Items>
                                   <dx:ListEditItem Text="Active" Value="0" Selected="true"/>
                                   <dx:ListEditItem Text="In-Active" Value="1"/>
                                   <dx:ListEditItem Text="Lapsed" Value="2"/>
                                   <dx:ListEditItem Text="Prospect" Value="3"/>
                                   <dx:ListEditItem Text="New Prospect" Value="4"/>
                               </Items>
                            </dx:ASPxCheckBoxList>
                            <br />
                            <br />
                            <br />

                            <table>
                                   <tr>
                                    <td>
                                                   <dx:ASPxLabel 
                                                       ID="ASPxLabel3" 
                                                       runat ="server" 
                                                       Text="Drivetime" />
                                    </td>
            
                                    <td>
                                    </td>
                                    
                                </tr>
                                
                                <tr>
                                       <td>
                                                  <dx:ASPxLabel 
                                                       ID="ASPxLabel4" 
                                                       runat ="server" 
                                                       Text="Less than" />
                                      </td>
                                
                                        <td>
                                                 <dx:ASPxLabel 
                                                       ID="ASPxLabel5" 
                                                       runat ="server" 
                                                       Text="More than" />
                                        </td>
                                 
                                     </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxSpinEdit ID="txtDriveTime1" runat="server" Number="0" Width="50px" />
                                    </td>
                                    <td>
                                        <dx:ASPxSpinEdit ID="txtDriveTime2" runat="server" Number="0" Width="50px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                            <dx:ASPxLabel 
                                                       ID="ASPxLabel6" 
                                                       runat ="server" 
                                                       Text="Distance" />
                                    </td>
     
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                                        <dx:ASPxLabel 
                                                       ID="ASPxLabel7" 
                                                       runat ="server" 
                                                       Text="Less than" />
                               </td>
                                    <td>
                                                        <dx:ASPxLabel 
                                                       ID="ASPxLabel8" 
                                                       runat ="server" 
                                                       Text="More than" />
                                </td>
                                </tr>
                                <tr>
                                    <td>
                                        <dx:ASPxSpinEdit ID="txtDistance1" runat="server" Number="0" Width="50px" />
                                    </td>
                                    <td>
                                        <dx:ASPxSpinEdit ID="txtDistance2" runat="server" Number="0" Width="50px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
          
                        <td align="left" valign="top" style="margin-left:10px">
                                    <dx:ASPxLabel 
                                            ID="lblIncCustomers" 
                                            Visible ="false"
                                            runat ="server" 
                                            Text="Include Customers who" />
                              
                                    <dx:ASPxComboBox
                                            ID="ddlHaveHaveNot" 
                                            Visible="false"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="have purchased"  Value="0" Selected="true"/>
                                                <dx:ListEditItem Text="have not purchased"  Value ="1"/>
                                            </Items>    
                                    </dx:ASPxComboBox>
                            
                             
                                <dx:ASPxTreeView
                                    ID="tvProducts" 
                                    runat="server" 
                                    AllowSelectNode="true" 
                                    AllowCheckNodes="true" 
                                    CheckNodesRecursive="true" 
                                    Style="margin-top:10px" 
                                    Visible="False">
                                </dx:ASPxTreeView>
                       
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                        </td>
                        <td align="left" valign="middle" style="margin-left:10px">
                        </td>
                        <td align="left" valign="middle" style="margin-left:10px">
                        </td>
                        <td align="left" valign="middle" style="margin-left:10px">
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                        </td>
                        <td align="left" valign="middle" style="margin-left:10px">
                        </td>
                        <td align="left" valign="middle" style="margin-left:10px">
                        </td>
                       <td align="right" valign="middle" style="margin-left:10px">
                            <dx:ASPxButton 
                                ID="btnSearch" 
                                runat="server" 
                                Text="Go" />
                       </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupContHist" 
        ClientInstanceName="PopupContHist" 
        Width="890px"
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Select type of contact and add a comment"
        Modal="False" 
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
 
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol7" runat="server">
                <table id="trContHist" width="100%">
                    <tr>
                        <td align="left" valign="middle" style="width:20%" >
                            <dx:ASPxLabel
                                 ID="lblContHist1" 
                                runat="server" 
                                Text="Type of contact" />
                        </td>
                      
                      <td align="left" valign="middle" style="margin-left:10px">
                           <dx:ASPxComboBox
                                 ID="ddlContHistType" 
                                 runat="server" 
                                 AutoPostBack="false" 
                                 Width="100%">
                                <Items>
                                    <dx:ListEditItem Text="Mailing (all records will be added to History)" Value="3" Selected="true" />
                                    <dx:ListEditItem Text="Email (only records with an email address will be added to History)" Value="1" />
                                </Items>
                            </dx:ASPxComboBox>
                      </td>
             
                     </tr>
             
                     <tr>
                    
                          <td align="left" valign="middle" style="width:20%" >
                               <dx:ASPxLabel 
                                   ID="lblContHist2" 
                                   runat="server" 
                                   Text="Message" />
                         </td>
         
                        <td align="left" valign="top" style="margin-left:10px">
                            <dx:ASPxTextBox
                                 ID="txtContHistComment" 
                                 runat="server" 
                                 TextMode="MultiLine" 
                                 MaxLength="255" 
                                 Width="99%" 
                                 Height="40px"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="middle">
                               <dx:ASPxLabel 
                                    ID="lblContHistError" 
                                    runat="server" 
                                   Text="" 
                                    ForeColor="Red" />
                        </td>
                       <td align="right" valign="middle" style="margin-left:10px">
                            <dx:ASPxButton 
                                ID="btnContHistGo" 
                                runat="server" 
                                Text="Go" 
                                Width="40px"
                                Style="margin-left:10px" />
                       </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopErrorMessage" 
        Width="205px" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopErrorMessage" 
        ShowHeader="True" 
        HeaderText="Marketing Manager Error"
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        Height="40px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
                <dx:ASPxLabel
                    ID="lblNoneFound" 
                    runat="server" 
                    Text="No matching records found." 
                    Width="200px" >
                </dx:ASPxLabel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopNameError" 
        Width="200px" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopNameError" 
        ShowHeader="True" 
        HeaderText="Marketing Manager Error"
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        Height="50px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
                <dx:ASPxLabel 
                    Font-Size="18px" 
                    ID="ASPxLabel1"
                    Text="Enter a name." 
                    runat="server" 
                    Width="330px">
                </dx:ASPxLabel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupRemoveListConfirm" 
        Width="350px" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopMapMessage" 
        ShowHeader="True" 
        HeaderText="Marketing Manager List"
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        Height="50px">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol6" runat="server">

                <table id="tabConfirmRemoveList" style="width:100%">
                    <tr align="left">
                        <td>
                            <dx:ASPxLabel ID="lblRemoveConf" Text="Press Remove to confirm" runat="server" Width="200px"/>
                        </td>
                  
                              <td align="right" style="width:5%">
                            <dx:ASPxButton ID="btnRemoveCancel" runat="server" Text="Cancel" Width="75px" style="margin-right:5px; margin-top:10px">
                            </dx:ASPxButton>
                        </td>
                        <td align="right" style="width:5%">
                            <dx:ASPxButton ID="btnRemoveConfirm" runat="server" Text="Remove" Width="75px" style="margin-right:5px; margin-top:10px">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl
         ID="PopSaveList" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopupSaveList" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        Width="400px" 
        Height="75px"
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        ShowHeader="True" 
        HeaderText="Enter List Name">
       
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">

                <table id="tabSaveList" style="width:100%">
                    <tr align="left">
                        <td>
                            <dx:ASPxTextBox ID="txtName" runat="server" Width="98%"/>
                         </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <dx:ASPxButton 
                                ID="btnSaveList" 
                                runat="server" 
                                Text="Save" 
                                Width="75px" 
                                style="margin-right:10px; margin-top:10px">
                            </dx:ASPxButton>
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopLoadList" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopLoadList" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        Width="400px" 
        Height="75px"
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        ShowHeader="True" 
        HeaderText="Select List">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
                <table id="tabLoadList" style="width:100%">
                    <tr align="left">
                        <td>
                              <dx:ASPxComboBox
                                runat="server"
                                ID="ddlLists" 
                                width="100%"
                                DataSourceID="dsLists" 
                                TextField="ListName" 
                                ValueField="Id"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                              <dx:ASPxButton 
                                ID="btnRemoveList" 
                                runat="server" 
                                Text="Delete" 
                                Width="75px"/> 
                         
                              <dx:ASPxButton 
                                ID="btnLoadList" 
                                runat="server" 
                                Text="Load" 
                                Width="75px"/> 
                                
                           </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

        <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative; left:2px">
            <table id="trCustomerDetails" width="100%" >
                <tr>
                    <td align="left" >
                        <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Customer Marketing Manager" />
                    </td>
                    
                    
                    <td align="right" style="width:5%" >
                            <dx:ASPxButton 
                                ID="btnMM" 
                                runat="server" 
                                Text="Back">
                              </dx:ASPxButton>
                    </td>

                    <td align="right" style="width:5%" >
                            <dx:ASPxButton 
                                ID="btnLoad" 
                                runat="server" 
                                Text="Saved Lists">
                        </dx:ASPxButton>
                    </td>

                    <td align="right" style="width:5%">
                        <dx:ASPxButton 
                            ID="btnNew" 
                            runat="server" 
                            Text="New" >
                        </dx:ASPxButton>
                   </td>
                </tr>
            </table>
        </div>
        
        <br />

        <dx:ASPxGridView 
            ID="gridCompanies" 
            runat="server" 
            onload="GridStyles" style="position:relative; left:5px"
            ClientInstanceName="gridCompanies" 
            AutoGenerateColumns="False" 
            KeyFieldName="Id" 
            Width="100%" 
            DataSourceId="dsCustomers">
                        
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowHeaderFilterButton="False" />

            <SettingsBehavior AllowSort="True" AllowMultiSelection="True"  AllowSelectByRowClick="true"  />
            
            <SettingsPager Visible="True" PageSize="16" Mode="ShowPager" AllButton-Visible="true"  >
            </SettingsPager>
            
            <Columns>

                <dx:GridViewCommandColumn 
                    ShowSelectCheckbox="True" 
                    VisibleIndex="0" 
                    Caption="Select" 
                    Width="4%" 
                    SelectAllCheckboxMode="AllPages"/>

                <dx:GridViewDataTextColumn 
                    FieldName="BusinessName" 
                    Caption="Name" 
                    VisibleIndex="1"
                   Width="12%" 
                    ExportWidth="300" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left"  />
                </dx:GridViewDataTextColumn>        
                
                <dx:GridViewDataTextColumn 
                    FieldName="FormattedAddress" 
                    Caption="Address" 
                    VisibleIndex="2" 
                    Width="25%"
                    ExportWidth="400" >
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" />
                </dx:GridViewDataTextColumn>        
                
                <dx:GridViewDataTextColumn 
                    FieldName="PostCode" 
                    Caption="Postcode"
                    width="5%" 
                    VisibleIndex="3" 
                    ExportWidth="120">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="PostCodeSector" 
                    Caption="Sector" 
                    width="4%" 
                    VisibleIndex="4" 
                    ExportWidth="120">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                     </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="EmailAddress" 
                    Caption="Email" 
                    VisibleIndex="5" 
                    Width="150px"
                    ExportWidth="300">
                   <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" />
                   </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="TelephoneNumber" 
                    Caption="Phone" 
                    VisibleIndex="6" 
                    Width="80px" 
                    ExportWidth="200">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="BusinessTypeDescription" 
                    Caption="Bus.Type" 
                    VisibleIndex="7" 
                    Width="50px" 
                    ExportWidth="300"> 
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="CustomerStatus" 
                    Caption="Spend" 
                    VisibleIndex="8" 
                    ExportWidth="120"
                    Width="50px" >
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="DriveTime" 
                    Name="DriveTime" 
                    Caption="Drive Time" 
                    VisibleIndex="9" 
                    PropertiesTextEdit-DisplayFormatString="#,##0.0" 
                    ExportWidth="120"
                    Width="40px" >
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Distance" 
                    Name="Distance" 
                    Caption="Distance" 
                    VisibleIndex="10" 
                    PropertiesTextEdit-DisplayFormatString="#,##0.0" 
                    Width="40px" >
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    
                <dx:GridViewDataTextColumn 
                    FieldName="SafeToMail" 
                    Caption="Mail OK ?" 
                    visible="false"
                    VisibleIndex="11" 
                    ExportWidth="80" />

                <dx:GridViewDataTextColumn 
                    FieldName="SafeToPhone" 
                    Caption="Phone OK ?" 
                    visible="false"
                    VisibleIndex="12" 
                    ExportWidth="80" />

                <dx:GridViewDataTextColumn 
                    FieldName="InCDA" 
                    Caption="In Territory? " 
                    VisibleIndex="13" 
                    ExportWidth="80"
                    PropertiesTextEdit-DisplayFormatString="#,##0.0" 
                    Width="60px" >
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                  

            </Columns>
            
        </dx:ASPxGridView>
        
        <table style="position: relative; left: 2px; top:10px; " Width="100%">
            <tr>
                <td align="left" style="width:10%"> 
                    <dx:ASPxButton 
                        ID="btnRemove" 
                        runat="server" 
                        Text="Remove" 
                        Width="100px" 
                        Enabled="false" >
                    </dx:ASPxButton>
                </td>
  
                <td align="right" style="width:10%">
                    <dx:ASPxButton 
                        ID="btnDownload1" 
                        runat="server" 
                        ClientInstanceName="btnDownload1" 
                        Text="Download List" 
                        Width="100px"  
                        style="position:relative; margin-left:5px" Enabled="false" >
                    </dx:ASPxButton>
                </td>

                <td align="right" style="width:20%">
                    <dx:ASPxComboBox
                        runat="server"
                        ID="ddlDownloadOptions" 
                        AutoPostBack="false" 
                        Width="200px" 
                        Enabled="false">
                        <Items>
                            <dx:ListEditItem   Text="All fields" Value="0"/>
                            <dx:ListEditItem   Text="Names and Addresses only" Value="1"/>
                            <dx:ListEditItem   Text="Email Addresses only" Value="2"/>
                       </Items>
                    </dx:ASPxComboBox>
                </td>

                 <td align="right" style="width:20%">
                    <dx:ASPxComboBox
                        runat="server"
                        ID="ddlDownloadFileType" 
                        AutoPostBack="false" 
                        Width="200px" 
                        Enabled="false">
                        <Items>
                            <dx:ListEditItem   Text="Excel" Value="0"/>
                            <dx:ListEditItem   Text="CSV" Value="1"/>
                         </Items>
                    </dx:ASPxComboBox>
                 </td>
                      
                 <td align="right" style="width:30%">
                     <dx:ASPxButton 
                        ID="btnExport" 
                        runat="server" 
                        ClientInstanceName="btnExport" 
                        Text="Export to Contact History" 
                        Width="150px"  
                         visible="false"
                        style="position:relative; margin-left:5px" Enabled="false" >
                    </dx:ASPxButton>
                </td>
            
                 <td align="right" style="width:10%">
                     <dx:ASPxButton 
                        ID="btnSave" 
                        runat="server" 
                        Text="Save" 
                        Width="100px" 
                        Enabled="false">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
      
        <br />

        <dx:ASPxGridView 
            ID="gridDownload1"
            KeyFieldName="Id" 
            runat="server" 
            onload="GridStyles"
            Settings-ShowHeaderFilterBlankItems="false" 
            DataSourceID="dsDownload1"
            Style="left: 155px; position: absolute; top: 255px; z-index: 100" 
            Width="100%" 
            AutoGenerateColumns="False"
            EnableRowsCache="False" 
            EnableViewState="False" 
            Visible="False">
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowHeaderFilterButton="False" />
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                <Cell HorizontalAlign="Center" />
            </Styles>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Name" FieldName="BusinessName" VisibleIndex="0" ExportWidth="250" Width="250px" />
                <dx:GridViewDataTextColumn Caption="Address" FieldName="FormattedAddress" VisibleIndex="1" ExportWidth="250" Width="250px" />
                <dx:GridViewDataTextColumn Caption="PostCode" FieldName="PostCode" VisibleIndex="2" ExportWidth="250" Width="250px" />
            </Columns>
        </dx:ASPxGridView>

        <dx:ASPxGridView 
            ID="gridDownload2"
            KeyFieldName="Id" 
            runat="server" 
            Settings-ShowHeaderFilterBlankItems="false" 
            DataSourceID="dsDownload2"
            Style="left: 17px; position: absolute; top: 226px; z-index: 100" 
            Width="100%" 
            AutoGenerateColumns="False"
            EnableRowsCache="false" 
            EnableViewState="false" 
            Visible="False">
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowHeaderFilterButton="False" />
            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                <Cell HorizontalAlign="Center" />
            </Styles>
            <Columns>
                <dx:GridViewDataTextColumn Caption="Email Address" FieldName="EmailAddress" VisibleIndex="0" ExportWidth="350" Width="250px" />
            </Columns>
        </dx:ASPxGridView>

   </div>

    <asp:SqlDataSource ID="dsCustomers" runat="server" SelectCommand="p_MM_GetWorkList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nListId" DefaultValue="0" SessionField="ListNumber" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

<%--    <asp:SqlDataSource ID="dsPostCodeList" runat="server" SelectCommand="p_Get_MapInfo" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nIncludeNTV" DefaultValue="0" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

    <asp:SqlDataSource ID="dsPostCodeList" runat="server" SelectCommand="sp_Get_NissanPostCodes" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="SelectionId" Type="String" Size="4" />
            <asp:SessionParameter Name="nOutOfTerritory_Flag" SessionField="nIncludeNTS" Type="Int32"  />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="dsLists" runat="server" SelectCommand="p_MM_Lists" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsDownload1" runat="server" SelectCommand="p_MM_GetDownloadList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nListId" SessionField="ListNumber" Type="Int32" Size="0" />
            <asp:SessionParameter Name="nType" DefaultValue="1" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsDownload2" runat="server" SelectCommand="p_MM_GetDownloadList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nListId" SessionField="ListNumber" Type="Int32" Size="0" />
            <asp:SessionParameter Name="nType" DefaultValue="2" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter" 
        runat="server" 
        GridViewID="gridCompanies">
    </dx:ASPxGridViewExporter>


</asp:Content>


