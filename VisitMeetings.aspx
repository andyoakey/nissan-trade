﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VisitMeetings.aspx.vb" Inherits="VisitMeetings" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <dxp:ASPxPopupControl ID="PopupUploadControl" ClientInstanceName="PopupUploadControl" Width="450px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="Upload Error!"
        Modal="False" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
            <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
                <asp:Label ID="lblfileUploadLabel" runat="server" Text="The file must be a PDF file, and it must be less than 500kb"></asp:Label>
            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <dxp:ASPxPopupControl ID="PopupUpload" Width="500px" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUpload" Modal="False" CloseAction="CloseButton"
        AllowDragging="True" ShowHeader="True" HeaderText="Trade Meeting File Upload - Select a PDF File (<=500kb)"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True" />

        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
                <table style="width: 100%;" >
                    <tr>
                        <td>
                            <asp:FileUpload ID='fileUpload' style="width:100%;" runat='server'  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="fileUploadButton" Style="float: right; margin: 0 auto;" runat="server" Text="Upload" />
                        </td>
                    </tr>
                </table>
            </dxp:PopupControlContentControl>
        </ContentCollection>

    </dxp:ASPxPopupControl>

    <dxp:ASPxPopupControl ID="PopupPDF" Width="800px" Height="630px" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupPDF" Modal="False" CloseAction="CloseButton"
        AllowDragging="True" ShowHeader="True" HeaderText="PDF Viewer"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True" />

        <ContentCollection>

            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">

                <div style="float: right;">
                    <asp:Button ID="deletePDF" ClientIDMode="Inherit" runat="server" Text="Remove" />
                    <asp:HiddenField ID="viewingPDF" ClientIDMode="static" Value="" runat="server" />
                </div>
                <div style="clear: both; padding: 3px 0 7px 0;"></div>

                <iframe id="iframe_pdf" style="width: 100%; height: 620px; margin: 0;"></iframe>

            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative; left:5px; padding-bottom:10px;">
            <table width="100%">
                <tr>
                    <td align="left" style="width:85%">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Meetings" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td>
                    </td>
                    <td align="right" >
                    </td>
                    <td align="right" >
                        <dxe:ASPxButton ID="btnVisits" ClientInstanceName="btnVisits" runat="server" Text="Visit Actions..." Width="100px" Style="padding-top:5px; margin-left:10px">
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridView ID="gridMeetings" ClientInstanceName="gridMeetings" runat="server" Width="100%" AutoGenerateColumns="True"
            DataSourceID="dsMeeting" Style="position: relative; left: 5px;">

            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center" />
                <Cell Font-Size="Small" HorizontalAlign="Center" />
                <LoadingPanel ImageSpacing="10px" />
            </Styles>

            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>

            <Settings
                ShowFooter="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded"
                UseFixedTableLayout="True" VerticalScrollableHeight="300"
                ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False"
                ShowFilterRowMenu="True" ShowHeaderFilterBlankItems="false" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000"
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />

            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="1" Width="75px" ExportWidth="75">
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="2" Width="245px" ExportWidth="245">
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Jan" Caption="Jan" VisibleIndex="100" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Feb" Caption="Feb" VisibleIndex="101" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Mar" Caption="Mar" VisibleIndex="102" ExportWidth="30">
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Apr" Caption="Apr" VisibleIndex="103" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_May" Caption="May" VisibleIndex="104" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Jun" Caption="Jun" VisibleIndex="105" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Jul" Caption="Jul" VisibleIndex="106" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Aug" Caption="Aug" VisibleIndex="107" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Sep" Caption="Sep" VisibleIndex="108" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Oct" Caption="Oct" VisibleIndex="109" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Nov" Caption="Nov" VisibleIndex="110" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="D_Dec" Caption="Dec" VisibleIndex="111" ExportWidth="30">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False" AllowSort="False" />
                    <CellStyle Wrap="False" Cursor="pointer">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>

        </dxwgv:ASPxGridView>

    </div>

    <asp:SqlDataSource ID="dsMeeting" runat="server" SelectCommand="p_dimtpbr" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="gridMeetings" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>











