﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RebateCampaignByPartNumber.aspx.vb" Inherits="RebateCampaignByPartNumber" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

       <div id="divTitle" style="position:relative;">
                    <dx:ASPxLabel 
                        Font-Size="Larger"
                        ID="lblPageTitle" 
                        runat="server" 
                        Text="Parts Basket Campaign By Part Number" />
       </div>
    
       <br />  

          <table style="width: 50%;">
                 <tr runat="server"  >
                <td width="150px" >
                    From
                </td>

                <td width="30px">

                </td>
                
                <td width="150px" >
                    To
                </td>

                <td width="30px">

                </td>
        
                <td width="300px">
                Round to Nearest £
                </td>
            </tr>

            <tr runat="server"  >
                <td width="150px" >
                <dx:ASPxComboBox 
                        id ="ddlFrom"
                        AutoPostBack="True"
                        runat="server">
                    </dx:ASPxComboBox>
                </td>

                <td width="30px">

                </td>
                
                <td width="150px" >
                    <dx:ASPxComboBox 
                        id ="ddlTo"
                        AutoPostBack="True"
                        runat="server">
                    </dx:ASPxComboBox>
                </td>

                <td width="30px">

                </td>
       
               <td width="300px">
                        <dx:ASPxCheckBox
                ID="chkRound"               
                runat="server"
                AutoPostBack="true"
                OnCheckedChanged="chkRound_CheckedChanged"
                Checked="false"
                Text=" ">
                        </dx:ASPxCheckBox>
                </td>
            </tr>
       </table>        

       <br />

        <dx:ASPxGridView 
            ID="gridCampaigns" 
            runat="server" 
            Width="100%" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode" 
            Style="margin-left:5px">

            <SettingsPager AllButton-Visible="true" Mode="ShowPager" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="12" />

            <Settings ShowFooter="True" ShowHeaderFilterButton="False" ShowStatusBar="Auto" ShowTitlePanel="False" ShowFilterBar="Hidden"
                ShowGroupFooter="VisibleIfExpanded" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000"
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

            <Styles>
                <Header Wrap="True" HorizontalAlign="Center" />
                <Cell HorizontalAlign="Center" />
                <Footer HorizontalAlign="Center" />
            </Styles>

            <Columns>

                <dx:GridViewDataTextColumn  Caption="Product Code" FieldName="pfc" VisibleIndex="0"  Width="6%" ExportWidth="100" Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList" />
                <dx:GridViewDataTextColumn Caption="Part Number" FieldName="partnumber" VisibleIndex="1" Width="14%" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" CellStyle-Wrap="False" ExportWidth="200" Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList"/>

                <dx:GridViewBandColumn Caption="Rebated Sales"> 
                    <Columns>
                        <dx:GridViewDataTextColumn 
                            Caption="Units Sold" 
                            FieldName="Rebated_Units" 
                            Name="Rebated_Units" 
                            VisibleIndex="2" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Invoiced Value" 
                            FieldName="Rebated_Value" 
                            Name="Rebated_Value" 
                            VisibleIndex="3" 
                            ExportWidth="100" 
                            Width="6%"/>

                        <dx:GridViewDataTextColumn 
                            Caption="Cost To Dealer" 
                            FieldName="Rebated_Cost" 
                            Name="Rebated_Cost" 
                            VisibleIndex="4" 
                            ExportWidth="100" 
                            Width="6%"/>
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Rebate Payable" 
                            FieldName="Rebate" 
                            Name="Rebate" 
                            VisibleIndex="5" 
                            ExportWidth="100" 
                            Width="6%"/>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Non-rebated Sales"> 
                    <Columns>
                        <dx:GridViewDataTextColumn 
                            Caption="Units Sold" 
                            FieldName="Missed_Units" 
                            Name="Missed_Units" 
                            VisibleIndex="6" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />

                        <dx:GridViewDataTextColumn 
                            Caption="Invoiced Value" 
                            FieldName="Missed_Value" 
                            Name="Missed_Value" 
                            VisibleIndex="7" 
                            ExportWidth="100" 
                            Width="6%"/>

                        <dx:GridViewDataTextColumn 
                            Caption="Cost To Dealer" 
                            FieldName="Missed_Cost" 
                            Name="Missed_Cost" 
                            VisibleIndex="8" 
                            ExportWidth="100" 
                            Width="6%" />
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Rebate Missed" 
                            FieldName="Missed_Rebate" 
                            Name="Missed_Rebate" 
                            VisibleIndex="9" 
                            ExportWidth="100" 
                            Width="6%"/>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Total Campaign Sales" ToolTip="Compares Sales of Rebate Items With 12 Months Ago"> 
                    <Columns>
                        <dx:GridViewBandColumn Caption="Units Sold" > 
                            <Columns>
                                <dx:GridViewDataTextColumn 
                            Caption="This Year" 
                            FieldName="Units_This" 
                            Name="Units_This" 
                            VisibleIndex="14" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                                    
                                <dx:GridViewDataTextColumn 
                            Caption="Last Year" 
                            FieldName="Units_Last" 
                            Name="Units_Last" 
                            VisibleIndex="15" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                          
                                <dx:GridViewDataTextColumn 
                            Caption="Uplift" 
                            FieldName="Units_Diff" 
                            Name="Units_Diff" 
                            VisibleIndex="16" 
                            ExportWidth="100"
                            Width="5%" 
                            PropertiesTextEdit-DisplayFormatString="#,##0" />

                                <dx:GridViewDataTextColumn 
                            Caption="% Growth" 
                            FieldName="Units_DiffPerc" 
                            Name="Units_DiffPerc" 
                            VisibleIndex="17" 
                            ExportWidth="100" 
                            Width="5%"
                            PropertiesTextEdit-DisplayFormatString="0.0%"/>
                           </Columns>
                       </dx:GridViewBandColumn>
                    
                        <dx:GridViewBandColumn Caption="Cost To Dealer" > 
                            <Columns>
                                <dx:GridViewDataTextColumn 
                            Caption="This Year" 
                            FieldName="Cost_This" 
                            Name="Cost_This" 
                            VisibleIndex="10" 
                            ExportWidth="100"
                            Width="6%" />

                                <dx:GridViewDataTextColumn 
                            Caption="Last Year" 
                            FieldName="Cost_Last" 
                            Name="Cost_Last" 
                            VisibleIndex="11" 
                            ExportWidth="100" 
                            Width="6%"/>
                          
                                <dx:GridViewDataTextColumn 
                            Caption="Uplift" 
                            FieldName="Cost_Diff" 
                            Name="Cost_Diff" 
                            VisibleIndex="12" 
                            ExportWidth="100" 
                            Width="5%"/>

                                <dx:GridViewDataTextColumn 
                            Caption="% Growth" 
                            FieldName="Cost_DiffPerc" 
                            Name="Cost_DiffPerc" 
                            VisibleIndex="13" 
                            ExportWidth="100" 
                            Width="5%"
                            PropertiesTextEdit-DisplayFormatString="0.0%"/>
                           </Columns>
                       </dx:GridViewBandColumn>
                    </Columns>
             </dx:GridViewBandColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Rebated_Units" ShowInColumn="Rebated_Units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebated_Value" ShowInColumn="Rebated_Value" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebated_Cost" ShowInColumn="Rebated_Cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebate" ShowInColumn="Rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Missed_Units" ShowInColumn="Missed_Units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Missed_Value" ShowInColumn="Missed_Value" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0"   FieldName="Missed_Cost" ShowInColumn="Missed_Cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Missed_Rebate" ShowInColumn="Missed_Rebate" SummaryType="Sum" />

                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_This" ShowInColumn="Cost_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_Last" ShowInColumn="Cost_Last" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_Diff" ShowInColumn="Cost_Diff" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Cost_DiffPerc" SummaryType="Custom" ShowInColumn="Cost_DiffPerc" />

                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_This" ShowInColumn="Units_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_Last" ShowInColumn="Units_Last" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_Diff" ShowInColumn="Units_Diff" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Units_DiffPerc" SummaryType="Custom" ShowInColumn="Units_DiffPerc" />
            </TotalSummary>

            <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="False" ExportMode="Expanded" />

		<%-- Removed JORA NMGBM-216 - replaced by cost fields	
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Value_This" ShowInColumn="Value_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Value_Last" ShowInColumn="Value_Last" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Value_Diff" ShowInColumn="Value_Diff" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Value_DiffPerc" SummaryType="Custom" ShowInColumn="Value_DiffPerc" />
		--%>


        </dx:ASPxGridView>
    
    </div>
       

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False" OnRenderBrick="ASPxGridViewExporter1_RenderBrick">
    </dx:ASPxGridViewExporter>

</asp:Content>




