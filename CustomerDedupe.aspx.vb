﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports DevExpress.Web
Imports DevExpress.Web.Rendering
Imports System.Collections.Generic

Partial Class CustomerDedupe

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Customer De-dupe"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer De-dupe", "De-duplicating Customer records")
        End If

    End Sub

    Protected Sub dsCompanies_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCompanies.Init
        dsCompanies.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ViewCompanies.aspx")
    End Sub

    Protected Sub btnDup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDup.Click

        Dim selectedValues As List(Of Object)
        Dim i As Integer
        Dim fieldNames As List(Of String) = New List(Of String)()
        Dim sStr As String = ""
        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        lblErr1.Text = ""
        Session("DeDupeTo") = Nothing
        fieldNames.Add("Id")
        selectedValues = gridCompanies.GetSelectedFieldValues(fieldNames.ToArray())
        For i = 0 To selectedValues.Count - 1
            Session("DeDupeTo") = selectedValues.Item(i).ToString
        Next

        If selectedValues.Count = 1 Then
            If Not Session("DeDupeTo") Is Nothing Then
                If OkToDeDupe() Then
                    If FishBowlsOk() Then
                        htIn.Add("@nCustomerId", Session("DeDupeTo"))
                        htIn.Add("@sDeDupeIDs", Session("DeDupeIDs"))
                        htIn.Add("@nUserId", Session("UserId"))
                        nRes = da.Update(sErr, "p_CustomerDedupe", htIn)
                        If sErr.Length <> 0 Then
                            Session("ErrorToDisplay") = sErr
                            Response.Redirect("dbError.aspx")
                        End If
                        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customers de-duped!", "")
                        Response.Redirect("ViewCompanies.aspx")
                    Else
                        lblErr1.Text = "These customers cannot be de-duped due to a fish bowl violation!"
                    End If
                Else
                    lblErr1.Text = "When de-duping with Yell records, a Yell record must be chosen as the Primary."
                End If
            Else
                lblErr1.Text = "Select something to de-dupe."
            End If
        ElseIf selectedValues.Count > 1 Then
            lblErr1.Text = "You must select one record as the Primary."
        Else
            lblErr1.Text = "Select something to de-dupe."
        End If

    End Sub

    Protected Sub gridCompanies_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompanies.HtmlDataCellPrepared

        If e.GetValue("BusinessURN") <> "" Then
            e.Cell.Font.Bold = True
        Else
            e.Cell.Font.Bold = False
        End If

    End Sub

    Private Function OkToDeDupe() As Boolean

        Dim sSQL As String
        Dim nCount As Long

        OkToDeDupe = False
        sSQL = "SELECT COUNT(*) FROM Customer WHERE Id IN (" & Session("DeDupeIDs") & ") AND BusinessURN IS NOT NULL"
        nCount = GetDataLong(sSQL)
        If nCount > 0 Then
            sSQL = "SELECT COUNT(*) FROM Customer WHERE Id = " & Session("DeDupeTo") & " AND BusinessURN IS NOT NULL"
            nCount = GetDataLong(sSQL)
            If nCount > 0 Then
                OkToDeDupe = True
            End If
        Else
            OkToDeDupe = True
        End If

    End Function

    Private Function FishBowlsOk() As Boolean

        Dim sSQL As String
        Dim nCount1 As Long
        Dim nCount2 As Long

        FishBowlsOk = False

        sSQL = "SELECT COUNT(*) FROM Customer WHERE Id IN (" & Session("DeDupeIDs") & ") AND BusinessTypeId IN (1,6,7)"
        nCount1 = GetDataLong(sSQL)

        sSQL = "SELECT COUNT(*) FROM Customer WHERE Id IN (" & Session("DeDupeIDs") & ") AND BusinessTypeId NOT IN (1,6,7)"
        nCount2 = GetDataLong(sSQL)

        If nCount1 > 0 And nCount2 > 0 Then
            FishBowlsOk = False
        Else
            FishBowlsOk = True
        End If

    End Function
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
