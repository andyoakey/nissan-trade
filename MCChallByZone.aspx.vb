﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class MCChall2011ByZone

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Toyota PARTS website - Campaign Reporting"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), Me.Title, Me.Title)
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

    End Sub

    Protected Sub dsCampaigns1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns1.Init
        dsCampaigns1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCampaigns2_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns2.Init
        dsCampaigns2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        If Request.QueryString(0) = "0" Then
            gridCampaigns.DataSourceID = "dsCampaigns1"
            gridCampaigns.Columns(1).Caption = "Zone"
            gridCampaigns.Columns(1).Width = 100
            ddlLeague.Items.Add("By Zone")
            ddlLeague.Items.Add("By TPSM")
        Else
            gridCampaigns.DataSourceID = "dsCampaigns2"
            gridCampaigns.Columns(1).Caption = "TPSM"
            gridCampaigns.Columns(1).Width = 200
            ddlLeague.Items.Add("By TPSM")
            ddlLeague.Items.Add("By Zone")
        End If
        ddlLeague.Items.Add("All")
        ddlLeague.Items.Add("By League")

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.FileName = "TradeChallenge_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared
        If e.GetValue("Ranking") <= 1 Then
            e.Cell.Font.Bold = True
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 4 Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub ddlLeague_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeague.SelectedIndexChanged
        If ddlLeague.SelectedIndex = 1 Then
            If Request.QueryString(0) = 0 Then
                Response.Redirect("MCChallByZone.aspx?1")
            Else
                Response.Redirect("MCChallByZone.aspx?0")
            End If
        End If
        If ddlLeague.SelectedIndex = 2 Then
            Response.Redirect("MCChallAll.aspx")
        End If
        If ddlLeague.SelectedIndex = 3 Then
            Response.Redirect("MCChall.aspx")
        End If
    End Sub

End Class
