﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCompaniesRealloc.aspx.vb" Inherits="ViewCompaniesRealloc" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

        <form id="form1" runat="server">

        <dxwgv:ASPxGridView ID="gridNewCustomers" runat="server" AutoGenerateColumns="False" Width="99%" style="margin-bottom:10px" KeyFieldName="Id">
       
            <SettingsPager Visible="false" />
        
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False" ShowFooter="False" HorizontalScrollBarMode="Hidden" 
                VerticalScrollableHeight="250"  VerticalScrollBarMode="Auto"  />
            
            <SettingsText EmptyDataRow="No alternative customers found" />

            <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" AllowSelectByRowClick="true" AllowSelectSingleRowOnly="True"
                AllowDragDrop="False" AllowGroup="False" />

            <Styles>
                <Header HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" />
                <Cell HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" />
                <Footer HorizontalAlign="Center" Wrap="True" Font-Size="X-Small" />
            </Styles>           
            
            <Columns>
                
                 <dxwgv:GridViewCommandColumn  Caption="Select" ShowSelectCheckbox="True" VisibleIndex="0" Width="65px">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                 </dxwgv:GridViewCommandColumn>

                <dxwgv:GridViewDataTextColumn Caption="Customer Id" FieldName="Id" VisibleIndex="1" Width="12%" />
                <dxwgv:GridViewDataTextColumn Caption="Experian URN" FieldName="BusinessURN" VisibleIndex="2" Width="12%" />
                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="BusinessName" VisibleIndex="3" CellStyle-Wrap="False"  />
                <dxwgv:GridViewDataTextColumn Caption="Address" FieldName="Address1" VisibleIndex="4" CellStyle-Wrap="False" />
                <dxwgv:GridViewDataTextColumn Caption="Postcode" FieldName="PostCode" VisibleIndex="5" Width="10%" />
                <dxwgv:GridViewDataTextColumn Caption="Telephone Number" FieldName="TelephoneNumber" VisibleIndex="6" Width="12%" />
                
            </Columns>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

        </dxwgv:ASPxGridView>

        <table style="width:99%" runat="server" >
            <tr id="SelChoice"  runat="server" >
                <td align="left">
                    <dxe:ASPxButton ID="btnSelectCustomer" ClientInstanceName="btnSelectCustomer" runat="server" Text="Use Selected Customer" Width="150px" ImagePosition="Top" >
                    </dxe:ASPxButton>
                </td>
                <td align="right">
                    <dxe:ASPxButton ID="btnNewCustomer" ClientInstanceName="btnNewCustomer" runat="server" Text="Create New Customer" Width="150px" ImagePosition="Top">
                    </dxe:ASPxButton>
                </td>
            </tr>
            <tr id="ConfChoice" runat="server" visible="false"  >
                <td align="right">
                </td>
                <td align="right">
                    <asp:Label ID="lblYesNo" runat="server" Text="Are you sure (this will take some time)?" Font-Size="13px" Font-Names="Calibri,Verdana"> </asp:Label>
                    <dxe:ASPxButton ID="btnYes" ClientInstanceName="btnYes" runat="server" Text="Yes" Width="50px" ImagePosition="Top">
                    </dxe:ASPxButton>
                    <dxe:ASPxButton ID="btnNo" ClientInstanceName="btnNo" runat="server" Text="No" Width="50px" ImagePosition="Top">
                    </dxe:ASPxButton>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
