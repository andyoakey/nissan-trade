﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="dbError.aspx.vb" Inherits="dbError" %>

<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dxm" %>

<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="div1" style="position:relative; left:5px">
            <asp:Label ID="Label1" runat="server" Text="Nissan Trade Site" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <table style="width: 100%; height:25px">
            <tr>
                <td>
                    <dxe:ASPxLabel ID="lblTitle2" runat="server" 
                        Text="Unfortunately, your request could not be processed at this time. Please try again later." 
                         >
                    </dxe:ASPxLabel>
                </td>
            </tr>
        </table>
        
        <table style="width: 100%; height:75px">
            <tr>
                <td valign="top">
                    <dxe:ASPxLabel ID="lblErrorMessage" runat="server" Text="If the problem persists, please contact the helpdesk.">
                    </dxe:ASPxLabel>
                </td>
            </tr>
        </table>

        <table style="width: 100%; height:75px">
            <tr>
                <td valign="top">
                    <dxe:ASPxLabel ID="lblErrorModule" runat="server">
                    </dxe:ASPxLabel>
                </td>
            </tr>
        </table>
    
    </div>
    
</asp:Content>

