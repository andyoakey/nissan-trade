﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting


Partial Class UserList

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - User Details"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "User List", "Viewing User List")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsUsers_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsUsers.Init
        dsUsers.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridUsers_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridUsers.HtmlDataCellPrepared
        If e.GetValue("Deleted") = 1 Then
            e.Cell.Font.Italic = True
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 2 Then
            If Session("UserEmailAddress").ToString().Contains("@qubedata.") Then
                e.Cell.ToolTip = e.GetValue("Password")
            Else
                e.Cell.ToolTip = ""
            End If
        End If
    End Sub

    Protected Sub gridUsers_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gridUsers.RowUpdating

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@nUserId", e.Keys(0))
        htIn.Add("@sFirstName", Convert.ToString(e.NewValues("FirstName")))
        htIn.Add("@sSurname", "")
        htIn.Add("@sEmailAddress", Convert.ToString(e.NewValues("EmailAddress")))
        htIn.Add("@nDeleted", Convert.ToString(e.NewValues("Deleted")))

        nRes = da.Update(sErr, "p_Update_UserDetails", htIn)

        e.Cancel = True
        If sErr.Length = 0 Then
            gridUsers.CancelEdit()
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "User List", "Edit User Details for User Id : " & e.Keys(0))
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Sub

    Protected Sub gridUsers_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridUsers_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridUsers.CustomColumnDisplayText
        If e.Column.Index = 9 Then
            If e.GetFieldValue("Deleted") = 1 Then
                e.DisplayText = ""
            End If
        End If
    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "UserListing"
        Dim nCols As Integer
        Dim nRows As Integer

        nCols = gridUsers.Columns.Count - 1
        nRows = gridUsers.VisibleRowCount

        gridUsers.Columns(8).Visible = False
        ASPxGridViewExporter.FileName = sFileName
        ASPxGridViewExporter.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        gridUsers.Columns(8).Visible = True

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub


End Class

