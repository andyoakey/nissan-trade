﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DocLibrary.aspx.vb" Inherits="DocLibrary" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxdv" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>

<%--<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>--%>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>


    

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Document Library" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <dxdv:ASPxDataView ID="ASPxDataView1" runat="server" DataSourceID="dsDocLib"
            RowPerPage="2"  
            PagerPanelSpacing="0px" 
            >
            <LoadingPanelImage >
            </LoadingPanelImage>
            <ItemTemplate>
                <div>
                    <dxe:aspxhyperlink imageurl='<%# Eval("FileTypeImage") %>' Id="ASPxHyperLink1" runat="server"
                        target="_blank" text='<%# Eval("DocumentName") %>' navigateurl='<%# Eval("DocLink") %>'>
                    </dxe:aspxhyperlink>
                    <br>
                    <b>
                        <dxe:aspxhyperlink forecolor="Crimson" id="ASPxHyperLink2" runat="server" target="_blank"
                            text='<%# Eval("DocumentName") %>' navigateurl='<%# Eval("DocLink") %>'>
                        </dxe:aspxhyperlink>
                    </b>
                    <br>
                    <br>
                    <b>Description:</b>
                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("DocumentDesc")%>'></asp:Label>
                    <br>
                    <br>
                    <b>Date Added:</b>
                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Posted")%>'></asp:Label>
                    <br>
                    <br>
                    <b>Type:</b>
                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("DocumentCategory")%>'></asp:Label>
                </div>
            </ItemTemplate>
            <PagerSettings Position="Bottom">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <PrevPageButton>
                </PrevPageButton>
                <NextPageButton>
                </NextPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </PagerSettings>
        </dxdv:ASPxDataView>
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsDocLib" runat="server" SelectCommand="p_DocLib" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>


