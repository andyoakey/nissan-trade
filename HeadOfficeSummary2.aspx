 <%@ Page AutoEventWireup="false" CodeFile="HeadOfficeSummary2.aspx.vb" Inherits="HeadOfficeSummary2"    Language="VB" MasterPageFile="~/MasterPage.master" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
      <div style="position: relative; font-family: Calibri; text-align: left;" >


     <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Sales Drill Down By Month" />
        </div>

    <br />
          
        <table id="trSelectionStuff" style="position: relative;" Width="976px">
    
            <tr runat="server" >

                <td style="width:20%;" align="left" >
                   <dx:ASPxLabel 
                    ID="ASPxLabel1" 
                    runat ="server" 
                    Text="Financial Year" />
                </td>

                <td style="width:20%;" align="left" >
                        <dx:ASPxLabel 
                            id="lblProductGroup"
                            runat="server" 
                            Text="Service Description">
                        </dx:ASPxLabel>
                </td>
         
                <td style="width:20%;" align="left" >
                        <dx:ASPxLabel 
                            id="ASPxLabel2"
                            runat="server" 
                            Text="Trade Analysis Group">
                        </dx:ASPxLabel>
                </td>

                <td style="width:20%;" align="left" >
                        <dx:ASPxLabel 
                            id="ASPxLabel3"
                            runat="server" 
                            Text="Required Viewing Level">
                        </dx:ASPxLabel>
                </td>


            </tr>

            <tr>
                <td align="left" >
                        <dx:ASPxComboBox
        ID ="cboFY"
        runat="server"
        AutoPostBack="true"
        SelectedIndex="0">
        <Items>
	    <dx:ListEditItem Text ="2018/19"  Value="2018/19" Selected="true"/>
	    <dx:ListEditItem Text ="2017/18"  Value="2017/18" />
  	    <dx:ListEditItem Text ="2016/17"  Value="2016/17" />
            <dx:ListEditItem Text ="2015/16"  Value="2015/16" />
            <dx:ListEditItem Text ="2014/15"  Value="2014/15" />
            <dx:ListEditItem Text ="2013/14"  Value="2013/14" />
            <dx:ListEditItem Text ="2012/13"  Value="2012/13" />
        </Items>
    </dx:ASPxComboBox>
                </td>
            
                <td align="left" >
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlProductGroup"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All" Value="ALL" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="ACC"/>
                            <dx:ListEditItem Text="Damage" Value="DAM"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="EXM"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="MEC"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="ROM"/>
                            <dx:ListEditItem Text="Additional Product" Value="OTH"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td align="left" >
                    <dx:ASPxComboBox
                        ID="ddlTA"
                        ValueField="TradeAnalysis"
                        TextField="TradeAnalysis"
                        runat="server" 
                        DataSourceID="dsTradeAnalysis"
                        SelectedIndex="0"
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                </td>


                <td style="width:20%;" align="left" >
                    <dx:ASPxComboBox
                        id="cboViewLevel"
                        runat="server"
                        SelectedIndex="2"
                         ToolTip="Select the the lowest required level of information. The selected level always includes parents."
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="National" Value ="0"/>
                            <dx:ListEditItem Text="Regional " Value ="1"/>
                            <dx:ListEditItem Text="Zone " Value ="2" Selected="true"/>
                            <dx:ListEditItem Text="Dealer" Value ="3"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>


        
            </tr>

        </table> 

    <br />

 <dxwtl:ASPxTreeList 
        ID="tlDealerDrillDown" 
        runat="server" 
        AutoGenerateColumns="False"  
        DataSourceID="dsDealerDrillDown" 
        onload ="TreeListStyles"   
        CssClass="grid_styles"
        Settings-GridLines="None"
        KeyFieldName="ID" 
        Width = "100%"
        Styles-Header-Wrap="False"
        ParentFieldName="parent_ID" >
       <Settings SuppressOuterGridLines="False"  GridLines="Both" />
       <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
        <Columns>
  
            <dxwtl:TreeListDataColumn 
                FieldName="treelevel" 
                Visible="False" 
                VisibleIndex="0">
              </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                Caption="Level" 
                FieldName="nodename" 
                ExportWidth="350"
                width="8%"
                VisibleIndex="0"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left">
        
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="apr_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="1" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="may_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="2" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="jun_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="3" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="jul_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="4" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="aug_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="4" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="sep_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="5" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="oct_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="6" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="nov_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="7" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="dec_sales" 
                ExportWidth="150"
                Width="7%"
                VisibleIndex="8" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="jan_sales" 
                Width="7%"
                ExportWidth="150"
                VisibleIndex="9" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="feb_sales" 
                Width="7%"
                ExportWidth="150"
                VisibleIndex="10" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="mar_sales" 
                Width="7%"
                ExportWidth="150"
                VisibleIndex="11" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      
            </dxwtl:TreeListDataColumn>

            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="tot_sales" 
                Width="4%"
                ExportWidth="150"
                VisibleIndex="12" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
      

            </dxwtl:TreeListDataColumn>

        </Columns>
    </dxwtl:ASPxTreeList>

    <asp:SqlDataSource 
        ID="dsDealerDrillDown" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_FullDrillDownByMonth" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="sLevel4_Code" SessionField="Level4_Code" Type="String" Size="3"  />
            <asp:SessionParameter Name="sTradeAnalysis" SessionField="TA" Type="String" Size="50"  />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsTradeAnalysis" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeAnalysisProducts" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>


    <dxwtl:ASPxTreeListExporter 
        ID="ASPxTreeListExporter1" 
        runat="server" 
        TreeListID="tlDealerDrillDown">
    </dxwtl:ASPxTreeListExporter>
        
       </div>

</asp:Content>

