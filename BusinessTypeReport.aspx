﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="BusinessTypeReport.aspx.vb" Inherits="BusinessTypeReport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="971px">
                <tr>
                    <td align="left" style="width:75%">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Business Type Report" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right">
                    </td>
                </tr>
            </table>
        </div>
        <br />

        <table id="tabCharts" width="976px">
            <tr>
                <td style="width:60%">
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="500px" 
                        Width="580px" PaletteName="Office" BackColor="Aqua">
                    </dxchartsui:WebChartControl>
                </td>
                <td valign="top" align="right">
                    <dxwgv:ASPxGridView ID="gridData" runat="server" AutoGenerateColumns="False"  Width="380px"
                        >
                        <Styles  >
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <Settings ShowFooter="True" />
                        <SettingsPager Visible="False">
                        </SettingsPager>
                        <Images>
                            <CollapsedButton Height="12px" 
                                Width="11px" />
                            <ExpandedButton Height="12px" 
                                Width="11px" />
                            <DetailCollapsedButton Height="12px" 
                                Width="11px" />
                            <DetailExpandedButton Height="12px" 
                                Width="11px" />
                            <FilterRowButton Height="13px" Width="13px" />
                        </Images>
                        <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="Business Type" FieldName="Description" Width="125px" 
                                VisibleIndex="0">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Count" FieldName="BTValue" VisibleIndex="1" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="#,##0">
                                </PropertiesTextEdit>
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="Center">
                                </FooterCellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem FieldName="BTValue" DisplayFormat="Total Customers: {0}" SummaryType="Sum" ShowInColumn="Count" />
                        </TotalSummary>
                    </dxwgv:ASPxGridView>
                    <br />
                </td>
            </tr>
        </table>       
        <br />
        
    </div>
    
</asp:Content>



