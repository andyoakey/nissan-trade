﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewPFCsByCustomer.aspx.vb" Inherits="ViewPFCsByCustomer" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxwpg" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxpgw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="PFC Breakdown" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwpg:ASPxPivotGrid ID="ASPxPivotGrid1" runat="server" DataSourceID="dsViewSalesLevel1" 
            OnCustomCellDisplayText="ASPXPivotResults_CustomCellDisplayText" Width="100%" OptionsView-ShowHorizontalScrollBar="true" >
            <Fields>
                <dxwpg:PivotGridField ID="fieldPFC" 
                    AllowedAreas="ColumnArea" AreaIndex="0" 
                    FieldName="PFC" Area="ColumnArea" ExpandedInFieldsGroup="False"
                    Options-AllowFilter="True" Caption="Desc Code">
                    <HeaderStyle Wrap="True" />
                </dxwpg:PivotGridField>
                <dxwpg:PivotGridField ID="fieldQuantity" AllowedAreas="DataArea" AreaIndex="0" 
                    FieldName="Units" Area="DataArea" CellFormat-FormatString="#,##0" CellStyle-HorizontalAlign="Center">
                </dxwpg:PivotGridField>
                <dxwpg:PivotGridField ID="fieldSaleValue" AllowedAreas="DataArea" AreaIndex="1" 
                    FieldName="Sale Value" Area="DataArea" CellFormat-FormatString="£#,##0" CellStyle-HorizontalAlign="Center">
                </dxwpg:PivotGridField>
                <dxwpg:PivotGridField ID="fieldCustomerName" AllowedAreas="RowArea, FilterArea" 
                    AreaIndex="0" FieldName="CustomerName" Area="RowArea" Caption="Customer">
                </dxwpg:PivotGridField>
            </Fields>
            <Styles >
                <CustomizationFieldsHeaderStyle>
                    <Paddings PaddingLeft="12px" PaddingRight="6px" />
                </CustomizationFieldsHeaderStyle>
            </Styles>
            <Images >
                <FieldValueCollapsed Height="12px" 
                    Width="11px" />
                <FieldValueExpanded Height="12px" 
                    Width="11px" />
                <HeaderSortDown Height="8px" 
                    Width="7px" />
                <HeaderSortUp Height="8px" 
                     Width="7px" />
                <SortByColumn Height="7px" 
                    Width="11px" />
            </Images>
            <OptionsPager RowsPerPage="100">
                <AllButton Visible="True">
                </AllButton>
            </OptionsPager>
        </dxwpg:ASPxPivotGrid>
        <br />
        <br />
    </div>
    
    <asp:SqlDataSource ID="dsViewSalesLevel1" runat="server" SelectCommand="p_ProductSummary_Level1_Customer" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nType" SessionField="AnalysisType" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="sGrouping" SessionField="Grouping" Type="String" Size="50" />
            <asp:SessionParameter DefaultValue="" Name="nTradeClubType" SessionField="TradeClubType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxpgw:ASPxPivotGridExporter ID="ASPxPivotGridExporter1"  ASPxPivotGridID="ASPxPivotGrid1" runat="server">
    </dxpgw:ASPxPivotGridExporter>

</asp:Content>

