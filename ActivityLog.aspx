﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ActivityLog.aspx.vb" Inherits="ActivityLog" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Activity Log" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <table id="trSelectionStuff" width="976px">
            <tr id="Tr3" runat="server" style="height: 30px;">
                <td style="font-size: small; width:10%" align="left">
                    From
                </td>
                <td style="font-size: small;" align="left">
                    <asp:DropDownList ID="ddlFrom" runat="server" AutoPostBack="True" Width="180px"  >
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="right">
                </td>
            </tr>
        </table>
        <br />
        
        <dxwgv:ASPxGridView ID="gridActivity" runat="server" DataSourceID="dsActivityLog"
            AutoGenerateColumns="False" Width="976px"            >
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <SettingsPager PageSize="32">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </SettingsPager>
            <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" ShowGroupedColumns="True"
                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="True" />
            <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>
            <Images>
                <CollapsedButton Height="12px" 
                     
                    Width="11px" />
                <ExpandedButton Height="12px" 
                     
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                     
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                     
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="UserName" ReadOnly="True"
                    VisibleIndex="0" Width="200px">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Date / Time" FieldName="TimeStamp" VisibleIndex="1"
                    Width="175px">
                    <HeaderStyle Wrap="True" />
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Level" FieldName="SelectionLevel" ReadOnly="True"
                    VisibleIndex="2" Width="75px">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Detail" FieldName="SelectionId" ReadOnly="True"
                    VisibleIndex="3" Width="75px">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Page" FieldName="Screen" ReadOnly="True"
                    VisibleIndex="4" Width="200px">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Notes" FieldName="Notes" ReadOnly="True"
                    VisibleIndex="5">
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
        </dxwgv:ASPxGridView>
        <br />
    </div>
    
    <asp:SqlDataSource ID="dsActivityLog" runat="server" SelectCommand="p_ActivityLog_Sel" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="MonthFrom" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridActivity"
        PreserveGroupRowStates="True">
    </dxwgv:ASPxGridViewExporter>
    
</asp:Content>

