﻿Imports DevExpress.Web.ASPxTreeList

Partial Class PumpIn
    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Me.IsPostBack = False Then
            Session("SalesType") = 1
            Session("nDataPeriod") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'") + 1
        End If
        Me.memPostCodeList.Text = Session("PostCodeList2")

        Dim master_label As DevExpress.Web.ASPxLabel
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_label = CType(Master.FindControl("lblDealerOnly"), DevExpress.Web.ASPxLabel)
        master_btnExcel.Visible = True

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub
    '    Protected Sub ExportToExcel()
    'Dim sFileName As String = "PostCodeAllocation"
    'Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
    'Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
    '   composite.Links.AddRange(New Object() {linkResults1})
    '  composite.CreateDocument()
    'Dim stream As New System.IO.MemoryStream()
    '   composite.PrintingSystem.ExporttoXlsx(stream)
    '    gu.WriteToResponse(Page, sFileName, True, "xlsx", stream)
    ' End Sub


    Protected Sub btnRunQuery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunQuery.Click
        Call RunThisQuery()
    End Sub

    Sub RunThisQuery()

        If isEmpty(Me.memPostCodeList.Text) = True Then Exit Sub

        'Session("SalesType") = Me.cboSalesType.Value
        Session("SalesType") = 1
        Me.tabPageControl.ActiveTabIndex = 1
        With Me.gridPumpIn
            .DataBind()
            .Visible = True
        End With

    End Sub
    'Protected Sub rblSalesType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSalesType.SelectedIndexChanged
    '    Call RunThisQuery()
    'End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = ""
        '   Select Case cboSalesType.SelectedItem.Value
        '  Case 1
        sFileName = "PumpIn_TotalSales_Last12Months"
        '     Case 2
        'sFileName = "PumpIn_KeySales_Last12Months"
        'Case 3
        'sFileName = "PumpIn_MECSales_Last12Months"
        'Case 3
        'sFileName = "PumpIn_NonDamageSales_Last12Months"
        'End Select

        ASPxPivotGridExporter1.ExportXlsxToResponse(sFileName)
    End Sub

    Protected Sub tlPostCodes_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlPostCodes.SelectionChanged
        Dim treeList As DevExpress.Web.ASPxTreeList.ASPxTreeList = CType(sender, DevExpress.Web.ASPxTreeList.ASPxTreeList)
        Dim iterator As DevExpress.Web.ASPxTreeList.TreeListNodeIterator = treeList.CreateNodeIterator()
        Dim sThisPostCodeList As String = ""
        Dim sThisPostCodeList2 As String = ""
        Do While iterator.Current IsNot Nothing
            Dim node As TreeListNode = iterator.Current
            If node.Selected And node.Level = 3 Then
                If isEmpty(sThisPostCodeList) = False Then
                    '''''''''''''''''''  First one is for the SP 
                    sThisPostCodeList = sThisPostCodeList & "," & Trim(node.DataItem(5).ToString)
                    ''''''''''''''''''' Second one is for the Screen List
                    sThisPostCodeList2 = sThisPostCodeList2 & vbCrLf & Trim(node.DataItem(3).ToString)
                Else
                    sThisPostCodeList = Trim(node.DataItem(5).ToString)
                    sThisPostCodeList2 = Trim(node.DataItem(3).ToString)
                End If

            End If
            iterator.GetNext()
        Loop
        Session("PostCodeList") = sThisPostCodeList
        Session("PostCodeList2") = sThisPostCodeList2
    End Sub

    Protected Sub ASPxCallback1_Callback(ByVal source As Object, ByVal e As DevExpress.Web.CallbackEventArgs) Handles ASPxCallback1.Callback
        ASPxCallback1.JSProperties("cppostcodelist") = Session("PostCodeList2")
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub TreeListStyles(sender As Object, e As EventArgs)
        Call TreeList_Styles(sender)
    End Sub

    Protected Sub PivotGridStyles(sender As Object, e As EventArgs)
        Call PivotGrid_Styles(sender)
    End Sub

End Class
