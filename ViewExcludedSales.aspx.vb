﻿Imports System.Data
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class ViewExcludedSales

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - View Excluded Sales"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Excluded Sales", "Viewing Excluded Sales")
        End If

    End Sub

    Protected Sub dsExcludedSalesSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsExcludedSalesSummary.Init
        dsExcludedSalesSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsExcludedSales_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsExcludedSales.Init
        dsExcludedSales.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsExcludedInvoices_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsExcludedInvoices.Init
        dsExcludedInvoices.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridExcludedInvoices_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("CustomerDealerId") = sKey
    End Sub

    Protected Sub gridExcludedInvoices_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Or e.Column.FieldName = "ExcludedPerc" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Session("ReturnProgram") = "ViewExcludedSales.aspx"

    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.FileName = "ExcludedSalesList"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStylesSearchOff(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim dealerCode = Left(templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString(), 4)

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub

End Class
