<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ServiceKitVA.aspx.vb" Inherits="ServiceKitVA" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
        
        <div id="divTitle" style="position: relative;">
            <table width="100%">
                <tr>
                    <td>
                       <dx:ASPxLabel 
                Font-Size="Larger"
                 ID="lblPageTitle" 
        runat ="server" 
        Text="VA Service Kit Year on Year Performance" />
                        </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <dx:ASPxComboBox ID="ddlYear" runat="server" AutoPostBack="True"  SelectedIndex="0" visible="false">
                            <Items>
                                <dx:ListEditItem Text ="2019 vs 2018" Value =2018 Selected="true" />
                                <dx:ListEditItem Text ="2018 vs 2017" Value =2018  />
                                <dx:ListEditItem Text ="2017 vs 2016" Value =2017 />
                            </Items>
                            </dx:ASPxComboBox>
                    </td>
                </tr>
            </table>
        </div>        
        <br />
        
        <dx:ASPxPageControl ID="ASPxPageControl1" ClientInstanceName="PageControl" runat="server" ActiveTabIndex="0" Width="100%" EnableHierarchyRecreation="true"  CssClass="page_tabs" >

            <TabPages>
            
                <%------------------------------------------------------------------------------------------
                ' TAB 1
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Name="Chart" Text="Chart">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl2" runat="server">
                        

                            <dxchartsui:WebChartControl 
                                CssClass="graph_respond" 
                                ID="WebChartControl1" 
                                runat="server" 
                                AppearanceName="Origin" 			
                                Height="350px" 
                                PaletteName="Origin" 
                                Width="1500px"
                                ClientInstanceName="WebChartControl1"> 
                            </dxchartsui:WebChartControl>
                            
                            <br />
                            <br />
                            
                            <dx:ASPxGridView ID="gridTotals" runat="server" AutoGenerateColumns="False" Width="100%"  >
                                
                                <SettingsPager Mode="ShowAllRecords" Visible="False">
                                </SettingsPager>
                                
                                <Settings ShowGroupButtons="False" ShowStatusBar="Hidden" ShowTitlePanel="False" ShowFilterBar="Hidden" ShowFilterRowMenu="False" ShowHeaderFilterButton="false"  />
                                
                                <Styles>
                                    <Cell HorizontalAlign="Center"/>
                                    <Header HorizontalAlign="Center"/>

                                </Styles>
                                
                                <Columns>
                                
                                    <dx:GridViewDataTextColumn Caption="Year" FieldName="Year" VisibleIndex="0">
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="January" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}01" VisibleIndex="1">
                                        <PropertiesHyperLinkEdit Text="Jan" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}01" TextField="Month_1">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="February" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}02" VisibleIndex="2">
                                        <PropertiesHyperLinkEdit Text="Feb" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}02" TextField="Month_2">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="March" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}03" VisibleIndex="3">
                                        <PropertiesHyperLinkEdit Text="Mar" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}03" TextField="Month_3">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="April" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}04" VisibleIndex="4">
                                        <PropertiesHyperLinkEdit Text="Apr" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}04" TextField="Month_4">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="May" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}05" VisibleIndex="5">
                                        <PropertiesHyperLinkEdit Text="May" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}05" TextField="Month_5">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="June" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}06" VisibleIndex="6">
                                        <PropertiesHyperLinkEdit Text="Jun" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}06" TextField="Month_6">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="July" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}07" VisibleIndex="7">
                                        <PropertiesHyperLinkEdit Text="Jul" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}07" TextField="Month_7">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="August" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}08" VisibleIndex="8">
                                        <PropertiesHyperLinkEdit Text="Aug" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}08" TextField="Month_8">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="September" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}09" VisibleIndex="9">
                                        <PropertiesHyperLinkEdit Text="Sep" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}09" TextField="Month_9">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="October" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}10" VisibleIndex="10">
                                        <PropertiesHyperLinkEdit Text="Oct" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}10" TextField="Month_10">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="November" FieldName="Year"  Width="8%"
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}11" VisibleIndex="11">
                                        <PropertiesHyperLinkEdit Text="Nov" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}11" TextField="Month_11">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                    <dx:GridViewDataHyperLinkColumn Caption="December" FieldName="Year"  Width="8%" 
                                        PropertiesHyperLinkEdit-Text="Year" PropertiesHyperLinkEdit-Target="_self"
                                        PropertiesHyperLinkEdit-TextField="Year" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}12" VisibleIndex="12">
                                        <PropertiesHyperLinkEdit Text="Dec" Target="_self"
                                            NavigateUrlFormatString="ServiceKitVADetail.aspx?{0}12" TextField="Month_12">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                    </dx:GridViewDataHyperLinkColumn>

                                </Columns>
                                
                            </dx:ASPxGridView>
                            <br />
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 2
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Name="Data" Text="Breakdown By Dealer">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server">

                            <dx:ASPxGridView ID="gridServiceKits" runat="server" AutoGenerateColumns="False" Width="100%" >
                                
                                <SettingsPager PageSize="16"  Visible="True" AllButton-Visible="true"/>
                                
                                <SettingsBehavior AllowSort="true" />    

                                <Settings 
                                    ShowGroupButtons="False" 
                                    ShowFooter="true"
                                    ShowStatusBar="Hidden" 
                                    ShowTitlePanel="False" 
                                    UseFixedTableLayout="True"  
                                    
                                    ShowFilterBar="Hidden" 
                                    ShowFilterRowMenu="False" 
                                    ShowHeaderFilterButton="false"/>
                                          
                                <Styles>
                                    <Header Wrap="True" HorizontalAlign="Center" />
                                    <Cell HorizontalAlign="Center"/>
                                    <Footer HorizontalAlign="Center" />
                                </Styles>

                                <Columns>
                                
                                    <dx:GridViewBandColumn Caption="Dealer" HeaderStyle-HorizontalAlign="Center" >
                                        <Columns>
                                                <dx:GridViewDataTextColumn VisibleIndex="0" FieldName="DealerCode" ReadOnly="True" Width="3%" Caption="Code" ExportWidth="100">
                                                </dx:GridViewDataTextColumn>
                                    
                                                <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="CentreName" ReadOnly="True" Width="10%" Caption="Name" CellStyle-Wrap="False" HeaderStyle-HorizontalAlign="Left"  CellStyle-HorizontalAlign="Left" ExportWidth="200" CellStyle-Font-Size="Smaller">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="January">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_1" 
                                                Name="LastYear_1" 
                                                VisibleIndex="2" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_1" 
                                                Name="ThisYear_1" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="3" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_1diff" 
                                                Name="ThisYear_1diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="3" 
                                                ExportWidth="50"/>
                                       </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="February">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_2" 
                                                Name="LastYear_2" 
                                                VisibleIndex="4" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_2" 
                                                Name="ThisYear_2" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="5" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_2diff" 
                                                Name="ThisYear_2diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="5" 
                                                ExportWidth="50"/>

                                            </Columns>
                                    </dx:GridViewBandColumn>

                                     <dx:GridViewBandColumn Caption="March">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_3" 
                                                Name="LastYear_3" 
                                                VisibleIndex="6" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_3" 
                                                Name="ThisYear_3" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="7" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_3diff" 
                                                Name="ThisYear_3diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="8" 
                                                ExportWidth="50"/>

                                            </Columns>


                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="April">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_4" 
                                                Name="LastYear_4" 
                                                VisibleIndex="8" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_4" 
                                                Name="ThisYear_4" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="9" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_4diff" 
                                                Name="ThisYear_4diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="9" 
                                                ExportWidth="50"/>




                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="May">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_5" 
                                                Name="LastYear_5" 
                                                VisibleIndex="10" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_5" 
                                                Name="ThisYear_5" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="11" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_5diff" 
                                                Name="ThisYear_5diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="11" 
                                                ExportWidth="50"/>


                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="June">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_6" 
                                                Name="LastYear_6" 
                                                VisibleIndex="12" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_6" 
                                                Name="ThisYear_6" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="13" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_6diff" 
                                                Name="ThisYear_6diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="13" 
                                                ExportWidth="50"/>



                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="July">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_7" 
                                                Name="LastYear_7" 
                                                VisibleIndex="14" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_7" 
                                                Name="ThisYear_7" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="15" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_7diff" 
                                                Name="ThisYear_7diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="15" 
                                                ExportWidth="50"/>

                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="August">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_8" 
                                                Name="LastYear_8" 
                                                VisibleIndex="16" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_8" 
                                                Name="ThisYear_8" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="17" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_8diff" 
                                                Name="ThisYear_8diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="17" 
                                                ExportWidth="50"/>



                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="September">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_9" 
                                                Name="LastYear_9" 
                                                VisibleIndex="18" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_9" 
                                                Name="ThisYear_9" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="19" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_9diff" 
                                                Name="ThisYear_9diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="19" 
                                                ExportWidth="50"/>



                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="October">   
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_10" 
                                                Name="LastYear_10" 
                                                VisibleIndex="20" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_10"
                                                Name="ThisYear_10" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="21" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_10diff" 
                                                Name="ThisYear_10diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="21" 
                                                ExportWidth="50"/>



                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="November">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_11" 
                                                Name="LastYear_11" 
                                                VisibleIndex="22" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_11" 
                                                Name="ThisYear_11" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="23" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_11diff" 
                                                Name="ThisYear_11diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="23" 
                                                ExportWidth="50"/>


                                            </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="December">
                                        <Columns>
                                            <dx:GridViewDataTextColumn  
                                                FieldName="LastYear_12" 
                                                Name="LastYear_12" 
                                                VisibleIndex="24" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                ExportWidth="50"/>
                        
                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_12" 
                                                Name="ThisYear_12" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                VisibleIndex="25" 
                                                ExportWidth="50"/>

                                            <dx:GridViewDataTextColumn 
                                                FieldName="ThisYear_12diff" 
                                                Name="ThisYear_12diff" 
                                                PropertiesTextEdit-DisplayFormatString="#,##0"
                                                Caption="+/-"
                                                VisibleIndex="25" 
                                                ExportWidth="50"/>


                                            </Columns>
                                    </dx:GridViewBandColumn>

                                </Columns>
                                
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName ="LastYear_1" DisplayFormat="#,##0" ShowInColumn ="LastYear_1"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_1" DisplayFormat="#,##0" ShowInColumn ="ThisYear_1"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_1diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_1diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_2" DisplayFormat="#,##0" ShowInColumn ="LastYear_2"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_2" DisplayFormat="#,##0" ShowInColumn ="ThisYear_2"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_2diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_2diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_3" DisplayFormat="#,##0" ShowInColumn ="LastYear_3"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_3" DisplayFormat="#,##0" ShowInColumn ="ThisYear_3"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_3diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_3diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_4" DisplayFormat="#,##0" ShowInColumn ="LastYear_4"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_4" DisplayFormat="#,##0" ShowInColumn ="ThisYear_4"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_4diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_4diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_5" DisplayFormat="#,##0" ShowInColumn ="LastYear_5"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_5" DisplayFormat="#,##0" ShowInColumn ="ThisYear_5"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_5diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_5diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_6" DisplayFormat="#,##0" ShowInColumn ="LastYear_6"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_6" DisplayFormat="#,##0" ShowInColumn ="ThisYear_6"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_6diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_6diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_7" DisplayFormat="#,##0" ShowInColumn ="LastYear_7"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_7" DisplayFormat="#,##0" ShowInColumn ="ThisYear_7"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_7diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_7diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_8" DisplayFormat="#,##0" ShowInColumn ="LastYear_8"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_8" DisplayFormat="#,##0" ShowInColumn ="ThisYear_8"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_8diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_8diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_9" DisplayFormat="#,##0" ShowInColumn ="LastYear_9"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_9" DisplayFormat="#,##0" ShowInColumn ="ThisYear_9"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_9diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_9diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_10" DisplayFormat="#,##0" ShowInColumn ="LastYear_10"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_10" DisplayFormat="#,##0" ShowInColumn ="ThisYear_10"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_10diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_10diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_11" DisplayFormat="#,##0" ShowInColumn ="LastYear_11"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_11" DisplayFormat="#,##0" ShowInColumn ="ThisYear_11"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_11diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_11diff"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="LastYear_12" DisplayFormat="#,##0" ShowInColumn ="LastYear_12"  SummaryType="Sum"  />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_12" DisplayFormat="#,##0" ShowInColumn ="ThisYear_12"  SummaryType="Sum" />
                                    <dx:ASPxSummaryItem FieldName ="ThisYear_12diff" DisplayFormat="#,##0" ShowInColumn ="ThisYear_12diff"  SummaryType="Sum" />
                                </TotalSummary>                            
                            
                            
                            
                            
                            </dx:ASPxGridView>



                            <br />
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                

            </TabPages>
            
        </dx:ASPxPageControl>

    </div>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridServiceKits">
    </dx:ASPxGridViewExporter>

</asp:Content>

