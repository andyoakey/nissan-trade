﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TradeLoyaltyAdd.aspx.vb" Inherits="TradeLoyaltyAdd" %>

<!DOCTYPE html>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function OnBtnClientClick(s, e) {
            window.parent.HideUserActionWindow();
        }
    </script>
</head>
<body>

    <form id="form1" runat="server">

    <div style="width:100%">

        <table style="width:100%; margin-top:10px;">
            
            <%-- Dealer Code --%>
            <tr>
                <td style="width:175px">
                    <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblDealerCode" runat="server" Text="Centre" TabIndex="0" />
                </td>
                <td style="width:75px">
                    <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtDealerCode" runat="server" Width="75px" TabIndex="1" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtDealerName" runat="server" Width="200px" TabIndex="2" AutoPostBack="True" ReadOnly="True" />
                </td>
            </tr>

            <%-- Owner Dealer Code --%>
            <tr>
                <td style="width:175px">
                    <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblOwnerDealerCode" runat="server" Text="Trade Centre" TabIndex="3" />
                </td>
                <td style="width:75px">
                    <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtOwnerDealerCode" runat="server" Width="75px" TabIndex="4" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtOwnerDealerName" runat="server" Width="200px" TabIndex="5" AutoPostBack="True" ReadOnly="True" />
                </td>
            </tr>

        </table>

        <table style="width:100%; border-top:solid; border-top-width:thin; border-top-color:black; margin-top:10px;">

            <tr>
                <td>
                    <dxe:ASPxLabel Font-Size="14px" Font-Bold="true" Font-Names="Calibri,Verdana" ID="lblAccountTitle" runat="server" Text="Links" TabIndex="19" />
                </td>
                <td>
                </td>
                <td>
                    <table style="width:100%">
                        <tr>
                            <td>
                                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblCustomerId" runat="server" Text="Customer Id (Optional)" TabIndex="19" />
                            </td>
                            <td>
                                <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtCustomerId"
                                    runat="server" Width="75px" TabIndex="7"  />
                            </td>
                            <td>
                                <dxe:ASPxButton ID="btnLookup" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="Lookup" 
                                    Width="75px" CssClass="indbutton" TabIndex="24" style="margin-left:10px" />
                                <dxe:ASPxButton ID="btnResetLinks" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="Reset" 
                                    Width="75px" CssClass="indbutton" TabIndex="24" style="margin-left:10px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:DropDownList ID="ddlLink1" runat="server" Width="200px" AutoPostBack="true" >
                        <asp:ListItem Text="<Add>" Value="0" />
                        <asp:ListItem Text="Account Code" Value="A" />
                        <asp:ListItem Text="Company Magic Number" Value="C" />
                        <asp:ListItem Text="Target Magic Number" Value="M" />
                    </asp:DropDownList>
                </td>
                <td>
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtLink1" runat="server" Width="145px" TabIndex="21" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblLink1" runat="server" Text="" TabIndex="19" />
                </td>
            </tr>

            <tr>
                <td style="width:210px">
                    <asp:DropDownList ID="ddlLink2" runat="server" Width="200px" AutoPostBack="true" >
                        <asp:ListItem Text="<Add>" Value="0" />
                        <asp:ListItem Text="Account Code" Value="A" />
                        <asp:ListItem Text="Company Magic Number" Value="C" />
                        <asp:ListItem Text="Target Magic Number" Value="M" />
                    </asp:DropDownList>
                </td>
                <td style="width:150px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtLink2" runat="server" Width="145px" TabIndex="21" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblLink2" runat="server" Text="" TabIndex="19" />
                </td>
            </tr>

            <tr>
                <td style="width:210px">
                    <asp:DropDownList ID="ddlLink3" runat="server" Width="200px" AutoPostBack="true" >
                        <asp:ListItem Text="<Add>" Value="0" />
                        <asp:ListItem Text="Account Code" Value="A" />
                        <asp:ListItem Text="Company Magic Number" Value="C" />
                        <asp:ListItem Text="Target Magic Number" Value="M" />
                    </asp:DropDownList>
                </td>
                <td style="width:150px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtLink3" runat="server" Width="145px" TabIndex="21" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblLink3" runat="server" Text="" TabIndex="19" />
                </td>
            </tr>

            <tr>
                <td style="width:210px">
                    <asp:DropDownList ID="ddlLink4" runat="server" Width="200px" AutoPostBack="true" >
                        <asp:ListItem Text="<Add>" Value="0" />
                        <asp:ListItem Text="Account Code" Value="A" />
                        <asp:ListItem Text="Company Magic Number" Value="C" />
                        <asp:ListItem Text="Target Magic Number" Value="M" />
                    </asp:DropDownList>
                </td>
                <td style="width:150px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtLink4" runat="server" Width="145px" TabIndex="21" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblLink4" runat="server" Text="" TabIndex="19" />
                </td>
            </tr>

            <tr>
                <td style="width:210px">
                    <asp:DropDownList ID="ddlLink5" runat="server" Width="200px" AutoPostBack="true" >
                        <asp:ListItem Text="<Add>" Value="0" />
                        <asp:ListItem Text="Account Code" Value="A" />
                        <asp:ListItem Text="Company Magic Number" Value="C" />
                        <asp:ListItem Text="Target Magic Number" Value="M" />
                    </asp:DropDownList>
                </td>
                <td style="width:150px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtLink5" runat="server" Width="145px" TabIndex="21" AutoPostBack="True" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblLink5" runat="server" Text="" TabIndex="19" />
                </td>
            </tr>

        </table>

        <table style="width:100%; border-top:solid; border-top-width:thin; border-top-color:black; margin-top:10px;">

            <%-- Account Name --%>
            <tr style="margin-top:10px">
                <td style="width:175px;">
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblAccountName" runat="server" Text="Account Name" TabIndex="8"  style="margin-top:10px"/>
                </td>
                <td style="width:325px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtAccountName" runat="server" Width="300px" TabIndex="9" AutoPostBack="True"  style="margin-top:10px" />
                </td>
                <td>
                </td>
            </tr>

            <%-- Account Post Code --%>
            <tr>
                <td style="width:175px">
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblAccountPostCode" runat="server" Text="Postcode" TabIndex="10" />
                </td>
                <td style="width:325px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtAccountPostCode" runat="server" Width="75px" TabIndex="11" AutoPostBack="True" />
                </td>
                <td>
                </td>
            </tr>

            <%-- Rebate --%>
            <tr>
                <td style="width:175px">
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblRebate" runat="server" Text="Rebate %" TabIndex="12" />
                </td>
                <td style="width:325px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtRebate" runat="server" Width="75px" TabIndex="13" />
                </td>
                <td>
                </td>
            </tr>

            <%-- Qualifier --%>
            <tr>
                <td style="width:175px">
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblQualifier" runat="server" Text="Qualifier %" TabIndex="14"  />
                </td>
                <td style="width:325px">
                    <dxe:ASPxTextBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="txtQualifier" runat="server" Width="75px" TabIndex="15" />
                </td>
                <td>
                </td>
            </tr>

            <%-- Start Date --%>
            <tr>
                <td style="width:175px;">
                    <dxe:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblStartdate" runat="server" Text="Start" TabIndex="16" />
                </td>
                <td style="width:325px">
                    <asp:DropDownList Font-Size="13px" Font-Names="Calibri,Verdana" DropDownRows="4"
                        ID="ddlStartPeriod" runat="server" AutoPostBack="True" Width="75px" TabIndex="17">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
            </tr>

        </table>

        <table style="width:100%; border-top:solid; border-top-width:thin; border-top-color:black; margin-top:10px;">
            <tr>
                <td style="width:100px;">
                    <dxe:ASPxButton ID="btnSaveReg" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="Save" 
                        Width="75px" CssClass="indbutton" TabIndex="24" style="margin-top:10px" />
                </td>
                <td>
                    <dxe:ASPxLabel Font-Size="14px" Font-Bold="True" Font-Names="Calibri,Verdana" ID="lblErr" runat="server" ForeColor="Red" />
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
