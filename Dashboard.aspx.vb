﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Web
Imports DevExpress.Web.Data
Imports QubeSecurity

Partial Class Dashboard

    Inherits System.Web.UI.Page

    Dim dsTargetResults As DataSet
    Dim dsTargetResults2015 As DataSet
    Dim dsSalesResults1 As DataSet
    Dim dsSalesResults2 As DataSet
    Dim dsKPIResults1 As DataSet
    Dim dsKPIResults2 As DataSet
    Dim dsCustomerSummaryResults As DataSet
    Dim dsBusinessSummaryResults As DataSet
    Dim dsBD4 As DataSet
    Dim dsTradeRewards As DataSet
    Dim dsStandards As DataSet
    Dim dsServiceTypes As DataSet

    Dim nForecastApplic As Double
    Dim nGrowthColumn As Integer

    Dim nperformance_last As Decimal = 0
    Dim nperformance_this As Decimal = 0

    Dim nEX_actual_2017 As Decimal = 0
    Dim nEX_forecast_2018 As Decimal = 0

    Dim nALL_actual_2017 As Decimal = 0
    Dim nALL_forecast_2018 As Decimal = 0

    Dim nDailyRunRate As Integer = 0
    Dim sBand As String = ""
    Dim bShowInfo As Boolean = False

    ' Used For Highlighting 
    Dim nQPayBand As Integer = 0
    Dim nDealerAverage As Integer = 0
    Dim nQForecast As Decimal = 0
    Dim nQRow As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '        Session("HighlightColour") = Color.LightCyan
        Session("HighlightColour") = Color.White
        'rblHighlight.BackColor = Session("HighlightColour")

        Dim i As Integer

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If



        Try
            If Not IsPostBack Then

                Session("MonthFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O'")
                Session("TradeRewardYear") = 2017
                Session("KPIYear") = Session("CurrentYear")
                'Session("KPIYear") = 2017
                Session("BandCaption") = "Sales Forecast"
                Session("dealerband") = 1
                For i = Session("CurrentYear") To (Session("CurrentYear") - 2) Step -1
                    ddlKPIYear.Items.Add(Trim(Str(i)))
                    ddlKPIYear.SelectedIndex = 0
                Next
                Session("HighlightPeriod") = "Q2"
            End If

        Catch ex As Exception
            Session("CallingModule") = "Dashboard.ASPX - Page_Load()"
            Session("StackTrace") = ex.StackTrace.ToString
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try



    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        'Try

        Dim sDate As String
            Dim sWholeMonth As String

            Dim sSelectionLevel As String = Session("SelectionLevel")
            Dim sSelectionId As String = Session("SelectionId")
            Dim nMonthFrom As Integer = Session("MonthFrom")
            Dim nKPIYear As Integer = Session("KPIYear")
            Dim nTradeRewardYear As Integer = Session("TradeRewardYear")

            If Page.IsPostBack = False Then
                If Session("ServiceType") = " " Or Session("ServiceType") Is Nothing Then
                    Session("ServiceType") = "DAM"
                End If
            End If

            Session("FinYear") = Trim(GetDataString("select financialyear from dataperiods where id = '" & Session("MonthFrom") & "'"))
            Session("ShortFinYear") = Right(Session("FinYear"), 5)

            '  Handle Master Page Button Clicks
            'Dim master_btnExcel As DevExpress.Web.ASPxButton
            'master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
            'master_btnExcel.Visible = True




            Dim master_btnBack As DevExpress.Web.ASPxButton
            master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
            master_btnBack.Visible = False

            If Session("ExcelClicked") = True Then
                Session("ExcelClicked") = False
                Call btnExcel_Click()
            End If
            '  End Handle Master Page Button Clicks


            Call LoadTab1DashboardData()
            Call LoadMainDashboardData()


            Me.Title = "Nissan Trade Site - Dashboard (Sales Performance)"

            Select Case sSelectionLevel
                Case "N", "G"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = False
                    gridKPI1.Columns(4).Visible = False
                    gridKPI2.Visible = False
                Case "R"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = False
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "T"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "Z"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = True
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "C"
                    If Not Page.IsPostBack Then
                        ASPxPageControl1.ActiveTabIndex = 0
                    End If
                    gridKPI1.Columns(1).Visible = True
                    gridKPI1.Columns(2).Visible = True
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = True
            End Select

            sDate = GetMaxDate(sSelectionLevel, sSelectionId)
            sWholeMonth = GetDataString("SELECT dbo.NicePeriodName(PeriodName) FROM DataPeriods WHERE Id = (SELECT DataEndPeriodWholeMonth FROM System)")

            lblPageTitle.Text = "Dashboard : Sales up to and including : " & sDate
            gridCustomerSummary.Columns(0).Caption = " * Customer Summary as at " & sDate
            If nKPIYear = CurrentYear() Then
                gridKPI1.Columns(0).Caption = "KPI Summary as at " & sWholeMonth
                gridKPI2.Columns(0).Caption = "National position as at " & sWholeMonth
                gridKPI2.Columns(2).Visible = True
            Else
                gridKPI1.Columns(0).Caption = "KPI Summary as at 31st Dec " & nKPIYear
                gridKPI2.Columns(0).Caption = "National position as at 31st Dec " & nKPIYear
                gridKPI2.Columns(2).Visible = False
            End If


        '_________________ TEMPORARILY REMOVED AT CLIENTS REQUEST( FOR JIRA NMGBM-188)
        If sSelectionLevel = "C" Then '' Only show this for Participating Single Dealers 

            Session("BonusDealer") = GetDataDecimal("Select dbo.IsBonusDealer('" & Session("SelectionID") & "','2019/20')")
            If Session("BonusDealer") = 1 Then
                ASPxPageControl1.TabPages(3).Visible = True
                Call LoadBonusExtraData()
            Else
                ASPxPageControl1.TabPages(3).Visible = False
            End If

        Else
                ASPxPageControl1.TabPages(3).Visible = False
        End If

        '_________________ AND REPLACED WITH
        'ASPxPageControl1.TabPages(3).Visible = False			






        '        Catch ex As Exception
        '           Session("CallingModule") = "Dashboard.ASPX - Page_LoadComplete()"
        '          Session("ErrorToDisplay") = ex.Message
        '         Session("StackTrace") = ex.StackTrace.ToString
        '        Response.Redirect("dbError.aspx")
        '   End Try

    End Sub

    Protected Sub ds_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDashboardLeft.Init, dsDashboardRight.Init
        dsDashboardLeft.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsDashboardRight.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsCustomerSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBusinessTypeSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPI1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPI2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusPayments.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusBanding.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusByQuarter.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPIRanking.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Sub LoadTab1DashboardData()

        Dim da As New DatabaseAccess
        Dim sSection As String = ""
        Dim sErr As String = ""

        If Session("SelectionLevel") <> "N" And Session("SelectionLevel") <> "R" And Session("SelectionLevel") <> "T" And Session("SelectionLevel") <> "Z" And Session("SelectionLevel") <> "G" And Session("SelectionLevel") <> "C" Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If

        If Session("SelectionLevel") = "D" Or Session("SelectionLevel") = "C" Then
            gridtotalsales.Columns("valid_pc").Visible = True
        Else
            gridtotalsales.Columns("valid_pc").Visible = False
        End If

        '  Product Group Drop Down
        Try

            Dim htIn3 As New Hashtable

            dsServiceTypes = da.Read(sErr, "sp_ProductGroupList ", htIn3)

            ddlServiceTypes.DataSource = dsServiceTypes.Tables(0)
            ddlServiceTypes.DataBind()

        Catch ex As Exception

            If sErr <> "" Then
                Session("CallingModule") = "Dashboard.ASPX - LoadMainDashboardData() - " & sSection
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

        End Try

    End Sub

    Protected Sub ddlKPIYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlKPIYear.SelectedIndexChanged
        Session("KPIYear") = ddlKPIYear.SelectedItem.Value

        If ddlKPIYear.SelectedIndex = 0 Then
            gridKPI2.DataSourceID = "dsKPIRanking"
        Else
            gridKPI2.DataSourceID = "dsKPI2"
        End If

    End Sub

    Function GetPercChange(ByVal nThisYear As Decimal, ByVal nLastYear As Decimal) As Decimal

        Dim nResult As Decimal
        nResult = 0
        If nThisYear <> 0 And nLastYear <> 0 Then
            nResult = (nThisYear - nLastYear) / nLastYear
        End If
        GetPercChange = nResult

    End Function

    Function GetMargin(ByVal nSale As Decimal, ByVal nCost As Decimal) As Decimal
        Dim nResult As Decimal
        nResult = 0
        If nSale <> 0 And nCost <> 0 Then
            nResult = (nSale - nCost) / nSale
        End If
        GetMargin = nResult
    End Function

    Protected Sub gridSalesSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName <> "RowTitle" Then
            If e.GetFieldValue("RowType") = "P" Then
                If e.Column.FieldName <> "Col6" Then
                    e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
                Else
                    e.DisplayText = "-"
                End If
            ElseIf e.GetFieldValue("RowType") = "I" Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "#,##0")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "£#,##0")
            End If
        End If

    End Sub

    Protected Sub gridKPI2_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        Dim nNumber As Integer
        If e.Column.FieldName = "ColCentre" Then
            nNumber = IIf(IsDBNull(e.Value), 0, e.Value)
            e.DisplayText = FormatFancyNumber(Str(nNumber))
        End If
    End Sub

    Protected Sub gridCustomerSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ExcludedPerc" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub gridKPI2_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridKPI2.HtmlDataCellPrepared

        If e.DataColumn.Name = "Movement" Then
            If Not IsDBNull(e.CellValue) Then
                If e.CellValue > 0 Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Green
                    e.Cell.Text = "+" & e.CellValue
                End If

                If e.CellValue < 0 Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Red
                End If

            End If
        End If

    End Sub

    Protected Sub gridBD4_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If InStr(e.Column.FieldName, "_1") > 0 Or InStr(e.Column.FieldName, "_2") > 0 Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = Format(Val(e.Value), "£#,##0")
            End If
        End If

        If InStr(e.Column.FieldName, "_3") Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If

    End Sub

    Protected Sub gridBD4_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        Dim nThisValue As Decimal = 0

        If InStr(e.DataColumn.FieldName, "_3") Then
            If Not IsDBNull(e.CellValue) = True Then
                nThisValue = CDec(e.CellValue)
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = GlobalVars.g_Color_White
                If nThisValue = 0 Then
                    e.Cell.Font.Bold = False
                    e.Cell.BackColor = GlobalVars.g_Color_White
                    e.Cell.ForeColor = GlobalVars.g_Color_Black
                ElseIf nThisValue >= 100 Then
                    e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
                Else
                    e.Cell.BackColor = GlobalVars.g_Color_Tomato
                End If
            End If
        End If

    End Sub

    Protected Sub gridBusinessType_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridBusinessTypeSummary.HtmlDataCellPrepared
        If e.GetValue("RowOrder") = 2 Or e.GetValue("RowOrder") = 4 Or e.GetValue("RowOrder") = 5 Then
            e.Cell.Font.Bold = True
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#a6a6a6")
        End If
    End Sub

    Sub LoadMainDashboardData()

        Dim da As New DatabaseAccess

        Dim sSection As String = ""
        Dim sErr As String = ""

        Dim htIn1 As New Hashtable
        Dim htIn2 As New Hashtable
        Dim htIn3 As New Hashtable
        Dim htIn4 As New Hashtable
        Dim htIn5 As New Hashtable
        Dim htIn6 As New Hashtable
        Dim htIn7 As New Hashtable
        Dim htIn8 As New Hashtable
        Dim htIn9 As New Hashtable

        Dim sSelectionLevel As String = Session("SelectionLevel")
        Dim sSelectionId As String = Session("SelectionId")
        Dim nMonthFrom As Integer = Session("MonthFrom")
        Dim nKPIYear As Integer = Session("KPIYear")
        Dim nTradeRewardYear As Integer = Session("TradeRewardYear")
        '     Dim nApplic As Integer = ddlApplic.SelectedIndex

        If sSelectionLevel Is Nothing Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If

        If Session("SelectionLevel") <> "N" And Session("SelectionLevel") <> "R" And Session("SelectionLevel") <> "T" And Session("SelectionLevel") <> "Z" And Session("SelectionLevel") <> "G" And Session("SelectionLevel") <> "C" Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If
    End Sub

    Protected Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridtotalsales.HtmlDataCellPrepared, gridselectedsales.HtmlDataCellPrepared
        Dim nThisValue As Decimal = 0

        If e.DataColumn.Caption = "" Then
            e.Cell.Width = 2
        End If

        If e.DataColumn.Caption = "% Uplift" Then
            e.Cell.ForeColor = Color.White
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            If nThisValue = 0 Then
                e.Cell.BackColor = Color.White
                e.Cell.ForeColor = Color.Black
            ElseIf nThisValue >= 1 Then
                e.Cell.BackColor = Color.Green
            ElseIf nThisValue >= 0.9 Then
                e.Cell.BackColor = Color.Orange
                e.Cell.ForeColor = Color.Black
            Else
                e.Cell.BackColor = Color.Red
            End If
        End If

        If e.DataColumn.Caption = "% Valid Sales" Then
            e.Cell.ForeColor = Color.White
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If

            If nThisValue >= 0.8 Then
                e.Cell.BackColor = Color.Green
            Else
                e.Cell.BackColor = Color.Red
            End If

            If nThisValue = 0 Then
                e.Cell.ForeColor = Color.White
                e.Cell.BackColor = Color.White
            End If

        End If

    End Sub

    Private Sub ddlServiceTypes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceTypes.SelectedIndexChanged
        Session("ServiceType") = ddlServiceTypes.Value
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles expTotalSales.RenderBrick, expSelectedSales.RenderBrick, expCustomerSummary.RenderBrick, expBusinessTypeSummary.RenderBrick, expKPI1.RenderBrick, expKPI2.RenderBrick, expGridBonus1.RenderBrick, expGridBonus2.RenderBrick, expGridBonus3.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Sub btnExcel_Click()

        Dim link1 As New DevExpress.Web.Export.GridViewLink(expTotalSales)
        Dim link2 As New DevExpress.Web.Export.GridViewLink(expSelectedSales)
        Dim link3 As New DevExpress.Web.Export.GridViewLink(expCustomerSummary)
        Dim link4 As New DevExpress.Web.Export.GridViewLink(expBusinessTypeSummary)
        Dim link5 As New DevExpress.Web.Export.GridViewLink(expKPI1)
        Dim link6 As New DevExpress.Web.Export.GridViewLink(expKPI2)

        Dim linkbonus1 As New DevExpress.Web.Export.GridViewLink(expGridBonus1)
        Dim linkbonus2 As New DevExpress.Web.Export.GridViewLink(expGridBonus2)
        Dim linkbonus3 As New DevExpress.Web.Export.GridViewLink(expGridBonus3)

        gridtotalsales.Settings.ShowTitlePanel = True
        gridtotalsales.SettingsText.Title = "Total Sales"

        gridselectedsales.Settings.ShowTitlePanel = True
        gridselectedsales.SettingsText.Title = ddlServiceTypes.Text

        gridCustomerSummary.Settings.ShowTitlePanel = True
        gridCustomerSummary.SettingsText.Title = "Customer Summary"

        gridBusinessTypeSummary.Settings.ShowTitlePanel = True
        gridBusinessTypeSummary.SettingsText.Title = "Business Type Summary"

        gridKPI1.Settings.ShowTitlePanel = True
        gridKPI1.SettingsText.Title = "KPI Summary 1"

        gridKPI2.Settings.ShowTitlePanel = True
        gridKPI2.SettingsText.Title = "KPI Summary 2"


        If Session("SelectionLevel") = "C" Then
            gridBonus1.Settings.ShowTitlePanel = True
            gridBonus1.SettingsText.Title = lblBonus1.Text

            gridBonus2.Settings.ShowTitlePanel = True
            gridBonus2.SettingsText.Title = lblBonus2.Text

            gridBonus3.Settings.ShowTitlePanel = True
            gridBonus3.SettingsText.Title = lblBonus3.Text
        End If


        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())

        Dim headercell As New Link()
        Dim blankcell As New Link()
        AddHandler headercell.CreateDetailArea, AddressOf createheadercell
        AddHandler blankcell.CreateDetailArea, AddressOf createblankcell

        If Session("SelectionLevel") = "C" Then
            composite.Links.AddRange(New Object() {headercell, blankcell, link1, blankcell, link2, blankcell, link3, blankcell, link4, blankcell, link5, blankcell, link6, blankcell, linkbonus1, blankcell, linkbonus2, blankcell, linkbonus3})
        Else
            composite.Links.AddRange(New Object() {headercell, blankcell, link1, blankcell, link2, blankcell, link3, blankcell, link4, blankcell, link5, blankcell, link6})
        End If

        composite.CreateDocument()

        Dim stream As New System.IO.MemoryStream()

        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, "Dashboard", True, "xlsx", stream)

        gridtotalsales.Settings.ShowTitlePanel = vbFalse
        gridtotalsales.SettingsText.Title = ""

        gridselectedsales.Settings.ShowTitlePanel = vbFalse
        gridselectedsales.SettingsText.Title = ""

        gridCustomerSummary.Settings.ShowTitlePanel = vbFalse
        gridCustomerSummary.SettingsText.Title = ""

        gridBusinessTypeSummary.Settings.ShowTitlePanel = vbFalse
        gridBusinessTypeSummary.SettingsText.Title = ""

        gridKPI1.Settings.ShowTitlePanel = vbFalse
        gridKPI1.SettingsText.Title = ""

        gridKPI2.Settings.ShowTitlePanel = vbFalse
        gridKPI2.SettingsText.Title = ""

        If Session("SelectionLevel") = "C" Then
            gridBonus1.Settings.ShowTitlePanel = vbFalse
            gridBonus2.Settings.ShowTitlePanel = vbFalse
            gridBonus3.Settings.ShowTitlePanel = vbFalse
        End If


        'If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then
        '    gridBonusByQuarter.Settings.ShowTitlePanel = vbFalse
        '    gridBonusByQuarter.SettingsText.Title = ""
        'End If

    End Sub

    Public Sub createblankcell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.White)
        brick.Text = " "
        brick.Rect = New RectangleF(0, 0, 2000, 20)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 16, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "Trade Parts Overview "
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub gridBonusByQuarter_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridBonus1.HtmlDataCellPrepared, gridBonus2.HtmlDataCellPrepared, gridBonus3.HtmlDataCellPrepared

        If e.GetValue("workingdayscompleted") = 0 Then
            If FoundInList("quartername,workingdaysinquarter,workingdayscompleted", e.DataColumn.Name) = False Then
                e.Cell.ForeColor = Color.White
            End If
        End If

        If Right(Trim(e.DataColumn.Name), 6) = "growth" Then
            e.Cell.ForeColor = Color.White

            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If

            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            End If

            If e.CellValue = 0 Then
                e.Cell.BackColor = Color.White
            End If
        End If


        If sender.id = "gridBonus3" Then
            If e.GetValue("forecast_growth") <= 0 And e.DataColumn.FieldName = "rewardband" Then
                e.Cell.Text = "No Band Achieved"
                e.Cell.Font.Italic = True
            End If
        End If

        'If Left(e.GetValue("quartername"), 2) = Session("HighlightPeriod") Then
        '    If e.DataColumn.FieldName = "quartername" _
        '        Or e.DataColumn.FieldName = "sales_forecast" _
        '        Or e.DataColumn.FieldName = "rewardband" _
        '        Or e.DataColumn.FieldName = "sales_ex_2016fq" _
        '        Or e.DataColumn.FieldName = "forecast_exsales" Then
        '        e.Cell.BackColor = Session("HighlightColour")
        '    End If
        'End If

        If e.GetValue("rewardband") = 0 Then
            bShowInfo = False
        Else
            bShowInfo = True
        End If

    End Sub

    Sub LoadBonusExtraData()

        With gridBonus1
            With .Columns("quartername")
                .Caption = "Quarter"
                .ExportWidth = 200
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdaysinquarter")
                .Caption = "Working Days this Quarter"
                .ToolTip = "Total Number of Working Days in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdayscompleted")
                .Caption = "Working Days Completed"
                .ToolTip = "Total Number of Working Days that have accrued so far in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_ex_Last")
                .Caption = "Actual - FY18 (Quarter to Date)"
                .ToolTip = "Total Invoiced Sales for same period in Financial Year 2018/19 (Excludes NWCR Sales). If this is the current Quarter then sales from 2018/19 are pro-rata."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("runrate_ex")
                .Caption = "Daily Run Rate This Quarter"
                .ToolTip = "Sales Per Day for this Quarter (Excludes NWCR Sales). "
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_ex_Lastfq")
                .Caption = "Actual - FY18 (Full Quarter)"
                .ToolTip = "Total Invoiced Sales for whole Quarter in Financial Year 2018/19 (Excludes NWCR Sales). "
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_ex_This")
                .Caption = "Actual - FY19 (Quarter To Date)"
                .ToolTip = "Total Invoiced Sales in this Quarter - to date if current Quarter - (Excludes NWCR Sales)"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("growth_ex")
                .Caption = "Growth Year on Year Projected"
                .ToolTip = "Uplift"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("forecast_exsales")
                .Caption = "Projected Sales"
                .ToolTip = "Projected Sales for this Quarter calculated as follows:   1. For Quarters that are completed - [Actual Sales]   2. For Quarters that are started but incomplete - [Daily Run Rate * Working Days accrued]."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

        End With

        With gridBonus2
            With .Columns("quartername")
                .Caption = "Quarter"
                .ExportWidth = 200
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdaysinquarter")
                .Caption = "Working Days this Quarter"
                .ToolTip = "Total Number of Working Days in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdayscompleted")
                .Caption = "Working Days Completed"
                .ToolTip = "Total Number of Working Days that have accrued so far in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_all_Last")
                .Caption = "Actual - FY18 (Quarter to Date)"
                .ToolTip = "Total Invoiced Sales for same period in Financial Year 2018/19 (Including NWCR Sales). If this is the current Quarter then sales from 2018/19 are pro-rata."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("runrate_all")
                .Caption = "Daily Run Rate This Quarter"
                .ToolTip = "Sales Per Day for this Quarter . "
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_all_Lastfq")
                .Caption = "Actual - FY18 (Full Quarter)"
                .ToolTip = "Total Invoiced Sales for whole Quarter in Financial Year 2018/19"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("sales_all_This")
                .Caption = "Actual - FY19 (Quarter To Date)"
                .ToolTip = "Total Invoiced Sales in this Quarter - to date if current Quarter"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("growth_all")
                .Caption = "Growth Year on Year Projected"
                .ToolTip = "Uplift"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("forecast_sales")
                .Caption = "Projected Sales"
                .ToolTip = "Projected Sales for this Quarter calculated as follows:   1. For Quarters that are completed - [Actual Sales]   2. For Quarters that are started but incomplete - [Daily Run Rate * Working Days accrued]."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

        End With

        With gridBonus3
            With .Columns("quartername")
                .Caption = "Quarter"
                .ExportWidth = 200
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdaysinquarter")
                .Caption = "Working Days this Quarter"
                .ToolTip = "Total Number of Working Days in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("workingdayscompleted")
                .Caption = "Working Days Completed"
                .ToolTip = "Total Number of Working Days that have accrued so far in this quarter.  Working days are Mon-Fri ex Bank Holidays"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("forecast_sales")
                .Caption = "Operating Standards Projected Sales"
                .ToolTip = "Projected Sales for this Quarter calculated as follows:   1. For Quarters that are completed - [Actual Sales]   2. For Quarters that are started but incomplete - [Daily Run Rate * Working Days accrued]."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("StandardsReward")
                .Caption = "Operating Standards Projected Reward*"
                .ExportWidth = 150
                .ToolTip = "Operating Standards Reward payment is subject to achieving Nissan Trade Direct Standards pass."
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("forecast_exsales")
                .Caption = "Sales Performace Projected Sales"
                .ToolTip = "Projected Sales for this Quarter calculated as follows:   1. For Quarters that are completed - [Actual Sales]   2. For Quarters that are started but incomplete - [Daily Run Rate * Working Days accrued]."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("forecast_growth")
                .Caption = "Sales Performace Projected Growth %"
                .ToolTip = "Projected uplift for this Quarter excluding NWCR .  If quarter is complete figures are actual totals"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

            With .Columns("rewardband")
                .Caption = "Payment Band"
                .ToolTip = "The Payment Band is determined by the Projected % Growth (excluding NWCR)."
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With


            With .Columns("rewardpayment")
                .Caption = "Sales Performace Projected Reward £"
                .ExportWidth = 150
                .HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                .CellStyle.HorizontalAlign = HorizontalAlign.Center
            End With

        End With

        If Session("SelectionLevel") = "C" Then
            sBand = GetDataString("Select banding from Bonus_DealerUIO where dealercode = '" & Session("SelectionId") & "'")

            If Left(sBand, 1) = "1" Then
                sBand = "1. Above Average"
                Session("dealerband") = 1
            End If

            If Left(sBand, 1) = "2" Then
                sBand = "2. Average"
                Session("dealerband") = 2
            End If

            If Left(sBand, 1) = "3" Then
                sBand = "3. Below Average"
                Session("dealerband") = 3
            End If

            lblPerformanceBand.Text = "Dealer Performance Banding:  " & sBand



        End If

        'If Session("SelectionLevel") = "G" Then
        '    sBand = GetDataString("select banding from Bonus_DealerGroupUIO Where dealergroup_id = " & Session("SelectionId"))
        '    lblRunRate.Text = "Group Daily Run Rate (inc. NWCR sales where applicable) - £" & Format(nDailyRunRate, "#,###.")
        '    lblPerformanceBand.Text = "Group Performance Banding: " & sBand
        'End If

        gridPaymentScale.DataBind()

    End Sub

    Protected Sub CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridBonus1.CustomSummaryCalculate, gridBonus2.CustomSummaryCalculate, gridBonus3.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nperformance_last = 0
            nperformance_this = 0

            nEX_actual_2017 = 0
            nEX_forecast_2018 = 0

            nALL_actual_2017 = 0
            nALL_forecast_2018 = 0

        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nperformance_last = nperformance_last + e.GetValue("sales_all_2017")
            nperformance_this = nperformance_this + e.GetValue("sales_all_2018")

            nEX_actual_2017 = nEX_actual_2017 + e.GetValue("sales_ex_2017")
            nEX_forecast_2018 = nEX_forecast_2018 + e.GetValue("sales_ex_2018")

            nALL_actual_2017 = nALL_actual_2017 + e.GetValue("sales_ex_2017fq")
            nALL_forecast_2018 = nALL_forecast_2018 + e.GetValue("forecast_exsales")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "growth_all" Then
                If nperformance_last > 0 And nperformance_this > 0 Then
                    e.TotalValue = ((nperformance_this / nperformance_last) - 1)
                End If
            End If

            If objSummaryItem.FieldName = "growth_ex" Then
                If nEX_actual_2017 > 0 And nEX_forecast_2018 > 0 Then
                    e.TotalValue = ((nEX_forecast_2018 / nEX_actual_2017) - 1)
                End If
            End If

            If objSummaryItem.FieldName = "forecast_growth" Then
                If nALL_actual_2017 > 0 And nALL_forecast_2018 > 0 Then
                    e.TotalValue = ((nALL_forecast_2018 / nALL_actual_2017) - 1)
                End If
            End If

        End If

    End Sub

    Protected Sub grid_CustomButtonCallback(ByVal sender As Object, ByVal e As ASPxGridViewCustomButtonCallbackEventArgs) Handles gridBonus1.CustomButtonCallback, gridBonus2.CustomButtonCallback, gridBonus3.CustomButtonCallback
        If e.ButtonID <> "btninfo" Then
            Return
        End If
        Session("ShowInformationColours") = vbTrue
    End Sub

    Private Sub gridBonusBandings_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridBonusBandings.HtmlDataCellPrepared

        'Select Case Left(LTrim(sBand), 1)
        '    Case "1"  ' Below Average
        '        If e.DataColumn.Name = "band0_text" Then
        '            If nQPayBand = 0 Then
        '                If e.GetValue("id") = 1 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band1_text" Then
        '            If nQPayBand = 1 Then
        '                If e.GetValue("id") = 1 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band2_text" Then
        '            If nQPayBand = 2 Then
        '                If e.GetValue("id") = 1 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band3_text" Then
        '            If nQPayBand = 3 Then
        '                If e.GetValue("id") = 1 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '    Case "2"  ' Average

        '        If e.DataColumn.Name = "band0_text" Then
        '            If nQPayBand = 0 Then
        '                If e.GetValue("id") = 2 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If


        '        If e.DataColumn.Name = "band1_text" Then
        '            If nQPayBand = 1 Then
        '                If e.GetValue("id") = 2 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band2_text" Then
        '            If nQPayBand = 2 Then
        '                If e.GetValue("id") = 2 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band3_text" Then
        '            If nQPayBand = 3 Then
        '                If e.GetValue("id") = 2 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If
        '    Case "3"  ' AboveAverage

        '        If e.DataColumn.Name = "band0_text" Then
        '            If nQPayBand = 0 Then
        '                If e.GetValue("id") = 3 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band1_text" Then
        '            If nQPayBand = 1 Then
        '                If e.GetValue("id") = 3 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band2_text" Then
        '            If nQPayBand = 2 Then
        '                If e.GetValue("id") = 3 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If

        '        If e.DataColumn.Name = "band3_text" Then
        '            If nQPayBand = 3 Then
        '                If e.GetValue("id") = 3 Then
        '                    e.Cell.BackColor = Session("HighlightColour")
        '                End If
        '            End If
        '        End If
        'End Select


    End Sub

    Private Sub gridPaymentScale_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridPaymentScale.HtmlDataCellPrepared

        Dim nDisplayValueLower As Integer = 0
        Dim nDisplayValueUpper As Integer = 0

        Select Case e.GetValue("id")
            Case 1
                nDisplayValueLower = 0
                nDisplayValueUpper = 31249
            Case 2
                nDisplayValueLower = 31250
                nDisplayValueUpper = 62499
            Case 3
                nDisplayValueLower = 62500
                nDisplayValueUpper = 124999
            Case 4
                nDisplayValueLower = 125000
                nDisplayValueUpper = 187499
            Case 5
                nDisplayValueLower = 187500
                nDisplayValueUpper = 274999
            Case 6
                nDisplayValueLower = 275000
                nDisplayValueUpper = 374999
            Case 7
                nDisplayValueLower = 375000
                nDisplayValueUpper = 499999
            Case 8
                nDisplayValueLower = 500000
                nDisplayValueUpper = 50000000
        End Select

        'If e.DataColumn.Name = "payband_0" Then
        '    If nQPayBand = 0 Then
        '        If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
        '            e.Cell.BackColor = Session("HighlightColour")
        '        Else
        '            e.Cell.ForeColor = Color.Black
        '            e.Cell.BackColor = Color.White
        '        End If
        '    End If
        'End If


        'If e.DataColumn.Name = "payband_1" Then
        '    If nQPayBand = 1 Then
        '        If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
        '            e.Cell.BackColor = Session("HighlightColour")
        '        Else
        '            e.Cell.ForeColor = Color.Black
        '            e.Cell.BackColor = Color.White
        '        End If
        '    End If
        'End If

        'If e.DataColumn.Name = "payband_2" Then
        '    If nQPayBand = 2 Then
        '        If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
        '            e.Cell.BackColor = Session("HighlightColour")
        '        Else
        '            e.Cell.ForeColor = Color.Black
        '            e.Cell.BackColor = Color.White
        '        End If
        '    End If
        'End If

        'If e.DataColumn.Name = "payband_3" Then
        '    If nQPayBand = 3 Then
        '        If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
        '            e.Cell.BackColor = Session("HighlightColour")
        '        Else
        '            e.Cell.ForeColor = Color.Black
        '            e.Cell.BackColor = Color.White
        '        End If
        '    End If
        'End If


    End Sub

    Private Sub btnInfo_Click(sender As Object, e As EventArgs) Handles btnInfo.Click
        popSupportingInfo.ShowOnPageLoad = True
    End Sub
End Class


