﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraCharts
Imports DevExpress.XtraCharts.Web

Partial Class TCSpendComparisonTA

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Trade Club Member Spends"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Member Spends by Product Catgeory", "Viewing Trade Club Member Spends By Product Category")
        End If

    End Sub

    Protected Sub dsSpendByTA_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSpendsByTA.Init
        If Not IsPostBack Then
            Call GetMonths(ddlFrom)
            Call GetMonths(ddlTo)
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
            Session("MonthTo") = Session("MonthFrom")
        End If
        dsSpendsByTA.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridSpendByTA_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 0 Then
            e.DisplayText = NicePeriodName(e.Value)
        End If
    End Sub

    Sub ChartLoad()

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable
        Dim i As Integer
        Dim nThisValue As Integer

        While WebChartControl1.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End While

        If WebChartControl1.Titles.Count = 0 Then
            Dim webTitle As New ChartTitle
            webTitle.Text = "Trade Club Spends Vs Non-Trade Club Spends By Category"
            WebChartControl1.Titles.Add(webTitle)
        End If


        Dim series1 As New Series
        Dim view1 As New SideBySideBarSeriesView

        series1.ArgumentScaleType = ScaleType.Qualitative
        series1.Label.Visible = True
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Currency
        series1.View = view1
        series1.LegendText = "Non-TC"

        Dim series2 As New Series
        Dim view2 As New SideBySideBarSeriesView

        series2.ArgumentScaleType = ScaleType.Qualitative
        series2.Label.Visible = True
        series2.PointOptions.ValueNumericOptions.Format = NumericFormat.Currency
        series2.View = view2
        series2.LegendText = "Trade Club"

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nFrom", Session("MonthFrom"))
        htin.Add("@nTo", Session("MonthTo"))

        ds = da.Read(sErrorMessage, "p_TC_Spend_Comparison_ByTA", htin)
        With ds.Tables(0).Rows(0)

            nThisValue = Val(.Item("TA1_Diff"))
            series1.Points.Add(New SeriesPoint("Mechanical Competitive", nThisValue))
            nThisValue = Val(.Item("TA1_TC"))
            series2.Points.Add(New SeriesPoint("Mechanical Competitive", nThisValue))

            nThisValue = Val(.Item("TA2_Diff"))
            series1.Points.Add(New SeriesPoint("Mechanical Balance", nThisValue))
            nThisValue = Val(.Item("TA2_TC"))
            series2.Points.Add(New SeriesPoint("Mechanical Balance", nThisValue))

            nThisValue = Val(.Item("TA3_Diff"))
            series1.Points.Add(New SeriesPoint("Lighting", nThisValue))
            nThisValue = Val(.Item("TA3_TC"))
            series2.Points.Add(New SeriesPoint("Lighting", nThisValue))

            nThisValue = Val(.Item("TA4_Diff"))
            series1.Points.Add(New SeriesPoint("Body", nThisValue))
            nThisValue = Val(.Item("TA4_TC"))
            series2.Points.Add(New SeriesPoint("Body", nThisValue))

            nThisValue = Val(.Item("TA5_Diff"))
            series1.Points.Add(New SeriesPoint("Accessories", nThisValue))
            nThisValue = Val(.Item("TA5_TC"))
            series2.Points.Add(New SeriesPoint("Accessories", nThisValue))

            nThisValue = Val(.Item("TA6_Diff"))
            series1.Points.Add(New SeriesPoint("Other", nThisValue))
            nThisValue = Val(.Item("TA6_TC"))
            series2.Points.Add(New SeriesPoint("Other", nThisValue))

        End With

        WebChartControl1.Series.Add(series1)
        WebChartControl1.Series.Add(series2)

        Dim diagram As XYDiagram = CType(WebChartControl1.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Product Category"
        diagram.AxisX.Title.Visible = True
        diagram.AxisX.Range.SideMarginsEnabled = True

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Total Sales £"
        diagram.AxisY.Title.Visible = True
        diagram.AxisY.Range.SideMarginsEnabled = True
        diagram.AxisY.NumericOptions.Format = NumericFormat.Currency

        WebChartControl1.Legend.Visible = True

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Call ChartLoad()
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.SelectedValue
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.SelectedValue
    End Sub

End Class
