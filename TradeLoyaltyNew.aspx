﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TradeLoyaltyNew.aspx.vb" Inherits="TradeLoyaltyNew" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
        function RegDelClick(contentUrl) {
            PopupUserAction.SetContentUrl(contentUrl);
            PopupUserAction.SetHeaderText('Remove Registration');
            PopupUserAction.SetSize(700, 160);
            PopupUserAction.Show();
        }
        function RegEditClick(contentUrl) {
            PopupUserAction.SetContentUrl(contentUrl);
            PopupUserAction.SetHeaderText('Edit Trade Loyalty Member Parameters');
            PopupUserAction.SetSize(900, 520);
            PopupUserAction.Show();
        }
        function HideRegDelWindow() {
            PopupUserAction.Hide();
            gridMembers.Refresh();
            gridLoyalty.Refresh();
        }
        function HideRegAddWindow() {
            PopupUserEdit.Hide();
            gridMembers.Refresh();
            gridLoyalty.Refresh();
        }
        function ViewInvoice(contentUrl) {
            PopupInvoice.SetContentUrl(contentUrl);
            PopupInvoice.SetHeaderText('Trade Invoice Details');
            PopupInvoice.SetSize(800, 500);
            PopupInvoice.Show();
        }
    </script>

    <dxe:ASPxPopupControl ID="PopupInvoice" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" ShowOnPageLoad="False" 
        ClientInstanceName="PopupInvoice" Modal ="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" HeaderStyle-Font-Size="Large" HeaderStyle-Font-Bold="true" >
        <ContentCollection>
            <dxe:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            </dxe:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dxe:ASPxPopupControl>

    <dxe:ASPxPopupControl ID="PopupUserAction" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUserAction"
        Modal="True" CloseAction="CloseButton" AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" HeaderStyle-Font-Size="Large"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxe:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            </dxe:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dxe:ASPxPopupControl>

    <dxe:ASPxPopupControl ID="PopupUserEdit" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUserEdit" 
        Modal="True" CloseAction="CloseButton" AllowDragging="True" Height="500px" Width="880px"
        HeaderText="New Trade Loyalty Member" PopupHorizontalAlign="WindowCenter" HeaderStyle-Font-Size="13px"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dxe:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
            </dxe:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dxe:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="100%">
                <tr>
                    <td style="width:60%" align="left" valign="middle" > 
                        <asp:Label ID="lblPageTitle" runat="server" Text="Trade Loyalty" Style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>

        <asp:Label ID="lblNoCust" runat="server" Text="No Trade Loyalty Customers found." style="font-weight: 700; font-size: large; margin-left:8px; margin-top:20px" Visible="false" >
        </asp:Label>

        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" EnableHierarchyRecreation="true"  
            Width="100%" ActiveTabIndex="0" >

            <ContentStyle>
                <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>

            <TabPages>

                <dxtc:TabPage Text="Loyalty Performance">

                    <ContentCollection>

                        <dxw:ContentControl runat="server">

                            <dxwgv:ASPxGridView ID="gridLoyalty" runat="server" width="100%" 
                                DataSourceID="dsLoyalty"  AutoGenerateColumns="False"  ClientInstanceName="gridLoyalty"
                                OnHtmlDataCellPrepared="AllGrids_HtmlDataCellPrepared"
                                style="position:relative; top:10px; left:5px; margin-bottom:20px;" KeyFieldName="Id">
       
                                <SettingsPager PageSize="120" Mode="ShowAllRecords">
                                </SettingsPager>
            
                                <Settings
                                    ShowFooter="True"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                                    ShowGroupFooter="VisibleIfExpanded"  />
            
                                <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                                    ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
                                <styles>
                                    <Header Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                                    <Footer Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                                    <Cell HorizontalAlign="Center" Font-Size="Small" />
                                </styles>

                                <Columns>
                
                                    <dxwgv:GridViewDataTextColumn Caption="Month" FieldName="sMonth" VisibleIndex="0" ExportWidth="100" />
                                    <dxwgv:GridViewDataTextColumn Caption="This Year" FieldName="ThisYear" VisibleIndex="1" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                    <dxwgv:GridViewDataTextColumn Caption="Last Year" FieldName="LastYear" VisibleIndex="2" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                    <dxwgv:GridViewDataTextColumn Caption="Variance" FieldName="Variance" VisibleIndex="3" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                    <dxwgv:GridViewDataTextColumn Caption="%"  FieldName="VariancePerc" Name="VariancePerc" VisibleIndex="4" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="0.0%" />
                                    <dxwgv:GridViewDataTextColumn Caption="Rebate" FieldName="Rebate" VisibleIndex="5" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" />
                                    <dxwgv:GridViewDataTextColumn Caption="Non-qualifying" FieldName="NonQuali" VisibleIndex="6" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="#,##0" />

                                </Columns>
            
                                <Templates>
            
                                    <DetailRow>

                                        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">

                                            <ContentStyle>
                                                <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
                                            </ContentStyle>

                                            <TabPages>

                                                <dxtc:TabPage Text="Centre Summary">
                                                    <ContentCollection>
                                                        <dxw:ContentControl ID="ContentControl1" runat="server">

                                                            <dxwgv:ASPxGridView ID="gridLoyaltyDetail" runat="server" KeyFieldName="DealerCode"
                                                                OnDetailRowGetButtonVisibility="gridLoyalty_DetailRowGetButtonVisibility"
                                                                OnBeforePerformDataSelect="gridLoyaltyDetail_BeforePerformDataSelect"
                                                                OnHtmlDataCellPrepared="AllGrids_HtmlDataCellPrepared"
                                                                OnCustomSummaryCalculate="gridLoyaltyDetail_CustomSummaryCalculate"
                                                                AutoGenerateColumns="False" DataSourceID="dsLoyaltyDetail" Width="100%">

                                                                <SettingsPager PageSize="20" Mode="ShowPager" AllButton-Visible="true" >
                                                                </SettingsPager>

                                                                <Settings
                                                                    ShowFooter="True" ShowStatusBar="Hidden" ShowTitlePanel="True"
                                                                    ShowGroupFooter="VisibleIfExpanded" />

                                                                <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000"
                                                                    ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                                                                <Styles>
                                                                    <Header Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                                                                    <Footer Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                                                                    <Cell HorizontalAlign="Center" Font-Size="Small" />
                                                                </Styles>

                                                                <Columns>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" Width="10%" ExportWidth="100" />

                                                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="1" ExportWidth="200" Width="28%">
                                                                        <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                                                        <CellStyle Font-Size="Small" HorizontalAlign="Left">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="This Year" FieldName="ThisYear" VisibleIndex="2" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                                                    <dxwgv:GridViewDataTextColumn Caption="Last Year" FieldName="LastYear" VisibleIndex="3" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                                                    <dxwgv:GridViewDataTextColumn Caption="Variance" FieldName="Variance" VisibleIndex="4" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" />
                                                                    <dxwgv:GridViewDataTextColumn Caption="%" FieldName="VariancePerc" Name="VariancePerc" VisibleIndex="5" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="0.0%" />
                                                                    <dxwgv:GridViewDataTextColumn Caption="Rebate" FieldName="Rebate" VisibleIndex="6" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" />
                                                                    <dxwgv:GridViewDataTextColumn Caption="Non-qualifying" FieldName="NonQuali" VisibleIndex="7" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="#,##0" />

                                                                </Columns>

                                                                <TotalSummary>
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="ThisYear" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="LastYear" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Variance" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="0.0%" FieldName="VariancePerc" SummaryType="Custom" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="Rebate" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="NonQuali" SummaryType="Sum" />
                                                                </TotalSummary>

                                                            </dxwgv:ASPxGridView>

                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>

                                                <dxtc:TabPage Text="Customer Spend Summary">
                                                    <ContentCollection>
                                                        <dxw:ContentControl ID="ContentControl2" runat="server">

                                                            <dxwgv:ASPxGridView ID="gridLoyaltyDrillDown" runat="server" KeyFieldName="TradeLoyaltyMemberId"
                                                                OnBeforePerformDataSelect="gridLoyaltyDetail_BeforePerformDataSelect"
                                                                OnHtmlDataCellPrepared="AllGrids_HtmlDataCellPrepared"
                                                                OnCustomSummaryCalculate="gridLoyaltyDetail1_CustomSummaryCalculate"  
                                                                AutoGenerateColumns="False" DataSourceID="dsLoyaltyDrillDownSpends" Width="100%">

                                                                <SettingsPager PageSize="20" Mode="ShowPager" AllButton-Visible="true" >
                                                                </SettingsPager>

                                                                <Settings ShowFilterRow="True" ShowHeaderFilterButton="true" 
                                                                    ShowFooter="True" ShowStatusBar="Hidden" ShowTitlePanel="True"
                                                                    ShowGroupFooter="VisibleIfExpanded" />

                                                                <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000"
                                                                    ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                                                                <Styles>
                                                                    <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                                                    <Footer Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                                                    <Cell HorizontalAlign="Center" Font-Size="Small" />
                                                                </Styles>

                                                                <Columns>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" Width="8%" ExportWidth="100" />

                                                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="1" ExportWidth="200" Width="20%" Visible="false" >
                                                                        <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                                                        <CellStyle Font-Size="X-Small" HorizontalAlign="Left">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Account" FieldName="AccountName" VisibleIndex="2" ExportWidth="300" Width="28%">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <CellStyle HorizontalAlign="Left" />
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Postcode" FieldName="AccountPostCode" VisibleIndex="3" Width="12%" ExportWidth="100" />

                                                                    <dxwgv:GridViewDataTextColumn Caption="This Year" FieldName="ThisYear" VisibleIndex="6" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                     
                                                                    <dxwgv:GridViewDataTextColumn Caption="Last Year" FieldName="LastYear" VisibleIndex="7" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Variance" FieldName="Variance" VisibleIndex="8" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="%" FieldName="VariancePerc" Name="VariancePerc" VisibleIndex="9" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="0.0%" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Rebate" FieldName="Rebate" VisibleIndex="12" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                </Columns>

                                                                <StylesEditors>
                                                                    <ProgressBar Height="25px">
                                                                    </ProgressBar>
                                                                </StylesEditors>

                                                                <TotalSummary>
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="ThisYear" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="LastYear" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Variance" SummaryType="Sum" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="0.0%" FieldName="VariancePerc" SummaryType="Custom" />
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="Rebate" SummaryType="Sum" />
                                                                </TotalSummary>

                                                            </dxwgv:ASPxGridView>

                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>

                                                <dxtc:TabPage Text="Customer Rebates Summary">
                                                    <ContentCollection>
                                                        <dxw:ContentControl ID="ContentControl3" runat="server">

                                                            <dxwgv:ASPxGridView ID="gridLoyaltyDrillDown1" runat="server" KeyFieldName="TradeLoyaltyMemberId"
                                                                OnBeforePerformDataSelect="gridLoyaltyDetail_BeforePerformDataSelect"
                                                                OnHtmlDataCellPrepared="DetailGrid_HtmlDataCellPrepared"
                                                                AutoGenerateColumns="False" DataSourceID="dsLoyaltyDrillDownRebates" Width="100%">

                                                                <SettingsPager PageSize="20" Mode="ShowPager" AllButton-Visible="true">
                                                                </SettingsPager>

                                                                <Settings ShowFilterRow="True" ShowHeaderFilterButton="true"
                                                                    ShowFooter="False" ShowStatusBar="Hidden" ShowTitlePanel="False"
                                                                    ShowGroupFooter="VisibleIfExpanded" />

                                                                <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000"
                                                                    ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                                                                <Styles>
                                                                    <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small" />
                                                                    <Footer Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small" />
                                                                    <Cell HorizontalAlign="Center" Font-Size="X-Small" />
                                                                </Styles>

                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" Width="8%" ExportWidth="100" />

                                                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="1" ExportWidth="200" Width="20%" Visible="false" >
                                                                        <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                                                        <CellStyle Font-Size="X-Small" HorizontalAlign="Left">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Account" FieldName="AccountName" VisibleIndex="2" ExportWidth="300" Width="20%">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                        <CellStyle HorizontalAlign="Left" />
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Postcode" FieldName="AccountPostCode" VisibleIndex="3" Width="8%" ExportWidth="100" />

                                                                    <dxwgv:GridViewDataTextColumn Caption="Centre Retail" FieldName="CentreRetail" VisibleIndex="4" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Sale Total" FieldName="ThisYear" VisibleIndex="5" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Discount Given" FieldName="ActualDiscountValue" VisibleIndex="6" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="%" FieldName="ActualDiscount" VisibleIndex="7" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="#,##0.0%" Width="6%" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Agreed Discount Qualifier" FieldName="AgreedDiscount" VisibleIndex="8" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="#,##0.0%" Width="6%" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Applicable Retail" FieldName="TGBRetail" VisibleIndex="9" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Agreed Rebate" FieldName="AgreedRebate" VisibleIndex="10" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="#,##0.0%" Width="6%" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataTextColumn Caption="Rebate" FieldName="Rebate" VisibleIndex="11" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="£#,##0.00" >
                                                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                </Columns>

                                                                <Templates>

                                                                    <DetailRow>

                                                                        <dxwgv:ASPxGridView ID="gridLoyaltyDrillDown2" runat="server" KeyFieldName="InvoiceNumber"
                                                                            OnBeforePerformDataSelect="gridLoyaltyDrillDown1_BeforePerformDataSelect"
                                                                            OnHtmlDataCellPrepared="InvoiceGrid_HtmlDataCellPrepared"
                                                                            OnCustomSummaryCalculate="gridLoyaltyDrillDown1_CustomSummaryCalculate"
                                                                            AutoGenerateColumns="False" DataSourceID="dsLoyaltyDrillDown2" Width="100%"
                                                                            Style="margin-top: 10px">

                                                                            <SettingsPager PageSize="12" Mode="ShowPager" AllButton-Visible="true">
                                                                            </SettingsPager>

                                                                            <Settings
                                                                                ShowFooter="True" ShowStatusBar="Hidden" ShowTitlePanel="True"
                                                                                ShowGroupFooter="VisibleIfExpanded" />

                                                                            <SettingsText Title="Invoices this month" />

                                                                            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000"
                                                                                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                                                                            <Styles>
                                                                                <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                                                                <Footer Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small" />
                                                                                <Cell HorizontalAlign="Center" Font-Size="Small" />
                                                                            </Styles>

                                                                            <Columns>
                                                                                <dxwgv:GridViewDataDateColumn Caption="Date" FieldName="InvoiceDate" VisibleIndex="0" ExportWidth="120" />

                                                                                <dxwgv:GridViewDataTextColumn Caption="Invoice Number" FieldName="EventId"
                                                                                    ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="1" Width="12%">
                                                                                    <DataItemTemplate>
                                                                                        <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init">
                                                                                        </dxe:ASPxHyperLink>
                                                                                    </DataItemTemplate>
                                                                                </dxwgv:GridViewDataTextColumn>

                                                                                <dxwgv:GridViewDataTextColumn Caption="Centre Retail" FieldName="RetailValue"
                                                                                    VisibleIndex="2" ExportWidth="200" PropertiesTextEdit-DisplayFormatString="£#,##0.00" />
                                                                                <dxwgv:GridViewDataTextColumn Caption="Sale Value" FieldName="SaleValue"
                                                                                    VisibleIndex="3" ExportWidth="200" PropertiesTextEdit-DisplayFormatString="£#,##0.00" />
                                                                                <dxwgv:GridViewDataTextColumn Caption="Discount" FieldName="Discount"
                                                                                    VisibleIndex="4" ExportWidth="200" PropertiesTextEdit-DisplayFormatString="£#,##0.00" />
                                                                                <dxwgv:GridViewDataTextColumn Caption="%" FieldName="DiscountPerc" Name="DiscountPerc"
                                                                                    VisibleIndex="5" ExportWidth="200" PropertiesTextEdit-DisplayFormatString="#,##0.0%" />
                                                                            </Columns>

                                                                            <TotalSummary>
                                                                                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="SaleValue" SummaryType="Sum" />
                                                                                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="RetailValue" SummaryType="Sum" />
                                                                                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="Discount" SummaryType="Sum" />
                                                                                <dxwgv:ASPxSummaryItem DisplayFormat="0.0%" FieldName="DiscountPerc" SummaryType="Custom" />
                                                                            </TotalSummary>

                                                                        </dxwgv:ASPxGridView>

                                                                    </DetailRow>

                                                                </Templates>

                                                                <SettingsDetail ShowDetailRow="True" ExportMode="Expanded"  />

                                                            </dxwgv:ASPxGridView>

                                                        </dxw:ContentControl>
                                                    </ContentCollection>
                                                </dxtc:TabPage>

                                            </TabPages>
            
                                            <LoadingPanelStyle ImageSpacing="6px">
                                            </LoadingPanelStyle>

                                        </dxtc:ASPxPageControl>

                                    </DetailRow>
                
                                </Templates>

                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>

                                <SettingsDetail ShowDetailRow="True" ExportMode="Expanded" />

                                <TotalSummary>
                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="ThisYear" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="LastYear" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Variance" SummaryType="Sum" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="0.0%" FieldName="VariancePerc" SummaryType="Custom" />
                                    <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="Rebate" SummaryType="Sum" />
                                </TotalSummary>

                            </dxwgv:ASPxGridView>

                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>

                <dxtc:TabPage Text="Members">

                    <ContentCollection>

                        <dxw:ContentControl runat="server">

                            <dxe:ASPxButton ID="btnNew" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana"
                                Text="New Member" Width="100px" style="position:relative; top:10px; left:5px; margin-bottom:10px;" >
                            </dxe:ASPxButton>

                            <dxwgv:ASPxGridView ID="gridMembers" runat="server" width="941px"  ClientInstanceName="gridMembers"
                                DataSourceID="dsLoyaltyMembers"  AutoGenerateColumns="False" 
                                style="position:relative; top:10px; left:5px; margin-bottom:20px;" >
       
                                <SettingsPager PageSize="12" Mode="ShowPager" AllButton-Visible="true" >
                                </SettingsPager>
            
                                <Settings ShowFilterRow="true" ShowFilterRowMenu="False" ShowHeaderFilterButton="True" 
                                    ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                                    ShowGroupFooter="VisibleIfExpanded"  />
            
                                <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000" 
                                    ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
                                <styles>
                                    <Header Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                                    <Footer Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" VerticalAlign="Middle" />
                                    <Cell HorizontalAlign="Center" Font-Size="X-Small" VerticalAlign="Top" />
                                </styles>

                                <Columns>
                
                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" ExportWidth="100" 
                                        Settings-AllowAutoFilter="False" Settings-HeaderFilterMode="List" Width="8%" />

                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="1" ExportWidth="300" Width="18%"
                                        Settings-HeaderFilterMode="List" >
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Account Name" FieldName="AccountName" VisibleIndex="2" ExportWidth="300" Width="18%"
                                        Settings-HeaderFilterMode="List" >
                                        <HeaderStyle Wrap="True" HorizontalAlign="Left" />
                                        <CellStyle HorizontalAlign="Left">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Postcode" FieldName="AccountPostCode" VisibleIndex="3" ExportWidth="100" 
                                        Settings-HeaderFilterMode="List"/>

                                    <dxwgv:GridViewDataTextColumn Caption="Rebate"  FieldName="Rebate" Name="Rebate" VisibleIndex="4" ExportWidth="120" 
                                        PropertiesTextEdit-DisplayFormatString="0.0%" Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False" />

                                    <dxwgv:GridViewDataTextColumn Caption="Qualifier"  FieldName="RebateDiscountQualifier" Name="RebateDiscountQualifier" VisibleIndex="5" ExportWidth="120" PropertiesTextEdit-DisplayFormatString="0.0%" 
                                        Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False" />

                                    <dxwgv:GridViewDataTextColumn Caption="Start Date" FieldName="PeriodName" VisibleIndex="6" ExportWidth="100" 
                                        Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False" />

                                    <dxwgv:GridViewDataTextColumn Caption="Links" FieldName="TradeLoyaltyMemberId" VisibleIndex="7" ExportWidth="100" width="18%" 
                                        PropertiesTextEdit-EncodeHTML="false" Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False" />

                                    <dxwgv:GridViewDataTextColumn Caption="Amend" FieldName="TradeLoyaltyMemberId" ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="8" 
                                        ToolTip="Amend this registration" UnboundType="String" Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False"> 
                                        <DataItemTemplate>
                                            <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="EditLink_Init">
                                            </dxe:ASPxHyperLink>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>        

                                    <dxwgv:GridViewDataTextColumn Caption="Remove" FieldName="TradeLoyaltyMemberId" ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="9" 
                                        ToolTip="Remove this registration" UnboundType="String" Settings-AllowAutoFilter="False" Settings-AllowHeaderFilter="False" Settings-ShowFilterRowMenu ="False"> 
                                        <DataItemTemplate>
                                            <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="DeleteLink_Init">
                                            </dxe:ASPxHyperLink>
                                        </DataItemTemplate>
                                    </dxwgv:GridViewDataTextColumn>        

                                </Columns>

                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>

                            </dxwgv:ASPxGridView>

                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>

            </TabPages>

            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>

        </dxtc:ASPxPageControl>

    </div>
    
    <asp:SqlDataSource ID="dsLoyalty" runat="server" SelectCommand="p_TradeLoyalty2_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TLYear" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsLoyaltyDetail" runat="server" SelectCommand="p_TradeLoyalty2_Summary_DrillDown" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TLYear" Type="Int16" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="TLMonth" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsLoyaltyDrillDownSpends" runat="server" SelectCommand="p_TradeLoyalty2_Detail_Spends" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TLYear" Type="Int16" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="TLMonth" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsLoyaltyDrillDownRebates" runat="server" SelectCommand="p_TradeLoyalty2_Detail_Rebates" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TLYear" Type="Int16" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="TLMonth" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsLoyaltyDrillDown2" runat="server" SelectCommand="p_TradeLoyalty2_Detail_Invoices" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nTradeLoyaltyMemberId" SessionField="TLMemberId" Type="Int16" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TLYear" Type="Int16" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="TLMonth" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsLoyaltyMembers" runat="server" SelectCommand="p_TradeLoyalty_Members" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridLoyalty" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="gridMembers" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>






