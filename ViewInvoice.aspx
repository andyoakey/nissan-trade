﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewInvoice.aspx.vb" Inherits="ViewInvoice"  MasterPageFile="~/MasterPage.master"%>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

<div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative; left:0px">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                runat="server" 
                Text="View Invoice Details" 
                Font-Size="Larger" />
        </div>

        <br />

        <table id="trSalesPerf" width="100%">

            <tr runat="server" id="tr2" style="height:30px" valign="top">
                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                    <dx:ASPxLabel
                        ID="ASPxLabel1" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Dealer Number:"/>
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblDealerCode" runat="server" Text="Label" />
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                     <dx:ASPxLabel
                        ID="ASPxLabel2" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Dealer Name:"/>
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblDealerName" runat="server" Text="Label" />
                </td>
            </tr>

            <tr runat="server" id="tr0" style="height:30px" valign="top">
                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                    <dx:ASPxLabel
                        ID="ASPxLabel3" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Invoice Number:"/>
                </td>
                
                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblInvoiceNo" runat="server" Text="Label" />
                </td>
                
                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                   <dx:ASPxLabel
                        ID="ASPxLabel4" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Invoice Date:"/>
                </td>
 
                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblInvoiceDate" runat="server" Text="Label" valign="middle"/>
                </td>
            </tr>
            
            <tr runat="server" id="tr1" style="height:30px" valign="top">
                 <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                      <dx:ASPxLabel
                        ID="ASPxLabel5" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Company Details:"/>
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblCompany" runat="server" Text="Label" />
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:10%"  valign="middle" >
                      <dx:ASPxLabel
                        ID="ASPxLabel6" 
                        runat="server" 
                        Font-Italic="true"
                        Text="Account Details:"/>
                </td>

                <td  style="border-style: solid; border-width: 1px; padding: 1px 4px; width:40%"  valign="middle" >
                    <dx:ASPxLabel ID="lblAccount" runat="server" Text="Label" />
                </td>
            </tr>

        </table> 

        <br />
        <br />
        
        <dx:ASPxGridView 
            ID="gridParts" 
            runat="server" 
            AutoGenerateColumns="False" 
            onload="GridStyles"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText"    
            DataSourceID="dsInvoiceParts" 
            KeyFieldName="Excluded" 
            Width="100%" >

            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true"
                LastPageButton-Visible="true" Mode="ShowAllRecords">
            </SettingsPager>

            <Settings ShowFooter="False" ShowGroupedColumns="False" ShowGroupFooter="Hidden"
                ShowGroupPanel="False" ShowHeaderFilterButton="False"  ShowFilterRow="false"
                ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True"/>

            <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"/>

            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>

            <Images >
                <CollapsedButton Height="12px" 
                    
                    Width="11px" />
                <ExpandedButton Height="12px" 
                     
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                     
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                     
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>

            <Columns>

                <dx:GridViewDataTextColumn Caption="Dept" FieldName="Department" ToolTip=""
                    VisibleIndex="0" Width="5%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Sale Type" FieldName="SaleType" ToolTip=""
                    VisibleIndex="0" Width="5%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Part" FieldName="PartNumber" ToolTip=""
                    VisibleIndex="1" Width="10%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Description" FieldName="PartDescription" ToolTip=""
                    VisibleIndex="2" Width="15%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="PFC" FieldName="PFC" ToolTip=""
                    VisibleIndex="3" Width="5%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Description" FieldName="PFCDescription" ToolTip=""
                    VisibleIndex="4" Width="15%">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="False" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Units" CellStyle-HorizontalAlign="Right" FieldName="Quantity"
                    ToolTip="The Quantity" VisibleIndex="5">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Sales Value" CellStyle-HorizontalAlign="Right"
                    FieldName="SaleValue" ToolTip="The Sales Value"
                    VisibleIndex="6">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Cost Value" CellStyle-HorizontalAlign="Right"
                    FieldName="CostValue" ToolTip="The Cost Value"
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Profit" CellStyle-HorizontalAlign="Right"
                    FieldName="Profit" ToolTip="The Margin"
                    VisibleIndex="8">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>

            </Columns>

        </dx:ASPxGridView>

        <br />
        <br />

 
        <dx:ASPxGridView 
            ID="gridTotals" 
            runat="server" 
            onload="GridStyles"
            AutoGenerateColumns="False" 
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText"    
            DataSourceID="dsInvoiceTotals" 
            KeyFieldName="SubTotal" 
            Width="60%" >

            <SettingsPager Mode="ShowAllRecords" Visible="False">
            </SettingsPager>
            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>
            <Images>
                <CollapsedButton Height="12px" 
                     
                    Width="11px" />
                <ExpandedButton Height="12px" 
                     
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                     
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>

            <Columns>

                <dx:GridViewDataTextColumn Caption="" FieldName="SubTotal" ToolTip=""
                    VisibleIndex="1" Width="25%">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Sales Value" CellStyle-HorizontalAlign="Right"
                    FieldName="SaleValue" ToolTip="The Sales Value"
                    VisibleIndex="6" Width="25%">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Cost Value" CellStyle-HorizontalAlign="Right"
                    FieldName="CostValue" ToolTip="The Cost Value"
                    VisibleIndex="7" Width="25%">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Margin" CellStyle-HorizontalAlign="Right"
                    FieldName="Margin" ToolTip="The Margin"
                    VisibleIndex="8">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>

            </Columns>

            <Settings UseFixedTableLayout="True" />

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

        </dx:ASPxGridView>
        
        <br />
  
        <asp:TextBox 
            ID="txtNotes" 
            runat="server" 
            Width="100%" 
            TabIndex="29" 
            TextMode="MultiLine" 
            BorderStyle="Outset" 
            ReadOnly="True" 
            Rows="4" 
            Visible="False" />

        <br />
        
    </div>


    <asp:SqlDataSource ID="dsInvoiceParts" runat="server" SelectCommand="p_InvoiceParts" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="DealerCode" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sInvoiceNumber" SessionField="InvoiceNumber" Type="String" Size="50" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsInvoiceTotals" runat="server" SelectCommand="p_InvoiceTotals" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="DealerCode" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sInvoiceNumber" SessionField="InvoiceNumber" Type="String" Size="50" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>