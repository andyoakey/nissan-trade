﻿Imports System.Data
Imports System.Net.Mail

Partial Class ContactHistoryRemove

    Inherits System.Web.UI.Page

    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        nId = Request.QueryString(0)
    End Sub

    Protected Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim sSQL As String

        '/ Remove history line
        sSQL = "DELETE FROM CustomerDealerContact WHERE Id = " & Trim(Str(nId))
        Call RunNonQueryCommand(sSQL)

        Dim startUpScript As String = String.Format("window.parent.HideContactHistoryWindow();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)

    End Sub

End Class

