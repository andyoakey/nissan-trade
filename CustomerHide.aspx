﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerHide.aspx.vb" Inherits="CustomerHide" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >

        <div id="divTitle" style="position:relative; left:5px">
        
            <table id="trCustomerDetails" width="976px" >
                <tr>
                    <td align="left" >
                        <asp:Label ID="lblPageTitle" runat="server" Text="Hide Customers" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                        <br />
                    </td>
                    <td align="right" >
                    </td>
                </tr>
            </table>
            
        </div>
        <br />
        
        <dxwgv:ASPxGridView ID="gridCompanies" runat="server" DataSourceID="dsCompanies"
            AutoGenerateColumns="False" KeyFieldName="Id" Width="976px" 
            >
            
            <SettingsBehavior AllowFocusedRow="True" />
            
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            
            <SettingsPager PageSize="12" Visible="False">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </SettingsPager>

            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>

            <Images >
                <LoadingPanelOnStatusBar >
                </LoadingPanelOnStatusBar>
                <LoadingPanel >
                </LoadingPanel>
            </Images>

            <Columns>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="BusinessURN"
                    Caption="URN" 
                    VisibleIndex="1" Visible="false" >
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" Wrap="False" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="BusinessName"
                    Caption="Name" 
                    VisibleIndex="1" Width="20%">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" Wrap="False" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address1"
                    Caption="Address 1" 
                    VisibleIndex="2"
                    Visible = "True" Width="20%">
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address2"
                    Caption="Address 2" 
                    VisibleIndex="3"
                    Visible = "True" Width="20%">
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="PostCode" 
                    Caption="Postcode" 
                    VisibleIndex="4"
                    Width="75px">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="TelephoneNumber"
                    Caption="Tele." 
                    VisibleIndex="5" Width="10%">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="CustomerStatus"
                    Caption="Customer Type" 
                    VisibleIndex="6" Width="5%">
                    <Settings ShowFilterRowMenu="False" AllowAutoFilter="False" 
                        AllowHeaderFilter="True" />
                    <EditFormSettings Visible="False" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="TradeClubNumber"
                    Caption="T/C No." 
                    VisibleIndex="7" Width="5%">
                    <Settings ShowFilterRowMenu="True" AllowAutoFilter="True" 
                        AllowHeaderFilter="True" />
                    <EditFormSettings Visible="False" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataDateColumn 
                    FieldName="LastSpend"
                    Caption="Last purchased" 
                    VisibleIndex="8" Width="5%">
                    <Settings ShowFilterRowMenu="True" AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataDateColumn>

                <dxwgv:GridViewDataTextColumn Caption="Total MTD" FieldName="SpendMTD" VisibleIndex="9"
                    Width="60px" EditFormSettings-Visible="False">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="True"
                        AutoFilterCondition="GreaterOrEqual" SortMode="Value" />
                    <EditFormSettings Visible="False"></EditFormSettings>
                    <HeaderStyle Wrap="True" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Total YTD" FieldName="SpendYTD" VisibleIndex="10"
                    Width="60px" EditFormSettings-Visible="False" >
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="True"
                        AutoFilterCondition="GreaterOrEqual" SortMode="Value" />
                    <EditFormSettings Visible="False"></EditFormSettings>
                    <HeaderStyle Wrap="True" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Id" 
                    Caption="Customer #" 
                    VisibleIndex="20"
                    Visible = "False">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            
        </dxwgv:ASPxGridView>
        <br />
        
        <table width="962px">
            <tr>
                <td>
                    <table width="650px">
                        <tr>
                            <td align="left" style="width:5%" >
                                <dxe:ASPxButton ID="btnHide" runat="server" Text="Hide" Width="100px" 
                                    >
                                </dxe:ASPxButton>
                            </td>
                            <td align="left" >
                                <dxe:ASPxLabel ID="lblErr1" runat="server" Text="" Visible="true" 
                                    Font-Size="Medium" ForeColor="#FF3300">
                                </dxe:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" >
                    <dxe:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="100px" 
                        >
                    </dxe:ASPxButton>
                </td>
            </tr>
        </table>
        <br />

   </div>
                
    <asp:SqlDataSource ID="dsCompanies" runat="server" SelectCommand="p_CustomerListDedupe" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sDeDupeIDs" SessionField="DeDupeIDs" Type="String" Size="255" />
        </SelectParameters>
    </asp:SqlDataSource>


</asp:Content>

