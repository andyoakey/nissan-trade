﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewBestSellingCustomers

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Highest Spending Customers"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        dsTradeAnalysis.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Best Selling Parts", "Best Selling Parts")
            Call RefreshGrid()
            Call GetBusinessTypesdx(ddlBusinessType)
            Session("BusinessType") = 0
        End If

    End Sub

    Protected Sub dsBestCustomers_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsBestCustomers.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        dsBestCustomers.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub
    Protected Sub ddlSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
            ddlProductGroup.SelectedIndexChanged,
            ddlTA.SelectedIndexChanged,
            ddlPeriod.SelectedIndexChanged,
            ddlTopN.SelectedIndexChanged,
            ddlBasis.SelectedIndexChanged,
            ddlType.SelectedIndexChanged,
            ddlBusinessType.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 10 Then
            If ddlBasis.SelectedIndex = 3 Then
                e.DisplayText = Format(e.Value, "£#,##0")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If
    End Sub

    Private Sub RefreshGrid()
        Session("Level4_Code") = ddlProductGroup.Value

        Session("TA") = ddlTA.Value
        If Session("TA") Is Nothing Then
            Session("TA") = "All Products"
        End If

        Session("Period") = ddlPeriod.Value
        Session("Items") = ddlTopN.Value
        Session("Basis") = ddlBasis.Value
        Session("Focus") = ddlType.Value
        Session("BusinessType") = ddlBusinessType.Value
        gridBestCustomers.DataBind()
        If ddlBasis.SelectedIndex = 3 Then
            gridBestCustomers.Columns("margin").Caption = "Profit (£)"
        Else
            gridBestCustomers.Columns("margin").Caption = "Margin (%)"
        End If
    End Sub

    Protected Sub ExpBestCustomers_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ExpBestCustomers.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        ExpBestCustomers.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStylesSearchOff(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

End Class
