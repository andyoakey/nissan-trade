﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class RebateCampaignByPartNumber

    Inherits System.Web.UI.Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim nTotalValue(2) As Double
    Dim nTotalUnits(2) As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dMaxDate As Date

        Me.Title = "Rebate Campaign By Part Number"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Session("DecimalPlaces") = 2
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Rebate Campaign", "Rebate Campaign")
            dMaxDate = GetMaxDate("N", "O")
            Dim nCurrentPeriod As Integer = GetDataDecimal("select ID from dataperiods WHERE '" & TextDate(dMaxDate) & "' between startdate and enddate")
            Session("MonthFrom") = nCurrentPeriod - 1
            Session("MonthTo") = nCurrentPeriod
            Call GetMonthsDevX(ddlFrom)
            ddlFrom.SelectedIndex = 1
            Call GetMonthsDevX(ddlTo)
        End If
        Session("CallingModule") = "RebateCampaign.aspx - PageLoad"
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Call LoadCampaignData()
        Call SetDecimals()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Session("SelectionLevel") = "C" Then
            gridCampaigns.Settings.ShowFooter = False
        Else
            gridCampaigns.Settings.ShowFooter = True
        End If

        'select rebatename,id from RebateCampaignControl order by id desc

    End Sub


    Sub LoadCampaignData()

        Dim htIn As New Hashtable
        htIn.Clear()
        htIn.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn.Add("@sSelectionId", Session("SelectionId"))
        htIn.Add("@nFrom", Session("MonthFrom"))
        htIn.Add("@nTo", Session("MonthTo"))

        dsResults = da.Read(sErrorMessage, "p_RebateCampaignByPart", htIn)
        gridCampaigns.DataSource = dsResults.Tables(0)
        gridCampaigns.DataBind()

    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "RebateCampaignByPartNumber"

        Call LoadCampaignData()
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsToResponse(New XlsExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub
    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared

        If e.DataColumn.FieldName = "Cost_Diff" Or e.DataColumn.FieldName = "Cost_DiffPerc" Or e.DataColumn.FieldName = "Units_Diff" Or e.DataColumn.FieldName = "Units_DiffPerc" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            Else
                e.Cell.BackColor = Color.Green
            End If
        End If

        If e.DataColumn.FieldName = "Rebate" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If
        End If

        If e.DataColumn.FieldName = "Missed_Rebate" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue <> 0 Then
                e.Cell.BackColor = Color.Red
            End If
        End If

    End Sub

    Protected Sub gridDrillDown_OnHtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Rebated_Value" Then
            e.DataColumn.PropertiesEdit.DisplayFormatString = "&#163;#,##0"
        End If


        If e.DataColumn.FieldName = "ActualRebate" Then
            e.Cell.ForeColor = Color.White

            If sender.id = "gridDrillDown0" Then
                e.Cell.BackColor = Color.Green   'Rebate Earned - show in Green
            End If

            If sender.id = "gridDrillDown1" Then
                e.Cell.BackColor = Color.Red   'Rebate Earned - show in Red
            End If

        End If

    End Sub


    Protected Sub gridDrillDown_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("RebateDealer") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Sub SetDecimals()

        ' Set the number of Decimal Places Based on User Input ,  The 3rd Parameter indicates if the Total Summary needs changing too
        Dim DecimalPlaces As Integer = Session("DecimalPlaces")
        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebated_Value"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebated_Value").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebated_Cost"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebated_Cost").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebate"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebate").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Value"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Value").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Cost"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Cost").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Rebate"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Rebate").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_This"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_This").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_Last"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_Last").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_Diff"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_Diff").DisplayFormat = "C" & DecimalPlaces


    End Sub
    Protected Sub chkRound_CheckedChanged(sender As Object, e As EventArgs)
        If chkRound.Checked Then
            Session("DecimalPlaces") = 0
        Else
            Session("DecimalPlaces") = 2
        End If

    End Sub
    Protected Sub gridDrillDown_DataBinding(sender As Object, e As EventArgs)

        Dim DecimalPlaces As Integer = Session("DecimalPlaces")

        Call SetDecimalPlacesLocal(sender.Columns("TotalCostValue"), DecimalPlaces, True)
        sender.TotalSummary("TotalCostValue").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(sender.Columns("UnitSaleValue"), DecimalPlaces, True)

        Call SetDecimalPlacesLocal(sender.Columns("TotalSaleValue"), DecimalPlaces, True)

        Call SetDecimalPlacesLocal(sender.Columns("RebatePrice"), DecimalPlaces, True)

        If sender.id <> "gridDrillDown2" Then
            Call SetDecimalPlacesLocal(sender.Columns("ActualRebate"), DecimalPlaces, True)
            sender.TotalSummary("ActualRebate").DisplayFormat = "C" & DecimalPlaces
        End If

        Call SetDecimalPlacesLocal(sender.Columns("nettprofit"), DecimalPlaces, True)
        sender.TotalSummary("nettprofit").DisplayFormat = "C" & DecimalPlaces


    End Sub

    Sub SetDecimalPlacesLocal(thiscolumn As GridViewDataTextColumn, DecimalPlaces As Integer, AlterSummary As Boolean)

        Dim localColumn As GridViewDataTextColumn = TryCast(thiscolumn, GridViewDataTextColumn)
        Dim totalSummary As ASPxSummaryItem = New ASPxSummaryItem()
        Dim sLocalFieldName As String = localColumn.FieldName

        localColumn.PropertiesTextEdit.DisplayFormatString = "C" & DecimalPlaces


        '  Cannot get this to work 
        'If AlterSummary Then
        '    totalSummary.FieldName = sLocalFieldName
        '    totalSummary.ShowInColumn = sLocalFieldName
        '    totalSummary.DisplayFormat = "C" & DecimalPlaces
        'End If

    End Sub
    Protected Sub ASPxGridViewExporter1_RenderBrick(sender As Object, e As ASPxGridViewExportRenderingEventArgs)
        Call GlobalRenderBrick(e)
    End Sub
    Protected Sub pageControlDetail_Load(sender As Object, e As EventArgs)
        If Session("ShowOtherParts") = 1 Then
            sender.tabpages(2).visible = True
        Else
            sender.tabpages(2).visible = False
        End If
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub



End Class
