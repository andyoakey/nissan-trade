﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraCharts

Partial Class report5

    Inherits System.Web.UI.Page

    Protected Sub dsCampaignSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaignSummary.Init
        dsCampaignSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Campaigns"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Campaigns", "Campaigns")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        '    If Session("ExcelClicked") = True Then
        '        Session("ExcelClicked") = False
        '        Call btnExcel_Click()
        '    End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = False

        'Dim nButtonHeight As Integer = Session("ButtonHeight")
        Dim nButtonWidth As Integer = 100

        'btn1.Image.Height = nButtonHeight
        btn1.Image.Width = nButtonWidth
        'btn2.Image.Height = nButtonHeight
        'btn2.Image.Width = nButtonWidth
        'btn3.Image.Height = nButtonHeight
        'btn3.Image.Width = nButtonWidth
        'btn4.Image.Height = nButtonHeight
        'btn4.Image.Width = nButtonWidth

    End Sub

    Protected Sub IconButtons_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn1.Click      ', btn2.Click, btn3.Click, btn4.Click

        Select Case sender.id
            Case "btn1"
                Response.Redirect("TradePriceSupport.aspx")
                'Case "btn2"
                '    Response.Redirect("Campaign201502C2.aspx")
                'Case "btn3"
                '    Response.Redirect("Campaign201603Clutch.aspx")
                'Case "btn4"
                '    Response.Redirect("MCChall.aspx")
        End Select

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "CampaignSummary_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    'Protected Sub gridSummary_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridSummary.CustomColumnDisplayText

    '    If Left(e.Column.FieldName, 2) = "Q2" And Session("CurrentMonth") < 3 Then
    '        e.DisplayText = "-"
    '    End If

    '    If Left(e.Column.FieldName, 2) = "Q3" And Session("CurrentMonth") < 6 Then
    '        e.DisplayText = "-"
    '    End If

    '    If Left(e.Column.FieldName, 2) = "Q4" And Session("CurrentMonth") < 9 Then
    '        e.DisplayText = "-"
    '    End If

    'End Sub

End Class