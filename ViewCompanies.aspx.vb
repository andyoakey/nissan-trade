﻿
Imports System.Data
Imports DevExpress.Web
Imports System.Collections.Generic
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewCompanies

    Inherits System.Web.UI.Page

    Private Const GridCustomPageSizeName As String = "gridCustomPageSize"

    Dim nBand1 As Integer
    Dim nBand2 As Integer

    Dim dsCompanies As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""




    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim i As Integer
        Dim sSelectedPostCode As String = ""
        Dim nSelectedCustomerType As Integer = 0

        Me.Title = "Nissan Trade Website - View all Customers"
        Session("CustomerCalledFrom") = "ViewCompanies"

        Session("CallingModule") = "ViewCompanies.aspx - LoadComplete"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        '        If Session("SuperUser") = "Y" Then      ' Restriction removed as Per Marks email 22/01/18
        btnDup.Visible = True
        gridCompanies.Columns("BusinessTypeDescriptionEdit").Visible = True
        gridCompanies.Columns("BusinessTypeDescriptionView").Visible = False
        '       Else
        '      btnDup.Visible = False
        '     gridCompanies.Columns("BusinessTypeDescriptionEdit").Visible = False
        '    gridCompanies.Columns("BusinessTypeDescriptionView").Visible = True
        '   End If

        gridCompanies.SettingsPager.AllButton.Visible = (Session("SelectionLevel") <> "N")

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer List", "Viewing Customer List")
            Call ResetFilters()
            Session("FocusType") = 0
        End If

        If IsPostBack = False And Session("C_BusinessName") <> "9999999" Then
            ' If its set to "9999999" then the page has been called from the menu and all Session vars pertaining to the filters should be ignored
            ' _______________________________________________________________
            '  Move the contents of the filters set up in CustomerConsent.aspx.vb to this page
            Dim sBT As String = ""
            txtFiltBusinessName.Text = Session("C_BusinessName")
            txtFiltPostCode.Text = Session("C_Postcode")
            sBT = Session("C_BusinessTypeString")
            chkFilterCustType.Items(0).Selected = Session("C_Active")
            chkFilterCustType.Items(1).Selected = Session("C_InActive")
            chkFilterCustType.Items(2).Selected = Session("C_Lapsed")
            chkFilterCustType.Items(3).Selected = Session("C_Prospect")
            chkFilterCustType.Items(4).Selected = Session("C_NewProspect")
            If sBT Like ("'1'") Then
                chkFilterBusType.Items(0).Selected = True
            End If
            If sBT Like ("'2'") Then
                chkFilterBusType.Items(1).Selected = True
            End If
            If sBT Like ("'3'") Then
                chkFilterBusType.Items(2).Selected = True
            End If
            If sBT Like ("'4'") Then
                chkFilterBusType.Items(3).Selected = True
            End If
            If sBT Like ("'5'") Then
                chkFilterBusType.Items(4).Selected = True
            End If
            If sBT Like ("'6'") Then
                chkFilterBusType.Items(5).Selected = True
            End If
            If sBT Like ("'7'") Then
                chkFilterBusType.Items(6).Selected = True
            End If
            If sBT Like ("'8'") Then
                chkFilterBusType.Items(7).Selected = True
            End If
            If sBT Like ("'9'") Then
                chkFilterBusType.Items(8).Selected = True
            End If
            If sBT Like ("'99'") Then
                chkFilterBusType.Items(9).Selected = True
            End If
            '  End
            ' _______________________________________________________________
        End If

        If Request.QueryString.Count > 0 Then
            sSelectedPostCode = Request.QueryString(0)
            txtFiltPostCode.Text = sSelectedPostCode
        End If

        If Request.QueryString.Count = 2 Then
            For i = 0 To chkFilterCustType.Items.Count - 1
                chkFilterCustType.Items(i).Selected = False
            Next
            For i = 0 To chkFilterBusType.Items.Count - 1
                chkFilterBusType.Items(i).Selected = True
            Next
            chkFilterBusType.Items(5).Selected = False
            chkFilterBusType.Items(6).Selected = False
            Select Case Request.QueryString(1)
                Case 1  '/ Known
                    chkFilterCustType.Items(0).Selected = True
                    chkFilterCustType.Items(1).Selected = True
                    chkFilterCustType.Items(2).Selected = True
                    chkFilterCustType.Items(3).Selected = True
                Case 2  '/ Active
                    chkFilterCustType.Items(0).Selected = True
                Case 3  '/ Inactive
                    chkFilterCustType.Items(1).Selected = True
                Case 4  '/ Lapsed
                    chkFilterCustType.Items(2).Selected = True
                Case 5 '/ Prospect
                    chkFilterCustType.Items(3).Selected = True
                Case 6 '/ New Prospect
                    chkFilterCustType.Items(4).Selected = True
                Case Else

            End Select

        End If

        If isQubeUser(Session("UserEmailAddress")) Then
            btnConsent.Visible = True
            btnMM.Visible = True
        Else
            btnMM.Visible = Session("SelectionLevel") = "C"
            btnConsent.Visible = btnMM.Visible
        End If

        Call BindGrid()

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True


        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub gridCompanies_DetailRowGetButtonVisibility(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewDetailRowButtonEventArgs)
        If (e.VisibleIndex > -1) Then
            Dim gv As ASPxGridView = CType(sender, ASPxGridView)
            If gv.GetRowValues(e.VisibleIndex, "CustomerStatus") = "New Prospect" Then
                e.ButtonState = GridViewDetailRowButtonState.Hidden
            End If
        End If
    End Sub

    Protected Sub gridAccounts_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("CustomerId") = Trim(Left(CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue(), 10))
        gridCompanies.SettingsText.Title = Session("Title")
        Dim gv As ASPxGridView = CType(sender, ASPxGridView)
        gv.Columns(0).Visible = Session("UserEmailAddress").ToString().Contains("@qubedata.")
    End Sub

    Protected Sub dsLinks_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLinks.Init
        dsLinks.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridCompanies_CommandButtonInitialize(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewCommandButtonEventArgs) Handles gridCompanies.CommandButtonInitialize

        Dim sHidden As String
        If e.ButtonType = ColumnCommandButtonType.SelectCheckbox Then
            If (e.VisibleIndex > -1) Then
                Dim gv2 As ASPxGridView = CType(sender, ASPxGridView)
                sHidden = gv2.GetRowValues(e.VisibleIndex, "Hidden")
                If sHidden = "N" Then
                    e.Visible = True
                Else
                    e.Visible = False
                End If
            End If
        End If

    End Sub

    Protected Sub gridCompanies_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridCompanies.CustomColumnDisplayText

        Dim nDriveTime As Integer

        If e.Column.FieldName = "Distance" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                nDriveTime = IIf(IsDBNull(e.Value), 0, e.Value)
                If nDriveTime < 1 Then
                    e.DisplayText = "<1 mile"
                Else
                    e.DisplayText = Str(CInt(nDriveTime)) & " miles"
                End If
            End If
        End If

        If e.Column.FieldName = "DriveTime" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = FormatTime(e.Value)
            End If
        End If

        If e.Column.FieldName = "CustomerStatus" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = ShortCustomerType(e.Value)
            End If
        End If

    End Sub

    Protected Sub gridCompanies_HeaderFilterFillItems(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewHeaderFilterEventArgs) Handles gridCompanies.HeaderFilterFillItems

        Dim sStr As String

        If e.Column.FieldName = "Distance" Then
            e.Values.Clear()
            e.AddShowAll()
            sStr = "<=" & Trim(Str(nBand1)) & " miles"
            e.AddValue(sStr, "", "[Distance]>0 AND [Distance]<=" & Trim(Str(nBand1)))
            sStr = "Between " & Trim(Str(nBand1)) & " miles and " & Trim(Str(nBand2)) & " miles"
            e.AddValue(sStr, "", "[Distance]>" & Trim(Str(nBand1) & " AND [Distance]<=" & Trim(Str(nBand2))))
            sStr = ">" & Trim(Str(nBand2)) & " miles"
            e.AddValue(sStr, "", "[Distance]>" & Trim(Str(nBand2)))
        End If

        If e.Column.FieldName = "DriveTime" Then
            e.Values.Clear()
            e.AddShowAll()
            e.AddValue("<15m", "", "[DriveTime]>0 AND [DriveTime]<=15")
            e.AddValue("15-30m", "", "[DriveTime]>15 AND [DriveTime]<=30")
            e.AddValue("30m-1hr", "", "[DriveTime]>30 AND [DriveTime]<=60")
            e.AddValue(">1hr", "", "[DriveTime]>60")
        End If

    End Sub

    Protected Sub btnDup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDup.Click

        Dim selectedValues As List(Of Object)
        Dim i As Integer
        Dim fieldNames As List(Of String) = New List(Of String)()
        Dim sStr As String = ""

        fieldNames.Add("Id")
        selectedValues = gridCompanies.GetSelectedFieldValues(fieldNames.ToArray())

        If selectedValues.Count = 0 Then
            Exit Sub
        End If

        For i = 0 To selectedValues.Count - 1
                sStr = sStr & selectedValues.Item(i).ToString & ","
            Next
            sStr = Left(sStr, Len(sStr) - 1)

        Session("DeDupeIDs") = sStr
        Response.Redirect("CustomerDeDupe.aspx")

    End Sub

    Protected Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHide.Click

        Dim selectedValues As List(Of Object)
        Dim i As Integer
        Dim fieldNames As List(Of String) = New List(Of String)()
        Dim sStr As String = ""

        fieldNames.Add("Id")
        selectedValues = gridCompanies.GetSelectedFieldValues(fieldNames.ToArray())
        If selectedValues.Count > 0 Then
            For i = 0 To selectedValues.Count - 1
                sStr = sStr & selectedValues.Item(i).ToString & ","
            Next
            sStr = Left(sStr, Len(sStr) - 1)
            Session("DeDupeIDs") = sStr
            Response.Redirect("CustomerHide.aspx")
        End If

    End Sub

    Private Function FormatTime(ByVal nMins As Long) As String

        Dim sTime As String = ""
        Dim nHours As Integer = 0

        If nMins > 60 Then
            nHours = Convert.ToInt32(Math.Floor(nMins / 60))
            nMins = nMins - (nHours * 60)
            sTime = Str(nHours) & "h " & Str(nMins) & "m"
        Else
            sTime = Str(CInt(nMins)) & "m"
        End If
        FormatTime = sTime

    End Function

    Private Function ShortBusinessType(ByVal sBT As String) As String
        Dim sShort As String
        Select Case Trim(sBT)
            Case "Bodyshop"
                sShort = "Bodyshop"
            Case "Breakdown Recovery Services"
                sShort = "Breakdown"
            Case "Car Dealer New/Used"
                sShort = "Dealer "
            Case "Independent Motor Trader"
                sShort = "IMT"
            Case "Mechanical Specialist"
                sShort = "Mech Spec"
            Case "Motor Factor"
                sShort = "Factor"
            Case "Parts Supplier"
                sShort = "Part Supp"
            Case "Taxi & Private Hire"
                sShort = "Taxi P/H"
            Case "Windscreens"
                sShort = "Windscreen"
            Case Else
                sShort = "Other"
        End Select
        ShortBusinessType = sShort
    End Function

    Private Function ShortCustomerType(ByVal sCT As String) As String

        Dim sShort As String

        sShort = sCT
        'Select Case Trim(sCT)


        '    Case "Active"
        '        sShort = "ACTV"
        '    Case "In-active"
        '        sShort = "INAC"
        '    Case "Lapsed"
        '        sShort = "LAPS"
        '    Case "Prospect"
        '        sShort = "PROS"
        '    Case ("Competitor")
        '        sShort = "COMPET"
        '    Case Else
        '        sShort = "NEW"
        'End Select

        ShortCustomerType = sShort

    End Function

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisValue As Decimal = 0
        Dim nDistance As VariantType
        Dim bNull As Boolean = False

        Call GlobalRenderBrick(e)

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            e.Url = ""
            e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)

            Try
                If Not IsDBNull(e.GetValue("BusinessURN")) Then
                    If Trim(e.GetValue("BusinessURN")) <> "" Then
                        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Bold)
                    End If
                End If
            Catch ex As Exception
            End Try

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Name = "DriveTime" Or e.Column.Name = "Distance" Then

                If IsDBNull(e.GetValue("Distance")) Then
                    nDistance = 0
                    bNull = True
                Else
                    nDistance = Convert.ToInt32(Math.Floor(e.GetValue("Distance")))
                End If

                If Not bNull Then
                    If nDistance < nBand1 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.YellowGreen
                    ElseIf nDistance < nBand2 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.Gold
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                        e.BrickStyle.ForeColor = System.Drawing.Color.White
                    End If
                End If
            End If

        End If

    End Sub

    Protected Sub gridCompanies_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompanies.HtmlDataCellPrepared

        Dim nDistance As VariantType
        Dim bNull As Boolean = False
        Dim sVal As String = ""

        If e.DataColumn.FieldName = "DriveTime" Or e.DataColumn.FieldName = "Distance" Then

            If IsDBNull(e.GetValue("Distance")) Then
                nDistance = 0
                bNull = True
            Else
                nDistance = Convert.ToInt32(Math.Floor(e.GetValue("Distance")))
            End If

            If Not bNull Then
                If nDistance < nBand1 Then
                    e.Cell.BackColor = System.Drawing.Color.YellowGreen
                ElseIf nDistance < nBand2 Then
                    e.Cell.BackColor = System.Drawing.Color.Gold
                Else
                    e.Cell.BackColor = System.Drawing.Color.Tomato
                    e.Cell.ForeColor = System.Drawing.Color.White
                End If
            End If

        End If


        If e.DataColumn.FieldName = "Email_Status" Then
            If IsDBNull(e.GetValue("Email_Status")) Then
                sVal = " "
            Else
                sVal = e.GetValue("Email_Status")
            End If

            If sVal = "Rejected" Or sVal = "Blank" Then
                e.Cell.BackColor = System.Drawing.Color.Red
                e.Cell.ForeColor = System.Drawing.Color.White
            End If

            If sVal = "Valid" Or sVal = "Valid Mailed" Then
                e.Cell.BackColor = System.Drawing.Color.Green
                e.Cell.ForeColor = System.Drawing.Color.White
            End If

            If sVal = "Valid Unsubscribed" Or sVal = "Do Not Mail" Then
                e.Cell.BackColor = System.Drawing.Color.Gold
            End If
        End If

        'If e.GetValue("BusinessURN") <> "" Then
        'e.Cell.Font.Bold = True
        'Else
        'e.Cell.Font.Bold = False
        'End If

        If e.DataColumn.Index > 0 Then
            If e.GetValue("Hidden") = "Y" Then
                e.Cell.Font.Italic = True
                e.Cell.Font.Strikeout = True
            Else
                e.Cell.Font.Italic = False
                e.Cell.Font.Strikeout = False
            End If
        End If

    End Sub

    Private Function HiddenCount() As Integer

        Dim da As New DatabaseAccess
        Dim ds As DataSet
        Dim sErr As String = ""
        Dim htIn As New Hashtable

        htIn.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn.Add("@sSelectionId", Session("SelectionId"))
        ds = da.Read(sErr, "p_CustomerHiddenCount", htIn)

        HiddenCount = 0
        If sErr.Length = 0 Then
            HiddenCount = ds.Tables(0).Rows(0).Item("HiddenCount")
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Function

    Protected Sub btnExcel_Click()

        Dim nCols As Integer
        Dim nRows As Integer
        Dim sFileName As String = ""

        gridCompanies.DataBind()

        nCols = gridCompanies.Columns.Count - 1
        nRows = gridCompanies.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            'gridCompanies.Columns(0).Visible = False
            'gridCompanies.Columns(1).Visible = True
            'gridCompanies.Columns(14).Visible = True
            'gridCompanies.Columns(15).Visible = True
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
            'gridCompanies.Columns(0).Visible = True
            'gridCompanies.Columns(1).Visible = False
            'gridCompanies.Columns(14).Visible = False
            'gridCompanies.Columns(15).Visible = False
            For i = 0 To gridCompanies.Columns.Count - 1
                gridCompanies.Columns(i).VisibleIndex = i
            Next

        End If
    End Sub

    Protected Sub BusinessType_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sBusinessURN As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "BusinessURN").ToString()
        Dim sCustId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sBTypeId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "BusinessTypeId").ToString()
        Dim sBTypeDesc As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "BusinessTypeDescription").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}&BType={2}", "ViewCompaniesBT.aspx", sCustId, sBTypeId)

        link.Text = ShortBusinessType(sBTypeDesc)
        If sBusinessURN = "" Then
            link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewCompaniesBT('{0}'); }}", sContentUrl)
            link.ToolTip = "Click here to update the business type."
            link.ForeColor = System.Drawing.Color.Black
            link.ForeColor = System.Drawing.Color.DarkBlue

            '            link.Font.Underline = True
        Else



            link.Font.Underline = False
            link.ForeColor = System.Drawing.Color.Gray
            ' link.ToolTip = "Business types for Experian records cannot be updated."
            link.ToolTip = ""


        End If

    End Sub

    Protected Sub Focus_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sFocusType As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "focustype").ToString()
        Dim sCustId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sDealerCode As String = Trim(Session("SelectionID"))
        Dim sContentUrl As String = String.Format("{0}?Id={1}&sDealerCode ={2}&sFocusType ={3}", "ViewCompaniesFocusType.aspx", sCustId, sDealerCode, sFocusType)

        link.Text = sFocusType
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewCompaniesFocusType('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to update the Focus Type."
        link.ForeColor = System.Drawing.Color.Black
        '        link.Font.Underline = True
        link.ForeColor = System.Drawing.Color.DarkBlue

    End Sub

    Protected Sub Email_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sCustId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sEmailAddress As String = Trim(templateContainer.Grid.GetRowValues(nRowVisibleIndex, "EmailAddress").ToString())
        Dim sContentUrl As String = String.Format("{0}?Id={1}&BType={2}", "ViewCompaniesEmail.aspx", sCustId, sEmailAddress)

        If sEmailAddress = "" Then
            link.Text = "<none>"
            link.ToolTip = "Click here to add a New Email Address."
        Else
            link.Text = sEmailAddress
            link.ToolTip = "Click here to update the Existing Email Address."
        End If
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewCompaniesEmail('{0}'); }}", sContentUrl)
        link.ForeColor = System.Drawing.Color.DarkBlue



    End Sub



    Protected Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        PopFilter.ShowOnPageLoad = True
    End Sub

    Private Sub ResetFilters()

        Dim i As Integer

        Session("C_BusinessName") = ""
        Session("C_PostCode") = ""
        Session("C_BusinessType") = ""
        Session("C_Active") = 1
        Session("C_InActive") = 0
        Session("C_Lapsed") = 0
        Session("C_Prospect") = 0
        Session("C_NewProspect") = 0
        Session("C_CustomerType") = 0
        Session("C_ShowAddress") = 0
        Session("C_ShowHidden") = 0
        Session("C_FocusType") = 0

        txtFiltBusinessName.Text = ""
        txtFiltPostCode.Text = ""
        chkFilterCustType.Items(0).Selected = True
        chkFilterCustType.Items(1).Selected = False
        chkFilterCustType.Items(2).Selected = False
        chkFilterCustType.Items(3).Selected = False
        chkFilterCustType.Items(4).Selected = False

        For i = 0 To 9
            chkFilterBusType.Items(i).Selected = False
        Next

        ddlType.SelectedIndex = 0
        chkShowAddress.Checked = False
        chkShowHidden.Checked = False

        gridCompanies.FilterEnabled = False
        gridCompanies.FilterExpression = ""

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        'Call ResetFilters()
        'PopFilter.ShowOnPageLoad = False
        Response.Redirect("ViewCompanies.aspx")
    End Sub

    Private Sub BindGrid()

        Dim i As Integer
        Dim sBT As String = ""
        Dim nHiddenCount As Integer

        sBT = ""
        For i = 0 To 9
            If chkFilterBusType.Items(i).Selected Then
                sBT += "'" & chkFilterBusType.Items(i).Value & "',"
            End If
        Next
        sBT = Trim(sBT)
        If Right(sBT, 1) = "," Then
            sBT = Left(sBT, Len(sBT) - 1)
        End If

        Dim htin As New Hashtable
        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@sBusinessName", Trim(txtFiltBusinessName.Text))
        htin.Add("@sPostCode", Trim(txtFiltPostCode.Text))
        htin.Add("@sBusinessTypeString", sBT)
        htin.Add("@nActive", IIf(chkFilterCustType.Items(0).Selected, 1, 0))
        htin.Add("@nInActive", IIf(chkFilterCustType.Items(1).Selected, 1, 0))
        htin.Add("@nLapsed", IIf(chkFilterCustType.Items(2).Selected, 1, 0))
        htin.Add("@nProspect", IIf(chkFilterCustType.Items(3).Selected, 1, 0))
        htin.Add("@nNewProspect", IIf(chkFilterCustType.Items(4).Selected, 1, 0))
        htin.Add("@nFocus", Session("FocusType"))
        htin.Add("@nDetailed", IIf(chkShowAddress.Checked, 1, 0))
        htin.Add("@nShowHidden", IIf(chkShowHidden.Checked, 1, 0))
        dsCompanies = da.Read(sErrorMessage, "p_CustomerListAll_2018", htin)

        Session("C_BusinessName") = Trim(txtFiltBusinessName.Text)
        Session("C_Postcode") = Trim(txtFiltPostCode.Text)
        Session("C_BusinessTypeString") = sBT
        Session("C_Active") = IIf(chkFilterCustType.Items(0).Selected, 1, 0)
        Session("C_InActive") = IIf(chkFilterCustType.Items(1).Selected, 1, 0)
        Session("C_Lapsed") = IIf(chkFilterCustType.Items(2).Selected, 1, 0)
        Session("C_Prospect") = IIf(chkFilterCustType.Items(3).Selected, 1, 0)
        Session("C_NewProspect") = IIf(chkFilterCustType.Items(4).Selected, 1, 0)
        Session("C_Detailed") = IIf(chkShowAddress.Checked, 1, 0)
        Session("C_ShowHidden") = IIf(chkShowHidden.Checked, 1, 0)


        gridCompanies.DataSource = dsCompanies.Tables(0)
        gridCompanies.DataBind()

        If Not Session("CompanyReturnKeyValue") Is Nothing Then
            gridCompanies.MakeRowVisible(Session("CompanyReturnKeyValue"))
            Session("CompanyReturnKeyValue") = Nothing
        End If

        If Session("SelectionLevel") = "C" Then
            nBand1 = GetDataLong("SELECT MileageBand1 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
            nBand2 = GetDataLong("SELECT MileageBand2 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
        Else
            nBand1 = 10
            nBand2 = 20
        End If

        gridCompanies.Columns(3).Visible = (chkShowAddress.Checked)

        'gridCompanies.Columns(8).Visible = (Session("SelectionLevel") = "C")
        'gridCompanies.Columns(9).Visible = (Session("SelectionLevel") = "C")
        'gridCompanies.Columns(10).Visible = (Session("SelectionLevel") = "C")

        If Session("UserLevel") <> "T" And Session("UserLevel") <> "N" Then
            btnDup.Enabled = False
        End If
        If Session("SelectionLevel") <> "C" Then
            btnHide.Visible = False
        End If

        nHiddenCount = HiddenCount()
        If nHiddenCount = 0 Then
            chkShowHidden.Enabled = False
        Else
            chkShowHidden.Enabled = True
            chkShowHidden.Text = "Include (" & Trim(Str(nHiddenCount)) & ") hidden"
        End If

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        '/ The Page_Load_Complete will refresh the grid
        PopFilter.ShowOnPageLoad = False
    End Sub

    Protected Sub btnMM_Click(sender As Object, e As EventArgs) Handles btnMM.Click
        Response.Redirect("MM.aspx")
    End Sub

    Protected Sub ReAlloc_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sCustomerDealerId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "CustomerDealerId").ToString()
        Dim sContentUrl As String = String.Format("{0}?id=0{1}", "ViewCompaniesRealloc.aspx", sCustomerDealerId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/realloc.jpg"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ReAllocWindow('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to choose a different Customer record."

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        Session("FocusType") = ddlType.Value
    End Sub

    Private Sub ViewCompanies_Load(sender As Object, e As EventArgs) Handles Me.Load
        Session("CallingModule") = "ViewCompanies.aspx - Load"



    End Sub

    Private Sub btnConsent_Click(sender As Object, e As EventArgs) Handles btnConsent.Click
        Response.Redirect("CustomerConsent.aspx")
    End Sub
End Class
