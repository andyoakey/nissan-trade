﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CentreDetails.aspx.vb" Inherits="CentreDetails" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
    
    <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                runat="server" 
                Text="Dealer Details" 
                Font-Size="Larger" />
        </div>
         <br />
        
          <dx:ASPxGridView 
                       ID="gridCentres" 
                       runat="server" 
                       OnLoad="GridStyles" 
                       DataSourceID="dsCentres" 
                       AutoGenerateColumns="False" 
                       KeyFieldName="DealerCode" 
                       Width="100%" >
            
                    <Settings 
                        ShowHeaderFilterButton="True" 
                        UseFixedTableLayout="True" 
                        ShowFilterRow="False" />

            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16" />

            <SettingsPopup EditForm-ShowHeader="true"  EditForm-Width="750px"/>

            <SettingsEditing  
                Mode="PopupEditForm" 
                EditFormColumnCount="1" />
    
                <Columns>
            
                <dx:GridViewCommandColumn  
                    Width = "3%"
                    Caption=" " 
                    ShowNewButton="false"
                    ShowDeleteButton="false"
                    ShowEditButton="true"
                    VisibleIndex="0" 
                    ButtonType="Link" >
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dx:GridViewCommandColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="DealerCode" 
                    Caption="Dealer" 
                    VisibleIndex="0" 
                    ReadOnly="true" 
                    ExportWidth="150"
                    PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"  
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "0" Width="10%">
                    <PropertiesTextEdit Width="120px">
                    <ReadOnlyStyle BackColor="LightGray"></ReadOnlyStyle>
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="True" 
                        ShowFilterRowMenu="False" />
                    <EditFormSettings Visible="True" VisibleIndex="0"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="CentreName"
                    Caption="Dealer Name" 
                    ExportWidth="250"
                    Width="15%"  
                    VisibleIndex="1" 
                    EditFormSettings-Visible="True"> 
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" VisibleIndex="3" ColumnSpan="2"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left"/>
                </dx:GridViewDataTextColumn>
                
         <dx:GridViewDataComboBoxColumn 
                    Caption="Zone" 
                    FieldName="Zone" 
                    ExportWidth="150"
                    VisibleIndex="2" 
                    Width="10%"  
                    EditFormSettings-VisibleIndex="1"
                    PropertiesComboBox-DropDownWidth="120px" >
                    <PropertiesComboBox DataSourceID="dsZones" TextField="Zone"
                        ValueType="System.String" EnableCallbackMode="True" EnableClientSideAPI="True">
                        <ValidationSettings CausesValidation="True" EnableCustomValidation="True">
                        </ValidationSettings>
                    </PropertiesComboBox>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="True" 
                        ShowFilterRowMenu="False" />
                    <EditCellStyle ></EditCellStyle>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataComboBoxColumn>

                <dx:GridViewDataComboBoxColumn 
                    Caption="TPM" 
                    FieldName="TPSMName" 
                    VisibleIndex="3" 
                    ExportWidth="150"
                    Width="15%"  
                    EditFormSettings-VisibleIndex="2" 
                    PropertiesComboBox-DropDownWidth="120px" 
                    Visible="True">
                    <PropertiesComboBox DataSourceID="dsTPSMs" TextField="TPSM"
                        ValueType="System.String" EnableCallbackMode="True" EnableClientSideAPI="True">
                        <ValidationSettings CausesValidation="True" EnableCustomValidation="True">
                        </ValidationSettings>
                    </PropertiesComboBox>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="True" 
                        ShowFilterRowMenu="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left"/>
                </dx:GridViewDataComboBoxColumn>
                
                        <dx:GridViewDataTextColumn 
                    FieldName="MarketingName"
                    ExportWidth="250"
                    Name="Name used on Marketing" 
                    Width="15%"  
                    VisibleIndex="4" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "4" Visible="False">
                    <EditFormSettings Visible="True" VisibleIndex="4"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
   
                                <dx:GridViewDataTextColumn 
                    FieldName="MarketAreaDescription"
                    Name="Market Area Desc" 
                    ExportWidth="250"
                    Width="15%"  
                    VisibleIndex="4" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "5" Visible="False">
                    <EditFormSettings Visible="True" VisibleIndex="5"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
          
                <dx:GridViewDataTextColumn 
                    FieldName="TelephoneNumber"
                    ExportWidth="150"
                    Name="Direct Trade Telephone Number" 
                    Width="15%"  
                    VisibleIndex="4" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "4" >
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="True" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="FaxNumber"
                    Name="Fax Number" 
                    ExportWidth="150"
                    VisibleIndex="5" 
                    Width="15%"  
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "5" Visible="False">
                    <EditFormSettings Visible="True" VisibleIndex="5"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Contact"
                    Name="Contact" 
                    Width="15%"  
                    ExportWidth="250"
                    VisibleIndex="5"  
                    Visible="false" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "13" >
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" VisibleIndex="13"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
         
                    <dx:GridViewDataTextColumn 
                    FieldName="Position"
                    Name="Position" 
                    ExportWidth="250"
                        Width="15%"  
                    VisibleIndex="6" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "15" Visible="False">
                    <EditFormSettings Visible="True" VisibleIndex="15"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Email"
                    ExportWidth="250"
                    Name="Email Address" 
                    VisibleIndex="6" 
                    Width="20%"  
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "6">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" VisibleIndex="6"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Address1"
                    Name="Address 1" 
                    ExportWidth="250"
                    VisibleIndex="11" 
                    Width="15%"  
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "7">
                    <EditFormSettings Visible="True" VisibleIndex="7"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Address2"
                    Name="Address 2" 
                    ExportWidth="250"
                    VisibleIndex="12" 
                    Width="15%"  
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "8">
                    <EditFormSettings Visible="True" VisibleIndex="8"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Address3"
                    Name="Address3" 
                    ExportWidth="250"
                    VisibleIndex="13" 
                    Width="15%"  
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "9">
                    <EditFormSettings Visible="True" VisibleIndex="9"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Address4"
                    Name="Address4" 
                    ExportWidth="250"
                    VisibleIndex="14" 
                    Width="15%"  
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "10">
                    <EditFormSettings Visible="True" VisibleIndex="10"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="Postcode"
                    ExportWidth="150"
                    Name="Postcode" 
                    VisibleIndex="15" 
                    Width="15%"  
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "11">
                    <EditFormSettings Visible="True" VisibleIndex="11"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
           <dx:GridViewDataTextColumn 
                    FieldName="MileageBand1"
                    Name="Mileage Band 1" 
                    VisibleIndex="16" 
                    Visible="false"
                    ExportWidth="150"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "16" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="16"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="MileageBand2"
                    Name="Mileage Band 2" 
                    VisibleIndex="17" 
                    ExportWidth="150"
                    Visible="false"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "17" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="17"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Userfield1"
                    Name="User Defined 1" 
                    VisibleIndex="18" 
                    Visible="false"
                    ExportWidth="250"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "18" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="18"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Userfield2"
                    Name="User Defined 2" 
                    VisibleIndex="19" 
                    Visible="false"
                    ExportWidth="250"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "19" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="19"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Userfield3"
                    Name="User Defined 3" 
                    VisibleIndex="20" 
                    Visible="false"
                    ExportWidth="250"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "20" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="20"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Userfield4"
                    Name="User Defined 4" 
                    VisibleIndex="21" 
                    Visible="false"
                    ExportWidth="250"
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "21" Width="10%">
                    <EditFormSettings Visible="True" VisibleIndex="21"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

            </Columns>
           
        </dx:ASPxGridView>
    
          <br />

    </div>

    <asp:SqlDataSource 
        ID="dsCentres" 
        runat="server" 
        SelectCommand="p_CentreList" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsZones" 
        runat="server" 
        SelectCommand="p_ZoneList" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>       
    
    <asp:SqlDataSource 
        ID="dsTPSMs" 
        runat="server" 
        SelectCommand="p_TPSMList" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>       

    <dx:ASPxGridViewExporter 
        ID="ExpGridDealer"
        runat="server" 
        GridViewID="gridCentres">
    </dx:ASPxGridViewExporter>

</asp:Content>

