﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting


Partial Class HOPointsPromotion

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Head Office Points Promotion "
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Not IsPostBack Then
            Session("FocusType") = 0

            ' Very strange bug thats causes it to time out first time in 
            ' Setting these to 0 tells the SP to use last month whch seems to work fine
            Session("MonthFrom") = 0
            Session("MonthTo") = 0
            Call GetMonthsDevX(ddlFrom)
            ddlFrom.SelectedIndex = 1
            Call GetMonthsDevX(ddlTo)
            ddlTo.SelectedIndex = 1
        End If

    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Call GlobalRenderBrick(e)


    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String

        gridPointsPromotion.Columns("bandDataSubmission").Visible = True
        gridPointsPromotion.Columns("bandOutbound").Visible = True

        sFileName = "HOPointsPromotion"
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        gridPointsPromotion.Columns("bandDataSubmission").Visible = False
        gridPointsPromotion.Columns("bandOutbound").Visible = True

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub GridStylesFalse(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

End Class
