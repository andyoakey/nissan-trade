﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CoreStockReport.aspx.vb" Inherits="CoreStockReport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
 
<div style="position: relative; font-family: Calibri; text-align: left;" >
    
    <div id="divTitle" style="position:relative; left:0px">
           <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Core Stock Reporting" />
    </div>
   
    <table width="100%" style="margin-bottom: 10px;">
        <tr >
            <td width ="10%">
               <dx:ASPxLabel     
        ID="lblFrom" 
        text="Month From"
        runat="server" />
            </td>

            <td width ="10%">
               <dx:ASPxLabel     
        ID="lblTo" 
        text="Month To"
        runat="server" 
        width= "100%" />
            </td>

            <td width ="10%">
               <dx:ASPxLabel     
        ID="lblReportType" 
        text="Report Type"
        runat="server" 
        width= "100%" />
            </td>

            <td width ="10%">
               <dx:ASPxLabel     
        ID="lblDealers" 
        text="Which Dealers ?"
        runat="server" 
        width= "100%" />
            </td>

             <td width ="10%">
             </td>

             <td width ="50%" >
             </td>
          </tr >
       
        <tr >
            <td width ="10%">
               <dx:ASPxComboBox 
        ID="ddlSelectFrom" 
        runat="server" 
        AutoPostBack="True" 
        DataSourceID="dsMonth" 
        TextField="periodname" 
        ValueField="id" 
        Width="100%">
      </dx:ASPxComboBox>
             </td>
        
            <td width ="10%">
               <dx:ASPxComboBox 
        ID="ddlSelectTo" 
        runat="server" 
        AutoPostBack="True" 
        DataSourceID="dsMonth" 
        TextField="periodname" 
        ValueField="id" 
        Width="200px">
      </dx:ASPxComboBox>
             </td>
         
            <td width ="10%">
               <dx:ASPxComboBox 
        ID="ddlReportType" 
        runat="server" 
        AutoPostBack="True" 
        SelectedIndex="0" 
        TextField="periodname" 
        ValueField="id" 
        ValueType="System.Int32"
        Width="100%">
       <Items>
           <dx:ListEditItem  Text="By Dealer"  Value="1" Selected ="true"/>
            <dx:ListEditItem Text="By Customer" Value="2"/>
            <dx:ListEditItem Text="By Product Group" Value="3"/>
            <dx:ListEditItem Text="By Part" Value="4"/>
       </Items>
      </dx:ASPxComboBox>
            </td>
      
            <td width ="10%">
              <dx:aspxcheckbox 
            runat="server"
            id ="chkLikeForLike"
            Text="Like For Like"
            AutoPostBack="true"
            Checked="true"
            ToolTip="Only show dealers with data for this year and last year">
    </dx:aspxcheckbox>
            </td>
       
             <td width ="10%">
               <dx:ASPxButton 
                   ID="btnClearFilters" 
                   text="Clear Filters"
                    runat="server" 
                   ToolTip="Clear All Active Filters">
                     </dx:ASPxButton>
             </td>

       <td width ="50%" align="right">
               <dx:ASPxLabel     
        ID="ASPxLabel1" 
        Width="100%"
        Font-Italic="true"
        text="* Variance is the period selected vs. previous year"
        runat="server" />
            </td>
          </tr > 

   </table>
 

   <%-- Grid 1--%>    
   <dx:ASPxGridView 
        ID="gridDealer" 
        visible="true"
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        OnHeaderFilterFillItems="grid_HeaderFilterFillItems"
        Settings-AllowHeaderFilter="True"
        Settings-HeaderFilterMode="CheckedList"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsgridDealer" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"   />
        <Settings ShowFooter="True"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>

        <Styles Footer-HorizontalAlign="Center" />

        <Columns>

            <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                VisibleIndex="0"
                Width="6%"
                ExportWidth="200"
                Name="dealercode"
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer Name" 
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-HorizontalAlign="Left"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                VisibleIndex="1"
                Width="30%"
                ExportWidth="200"
                Name="dealername"
                FieldName="dealername">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewBandColumn
                Caption="Units"
                Name="bandUnits"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    Width="8%"
                    VisibleIndex="2"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_this"
                    FieldName="units_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Units Last" 
                    Width="8%"
                    VisibleIndex="2"
                    visible="false"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_last"
                    FieldName="units_last">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>
                    
                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="3"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_variance"
                    FieldName="units_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Sales"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Sales" 
                    Width="8%"
                    VisibleIndex="4"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    Name="sales_this"
                    FieldName="sales_this">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="5"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_variance"
                    FieldName="sales_variance">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Margin"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Margin" 
                    Width="8%"
                    VisibleIndex="6"
                    ExportWidth="100"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_this"
                    FieldName="margin_this">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="7"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    Name="margin_variance"
                    FieldName="margin_variance">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Customers"
                Name="bandCustomers"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Customers Purchased" 
                    Width="8%"
                    VisibleIndex="8"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="customers_this"
                    FieldName="customers_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="9"
                    ExportWidth="100"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="customers_variance"
                    FieldName="customers_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
        
        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="units_this" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="units_variance" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="sales_this" SummaryType="Sum"  DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="sales_variance" SummaryType="Sum" DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="customers_this" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="customers_variance" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="margin_this" SummaryType="Custom" DisplayFormat="  0.00%"/>
            <dx:ASPxSummaryItem FieldName ="margin_variance" SummaryType="Custom"  DisplayFormat="  0.00%" />
        </TotalSummary>


    </dx:ASPxGridView>
   <%-- End of Grid 1--%>    

   <%-- Grid 2--%>    
   <dx:ASPxGridView 
        ID="gridCustomer" 
        visible="true"
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        OnHeaderFilterFillItems="grid_HeaderFilterFillItems"
        AutoGenerateColumns="False"
        DataSourceID="dsgridCustomer" 
        Settings-AllowHeaderFilter="True"
        Settings-HeaderFilterMode="CheckedList"
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        KeyFieldName="refcode" 
        Width="100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
        <Settings ShowFooter="True"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>

        <Styles Footer-HorizontalAlign="Center" />

        <SettingsDetail 
                AllowOnlyOneMasterRowExpanded="True" 
                ExportMode="Expanded"
                ShowDetailRow="True" />
       
        <Columns>

            <dx:GridViewDataTextColumn 
                Caption="refcode" 
                visible="false">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-HorizontalAlign="Left"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                VisibleIndex="0"
                Width="20%"
                ExportWidth="200"
                Name="dealer"
                FieldName="dealer">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="A/C Code" 
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                VisibleIndex="0"
                Width="8%"
                ExportWidth="100"
                Name="accountcode"
                FieldName="accountcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Customer Name" 
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-HorizontalAlign="Left"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                VisibleIndex="1"
                Width="20%"
                ExportWidth="200"
                Name="accountname"
                FieldName="accountname">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewBandColumn
                Caption="Units"
                Name="bandUnits"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    Width="8%"
                    VisibleIndex="2"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_this"
                    FieldName="units_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="3"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_variance"
                    FieldName="units_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Sales"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Sales" 
                    Width="10%"
                    VisibleIndex="4"
                    ExportWidth="100"        
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_this"
                    FieldName="sales_this">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="10%"
                    VisibleIndex="5"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_variance"
                    FieldName="sales_variance">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Margin"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Margin" 
                    Width="8%"
                    VisibleIndex="6"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_this"
                    FieldName="margin_this">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="7"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_variance"
                    FieldName="margin_variance">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>






                </Columns>
            </dx:GridViewBandColumn>
            
        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="units_this" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="units_variance" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="sales_this" SummaryType="Sum"  DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="sales_variance" SummaryType="Sum" DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="margin_this" SummaryType="Custom" DisplayFormat="  0.00%"/>
            <dx:ASPxSummaryItem FieldName ="margin_variance" SummaryType="Custom"  DisplayFormat="  0.00%" />
        </TotalSummary>

       <Templates>
           <DetailRow>

                  <%-- Grid 2a--%>    
                  <dx:ASPxGridView 
        ID="gridAccount" 
        CssClass="grid_styles"
        OnLoad ="gridStylesSearchOff"
        OnBeforePerformDataSelect="gridAccount_BeforePerformDataSelect"
        Settings-ShowHeaderFilterButton="false"
        Settings-ShowHeaderFilterBlankItems="false"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsgridAccount" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"   />
        <Settings ShowFooter="True"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>

        <Styles Footer-HorizontalAlign="Center" />

        <Columns>
            
            <dx:GridViewDataTextColumn 
                    Caption="Product Group Code" 
                    Width="15%"
                    VisibleIndex="0"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="level2_code"
                    FieldName="level2_code">
             </dx:GridViewDataTextColumn>

             <dx:GridViewDataTextColumn 
                    Caption="Product Group Description" 
                    Width="25%"
                    VisibleIndex="1"
                    ExportWidth="200"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="l2_description"
                    FieldName="l2_description">
                </dx:GridViewDataTextColumn>

             <dx:GridViewDataTextColumn 
                    Caption="Part Number" 
                    Width="15%"
                    VisibleIndex="2"
                    ExportWidth="200"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="partnumber"
                    FieldName="partnumber">
                </dx:GridViewDataTextColumn>
         
             <dx:GridViewDataTextColumn 
                    Caption="Part Description" 
                    Width="15%"
                    VisibleIndex="3"
                    ExportWidth="200"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="partdescription"
                    FieldName="partdescription">
              </dx:GridViewDataTextColumn>

              <dx:GridViewDataTextColumn 
                    Caption="Units Sold" 
                    Width="10%"
                    VisibleIndex="4"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="quantity"
                    FieldName="quantity">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    Width="10%"
                    VisibleIndex="5"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="invoice_value"
                    FieldName="invoice_value">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Margin" 
                    Width="10%"
                    VisibleIndex="6"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin"
                    FieldName="margin">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>


        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="quantity" SummaryType="Sum"  DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="invoice_value" SummaryType="Sum"  DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="margin" SummaryType="Average"  DisplayFormat="  0.00%" />
        </TotalSummary>


    </dx:ASPxGridView>
                  <%-- End of Grid 2a--%>    

           </DetailRow>
       </Templates>



    </dx:ASPxGridView>
   <%-- End of Grid 2--%>    

   <%-- Grid 3--%>    
   <dx:ASPxGridView 
        ID="gridProductGroup" 
        visible="true"
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        OnHeaderFilterFillItems="grid_HeaderFilterFillItems"
        AutoGenerateColumns="False"
        DataSourceID="dsgridProductGroup" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Settings-AllowHeaderFilter="True"
        Settings-HeaderFilterMode="CheckedList"
        Width="100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
        <Settings ShowFooter="True"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>

        <Styles Footer-HorizontalAlign="Center" />
       
        <Columns>

            <dx:GridViewDataTextColumn 
                    Caption="Product Group" 
                    Width="15%"
                    VisibleIndex="0"
                    ExportWidth="150"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    Name="productgroup"
                    FieldName="productgroup">
            </dx:GridViewDataTextColumn>

            <dx:GridViewBandColumn
                Caption="Units"
                Name="bandUnits"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    Width="7%"
                    VisibleIndex="2"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_this"
                    FieldName="units_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    VisibleIndex="3"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_variance"
                    FieldName="units_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Sales"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Sales" 
                    Width="7%"
                    VisibleIndex="4"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_this"
                    FieldName="sales_this">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    VisibleIndex="5"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_variance"
                    FieldName="sales_variance">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Margin"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Margin" 
                    Width="6%"
                    VisibleIndex="6"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_this"
                    FieldName="margin_this">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="7"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_variance"
                    FieldName="margin_variance">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn
                Caption="Dealers"
                Name="bandDealers"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Dealers Sold" 
                    Width="8%"
                    VisibleIndex="8"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="dealers_this"
                    FieldName="dealers_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="9"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="dealers_variance"
                    FieldName="dealers_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
     
            <dx:GridViewBandColumn
                Caption="Customers"
                Name="bandCustomers"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Customers Purchased" 
                    Width="8%"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="8"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="customers_this"
                    FieldName="customers_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="8%"
                    VisibleIndex="9"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    Name="customers_variance"
                    FieldName="customers_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="units_this" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="units_variance" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="sales_this" SummaryType="Sum"  DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="sales_variance" SummaryType="Sum" DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="margin_this" SummaryType="Custom" DisplayFormat="  0.00%"/>
            <dx:ASPxSummaryItem FieldName ="margin_variance" SummaryType="Custom"  DisplayFormat="  0.00%" />
         </TotalSummary>

    </dx:ASPxGridView>
   <%-- End of Grid 3--%>    

    <%-- Grid 4--%>    
   <dx:ASPxGridView 
        ID="gridPartNumber" 
        visible="true"
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        OnHeaderFilterFillItems="grid_HeaderFilterFillItems"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsgridPartnumber" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Settings-AllowHeaderFilter="True"
        Settings-HeaderFilterMode="CheckedList"
        Width="100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
        <Settings ShowFooter="True"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>

        <Styles Footer-HorizontalAlign="Center" />
       
        <Columns>

            <dx:GridViewDataTextColumn 
                    Caption="Product Group" 
                    Width="10%"
                    VisibleIndex="0"
                    CellStyle-Wrap="False"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="150"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    Name="productgroup"
                    FieldName="productgroup">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                    Caption="Part Number" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    Width="8%"
                    VisibleIndex="1"
                    ExportWidth="150"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="partnumber"
                    FieldName="partnumber">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                    Caption="Part Description" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    Width="9%"
                    VisibleIndex="1"
                    ExportWidth="150"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    Name="partdescription"
                    FieldName="partdescription">
            </dx:GridViewDataTextColumn>

            <dx:GridViewBandColumn
                Caption="Units"
                Name="bandUnits"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    Width="5%"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="2"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_this"
                    FieldName="units_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="6%"
                    VisibleIndex="3"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="units_variance"
                    FieldName="units_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Sales"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Sales" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    Width="7%"
                    VisibleIndex="4"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_this"
                    FieldName="sales_this">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    VisibleIndex="5"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="sales_variance"
                    FieldName="sales_variance">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
            <dx:GridViewBandColumn
                Caption="Margin"
                Name="bandSales"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Margin" 
                    Width="7%"
                    VisibleIndex="6"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_this"
                    FieldName="margin_this">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    VisibleIndex="7"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="margin_variance"
                    FieldName="margin_variance">
                    <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn
                Caption="Dealers"
                Name="bandDealers"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Dealers Sold" 
                    Width="7%"
                    VisibleIndex="8"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="dealers_this"
                    FieldName="dealers_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="9"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="dealers_variance"
                    FieldName="dealers_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
     
            <dx:GridViewBandColumn
                Caption="Customers"
                Name="bandCustomers"
                HeaderStyle-HorizontalAlign="Center">
                <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Customers Purchased" 
                    Width="7%"
                    VisibleIndex="8"
                    ExportWidth="100"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Name="customers_this"
                    FieldName="customers_this">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Variance*" 
                    Width="7%"
                    VisibleIndex="9"
                    ExportWidth="100"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    Name="customers_variance"
                    FieldName="customers_variance">
                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                </Columns>
            </dx:GridViewBandColumn>
            
        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="units_this" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="units_variance" SummaryType="Sum" DisplayFormat="##,##0"/>
            <dx:ASPxSummaryItem FieldName ="sales_this" SummaryType="Sum"  DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="sales_variance" SummaryType="Sum" DisplayFormat="&#163;##,##0.00"/>
            <dx:ASPxSummaryItem FieldName ="margin_this" SummaryType="Custom" DisplayFormat="  0.00%"/>
            <dx:ASPxSummaryItem FieldName ="margin_variance" SummaryType="Custom"  DisplayFormat="  0.00%" />
           </TotalSummary>

    </dx:ASPxGridView>
   <%-- End of Grid 4--%>    

</div>

    <%-- Data Sources --%>    
    <asp:SqlDataSource 
        ID="dsgridDealer" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_CoreStock_Dealer" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
           <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
           <asp:SessionParameter Name="nFrom" SessionField="CoreStockFromPeriod" Type="Int32" />
           <asp:SessionParameter Name="nTo" SessionField="CoreStockToPeriod" Type="Int32" />
           <asp:SessionParameter Name="nLikeForLike" SessionField="everpresent_flag" Type="Int32" />
         </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsGridCustomer" 
        runat="server" 
         ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_CoreStock_Customer" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
           <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
           <asp:SessionParameter Name="nFrom" SessionField="CoreStockFromPeriod" Type="Int32" />
           <asp:SessionParameter Name="nTo" SessionField="CoreStockToPeriod" Type="Int32" />
           <asp:SessionParameter Name="nLikeForLike" SessionField="everpresent_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsGridAccount" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
          SelectCommand="sp_CoreStock_Account" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="sRefCode" SessionField="RefCode" Type="String" />
           <asp:SessionParameter Name="nFrom" SessionField="CoreStockFromPeriod" Type="String" />
           <asp:SessionParameter Name="nTo" SessionField="CoreStockToPeriod" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsGridProductGroup" 
        runat="server" 
       ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
           SelectCommand="sp_CoreStock_ProductGroup" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
           <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
           <asp:SessionParameter Name="nFrom" SessionField="CoreStockFromPeriod" Type="Int32" />
           <asp:SessionParameter Name="nTo" SessionField="CoreStockToPeriod" Type="Int32" />
           <asp:SessionParameter Name="nLikeForLike" SessionField="everpresent_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsGridPartNumber" 
        runat="server" 
      ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_CoreStock_PartNumber" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
           <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
           <asp:SessionParameter Name="nFrom" SessionField="CoreStockFromPeriod" Type="Int32" />
           <asp:SessionParameter Name="nTo" SessionField="CoreStockToPeriod" Type="Int32" />
           <asp:SessionParameter Name="nLikeForLike" SessionField="everpresent_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsMonth" 
        runat="server" 
     ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="p_Get_Months" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    
    <%-- Exporters --%>    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridDealer">
    </dx:ASPxGridViewExporter>
 
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter2" 
        runat="server" 
        GridViewID="gridCustomer">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter3" 
        runat="server" 
        GridViewID="gridProductGroup">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter4" 
        runat="server" 
        GridViewID="gridPartNumber">
    </dx:ASPxGridViewExporter>

 </asp:Content>

