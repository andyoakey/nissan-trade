﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DealerInformation.aspx.vb" Inherits="DealerInformation" title="Dealer Information" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
    

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
             <dx:ASPxLabel 
                ID="lblPageTitle" 
                Font-Size="Larger"  
                runat="server" 
                Text="Dealer Details"/>
        </div>

        
    </div>
    
   
    <br />
    <br />
    <br />
    <br />
    

    <div style="position: relative; font-family: Calibri; text-align: left; " >

        <table>
        <tr>
            <td>
                <dx:ASPxFormLayout runat="server" ID="formData" RequiredMarkDisplayMode="RequiredOnly" EnableViewState="false" EncodeHtml="false" >
                    <Items>

                        <dx:LayoutGroup Caption="Name" SettingsItemHelpTexts-Position="Bottom" GroupBoxStyle-Caption-Font-Size="Larger"  GroupBoxStyle-BackColor="WhiteSmoke"  >
                            <Items>

                                <dx:LayoutItem Caption="Dealer Name"  HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtDealerName" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="true" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Marketing Name" HelpText="The name to be used for all marketing materials" HelpTextStyle-Font-Italic="true">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtMarketingName" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="true" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                            </Items>
                        </dx:LayoutGroup>

                        <dx:LayoutGroup Caption="Address" SettingsItemHelpTexts-Position="Bottom" GroupBoxStyle-Caption-Font-Size="Larger" GroupBoxStyle-BackColor="WhiteSmoke"  >
                            <Items>

                                <dx:LayoutItem Caption="Address Line 1" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtAddress1" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="true" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Address Line 2" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtAddress2" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Address Line 3" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtAddress3" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Address Town" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtAddress4" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Address Post Code" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtPostCode" Width="100" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                       
                            </Items>
                        </dx:LayoutGroup>

                        <dx:LayoutGroup Caption="Contact Information" SettingsItemHelpTexts-Position="Bottom" GroupBoxStyle-Caption-Font-Size="Larger" GroupBoxStyle-BackColor="WhiteSmoke"  >
                            <Items>

                                <dx:LayoutItem Caption="Telephone Number" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtTel" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="true" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Fax Number" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtFax" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Email" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtEmail" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Contact Name" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtContact" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="true" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Contact Position" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtPosition" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Opening Hours" HelpText="" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxTextBox runat="server" ID="txtOpeningHours" Width="400" >
                                                <ValidationSettings RequiredField-IsRequired="false" Display="Dynamic" ErrorDisplayMode="Text" />
                                            </dx:ASPxTextBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem ShowCaption="false" RequiredMarkDisplayMode="Hidden" HorizontalAlign="Right" Width="100">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer>
                                            <dx:ASPxButton runat="server" ID="btnSave" Text="Save" Width="100"  />
                                            <dx:ASPxButton runat="server" ID="btnCancel" Text="Cancel" Width="100"  />
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                            </Items>
                        </dx:LayoutGroup>

                    </Items>
                </dx:ASPxFormLayout>
            </td>
        </tr>
    </table>    
    

    </div>


   <table>
            <tr>
                <td width ="25%">


                </td>

                <td width ="50%" align="center">
                         <dx:ASPxLabel  Width="100%" ForeColor="Red"
        ID="lblCentreError" 
        Visible="false"
        runat="server" 
        Font-Size="Larger"
        Text="This Screen is only available when a single Dealer is selected." />							
                </td>

                <td width ="25%">


                </td>
            </tr>


        </table>

    <asp:SqlDataSource ID="dsDocLib" runat="server" SelectCommand="p_DocLib" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>


