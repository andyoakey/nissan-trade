﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraCharts

Partial Class report4

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess
    Dim htIn1 As New Hashtable
    Dim sErr As String
    Dim sSQL As String
    Dim dsResults As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Tools and Help"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Tools and Help", "Tools and Help")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        'Dim nButtonHeight As Integer = Session("ButtonHeight")
        Dim nButtonWidth As Integer = 100

        btn1.Image.Width = nButtonWidth
        btn2.Image.Width = nButtonWidth
        btn3.Image.Width = nButtonWidth
        btn4.Image.Width = nButtonWidth
        btn5.Image.Width = nButtonWidth
        btn6.Image.Width = nButtonWidth
        btn7.Image.Width = nButtonWidth
        btn8.Image.Width = nButtonWidth

        htIn1.Clear()
        htIn1.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn1.Add("@sSelectionId", Session("SelectionId"))
        dsResults = da.Read(sErr, "p_ExcludedSales_Summary", htIn1)

        Call PopulateChart(chartExcludedSales, "Excluded Sales", dsResults.Tables(0).Rows(0).Item("ExcludedSales"), dsResults.Tables(0).Rows(0).Item("IncludedSales"))

    End Sub

    Protected Sub dsExcludedSalesSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsExcludedSalesSummary.Init
        dsExcludedSalesSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub IconButtons_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click, btn6.Click, btn7.Click, btn8.Click
        Select Case sender.id
            Case "btn1"
                Response.Redirect("ViewExcludedSales.aspx")
            Case "btn2"
                Response.Redirect("DataSubmission.aspx")
            Case "btn3"
                Response.Redirect("PartInfo.aspx")
            Case "btn4"
                Response.Redirect("QuickInvoiceSearch.aspx")
            Case "btn5"
                Response.Redirect("DocLibrary.aspx")
            Case "btn6"
                Response.Redirect("Feedback.aspx")
            Case "btn7"
                Response.Redirect("ChgPwd.aspx")
            Case "btn8"
                Response.Redirect("DealerInformation.aspx")
        End Select
    End Sub

    Protected Sub gridExcludedInvoices_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Or e.Column.FieldName = "ExcludedPerc" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Private Sub PopulateChart(objChart As DevExpress.XtraCharts.Web.WebChartControl, sTitle As String, nExcluded As Double, nIncluded As Double)

        ' Create a bar series.
        Dim series1 As New Series(sTitle, ViewType.Bar)
        Dim series2 As New Series(sTitle, ViewType.Bar)

        ' Populate the series with points.
        series1.Points.Add(New SeriesPoint("Included Sales (YTD)", nIncluded))
        series2.Points.Add(New SeriesPoint("Excluded Sales (YTD)", nExcluded))

        ' Add the series to the chart.
        objChart.Series.Clear()
        objChart.Series.Add(series1)
        objChart.Series.Add(series2)

        ' Adjust the point options of the series.
        series1.Label.Visible = True
        series1.Label.TextPattern = "{V:c0}"
        series1.Label.TextAlignment = StringAlignment.Center
        series1.LegendTextPattern = "{A}"
        series1.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.False

        series2.Label.Visible = True
        series2.Label.TextPattern = "{V:c0}"
        series2.Label.TextAlignment = StringAlignment.Center
        series2.LegendTextPattern = "{A}"
        series2.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.False

        Dim diagram As XYDiagram = CType(objChart.Diagram, XYDiagram)
        diagram.Rotated = True
        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.NumericOptions.Format = NumericFormat.Currency
        diagram.AxisY.NumericOptions.Precision = 0

        ' Access the view-type-specific options of the series.
        Dim myView As BarSeriesView = CType(series1.View, BarSeriesView)

        With objChart
            .Legend.Visible = False
            .AppearanceNameSerializable = "Toyota"
            .BackColor = Color.Transparent
            .ToolTipEnabled = DevExpress.Utils.DefaultBoolean.False
        End With

    End Sub

End Class
