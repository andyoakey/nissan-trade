﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class CustomerSummaryByDealer

    Inherits System.Web.UI.Page

    Dim nSale1 As Double
    Dim nCost1 As Double
    Dim nSale2 As Double
    Dim nCost2 As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Customer Summary by Centre"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer Summary by Centre", "Viewing Customer Summary by Centre")
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsCustSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCustSummary.Init
        dsCustSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCustomerSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ExcludedPerc" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub


End Class
