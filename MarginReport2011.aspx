<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MarginReport2011.aspx.vb" Inherits="MarginReport2011" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Margin Report 2011" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridMargin" runat="server" width="976px" 
            DataSourceID="dsMargin"  AutoGenerateColumns="False" KeyFieldName="DealerCode" >
       
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden"
                ShowGroupFooter="VisibleIfExpanded" 
                UseFixedTableLayout="True" VerticalScrollableHeight="350" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <Images ImageFolder="~/App_Themes/Office2003 Silver/{0}/">
                <CollapsedButton Height="12px" 
                    Url="~/App_Themes/Office2003 Silver/GridView/gvCollapsedButton.png" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Url="~/App_Themes/Office2003 Silver/GridView/gvExpandedButton.png" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Url="~/App_Themes/Office2003 Silver/GridView/gvCollapsedButton.png" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Url="~/App_Themes/Office2003 Silver/GridView/gvExpandedButton.png" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone"
                    VisibleIndex="1" Width="10%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="True" ShowInFilterControl="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode"
                    VisibleIndex="2" Width="10%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="True" ShowInFilterControl="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName"
                    VisibleIndex="3" Width="25%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="True" ShowInFilterControl="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Sales Q1" FieldName="Sale1"
                    VisibleIndex="4">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Cost Q1" FieldName="Cost1"
                    VisibleIndex="5">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Margin Q1" FieldName="Margin1"
                    VisibleIndex="6">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>


                <dxwgv:GridViewDataTextColumn Caption="Sales Q2 onwards" FieldName="Sale2"
                    VisibleIndex="7">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Cost Q2 onwards" FieldName="Cost2"
                    VisibleIndex="8">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="X-Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Margin Q2 onwards" FieldName="Margin2"
                    VisibleIndex="9">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Margin uplift" FieldName="Diff3"
                    VisibleIndex="10">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />

        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsMargin" runat="server" SelectCommand="p_Margin_Report" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridMargin" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>


