﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MechCompSalesPerf.aspx.vb" Inherits="MechCompSalesPerf" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>



<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >

        <div id="divTitle" style="position: relative; left: 5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Mechanical Competitive Sales Performance"
                Style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridTargetSummary3" runat="server" Visible="false"
            AutoGenerateColumns="False"
            OnCustomColumnDisplayText="gridTargetSummary3_OnCustomColumnDisplayText"
            OnHtmlDataCellPrepared="gridTargetSummary3_HtmlDataCellPrepared" Width="976px">
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="Description1" ReadOnly="True"
                    VisibleIndex="0" Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="TargetValue1" ReadOnly="True"
                    VisibleIndex="1">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Description2" ReadOnly="True"
                    VisibleIndex="2" Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="TargetValue2" ReadOnly="True"
                    VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Description3" ReadOnly="True"
                    VisibleIndex="4" Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="TargetValue3" ReadOnly="True"
                    VisibleIndex="5">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <SettingsPager Visible="False">
            </SettingsPager>
            <Settings ShowColumnHeaders="False" UseFixedTableLayout="True" />
            <Images>
                <CollapsedButton Height="12px" Width="11px">
                </CollapsedButton>
                <ExpandedButton Height="12px" Width="11px">
                </ExpandedButton>
                <DetailCollapsedButton Height="12px" Width="11px">
                </DetailCollapsedButton>
                <DetailExpandedButton Height="12px" Width="11px">
                </DetailExpandedButton>
                <FilterRowButton Height="13px" Width="13px">
                </FilterRowButton>
            </Images>
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
        </dxwgv:ASPxGridView>

        <dxwgv:ASPxGridView ID="gridApplicPerf" runat="server" AutoGenerateColumns="False"
            Visible="false" OnHtmlDataCellPrepared="gridApplicPerf_HtmlDataCellPrepared" Width="976px">
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="Text1" ReadOnly="True" VisibleIndex="0"
                    Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="ApplicSpendYTD" ReadOnly="True"
                    VisibleIndex="1">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Text2" ReadOnly="True" VisibleIndex="2"
                    Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="ApplicSpendLastYear" ReadOnly="True"
                    VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Text3" ReadOnly="True" VisibleIndex="4"
                    Width="25%">
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Variance" ReadOnly="True"
                    VisibleIndex="5">
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <SettingsPager Visible="False">
            </SettingsPager>
            <Settings ShowColumnHeaders="False" UseFixedTableLayout="True" />
            <Images>
                <CollapsedButton Height="12px" Width="11px">
                </CollapsedButton>
                <ExpandedButton Height="12px" Width="11px">
                </ExpandedButton>
                <DetailCollapsedButton Height="12px" Width="11px">
                </DetailCollapsedButton>
                <DetailExpandedButton Height="12px" Width="11px">
                </DetailExpandedButton>
                <FilterRowButton Height="13px" Width="13px">
                </FilterRowButton>
            </Images>
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
        </dxwgv:ASPxGridView>

        <dxwgv:ASPxGridView ID="gridTargetSummary2" runat="server"
            AutoGenerateColumns="False" OnCustomColumnDisplayText="gridTargetSummary2_OnCustomColumnDisplayText"
            Width="976px">
            <TotalSummary>
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="WorkingDays"
                    ShowInColumn="WorkingDays" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="MCApplicTarget"
                    ShowInColumn="MCApplicTarget" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="MCApplicForecast"
                    ShowInColumn="MCApplicForecast" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="MCTotalForecast"
                    ShowInColumn="MCTotalForecast" SummaryType="Sum" />
            </TotalSummary>
            <Columns>
                <dxwgv:GridViewDataTextColumn Caption="Month" FieldName="PeriodName"
                    ReadOnly="True" VisibleIndex="0" Width="10%">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="Working Days" FieldName="WorkingDays"
                    ReadOnly="True" VisibleIndex="1" Width="10%">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Applicable Sales"
                    FieldName="MCApplicSales" VisibleIndex="2">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Applicable Target"
                    FieldName="MCApplicTarget" VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Applicable Achievement"
                    FieldName="MCApplicAchieved" VisibleIndex="4">
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Applicable Daily Average"
                    FieldName="MCApplicAverage" VisibleIndex="5">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Applicable Forecast"
                    FieldName="MCApplicForecast" VisibleIndex="6">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Total Sales" FieldName="MCTotalSales"
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Total Daily Average"
                    FieldName="MCTotalAverage" VisibleIndex="8">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn Caption="M/C Total Forecast"
                    FieldName="MCTotalForecast" VisibleIndex="9">
                    <PropertiesTextEdit DisplayFormatString="£#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
            </Columns>
            <SettingsPager Mode="ShowAllRecords" Visible="False">
            </SettingsPager>
            <Settings ShowFooter="True" UseFixedTableLayout="True" />
            <Images>
                <CollapsedButton Height="12px" Width="11px">
                </CollapsedButton>
                <ExpandedButton Height="12px" Width="11px">
                </ExpandedButton>
                <DetailCollapsedButton Height="12px" Width="11px">
                </DetailCollapsedButton>
                <DetailExpandedButton Height="12px" Width="11px">
                </DetailExpandedButton>
                <FilterRowButton Height="13px" Width="13px">
                </FilterRowButton>
            </Images>
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
        </dxwgv:ASPxGridView>
        <br />
</asp:Content>


