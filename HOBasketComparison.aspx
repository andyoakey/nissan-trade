﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HOBasketComparison.aspx.vb" Inherits="HOBasketComparison" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>

    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

  <dx:ASPxPopupControl 
        ID="popProducts" 
        runat="server"  
        Width ="500px"  
        Height="400px" 
        HeaderText="Select the Parts Basket Product Types to Report On"
        ShowOnPageLoad="False" 
        ClientInstanceName="popProducts" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        CloseOnEscape="True">
        
        <ContentCollection>

            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" CssClass="invoice">
                  <dx:ASPxRadioButtonList ID="rblAll"  runat="server" OnSelectedIndexChanged="rblAll_SelectedIndexChanged" RepeatColumns="2" RepeatDirection="Vertical"  AutoPostBack="true">
                     <Items>
                            <dx:ListEditItem Text="All Products" Selected="true" Value="0" />
                            <dx:ListEditItem Text="Selected Products" Value="1" />
                     </Items>

                  </dx:ASPxRadioButtonList>

                  <br />

                  <dx:ASPxCheckBoxList id="cblProducts"  Enabled="false" OnValueChanged="cboProduct_ValueChanged" RepeatColumns="2" RepeatDirection="Vertical"  SelectedIndex="0" DataSourceID="dsProductGroups" ValueField="Code" TextField="Description" runat="server" Border-BorderStyle="None"/>

                  <br />

                  <dx:ASPxButton ID ="btnClose" runat="Server" text ="Update Query"  OnClick="btnClose_Click"  AutoPostBack="true" />

            </dx:PopupControlContentControl>



        </ContentCollection>
    </dx:ASPxPopupControl>

  <script type="text/javascript">
      var textSeparator = ",";
      function OnListBoxSelectionChanged(listBox, args) {
          if (args.index == 0)
              args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
          UpdateSelectAllItemState();
          UpdateText();
      }
      function UpdateSelectAllItemState() {
          IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
      }
      function IsAllSelected() {
          var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
          return checkListBox.GetSelectedItems().length == selectedDataItemCount;
      }
      function UpdateText() {
          var selectedItems = checkListBox.GetSelectedItems();
          checkComboBox.SetText(GetSelectedItemsText(selectedItems));
      }
      function SynchronizeListBoxValues(dropDown, args) {
          checkListBox.UnselectAll();
          var texts = dropDown.GetText().split(textSeparator);
          var values = GetValuesByTexts(texts);
          checkListBox.SelectValues(values);
          UpdateSelectAllItemState();
          UpdateText(); // for remove non-existing texts
      }
      function GetSelectedItemsText(items) {
          var texts = [];
          for (var i = 0; i < items.length; i++)
              if (items[i].index != 0)
                  texts.push(items[i].value);
          return texts.join(textSeparator);

      }
      function GetValuesByTexts(texts) {
          var actualValues = [];
          var item;
          for (var i = 0; i < texts.length; i++) {
              item = checkListBox.FindItemByText(texts[i]);
              if (item != null)
                  actualValues.push(item.value);
          }
          return actualValues;
      }
    </script>

    
    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel 
                    ID="lblPageTitle" 
                    runat="server" 
                    Font-Size="Larger"
                    Text="Basket Campaign Comparison Report"  />
       </div>
       
        <table id="trSelectionStuff"  style="position: relative; margin-bottom: 10px;">
    
            <tr id="Tr2" runat="server" style="height:30px;">
       
                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
                            id="lblReportType"
                            runat="server" 
                            Text="Report Level">
                        </dx:ASPxLabel>
                </td>

                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="lblType"
        runat="server" 
        Text="Month">
    </dx:ASPxLabel>
                </td>
    
                <td style="width: 350px;" align="left" valign="bottom">

                


	                    <dx:ASPxLabel 
        id="ASPxLabel1"
        runat="server" 
        Text="Comparison Period">
    </dx:ASPxLabel>
                </td>
                      
                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="ASPxLabel3"
        runat="server" 
        Text="Report Content">
    </dx:ASPxLabel>
	            </td>

                <td style="width: 100px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="ASPxLabel2"
        runat="server" 
        Text="Basket Items">
    </dx:ASPxLabel>
	            </td>

                <td style="width: 300px;" 
                align="left" valign="bottom">
	            </td>
  
  
  
            </tr>
       
            <tr id="Tr3" runat="server" style="height:30px;">
                
                <td style="width: 170px;" align="left" valign="top">
                    <dx:ASPxComboBox
                        runat="server" 
                        Width="160px"
                        ID="cboReportLevel"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Show All Customers" Value="1" Selected="true" />
                            <dx:ListEditItem Text="Summary by Dealer" Value="2" Selected="false" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width: 170px;" align="left" valign="top">
                     <dx:ASPxComboBox
                        ID="ddlFrom"
                        Width="150px"
                        runat="server" 
                        ValueField="ID"
                        TextField="PeriodName"
                        SelectedIndex="0"
                        DataSourceID="dsMonths"
                        AutoPostBack="True"/>
                   </td>
            
                <td style="width: 350px;" align="left" valign="top">

                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType"
                        Width="340px"
                        SelectedIndex="0"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Monthly - Compare to last month" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Monthly - Compare to 12 months ago" Value="1" Selected="true" />
                            <dx:ListEditItem Text="Quarterly - Compare to last quarter" Value="2"/>
                            <dx:ListEditItem Text="Quarterly - Compare to same quarter 12 months ago" Value="3"/>
                            <dx:ListEditItem Text="Yearly - last 12 months vs. preceding 12 months" Value="4"/>
                            <dx:ListEditItem Text="Calendar Year To Date -  vs. equivalent last year" Value="5"/>
                            <dx:ListEditItem Text="Financial Year To Date -  vs. equivalent last year" Value="6"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                
                <td style="width: 170px;" align="left" valign="top">
                    <dx:ASPxComboBox
                        runat="server" 
                        Width="160px"
                        ID="ddlReportType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Sale Value" Value="1" Selected="true" />
                            <dx:ListEditItem Text="Cost to Dealer" Value="2" Selected="false" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width: 100px;" align="left" valign="top">
                   <dx:ASPxButton ID="btnProducts" OnClick="btnProducts_Click" runat="server" AutoPostBack="true"  Text="Select..." ToolTip="Select one or more items from the Parts Basket to report on individually"/>
                </td>
        
                <td style="width: 300px;" align="left" valign="top">   
                     <dx:ASPxTextBox ID="txtProductList"  Width="300px" Text="All Basket Products" runat="server"/>
                 </td>

                <td style="width: 300px;" align="right" valign="top">
	               <dx:ASPxButton ID="btnClearFilters" OnClick="btnClearFilters_Click" runat="server" AutoPostBack="true"  Text="Clear Filters" ToolTip="Clears all Filters and resets all drop down lists to default values."/>
                </td>
  


              </tr>

        </table>

        <dx:ASPxGridView 
            ID="gridCompare" 
            runat="server" 
            onload="GridStyles"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            Visible="False"
            KeyFieldName="CustomerDealerId"
            AutoGenerateColumns="False" 
            style="position: relative;" Width="100%" >
       
            <SettingsDetail  ShowDetailRow="true"  ShowDetailButtons="true"  AllowOnlyOneMasterRowExpanded="true"/>

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="True"
                ShowHeaderFilterBlankItems="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                ShowFooter="true"
                UseFixedTableLayout="True" />
     
            <SettingsBehavior 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Styles Footer-HorizontalAlign ="Center" />

            <SettingsPager PageSize="15">
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Columns>
                
          
              <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndDealer" HeaderStyle-Font-Size="Larger"  Caption="Dealer Information">
                         <Columns>
                            <dx:GridViewDataTextColumn FieldName="CustomerDealerId" Visible ="false"></dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn  Caption="Zone" FieldName="Zone" PropertiesTextEdit-DisplayFormatString="c0" VisibleIndex="0" Width="5%" ExportWidth="150">
                                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="Code" 
                    FieldName="DealerCode" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    VisibleIndex="1" 
                    Width="5%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    Caption="Dealer" 
                    FieldName="DealerName" 
                    VisibleIndex="2" 
                    Width="13%" 
                    PropertiesTextEdit-DisplayFormatString="c"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Left" />
                </dx:GridViewDataTextColumn>
                        </Columns>
                </dx:GridViewBandColumn>
              
              <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndCustomer" HeaderStyle-Font-Size="Larger" Caption="Customer Information">
                         <Columns>
                                <dx:GridViewDataTextColumn 
                    Caption="A/C Code" 
                    FieldName="AccountCode" 
                    VisibleIndex="3" 
                    Width="6%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    Caption="Name" 
                    FieldName="CustomerName" 
                    VisibleIndex="4" 
                    Width="21%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Left" />
                </dx:GridViewDataTextColumn>


                                <dx:GridViewDataTextColumn 
                    Caption="Business Type" 
                    FieldName="BusinessType" 
                    VisibleIndex="5" 
                    Width="8%" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Recency" 
                    FieldName="Recency" 
                    VisibleIndex="5" 
                    Width="7%" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="True"  HeaderFilterMode="CheckedList"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Wrap="False" HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>



                        </Columns>
                </dx:GridViewBandColumn>
              
              <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndCompareBasket" HeaderStyle-Font-Size="Larger"  Caption="Basket Products">
                            <Columns>
                                        <dx:GridViewDataTextColumn 
                                           Caption="BasketThis" 
                                           FieldName="BasketThis" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="6" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="BasketLast" 
                                           FieldName="BasketLast" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="7" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="Comparison" 
                                           FieldName="BasketDiff" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="8" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="Comparison %" 
                                           FieldName="BasketDiffPC" 
                                           PropertiesTextEdit-DisplayFormatString="0.00%"                     
                                           VisibleIndex="9" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>


                            </Columns>
               </dx:GridViewBandColumn>

              <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndCompareFull" HeaderStyle-Font-Size="Larger"  Caption="All Products">
                            <Columns>
                                        <dx:GridViewDataTextColumn 
                                           Caption="FullThis" 
                                           FieldName="FullThis" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="6" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="FullLast" 
                                           FieldName="FullLast" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="7" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="Comparison" 
                                           FieldName="FullDiff" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="8" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                           Caption="Comparison %" 
                                           FieldName="FullDiffPC" 
                                           PropertiesTextEdit-DisplayFormatString="0.00%"                     
                                           VisibleIndex="9" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>


                            </Columns>
               </dx:GridViewBandColumn>
  

            </Columns>

            <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="BasketThis" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="BasketLast" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="BasketDiff" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="FullThis" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="FullLast" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="FullDiff" SummaryType="Sum" />
            </TotalSummary>

            <Templates>
                <DetailRow>

                        <div id="divTitle" style="position:relative;">
                             <dx:ASPxLabel 
                    ID="lblDetails" 
                    runat="server" 
                    Font-Size="Larger"
                    Text="Parts Basket Sales"  />
                        </div>
                        
                        <dx:ASPxPageControl
            ID="pgcDetail"
            runat="server"
            Width="100%" 
            OnDataBound="pgcDetail_DataBound"
            ActiveTabIndex="0" 
            CssClass="page_tabs">

            <TabPages>

                <dx:TabPage Text="ThisPeriod" name = "tabThis"  >

                    <ContentCollection>

                            <dx:ContentControl runat="server">
                                 <dx:ASPxGridView 
            ID="gridBasketDetailThis" 
            runat="server" 
            onload="GridStylesOff"
            DataSourceID="dsBasketDetailThis"
            OnBeforePerformDataSelect="gridBasketDetail_BeforePerformDataSelect" 
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            AutoGenerateColumns="False" 
            style="position: relative;" Width="100%" >
       

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="True"
                ShowHeaderFilterBlankItems="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                ShowFooter="true"
                UseFixedTableLayout="True" />
     
            <SettingsBehavior 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Styles Footer-HorizontalAlign ="Center" />

            <SettingsPager PageSize="15">
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>

            <Columns>

                <dx:GridViewDataTextColumn FieldName="DealerCode" Visible ="false"></dx:GridViewDataTextColumn>

                 <dx:GridViewDataDateColumn 
                                            Caption="Invoice Date" 
                                            FieldName="InvoiceDate" 
                                            ToolTip=""
                                            CellStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center"
                                            Settings-HeaderFilterMode="CheckedList"
                                            Settings-AllowHeaderFilter="True"
                                            VisibleIndex="0" 
                                            Width="10%" 
                                            ExportWidth="150">
                                        </dx:GridViewDataDateColumn>

                    <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="1" ExportWidth="150" Width="10%" >
                            <DataItemTemplate>
                                <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                            </DataItemTemplate>
                    </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                            Caption="Part Number" 
                                            FieldName="PartNumber" 
                                            ToolTip=""
                                            CellStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center"
                                            Settings-HeaderFilterMode="CheckedList"
                                            Settings-AllowHeaderFilter="True"
                                            VisibleIndex="2" 
                                            Width="10%" 
                                            ExportWidth="150">
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                  FieldName="PartDescription" 
                                  Name="PartDescription" 
                                  Caption="Description"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="3" 
                                  Width="20%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>
                
                 <dx:GridViewDataTextColumn 
                                  FieldName="PFC" 
                                  Name="PFC" 
                                  Caption="Product Code"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="4" 
                                  Width="8%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                  FieldName="PFCDescription" 
                                  Name="PFCDescription" 
                                  Caption="Product Name"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="5" 
                                  Width="12%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Units Sold" 
                                           FieldName="Quantity" 
                                           PropertiesTextEdit-DisplayFormatString="0"                     
                                           VisibleIndex="6" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Sale Value" 
                                           FieldName="SaleValue" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="7" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Cost Value" 
                                           FieldName="CostValue" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="8" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

            </Columns>

               <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="0" FieldName="Quantity" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="SaleValue" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="CostValue" SummaryType="Sum" />
                </TotalSummary>


            </dx:ASPxGridView>
                            </dx:ContentControl>
                            
                        </ContentCollection>

                   </dx:TabPage>

                <dx:TabPage Text="LastPeriod" name = "tabLast" >

                      <ContentCollection>

                            <dx:ContentControl runat="server">
                                 <dx:ASPxGridView 
            ID="gridBasketDetailLast" 
            runat="server" 
            onload="GridStylesOff"
            DataSourceID="dsBasketDetailLast"
            OnBeforePerformDataSelect="gridBasketDetail_BeforePerformDataSelect" 
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            AutoGenerateColumns="False" 
            style="position: relative;" Width="100%" >
       

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="True"
                ShowHeaderFilterBlankItems="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                ShowFooter="true"
                UseFixedTableLayout="True" />
     
            <SettingsBehavior 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Styles Footer-HorizontalAlign ="Center" />

            <SettingsPager PageSize="15">
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>

            <Columns>

                <dx:GridViewDataTextColumn FieldName="DealerCode" Visible ="false"></dx:GridViewDataTextColumn>

                 <dx:GridViewDataDateColumn 
                                            Caption="Invoice Date" 
                                            FieldName="InvoiceDate" 
                                            ToolTip=""
                                            CellStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center"
                                            Settings-HeaderFilterMode="CheckedList"
                                            Settings-AllowHeaderFilter="True"
                                            VisibleIndex="0" 
                                            Width="10%" 
                                            ExportWidth="150">
                                        </dx:GridViewDataDateColumn>

                    <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="1" ExportWidth="150" Width="10%" >
                            <DataItemTemplate>
                                <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                            </DataItemTemplate>
                    </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                            Caption="Part Number" 
                                            FieldName="PartNumber" 
                                            ToolTip=""
                                            CellStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center"
                                            Settings-HeaderFilterMode="CheckedList"
                                            Settings-AllowHeaderFilter="True"
                                            VisibleIndex="2" 
                                            Width="10%" 
                                            ExportWidth="150">
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                  FieldName="PartDescription" 
                                  Name="PartDescription" 
                                  Caption="Description"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="3" 
                                  Width="20%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>
                
                 <dx:GridViewDataTextColumn 
                                  FieldName="PFC" 
                                  Name="PFC" 
                                  Caption="Product Code"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="4" 
                                  Width="8%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                  FieldName="PFCDescription" 
                                  Name="PFCDescription" 
                                  Caption="Product Name"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="5" 
                                  Width="12%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Units Sold" 
                                           FieldName="Quantity" 
                                           PropertiesTextEdit-DisplayFormatString="0"                     
                                           VisibleIndex="6" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Sale Value" 
                                           FieldName="SaleValue" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="7" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                                           Caption="Cost Value" 
                                           FieldName="CostValue" 
                                           PropertiesTextEdit-DisplayFormatString="c2"                     
                                           VisibleIndex="8" 
                                           Width="10%" 
                                          ExportWidth="150">
                                        <Settings AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle Wrap="False" HorizontalAlign="Center" />
                                        </dx:GridViewDataTextColumn>

            </Columns>

               <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="0" FieldName="Quantity" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="SaleValue" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c2" FieldName="CostValue" SummaryType="Sum" />
                </TotalSummary>


            </dx:ASPxGridView>
                            </dx:ContentControl>
                     </ContentCollection>

                   </dx:TabPage>

              </TabPages>


               </dx:ASPxPageControl>
                </DetailRow>
            </Templates>
            
        </dx:ASPxGridView>
     
    </div>
    


    <asp:SqlDataSource ID="dsBasketDetailThis" runat="server" SelectCommand="p_HOBasketSalesComparisonDetail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nCustomerDealerID" SessionField="CustomerDealerID" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="FromThis" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="ToThis" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsBasketDetailLast" runat="server" SelectCommand="p_HOBasketSalesComparisonDetail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nCustomerDealerID" SessionField="CustomerDealerID" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="FromLast" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="ToLast" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsMonths" 
        runat="server" 
        SelectCommand="p_Get_Months_whole" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsProductGroups" 
        runat="server" 
        SelectCommand="sp_PartsBasketProductGroups" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>


    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridCompare">
    </dx:ASPxGridViewExporter>


</asp:Content>


