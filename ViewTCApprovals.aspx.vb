﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports DevExpress.Web
Imports DevExpress.Web.Rendering
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewTCApprovals

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - View Trade Club Membership Approvals"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Approval List", "Viewing Trade Club Approvals List")
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub dsCompanies_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCompanies.Init
        dsCompanies.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCompanies_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompanies.HtmlDataCellPrepared

        Dim sReason As String = ""

        If IsDBNull(e.GetValue("RemovalReason")) Then
            sReason = ""
        Else
            sReason = Trim(e.GetValue("RemovalReason"))
        End If

        If sReason <> "" Then
            e.Cell.ForeColor = System.Drawing.Color.Red
            e.Cell.Font.Italic = True
        End If

    End Sub

    Protected Sub btnExcel_Click()
         Dim nCols As Integer
        Dim nRows As Integer

        nCols = gridCompanies.Columns.Count - 1
        nRows = gridCompanies.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            Dim nVisible(nCols) As Integer
            For i = 0 To nCols
                nVisible(i) = IIf(gridCompanies.Columns(i).Visible, 1, 0)
                gridCompanies.Columns(i).Visible = True
            Next
            gridCompanies.Columns(0).Visible = False

            ASPxGridViewExporter1.FileName = "TCApprovals"
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
            For i = 0 To nCols
                gridCompanies.Columns(i).Visible = (nVisible(i) = 1)
            Next

        End If

    End Sub

End Class
