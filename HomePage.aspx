﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HomePage.aspx.vb" Inherits="HomePage" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxm" %>
<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxfm" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" tagprefix="dxx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <title>qubeDATA | Parts Website </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <!--[if IE]> <link rel="stylesheet" href="assets/css/normalize.min.css" /><!--<![endif]-->
    <link href="styles/master.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/images/toyota/favicon.ico" />
    <link rel="apple-touch-icon" href="assets/images/toyota/apple-touch-icon.png" />
    <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="assets/js/html5shiv.js"><\/script>')</script>
    <![endif]-->
</head>

<script type="text/javascript" language="javascript">
    function ReminderAck(contentUrl) {
        PopupAck.SetContentUrl(contentUrl);
        PopupAck.SetHeaderText('Click the Acknowledge button to continue');
        PopupAck.SetSize(320, 90);
        PopupAck.Show();
    }
    function HideReminderAck() {
        PopupAck.Hide();
        gridReminders.Refresh();
    }
</script>

<body>

    <form id="homepage" runat="server" class="mainform">

    <dxp:ASPxPopupControl ID="PopupAck" ClientInstanceName="PopupAck" Width="275px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="Acknowledge"
        Modal="True" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
         
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images2014/qube-logo2.png" Height="92px" Width="236px" style="position: absolute; top: 20px; left: 10px;margin-top:10px" />
        
        <dxe:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" Font-Bold="true" ID="lblWelcome" Text="You are logged in as - "
            style="position: absolute; top: 150px; left:10px; z-index: 1;" runat="server">
        </dxe:ASPxLabel>
    
        <div>
        
            <div style="width: 690px; left: 10px; position: absolute; top: 113px;">

                <dxe:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" Font-Bold="true" ID="lblPerfSnapshotTitle1" Text="Actions Summary"
                    Style="position: relative; top: 80px; z-index: 1;" runat="server">
                </dxe:ASPxLabel>

                <dxtc:ASPxPageControl ID="HomePageTabs" runat="server" EnableHierarchyRecreation="true" Width="98%"  style="position: relative; top: 100px; ">

                    <ContentStyle>
                <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
                    </ContentStyle>

                    <TabPages>

                        <dxtc:TabPage Text="Contact History Reminders">

                            <ContentCollection>

                                <dxe:ContentControl runat="server">

                                    <dxwgv:ASPxGridView ID="gridReminders" ClientInstanceName="gridReminders" runat="server" width="100%" DataSourceID="dsReminders">

                                        <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                                            ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                                            ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True" />

                                        <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                
                                        <Styles>
                                            <Header HorizontalAlign="Center" Wrap="True"  />
                                            <Cell HorizontalAlign="Center" Wrap="True"  Font-Size="Small" VerticalAlign="Top"  />
                                        </Styles>

                                        <SettingsPager Mode="ShowPager" PageSize="4" AllButton-Visible="true" >
                                        </SettingsPager>

                                        <Columns>
                                    
                                            <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="ReminderTypeId" VisibleIndex="0" Visible="False" />

                                            <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="1" Width="50px" />

                                            <dxwgv:GridViewDataDateColumn Caption="Date" FieldName="ReminderDate" VisibleIndex="2"  Width="125px" />

                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Customer" FieldName="CustomerId" VisibleIndex="3" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <PropertiesHyperLinkEdit NavigateUrlFormatString="ViewCustomerDetails.aspx?0{0}" Target="_self"  TextField="BusinessName">
                                                    <Style Font-Size="Small" HorizontalAlign="Left">
                                                    </Style>
                                                </PropertiesHyperLinkEdit>
                                            </dxwgv:GridViewDataHyperLinkColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="Reminder" FieldName="ContactMessage" VisibleIndex="4" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"  />

                                            <dxwgv:GridViewDataTextColumn Caption="Remove" FieldName="ReminderLinkId" VisibleIndex="5" Width="70px" UnboundType="String">
                                                <DataItemTemplate>
                                                    <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ReminderLink_Init">
                                                    </dxe:ASPxHyperLink>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>        

                                        </Columns>
                                    </dxwgv:ASPxGridView>

                                </dxe:ContentControl>

                            </ContentCollection>

                        </dxtc:TabPage>

                        <dxtc:TabPage Text="Outstanding Campaign Invoices">

                            <ContentCollection>

                                <dxe:ContentControl runat="server">

                                    <dxwgv:ASPxGridView ID="gridCampaigns" ClientInstanceName="gridCampaigns" runat="server" width="100%" DataSourceID="dsCampaigns" style="margin-top:10px">

                                        <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                                            ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                                            ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True" />

                                        <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                
                                        <Styles>
                                            <Header HorizontalAlign="Center" Wrap="True"  />
                                            <Cell HorizontalAlign="Center" Wrap="True"  Font-Size="Small" VerticalAlign="Top"  />
                                        </Styles>

                                        <SettingsPager Mode="ShowPager" PageSize="6" AllButton-Visible="true" >
                                        </SettingsPager>

                                        <Columns>
                                    
                                            <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" Width="50px" />

                                            <dxwgv:GridViewDataDateColumn Caption="Invoice Number" FieldName="InvoiceNumber" VisibleIndex="1"  Width="125px"/>

                                            <dxwgv:GridViewDataHyperLinkColumn Caption="Customer" FieldName="CustomerId" VisibleIndex="2" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                                <PropertiesHyperLinkEdit NavigateUrlFormatString="ViewCustomerDetails.aspx?0{0}" Target="_self"  TextField="CustomerName">
                                                    <Style Font-Size="Small" HorizontalAlign="Left">
                                                    </Style>
                                                </PropertiesHyperLinkEdit>
                                            </dxwgv:GridViewDataHyperLinkColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="Reward" FieldName="RewardType" VisibleIndex="3" />

                                            <dxwgv:GridViewDataTextColumn Caption="Action" FieldName="Id" VisibleIndex="4" Width="70px" UnboundType="String">
                                                <DataItemTemplate>
                                                    <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="CampaignLink_Init">
                                                    </dxe:ASPxHyperLink>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>        

                                        </Columns>
                                    </dxwgv:ASPxGridView>

                                </dxe:ContentControl>

                            </ContentCollection>

                        </dxtc:TabPage>

                        <dxtc:TabPage Text="Outstanding Visit Actions">

                            <ContentCollection>

                                <dxe:ContentControl runat="server">

                                      <dxwgv:ASPxGridView ID="gridVARDetail" runat="server" AutoGenerateColumns="False" DataSourceID="dsOutstandingActions" Width="100%">

                                        <Styles>
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center" />
                                            <Cell Font-Size="X-Small" HorizontalAlign="Center" Wrap="True" />
                                            <LoadingPanel ImageSpacing="10px" />
                                        </Styles>
       
                                        <SettingsPager PageSize="120" Mode="ShowAllRecords">
                                        </SettingsPager>
            
                                        <Settings
                                            ShowFooter="False" ShowHeaderFilterButton="False"
                                            ShowStatusBar="Hidden" ShowTitlePanel="True"
                                            ShowGroupFooter="VisibleIfExpanded" 
                                            UseFixedTableLayout="True" VerticalScrollableHeight="300" 
                                            ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False" ShowFilterRowMenu="False" />
                
                                        <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                                            ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
                                        <Images>
                                            <CollapsedButton Height="12px" Width="11px" />
                                            <ExpandedButton Height="12px" Width="11px" />
                                            <DetailCollapsedButton Height="12px" Width="11px" />
                                            <DetailExpandedButton Height="12px" Width="11px" />
                                            <FilterRowButton Height="13px" Width="13px" />
                                        </Images>

                                        <Columns>
                
                                            <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0">
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="Activity" FieldName="Activity" VisibleIndex="2" Width="20%">
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="Comments" FieldName="Comments" VisibleIndex="3" Width="30%">
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="Responsibility" FieldName="Responsibility" VisibleIndex="4">
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataDateColumn Caption="Target Date" FieldName="TargetDate" VisibleIndex="5">
                                            </dxwgv:GridViewDataDateColumn>
    
                                            <dxwgv:GridViewDataDateColumn Caption="Completed Date" FieldName="CompletionDate" VisibleIndex="6">
                                            </dxwgv:GridViewDataDateColumn>

                                        </Columns>

                                    </dxwgv:ASPxGridView>

                                </dxe:ContentControl>

                            </ContentCollection>

                        </dxtc:TabPage>

                    </TabPages>

                    <LoadingPanelStyle ImageSpacing="6px">
                    </LoadingPanelStyle>

                </dxtc:ASPxPageControl>

            </div>

            <div style="width: 310px; left: 710px; position: absolute; top: 113px;">

                <dxe:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" Font-Bold="true" ID="lblQAMTitle" Text="Quick Access Menu"
                    Style="position: relative; top: 80px; z-index: 1;" runat="server">
                </dxe:ASPxLabel>

                <dxe:ASPxButton Font-Size="14px" ID="btnEnterWebsite" runat="server" EnableDefaultAppearance="true" Text="Enter Website" Width="105px" Style="left: 57px; position: relative; top: 80px" />

                <ul class="HeadOfficeList" style="top: 100px; position:relative">
                    <li>
                            <dxe:ASPxHyperLink ID="LinkCRM" runat="server"  Text="Customer Relationship Manager" NavigateUrl="~/ViewCompanies.aspx" Font-Underline="false">
                            </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkCampaignSales" runat="server"  Text="Campaign Sales" NavigateUrl="~/Campaign201601.aspx" Font-Underline="false">
                        </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkVisitActions" runat="server"  Text="Visit Actions" NavigateUrl="~/VisitActions.aspx" Font-Underline="false">
                        </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkMarketingManager" runat="server"  Text="Marketing Manager" NavigateUrl="~/MM.aspx" Font-Underline="false">
                        </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkAdvanceReports" runat="server"  Text="Advanced Reports" NavigateUrl="~/AdvQueryTool.aspx" Font-Underline="false" >
                        </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkMapping" runat="server"  Text="Mapping" NavigateUrl="~/Report6.aspx?map=0"  Font-Underline="false">
                        </dxe:ASPxHyperLink>
                    </li>
                    <li>
                        <dxe:ASPxHyperLink ID="LinkExcludedAccounts" runat="server"  Text="Excluded Accounts" NavigateUrl="~/ViewExcludedAccounts.aspx"  Font-Underline="false">
                        </dxe:ASPxHyperLink>
                    </li>
                </ul>
            </div>

        </div>

     </div>
    </form>

    <asp:SqlDataSource ID="dsReminders" runat="server" SelectCommand="p_ReminderList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCampaigns" runat="server" SelectCommand="p_OutstandingCampaigns" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsOutstandingActions" runat="server" SelectCommand="p_VAR_Details_HomePage" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

</body>

</html>
