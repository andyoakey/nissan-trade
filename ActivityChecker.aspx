<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ActivityChecker.aspx.vb" Inherits="ActivityChecker" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="976px">
                <tr>
                    <td style="width:60%" align="left" valign="middle" > 
                        <asp:Label ID="lblPageTitle" runat="server" Text="Activity Checker" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" Width="180px"   >
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        
        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0"  
            Width="976px" >

            <ContentStyle>
                <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>

            <TabPages>

                <%------------------------------------------------------------------------------------------
                ' TAB 1
                ------------------------------------------------------------------------------------------%>
                <dxtc:TabPage Text="Monthly Checker">

                    <ContentCollection>

                        <dxw:ContentControl ID="ContentControl1" runat="server">

                            <dxwgv:ASPxGridView ID="gridSubmissions" runat="server"    DataSourceID="dsSubmissions"   KeyFieldName="Centre"
                                Font-Size="7pt" AutoGenerateColumns="False" Width="976px">
                                
                                <Styles >
                                    <Header  Font-Bold = "True" ImageSpacing="5px" SortingImageSpacing="5px" 
                                        Font-Size = "7pt">
                                    </Header>
                                    <Cell   >
                                    </Cell>
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                </Styles>
                                
                                <ImagesFilterControl>
                                    <LoadingPanel>
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                
                                <Images >
                                    <LoadingPanelOnStatusBar>
                                    </LoadingPanelOnStatusBar>
                                    <LoadingPanel>
                                    </LoadingPanel>
                                </Images>
                                
                                <SettingsPager PageSize = "16" Mode="ShowAllRecords" Visible="False">
                                    <AllButton Visible="True">
                                    </AllButton>
                                    <FirstPageButton Visible="True">
                                    </FirstPageButton>
                                    <LastPageButton Visible="True">
                                    </LastPageButton>
                                </SettingsPager>
                                
                                <Settings ShowFooter="False" ShowHeaderFilterButton="False" 
                                    ShowTitlePanel="False" UseFixedTableLayout="True" 
                                    ShowVerticalScrollBar="True" VerticalScrollableHeight="400" />
                                
                                <Columns>
                                  
                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="Centre" 
                                        VisibleIndex="0" Width="5%">
                                        <HeaderStyle Wrap="False" />
                                        <CellStyle HorizontalAlign="Left" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" 
                                        VisibleIndex="1" Width="15%">
                                        <HeaderStyle Wrap="False" />
                                        <CellStyle HorizontalAlign="Left" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Hit Rate" FieldName="DayInfo" 
                                        VisibleIndex="1" Width="5%">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Total Hits" FieldName="TotalHits" 
                                        VisibleIndex="1" Width="5%">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn FieldName="d1" VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d2" VisibleIndex="3">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d3" VisibleIndex="4">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d4" VisibleIndex="5">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d5" VisibleIndex="6">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d6" VisibleIndex="7">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d7" VisibleIndex="8">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d8" VisibleIndex="9">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d9" VisibleIndex="10">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d10" VisibleIndex="11">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d11" VisibleIndex="12">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d12" VisibleIndex="13">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d13" VisibleIndex="14">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d14" VisibleIndex="15">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d15" VisibleIndex="16">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d16" VisibleIndex="17">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d17" VisibleIndex="18">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d18" VisibleIndex="19">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d19" VisibleIndex="20">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d20" VisibleIndex="21">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d21" VisibleIndex="22">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d22" VisibleIndex="23">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d23" VisibleIndex="24">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d24" VisibleIndex="25">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d25" VisibleIndex="26">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d26" VisibleIndex="27">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d27" VisibleIndex="28">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d28" VisibleIndex="29">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d29" VisibleIndex="30">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d30" VisibleIndex="31">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="d31" VisibleIndex="32">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                        <CellStyle Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                </Columns>
                                
                                <Templates>
                                
                                    <DetailRow >

                                        <br />
                                        <dxwgv:ASPxGridView ID="gridDrillDown" runat="server" AutoGenerateColumns="False"
                                            DataSourceID="dsDrillDown" Font-Bold="False"   Font-Size="8pt"
                                            
                                            Font-Strikeout="False" 
                                            OnBeforePerformDataSelect="gridDrillDown_BeforePerformDataSelect" 
                                            Width="860px">
                                            
                                            <Styles >
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>

                                            <SettingsPager>
                                                <AllButton Visible="True">
                                                </AllButton>
                                            </SettingsPager>
                                            
                                            <ImagesFilterControl>
                                                <LoadingPanel >
                                                </LoadingPanel>
                                            </ImagesFilterControl>

                                            <Images >
                                                <LoadingPanelOnStatusBar>
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel>
                                                </LoadingPanel>
                                            </Images>

                                            <SettingsBehavior ColumnResizeMode="Control" />

                                            <Settings ShowFilterRow="true" ShowFilterRowMenu="True" ShowFooter="True" 
                                                ShowHeaderFilterButton="false" />

                                            <Columns>
                                            
                                                <dxwgv:GridViewDataTextColumn Caption="User" CellStyle-HorizontalAlign="left" FieldName="UserName"
                                                    VisibleIndex="0" Width="150px">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn>
                                                
                                                <dxwgv:GridViewDataTextColumn Caption="Date/Time" 
                                                    CellStyle-HorizontalAlign="left" FieldName="TimeStamp"
                                                    VisibleIndex="1" Width="220px">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn>

                                                <dxwgv:GridViewDataTextColumn Caption="Action" CellStyle-HorizontalAlign="left" FieldName="Notes"
                                                    VisibleIndex="2">
                                                    <CellStyle HorizontalAlign="Left">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn>

                                            </Columns>
                                            
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                            
                                            <SettingsDetail IsDetailGrid="True" ExportMode="Expanded" />
                                            
                                        </dxwgv:ASPxGridView>
                                        <br />
                                        
                                    </DetailRow>

                                </Templates>

                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>
                               
                                <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" 
                                    ExportMode="Expanded" />

                            </dxwgv:ASPxGridView>
                            <br />
        
                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>


                <%------------------------------------------------------------------------------------------
                ' TAB 2
                ------------------------------------------------------------------------------------------%>
                <dxtc:TabPage Text="YTD/MTD Summary">

                    <ContentCollection>

                        <dxw:ContentControl ID="ContentControl5" runat="server">
                        
                            <dxwgv:ASPxGridView ID="gridSummary" runat="server"  
                                
 DataSourceID="dsYTDSummary"   
                                Font-Size="7pt" AutoGenerateColumns="False" Width="976px">
                                
                                <Styles>
                                    <Header  Font-Bold = "True" ImageSpacing="5px" SortingImageSpacing="5px" 
                                        Font-Size = "7pt">
                                    </Header>
                                    <Cell   >
                                    </Cell>
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                </Styles>
                                
                                <ImagesFilterControl>
                                    <LoadingPanel>
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                
                                <Images>
                                    <LoadingPanelOnStatusBar >
                                    </LoadingPanelOnStatusBar>
                                    <LoadingPanel>
                                    </LoadingPanel>
                                </Images>
                                
                                <SettingsPager PageSize = "16" Mode="ShowAllRecords" Visible="False">
                                    <AllButton Visible="True">
                                    </AllButton>
                                    <FirstPageButton Visible="True">
                                    </FirstPageButton>
                                    <LastPageButton Visible="True">
                                    </LastPageButton>
                                </SettingsPager>
                                
                                <Settings ShowFooter="False" ShowHeaderFilterButton="False" 
                                    ShowTitlePanel="False" UseFixedTableLayout="True" 
                                    ShowVerticalScrollBar="True" VerticalScrollableHeight="400" />
                                
                                <Columns>
                                  
                                    <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="Centre" 
                                        VisibleIndex="0" Width="5%">
                                        <HeaderStyle Wrap="False" />
                                        <CellStyle HorizontalAlign="Left" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" 
                                        VisibleIndex="1" Width="15%">
                                        <HeaderStyle Wrap="False" />
                                        <CellStyle HorizontalAlign="Left" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                    <dxwgv:GridViewDataTextColumn Caption="Logins (MTD)" FieldName="MTD1" 
                                        VisibleIndex="1" >
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Activity (MTD)" FieldName="MTD2" 
                                        VisibleIndex="1">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Logins (YTD)" FieldName="YTD1" 
                                        VisibleIndex="1" >
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>

                                    <dxwgv:GridViewDataTextColumn Caption="Activity (YTD)" FieldName="YTD2" 
                                        VisibleIndex="1">
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center" Wrap="False">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    
                                </Columns>

                            </dxwgv:ASPxGridView>
                            <br />

                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 3
                ------------------------------------------------------------------------------------------%>
                <dxtc:TabPage Text="Web User Detail">

                    <ContentCollection>

                        <dxw:ContentControl ID="ContentControl3" runat="server">

                            <table id="Table1" width="976px">
                                <tr id="Tr1" runat="server" style="height:30px;">
                                    <td style="font-size: small;" align="left" >
                                        <asp:DropDownList ID="ddlUsers" runat="server" AutoPostBack="True" Width="180px"   >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <br />

                            <dxwgv:ASPxGridView ID="gridActivity" runat="server" DataSourceID="dsActivityLog"
                                AutoGenerateColumns="False" Width="976px">
                                <Styles>
                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                    </Header>
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                </Styles>
                                <SettingsPager PageSize="32">
                                    <AllButton Visible="True">
                                    </AllButton>
                                    <FirstPageButton Visible="True">
                                    </FirstPageButton>
                                    <LastPageButton Visible="True">
                                    </LastPageButton>
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" ShowGroupedColumns="True"
                                    ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                                    ShowStatusBar="Hidden" ShowTitlePanel="True" />
                                <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                <ImagesFilterControl>
                                    <LoadingPanel >
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                <Images>
                                    <CollapsedButton Height="12px" 
                                         
                                        Width="11px" />
                                    <ExpandedButton Height="12px" 
                                         
                                        Width="11px" />
                                    <DetailCollapsedButton Height="12px" 
                                         
                                        Width="11px" />
                                    <DetailExpandedButton Height="12px" 
                                        
                                        Width="11px" />
                                    <FilterRowButton Height="13px" Width="13px" />
                                </Images>
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="UserName" ReadOnly="True"
                                        VisibleIndex="0" Width="150px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Date / Time" FieldName="TimeStamp" VisibleIndex="1"
                                        Width="175px">
                                        <HeaderStyle Wrap="True" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Level" FieldName="SelectionLevel" ReadOnly="True"
                                        VisibleIndex="2" Width="75px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Detail" FieldName="SelectionId" ReadOnly="True"
                                        VisibleIndex="3" Width="175px">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Page" FieldName="Screen" ReadOnly="True"
                                        VisibleIndex="4">
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Notes" FieldName="Notes" ReadOnly="True"
                                        VisibleIndex="5">
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <br />
                            
                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>

            </TabPages>

            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>

        </dxtc:ASPxPageControl>
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsSubmissions" runat="server" SelectCommand="p_Full_Activity_Grid" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsDrillDown" runat="server" SelectCommand="p_Full_Activity_Grid_DrillDown" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="DealerCode" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsYTDSummary" runat="server" SelectCommand="p_Full_Activity_Grid_YTD" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsActivityLog" runat="server" SelectCommand="p_ActivityLog_Sel" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nUserId" SessionField="ActivityUser" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>


