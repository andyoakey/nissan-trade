<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewCompanies.aspx.vb" Inherits="ViewCompanies" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <script type="text/javascript" language="javascript">
        function ViewCompaniesBT(contentUrl) {
            PopupBT.SetContentUrl(contentUrl);
            PopupBT.SetHeaderText('Select a Business Type');
            PopupBT.SetSize(400, 250);
            PopupBT.Show();
        }

        function ViewCompaniesFocusType(contentUrl) {
            PopupFT.SetContentUrl(contentUrl);
            PopupFT.SetHeaderText('Select a Focus Type');
            PopupFT.SetSize(400, 150);
            PopupFT.Show();
        }
     
        function ViewCompaniesEmail(contentUrl) {
            PopupEmail.SetContentUrl(contentUrl);
            PopupEmail.SetHeaderText('Email Address');
            PopupEmail.SetSize(400, 150);
            PopupEmail.Show();
        }

        function HideBTEdit() {
            PopupBT.Hide();
            gridCompanies.Refresh();
        }

        function HideFTEdit() {
            PopupFT.Hide();
            gridCompanies.Refresh();
        }

        function HideEmailEdit() {
            PopupEmail.Hide();
            gridCompanies.Refresh();
        }


        function ReAllocWindow(contentUrl) {
            PopupReAlloc.SetContentUrl(contentUrl);
            PopupReAlloc.SetHeaderText('Select a new customer record');
            PopupReAlloc.SetSize(850, 400);
            PopupReAlloc.Show();
        }
        function HideReAlloc() {
            PopupReAlloc.Hide();
            gridCompanies.Refresh();
        }
        function HideReAllocAndConf() {
            PopupReAlloc.Hide();
            gridCompanies.Refresh();
        }
    </script>

    <dx:ASPxPopupControl 
        ID="PopFilter" 
        ClientInstanceName="PopFilter" 
        Width="610px"
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Select filters and press 'Go'"
        Modal="False" 
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">

                <table id="trFilter" width="100%">
    
                    <tr>
                        <td align="left" valign="middle" style="width:55%" >
                            <dx:ASPxLabel 
                                ID="lblFilter1" 
                                runat ="server" 
                                Text="Business Name" />
                        </td>

                         <td align="left" valign="middle" >
                                <dx:ASPxLabel 
                                ID="lblFilter2" 
                                runat ="server" 
                                Text="Postcode" />
                          </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxTextBox 
                                ID="txtFiltBusinessName" 
                                runat="server" />
                        </td>

                         <td align="left" valign="middle" >
                            <dx:ASPxTextBox 
                                ID="txtFiltPostCode" 
                                runat="server" />
                        </td>
                       </tr>

                    <tr>
                        <td align="left" valign="middle">
                              <dx:ASPxLabel 
                                ID="lblFilter3" 
                                runat ="server" 
                                Text="Customer Type" />
                        </td>
                         <td align="left" valign="middle" >
                              <dx:ASPxLabel 
                                ID="lblFilter4" 
                                runat ="server" 
                                Text="Business Type" />
                        </td>
                    </tr>

                    <tr>
                    <td>

                    </td>
                    </tr>

                    <tr>
                        <td align="left" valign="top">
                            <dx:ASPxCheckBoxList
                                id ="chkFilterCustType" 
                                runat="server">
                               <Items>
                                   <dx:ListEditItem Text="Active" Value="0" Selected="True"/>
                                   <dx:ListEditItem Text="In-Active" Value="1"/>
                                   <dx:ListEditItem Text="Lapsed" Value="2"/>
                                   <dx:ListEditItem Text="Prospect" Value="3"/>
                                   <dx:ListEditItem Text="New Prospect" Value="4"/>
                               </Items>
                            </dx:ASPxCheckBoxList>
                        </td>
                         <td align="left" valign="top" >
                            <dx:ASPxCheckBoxList
                                id ="chkFilterBusType" 
                                runat="server">
                               <Items>
                                    <dx:ListEditItem Text="Bodyshop" Value="1"/>
                                    <dx:ListEditItem Text="Breakdown Recovery Services" Value="2"/>
                                    <dx:ListEditItem Text="Car Dealer New/Used" Value="3" />
                                    <dx:ListEditItem Text="Independent Motor Trader" Value="4" />
                                    <dx:ListEditItem Text="Mechanical Specialist" Value="5" />
                                    <dx:ListEditItem Text="Motor Factor" Value="6" />
                                    <dx:ListEditItem Text="Parts Supplier" Value="7" />
                                    <dx:ListEditItem Text="Taxi & Private Hire" Value="8" />
                                    <dx:ListEditItem Text="Windscreens" Value="9" />
                                    <dx:ListEditItem Text="Other" Value="99" />
                               </Items>
                            </dx:ASPxCheckBoxList>
                        </td>
                    </tr>

                    <tr>
                    <td>

                    </td>
                    </tr>


                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxLabel 
                                ID="lblFilter5" 
                                runat ="server" 
                                Text="Customer Type" />
                        </td>
                         <td align="left" valign="middle" style="margin-left:10px">
                               <dx:ASPxLabel 
                                    ID="lblFilter6" 
                                    runat ="server" 
                                    Text="Options" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxComboBox
                                id ="ddlType" 
                               AutoPostBack="true"
                               runat="server">
                               <Items>
                                    <dx:ListEditItem Text="All Customers"  Value="0" Selected="true"/>
                                    <dx:ListEditItem Text ="All Focus" Value ="1"/>
                                    <dx:ListEditItem Text ="Dealer Focus Only" Value ="2"/>
                                    <dx:ListEditItem Text ="OEC Focus Only"  Value ="3"/>
                                    <dx:ListEditItem Text ="Not Focus" Value ="4"/>
                                </Items>
                            </dx:ASPxComboBox>
                        </td>

                         <td align="left" valign="middle" style="margin-left:10px">
                             <dx:ASPxCheckBox 
                                 ID="chkShowAddress" 
                                 runat="server" 
                                 Text="Show address info" 
                                 TextAlign="right"/>

                             <dx:ASPxCheckBox 
                                 ID="chkShowHidden" 
                                 runat="server" 
                                 Text="Include hidden" 
                                 TextAlign="right"/>
                        </td>
                    </tr>

                    <tr>
                       <td align="left">
                        </td>
                        
                        <td align="right" valign="middle" style="margin-left:10px">
                        
                            <dx:ASPxButton ID="btnReset" runat="server" Text="Reset"  />

                            <dx:ASPxButton ID="btnSearch" runat="server" Text="Go" />
                        
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupBT" 
        ClientInstanceName="PopupBT" 
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Update Business Type"
        Modal="True" 
        AllowDragging="true"
        AllowResize="true"
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupEmail" 
        ClientInstanceName="PopupEmail" 
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Update Email Address"
        Modal="True" 
        AllowDragging="true"
        AllowResize="true"
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol6" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupFT" 
        ClientInstanceName="PopupFT" 
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Update Focus Type"
        Modal="True" 
        AllowDragging="true"
        AllowResize="true"
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="PopupReAlloc" ClientInstanceName="PopupReAlloc" Width="275px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="New Visit Action - Select Centre and Date"
        Modal="True" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="PopupReAllocConf" ClientInstanceName="PopupReAllocConf" Width="100px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="Customer updated"
        Modal="False" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
                    <asp:Label ID="lblConf" runat="server" Text="Customer updated." Font-Size="13px" Font-Names="Calibri,Verdana"> </asp:Label>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
            <table id="trCustomerDetails" width="100%" >
                <tr>
                    <td align="left" width="20%" >                  
                        <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Customer Listing" />
                     </td>

                    <td align="left" width="60%" >                  
                     </td>

                    <td width="5%">
                        <dx:ASPxButton 
                            ID="btnConsent" 
                            runat="server" 
                            ClientInstanceName="btnConsent" 
                            Text="Customer Consent Manager" >
                        </dx:ASPxButton>
                    </td>

                    <td width="5%">
                        <dx:ASPxButton 
                            ID="btnMM" 
                            runat="server" 
                            ClientInstanceName="btnMM" 
                            Text="Marketing Manager" 
                            Visible="False" >
                        </dx:ASPxButton>
                    </td>

                    <td width="4%" align="right" > 
                        <dx:ASPxButton  
                            ID="btnMenu" 
                            runat="server" 
                            ClientInstanceName="btnMenu" 
                            ToolTip="Search & Filtering Options" 
                            Text="..." 
                            width="30px"
                            Visible="True">
                        </dx:ASPxButton>
                   </td>

                </tr>
            </table>
        </div>
        
        <table Width="100%" style="position:relative; top:10px">
            <tr>
            <td Width="100%" >
                <dx:ASPxGridView 
            ID="gridCompanies" 
            runat="server" 
            ClientInstanceName="gridCompanies" 
            AutoGenerateColumns="False" 
            KeyFieldName="Id" 
            Width="100%"
            OnLoad="GridStyles" 
            cssclass="grid_styles"
            OnDetailRowGetButtonVisibility="gridCompanies_DetailRowGetButtonVisibility"
            OnCustomColumnDisplayText="gridCompanies_OnCustomColumnDisplayText">
                        
            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowHeaderFilterButton="False" />
            
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16" />
            
            <SettingsDetail 
                AllowOnlyOneMasterRowExpanded="True" 
                ShowDetailRow="True" 
                ExportMode="Expanded" />
            
            <SettingsBehavior AllowSelectByRowClick="True" />
            
            <Styles>
                <Header Wrap="True" 
                    HorizontalAlign="Center" 
                    VerticalAlign="Middle" />
            </Styles>

            <Columns>

                <dx:GridViewDataTextColumn 
                    FieldName="Id" 
                    Caption="ID" 
                    VisibleIndex="0" 
                    visible="false"  
                    ExportWidth="100" />

                <dx:GridViewDataHyperLinkColumn 
                    Caption="View" 
                    FieldName="Id" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="1" 
                    Width="3%">
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="ViewCustomerDetails.aspx?0{0}" Target="_self"  Text="View" Style-ForeColor="Black">
                        <Style HorizontalAlign="Center"/>
                    </PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="BusinessName" 
                    Caption="Name" 
                    VisibleIndex="2" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-Wrap="False"
                    Width="5%" 
                    ExportWidth="300" >
                </dx:GridViewDataTextColumn>        
                
                <dx:GridViewDataTextColumn 
                    FieldName="FormattedAddress" 
                    Caption="Address" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    VisibleIndex="3" 
                    Visible="false"
                    Width="20%" 
                    ExportWidth="400" />
                
                <dx:GridViewDataTextColumn 
                    FieldName="PostCode" 
                    Caption="Postcode" 
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Visible="true"
                    Width="5%" 
                    VisibleIndex="4"  
                    ExportWidth="150" />
                
                <dx:GridViewDataTextColumn 
                    Caption="Business Type" 
                    FieldName="BusinessTypeDescription" 
                    Name="BusinessTypeDescriptionEdit" 
                    VisibleIndex="5" 
                    Visible="true"
                    Width="5%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="200">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink 
                            ID="hyperLink" 
                             runat="server" 
                             OnInit="BusinessType_Init" >
                        </dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>        

                <dx:GridViewDataTextColumn 
                    Caption="Business Type" 
                    FieldName="BusinessTypeDescription" 
                    Name="BusinessTypeDescriptionView" 
                    VisibleIndex="5" 
                    Visible="true"
                    Width="5%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="200">
                </dx:GridViewDataTextColumn>        

                <dx:GridViewDataTextColumn 
                    FieldName="CustomerStatus" 
                    Caption="Customer Type" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="6" 
                    Visible="true"
                    Width="5%" 
                    ExportWidth="200" />

                   <dx:GridViewDataTextColumn 
                    Caption="Focus ?" 
                    FieldName="focustype" 
                    Name="focustypeEdit" 
                    VisibleIndex="7" 
                    Visible="true"
                    Width="5%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="200">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink 
                            ID="hyperLink" 
                             runat="server" 
                             OnInit="Focus_Init" >
                        </dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>        

                <dx:GridViewDataTextColumn 
                    FieldName="VanRoute" 
                    Caption="Van Route" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Visible="false"
                    Width="4%" 
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    FieldName="DriveTime" 
                    Name="DriveTime" 
                    Caption="Drive Time" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="9" 
                    Visible="false"
                    Width="4%" 
                    ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Distance" 
                    Name="Distance" 
                    Caption="Distance" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="10" 
                    Visible="false"
                    Width="4%" 
                    ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="#,##0.0" />
                </dx:GridViewDataTextColumn>
                    
                <dx:GridViewDataTextColumn 
                    FieldName="InCDA" 
                    Caption="In Territory?" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                     ToolTip="Is this Customer is located inside this Dealer's Allocated Territory ?"
                    VisibleIndex="11" 
                    Visible="false"
                    Width="5%" 
                    ExportWidth="80" />

                <dx:GridViewDataDateColumn 
                    FieldName="LastSpend" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Caption="Last Purchase" 
                    Visible="true"
                    Width="4%" 
                    VisibleIndex="12" 
                    ExportWidth="120" />

                <dx:GridViewDataTextColumn 
                    Caption="Total MTD" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    FieldName="SpendMTD" 
                    Tooltip="Spend This Month To Date"
                    VisibleIndex="13"  
                    Visible="true"
                    Width="4%" 
                    ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="Total YTD" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    FieldName="SpendYTD" 
                    Tooltip="Spend This Year To Date"
                    VisibleIndex="14"  
                    Visible="true"
                    Width="5%" 
                    ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="TelephoneNumber" 
                    Caption="Phone" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="15"  
                    Width="8%" 
                    CellStyle-Wrap="False"
                    ExportWidth="200" 
                    Visible="false" />
                
                <dx:GridViewDataTextColumn 
                    FieldName="EmailAddress" 
                    Caption="Email" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    VisibleIndex="16"  
                    ExportWidth="300" 
                    Width="15%"
                    Visible="true" >
                       <DataItemTemplate>
                            <dx:ASPxHyperLink 
                                ID="hyperLink" 
                                 runat="server" 
                                 OnInit="Email_Init" >
                            </dx:ASPxHyperLink>
                    </DataItemTemplate>
                   </dx:GridViewDataTextColumn>

                  <dx:GridViewDataTextColumn 
                    FieldName="Email_Status" 
                    Caption="Email Status" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    VisibleIndex="17"  
                    ExportWidth="300" 
                    ToolTip="Valid means not blank, correctly formatted and not returned as Rejected. Where an email has been captured but Customer has opted out this is counted as valid."
                    Width="10%"
                    Visible="true" />

                <dx:GridViewCommandColumn 
                    ShowSelectCheckbox="True" 
                    VisibleIndex="17" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Caption="Select" 
                    Width="3%"/>


            </Columns>
            
            <Templates>
            
                <DetailRow >

                    <br />
                    <dx:ASPxGridView 
                        ID="gridAccounts" 
                        OnLoad="GridStyles" 
                        cssclass="grid_styles"
                        runat="server" 
                        AutoGenerateColumns="False"
                        DataSourceID="dsLinks" 
                        KeyFieldName="CustomerDealerId" 
                        Font-Strikeout="False" 
                        OnBeforePerformDataSelect="gridAccounts_BeforePerformDataSelect" 
                        Width="100%">
                        
                        <Styles>
                            <Header HorizontalAlign="Center" Wrap="True">
                            </Header>
                            <Cell HorizontalAlign="Left" Font-Size="X-Small">
                            </Cell>
                        </Styles>

                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>

                        <SettingsBehavior ColumnResizeMode="Control" />

                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowFooter="false" ShowHeaderFilterButton="false" />
                            
                        <Columns>
                        
                            <dx:GridViewDataTextColumn Caption="Move" FieldName="CustomerDealerId" ExportWidth="0" 
                                EditFormSettings-Visible="False" VisibleIndex="0" UnboundType="String" Width="55px">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ReAlloc_Init">
                                    </dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>        

                            <dx:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" Width="75px" >
                                <CellStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Account" FieldName="AccountNumber" VisibleIndex="1" Width="125px" />

                            <dx:GridViewDataTextColumn Caption="Name" FieldName="BusinessName" VisibleIndex="2" />

                            <dx:GridViewDataTextColumn Caption="Address" FieldName="FormattedAddress" VisibleIndex="3" />

                            <dx:GridViewDataTextColumn Caption="Postcode" FieldName="PostCode" VisibleIndex="4" Width="75px">
                                <CellStyle HorizontalAlign="Center" />
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Total MTD" FieldName="SpendMTD" VisibleIndex="5" Width="75px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                                <CellStyle HorizontalAlign="Right" />
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Total YTD" FieldName="SpendYTD" VisibleIndex="6" Width="75px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                                <CellStyle HorizontalAlign="Right" />
                            </dx:GridViewDataTextColumn>

                        </Columns>
                        
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                        
                        <SettingsDetail  ExportMode="Expanded" />
                        
                    </dx:ASPxGridView>
                    <br />
                    
                </DetailRow>

            </Templates>

        </dx:ASPxGridView>
            </td>
            </tr>
         </table>    
             
        <table Width="100%" style="position:relative; top:10px">
            <tr>
                <td width="90%">
                </td>

                <td width="5%">
                    <dx:ASPxButton ID="btnHide" runat="server" Text="Hide" >
                    </dx:ASPxButton>
                </td>

                <td width="5%">
                    <dx:ASPxButton ID="btnDup" runat="server" Text="De-dupe" align="right" >
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <br />

   </div>
                
    <asp:SqlDataSource ID="dsLinks" runat="server" SelectCommand="p_CustomerDealerList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nCustomerId" SessionField="CustomerId" Type="String" Size="12" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridCompanies">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter2" 
        runat="server" 
        GridViewID="gridAccounts">
    </dx:ASPxGridViewExporter>

</asp:Content>
