﻿Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting

Partial Class MarginReport2011

    Inherits System.Web.UI.Page

    Dim nSale1 As Double
    Dim nCost1 As Double
    Dim nSale2 As Double
    Dim nCost2 As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "qubeDATA PARTS - Margin Report 2011"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Margin Report 2011", "Viewing Margin Report 2011")
        End If

    End Sub

    Protected Sub dsMargin_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsMargin.Init
        dsMargin.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridMargin_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridMargin.HtmlDataCellPrepared

        Dim nThisValue As Decimal

        If e.DataColumn.Index = 2 Then
            If e.CellValue = "Summary" Then
                e.Cell.Font.Bold = True
            End If
        End If

        If e.DataColumn.Index >= 3 And e.DataColumn.Index <= 5 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index >= 6 And e.DataColumn.Index <= 8 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 9 Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            If nThisValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            ElseIf nThisValue < 0 Then
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

    Private Sub RenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)
        e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        Else

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Index = 2 Then
                If e.TextValue = "Summary" Then
                    e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Bold)
                End If
            End If

            If e.Column.Index >= 3 And e.Column.Index <= 5 Then
                e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                If e.Column.Index = 5 Then
                    e.TextValueFormatString = "p2"
                Else
                    e.TextValueFormatString = "c0"
                End If
            End If

            If e.Column.Index >= 6 And e.Column.Index <= 8 Then
                e.BrickStyle.BackColor = System.Drawing.Color.LightGreen
                If e.Column.Index = 8 Then
                    e.TextValueFormatString = "p2"
                Else
                    e.TextValueFormatString = "c0"
                End If
            End If

            If e.Column.Index = 9 Then
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value)
                End If
                If nThisValue > 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Green
                ElseIf nThisValue < 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                End If
            End If

        End If

    End Sub



    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call RenderBrick(e)
    End Sub

End Class
