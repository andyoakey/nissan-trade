﻿Imports System.Data
Imports System.Net.Mail

Partial Class VisitActionActivityNew

    Inherits System.Web.UI.Page

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewVisitLine.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim nVARId = Val(Request.QueryString(0))
        Dim sTargetDate As String = Format(CDate(txtTargetDate.Text), "dd-MMM-yyyy")

        If txtActivity.Text <> "" And txtComments.Text <> "" And txtResponsibility.Text <> "" And sTargetDate <> "" Then

            htIn.Add("@nVARId", nVARId)
            htIn.Add("@sActivity", txtActivity.Text)
            htIn.Add("@sComments", txtComments.Text)
            htIn.Add("@sResponsibility", txtResponsibility.Text)
            htIn.Add("@sTargetDate", sTargetDate)
            htIn.Add("@nUserId", Session("UserId"))

            nRes = da.Update(sErr, "p_VAR_Detail_Ins", htIn)

            If sErr.Length = 0 Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Visit Activity added", "Id : " & nVARId)
                Dim startUpScript As String = String.Format("window.parent.HideVisitActionActivityWindow();")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
            Else
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If
        End If

    End Sub

End Class
