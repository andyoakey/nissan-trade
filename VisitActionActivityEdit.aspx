﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VisitActionActivityEdit.aspx.vb" Inherits="VisitActionActivityEdit" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

    <table style="width:660px">

        <tr>
            <td style="padding-top:10px; vertical-align:top">
                <asp:Label ID="Label2" runat="server" Text="Activity : " Font-Names="Calibri,Verdana"></asp:Label>
            </td>
            <td style="padding-left:3px; padding-top:10px; vertical-align:top">
                <asp:TextBox ID="txtActivity" runat="server" Width="540px" Font-Names="Calibri,Verdana" TabIndex="0"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="vertical-align:top; padding-top:10px; vertical-align:top">
                <asp:Label ID="Label5" runat="server" Text="Comments : " Font-Names="Calibri,Verdana"></asp:Label>
            </td>
            <td style="padding-left:3px; padding-top:10px; vertical-align:top">
                <asp:TextBox ID="txtComments" Width="535px" Font-Names="Calibri,Verdana" Height="98px" runat="server" TextMode="MultiLine" TabIndex="1"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="padding-top:10px; vertical-align:top">
                <asp:Label ID="Label6" runat="server" Text="Responsibility : " Font-Names="Calibri,Verdana"></asp:Label>
            </td>
            <td style="padding-left:3px;padding-top:10px; vertical-align:top">
                <asp:TextBox ID ="txtResponsibility" runat="server" Width="120px" Font-Names="Calibri,Verdana" TabIndex="2">
                </asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="padding-top:15px; vertical-align:top">
                <asp:Label ID="Label4" runat="server" Text="Target Date : " Font-Names="Calibri,Verdana"></asp:Label>
            </td>
            <td style="padding-top:10px">
                <table>
                    <tr>
                        <td>
                            <dxe:ASPxDateEdit ID="txtTargetDate" runat="server" Width="120px" Font-Names="Calibri,Verdana" TabIndex="3" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                            </dxe:ASPxDateEdit>
                        </td>
                        <td>
                            <dxe:ASPxButton ID="btnNewVisitLine" runat="server" ClientInstanceName="btnNewVisitLine" ImagePosition="Top" Text="Save" Width="90px" Font-Names="Calibri,Verdana" TabIndex="4">
                            </dxe:ASPxButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    </form>
</body>
</html>
