﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataSubmission.aspx.vb" Inherits="DataSubmission" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
                    
        <table style="width: 100%;" >
            <tr id="Tr3" runat="server" style="height:30px;">
                <td style="font-size: small;" align="left" >
                    <dx:ASPxLabel 
                        Font-Size="Larger"
                        ID="lblPageTitle" 
                        runat="server" 
                        Text="Data Checker" />
                </td>
            
                <td style="font-size: small;" align="right">
                    <dx:ASPxComboBox
                        ID="ddlMonth" 
                        runat="server" 
                        AutoPostBack="True" 
                        Width="180px"   />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxGridView 
                        ID="gridSubmissions" 
                        runat="server" 
                        onload="gridStyles"
                        style="position:relative; left:0px; " 
                        AutoGenerateColumns="False" 
                        Width="100%">
              
                        <SettingsPager PageSize = "20" Visible="True" mode="ShowPager">
                            <AllButton Visible="True">
                            </AllButton>
                            <FirstPageButton Visible="True">
                            </FirstPageButton>
                            <LastPageButton Visible="True">
                            </LastPageButton>
                        </SettingsPager>
            
                        <Settings 
                            ShowFooter="False" 
                            ShowHeaderFilterButton="False" 
                            ShowTitlePanel="False" 
                            UseFixedTableLayout="True" 
                            ShowVerticalScrollBar="False" />
           
                        <Columns>
            
                            <dx:GridViewDataHyperLinkColumn 
                                Caption="Dealer" 
                                FieldName="Centre" 
                                PropertiesHyperLinkEdit-Text="Centre" 
                                PropertiesHyperLinkEdit-Target="_self"
                                PropertiesHyperLinkEdit-TextField="Centre" 
                                PropertiesHyperLinkEdit-TextFormatString="{0}" 
                                PropertiesHyperLinkEdit-NavigateUrlFormatString="DataSubmissionDrillDown.aspx?{0}" 
                                VisibleIndex="0"
                                Width="5%" 
                                ToolTip="Centre">
                    
                                <CellStyle HorizontalAlign="Center"  Wrap="False">
                                </CellStyle>
                    
                                <PropertiesHyperLinkEdit Text="Dealer" Target="_blank"
                                    NavigateUrlFormatString="DataSubmissionDrillDown.aspx?{0}" TextField="Centre">
                                </PropertiesHyperLinkEdit>
                
                                <Settings 
                                    AllowAutoFilter="False" 
                                    AllowHeaderFilter="False" 
                                    ShowFilterRowMenu="False" />
                
                            </dx:GridViewDataHyperLinkColumn>
                
                            <dx:GridViewDataTextColumn 
                                Caption="Name" 
                                FieldName="CentreName" 
                                VisibleIndex="1" Width="14%">
                                <HeaderStyle Wrap="False" />
                                <CellStyle HorizontalAlign="Left" Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                
                             <dx:GridViewDataTextColumn 
                                Caption="DMS" 
                                Visible="false"
                                FieldName="DMS" 
                                VisibleIndex="2" 
                                Width="7%">
                                <HeaderStyle Wrap="False" />
                                <CellStyle HorizontalAlign="Left" Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="d1" Name="d1" VisibleIndex="3">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d2" Name="d2" VisibleIndex="4">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d3" Name="d3" VisibleIndex="5">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d4" Name="d4" VisibleIndex="6">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d5" Name="d5" VisibleIndex="7">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d6" Name="d6" VisibleIndex="8">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d7" Name="d7" VisibleIndex="9">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d8" Name="d8" VisibleIndex="10">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d9" Name="d9" VisibleIndex="11">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d10" Name="d10" VisibleIndex="12">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d11" Name="d11" VisibleIndex="13">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d12" Name="d12" VisibleIndex="14">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d13" Name="d13" VisibleIndex="15">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d14" Name="d14" VisibleIndex="16">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d15" Name="d15" VisibleIndex="17">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d16" Name="d16" VisibleIndex="18">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d17" Name="d17" VisibleIndex="19">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d18" Name="d18" VisibleIndex="20">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d19" Name="d19" VisibleIndex="21">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d20" Name="d20" VisibleIndex="22">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d21" Name="d21" VisibleIndex="23">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d22" Name="d22" VisibleIndex="24">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d23" Name="d23" VisibleIndex="25">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d24" Name="d24" VisibleIndex="26">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d25" Name="d25" VisibleIndex="27">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d26" Name="d26" VisibleIndex="28">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d27" Name="d27" VisibleIndex="29">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d28" Name="d28" VisibleIndex="30">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d29" Name="d29" VisibleIndex="31">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d30" Name="d30" VisibleIndex="32">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="d31" Name="d31" VisibleIndex="33">
                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                <CellStyle Wrap="False">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                
                        </Columns>
           
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
            
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
       
        <br />
        
    </div>

</asp:Content>

