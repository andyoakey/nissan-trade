﻿Imports System.Data
Imports System.Net.Mail
Imports QubeSecurity
Imports QubeUtils

Partial Class Admin_Users_Delete
    Inherits Page

    Dim sEmail As String
    Dim da As New DatabaseAccess, ds As DataSet, dsC As DataSet, sErr, sSQL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("SuperAdmin") Then
            Response.Redirect("Default.aspx")
        End If

        Dim userID As Integer = Int16.Parse(Request.QueryString("uid").ToString())
        Dim wUser As DataTable = WebUser.GetUser(userID)

        Dim sSQL = "UPDATE Web_User SET Deleted = 'Y' WHERE UserId = " & Trim(Str(Request.QueryString(0)))
        Call RunNonQueryCommand(sSQL)

        If wUser.Rows.Count > 0 AndAlso Not String.IsNullOrWhiteSpace(wUser.Rows(0)("GUID").ToString()) Then
            Dim user = Membership.GetUser(Guid.Parse(wUser.Rows(0)("GUID").ToString()))
            QubeSecurity.User.DeleteUser(user)
        End If

        Response.Redirect("Default.aspx?msg=deleted")

    End Sub
    
End Class
