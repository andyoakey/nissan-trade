﻿Imports System.Data
Imports System.Net.Mail
Imports QubeSecurity

Partial Class Admin_Users_Unlock
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        If Not Session("SuperAdmin") Then
            Response.Redirect("Default.aspx")
        End If

        If Request.QueryString IsNot Nothing Then
            Dim userId = Int16.Parse(Request.QueryString("uid"))
            Dim wUser as DataTable = WebUser.GetUser(userId)
        
            If wUser.Rows.Count > 0 Then
                Dim user = Membership.GetUser(Guid.Parse(wUser.Rows(0)("GUID").ToString()))

                QubeSecurity.User.UnlockUser(user)

                Response.Redirect("Default.aspx?msg=unlocked")
            End If

        End If

        Response.Redirect("Default.aspx")    
    End Sub

    
End Class
