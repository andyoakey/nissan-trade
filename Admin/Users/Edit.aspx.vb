﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports DevExpress.Web
Imports QubeSecurity
Imports QubeUtils

Partial Class Admin_Users_Edit
    Inherits Page

    Dim da As New DatabaseAccess
    Dim webUser As DataTable
    Dim errorText, sQLQuery As String
    Dim userId As Integer = 0


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("SuperUser") <> "Y" Then
        If Not Session("SuperAdmin") Then
            Response.Redirect("~/Dashboard.aspx")
        End If

        If Request.QueryString IsNot Nothing Then
            If Request.QueryString("uid") IsNot Nothing Then
                userId = Request.QueryString("uid")
            End If
        End If

        If Not IsPostBack Then
            webUser = FetchUserDetails()
        End If

    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Not IsPostBack Then
            SetFieldVisibility()
            PopulateAccessLevels()

            If webUser IsNot Nothing Then
                If webUser.Rows.Count > 0 Then
                    txtEmailAddress.Text = webUser.Rows(0)("EmailAddress").ToString()
                    txtConfirmEmailAddress.Text = webUser.Rows(0)("EmailAddress").ToString()
                    txtFirstname.Text = webUser.Rows(0)("FirstName").ToString()
                    txtSurname.Text = webUser.Rows(0)("Surname").ToString()
                    ddlAccessLevel.SelectedValue = TRIM(webUser.Rows(0)("AccessLevel").ToString())
                    chkAdmin.Checked = IIf(webUser.Rows(0)("SuperUser").ToString() = "Y", True, False)
                    ddlAccessLevel.Enabled = userId <> Session("UserId")
                End If
            End If

            'If Session("SuperAdmin") <> true Then
            '    ddlAccessLevel.Enabled = false
            'End If

            divAdmin.Visible = Session("SuperAdmin")
        End If
    End Sub


    Private Sub PopulateAccessLevels()
        
        sQLQuery = "p_Admin_AccessLevelOptions"

        Dim param as SqlParameter() = new SqlParameter(0){}
        param(0) = New SqlParameter("@UserId", SqlDbType.Int) With {.Value = Session("UserID")}

        ddlAccessLevel.DataSource = MSSQLHelper.GetDataTable(sQLQuery, param, "", true)
        ddlAccessLevel.DataTextField = "AccessText"
        ddlAccessLevel.DataValueField = "AccessLevel"
        ddlAccessLevel.DataBind()
        

    End Sub


    Function FetchUserDetails() As DataTable
        sQLQuery = "SELECT Web_User.UserId, FirstName, Surname, FirstName + ' ' + Surname as FullName, anm.Email, Web_User.EmailAddress, AccessLevel, Deleted, Blocked, SuperUser, anm.LastLoginDate, anm.LastPasswordChangedDate, anm.IsLockedOut FROM dbo.Web_User INNER JOIN aspnet_Membership anm on anm.UserId = Web_User.GUID WHERE Deleted = 'N' AND Web_User.UserId= " & userId
        
        return MSSQLHelper.GetDataTable(sQLQuery, Nothing)
    End Function


    Sub SetFieldVisibility()

        'If user IsNot Nothing Then
        '    If Boolean.Parse(Session("SuperAdmin")) = True Then
        '        lblConfirmEmailAddress.Enabled = true
        '        txtConfirmEmailAddress.enabled = true
        '        txtEmailAddress.Enabled = true
        '    Else
        '        lblConfirmEmailAddress.Enabled = false
        '        txtConfirmEmailAddress.enabled = false
        '        txtEmailAddress.Enabled = false
        '    End If
        'End If

        
    End Sub

#region "Save"
    Protected Sub btnSaveReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReg.Click

        Dim bErr  = False
        Dim htIn As New Hashtable

        Dim emailAddress As String = Trim(txtEmailAddress.Text)

        lblErr.Visible = False
        lblErr.Text = ""

        If userId = 0 Then

            If emailAddress = "" Then
                lblErr.Text += "Email Address is mandatory" & vbCrLf
                bErr = True
            End If

            If Not bErr And Trim(txtConfirmEmailAddress.Text) = "" Then
                lblErr.Text += "Email Address must be confirmed" & vbCrLf
                bErr = True
            End If

            If Not bErr And emailAddress <> Trim(txtConfirmEmailAddress.Text) Then
                lblErr.Text += "Email addresses do not match" & vbCrLf
                bErr = True
            End If

        End If

        If Not bErr And Trim(txtFirstname.Text) = "" Then
            lblErr.Text += "First Name is required" & vbCrLf
            bErr = True
        End If

        If Not bErr And Trim(txtSurname.Text) = "" Then
            lblErr.Text += "Surname is required" & vbCrLf
            bErr = True
        End If

        If Not bErr And Trim(ddlAccessLevel.SelectedItem.Text) = "" Then
            lblErr.Text += "Access Level is required" & vbCrLf
            bErr = True
        End If

        If Not bErr And userId = 0 Then
            sQLQuery = "SELECT * FROM Web_User WHERE Deleted = 'N' AND EmailAddress = '" & emailAddress & "'"

            webUser = MSSQLHelper.GetDataTable(sQLQuery, Nothing)
            If Not user Is Nothing Then
                If Not bErr And errorText = "" Then
                    If webUser.Rows.Count > 0 Then
                        lblErr.Text += "This Email Address has already been registered on this website." & vbCrLf
                        bErr = True
                    End If
                Else
                    HttpContext.Current.Session("sErr") = errorText
                    HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
                End If
            Else
                HttpContext.Current.Session("sErr") = errorText
                HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            End If
        End If

        If Not bErr Then

            htIn.Add("@FirstName", Trim(txtFirstname.Text))
            htIn.Add("@Surname", Trim(txtSurname.Text))
            htIn.Add("@EmailAddress", emailAddress)
            htIn.Add("@AccessLevel", ddlAccessLevel.SelectedItem.Value)
            htIn.Add("@SuperUser", IIf(chkAdmin.Checked, "Y", "N"))

            Dim isAdmin as Integer = 0
            Dim isSuperAdmin as Integer = 0

            if divAdmin.Visible Then
                isAdmin = IIf(chkAdmin.checked, 1,0)
            End If

            'NEW USER
            If userId = 0 Then
                da.Update(errorText, "p_Admin_Users_New", htIn)

                If errorText.Length > 0 Then
                    HttpContext.Current.Session("sErr") = errorText : HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
                Else
                    Dim status = MembershipCreateStatus.ProviderError
                    Dim returnMessage = ""

                    'Check email against existing users and lockouts
                    If Membership.GetUser(emailAddress) Is Nothing Then
                        returnMessage = CreateUser(emailAddress)

                        If returnMessage.Length > 0 Then
                            lblErr.Text = returnMessage
                            lblErr.Visible = true
                        End If
                    Else
                        If QubeSecurity.User.CheckDeletedUsers(emailAddress) Then
                            lblErr.Text = "User has been permenantly deleted and cannot be re-added into the system using the same email address."
                            lblErr.Visible = true
                        else
                            If Membership.GetUser(emailAddress).IsLockedOut Or Not Membership.GetUser(emailAddress).IsApproved Then
                                lblErr.Text = "User with this email address has either been admin blocked or locked-out of the system."
                                lblErr.Visible = true
                            else
                                returnMessage = CreateUser(emailAddress)

                                If returnMessage.Length > 0 Then
                                    lblErr.Text = returnMessage
                                    lblErr.Visible = true
                                End If
                            End If
                        End If
                    End If
                End If
                
            Else
                'UPDATE USER
                htIn.Add("@UserId", userId)
                da.Update(errorText, "p_Admin_Users_Update", htIn)

                If errortext.Length > 0 Then
                    HttpContext.Current.Session("sErr") = errortext : HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
                Else
                    webUser = QubeSecurity.WebUser.GetUser(userId)

                    Call UpdateMembershipTables(webUser, emailAddress)

                    Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "VC Admin User Updated", "User: '" & Trim(txtEmailAddress.Text) & "'")
                End If
                
            End If

            If lblErr.Text.Length = 0 Then
                Dim startUpScript As String = String.Format("window.parent.HideUpdReminder();window.parent.reload();")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
            End If

        Else
            lblErr.Visible = True
        End If



    End Sub


    Private Sub UpdateMembershipTables(webUser As DataTable, newEmailAddress As string)
        Dim existingUser as MembershipUser = Membership.GetUser(GUID.Parse(webUser.Rows(0)("GUID").ToString()))

        If existingUser isNot Nothing Then
            existingUser.Email = newEmailAddress
            Membership.UpdateUser(existingUser)

            Dim param = New SqlParameter(2){}
            param(0) = New SqlParameter("@NewUsername", SqlDbType.NVarChar,50) With {.Value = newEmailAddress}
            param(1) = New SqlParameter("@NewUsernameLowered", SqlDbType.NVarChar,50) With {.Value = newEmailAddress.ToLower()}
            param(2) = New SqlParameter("@UserId", SqlDbType.UniqueIdentifier) With {.Value = GUID.Parse(webUser.Rows(0)("GUID").ToString())}

            MSSQLHelper.ExecuteSQL("UPDATE aspnet_Users SET UserName=@NewUsername,LoweredUserName=@NewUsernameLowered WHERE UserId=@UserId", param)
        End If

    End Sub


    Function CreateUser(ByVal emailAddress As string) As string

        Dim status = QubeSecurity.User.Create(emailAddress)

        If status = MembershipCreateStatus.Success Then

            Dim user As MembershipUser = Membership.GetUser(emailAddress)

            If user IsNot Nothing Then
                QubeSecurity.WebUser.SetGuid(user)
            End If

            Call SendConfirmationEmail(Trim(txtEmailAddress.Text), Trim(txtFirstname.Text), Trim(txtSurname.Text), ddlAccessLevel.SelectedItem.Value)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Admin User Edited", "New User: '" & Trim(txtEmailAddress.Text) & "' added with access '" & ddlAccessLevel.SelectedItem.Value & "'")
            
            Return ""
        End If

        return QubeSecurity.User.GetErrorMessage(status)
    End Function

    
    Sub SendConfirmationEmail(ByVal sUserEmail As String, ByVal sFirstName As String, ByVal sSurname As String, ByVal sAccessLevel As String)

        Dim sBody As String = ""

        sBody = "To: " & Trim(sFirstName) & " " & sSurname
        sBody += "<br/><br/>You have been granted access to the " & Settings.GetString("WebAppName") &" website - " & Settings.GetString("WebAppURL") & "."
        sBody += "<br/><br/>Your username is your email address. You will need to set a new password by clicking the 'Reset Password' button on the login screen."

        Call SendSupportEmail(sUserEmail, sBody, Settings.GetString("WebAppName") & " - New user account.", True, True)
    End Sub
#End Region



End Class
