<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Groups.aspx.vb" Inherits="Admin_Users_groups" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        // <![CDATA[
        function AddSelectedItems() {
            MoveSelectedItems(lbAvailable, lbChosen);
            UpdateButtonState();
            lblRegDone.SetText('');
        }
        function RemoveSelectedItems() {
            MoveSelectedItems(lbChosen, lbAvailable);
            UpdateButtonState();
            lblRegDone.SetText('');
        }
        function MoveSelectedItems(srcListBox, dstListBox) {
            srcListBox.BeginUpdate();
            dstListBox.BeginUpdate();
            var items = srcListBox.GetSelectedItems();
            for (var i = items.length - 1; i >= 0; i = i - 1) {
                dstListBox.AddItem(items[i].text, items[i].value);
                srcListBox.RemoveItem(items[i].index);
            }
            srcListBox.EndUpdate();
            dstListBox.EndUpdate();
        }
        function UpdateButtonState() {
            btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
            btnMoveSelectedItemsToLeft.SetEnabled(lbChosen.GetSelectedItems().length > 0);
        }

        function confirmDelete() {
            if (confirm('Are you sure you want to delte this group?') === true) {
                return true;
            } else {
                return false;
            }
        }

        // ]]> 
    </script>
    <style>
        .dxgvFocusedRow_Office2010Silver td {
            padding: 5px;
        }

        #ContentFix {
            width: 100% !important;
            top: 0 !important;
        }

        .row {
            margin: 0px;
        }
        .deletebutton {
            font-family: Calibri,Verdana;
            font-size: 12px;
            width: 100px;
            background: #d83132;
            color: #fff;
            font-weight: bold;
            border: none;
            padding: 5px 10px;
        }
        .addbutton {
            font-family: Calibri,Verdana;
            font-size: 12px;
            width: 100px;
            background: #3b3e3d;
            color: #fff;
            font-weight: bold;
            border: none;
            padding: 5px 10px;
        }
        .row {
            margin: 0px;
        }

        .btn-success, .btn-success:hover, .btn-success.focus, .btn-success:focus {
            background:#555453;
            color: #ffffff;
            border:none;
            font-weight:bold;
        }

        .btn-info, .btn-info:hover, .btn-info.focus, .btn-info:focus {
            background:#cccccc;
            color: #555453;
            border:none;
            font-weight:bold;
        }
    </style>

    <dx:ASPxPopupControl ID="NewGroup" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" ShowOnPageLoad="False" ClientInstanceName="NewGroup" Modal="True" CloseAction="CloseButton" AllowDragging="True"
        PopupHorizontalAlign="WindowCenter" HeaderStyle-Font-Size="Large" PopupVerticalAlign="WindowCenter" Width="360px" Height="200px" HeaderText="New Group">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
                <div class="row">
                    <div class="col-md-4"> Name:</div>
                    <div class="col-md-8">
                        <asp:TextBox runat="server" id="txtGroupName" width="100%" MaxLength="40"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="valGroupName" runat="server" ControlToValidate="txtGroupName" ErrorMessage="Invalid Input" ValidationExpression="^[0-9a-zA-Z\. ]+$" />
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-8"><asp:Button runat="server" id="btnCreateGroup" text="Save"/></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-12">
                        <dx:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblGroupError" Text="" ForeColor="Red" Visible="false" runat="server"></dx:ASPxLabel>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>

        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>

    </dx:ASPxPopupControl>


    <dx:ASPxPopupControl ID="PopupGroupAdded" Font-Size="Medium" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupGroupAdded"
        Height="68px" Modal="True" CloseAction="CloseButton" Width="700px" AllowDragging="True"
        HeaderText="New Group Added" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
                <dx:ASPxImage ID="ASPxImage2" runat="server" Style="left: 10px; position: relative; top: 10px" Height="30px"
                    Width="30px" ImageUrl="~/images2014/info.png" />
                <dx:ASPxLabel Font-Names="Calibri,Verdana" Font-Size="Medium" Style="left: 50px; position: relative; top: 0px"
                    runat="server" ID="lblUserAmended" Text="New Group added.">
                </dx:ASPxLabel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxGlobalEvents ID="GlobalEvents" runat="server"><ClientSideEvents ControlsInitialized="function(s, e) { UpdateButtonState(); }" /></dx:ASPxGlobalEvents>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col">
            <div class="f-left">
                <dx:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" ID="lblPageTitle" Text="Centre Group Management" runat="server"></dx:ASPxLabel>
            </div>
            <div class="f-right">
                <div class="f-right ml10">
                    <asp:button Font-Size="12px" Font-Names="Calibri,Verdana" ID="btnAddGroup" CssClass="btn btn-sml btn-success" runat="server" Text="New Group" Width="100px"></asp:button>
                </div>
                <div class="f-right ml10">
                    <asp:Linkbutton Font-Size="12px" Font-Names="Calibri,Verdana" ID="btnBackUsers" CssClass="btn btn-sm btn-info" runat="server" Text="Back to Users" Width="100px" PostBackUrl="Default.aspx"></asp:Linkbutton>
                </div>
                <div class="f-right ml10">
                    <asp:Button Font-Size="12px" Font-Names="Calibri,Verdana" ID="btnDeleteGroup" CssClass="btn btn-sm btn-danger" runat="server"  Text="Delete Group" Width="100px" Visible="False" OnClientClick="return confirmDelete()" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <dx:ASPxLabel Font-Size="13px" Font-Names="Calibri,Verdana" ID="lblError" Text="" Visible="false" Width="100%" runat="server"></dx:ASPxLabel>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <table>
                <tr>
                    <td valign="top" style="padding-right: 40px;">
                        <strong>Selected group:</strong>
                    </td>
                    <td>
                        <strong>All Centres</strong>
                    </td>
                    <td>&nbsp;</td>
                    <td style="padding-bottom: 5px;">
                        <strong>Centres in group</strong>
                    </td>
                </tr>
                <tr>
                    <td valign="top"  style="padding-right: 40px;">
                        <asp:DropDownList Font-Size="13px" Font-Names="Calibri,Verdana" ID="ddlGroups" runat="server"  AutoPostBack="True" Width="200px" DataTextField="GroupName" DataValueField="Id"></asp:DropDownList>
                    </td>
                    <td>
                        <dx:ASPxListBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="lbAvailable" runat="server" TextField="CentreName" ValueField="DealerCode" ClientInstanceName="lbAvailable" Width="250px" Height="240px" SelectionMode="CheckColumn">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                        </dx:ASPxListBox>
                    </td>
                    <td valign="middle" style="padding: 10px;">
                        <dx:ASPxButton Font-Size="13px" Font-Names="Calibri,Verdana" ID="btnMoveSelectedItemsToRight" runat="server" CssClass="indbutton" ClientInstanceName="btnMoveSelectedItemsToRight" 
                                       AutoPostBack="False" Text="Add >>" Width="130px" Height="23px" ClientEnabled="False" ToolTip="Remove selected items">
                            <ClientSideEvents Click="function(s, e) { AddSelectedItems(); }" />
                        </dx:ASPxButton>
                        <br/><br/>
                        <dx:ASPxButton Font-Size="13px" Font-Names="Calibri,Verdana" ID="btnMoveSelectedItemsToLeft" runat="server" CssClass="indbutton" ClientInstanceName="btnMoveSelectedItemsToLeft"
                        AutoPostBack="False" Text="<< Remove" Width="130px" Height="23px" ClientEnabled="False" ToolTip="Add selected items">
                            <ClientSideEvents Click="function(s, e) { RemoveSelectedItems(); }" />
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <dx:ASPxListBox Font-Size="13px" Font-Names="Calibri,Verdana" ID="lbChosen" runat="server" TextField="CentreName" ValueField="DealerCode" ClientInstanceName="lbChosen" Width="250px" Height="240px" SelectionMode="CheckColumn">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                        </dx:ASPxListBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="Default.aspx"></a>
                    </td>
                    <td></td>
                    <td></td>
                    <td align="right" style="padding-top: 20px;">
                        <asp:Button Font-Size="12px" Font-Names="Calibri,Verdana" ID="btnSaveReg" runat="server" Text="Save Changes" Width="100px" CssClass="addbutton"> </asp:Button>    
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <dx:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblRegDone" ClientInstanceName="lblRegDone" Text="" Visible="false" runat="server"></dx:ASPxLabel>
        </div>
    </div>

</asp:Content>


