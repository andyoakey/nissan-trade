﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting
Imports QubeUtils

Partial Class Admin_Users_Default
    Inherits UI.Page

    Dim dsUsers As DataSet
    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim htIn1 As New Hashtable
    Dim sErr As String
    Dim sSQL As String


    Protected Sub dsActivityLog_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsActivityLog.Init
        dsActivityLog.ConnectionString = ConfigurationManager.ConnectionStrings(Settings.GetString("ConnectionStringName")).ToString()
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("SuperUser") <> "Y" Then
        If Not Session("SuperAdmin") Then
            Response.Redirect("~/Dashboard.aspx")
        End If

        Me.Title = "User Management - " & Settings.GetString("WebAppName")

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "User Management", "User List")
        End If

        If Session("SuperAdmin") Then
            btnGroups.Visible = True
        End If

    End Sub


    Protected Sub gridActivity_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("ActivityUser") = Trim(CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowFieldValues("UserId"))
    End Sub


    Protected Sub gridUsers_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridUsers.HtmlDataCellPrepared
        If e.GetValue("Blocked") = 1 Then
            e.Cell.Font.Italic = True
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
    End Sub


    Protected Sub gridUsers_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub


    Protected Sub gridUsers_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridUsers.CustomColumnDisplayText
        'If e.Column.Index = 7 Then
        '    If e.GetFieldValue("Deleted") <> "N" Then
        '        e.DisplayText = ""
        '    End If
        'End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
        
        Call ShowMessage()
        Call LoadData()

        gridUsers.Columns("Delete").Visible = Session("SuperAdmin")
        gridUsers.Columns("Login As").Visible = Session("SuperAdmin")
        gridUsers.Columns("Edit").Visible = Session("SuperAdmin")
        gridUsers.Columns("Admin?").Visible = Session("SuperAdmin")
        

    End Sub

    Private Sub ShowMessage()

        Dim message as String = ""

        If Request.QueryString IsNot nothing Then
            If Request.Querystring("msg") IsNot nothing Then

                Select Case Request.Querystring("msg")
                    Case "blocked"
                        message = "User has been blocked"
                    Case "unblocked"
                        message = "User has been un-blocked"
                    Case "unlocked"
                        message = "User has been un-locked"
                    Case "deleted"
                        message = "User has been deleted"
                End Select

                lblMessage.Text = message 
                divMessageContainer.Visible = true
            End If
        End If
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "NissanTradePartsUsers_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        Call LoadData()

        gridUsers.Columns("Delete").Visible = False
        gridUsers.Columns("Login As").Visible = false
        gridUsers.Columns("Edit").Visible = false
        gridUsers.Columns("Admin?").Visible = True
        gridUsers.Columns("Delete").Visible = false
        gridUsers.Columns("Blocked").Visible = false

        ASPxGridViewExporter.FileName = sFileName
        ASPxGridViewExporter.WriteXlsToResponse(New XlsExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        If Session("SuperAdmin").ToString() = "Y" Then
            gridUsers.Columns("Delete").Visible = False
            gridUsers.Columns("Login As").Visible = false
            gridUsers.Columns("Edit").Visible = false
            gridUsers.Columns("Admin?").Visible = false
        End If

        gridUsers.Columns("Blocked").Visible = True
        gridUsers.Columns("Delete").Visible = True

    End Sub


    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        PopupUserEdit.ContentUrl = "Edit.aspx?uid=0"
        PopupUserEdit.ShowOnPageLoad = True
    End Sub

    Protected Sub LoginAsUser_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)
        Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex

        Dim userId As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "UserId").ToString()
        Dim emailAddress As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "EmailAddress").ToString()
        Dim blocked As Integer = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Blocked").ToString()

        Dim contentUrl As String = String.Format("{0}?uid={1}", "LoginAs.aspx", userId)

        Dim user As MembershipUser = Membership.GetUser(emailAddress)
        Dim userHasChangedPassword As Boolean = False
        Dim userLockedOut = false

        If user IsNot Nothing Then
            userHasChangedPassword = user.LastPasswordChangedDate > DateTime.Today.AddDays(-90)
            userLockedOut = user.IsLockedOut
        End If

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/img/icons/userloginas.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ UserLoginAsClick('{0}','{1}'); }}", contentUrl, emailAddress)

        If blocked = 0 And userHasChangedPassword And Not userLockedOut And Session("SuperUser") = "Y" Then
            link.Visible = True
        End If


    End Sub

    Protected Sub UserEditLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim userId As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "UserId").ToString()
        Dim contentUrl As String = String.Format("{0}?uid={1}", "Edit.aspx", userId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/img/icons/vcard_edit.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ UserEditClick('{0}'); }}", contentUrl)

    End Sub
    

    Protected Sub BlockLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim emailAddress As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "EmailAddress").ToString()
        Dim userId As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "UserId").ToString()
        Dim blocked As Integer = templateContainer.Grid.GetRowValues(rowVisibleIndex, "Blocked").ToString()
        Dim contentUrl As String = String.Format("{0}?uid={1}&block={2}", "BlockUnblock.aspx", userId, IIf(blocked=0,1,0))

        Dim action As String = IIf(blocked, "un-block", "block")

        link.NavigateUrl = "javascript:void(0);"
        If blocked = 0 Then
            link.ImageUrl = "~/img/icons/user_unblock.png"
        Else
            link.ImageUrl = "~/img/icons/user_block.png"
        End If
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ UserBlockClick('{0}','{1}','{2}'); }}", contentUrl, action, emailAddress)

    End Sub

    Protected Sub UnlockLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim userID As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "UserId").ToString()
        Dim emailAddress As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "EmailAddress").ToString()
        Dim isLockedOut As Boolean = templateContainer.Grid.GetRowValues(rowVisibleIndex, "IsLockedOut").ToString()
        Dim sContentUrl As String = String.Format("{0}?uid={1}", "Unlock.aspx", userID)
        
        link.NavigateUrl = "javascript:void(0);"
        link.Visible = isLockedOut
        link.ImageUrl = "~/img/icons/lock_open.png"

        link.ClientSideEvents.Click = String.Format("function(s, e) {{ UserUnlockClick('{0}','{1}'); }}", sContentUrl, emailAddress)

    End Sub

    Protected Sub DeleteLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim rowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim userId As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "UserId").ToString()
        Dim emailAddress As String = templateContainer.Grid.GetRowValues(rowVisibleIndex, "EmailAddress").ToString()
        Dim contentUrl As String = String.Format("{0}?uid={1}", "Delete.aspx", userId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/img/icons/user_delete.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ UserDelClick('{0}','{1}'); }}", contentUrl, emailAddress)

    End Sub

    Private Sub LoadData()
        
        Dim sSQL =  "SELECT " + chr(10) _
                    + "	Web_User.UserId, ISNULL(FirstName,'') as FirstName, ISNULL(Surname,'') as Surname, ISNULL(FirstName,'') + ' ' + ISNULL(Surname,'') as FullName, anm.Email, " + chr(10) _
                    + "	Web_User.EmailAddress, AccessLevel, Deleted, Blocked, SuperUser, anm.LastLoginDate, anm.LastPasswordChangedDate, anm.IsLockedOut, " + chr(10) _
                    + "	(" + chr(10) _
                    + "	CASE SUBSTRING(AccessLevel,1,1)" + chr(10) _
                    + "	WHEn 'N' Then 'National'" + chr(10) _
                    + "	WHEN 'Z' THEN 'Zone'" + chr(10) _
                    + "	WHEN 'T' THEN 'TPSM'" + chr(10) _
                    + "	WHEN 'C' THEN (SELECT LTRIM(Centrename) from CentreMaster where dealercode = SUBSTRING(AccessLevel,2,5)) " + chr(10) _
                    + "	WHEN 'G' THEN (SELECT LTRIM(GroupName) from CentreGroup where Id = SUBSTRING(AccessLevel,2,5))" + chr(10) _
                    + "	END ) " + chr(10) _
                    + "	as LevelName" + chr(10) _
                    + "FROM " + chr(10) _
                    + "	dbo.Web_User " + chr(10) _
                    + "INNER JOIN " + chr(10) _
                    + "	aspnet_Membership anm on anm.UserId = Web_User.GUID " + chr(10) _
                    + "WHERE " + chr(10) _
                    + "	Deleted = 'N'"

        If Session("SuperAdmin") = 0 Then
            sSQL = sSQL + " AND anm.Email NOT LIKE '%qubedata%'"
        End If

        sSQL = sSQL + " Order by Surname, EmailAddress" 

        Dim users As DataTable = MSSQLHelper.GetDataTable(sSQL, Nothing)

        gridUsers.DataSource = users
        gridUsers.DataBind()
    End Sub


    Protected Sub btnGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGroups.Click
        Response.Redirect("Groups.aspx")
    End Sub

    Protected Sub gridUsers_HtmlDataCellPrepared1(sender As Object, e As ASPxGridViewTableDataCellEventArgs)
        If e.DataColumn.FieldName = "AccessLevel" Then
            e.Cell.ToolTip = gridUsers.GetRowValues(e.VisibleIndex, "LevelName").ToString() & " (" + e.CellValue.ToString() & ")"
        End If
    End Sub


End Class

