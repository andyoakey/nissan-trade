﻿Imports System.Activities.Statements
Imports System.Net.Mail
Imports System.Data
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Web.UI
Imports DevExpress.Web
Imports QubeUtils

Partial Class Admin_Users_groups
    Inherits Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Session("SuperAdmin") Then
            Response.Redirect("Default.aspx")
        End If

        Me.Title = "Group Management - " & Settings.GetString("WebAppname")
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Group Management", "Managing user groups")
            Call LoadGroups()
        End If

        If Session("SuperAdmin").ToString() = "Y" Then
            btnDeleteGroup.Visible = True
        End If
    End Sub


    Protected Sub LoadGroups()
        Dim sQLQuery as string = "SELECT Id, GroupName FROM CentreGroup ORDER BY GroupName ASC"
        Dim dtGroups as DataTable = MSSQLHelper.GetDataTable(SqlQuery, nothing)

        ddlGroups.DataSource = dtGroups
        ddlGroups.DataBind()

        Call LoadValues(ddlGroups.SelectedValue)
    End Sub


    Protected Sub LoadValues(ByVal groupId As Integer)

        lbAvailable.Items.Clear()
        lbChosen.Items.Clear()

        Dim sQLQuery = "SELECT DealerCode, CentreName, (Case When DealerCode IN (SELECT DealerCode FROM CentreGroupMembership WHERE CentreGroupId = @GroupID) Then 1 Else 0 End) as Checked FROM CentreMaster"
        Dim sQLOrderBy = " ORDER BY CentreName ASC"

        Dim param as SqlParameter() = new SqlParameter(0){}
        param(0) = New SqlParameter("@GroupID", SqlDbType.Int) With {.Value = groupId}

        Dim param1 as SqlParameter() = new SqlParameter(0){}
        param1(0) = New SqlParameter("@GroupID", SqlDbType.Int) With {.Value = groupId}

        Dim sQLQueryChecked = sQLQuery & " WHERE  (DealerCode IN (SELECT DealerCode FROM CentreGroupMembership WHERE CentreGroupId = @GroupID)) " & sQLOrderBy
        Dim sQLQueryNotChecked = sQLQuery & " WHERE  (DealerCode NOT IN (SELECT DealerCode FROM CentreGroupMembership WHERE CentreGroupId = @GroupID))" & sQLOrderBy
        
        Dim dtGroupMembers As DataTable = MSSQLHelper.GetDataTable(sQLQueryChecked, param)
        Dim dtNotGroupMembers As DataTable = MSSQLHelper.GetDataTable(sQLQueryNotChecked, param1)

        lbAvailable.DataSource = dtNotGroupMembers
        lbAvailable.DataBind()

        lbChosen.DataSource = dtGroupMembers
        lbChosen.DataBind()

    End Sub


    Protected Sub btnSaveReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReg.Click

        Dim bErr As Boolean = False
        Dim da As New DatabaseAccess
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim sErr As String = ""
        Dim sSQL As String = ""
        Dim htIn As New Hashtable
        Dim lstCentres As New List(Of String)

        For Each item As ListEditItem In lbChosen.Items
            lstCentres.Add(item.Value)
        Next

        lblError.Visible = False

        htIn.Add("@GroupId", ddlGroups.SelectedValue)
        htIn.Add("@Centres", String.Join(",", lstCentres.ToArray()))

        da.Update(sErr, "p_Admin_GroupCentres_Update", htIn)

        If sErr.Length > 0 Then
            HttpContext.Current.Session("sErr") = sErr : HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
        Else
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Group Updated", "Group updated: '" & Trim(ddlGroups.SelectedItem.Text) & "'")
            lblRegDone.Text = "Updated the group '" & Trim(ddlGroups.SelectedItem.Text) & "' with " & lstCentres.Count & " members!"
            lblRegDone.Visible = True
            Call LoadValues(ddlGroups.SelectedValue)
        End If

    End Sub


    Protected Sub ddlGroups_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroups.SelectedIndexChanged
        lblRegDone.Text = ""
        lblRegDone.Visible = False
        LoadValues(ddlGroups.SelectedValue)
    End Sub


    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGroup.Click
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Create Group", "Creating a group")
        lblGroupError.Text = ""
        lblGroupError.Visible = False
        txtGroupName.Text = ""
        NewGroup.ShowOnPageLoad = True
    End Sub


    Protected Sub btnDeleteGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGroup.Click
        Dim bErr = False
        Dim da As New DatabaseAccess
        Dim ds As New DataSet
        Dim sErr = ""
        Dim sSql = ""

        sSql = "SELECT UserId FROM Web_User WHERE AccessLevel = 'G" & Trim(ddlGroups.SelectedValue) & "' AND Deleted = 'N'"
        ds = da.ExecuteSQL(sErr, sSql)

        If Not ds Is Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                lblError.Text = "You must un-assign users from this group before deleting it!"
                lblError.Visible = True
                bErr = True
            End If
        End If

        If Not bErr Then
            sErr = ""

            sSql = "DELETE FROM CentreGroupMembership WHERE CentreGroupId = " & ddlGroups.SelectedValue
            da.ExecuteSQL(sErr, sSql)

            sSql = "DELETE FROM CentreGroup WHERE Id = " & ddlGroups.SelectedValue
            da.ExecuteSQL(sErr, sSql)

            If sErr = "" Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Delete Group", "Group Deleted: " & ddlGroups.SelectedItem.Text)
                Call LoadGroups()
            Else
                If Session("SuperUser") Then HttpContext.Current.Session("ErrorToDisplay") = sErr
                HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            End If
        End If

    End Sub
    

    Protected Sub btnCreateGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateGroup.Click

        Dim bErr As Boolean = False
        Dim da As New DatabaseAccess
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim sErr As String = ""
        Dim sQLQuery As String = ""
        Dim htIn As New Hashtable
        Dim i As Integer

        lblError.Visible = False

        If Trim(txtGroupName.Text) = "" Then
            lblGroupError.Text = "A group name is mandatory"
            lblGroupError.Visible = True
            bErr = True
        End If

        If Not bErr Then
            sQLQuery = "SELECT Id FROM CentreGroup WHERE GroupName = '" & Trim(txtGroupName.Text) & "'"
            ds = da.ExecuteSQL(sErr, sQLQuery)

            If Not ds Is Nothing Then
                If sErr = "" Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblGroupError.Text = "This group name is already in use on the website!"
                        lblGroupError.Visible = True
                        bErr = True
                    End If
                Else
                    HttpContext.Current.Session("sErr") = sErr
                    HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
                End If
            Else
                HttpContext.Current.Session("sErr") = sErr
                HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            End If
        End If

        If Not bErr Then

            Dim param as SqlParameter() = new SqlParameter(1){}
            param(0) = New SqlParameter("@GroupName", SqlDbType.NVarChar,50) With {.Value = Trim(txtGroupName.Text)}

            sQLQuery = "INSERT INTO CentreGroup (GroupName) VALUES (@GroupName)"
            MSSQLHelper.ExecuteSQL(sqlQuery, param)
            
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Group Created", "New group created: '" & Trim(txtGroupName.Text) & "'")

            NewGroup.ShowOnPageLoad = False

            Call LoadGroups()

            For i = 0 To ddlGroups.Items.Count - 1
                If ddlGroups.Items(i).Text = txtGroupName.Text Then
                    ddlGroups.SelectedIndex = i
                    Exit For
                End If
            Next

            Call LoadValues(ddlGroups.SelectedValue)

        End If
    End Sub


End Class
