﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Admin_Users_Default" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        
        function UserDelClick(contentUrl, emailAddress) {
            if (confirm('Are you sure you wish to delete the user account for ' + emailAddress + '?') === true) {
                window.location.href = contentUrl;
            } else {
                return false;
            }
        }

        function UserBlockClick(contentUrl, action, emailAddress) {
            if (confirm('Are you sure you wish to ' + action + ' ' + emailAddress + '?') === true) {
                window.location.href = contentUrl;
            } else {
                return false;
            }
        }

        function UserUnlockClick(contentUrl, emailAddress) {
            if (confirm('Are you sure you wish to un-lock the account for ' + emailAddress + '?') === true) {
                window.location.href = contentUrl;
            } else {
                return false;
            }
        }

        function UserLoginAsClick(contentUrl, emailAddress) {
            if (confirm('Are you sure you wish to login as ' + emailAddress + '?') === true) {
                window.location.href = contentUrl;
            } else {
                return false;
            }
        }

        function HideUserActionWindow() {
            PopupUserAction.Hide();
            gridUsers.Refresh();
        }

        function UserEditClick(contentUrl) {
            PopupUserEdit.SetContentUrl(contentUrl);
            PopupUserEdit.SetSize(475, 320);
            PopupUserEdit.Show();
        }

        function HideUpdReminder() {
            PopupUserEdit.Hide();
            gridUsers.Refresh();
        }

    </script>

    <style>
        .dxgvFocusedRow_Office2010Silver td {
            padding: 5px;
        }

        #ContentFix {
            width: 100% !important;
            top: 0 !important;
        }
        .row {
            margin: 0px;
        }

        .btn-success, .btn-success:hover, .btn-success.focus, .btn-success:focus {
            background:#555453;
            color: #ffffff;
            border:none;
            font-weight:bold;
        }

        .btn-info, .btn-info:hover, .btn-info.focus, .btn-info:focus {
            background:#cccccc;
            color: #555453;
            border:none;
            font-weight:bold;
        }
    </style>

    <dx:ASPxPopupControl ID="PopupUserAmended" Font-Size="Medium" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUserAmended"
        Height="68px" Modal="True" CloseAction="CloseButton" Width="700px" AllowDragging="True"
        HeaderText="User Details Updated" PopupHorizontalAlign="WindowCenter"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol5" runat="server">
                <dx:ASPxImage ID="ASPxImage2" runat="server" Style="left: 10px; position: relative; top: 10px" Height="30px"
                    Width="30px" ImageUrl="~/images2014/info.png" />
                <dx:ASPxLabel Font-Names="Calibri,Verdana" Font-Size="Medium" Style="left: 50px; position: relative; top: 0px"
                    runat="server" ID="lblUserAmended" Text="User details updated.">
                </dx:ASPxLabel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="PopupUserAction" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUserAction"
        Modal="True" CloseAction="CloseButton" AllowDragging="True"
        PopupHorizontalAlign="WindowCenter" HeaderStyle-Font-Size="Large"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="PopupUserEdit" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupUserEdit"
        Modal="True" CloseAction="CloseButton" AllowDragging="True" Height="340px" Width="475px"
        HeaderText="User Details" PopupHorizontalAlign="WindowCenter" HeaderStyle-Font-Size="13px"
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>
    
    <div class="row">
        <div class="col">
            <div class="f-left">
                <dx:ASPxLabel Font-Size="18px" Font-Style="Bold" Font-Names="Calibri,Verdana" ID="lblPageTitle" Text="User Management" runat="server"></dx:ASPxLabel>
            </div>
            <div class="f-right ml10">
                <div class="f-right ml10">
                    <asp:Button ID="btnNew" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="New User" CssClass="btn btn-sml btn-success"></asp:Button>
                </div>
                <div class="f-right ml10">
                    <asp:Button ID="btnGroups" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="Groups" CssClass="btn btn-sml btn-info" visible="False"></asp:Button>
                </div>
            </div>
        </div>
    </div>
    <asp:panel CssClass="row" id="divMessageContainer" runat="server" visible="false">
        <div class="col">
            <div class="alert alert-info fade in alert-dismissible" role="alert">
               <asp:Label runat="server" id="lblMessage"></asp:Label>
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            </div>
        </div>
    </asp:panel>
    <div class="row">
        <div class="col">
            <dx:ASPxGridView runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" ID="gridUsers" ShowHeaderFilterBlankItems="false" AutoGenerateColumns="False" ClientInstanceName="gridUsers"
                KeyFieldName="UserId" Width="100%" OnHtmlDataCellPrepared="gridUsers_HtmlDataCellPrepared1" OnCustomColumnDisplayText="gridUsers_OnCustomColumnDisplayText" SettingsBehavior-AllowSelectByRowClick="True" SettingsBehavior-AllowFocusedRow="True" SettingsBehavior-AllowSelectSingleRowOnly="True">

                <SettingsPager PageSize="25" Mode="ShowPager" Visible="True">
                    <AllButton Visible="True"></AllButton>
                    <FirstPageButton Visible="True"></FirstPageButton>
                    <LastPageButton Visible="True"></LastPageButton>
                </SettingsPager>

                <Columns>

                    <dx:GridViewDataTextColumn Caption="User ID" EditFormSettings-Visible="False" ExportWidth="60" FieldName="UserId" Visible="false" VisibleIndex="0" Width="0px"></dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Name" EditFormSettings-Visible="False" ExportWidth="100" FieldName="FullName" VisibleIndex="1" Width="100px">
                        <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="False" AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True" />
                        <CellStyle HorizontalAlign="Left"></CellStyle>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Email" EditFormSettings-Visible="True" ExportWidth="250" FieldName="Email" Name="Email Address" VisibleIndex="2" Width="250px">
                        <Settings AllowAutoFilter="True" AllowHeaderFilter="False" ShowFilterRowMenu="False" AutoFilterCondition="Contains" />
                        <HeaderStyle HorizontalAlign="left" VerticalAlign="Middle" Wrap="True" />
                        <CellStyle HorizontalAlign="Left" Wrap="False" />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="Access" ExportWidth="50" FieldName="AccessLevel" Name="AccessLevel" VisibleIndex="3" Width="50px">
                        <Settings AllowAutoFilter="True" AllowHeaderFilter="false" ShowFilterRowMenu="False" HeaderFilterMode="CheckedList" AllowSort="true" />
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True" />
                        <CellStyle HorizontalAlign="left" />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataDateColumn Caption="Last Login" EditFormSettings-Visible="False" ExportWidth="130" FieldName="LastLoginDate" Name="Last Login" VisibleIndex="4" Width="130px">
                        <PropertiesDateEdit DisplayFormatString=""></PropertiesDateEdit>
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataDateColumn>

                    <dx:GridViewDataTextColumn Name="Admin" Caption="Admin?" ExportWidth="50" FieldName="SuperUser" ToolTip="Administrator" Visible="true" VisibleIndex="5" Width="50px">
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <Settings AllowHeaderFilter="false" AllowAutoFilter="False" ShowFilterRowMenu="False" AllowSort="true" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn Name="Edit" Caption="Edit" EditFormSettings-Visible="False" ExportWidth="80" FieldName="UserId" ToolTip="Forgotten Password Reminder" UnboundType="String" VisibleIndex="6" Width="50px">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" ShowFilterRowMenu="False" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="UserEditLink_Init"></dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn Name="LockedOut" Caption="Locked-Out" EditFormSettings-Visible="False" ExportWidth="80" FieldName="IsLockedOut" ToolTip="Unlock account" UnboundType="String" VisibleIndex="7" Width="50px">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="true" ShowFilterRowMenu="False" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="UnlockLink_Init">
                            </dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn Name="LoginAs" Caption="Login As" FieldName="UserId" ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="8" Width="50px" ToolTip="Login as this user" UnboundType="String">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="true" ShowFilterRowMenu="False" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="LoginAsUser_Init" Visible="False"></dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Name="Blocked" Caption="Access?" FieldName="UserId" ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="9" Width="50px" ToolTip="Block/Unblock" UnboundType="String">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="true" ShowFilterRowMenu="False" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="BlockLink_Init">
                            </dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn Name="Delete" Caption="Delete" FieldName="UserId" ExportWidth="80" EditFormSettings-Visible="False" VisibleIndex="10" Width="50px" ToolTip="Delete the user" UnboundType="String">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" ShowFilterRowMenu="False" />
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <CellStyle HorizontalAlign="Center"></CellStyle>
                        <DataItemTemplate>
                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="DeleteLink_Init">
                            </dx:ASPxHyperLink>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>

                </Columns>

                <Templates>

                    <DetailRow>

                        <dx:ASPxGridView Font-Size="11px" Font-Names="Calibri,Verdana" ID="gridActivity" runat="server" DataSourceID="dsActivityLog" AutoGenerateColumns="False" ShowHeaderFilterBlankItems="false"
                            OnBeforePerformDataSelect="gridActivity_BeforePerformDataSelect" Width="100%">

                            <SettingsPager PageSize="20">
                                <AllButton Visible="True"></AllButton>
                                <FirstPageButton Visible="True"></FirstPageButton>
                                <LastPageButton Visible="True"></LastPageButton>
                            </SettingsPager>

                            <Styles Header-HorizontalAlign="Center" Header-Wrap="True" Cell-VerticalAlign="Middle" Cell-HorizontalAlign="Center" Footer-HorizontalAlign="Center">
                                <AlternatingRow BackColor="#f9f9f9"></AlternatingRow>
                            </Styles>

                            <Settings ShowHeaderFilterBlankItems="false" ShowFilterRow="False" ShowFilterRowMenu="False" ShowFooter="True"  ShowGroupedColumns="True" ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False"
                                ShowHeaderFilterButton="True" ShowStatusBar="Hidden" ShowTitlePanel="False" />

                            <Columns>
                                <dx:GridViewDataDateColumn Caption="Date" FieldName="TimeStamp" VisibleIndex="1" Settings-AllowHeaderFilter="True" Width="7%"></dx:GridViewDataDateColumn>

                                <dx:GridViewDataDateColumn Caption="Time" FieldName="TimeStamp" VisibleIndex="1" Settings-AllowHeaderFilter="False" PropertiesDateEdit-DisplayFormatString="t" Width="7%"></dx:GridViewDataDateColumn>

                                <dx:GridViewDataTextColumn Caption="Level" FieldName="SelectionLevel" ReadOnly="True" Settings-HeaderFilterMode="CheckedList" VisibleIndex="2" Width="7%"></dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Detail" FieldName="SelectionId" ReadOnly="True" Settings-HeaderFilterMode="CheckedList" VisibleIndex="3" Width="14%"></dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Page" FieldName="Screen" ReadOnly="True" Settings-HeaderFilterMode="CheckedList" VisibleIndex="4" Width="20%"></dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Notes" FieldName="Notes" ReadOnly="True" Settings-HeaderFilterMode="CheckedList" VisibleIndex="5" Width="25%"></dx:GridViewDataTextColumn>
                            </Columns>

                        </dx:ASPxGridView>

                    </DetailRow>

                </Templates>

                <SettingsDetail ShowDetailRow="true" ShowDetailButtons="True" ExportMode="None" AllowOnlyOneMasterRowExpanded="true" />
                <Settings ShowHeaderFilterButton="True" UseFixedTableLayout="true" ShowFilterRow="True" ShowFooter="false" ShowVerticalScrollBar="False" />
            </dx:ASPxGridView>

        </div>

    </div>


    <%--<asp:SqlDataSource ID="dsAccessOptions" runat="server" SelectCommand="p_VCAUserListOptions" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nUserId" SessionField="UserId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

    

    <%--<asp:SqlDataSource ID="dsAccessLevels" runat="server" SelectCommand="p_Sel_Switch_Centres" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nUserId" SessionField="UserID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

    <asp:SqlDataSource ID="dsActivityLog" runat="server" SelectCommand="p_ActivityLog_User" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nUserId" SessionField="ActivityUser" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter" runat="server" GridViewID="gridUsers"></dx:ASPxGridViewExporter>

</asp:Content>
