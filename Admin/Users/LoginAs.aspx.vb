﻿Imports System.Data
Imports QubeSecurity

Partial Class Admin_Users_LoginAs
    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSQL As String
    Dim nUserId As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not Session("SuperAdmin") Then
            Response.Redirect("Default.aspx")
        End If

        Dim userId As Long = 0

        If Request.QueryString() Is Nothing Or Request.QueryString("uid") = "" Or Not Session("UserEmailAddress").ToString().Contains("qubedata") Then
            Response.Redirect("Default.aspx")
        End If

        userId = Request.QueryString("uid")

        Dim dtWebUser As DataTable = WebUser.GetUser(userId)

        If dtWebUser Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        Dim user = Membership.GetUser(CType(dtWebUser.Rows(0)("GUID"), Guid))

        If user Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        FormsAuthentication.SetAuthCookie(user.UserName, False)
        SetSessionVars(userId)

        Response.Redirect("~/Dashboard.aspx")

    End Sub

    Sub SetSessionVars(ByVal userId As Long)

        Dim dtWebUser As DataTable = WebUser.GetUser(userId)

        Session.Clear()
        Session("UserID") = dtWebUser.Rows(0).Item("UserId")
        Session("UserLevel") = Left(dtWebUser.Rows(0).Item("AccessLevel"), 1)
        Session("SelectionLevel") = Left(dtWebUser.Rows(0).Item("AccessLevel"), 1)
        Session("SelectionId") = Mid(dtWebUser.Rows(0).Item("AccessLevel"), 2)
        Session("DealerCode") = Session("SelectionId")
        Session("SuperUser") = dtWebUser.Rows(0).Item("SuperUser")
        Session("SuperAdmin") = If(dtWebUser.Rows(0).Item("EmailAddress").Contains("qubedata"), True, False)
        Session("UserEmailAddress") = dtWebUser.Rows(0).Item("EmailAddress")
        Session("JustLoggedIn") = 1
        Session("CurrentSelection") = 0
        Session("CurrentPeriod") = CurrentPeriodId()
        Session("CurrentYear") = CurrentYear()
        Session("CurrentMonth") = CurrentMonth()
        Session("ButtonHeight") = 110
        Session("ButtonWidth") = 112
    End Sub

End Class
