﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Edit.aspx.vb" Inherits="Admin_Users_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        .row { margin: 0 0 10px 0; float: none;}
        .row:after { clear: both; height:0}
        .col-md-6 { width: 50%; float: left;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblErr" runat="server" ForeColor="Red" Width="300px"></dxe:ASPxLabel>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblFirstName" runat="server" Text="First name"></dxe:ASPxLabel>
            </div>
            <div class="col-md-6">
                <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtFirstname" runat="server" Width="200px" TabIndex="4"></dxe:ASPxTextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblSurname" runat="server" Text="Surname"></dxe:ASPxLabel>
            </div>
            <div class="col-md-6">
                <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtSurname" runat="server" Width="200px" TabIndex="4"></dxe:ASPxTextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblEmailAddress" runat="server" Text="Email Address"></dxe:ASPxLabel>
            </div>
            <div class="col-md-6">
                <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtEmailAddress" runat="server" Width="200px" TabIndex="3"></dxe:ASPxTextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblConfirmEmailAddress" runat="server" Text="Confirm Email Address"></dxe:ASPxLabel>
            </div>
            <div class="col-md-6">
                <dxe:ASPxTextBox Font-Size="12px" Font-Names="Calibri,Verdana" ID="txtConfirmEmailAddress" runat="server" Width="200px" TabIndex="3"></dxe:ASPxTextBox>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <dxe:ASPxLabel Font-Size="12px" Font-Names="Calibri,Verdana" ID="lblAccess" runat="server" Text="Associated Centre/Group"></dxe:ASPxLabel>
            </div>
            <div class="col-md-6">
                <asp:DropDownList Font-Size="12px" Font-Names="Calibri,Verdana" DropDownRows="10" ID="ddlAccessLevel" runat="server" AutoPostBack="True" Width="200px" DataTextField="Name" DataValueField="AccessLevel"></asp:DropDownList>
            </div>
        </div>
        <div class="row" runat="server" id="divAdmin" visible="false">
            <div class="col-md-6">&nbsp;</div>
            <div class="col-md-6"><dxe:ASPxCheckBox ID="chkAdmin" runat="server" TabIndex="5" Checked="false" Text="TFP Administrator?" /></div>
        </div>
        <div class="row">
            <div class="col-md-6">&nbsp;</div>
            <div class="col-md-6">
                <asp:button ID="btnSaveReg" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana" Text="Save" Width="100px" CssClass="indbutton" OnClientClick="this.value='Saving...';this.disabled=true;" UseSubmitBehavior="false" ></asp:button>
                <asp:Label ID="txtMessage" runat="server"></asp:Label>
            </div>
        </div>        
    </form>

</body>

</html>
