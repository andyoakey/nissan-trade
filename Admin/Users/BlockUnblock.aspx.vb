﻿Imports System.Data
Imports System.Net.Mail
Imports DevExpress.DocumentView
Imports QubeSecurity

Partial Class Admin_Users_BlockUnBlock
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Session("SuperUser") <> "Y" Then
        If Not Session("SuperAdmin") Then
            Response.Redirect("~/Dashboard.aspx")
        End If

        Dim redirectUrl as String = "Default.aspx"
        Dim blockUser as Boolean = false

        If Request.QueryString IsNot nothing Then
            blockUser = Request.QueryString("block") = "1"
            Dim userID = Int16.Parse(Request.QueryString("uid"))

            Dim wUser as DataTable = WebUser.GetUser(userID)

            If wUser.Rows.Count > 0 Then

                If BlockWebUser(wUser.Rows(0)("UserId"), blockUser) Then
                    Dim user = Membership.GetUser(Guid.Parse(wUser.Rows(0)("GUID").ToString()))

                    redirectUrl = redirectUrl & "?msg=" & IIf(blockUser,"blocked","unblocked") 

                    If QubeSecurity.User.BlockUser(user, blockUser) = false Then
                        BlockWebUser(wUser.Rows(0)("UserId"), False)

                        redirectUrl = redirectUrl & "?msg=notblocked" 
                    End If
                End If
            End If
        End If
        
        Response.Redirect(redirectUrl)
    End Sub


    Protected Function BlockWebUser(ByVal userId as Long, isBlocked as boolean) As Boolean
        Dim sSQL As String = ""

        If isBlocked Then
            sSQL = "UPDATE Web_User SET Blocked = 1 WHERE UserId = " & Trim(Str(Request.QueryString(0)))
        Else
            sSQL = "UPDATE Web_User SET Blocked = 0 WHERE UserId = " & Trim(Str(Request.QueryString(0)))
        End If

        Try
            Call RunNonQueryCommand(sSQL)
            Return True
        Catch ex As Exception
            return False
        End Try

    End Function
    

End Class
