﻿Imports System.Data
Imports System.IO
Imports System.Net.Mime
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports DevExpress.Web
Imports DevExpress.Export

Partial Class SalesTracker

    Inherits System.Web.UI.Page

    Dim dsSalesPerf As DataSet
    Dim nTotalLastYear1 As Double
    Dim nTotalLastYear2 As Double
    Dim nTotalThisYear As Double
    Dim nTotalForecast As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Try
            If Not IsPostBack Then
                ddlType.Items.Add("Applicable Mechanical")
                ddlType.Items.Add("Total Mechanical")
                ddlType.Items.Add("Applicable Total Sales")
                ddlType.Items.Add("Total Sales")
                ddlType.SelectedIndex = 0
                ddlYear.Items.Add("2016")
                ddlYear.Items.Add("2015")
                ddlYear.Items.Add("2014")
                ddlYear.Items.Add("2013")
                ddlYear.SelectedIndex = 0
            End If
        Catch ex As Exception
            Session("CallingModule") = "SalesTracker - Page_Load()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Try

            Me.Title = "NissanDATA PARTS - Monthly Sales Performance"
            Call LoadPerfData()

        Catch ex As Exception
            Session("CallingModule") = "SalesTracker - Page_LoadComplete()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Sub LoadPerfData()

        Dim htIn1 As New Hashtable
        Dim htIn2 As New Hashtable
        Dim sSelectionId As String = Session("SelectionId")
        Dim da As New DatabaseAccess
        Dim sErr As String = ""

        Try

            htIn1.Add("@sDealerCode", sSelectionId)
            htIn1.Add("@nYear", CInt(ddlYear.SelectedItem.Text))
            htIn1.Add("@nType", ddlType.SelectedIndex + 1)
            dsSalesPerf = da.Read(sErr, "p_SalesTracker", htIn1)
            If sErr <> "" Then
                Session("CallingModule") = "SalesTracker - LoadTradeTargetData() - p_Sales_Tracker"
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

            gridSalesPerf.DataSource = dsSalesPerf.Tables(0)
            gridSalesPerf.DataBind()

        Catch ex As Exception
            Session("CallingModule") = "SalesTracker - LoadTradeTargetData()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

    End Sub

    Protected Sub gridSalesPerf_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridSalesPerf.CustomColumnDisplayText

        If e.Column.FieldName = "ThisYear" Or e.Column.FieldName = "Variance" Or e.Column.FieldName = "VariancePerc" Or e.Column.FieldName = "AverageDailySales" Then
            If e.GetFieldValue("DataPeriodId") > Session("CurrentPeriod") Then
                e.DisplayText = ""
            End If
        End If

    End Sub

    Protected Sub gridSalesPerf_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridSalesPerf.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nTotalLastYear1 = nTotalLastYear1 + e.GetValue("LastYear")
            If e.GetValue("DataPeriodId") <= Session("CurrentPeriod") Then
                nTotalLastYear2 = nTotalLastYear2 + e.GetValue("LastYear")
                nTotalThisYear = nTotalThisYear + e.GetValue("ThisYear")
            End If
            nTotalForecast = nTotalForecast + e.GetValue("ForecastSales")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then

            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "VariancePerc" Then
                If nTotalLastYear2 <> 0 And nTotalThisYear <> 0 Then
                    e.TotalValue = (nTotalThisYear - nTotalLastYear2) / nTotalLastYear2
                Else
                    e.TotalValue = 0
                End If
            End If

            If objSummaryItem.FieldName = "AverageDailySales" Then
                e.TotalValue = dsSalesPerf.Tables(1).Rows(0).Item(0)
            End If

            If objSummaryItem.FieldName = "ForecastPerc" Then
                If nTotalLastYear1 <> 0 And nTotalForecast <> 0 Then
                    e.TotalValue = (nTotalForecast - nTotalLastYear1) / nTotalLastYear1
                Else
                    e.TotalValue = 0
                End If
            End If

        End If

    End Sub

    Protected Sub gridSalesPerf_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSalesPerf.HtmlDataCellPrepared

        Dim nValue As Double

        If e.DataColumn.FieldName = "Variance" Or e.DataColumn.FieldName = "VariancePerc" Or e.DataColumn.FieldName = "ForecastPerc" Then
            If e.GetValue("DataPeriodId") <= Session("CurrentPeriod") Or e.DataColumn.FieldName = "ForecastPerc" Then
                e.Cell.ForeColor = System.Drawing.Color.White
                nValue = IIf(IsDBNull(e.CellValue), 0, e.CellValue)
                If nValue >= 0 Then
                    e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
                Else
                    e.Cell.BackColor = GlobalVars.g_Color_Tomato
                End If
            End If
        End If

    End Sub

    Protected Sub gridSalesPerf_HtmlFooterCellPrepared(sender As Object, e As ASPxGridViewTableFooterCellEventArgs) Handles gridSalesPerf.HtmlFooterCellPrepared

        Dim column As GridViewDataColumn = CType(e.Column, GridViewDataColumn)

        If column.FieldName = "Variance" Then
            Dim gridView As ASPxGridView = sender
            Dim item As ASPxSummaryItem = gridView.TotalSummary("Variance")
            Dim nValue = IIf(IsDBNull(e.GetSummaryValue(item)), 0, e.GetSummaryValue(item))
            e.Cell.ForeColor = System.Drawing.Color.White
            If nValue > 0 Then
                e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
            Else
                e.Cell.BackColor = GlobalVars.g_Color_Tomato
            End If
        End If

        If column.FieldName = "VariancePerc" Then
            Dim gridView As ASPxGridView = sender
            Dim item As ASPxSummaryItem = gridView.TotalSummary("VariancePerc")
            Dim nValue = IIf(IsDBNull(e.GetSummaryValue(item)), 0, e.GetSummaryValue(item))
            e.Cell.ForeColor = System.Drawing.Color.White
            If nValue > 0 Then
                e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
            Else
                e.Cell.BackColor = GlobalVars.g_Color_Tomato
            End If
        End If

        If column.FieldName = "ForecastPerc" Then
            Dim gridView As ASPxGridView = sender
            Dim item As ASPxSummaryItem = gridView.TotalSummary("ForecastPerc")
            Dim nValue = IIf(IsDBNull(e.GetSummaryValue(item)), 0, e.GetSummaryValue(item))
            e.Cell.ForeColor = System.Drawing.Color.White
            If nValue > 0 Then
                e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
            Else
                e.Cell.BackColor = GlobalVars.g_Color_Tomato
            End If
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call RenderBrick(e)
    End Sub

    Private Sub RenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Dim nValue As Decimal = 0

        Session("CallingModule") = "Campaign201502.aspx - Renderbrick"

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)
        e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter

        If e.RowType = DevExpress.Web.GridViewRowType.Header Then

            e.BrickStyle.ForeColor = GlobalVars.g_Color_Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        ElseIf e.RowType = DevExpress.Web.GridViewRowType.Footer Then

            e.BrickStyle.ForeColor = GlobalVars.g_Color_Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

            If e.Column.Name = "Variance" Or e.Column.Name = "VariancePerc" Or e.Column.Name = "ForecastPerc" Then
                If e.GetValue("DataPeriodId") <= Session("CurrentPeriod") Or e.Column.Name = "ForecastPerc" Then
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                    nValue = IIf(IsDBNull(e.Value), 0, e.Value)
                    If nValue >= 0 Then
                        e.BrickStyle.BackColor = GlobalVars.g_Color_YellowGreen
                    Else
                        e.BrickStyle.BackColor = GlobalVars.g_Color_Tomato
                    End If
                End If
            End If

        Else

            e.BrickStyle.ForeColor = GlobalVars.g_Color_Black
            e.BrickStyle.BackColor = GlobalVars.g_Color_White

            If e.Column.Name = "Variance" Or e.Column.Name = "VariancePerc" Or e.Column.Name = "ForecastPerc" Then
                If e.GetValue("DataPeriodId") <= Session("CurrentPeriod") Or e.Column.Name = "ForecastPerc" Then
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                    nValue = IIf(IsDBNull(e.Value), 0, e.Value)
                    If nValue >= 0 Then
                        e.BrickStyle.BackColor = GlobalVars.g_Color_YellowGreen
                    Else
                        e.BrickStyle.BackColor = GlobalVars.g_Color_Tomato
                    End If
                End If
            End If

        End If

    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "MonthlySalesPerformance_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        Call LoadPerfData()
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub

End Class
