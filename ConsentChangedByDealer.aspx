﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ConsentChangedByDealer.aspx.vb" Inherits="FocusMailing" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
        </div>

        <div id="div" style="position:relative;">
            <table>
                <tr>
                    <td width ="50%">
                       <dx:ASPxLabel 
                        Font-Size="Larger"
                        ID="lblPageTitle" runat="server" 
                        Text="Create Changed By Dealer" />
                   </td>

                    <td width ="50%" align="right">
                        <dx:ASPxLabel 
                            ID="lblNotes" 
                            runat="server" 
                            Font-Italic="true"
                            Text="This shows only Dealers who have changed Consents, not those who have viewed them." />
                   </td>
                </tr>
            </table>
            
        </div> 

         <br />


        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsConsentChanges" 
        Styles-Header-HorizontalAlign="Left"
        Styles-CellStyle-HorizontalAlign="Left"
        Styles-Footer-HorizontalAlign="Left"
        Settings-HorizontalScrollBarMode="Auto"
        Styles-Cell-Wrap="False"
        Width="100%">

        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />

        <Styles Footer-HorizontalAlign ="Center"/>
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true"/>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FooterCellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="100"
                Caption="Region" 
                FieldName="region"
                Name="region">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FooterCellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="100"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Caption="Zone" 
                FieldName="zone"
                Name="zone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="20%" 
                exportwidth="200"
                Caption="Dealer" 
                FieldName="dealer"
                Name="dealer">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="25%" 
                exportwidth="300"
                Caption="Email Address" 
                FieldName="emailaddress"
                Name="emailaddress">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="20%" 
                exportwidth="200"
                Caption="Name" 
                FieldName="firstname"
                Name="firstname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FooterCellStyle-HorizontalAlign="Center"
                Width="15%" 
                exportwidth="100"
                Caption="Records Changed" 
                FieldName="recordschanged"
                Name="recordschanged">
            </dx:GridViewDataTextColumn>


         </Columns>
   
                 
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsConsentChanges" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_ConsentChangedByDealer" 
            SelectCommandType="StoredProcedure">
           </asp:SqlDataSource>
    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

