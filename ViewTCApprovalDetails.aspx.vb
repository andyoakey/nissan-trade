﻿Imports System.Data
Imports System.Net
Imports System.Net.Mail

Partial Class ViewTCApprovalDetails

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, ds2 As DataSet, ds3 As DataSet, sErr, sSQL As String, nTCRegId As Integer
    Dim bSeeOtherButton As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Approved/Decline Trade Club Registration"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        nTCRegId = Val(Request.QueryString(0))

        If Not IsPostBack Then
            lblStatusMessage.Text = ""
            lblDeleteMessage.Text = ""
            txtReason.Text = ""
            btnCancel.Attributes.Add("onClick", "javascript:history.back(); return false;")
            Call GetBusinessTypes(ddlBusinessType)
            If Trim(Request.QueryString(0)) <> "" Then
                Call FetchCompanyDetails()
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Approve/Decline TC Membership", "Approve/Decline TC Membership : " & txtBusinessName.Text)
            End If
        End If

    End Sub

    Sub FetchCompanyDetails()

        Dim i As Integer
        Dim bOnTradeClub As Boolean = False
        Dim sVanRoute As String = ""
        Dim sDriveTime As String = ""
        Dim sDistance As String = ""
        Dim sSalesRep As String = ""
        Dim sNotes As String = ""
        Dim nApprovalStatus As Integer = 0
        Dim sContactName As String = ""
        Dim sTelephoneNumber As String = ""
        Dim sUserField1 As String = ""
        Dim sUserField2 As String = ""
        Dim sUserField3 As String = ""
        Dim sUserField4 As String = ""

        Dim nCustomerId As Integer
        Dim sDealerCode As String

        nCustomerId = GetDataLong("SELECT CustomerId FROM CustomerTradeClubMembership WHERE Id = " & Trim(Str(nTCRegId)))
        sDealerCode = GetDataString("SELECT DealerCode FROM CustomerTradeClubMembership WHERE Id = " & Trim(Str(nTCRegId)))
        txtReason.Text = GetDataString("SELECT ISNULL(RemovalReason,'') AS RemovalReason FROM CustomerTradeClubMembership WHERE Id = " & Trim(Str(nTCRegId)))

        lblStatusMessage.Text = ""
        lblDeleteMessage.Text = ""
        lblApproval.Visible = False

        sSQL = "SELECT * FROM v_Customer WHERE Id = " & Str(nCustomerId)
        ds = da.ExecuteSQL(sErr, sSQL)
        If ds.Tables(0).Rows.Count = 1 Then

            With ds.Tables(0).Rows(0)

                '/ -----------------------------------------------------------------------------------------------
                '/ Standard Customer fields
                '/ -----------------------------------------------------------------------------------------------
                ASPxPageControl1.TabPages(0).Text = "Customer Details (#" & Trim(Str(nCustomerId)) & ")"
                txtBusinessName.Text = .Item("BusinessName").ToString
                txtAddress1.Text = .Item("Address1").ToString
                txtAddress2.Text = .Item("Address2").ToString
                txtAddress3.Text = .Item("Address3").ToString
                txtAddress4.Text = .Item("Address4").ToString
                txtAddress5.Text = .Item("Address5").ToString
                txtAddress6.Text = .Item("Address6").ToString
                txtPostCode.Text = .Item("PostCode").ToString
                If Not IsInCDA(Session("SelectionLevel"), Session("SelectionId"), .Item("PostCodeSector")) Then
                    lblCDA.Text = "(Out of CDA)"
                End If
                txtYPCategory.Text = .Item("YPCategory").ToString & " - " & .Item("YPDescription").ToString
                For i = 0 To ddlBusinessType.Items.Count - 1
                    If ddlBusinessType.Items(i).Value = .Item("BusinessTypeId") Then
                        ddlBusinessType.SelectedIndex = i
                    End If
                Next
                txtTelephone.Text = .Item("TelephoneNumber").ToString
                txtProprietorName.Text = .Item("ProprietorsName").ToString
                txtProprietorJobTitle.Text = .Item("ProprietorsJobTitle").ToString
                txtKeyContactName.Text = .Item("KeyContactName").ToString
                txtKeyContactJobTitle.Text = .Item("KeyContactJobTitle").ToString
                cblMailFlags1.Items(0).Selected = (.Item("SafeToMail").ToString = "Y")
                cblMailFlags1.Items(1).Selected = (.Item("SafeToPhone").ToString = "Y")
                lblCreated.Text = Trim(.Item("CreatedUserName").ToString) & " " & .Item("CreatedDate").ToString
                lblUpdated.Text = Trim(.Item("UpdatedUserName").ToString) & " " & .Item("LastUpdatedDate").ToString

                '/ -----------------------------------------------------------------------------------------------
                '/ Centre Specific Info
                '/ -----------------------------------------------------------------------------------------------
                Call GetCentreSpecificInfo(nCustomerId, Session("SelectionId"), sVanRoute, sDriveTime, sDistance, _
                                           sSalesRep, sContactName, sTelephoneNumber, sUserField1, sUserField2, _
                                           sUserField3, sUserField4, sNotes)
                txtVanRoute.Text = sVanRoute
                txtDriveTime.Text = sDriveTime
                txtDistance.Text = sDistance
                txtSalesRep.Text = sSalesRep
                txtContactNameSpec.Text = sContactName
                txtTelephoneSpec.Text = sTelephoneNumber
                txtUserField1.Text = sUserField1
                txtUserField2.Text = sUserField2
                txtUserField3.Text = sUserField3
                txtUserField4.Text = sUserField4
                txtNotes.Text = sNotes

                txtDealerCode.Text = sDealerCode & " " & GetDataString("SELECT CentreName FROM CentreMaster WHERE DealerCode = '" & sDealerCode & "'")
                txtTradeClub.Text = .Item("TradeClubNumber").ToString
                txtMobile.Text = .Item("MobileNumber").ToString
                txtFaxNumber.Text = .Item("FaxNumber").ToString
                txtEmailAddress.Text = .Item("EmailAddress").ToString
                txtWebSite.Text = .Item("WebsiteAddress").ToString

                cblMailFlags2.Items(0).Selected = (.Item("SafeToFax").ToString = "Y")
                cblMailFlags2.Items(1).Selected = (.Item("SafeToEMail").ToString = "Y")

                spinRamps.Number = .Item("NoRamps").ToString
                spinTechnicians.Number = .Item("NoTech").ToString
                spinVehicleRepairs.Number = .Item("NoRepairs").ToString
                spinToyotaRepairs.Number = .Item("NoToyotaRepairs").ToString

                cblServices.Items(0).Selected = (.Item("Flag1").ToString = 1)
                cblServices.Items(1).Selected = (.Item("Flag2").ToString = 1)
                cblServices.Items(2).Selected = (.Item("Flag3").ToString = 1)
                cblServices.Items(3).Selected = (.Item("Flag4").ToString = 1)
                cblServices.Items(4).Selected = (.Item("Flag5").ToString = 1)
                cblServices.Items(5).Selected = (.Item("Flag6").ToString = 1)

                txtOther.Text = .Item("Specialism").ToString

                txtMotorFactor1.Text = .Item("Factor1").ToString
                txtSpend1.Text = Format(Val(.Item("Spend1").ToString), "#,##0.00")
                txtMotorFactor2.Text = .Item("Factor2").ToString
                txtSpend2.Text = Format(Val(.Item("Spend2").ToString), "#,##0.00")
                txtMotorFactor3.Text = .Item("Factor3").ToString
                txtSpend3.Text = Format(Val(.Item("Spend3").ToString), "#,##0.00")
                txtMotorFactor4.Text = .Item("Factor4").ToString
                txtSpend4.Text = Format(Val(.Item("Spend4").ToString), "#,##0.00")

                If txtReason.Text <> "" Then
                    btnApprove.Text = "Allow"
                    btnDecline.Text = "Undo"
                Else
                    tabReason.Visible = False
                End If

            End With

        End If

    End Sub

    Private Sub ApproveReg()

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@nId", nTCRegId)
        htIn.Add("@nUserId", Session("UserId"))

        nRes = da.Update(sErr, "p_Customer_Upd_TCLink_Approve", htIn)
        If sErr.Length > 0 Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Membership Approved", "TC Membership : " & txtBusinessName.Text)
        Call FetchCompanyDetails()
        lblStatusMessage.Text = "This Trade Club Membership has now been approved."

    End Sub

    Private Sub AllowRemoval()

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@nId", nTCRegId)
        htIn.Add("@nUserId", Session("UserId"))

        nRes = da.Update(sErr, "p_Customer_Upd_TCLink_Remove", htIn)
        If sErr.Length > 0 Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Membership removal approved", "TC Membership : " & txtBusinessName.Text)
        Call FetchCompanyDetails()
        lblStatusMessage.Text = "This Trade Club Membership has now been removed."

    End Sub

    Private Sub DeclineReg()

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@nId", nTCRegId)
        htIn.Add("@nUserId", Session("UserId"))

        nRes = da.Update(sErr, "p_Customer_Upd_TCLink_Decline", htIn)
        If sErr.Length > 0 Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Membership DECLINED", "TC Membership : " & txtBusinessName.Text)
        Call FetchCompanyDetails()
        lblStatusMessage.Text = "This Trade Club Membership has now been DECLINED."

    End Sub

    Private Sub UndoRemoval()

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@nId", nTCRegId)
        htIn.Add("@nUserId", Session("UserId"))

        nRes = da.Update(sErr, "p_Customer_Upd_TCLink_Undo", htIn)
        If sErr.Length > 0 Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Membership removal undone", "TC Membership : " & txtBusinessName.Text)
        Call FetchCompanyDetails()
        lblStatusMessage.Text = "This Trade Club Membership will NOT now be removed."

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If btnSave.Text = "Decline" Then
            Call DeclineReg()
        ElseIf btnSave.Text = "Approve" Then
            Call ApproveReg()
        ElseIf btnSave.Text = "Allow" Then
            Call AllowRemoval()
        ElseIf btnSave.Text = "Undo" Then
            Call UndoRemoval()
        End If
        btnSave.Visible = False
        btnCancel.Visible = False
        btnApprove.Visible = False
        btnDecline.Visible = False
        btnBack.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        lblDeleteMessage.Text = ""
        btnSave.Visible = False
        btnCancel.Visible = False
        btnApprove.Visible = True
        btnDecline.Visible = True
        btnBack.Visible = True
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("ViewTCApprovals.aspx")
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        If btnApprove.Text = "Approve" Then
            lblDeleteMessage.Text = "Are you sure you want to approve this Trade Club registration?"
            btnSave.Text = "Approve"
        Else
            lblDeleteMessage.Text = "Are you sure you want to allow this Trade Club removal?"
            btnSave.Text = "Allow"
        End If
        btnSave.Visible = True
        btnCancel.Visible = True
        btnApprove.Visible = False
        btnDecline.Visible = False
        btnBack.Visible = False

    End Sub

    Protected Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click

        If btnDecline.Text = "Decline" Then
            lblDeleteMessage.Text = "Are you sure you want to DECLINE this Trade Club registration?"
            btnSave.Text = "Decline"
        Else
            lblDeleteMessage.Text = "Are you sure you want to un-do this Trade Club removal?"
            btnSave.Text = "Undo"
        End If
        btnSave.Visible = True
        btnCancel.Visible = True
        btnApprove.Visible = False
        btnDecline.Visible = False
        btnBack.Visible = False

    End Sub

End Class

