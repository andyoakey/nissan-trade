﻿Imports System.Data
Imports System.Net.Mail

Partial Class UserConfEmail

    Inherits System.Web.UI.Page

    Dim sPassword As String
    Dim sEmail As String
    Dim da As New DatabaseAccess, ds As DataSet, dsC As DataSet, sErr, sSQL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Password Confirmation Email"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = False

        Call FetchUserDetails()

    End Sub

    Sub FetchUserDetails()

        sSQL = "SELECT TOP 1 * FROM Web_User WHERE UserId = " & Trim(Str(Request.QueryString(0)))
        ds = da.ExecuteSQL(sErr, sSQL)

        btnSend.Enabled = False

        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                sEmail = .Item("EmailAddress")
                sPassword = .Item("Password")
                lblEmailAddress.Text = "Click OK to send the confirmation email to : " & sEmail
                btnSend.Enabled = True
            End With
        End If

    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click

        Dim MailObj As New SmtpClient
        Dim mm As New MailMessage
        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")
        basicAuthenticationInfo.UserName = "support@toyotavaluechain.net"
        basicAuthenticationInfo.Password = "4AhK56DmpGvn"
        MailObj.Host = "mail.toyotavaluechain.net"
        MailObj.UseDefaultCredentials = False
        MailObj.Credentials = basicAuthenticationInfo

        mm.To.Add(sEmail)
        mm.From = New MailAddress("nissansupport@qubedata.co.uk")
        mm.Subject = "Nissan Trade Site - Forgotten Password"
        mm.Body += "Your password has been retrieved from the database :" & vbCrLf & vbCrLf
        mm.Body += "Your current password is '" + sPassword + "'" + vbCrLf & vbCrLf
        mm.Body += "Please do not reply, this is an automated response from an unmonitored email address." & vbCrLf & vbCrLf
        mm.Body += "If you want to contact us, please use the details in the contact us box." & vbCrLf & vbCrLf
        mm.Body += "Kind Regards" & vbCrLf & vbCrLf
        mm.Body += "The Nissan Trade Site support team."

        MailObj.Send(mm)

        lblEmailAddress.Text = "The confirmation email has been sent to : " & sEmail

        btnSend.Enabled = False
        btnCancel.Text = "Close"

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Write("<script language='javascript'> { window.close();}</script>")
    End Sub

End Class

