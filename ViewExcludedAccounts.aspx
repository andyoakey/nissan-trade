﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewExcludedAccounts.aspx.vb" Inherits="ViewExcludedAccounts" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
                <dx:ASPxLabel 
                    ID="lblPageTitle" 
                    Font-Size="Larger"
                    runat ="server" 
                    Text="Excluded Accounts"  />
             </div>

        <br />
        
        <dx:ASPxGridView 
            ID="gridAccounts" 
            runat="server" 
            AutoGenerateColumns="False"
            DataSourceID="dsLinks" 
            KeyFieldName="CustomerDealerId"  
            Width="100%"
            Styles-Footer-HorizontalAlign="Center"
            Styles-Header-Wrap="True"
            Styles-Header-HorizontalAlign="Center"
            Styles-Cell-Wrap="False"
            Styles-Cell-HorizontalAlign="Center"
            OnLoad="GridStyles" 
            cssclass="grid_styles">
            
            <SettingsPager 
                PageSize="16"
                AllButton-Visible="true">
            </SettingsPager>

            <SettingsBehavior ColumnResizeMode="Control" />

            <SettingsEditing 
                PopupEditFormWidth="750px" 
                EditFormColumnCount="1" 
                Mode="PopupEditForm"
                PopupEditFormModal="True"/>

            <SettingsText PopupEditFormCaption="Excluded Account Details" />

            <Columns>
            
                <dx:GridViewCommandColumn 
                    VisibleIndex="0" 
                    Width="5%">
                    <EditButton Visible="True" Text="View">                       
                    </EditButton>
                </dx:GridViewCommandColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Dealer" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    FieldName="DealerCode" 
                    VisibleIndex="1" 
                    Width="5%" 
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="Dealer Name" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    FieldName="DealerName" 
                    VisibleIndex="2" 
                    Width="16%" 
                    ExportWidth="250">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Account" 
                    FieldName="AccountNumber" 
                    VisibleIndex="3" 
                    Width="6%" 
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Name" 
                    CellStyle-HorizontalAlign="left" 
                    FieldName="BusinessName" 
                    VisibleIndex="4" 
                    Width="20%" 
                    ExportWidth="400">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Total MTD" 
                    FieldName="SpendMTD" 
                    VisibleIndex="5" 
                    Width="16%" 
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="Total YTD" 
                    FieldName="SpendYTD"  
                    VisibleIndex="6"  
                    Width="16%" 
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Reason for Exclusion" 
                    FieldName="ExclusionReason" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="7" 
                    Width="16%" 
                    ExportWidth="200">
                </dx:GridViewDataTextColumn>     
                
            </Columns>
            
            <Templates>
            
                 <EditForm>
                 
                    <div style="padding: 4px 4px 3px 4px">
                    
                       <table width="450px">
                            <tr>
                                <td style="width:20%" align="left">
                                    Account Name
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountName" runat="server" Width="300px" 
                                        TabIndex="1" Text='<%# Eval("BusinessName")%>' />
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    Address
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountAddress1" runat="server" Width="300px" 
                                        TabIndex="2" Text='<%# Eval("Address1")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountAddress2" runat="server" Width="300px" 
                                        TabIndex="3" Text='<%# Eval("Address2")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountAddress3" runat="server" Width="300px" 
                                        TabIndex="4" Text='<%# Eval("Address3")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountAddress4" runat="server" Width="300px" 
                                        TabIndex="5" Text='<%# Eval("Address4")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountAddress5" runat="server" Width="300px" 
                                        TabIndex="6" Text='<%# Eval("Address5")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    Postcode
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountPostCode" runat="server" Width="300px" 
                                        TabIndex="7" Text='<%# Eval("PostCode")%>'/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%" align="left">
                                    Telephone
                                </td>
                                <td align="left" >
                                    <asp:TextBox ID="txtAccountTelephoneNumber" runat="server" Width="300px" 
                                        TabIndex="8" Text='<%# Eval("TelephoneNumber")%>' />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br /> 
                        <table width="450px">
                            <tr>
                                <td id="Td1" runat="server" align="left">
                                    Account #
                                </td>
                                <td id="Td2" runat="server" align="left">
                                    <asp:Label ID="lblCustomerDealerId" runat="server" Text='<%# Eval("CustomerDealerId")%>'></asp:Label>
                                </td>
                                <td id="Td6" runat="server" align="left">
                                    <asp:Label ID="lblAccountNumber" runat="server" Text='<%# Eval("AccountNumber")%>'></asp:Label>
                                </td>
                                <td id="Td3" runat="server" align="left">
                                    Last Updated
                                </td>
                                <td id="Td4" runat="server" align="left">
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("UpdatedUserName")%>'></asp:Label>
                                </td>
                                <td id="Td5" runat="server" align="left">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("LastUpdatedDate")%>'></asp:Label>
                                </td>
                            </tr>
                        </table>

                    </div>
                    
                    <div style="padding: 2px 2px 2px 2px; text-align:right">
                        <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                            runat="server">
                        </dx:ASPxGridViewTemplateReplacement>
                    </div>

                </EditForm>
           </Templates>
            
            </dx:ASPxGridView>
                            
    </div>
                
    <asp:SqlDataSource ID="dsLinks" runat="server" SelectCommand="p_ExcludedCustomerDealerList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nCustomerId" SessionField="CustomerId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridAccounts">
    </dx:ASPxGridViewExporter>

</asp:Content>

