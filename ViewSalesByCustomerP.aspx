﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewSalesByCustomerP.aspx.vb" Inherits="ViewSalesByCustomerP" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxwpg" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxpgw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Mechanical Competitive Summary" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <table id="trSelectionStuff" width="976px">
            <tr id="Tr3" runat="server" style="height:30px;">
                <td style="font-size: small;" align="left" >
                    From
                </td>
                <td style="font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlFrom" runat="server" AutoPostBack="True" Width="180px"   >
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="left" >
                    To
                </td>
                <td style="font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlTo" runat="server" AutoPostBack="True" Width="180px"   >
                    </asp:DropDownList>
                </td>
                <td align="left" >
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Width="250px" 
                          >
                        <asp:ListItem>All Customers</asp:ListItem>
                        <asp:ListItem>Trade Club Members only</asp:ListItem>
                        <asp:ListItem>Not Trade Club Members</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />

        <dxwpg:ASPxPivotGrid ID="gridKeyPFC" runat="server"  DataSourceID="dsPivotData" 
         OptionsCustomization-AllowDrag="false" OptionsView-ShowColumnHeaders="False" 
         OptionsView-ShowRowHeaders ="False" 
         OnCustomCellDisplayText="ASPXPivotResults_CustomCellDisplayText" 
         OptionsView-ShowDataHeaders="False" EnableCallBacks="False" 
            EnableRowsCache="False">
            
            <Fields>
                
                <dxwpg:PivotGridField ID="fieldCustomer" Caption="Customer"
                    AllowedAreas="RowArea" AreaIndex="0"
                    FieldName="Customer Name" Area="RowArea" Width="300">
                </dxwpg:PivotGridField>
                
                <dxwpg:PivotGridField ID="fieldTopLevel" AllowedAreas="ColumnArea" 
                    AreaIndex="0" FieldName="TopLevel" Area="ColumnArea"  
                    SortOrder="Descending">
                </dxwpg:PivotGridField>

                <dxwpg:PivotGridField ID="fieldKeyPFC" AllowedAreas="ColumnArea" 
                    AreaIndex="1" FieldName="KeyPFC" Area="ColumnArea" 
                    Options-ShowTotals ="False">
                </dxwpg:PivotGridField>

                <dxwpg:PivotGridField ID="fieldPFC" AllowedAreas="ColumnArea" 
                    AreaIndex="2" FieldName="PFC" Area="ColumnArea"  
                    Options-ShowTotals ="False">
                </dxwpg:PivotGridField>

                <dxwpg:PivotGridField ID="fieldSaleValue" AllowedAreas="DataArea" AreaIndex="0" 
                    FieldName="SaleValue" Area="DataArea" CellStyle-HorizontalAlign="Center" >
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dxwpg:PivotGridField>
                
            </Fields>
        
            <Styles >
                <CustomizationFieldsHeaderStyle>
                    <Paddings PaddingLeft="12px" PaddingRight="6px" />
                </CustomizationFieldsHeaderStyle>
            </Styles>

            <OptionsView ShowDataHeaders="False" ShowColumnHeaders="False" ShowRowHeaders="False"></OptionsView>

            <Images >
                <CustomizationFieldsBackground >
                </CustomizationFieldsBackground>
                <LoadingPanel >
                </LoadingPanel>
            </Images>

            <OptionsCustomization AllowDrag="False">
            </OptionsCustomization>
        
            <OptionsLoadingPanel>
                <Image >
                </Image>
            </OptionsLoadingPanel>
            <OptionsPager>
                <AllButton Visible="True">
                </AllButton>
            </OptionsPager>
        
        </dxwpg:ASPxPivotGrid>
        <br />
        <br />
        
    </div>
        
    <asp:SqlDataSource ID="dsPivotData" runat="server" SelectCommand="p_CustomerPFC_PivotData" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="PivotFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="PivotTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTradeClubType" SessionField="TradeClubType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxpgw:ASPxPivotGridExporter ID="ASPxPivotGridExporter1"  ASPxPivotGridID="gridKeyPFC" runat="server">
    </dxpgw:ASPxPivotGridExporter>

</asp:Content>

