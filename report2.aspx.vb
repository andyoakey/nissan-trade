﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Collections.Generic
Imports System.Drawing
Imports DevExpress.Web
Imports DevExpress.Web.ASPxGauges
Imports DevExpress.Web.ASPxGauges.Gauges.Circular
Imports DevExpress.XtraGauges.Base
Imports DevExpress.XtraGauges.Core.Model
Imports DevExpress.XtraGauges.Core.Base
Imports DevExpress.XtraGauges.Core.Drawing
Imports DevExpress.Web.ASPxGauges.Gauges
Partial Class report2
    Inherits System.Web.UI.Page

    Dim sErrorMessage As String = ""

    Dim nPartsLast(4) As Integer
    Dim nPartsThis(4) As Integer
    Dim nPartsPerc(4) As Decimal

    Dim nLabourLast(6) As Integer
    Dim nLabourThis(6) As Integer
    Dim nLabourPerc(6) As Decimal

    Dim nRedFrom As Integer = 0
    Dim nRedTo As Integer = 0
    Dim nGreenFrom As Integer = 0
    Dim nGreenTo As Integer = 0

    Dim da As New DatabaseAccess
    Dim htIn1 As New Hashtable
    Dim sErr As String
    Dim sSQL As String
    Dim dsResultsParts As DataSet
    Dim dsResultsLabour As DataSet

    Dim nYear2 As Integer = ThisYear()
    Dim nYear1 As Integer = nYear2 - 1
    Private _thisYear As Integer

    Property ThisYear As Integer
        Get
            Return _thisYear
        End Get
        Set(value As Integer)
            _thisYear = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Sales Analysis"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales Analysis", "Sales Analysis")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If



        '/ ----------------------------------------------------------------------------------------------------
        '/ Move this code into each page for the Quick Access menu
        '/ ----------------------------------------------------------------------------------------------------
        '/If GetUserPreference(Session("UserId"), "QUICKACCESS") = 1 And _
        '/    GetUserPreference(Session("UserId"), "LOGINPAGE") = 1 And _
        '/    Session("ClickHome") = 1 Then

        Dim popQA As DevExpress.Web.ASPxPopupControl
        popQA = CType(Master.FindControl("PopupMasterQuickAccess"), DevExpress.Web.ASPxPopupControl)
        Dim LinkWPS As DevExpress.Web.ASPxHyperLink
        LinkWPS = CType(Master.FindControl("LinkWorkshopPartsSales"), DevExpress.Web.ASPxHyperLink)
        If Not popQA Is Nothing Then
            If Not LinkWPS Is Nothing Then
                '/ If IsLexusInSelection(Session("SelectionLevel"), Session("SelectionId")) Then
                '/LinkWPS.Text = "Check my mechanical competitive workshop part sales growth"
            Else
                LinkWPS.Text = "Check my mechanical competitive Standards Payment"
                '/End If
                '/End If
            End If
            popQA.ShowOnPageLoad = True

        End If
        Session("ClickHome") = 0
        '/ ----------------------------------------------------------------------------------------------------

    End Sub

    '/Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    '/ Call LoadData()
    '/ Call BuildCharts()
    '/ Call CreateGauge1(nPartsPerc(3))
    '/  Call CreateGauge2(nLabourPerc(5))

    '/  End Sub

    Protected Sub IconButtons_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click
        Select Case sender.id
            Case "btn1"
                Response.Redirect("ViewSalesbyPFC.aspx")
            Case "btn2"
                Response.Redirect("ViewSalesByCustomer.aspx")
            Case "btn3"
                Response.Redirect("ViewBestSellingParts.aspx")
            Case "btn4"
                Response.Redirect("ViewBestSellingCustomers.aspx")
            Case "btn5"
                Response.Redirect("AdvQueryTool.aspx")
        End Select
    End Sub







   

End Class