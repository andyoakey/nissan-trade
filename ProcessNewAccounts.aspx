<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ProcessNewAccounts.aspx.vb" Inherits="ProcessNewAccounts" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <script type="text/javascript">
        function grid_SelectionChanged(s,e) {
            s.GetSelectedFieldValues("link" ,GetSelectedFieldValuesCallback);
        }

        function GetSelectedFieldValuesCallback(values) {
            selList.BeginUpdate();
            try {
                selList.ClearItems();
                for(var i=0;i<values.length;i++) {
                    selList.AddItem(values[i]);
                }
            } finally {
                selList.EndUpdate();
            }
            document.getElementById("selCount").innerHTML=grid.GetSelectedRowCount();
        }

        function ViewCompaniesBT(contentUrl) {
            PopupBT.SetContentUrl(contentUrl);
            PopupBT.SetHeaderText('Select a Business Type');
            PopupBT.SetSize(400,230);
            PopupBT.Show();
        }

        function HideBTEdit() {
            PopupBT.Hide();
            grid.Refresh();
        }


    </script>

    <dx:ASPxPopupControl 
        ID="PopupBT" 
        ClientInstanceName="PopupBT" 
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Update Business Type"
        Modal="True" 
        AllowDragging="true"
        AllowResize="true"
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

  <div style="position: relative; font-family: Calibri; text-align: left;" >
        
       <div id="divTitle" style="position:relative; top:0px">
            <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Process New Accounts" />
       </div>

       <br />

        <table id="trSelectionStuff" Width="30%" style="position:relative; margin-bottom: 10px;">
            <tr id="Tr2" runat="server" >
   
                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblFrom"
                            runat="server" 
                            Text="From">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblTo"
                            runat="server" 
                            Text="To">
                    </dx:ASPxLabel>
                </td>


                </tr>

                <tr id="Tr1" runat="server" >

                    <td align="left" width="50%" valign="top" >
                         <dx:ASPxComboBox
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                     </td>

                    <td align="left" width="50%" valign="top">
                         <dx:ASPxComboBox
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                    </td>
                
             </tr>
        </table>
        
        <div style="float: left; width: 10%">
    
            <div class="BottomPadding">
            Selected A/Cs:
        </div>
    
            <dx:ASPxListBox 
            ID="selList" 
            ClientInstanceName="selList" 
            runat="server" 
            Height="250px"
            Width="100%" />

            <div class="TopPadding">
                    Selected count: <span id="selCount" style="font-weight: bold">0</span>
            </div>

            <br />

            <dx:ASPxButton 
                ID ="btnProcessed"
                runat="server" 
                Width="100%"
                Text="Mark As Processed" />

            <br />
            <br />
            <br />

            <dx:ASPxButton 
                ID ="btnExclude"
                runat="server" 
                Width="100%"
                Text="Exclude From System" />


        </div>

        <div style="float: right; width: 88%">

            <dx:ASPxGridView 
            ID="grid" 
            runat="server" 
            onload="gridStyles"
            cssclass="grid_styles"
            KeyFieldName="id"
            SettingsEditing-Mode="PopupEditForm"
            SettingsEditing-EditFormColumnCount="1"
            AutoGenerateColumns="False" 
            DataSourceID="dsProcessNew" 
            ClientInstanceName="grid"
            Width="100%"> 
            <ClientSideEvents SelectionChanged="grid_SelectionChanged" />

            <SettingsDetail  ShowDetailRow="false"/>

                <SettingsText PopupEditFormCaption="Edit Name, Address and Telephone" />
            
                <SettingsPopup HeaderFilter-CloseOnEscape="True" EditForm-AllowResize="false"  EditForm-HorizontalAlign="WindowCenter" EditForm-Modal="true" />

            <Settings 
                ShowHeaderFilterBlankItems="false"
                ShowFilterRow="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior 
                AllowSort="true" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <SettingsPager  Mode="ShowPager" PageSize="16" >
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign ="Center" />

            <Columns>

                   <dx:GridViewCommandColumn ShowSelectCheckbox="True"   ShowEditButton="true" VisibleIndex="0" Width="5%">
                   </dx:GridViewCommandColumn>
             
                <dx:GridViewDataTextColumn 
                    VisibleIndex="0"
                    EditFormSettings-Visible="True"
                    PropertiesTextEdit-Style-BackColor="#999999"
                    ReadOnly="true"
                    FieldName="id"
                    Name="id"
                    caption="ID"
                    Visible="False"
                    >
                </dx:GridViewDataTextColumn>

               <dx:GridViewDataTextColumn 
                    VisibleIndex="2"
                    FieldName="dealer"
                    Name="dealer"
                    caption="Dealer"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    EditFormSettings-Visible="False"
                    ExportWidth="150"
                    Width="12%" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="3"
                    FieldName="link"
                    Name="link"
                    caption="Link"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    EditFormSettings-Visible="False"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="7%" >
                </dx:GridViewDataTextColumn>

                  <dx:GridViewDataTextColumn 
                    VisibleIndex="5"
                    FieldName="businessname"
                    Name="businessname"
                    caption="Business Name"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="15%" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="6"
                    FieldName="fulladdress"
                    Name="fulladdress"
                    EditFormSettings-Visible="False"
                    caption="Full Address"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="31%" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="7"
                    FieldName="address1"
                    Name="address1"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="Address 1"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="8"
                    FieldName="address2"
                    Name="address2"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="Address 2"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    VisibleIndex="8"
                    FieldName="address3"
                    Name="address3"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="Address 3"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    VisibleIndex="8"
                    FieldName="address4"
                    Name="address4"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="Address 4"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    VisibleIndex="8"
                    FieldName="address5"
                    Name="address5"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="Address 5"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                    VisibleIndex="8"
                    FieldName="postcode"
                    Name="postcode"
                    EditFormSettings-Visible="True"
                    Visible="false"
                    caption="PostCode"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Left">
                </dx:GridViewDataTextColumn>
            
                          <dx:GridViewDataTextColumn 
                    VisibleIndex="9"
                    FieldName="telephonenumber"
                    Name="telephonenumber"
                    caption="Phone"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="99"
                    FieldName="businesstypeid"
                    EditFormSettings-Visible="False"
                    Name="businesstypeid"
                    visible="false">
                </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                    Caption="Bus. Type" 
                    EditFormSettings-Visible="False"
                    FieldName="businesstypedescription" 
                    Name="businesstypedescription" 
                    VisibleIndex="10" 
                    Visible="true"
                    Width="5%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="200">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink 
                            ID="hyperLink" 
                             runat="server" 
                             OnInit="BusinessType_Init" >
                        </dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>        

                <dx:GridViewDataDateColumn 
                    VisibleIndex="12"
                    FieldName="createddate"
                    EditFormSettings-Visible="False"
                    Name="createddate"
                    caption="Date Added"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="7%" >
                </dx:GridViewDataDateColumn>

            </Columns>




          </dx:ASPxGridView>

        </div>
         
    </div>
    
    <asp:SqlDataSource ID="dsProcessNew" runat="server" SelectCommand="sp_HONewCustomerDetail " SelectCommandType="StoredProcedure" UpdateCommand="select * from customerdealer where 1=2">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridExcluded" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>
