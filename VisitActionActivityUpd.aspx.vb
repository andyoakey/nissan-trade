﻿Imports System.Data
Imports System.Net.Mail

Partial Class VisitActionActivityUpd

    Inherits System.Web.UI.Page

    Dim nPageType As Integer
    Dim nVarId As Integer
    Dim nId As Integer

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewVisitLine.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim sTargetDate As String = ""
        Dim bOk As Boolean = True

        Try
            sTargetDate = Format(CDate(txtTargetDate.Text), "dd-MMM-yyyy")
        Catch ex As Exception
            bOk = False
        End Try

        If txtActivity.Text <> "" And txtComments.Text <> "" And txtResponsibility.Text <> "" And sTargetDate <> "" And bOk Then

            If nPageType = 0 Then
                htIn.Add("@nVARId", nVarId)
                htIn.Add("@sActivity", txtActivity.Text)
                htIn.Add("@sComments", txtComments.Text)
                htIn.Add("@sResponsibility", txtResponsibility.Text)
                htIn.Add("@sTargetDate", sTargetDate)
                htIn.Add("@nUserId", Session("UserId"))
                nRes = da.Update(sErr, "p_VAR_Detail_Ins", htIn)
            Else
                htIn.Add("@nId", nId)
                htIn.Add("@sActivity", txtActivity.Text)
                htIn.Add("@sComments", txtComments.Text)
                htIn.Add("@sResponsibility", txtResponsibility.Text)
                htIn.Add("@sTargetDate", sTargetDate)
                htIn.Add("@nUserId", Session("UserId"))
                nRes = da.Update(sErr, "p_VAR_Detail_Upd", htIn)
            End If

            If sErr.Length = 0 Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Visit Activity added", "Id : " & nVarId)
                Response.Redirect("VisitActions.aspx")
            Else
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nPageType = Val(Request.QueryString(0))
        If nPageType = 0 Then
            nVarId = Val(Request.QueryString(1))
            nId = 0
        Else
            nId = Val(Request.QueryString(1))
            nVarId = GetDataLong("SELECT VarId FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId)))
            If Not Page.IsPostBack Then
                Call FetchDetails()
            End If
        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        txtActivity.Focus()
    End Sub

    Private Sub FetchDetails()

        Dim da As New DatabaseAccess
        Dim ds As DataSet
        Dim sErr As String = ""
        Dim sSQL As String = ""

        sSQL = "SELECT * FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId))
        ds = da.ExecuteSQL(sErr, sSQL)

        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                txtActivity.Text = "" & .Item("Activity")
                txtComments.Text = "" & .Item("Comments")
                txtResponsibility.Text = "" & .Item("Responsibility")
                txtTargetDate.Date = CDate("" & .Item("TargetDate"))
            End With
        End If

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("VisitActions.aspx")
    End Sub

End Class
