﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ContHistSummary.aspx.vb" Inherits="ContHistSummary" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
        function ViewContHistDrillDown(contentUrl) {
            PopHistDrillDown.SetContentUrl(contentUrl);
            PopHistDrillDown.SetHeaderText('Contact History');
            PopHistDrillDown.SetSize(920, 595);
            PopHistDrillDown.Show();
        }
        function HideContHistDrillDown() {
            PopHistDrillDown.Hide();
        }
    </script>

    <dxp:ASPxPopupControl ID="PopHistDrillDown" ClientInstanceName="PopHistDrillDown" Width="590px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="Contact History"
        Modal="False" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">
            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:2px; top: -5px;">
        
        <table id="trSelectionStuff"style="position: relative; left: 0px" Width="100%" >
            <tr id="Tr3" runat="server" style="height:30px;">
                <td style="font-size: small; width:50%" align="left" >
                    <asp:Label  Font-Size="18px" Font-Names="Calibri"  ID="lblPageTitle" runat="server" Text="Contact History Tracker" 
                        style="font-weight: 700;" >
                    </asp:Label>
                </td>
                <td style="font-size: small;" align="right">
                    <dxwgv:ASPxCheckBox ID="chkTradeClubOnly" runat="server" Text="Trade Club Members Only" AutoPostBack="true" ></dxwgv:ASPxCheckBox>
                </td>
                <td style="font-size: small;" align="right">
                    <asp:DropDownList ID="ddlContactType" runat="server" AutoPostBack="True" Width="180px"   >
                        <asp:ListItem Text="All" Selected="True" Value="-1" />
                        <asp:ListItem Text="Phone"  Value="0" />
                        <asp:ListItem Text="Email"  Value="1" />
                        <asp:ListItem Text="Visit"  Value="2" />
                        <asp:ListItem Text="Mail"  Value="3" />
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="right">
                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        </div>
        
        <dxwgv:ASPxGridView ID="gridContHist" runat="server" style="position:relative; left:5px; "  Font-Size="7pt" AutoGenerateColumns="False" Width="100%">
            
            <Styles>
                <Header Font-Bold = "True" ImageSpacing="5px" SortingImageSpacing="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" Font-Size = "7pt" />
                <Footer Font-Bold = "True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" Font-Size = "7pt" />
                <Cell   HorizontalAlign="Center" Wrap="True" />
            </Styles>

            <SettingsPager PageSize = "16" Mode="ShowPager" Visible="True">
                <AllButton Visible="True" />
                <FirstPageButton Visible="True" />
                <LastPageButton Visible="True" />
            </SettingsPager>

            <Settings ShowFooter="True" ShowHeaderFilterButton="False" ShowTitlePanel="False" />
           
            <Columns>
            
                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="0" ExportWidth="100"/>
                
                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" Name="CentreName" VisibleIndex="1" Width="25%" ExportWidth="300">
                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" Wrap="False" />
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone" VisibleIndex="2" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="TC visits in last 6 weeks" FieldName="Six_Weeks" Name="Six_Weeks" VisibleIndex="3" Width="7%">
                    <DataItemTemplate>
                        <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                        </dxe:ASPxHyperLink>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>        

                <dxe:GridViewBandColumn Caption="Contact History" >
                    <Columns>

                        <dxwgv:GridViewDataTextColumn Caption="Jan" FieldName="Month_1" Name="Month_1" VisibleIndex="4" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>        

                        <dxwgv:GridViewDataTextColumn Caption="Feb" FieldName="Month_2" Name="Month_2" VisibleIndex="5" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>        

                        <dxwgv:GridViewDataTextColumn Caption="Mar" FieldName="Month_3" Name="Month_3" VisibleIndex="6" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>        

                        <dxwgv:GridViewDataTextColumn Caption="Apr" FieldName="Month_4" Name="Month_4" VisibleIndex="7" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>   
                             
                        <dxwgv:GridViewDataTextColumn Caption="May" FieldName="Month_5" Name="Month_5" VisibleIndex="8" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn> 
                               
                        <dxwgv:GridViewDataTextColumn Caption="Jun" FieldName="Month_6" Name="Month_6" VisibleIndex="9" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>  
                              
                        <dxwgv:GridViewDataTextColumn Caption="Jul" FieldName="Month_7" Name="Month_7" VisibleIndex="10" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>  
                              
                        <dxwgv:GridViewDataTextColumn Caption="Aug" FieldName="Month_8" Name="Month_8" VisibleIndex="11" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn> 
                               
                        <dxwgv:GridViewDataTextColumn Caption="Sep" FieldName="Month_9" Name="Month_9" VisibleIndex="12" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                                
                        <dxwgv:GridViewDataTextColumn Caption="Oct" FieldName="Month_10" Name="Month_10" VisibleIndex="13" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>  
                              
                        <dxwgv:GridViewDataTextColumn Caption="Nov" FieldName="Month_11" Name="Month_11" VisibleIndex="14" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>  
                              
                        <dxwgv:GridViewDataTextColumn Caption="Dec" FieldName="Month_12" Name="Month_12" VisibleIndex="15" ExportWidth="100">
                            <DataItemTemplate>
                                <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="HistDrillDown_Init" Font-Size="X-Small"  Font-Underline="true">
                                </dxe:ASPxHyperLink>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>   
                             
                        <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="ContactTotal" VisibleIndex="16" ExportWidth="100" />

                    </Columns>
                </dxe:GridViewBandColumn>

            </Columns>
           
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            
            <TotalSummary>
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_1" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_2" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_3" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_4" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_5" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_6" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_7" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_8" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_9" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_10" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_11" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Month_12" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="ContactTotal" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Six_Weeks" SummaryType="Sum" />
            </TotalSummary>

        </dxwgv:ASPxGridView>
        <br />
        
        <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridContHist" PreserveGroupRowStates="False">
        </dxwgv:ASPxGridViewExporter>

    </div>

</asp:Content>


