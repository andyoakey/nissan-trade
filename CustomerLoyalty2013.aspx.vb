﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports System.Collections.Generic

Partial Class CustomerLoyalty

    Inherits System.Web.UI.Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Session("CallingModule") = "CustomerLoyalty.aspx - PageLoad"

        Me.Title = "qubeDATA PARTS - Customer Loyalty"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Page.IsPostBack Then
            LoadLoyalty(False)
        Else
            LoadLoyalty(True)
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub CheckBoxList1_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CheckBoxList1.SelectedIndexChanged
        For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
            Select Case c.Name.ToLower()
                Case "q1"
                    c.Visible = CheckBoxList1.Items(0).Selected
                Case "q2"
                    c.Visible = CheckBoxList1.Items(1).Selected
                Case "q3"
                    c.Visible = CheckBoxList1.Items(2).Selected
                Case "q4"
                    c.Visible = CheckBoxList1.Items(3).Selected
            End Select
        Next
    End Sub

    Private Sub UpdateMenuState()
        For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
            Select Case c.Name.ToLower()
                Case "q1"
                    If CheckBoxList1.Items(0).Enabled Then
                        CheckBoxList1.Items(0).Selected = c.Visible
                    End If
                Case "q2"
                    If CheckBoxList1.Items(1).Enabled Then
                        CheckBoxList1.Items(1).Selected = c.Visible
                    End If
                Case "q3"
                    If CheckBoxList1.Items(2).Enabled Then
                        CheckBoxList1.Items(2).Selected = c.Visible
                    End If
                Case "q4"
                    If CheckBoxList1.Items(3).Enabled Then
                        CheckBoxList1.Items(3).Selected = c.Visible
                    End If
            End Select
        Next
    End Sub

    Sub LoadLoyalty(ByVal bFirstLoad As Boolean)

        Dim nThisMonth As Integer = Month(Now)

        nThisMonth = 12 '/ temp until we rollover

        Session("CallingModule") = "CustomerLoyalty.aspx - LoadLoyalty"

        Dim htIn As New Hashtable

        htIn.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn.Add("@sSelectionId", Session("SelectionId"))

        dsResults = da.Read(sErrorMessage, "p_CustomerLoyalty_2013", htIn)
        ASPxGridView1.DataSource = dsResults.Tables(0)
        ASPxGridView1.DataBind()

        If bFirstLoad Then

            For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
                If (c.Name.Equals("Q1") Or c.Name.Equals("Q2") Or c.Name.Equals("Q3") Or c.Name.Equals("Q4")) Then
                    ASPxGridView1.Columns(c.Index).Visible = False
                End If
            Next

            If nThisMonth >= 1 And nThisMonth <= 3 Then
                For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
                    If (c.Name.Equals("Q1")) Then
                        ASPxGridView1.Columns(c.Index).Visible = True
                    End If
                Next
                CheckBoxList1.Items(0).Enabled = True
                CheckBoxList1.Items(1).Enabled = False
                CheckBoxList1.Items(2).Enabled = False
                CheckBoxList1.Items(3).Enabled = False
            ElseIf nThisMonth >= 4 And nThisMonth <= 6 Then
                For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
                    If (c.Name.Equals("Q2")) Then
                        ASPxGridView1.Columns(c.Index).Visible = True
                    End If
                Next
                CheckBoxList1.Items(0).Enabled = True
                CheckBoxList1.Items(1).Enabled = True
                CheckBoxList1.Items(2).Enabled = False
                CheckBoxList1.Items(3).Enabled = False
            ElseIf nThisMonth >= 7 And nThisMonth <= 9 Then
                For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
                    If (c.Name.Equals("Q3")) Then
                        ASPxGridView1.Columns(c.Index).Visible = True
                    End If
                Next
                CheckBoxList1.Items(0).Enabled = True
                CheckBoxList1.Items(1).Enabled = True
                CheckBoxList1.Items(2).Enabled = True
                CheckBoxList1.Items(3).Enabled = False
            ElseIf nThisMonth >= 10 Then
                For Each c As DevExpress.Web.GridViewColumn In ASPxGridView1.AllColumns
                    If (c.Name.Equals("Q4")) Then
                        ASPxGridView1.Columns(c.Index).Visible = True
                    End If
                Next
                CheckBoxList1.Items(0).Enabled = True
                CheckBoxList1.Items(1).Enabled = True
                CheckBoxList1.Items(2).Enabled = True
                CheckBoxList1.Items(3).Enabled = True
            End If

            UpdateMenuState() ' So we are not showing hidden quarters as being active
        End If
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String
        Session("CallingModule") = "CustomerLoyalty.aspx - ExportToExcel button"
        sFileName = "CustomerLoyalty"

        LoadLoyalty(False)

        ASPxGridViewExporter1.WriteXlsToResponse(fileName:=sFileName)
    End Sub
    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)

        If (e.Column.Name.ToLower().Contains("_rebate")) Then
            If (e.Value < 0) Then
                e.BrickStyle.ForeColor = Drawing.Color.Red
            Else
                e.BrickStyle.ForeColor = Drawing.Color.Green
            End If

            If e.Value = -9999999 Then
                e.TextValue = "NON-QUALIFY"
            End If
        End If

        If (e.Column.Name.ToLower().Contains("_yoypct") And e.TextValueFormatString.Length > 0) Then
            If e.Value <= 0 Then
                e.BrickStyle.ForeColor = Drawing.Color.Red
            Else
                e.BrickStyle.ForeColor = Drawing.Color.Green
            End If
        End If

    End Sub

    Protected Sub ASPxGridView1_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles ASPxGridView1.HtmlDataCellPrepared

        Session("CallingModule") = "CustomerLoyalty.aspx - ASPxGridView1_HtmlDataCellPrepared"

        If (e.DataColumn.FieldName.ToLower().Contains("_rebate")) Then
            If e.CellValue < 0 Then
                e.Cell.ForeColor = Drawing.Color.Red
            Else
                e.Cell.ForeColor = Drawing.Color.Green
            End If

            If e.CellValue = -9999999 Then
                e.Cell.Text = "NON-QUALIFY"
            End If
        End If

        If (e.DataColumn.FieldName.ToLower().Contains("_yoypct")) Then
            If e.CellValue <= 0 Then
                e.Cell.ForeColor = Drawing.Color.Red
            Else
                e.Cell.ForeColor = Drawing.Color.Green
            End If
        End If

    End Sub

End Class
