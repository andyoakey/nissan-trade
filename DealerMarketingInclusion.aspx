﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DealerMarketingInclusion.aspx.vb" Inherits="DealerMarketingInclusion" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >

     <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblTitle" 
                runat ="server" 
                Text="Dealer Marketing Inclusion" />
    </div>

    <br />

    <dx:ASPxGridView 
        ID="gridDealer" 
        OnLoad ="GridStyles"
        runat="server"              
        KeyFieldName="DealerCode"
        AutoGenerateColumns="False"
        DataSourceID="dsDealers" 
        Width="100%">

        <SettingsText CommandClearFilter="Clear" Title="Active Filters:" />
   
        <SettingsPager 
            PageSize="16" 
            ShowDefaultImages="True" 
            AllButton-Visible="true"/>
        
        <Settings 
            UseFixedTableLayout="true"
            ShowFilterRowMenu="True" 
            ShowFooter="True" 
            ShowHeaderFilterBlankItems="false"
            ShowGroupedColumns="True"
            ShowGroupFooter="VisibleIfExpanded" 
            ShowHeaderFilterButton="True"
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False" />
        
        <SettingsBehavior 
            AllowSort="False" 
            ColumnResizeMode="Control" /> 
           
        <Columns>
   
        
           <dx:GridViewDataTextColumn 
                FieldName="DealerCode" 
                VisibleIndex="0"
                Visible="false">
         </dx:GridViewDataTextColumn>
      
        <dx:GridViewDataTextColumn 
                Caption="Region" 
                FieldName="Region" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Width="10%"
                VisibleIndex="0">
                <EditFormSettings Visible ="False" />
            </dx:GridViewDataTextColumn>
        
            <dx:GridViewDataTextColumn 
                Caption="Zone" 
                FieldName="Zone" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Width="10%"
                VisibleIndex="1">
                <EditFormSettings Visible ="False" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer Group" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="DealerGroup" 
                Settings-AllowHeaderFilter="True"
                Width="20%"
                VisibleIndex="2">
                <EditFormSettings Visible ="False" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                Settings-AllowHeaderFilter="True"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                readonly ="true" 
                FieldName="Dealer" 
                VisibleIndex="3"
                exportwidth="200"
                Width="20%">
                <EditFormSettings Visible ="False" />
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataComboBoxColumn
                Caption="Excluded From Marketing" 
                Settings-AllowHeaderFilter="True"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                FieldName="Exclude_Text" 
                VisibleIndex="4"
                exportwidth="100"
                Width="15%">
                <HeaderStyle Wrap="True"  />
                <Settings HeaderFilterMode="CheckedList" /> 
                <EditFormSettings Visible ="True" />
               <PropertiesComboBox  FocusedStyle-HorizontalAlign ="Center" Style-HorizontalAlign="Center" ValueType="System.Int32"  ListBoxStyle-HorizontalAlign="Center" >
                    <Items>
                        <dx:ListEditItem Text ="No"  Selected="true"  Value="0"  />
                        <dx:ListEditItem Text ="Yes"  Selected="false"  Value="1" />
                    </Items>
                </PropertiesComboBox>
                <EditCellStyle  BackColor="Wheat"></EditCellStyle>
       
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn 
                Caption="Notes" 
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                FieldName="Notes" 
                VisibleIndex="5"
                exportwidth="100"
                Width="35%">
                <HeaderStyle Wrap="True" />
                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                <EditCellStyle  BackColor="Wheat"></EditCellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewCommandColumn  ShowEditButton="true" VisibleIndex="6"  Width="5%"/>
              
         </Columns>
            
        <SettingsEditing Mode="Inline" />
   
   
  </dx:ASPxGridView>

</div>

 
  <asp:SqlDataSource 
        ID="dsDealers" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_DealerMarketingInclusion" 
        UpdateCommand="Select * from systemcontrol where 1=2" 
        SelectCommandType="StoredProcedure">
  </asp:SqlDataSource>

  <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1"
        runat="server" 
        GridViewID="gridDealers">
    </dx:ASPxGridViewExporter>



</asp:Content>

