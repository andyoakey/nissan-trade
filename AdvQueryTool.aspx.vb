﻿Imports System.Data
Imports System.Collections.Generic


Partial Class AdvQueryTool

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess
    Dim ds As DataSet
    Dim sErr As String = ""

    Private Shared sGridLayout As String


    ' These are the Field Selection Options in correct order:
    'Dealer:
    '   1  Region 
    '	2  Zone
    '	3  Dealer Code
    '	4  Dealer Name

    'Customer:
    '   5  Customer ID
    '	6  Account
    '	7  Customer Name
    '	8  Address
    '	9  PostCode
    '	10 Telephone Number
    '	11 Email Address
    '	12 Focus Account
    '	13 Business Type

    'Location:
    '   14 Drive Time
    '	15 Distance
    '	16 Sales Rep
    '	17 Van Route

    'Product:
    '   18 Service Description
    '	19 Trade Analysis
    '	20 Description Code
    '	21 Part Number

    'Invoice:
    '   22 Invoice Month
    '	23 Invoice Number
    '	24 Invoice Date

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Advanced Reporting Tool"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then

            ASPxPageControl1.ActiveTabIndex = 0

            Session("SelectFieldList") = "''"
            Session("ProductCategoryList") = "''"
            Session("ProductGroupList") = "''"
            Session("BusinessTypeList") = "''"
            Session("PFCList") = "''"
            Session("PartList") = "''"
            Session("BlueGrassID") = 0   ' Defaults to all records ( other possible values 1 = bluegrass only, 2 = NON bluegrass only)
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
            Session("MonthTo") = Session("MonthFrom")

            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
            Call GetProductGroupsDevX(chkProductGroups)
            Call GetBusinessTypesChkBoxesDevX(chkBusinessTypes)
            ddlType.SelectedIndex = 0

            Call GetQueryDefnDevEx(ddlQueries, Session("UserID"))

            ddlQueries.SelectedIndex = -1

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Adv Query", "Running Advanced Query Tool")

        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExportToExcel_Click()
        End If


    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = Val(ddlFrom.Value)
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = Val(ddlTo.Value)
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("BlueGrassID") = ddlType.SelectedItem.Value
    End Sub

    Protected Sub dsResults_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsResults.Init
        dsResults.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    'Protected Sub chkFields_Render(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFields.PreRender
    '    For Each l As ListItem In chkFields.Items
    '        If Not l.Enabled Then
    '            l.Attributes.Add("class", "chkBoxCatLabel")
    '        End If
    '    Next l
    'End Sub

    Private Sub GetResultsBound()

        Call SaveDefVars(True)
        lblQSave.Text = ""

        Session("DateCompare") = 0
        If chkDateComparison.Checked = True Then Session("DateCompare") = 1

        With ASPXPivotResults

            .Fields.Clear()

            'If Mid(Session("SelectFieldList"), 34, 1) = "1" Then
            '    Call AddPivotField("Notes", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            'End If

            'If Mid(Session("SelectFieldList"), 33, 1) = "1" Then
            '    Call AddPivotField("UserField4", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            'End If

            'If Mid(Session("SelectFieldList"), 32, 1) = "1" Then
            '    Call AddPivotField("UserField3", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            'End If

            'If Mid(Session("SelectFieldList"), 31, 1) = "1" Then
            '    Call AddPivotField("UserField2", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            'End If

            'If Mid(Session("SelectFieldList"), 30, 1) = "1" Then
            '    Call AddPivotField("UserField1", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            'End If

            ' chkInvoice
            If Mid(Session("SelectFieldList"), 24, 1) = "1" Then
                Call AddPivotField("InvoiceDate", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 23, 1) = "1" Then
                Call AddPivotField("Invoice", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 22, 1) = "1" Then
                Call AddPivotField("Month", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If
            ' End chkInvoice

            ' chkProduct
            If Mid(Session("SelectFieldList"), 21, 1) = "1" Then
                Call AddPivotField("PartNumber", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 20, 1) = "1" Then
                Call AddPivotField("DescCode", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 19, 1) = "1" Then
                Call AddPivotField("TradeAnalysis", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 18, 1) = "1" Then
                Call AddPivotField("ServiceDescription", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If
            ' End chkProduct

            ' chkLocation
            If Mid(Session("SelectFieldList"), 17, 1) = "1" Then
                Call AddPivotField("VanRoute", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 16, 1) = "1" Then
                Call AddPivotField("SalesRep", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 15, 1) = "1" Then
                Call AddPivotField("Distance", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 14, 1) = "1" Then
                Call AddPivotField("DriveTime", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If
            ' End chkLocation

            ' chkCustomer
            If Mid(Session("SelectFieldList"), 13, 1) = "1" Then
                Call AddPivotField("BusinessType", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 12, 1) = "1" Then
                Call AddPivotField("Focus", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 11, 1) = "1" Then
                Call AddPivotField("EmailAddress", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 10, 1) = "1" Then
                Call AddPivotField("CustomerTelephoneNumber", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 9, 1) = "1" Then
                Call AddPivotField("CustomerPostCode", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 8, 1) = "1" Then
                Call AddPivotField("CustomerAddress", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 7, 1) = "1" Then
                Call AddPivotField("CustomerName", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 6, 1) = "1" Then
                Call AddPivotField("Account", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 5, 1) = "1" Then
                Call AddPivotField("CustomerID", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If
            'End chkCustomer


            ' chkDealer
            If Mid(Session("SelectFieldList"), 4, 1) = "1" Then
                Call AddPivotField("DealerName", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 3, 1) = "1" Then
                Call AddPivotField("DealerCode", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 2, 1) = "1" Then
                Call AddPivotField("Zone", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If

            If Mid(Session("SelectFieldList"), 1, 1) = "1" Then
                Call AddPivotField("Region", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.All, DevExpress.XtraPivotGrid.PivotArea.RowArea)
            End If
            'End chkDealer


            ' chkValues
            If chkValues.Items(0).Selected Then
                Call AddPivotField("Quantity", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                If Session("DateCompare") = 1 Then
                    Call AddPivotField("Quantity_Last", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                End If
            End If

                If chkValues.Items(1).Selected Then
                Call AddPivotField("SaleValue", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                If Session("DateCompare") = 1 Then
                    Call AddPivotField("SaleValue_Last", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                End If
            End If

            If chkValues.Items(2).Selected Then
                Call AddPivotField("CostValue", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                If Session("DateCompare") = 1 Then
                    Call AddPivotField("CostValue_Last", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                End If
            End If

            If chkValues.Items(3).Selected Then
                Call AddPivotField("Margin", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                If Session("DateCompare") = 1 Then
                    Call AddPivotField("Margin_Last", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                End If
            End If

            If chkValues.Items(4).Selected Then
                Call AddPivotField("RetailValue", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                If Session("DateCompare") = 1 Then
                    Call AddPivotField("RetailValue_Last", DevExpress.XtraPivotGrid.PivotGridAllowedAreas.DataArea, DevExpress.XtraPivotGrid.PivotArea.DataArea)
                End If
            End If
            ' End chkValues


            'Session("ProductCategoryList") = "'" & Replace(memTAItems.Text, vbCrLf, "','") & "'"
            'If Right(Session("ProductCategoryList"), 3) = ",''" Then
            'Session("ProductCategoryList") = Left(Session("ProductCategoryList"), Len(Session("ProductCategoryList")) - 3)
            'End If



            .Visible = True
            .DataBind()

        End With

    End Sub

    Private Function CollateListBoxValues(ByVal lst As ListBox) As String

        Dim i As Integer
        Dim nSeparator As Integer
        Dim sStr As String
        Dim sItem As String

        sStr = "'"
        For i = 0 To lst.Items.Count - 1
            sItem = Trim(lst.Items(i).Text)
            nSeparator = InStr(sItem, ":")
            sItem = Trim(Left(sItem, nSeparator - 1))
            sStr = sStr & sItem & "','"
        Next

        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If

        CollateListBoxValues = sStr

    End Function

    Private Function CollateSelectionStrings(ByVal ddl As CheckBoxList) As String

        Dim i As Integer
        Dim sStr As String

        sStr = "'"
        For i = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Selected Then
                sStr = sStr & Trim(ddl.Items(i).Text) & "','"
            End If
        Next

        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If

        CollateSelectionStrings = sStr

    End Function

    Private Function CollateSelectionStringsDevX(ByVal ddl As DevExpress.Web.ASPxCheckBoxList) As String

        Dim i As Integer
        Dim sStr As String

        sStr = "'"
        For i = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Selected Then
                sStr = sStr & Trim(ddl.Items(i).Value) & "','"
            End If
        Next

        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If

        CollateSelectionStringsDevX = sStr

    End Function

    Private Shared Function CollateFieldListDevX(ByVal _
                                                 chkDealer As DevExpress.Web.ASPxCheckBoxList,
                                                 chkCustomer As DevExpress.Web.ASPxCheckBoxList,
                                                 chkLocation As DevExpress.Web.ASPxCheckBoxList,
                                                 chkProduct As DevExpress.Web.ASPxCheckBoxList,
                                                 chkInvoice As DevExpress.Web.ASPxCheckBoxList,
                                                 chkUserDefined As DevExpress.Web.ASPxCheckBoxList) As String

        Dim sStr As StringBuilder = New StringBuilder()

        ' chkDealer 
        If True Then
            If chkDealer.Items(0).Selected = True Then  '1 -Region 
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkDealer.Items(1).Selected = True Then '2 -Zone
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkDealer.Items(2).Selected = True Then '3- Dealer Code
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkDealer.Items(3).Selected = True Then '4 -Dealer Name
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If
        End If
        ' End of chkdealer 

        ' chkCustomer
        If True Then
            If chkCustomer.Items(0).Selected = True Then  '5 -Customer ID
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(1).Selected = True Then '6 -Account
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(2).Selected = True Then '7 -Customer Name
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(3).Selected = True Then '8 -Address
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(4).Selected = True Then  '9 -PostCode
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(5).Selected = True Then '10 -Telephone Number
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(6).Selected = True Then '11- Email Address
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(7).Selected = True Then '12 -Focus Account
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkCustomer.Items(8).Selected = True Then '13 -Business Type
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If
            ' End of chkCustomer
        End If

        ' chkLocation
        If True Then
            If chkLocation.Items(0).Selected = True Then  ' 14 - Drive Time
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkLocation.Items(1).Selected = True Then ' 15 -Distance
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkLocation.Items(2).Selected = True Then '16 - Sales Rep
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkLocation.Items(3).Selected = True Then ' 17- Van Route
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If
        End If
        ' End of chkLocation 

        ' chkProduct
        If True Then
            If chkProduct.Items(0).Selected = True Then  ' 18 - Service Description
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkProduct.Items(1).Selected = True Then ' 19 - Trade Analysis
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkProduct.Items(2).Selected = True Then ' 20 -Description Code
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkProduct.Items(3).Selected = True Then ' 21 - Part Number
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If
        End If
        ' End of chkProduct 

        ' chkInvoice
        If True Then
            If chkInvoice.Items(0).Selected = True Then  ' 22 - Invoice Month
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkInvoice.Items(1).Selected = True Then ' 23 - Invoice Number
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If

            If chkInvoice.Items(2).Selected = True Then ' 24 - Invoice Date
                sStr.Append("1")
            Else
                sStr.Append("0")
            End If
        End If
        ' End of chkInvoice 

        ' chkUserDefined
        'If True Then
        '    If chkUserDefined.Items(0).Selected = True Then  ' 25 - User Field 1
        '        sStr.Append("1")
        '    Else
        '        sStr.Append("0")
        '    End If

        '    If chkUserDefined.Items(1).Selected = True Then  ' 26 - User Field 2
        '        sStr.Append("1")
        '    Else
        '        sStr.Append("0")
        '    End If

        '    If chkUserDefined.Items(2).Selected = True Then  ' 27 -User Field 3
        '        sStr.Append("1")
        '    Else
        '        sStr.Append("0")
        '    End If

        '    If chkUserDefined.Items(3).Selected = True Then  ' 28 - User Field 4
        '        sStr.Append("1")
        '    Else
        '        sStr.Append("0")
        '    End If

        '    If chkUserDefined.Items(4).Selected = True Then ' 29 - Notes
        '        sStr.Append("1")
        '    Else
        '        sStr.Append("0")
        '    End If
        'End If
        ' End of chkInvoice 


        Return sStr.ToString()
    End Function

    Sub LoadFieldListDevX(sFieldString As String)

        ' These are the Field Selection Options in correct order:
        'Dealer:
        '   1  Region 
        '	2  Zone
        '	3  Dealer Code
        '	4  Dealer Name
        Dim b1 As Boolean = IIf(Mid(sFieldString, 1, 1) = 1, vbTrue, vbFalse)
        Dim b2 As Boolean = IIf(Mid(sFieldString, 2, 1) = 1, vbTrue, vbFalse)
        Dim b3 As Boolean = IIf(Mid(sFieldString, 3, 1) = 1, vbTrue, vbFalse)
        Dim b4 As Boolean = IIf(Mid(sFieldString, 4, 1) = 1, vbTrue, vbFalse)
        chkDealer.Items(0).Selected = b1
        chkDealer.Items(1).Selected = b2
        chkDealer.Items(2).Selected = b3
        chkDealer.Items(3).Selected = b4

        'Customer:
        '   5  Customer ID
        '	6  Account
        '	7  Customer Name
        '	8  Address
        '	9  PostCode
        '	10 Telephone Number
        '	11 Email Address
        '	12 Focus Account
        '	13 Business Type
        Dim b5 As Boolean = IIf(Mid(sFieldString, 5, 1) = 1, vbTrue, vbFalse)
        Dim b6 As Boolean = IIf(Mid(sFieldString, 6, 1) = 1, vbTrue, vbFalse)
        Dim b7 As Boolean = IIf(Mid(sFieldString, 7, 1) = 1, vbTrue, vbFalse)
        Dim b8 As Boolean = IIf(Mid(sFieldString, 8, 1) = 1, vbTrue, vbFalse)
        Dim b9 As Boolean = IIf(Mid(sFieldString, 9, 1) = 1, vbTrue, vbFalse)
        Dim b10 As Boolean = IIf(Mid(sFieldString, 10, 1) = 1, vbTrue, vbFalse)
        Dim b11 As Boolean = IIf(Mid(sFieldString, 11, 1) = 1, vbTrue, vbFalse)
        Dim b12 As Boolean = IIf(Mid(sFieldString, 12, 1) = 1, vbTrue, vbFalse)
        Dim b13 As Boolean = IIf(Mid(sFieldString, 13, 1) = 1, vbTrue, vbFalse)
        chkCustomer.Items(0).Selected = b5
        chkCustomer.Items(1).Selected = b6
        chkCustomer.Items(2).Selected = b7
        chkCustomer.Items(3).Selected = b8
        chkCustomer.Items(4).Selected = b9
        chkCustomer.Items(5).Selected = b10
        chkCustomer.Items(6).Selected = b11
        chkCustomer.Items(7).Selected = b12
        chkCustomer.Items(8).Selected = b13

        'Location:
        '   14 Drive Time
        '	15 Distance
        '	16 Sales Rep
        '	17 Van Route
        Dim b14 As Boolean = IIf(Mid(sFieldString, 14, 1) = 1, vbTrue, vbFalse)
        Dim b15 As Boolean = IIf(Mid(sFieldString, 15, 1) = 1, vbTrue, vbFalse)
        Dim b16 As Boolean = IIf(Mid(sFieldString, 16, 1) = 1, vbTrue, vbFalse)
        Dim b17 As Boolean = IIf(Mid(sFieldString, 17, 1) = 1, vbTrue, vbFalse)
        chkLocation.Items(0).Selected = b14
        chkLocation.Items(1).Selected = b15
        chkLocation.Items(2).Selected = b16
        chkLocation.Items(3).Selected = b17

        'Product:
        '   18 Service Description
        '	19 Trade Analysis
        '	20 Description Code
        '	21 Part Number
        Dim b18 As Boolean = IIf(Mid(sFieldString, 18, 1) = 1, vbTrue, vbFalse)
        Dim b19 As Boolean = IIf(Mid(sFieldString, 19, 1) = 1, vbTrue, vbFalse)
        Dim b20 As Boolean = IIf(Mid(sFieldString, 20, 1) = 1, vbTrue, vbFalse)
        Dim b21 As Boolean = IIf(Mid(sFieldString, 21, 1) = 1, vbTrue, vbFalse)
        chkProduct.Items(0).Selected = b18
        chkProduct.Items(1).Selected = b19
        chkProduct.Items(2).Selected = b20
        chkProduct.Items(3).Selected = b21

        'Invoice:
        '   22 Invoice Month
        '	23 Invoice Number
        '	24 Invoice Date
        Dim b22 As Boolean = IIf(Mid(sFieldString, 22, 1) = 1, vbTrue, vbFalse)
        Dim b23 As Boolean = IIf(Mid(sFieldString, 23, 1) = 1, vbTrue, vbFalse)
        Dim b24 As Boolean = IIf(Mid(sFieldString, 24, 1) = 1, vbTrue, vbFalse)
        chkInvoice.Items(0).Selected = b22
        chkInvoice.Items(1).Selected = b23
        chkInvoice.Items(2).Selected = b24

    End Sub

    Private Sub AddPivotField(ByVal sFieldName As String, ByVal nAllowedAreas As DevExpress.XtraPivotGrid.PivotGridAllowedAreas, ByVal nArea As DevExpress.XtraPivotGrid.PivotArea)

        Dim nFromValue As Integer = ddlFrom.Value
        Dim nToValue As Integer = ddlTo.Value

        Dim sFromString = GetDataString("select dbo.getShortMonthNameFromDataPeriod2(" & ddlFrom.Value & ")")
        Dim sToString = GetDataString("select dbo.getShortMonthNameFromDataPeriod2(" & ddlTo.Value & ")")
        Dim sMonthText As String = sFromString

        If sFromString <> sToString Then
            sMonthText = sFromString & " - " & sToString
        End If


        Dim sFromLastString = GetDataString("select dbo.getShortMonthNameFromDataPeriod2(" & ddlFrom.Value - 12 & ")")
        Dim sToLastString = GetDataString("select dbo.getShortMonthNameFromDataPeriod2(" & ddlTo.Value - 12 & ")")
        Dim sMonthLastText As String = sFromLastString

        If sFromLastString <> sToLastString Then
            sMonthLastText = sFromLastString & " - " & sToLastString
        End If

        sMonthText = "(" & sMonthText & ")"
        sMonthLastText = "(" & sMonthLastText & ")"

        Dim itmX As New DevExpress.Web.ASPxPivotGrid.PivotGridField
        itmX = ASPXPivotResults.Fields.Add()
        itmX.ID = "field" & sFieldName
        itmX.AllowedAreas = nAllowedAreas
        itmX.AreaIndex = 0
        itmX.FieldName = sFieldName
        itmX.Area = nArea
        itmX.ExpandedInFieldsGroup = False
        itmX.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True
        itmX.HeaderStyle.Wrap = True
        itmX.RunningTotal = False
        If nArea = DevExpress.XtraPivotGrid.PivotArea.DataArea Then
            itmX.CellStyle.HorizontalAlign = HorizontalAlign.Center
            itmX.CellFormat.FormatString = "c2"
            itmX.Caption = itmX.Caption & " " & sMonthText

            If sFieldName = "Quantity" Then
                itmX.CellFormat.FormatString = "0"
                itmX.Caption = "Qty " & sMonthText
            End If

            If sFieldName = "Margin" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Margin " & sMonthText
            End If

            If sFieldName = "SaleValue" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Sale Value " & sMonthText
            End If

            If sFieldName = "RetailValue" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Retail Value " & sMonthText
            End If

            If sFieldName = "CostValue" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Cost Value " & sMonthText
            End If


            If sFieldName = "Margin_Last" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Margin " & sMonthLastText
            End If

            If sFieldName = "Quantity_Last" Then
                itmX.CellFormat.FormatString = "0"
                itmX.Caption = "Qty " & sMonthLastText
            End If


            If sFieldName = "SaleValue_Last" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Sale Value " & sMonthLastText
            End If

            If sFieldName = "RetailValue_Last" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Retail Value " & sMonthLastText
            End If

            If sFieldName = "CostValue_Last" Then
                itmX.CellFormat.FormatString = "0.00"
                itmX.Caption = "Cost Value " & sMonthLastText
            End If



            If sFieldName = "InvoiceDate" Then
                itmX.CellFormat.FormatString = "dd-MMM-yyyy"
            End If

        End If

    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged(ByVal source As Object, ByVal e As DevExpress.Web.TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged

        Dim i As Integer
        Dim nValues As Integer
        Dim nSwap As Integer

        lblErr1.Text = ""
        lblErr2.Text = ""
        lblErrDates.Text = ""
        lblTooManyParts.Text = ""

        nValues = 0
        For i = 0 To chkValues.Items.Count - 1
            If chkValues.Items(i).Selected Then
                nValues = nValues + 1
            End If
        Next

        Dim sFieldList As String = CollateFieldListDevX(chkDealer, chkCustomer, chkLocation, chkProduct, chkInvoice, chkUserDefined)

        If Session("SelectFieldList") <> sFieldList Then
            Session("SelectFieldList") = sFieldList
            sGridLayout = ""
        End If

        Session("ProductGroupList") = CollateSelectionStringsDevX(chkProductGroups)
        Session("BusinessTypeList") = CollateSelectionStringsDevX(chkBusinessTypes)
        Session("TradeAnalysis") = CollateSelectionStringsDevX(chkTradeAnalysis)

        Session("PFCList") = CollateListBoxValues(lstPFCs)
        Session("PartList") = CollateListBoxValues(lstParts)
        Session("MonthFrom") = Val(ddlFrom.Value)
        Session("MonthTo") = Val(ddlTo.Value)

        If Val(Session("MonthFrom")) > Val(Session("MonthTo")) Then
            nSwap = Val(Session("MonthTo"))
            Session("MonthTo") = Val(Session("MonthFrom"))
            Session("MonthFrom") = nSwap
        End If

        If ASPxPageControl1.ActiveTabIndex = 5 Then

            If InStr(Session("SelectFieldList"), "1") > 0 Then

                If nValues > 0 Then

                    If lstParts.Items.Count > 50 Then
                        lblTooManyParts.Text = "Too many parts specified (max 50). Please remove some parts."
                        ASPxPageControl1.ActiveTabIndex = 4
                    Else
                        Call GetResultsBound()
                        If sGridLayout <> "" Then
                            ASPXPivotResults.LoadLayoutFromString(sGridLayout)
                        End If
                    End If

                Else

                    lblErr2.Text = "You must enter some value fields before you can view results."
                    ASPxPageControl1.ActiveTabIndex = 0

                End If

            Else
                lblErr1.Text = "You must enter some data fields before you can view results."
                ASPxPageControl1.ActiveTabIndex = 0
            End If
        Else

            If ASPXPivotResults.Fields.Count > 0 Then
                sGridLayout = ASPXPivotResults.SaveLayoutToString()
            End If

        End If

    End Sub

    Protected Sub ASPXPivotResults_CustomCellDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxPivotGrid.PivotCellDisplayTextEventArgs) Handles ASPXPivotResults.CustomCellDisplayText
        Select Case e.DataField.FieldName
            Case "SaleValue", "CostValue", "RetailValue", "SaleValue_Last", "CostValue_Last", "RetailValue_Last"
                e.DisplayText = Format(e.Value, "£#,##0.00")
            Case "Quantity", "Quantity_Last"
                e.DisplayText = Format(e.Value, "#,##0")
            Case "Margin", "Margin_Last"
                e.DisplayText = Format(e.Value, "0.00") & IIf(e.Value <> 0, "%", "")
            Case Else
        End Select
    End Sub

    Protected Sub btnAddPart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPart.Click

        Dim nResult As Integer

        lblPartErr.Text = ""
        lblTooManyParts.Text = ""
        nResult = AddPart(Trim(txtPart.Text))
        If nResult = 1 Then
            lblPartErr.Text = "Invalid Part Number entered."
        ElseIf nResult = 2 Then
            lblPartErr.Text = "Part already in list."
        Else
            txtPart.Text = ""
            Call RemoveDuplicateListEntries(lstSrchParts, lstParts, 3)
        End If

    End Sub

    Protected Sub btnRemovePart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemovePart.Click

        Dim i As Integer
        Dim sPartList As List(Of String) = New List(Of String)()
        Dim sPart As String = ""

        For i = 0 To lstParts.Items.Count - 1
            If lstParts.Items(i).Selected = False Then
                sPartList.Add(lstParts.Items(i).Text)
            End If
        Next
        lstParts.Items.Clear()
        For i = 0 To sPartList.Count - 1
            lstParts.Items.Add(sPartList.Item(i).ToString)
        Next

        lblTooManyParts.Text = ""
        lblPartCount.Text = "Count : " & Trim(Str(lstParts.Items.Count))
        If lstParts.Items.Count > 50 Then
            lblTooManyParts.Text = "Too many parts specificed (max 50). Please remove some parts."
            ASPxPageControl1.ActiveTabIndex = 3
        End If

    End Sub

    Protected Sub btnSaveDef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveDef.Click
        Call SaveDefVars(False)
    End Sub

    Protected Sub btnQSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQSave.Click
        If txtDefName.Text = "" Then
            lblQSave.Text = "Enter a query name"
        Else
            If SaveQuery() Then
                Call SaveDefVars(True)
                lblQSave.Text = "Query saved."
            End If
        End If
    End Sub

    Protected Sub btnQSave_Cancel(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQCancel.Click
        Call SaveDefVars(True)
    End Sub

    Private Sub SaveDefVars(ByVal bBool As Boolean)
        btnSaveDef.Visible = bBool
        lblQName.Visible = Not bBool
        txtDefName.Visible = Not bBool
        ddlDefType.Visible = Not bBool
        btnQSave.Visible = Not bBool
        btnQCancel.Visible = Not bBool
    End Sub

    Private Function SaveQuery() As Boolean

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim nDateCompare As Integer = 0
        If chkDateComparison.Checked = True Then nDateCompare = 1

        sGridLayout = ASPXPivotResults.SaveLayoutToString()

        htIn.Add("@nUserId", Session("UserId"))
        htIn.Add("@sQueryName", Trim(txtDefName.Text))
        htIn.Add("@sFieldList", Session("SelectFieldList"))
        htIn.Add("@sProductCategories", Session("TradeAnalysis"))
        htIn.Add("@sProductGroups", Session("ProductGroupList"))
        htIn.Add("@sBusinessTypes", Session("BusinessTypeList"))
        htIn.Add("@nTradeClubType", Session("BlueGrassID"))
        htIn.Add("@sPFCs", Session("PFCList"))
        htIn.Add("@sParts", Session("PartList"))
        htIn.Add("@nPeriodFrom", Session("MonthFrom"))
        htIn.Add("@nPeriodTo", Session("MonthTo"))
        htIn.Add("@nPubPrivate", ddlDefType.SelectedIndex)
        htIn.Add("@nField1", IIf(chkValues.Items(0).Selected, 1, 0))
        htIn.Add("@nField2", IIf(chkValues.Items(1).Selected, 1, 0))
        htIn.Add("@nField3", IIf(chkValues.Items(2).Selected, 1, 0))
        htIn.Add("@nField4", IIf(chkValues.Items(3).Selected, 1, 0))
        htIn.Add("@nField5", IIf(chkValues.Items(4).Selected, 1, 0))
        htIn.Add("@sLayout", sGridLayout)
        htIn.Add("@nCompare", nDateCompare)

        nRes = da.Update(sErr, "p_AdvQuery_Ins", htIn)
        If sErr <> "" Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        Else
            SaveQuery = True
            Call GetQueryDefnDevEx(ddlQueries, Session("UserID"))
            'ddlQueries.Items.Add("Please select...")
            ddlQueries.SelectedIndex = -1
        End If

    End Function

    Protected Sub btnLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoad.Click

        Dim sSQL As String

        If ddlQueries.Value > 0 Then

            sSQL = "SELECT * FROM Toyota_AdvQueries WHERE Id = " & ddlQueries.Value
            ds = da.ExecuteSQL(sErr, sSQL)

            If ds.Tables(0).Rows.Count = 1 Then

                Call ClearDefn()

                With ds.Tables(0).Rows(0)

                    Call LoadFieldListDevX(.Item("FieldList").ToString)
                    Call LoadQueryPopulateCheckBoxValuesDevX(chkProductGroups, .Item("ProductGroups").ToString)
                    Call LoadQueryPopulateCheckBoxValuesDevX(chkBusinessTypes, .Item("BusinessTypes").ToString)
                    Call LoadQueryPopulateCheckBoxValuesDevX(chkTradeAnalysis, .Item("segments").ToString)
                    Call LoadQueryPopulatePFCList(.Item("PFCs"))
                    Call LoadQueryPopulatePartList(.Item("Parts"))

                    If ds.Tables(0).Rows(0).Item("compare") = 1 Then
                        chkDateComparison.Checked = True
                    Else
                        chkDateComparison.Checked = False
                    End If

                    ' Call LoadTradeAnalysisFields(.Item("segments"))   ' not used at present

                    memTAItems.Text = Replace(Replace(Session("TradeAnalysisList"), "'", ""), ",", vbCrLf)

                    ddlType.SelectedIndex = .Item("TradeClubType")

                    chkValues.Items(0).Selected = (.Item("Field1") = 1)
                    chkValues.Items(1).Selected = (.Item("Field2") = 1)
                    chkValues.Items(2).Selected = (.Item("Field3") = 1)
                    chkValues.Items(3).Selected = (.Item("Field4") = 1)
                    chkValues.Items(4).Selected = (.Item("Field5") = 1)

                    For i = 0 To ddlFrom.Items.Count - 1
                        If ddlFrom.Items(i).Value = .Item("PeriodFrom") Then
                            ddlFrom.SelectedIndex = i
                        End If
                    Next

                    For i = 0 To ddlTo.Items.Count - 1
                        If ddlTo.Items(i).Value = .Item("PeriodTo") Then
                            ddlTo.SelectedIndex = i
                        End If
                    Next

                    sGridLayout = .Item("Layout")

                    Session("SelectFieldList") = CollateFieldListDevX(chkDealer, chkCustomer, chkLocation, chkProduct, chkInvoice, chkUserDefined)
                    Session("MonthFrom") = ddlFrom.Value
                    Session("MonthTo") = ddlTo.Value

                End With

            End If

        Else

            Dim strMessage As String
            strMessage = "Select a definition to load from the drop down list."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        End If

    End Sub

    Private Function AddPFC(ByVal sPFC As String) As Integer

        Dim i As Integer
        Dim bFound As Boolean
        Dim sName As String

        AddPFC = 0
        bFound = False
        For i = 0 To lstPFCs.Items.Count - 1
            bFound = bFound Or Left(lstPFCs.Items(i).Text, 3) = sPFC
        Next
        If Not bFound Then
            sName = GetPFCDescription(sPFC)
            If sName <> "" Then
                lstPFCs.Items.Add(sPFC & " : " & sName)
            Else
                AddPFC = 1
            End If
        Else
            AddPFC = 2
        End If

    End Function

    Private Function AddPart(ByVal sPart As String) As Integer

        Dim i As Integer
        Dim bFound As Boolean
        Dim sName As String
        Dim sPartNumber As String
        Dim nLen As Integer

        nLen = Len(sPart)
        AddPart = 0
        bFound = False
        For i = 0 To lstParts.Items.Count - 1
            bFound = bFound Or Left(lstParts.Items(i).Text, nLen) = sPart
        Next
        If Not bFound Then
            sName = GetPartDescription(sPart)
            If sName <> "" Then
                sPartNumber = Left(Trim(sPart) & Space(20), 20)
                lstParts.Items.Add(sPart & " : " & sName)
            Else
                AddPart = 1
            End If
        Else
            AddPart = 2
        End If

        lblTooManyParts.Text = ""
        lblPartCount.Text = "Count : " & Trim(Str(lstParts.Items.Count))
        If lstParts.Items.Count > 50 Then
            lblTooManyParts.Text = "Too many parts specificed (max 50). Please remove some parts."
            ASPxPageControl1.ActiveTabIndex = 3
        End If

    End Function

    Protected Sub btnSrchPFC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSrchPFC.Click
        lblPFCErr.Text = ""
        Call PopulatePFCSearchList()
    End Sub

    Private Sub LoadQueryPopulatePFCList(ByVal sStr As String)

        Dim daPFC As New DatabaseAccess
        Dim dsPFC As DataSet
        Dim sErrPFC As String = ""
        Dim sSQL As String = ""

        lstPFCs.Items.Clear()
        If sStr <> "''" Then
            sSQL = "SELECT PFC, Description FROM PartPFC WHERE PFC IN (" & sStr & ")"
            dsPFC = daPFC.ExecuteSQL(sErrPFC, sSQL)
            If dsPFC.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsPFC.Tables(0).Rows.Count - 1
                    lstPFCs.Items.Add(dsPFC.Tables(0).Rows(i).Item("PFC") & " : " & dsPFC.Tables(0).Rows(i).Item("Description"))
                Next
            End If
        End If

    End Sub

    Private Sub LoadQueryPopulatePartList(ByVal sStr As String)

        Dim daPart As New DatabaseAccess
        Dim dsPart As DataSet
        Dim sErrPart As String = ""
        Dim sSQL As String = ""
        Dim sPartNumber As String

        lstParts.Items.Clear()
        If sStr <> "''" Then
            sSQL = "SELECT PartNumber, Description FROM Part WHERE PartNumber IN (" & sStr & ")"
            dsPart = daPart.ExecuteSQL(sErrPart, sSQL)
            If dsPart.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsPart.Tables(0).Rows.Count - 1
                    sPartNumber = Left(dsPart.Tables(0).Rows(i).Item("PartNumber") & Space(20), 20)
                    lstParts.Items.Add(sPartNumber & " : " & dsPart.Tables(0).Rows(i).Item("Description"))
                Next
            End If
        End If

        lblTooManyParts.Text = ""
        lblPartCount.Text = "Count : " & Trim(Str(lstParts.Items.Count))
        If lstParts.Items.Count > 50 Then
            lblTooManyParts.Text = "Too many parts specificed (max 50). Please remove some parts."
            ASPxPageControl1.ActiveTabIndex = 3
        End If

    End Sub

    Private Sub LoadQueryPopulateCheckBoxValuesDevX(ByRef ddl As DevExpress.Web.ASPxCheckBoxList, ByVal sStr As String)

        Dim i As Integer
        For i = 0 To ddl.Items.Count - 1
            ddl.Items(i).Selected = (InStr(sStr, ddl.Items(i).Text) > 0)
        Next

    End Sub

    'Sub LoadTradeAnalysisFields(sAnalysisList As String)

    '    Dim nMax As Integer = Len(sAnalysisList)
    '    Dim nPos As Integer = 1
    '    Dim sThisString As String = ""
    '    Dim sThisWord As String = ""
    '    Dim bQuotesOpen As Boolean = False

    '    While nPos <= nMax
    '        sThisString = Mid(sAnalysisList, nPos, 1)
    '        If sThisString <> "," Then
    '            If sThisString = "'" Then
    '                If bQuotesOpen = True Then
    '                    bQuotesOpen = False
    '                    Call SetTradeAnalysisField(sThisWord)
    '                    sThisWord = ""
    '                Else
    '                    bQuotesOpen = True
    '                End If
    '            Else
    '                sThisWord = sThisWord & sThisString
    '            End If
    '        End If
    '        nPos = nPos + 1
    '    End While
    'End Sub
    Sub SetTradeAnalysisField(sAnalysisText As String)

        Select Case sAnalysisText
            Case "Air Filters"
                chkServiceList.Items(0).Selected = True
            Case "Engine Oil"
                chkServiceList.Items(1).Selected = True
            Case "Fuel Filters"
                chkServiceList.Items(2).Selected = True
            Case "Genuine Coolant"
                chkServiceList.Items(3).Selected = True
            Case "Oil Filters"
                chkServiceList.Items(4).Selected = True
            Case "Pollen Filters"
                chkServiceList.Items(5).Selected = True
            Case "Spark Plugs"
                chkServiceList.Items(6).Selected = True
            Case "Wiper Blades"
                chkServiceList.Items(7).Selected = True

            Case "Ball Joints"
                chkSteeringList.Items(0).Selected = True
            Case "Coil & Leaf Springs"
                chkSteeringList.Items(1).Selected = True
            Case "Driveshafts & CV Joints"
                chkSteeringList.Items(2).Selected = True
            Case "Shock Absorbers"
                chkSteeringList.Items(3).Selected = True
            Case "Steering Racks"
                chkSteeringList.Items(4).Selected = True
            Case "Suspension Arms"
                chkSteeringList.Items(5).Selected = True
            Case "Wheel Bearings"
                chkSteeringList.Items(6).Selected = True

            Case "Brake Calipers"
                chkBrakeList.Items(0).Selected = True
            Case "Brake Discs"
                chkBrakeList.Items(1).Selected = True
            Case "Brake Others"
                chkBrakeList.Items(2).Selected = True
            Case "Brake Pads"
                chkBrakeList.Items(3).Selected = True
            Case "Brake Shoes"
                chkBrakeList.Items(4).Selected = True
            Case "Wheel Cylinders"
                chkBrakeList.Items(5).Selected = True


            Case "Clutch/ Drivetrain"
                chkClutchList.Items(0).Selected = True
            Case "Clutch Covers"
                chkClutchList.Items(1).Selected = True
            Case "Clutch Discs"
                chkClutchList.Items(2).Selected = True
            Case "Clutch Kits"
                chkClutchList.Items(3).Selected = True
            Case "Clutch Release Bearings"
                chkClutchList.Items(4).Selected = True
            Case "Flywheels"
                chkClutchList.Items(5).Selected = True
            Case "Timing Belts"
                chkClutchList.Items(6).Selected = True

            Case "Catalysts"
                chkExhaustList.Items(0).Selected = True
            Case "Centre Pipes"
                chkExhaustList.Items(1).Selected = True
            Case "Front Pipes"
                chkExhaustList.Items(2).Selected = True
            Case "Rear Silencers"
                chkExhaustList.Items(3).Selected = True

            Case "Alternators"
                chkElectricalList.Items(0).Selected = True
            Case "Batteries"
                chkElectricalList.Items(1).Selected = True
            Case "Oxygen Sensors"
                chkElectricalList.Items(2).Selected = True
            Case "Starter Motors"
                chkElectricalList.Items(3).Selected = True

            Case "Air Con Parts"
                chkHeatingList.Items(0).Selected = True
            Case "Radiators"
                chkHeatingList.Items(1).Selected = True
            Case "Water Pumps"
                chkHeatingList.Items(2).Selected = True

            Case "Consumables"
                chkConsumables.Checked = True

            Case "Bonnets"
                chkMainPanelsList.Items(0).Selected = True
            Case "Cross Members"
                chkMainPanelsList.Items(1).Selected = True
            Case "Door Skins"
                chkMainPanelsList.Items(2).Selected = True
            Case "Doors"
                chkMainPanelsList.Items(3).Selected = True
            Case "Quarter Panels"
                chkMainPanelsList.Items(4).Selected = True
            Case "Radiator Support Panels"
                chkMainPanelsList.Items(5).Selected = True
            Case "Tailgates & Trunk lids"
                chkMainPanelsList.Items(6).Selected = True
            Case "Wings"
                chkMainPanelsList.Items(7).Selected = True

            Case "Mirrors And Glass"
                chkMirrorsList.Items(0).Selected = True
            Case "Glass"
                chkMirrorsList.Items(1).Selected = True
            Case "Mirrors"
                chkMirrorsList.Items(2).Selected = True
            Case "Windscreens"
                chkMirrorsList.Items(3).Selected = True

            Case "Bumpers And Fixings"
                chkBumpersList.Items(0).Selected = True
            Case "Bumper Parts"
                chkBumpersList.Items(1).Selected = True
            Case "Front Bumpers"
                chkBumpersList.Items(2).Selected = True
            Case "Rear Bumpers"
                chkBumpersList.Items(3).Selected = True

            Case "Lighting"
                chkLightingList.Items(0).Selected = True
            Case "Foglamps"
                chkLightingList.Items(1).Selected = True
            Case "Headlamps"
                chkLightingList.Items(2).Selected = True
            Case "Minor Lamps"
                chkLightingList.Items(3).Selected = True
            Case "Rear Lamps"
                chkLightingList.Items(4).Selected = True

            Case "Grilles And Mouldings"
                chkGrillesList.Items(0).Selected = True
            Case "Mouldings & Badges"
                chkGrillesList.Items(1).Selected = True
            Case "Radiator Grilles"
                chkGrillesList.Items(2).Selected = True

            Case "Accessories"
                chkOtherList.Items(0).Selected = True
            Case "Air Bag Parts"
                chkOtherList.Items(1).Selected = True
            Case "Interior Parts"
                chkOtherList.Items(2).Selected = True
            Case "Misc Body Parts"
                chkOtherList.Items(3).Selected = True
            Case "Misc Fixings"
                chkOtherList.Items(4).Selected = True
            Case "Misc Panels"
                chkOtherList.Items(5).Selected = True
            Case "Wheels"
                chkOtherList.Items(6).Selected = True
        End Select

        If chkServiceList.Items(0).Selected = True _
            And chkServiceList.Items(1).Selected = True _
            And chkServiceList.Items(2).Selected = True _
            And chkServiceList.Items(3).Selected = True _
            And chkServiceList.Items(4).Selected = True _
            And chkServiceList.Items(5).Selected = True _
            And chkServiceList.Items(6).Selected = True _
            And chkServiceList.Items(7).Selected = True Then
            chkService.Checked = vbTrue
        Else
            chkService.Checked = vbFalse
        End If

        If chkSteeringList.Items(0).Selected = True _
            And chkSteeringList.Items(1).Selected = True _
            And chkSteeringList.Items(2).Selected = True _
            And chkSteeringList.Items(3).Selected = True _
            And chkSteeringList.Items(4).Selected = True _
            And chkSteeringList.Items(5).Selected = True _
            And chkSteeringList.Items(6).Selected = True Then
            chkSteering.Checked = vbTrue
        Else
            chkSteering.Checked = vbFalse
        End If

        If chkBrakeList.Items(0).Selected = True _
          And chkBrakeList.Items(1).Selected = True _
          And chkBrakeList.Items(2).Selected = True _
          And chkBrakeList.Items(3).Selected = True _
          And chkBrakeList.Items(4).Selected = True _
          And chkBrakeList.Items(5).Selected = True Then
            chkBrake.Checked = vbTrue
        Else
            chkBrake.Checked = vbFalse
        End If

        If chkClutchList.Items(0).Selected = True _
          And chkClutchList.Items(1).Selected = True _
          And chkClutchList.Items(2).Selected = True _
          And chkClutchList.Items(3).Selected = True _
          And chkClutchList.Items(4).Selected = True _
          And chkClutchList.Items(5).Selected = True Then
            chkClutch.Checked = vbTrue
        Else
            chkClutch.Checked = vbFalse
        End If

        If chkExhaustList.Items(0).Selected = True _
          And chkExhaustList.Items(1).Selected = True _
          And chkExhaustList.Items(2).Selected = True _
          And chkExhaustList.Items(3).Selected = True Then
            chkExhaust.Checked = vbTrue
        Else
            chkExhaust.Checked = vbFalse
        End If

        If chkElectricalList.Items(0).Selected = True _
            And chkElectricalList.Items(1).Selected = True _
            And chkElectricalList.Items(2).Selected = True _
            And chkElectricalList.Items(3).Selected = True Then
            chkElectrical.Checked = vbTrue
        Else
            chkElectrical.Checked = vbFalse
        End If

        If chkHeatingList.Items(0).Selected = True _
            And chkHeatingList.Items(1).Selected = True _
            And chkHeatingList.Items(2).Selected = True Then
            chkHeating.Checked = vbTrue
        Else
            chkHeating.Checked = vbFalse
        End If

        If chkMainPanelsList.Items(0).Selected = True _
            And chkMainPanelsList.Items(1).Selected = True _
            And chkMainPanelsList.Items(2).Selected = True _
            And chkMainPanelsList.Items(3).Selected = True _
            And chkMainPanelsList.Items(4).Selected = True _
            And chkMainPanelsList.Items(5).Selected = True _
            And chkMainPanelsList.Items(6).Selected = True _
            And chkMainPanelsList.Items(7).Selected = True Then
            chkMainPanels.Checked = vbTrue
        Else
            chkMainPanels.Checked = vbFalse
        End If

        If chkMirrorsList.Items(0).Selected = True _
            And chkMirrorsList.Items(1).Selected = True _
            And chkMirrorsList.Items(2).Selected = True Then
            chkMirrors.Checked = vbTrue
        Else
            chkMirrors.Checked = vbFalse
        End If

        If chkBumpersList.Items(0).Selected = True _
            And chkBumpersList.Items(1).Selected = True _
            And chkBumpersList.Items(2).Selected = True Then
            chkBumpers.Checked = vbTrue
        Else
            chkBumpers.Checked = vbFalse
        End If

        If chkLightingList.Items(0).Selected = True _
          And chkLightingList.Items(1).Selected = True _
          And chkLightingList.Items(2).Selected = True _
          And chkLightingList.Items(3).Selected = True Then
            chkLighting.Checked = vbTrue
        Else
            chkLighting.Checked = vbFalse
        End If

        If chkGrillesList.Items(0).Selected = True _
           And chkGrillesList.Items(1).Selected = True Then
            chkGrilles.Checked = vbTrue
        Else
            chkGrilles.Checked = vbFalse
        End If

        If chkOtherList.Items(0).Selected = True _
          And chkOtherList.Items(1).Selected = True _
          And chkOtherList.Items(2).Selected = True _
          And chkOtherList.Items(3).Selected = True _
          And chkOtherList.Items(4).Selected = True _
          And chkOtherList.Items(5).Selected = True _
          And chkOtherList.Items(6).Selected = True Then
            chkOther.Checked = vbTrue
        Else
            chkOther.Checked = vbFalse
        End If

    End Sub

    Private Sub LoadQueryPopulateFieldList(ByVal sStr As String)

        Dim i As Integer

        For i = 0 To chkDealer.Items.Count - 1
            chkDealer.Items(i).Selected = Mid(sStr, i + 1, 1) = "1"
        Next

    End Sub

    Protected Sub btnAddPFCMulti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPFCMulti.Click

        Dim i, j As Integer
        Dim sPFC As String = ""

        lblPFCErr.Text = ""
        For i = 0 To lstSrchPFCs.Items.Count - 1
            If lstSrchPFCs.Items(i).Selected Then
                sPFC = Left(lstSrchPFCs.Items(i).Text, 3)
                j = AddPFC(sPFC)
            End If
        Next
        Call RemoveDuplicateListEntries(lstSrchPFCs, lstPFCs, 3)

    End Sub

    Protected Sub btnAddPartMulti_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPartMulti.Click

        Dim i, j As Integer
        Dim sPart As String = ""

        lblPartErr.Text = ""
        lblTooManyParts.Text = ""
        For i = 0 To lstSrchParts.Items.Count - 1
            If lstSrchParts.Items(i).Selected Then
                sPart = Left(lstSrchParts.Items(i).Text, 20)
                j = AddPart(sPart)
            End If
        Next
        Call RemoveDuplicateListEntries(lstSrchParts, lstParts, 20)

    End Sub

    Protected Sub btnRemovePFC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemovePFC.Click

        Dim i As Integer
        Dim sPFCList As List(Of String) = New List(Of String)()
        Dim sPFC As String = ""

        For i = 0 To lstPFCs.Items.Count - 1
            If lstPFCs.Items(i).Selected = False Then
                sPFCList.Add(lstPFCs.Items(i).Text)
            End If
        Next
        lstPFCs.Items.Clear()
        For i = 0 To sPFCList.Count - 1
            lstPFCs.Items.Add(sPFCList.Item(i).ToString)
        Next

    End Sub

    Private Sub RemoveDuplicateListEntries(ByVal lst1 As ListBox, ByVal lst2 As ListBox, ByVal nLeft As Integer)

        Dim i, j As Integer
        Dim sList As List(Of String) = New List(Of String)()
        Dim sLine As String = ""
        Dim bFound As Boolean

        For i = 0 To lst1.Items.Count - 1
            sList.Add(lst1.Items(i).Text)
        Next
        lst1.Items.Clear()
        For i = 0 To sList.Count - 1
            bFound = False
            For j = 0 To lst2.Items.Count - 1
                bFound = bFound Or Left(lst2.Items(j).Text, nLeft) = Left(sList.Item(i).ToString, nLeft)
            Next
            lst1.Items.Add(sList.Item(i).ToString)
        Next

    End Sub

    Private Sub PopulatePFCSearchList()

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable
        Dim i As Integer
        Dim sSrchString As String

        If txtPFC.Text = "" Then
            sSrchString = "''"
        Else
            sSrchString = Trim(txtPFC.Text)
        End If

        lstSrchPFCs.Items.Clear()
        htin.Add("@sSegments", Session("ProductCategoryList"))
        htin.Add("@sProductGroups", Session("ProductGroupList"))
        htin.Add("@sPFCSrch", sSrchString)
        ds = da.Read(sErrorMessage, "p_PFCs", htin)

        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                lstSrchPFCs.Items.Add(.Rows(i).Item("PFC") + " : " + .Rows(i).Item("Description"))
            Next
        End With

        If lstSrchPFCs.Items.Count = 0 Then
            lstSrchPFCs.Items.Add("< No PFCs found >")
            lstSrchPFCs.Enabled = False
        Else
            lstSrchPFCs.Enabled = True
        End If

        Call RemoveDuplicateListEntries(lstSrchPFCs, lstPFCs, 3)

    End Sub

    Private Sub PopulatePartSearchList()

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable
        Dim i As Integer
        Dim sSrchString As String
        Dim sPartNumber As String

        If txtPart.Text = "" Then
            sSrchString = "''"
        Else
            sSrchString = Trim(txtPart.Text)
        End If

        lstSrchParts.Items.Clear()
        htin.Add("@sSegments", Session("ProductCategoryList"))
        htin.Add("@sProductGroups", Session("ProductGroupList"))
        htin.Add("@sPFCs", Session("PFCList"))
        htin.Add("@sPart", sSrchString)
        ds = da.Read(sErrorMessage, "p_Parts", htin)

        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                sPartNumber = Left(Trim(.Rows(i).Item("PartNumber")) + Space(20), 20)
                lstSrchParts.Items.Add(sPartNumber + " : " + .Rows(i).Item("PartDescription"))
            Next
        End With

        If lstSrchParts.Items.Count = 0 Then
            lstSrchParts.Items.Add("< No Parts found >")
            lstSrchParts.Enabled = False
        Else
            lstSrchParts.Enabled = True
        End If

        Call RemoveDuplicateListEntries(lstSrchParts, lstParts, 20)

    End Sub

    Protected Sub btnSrchPart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSrchPart.Click
        lblPartErr.Text = ""
        lblTooManyParts.Text = ""
        Call PopulatePartSearchList()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Call ClearDefn()
    End Sub

    Protected Sub btnAddPFC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPFC.Click

        Dim nResult As Integer

        lblPFCErr.Text = ""
        nResult = AddPFC(Trim(txtPFC.Text))
        If nResult = 1 Then
            lblPFCErr.Text = "Invalid Product Group code entered."
        ElseIf nResult = 2 Then
            lblPFCErr.Text = "Product Group already in list."
        Else
            txtPFC.Text = ""
            Call RemoveDuplicateListEntries(lstSrchPFCs, lstPFCs, 3)
        End If

    End Sub

    Protected Sub chkValues_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkValues.SelectedIndexChanged
        sGridLayout = ""
    End Sub

    Private Sub ClearDefn()

        ASPxPageControl1.ActiveTabIndex = 0

        Call LoadQueryPopulateFieldList("000000000000")
        'Call LoadQueryPopulateCheckBoxValuesdevx(chkSegments, "")
        Call LoadQueryPopulateCheckBoxValuesDevX(chkProductGroups, "")
        Call LoadQueryPopulateCheckBoxValuesDevX(chkBusinessTypes, "")

        memTAItems.Text = ""

        lstParts.Items.Clear()
        lstPFCs.Items.Clear()
        lstSrchParts.Items.Clear()
        lstSrchPFCs.Items.Clear()
        ddlType.SelectedIndex = 0

        chkDealer.Items(0).Selected = False
        chkDealer.Items(1).Selected = False
        chkDealer.Items(2).Selected = False
        chkDealer.Items(3).Selected = False

        chkCustomer.Items(0).Selected = False
        chkCustomer.Items(1).Selected = False
        chkCustomer.Items(2).Selected = False
        chkCustomer.Items(3).Selected = False
        chkCustomer.Items(4).Selected = False
        chkCustomer.Items(5).Selected = False
        chkCustomer.Items(6).Selected = False
        chkCustomer.Items(7).Selected = False
        chkCustomer.Items(8).Selected = False

        chkLocation.Items(0).Selected = False
        chkLocation.Items(1).Selected = False
        chkLocation.Items(2).Selected = False
        chkLocation.Items(3).Selected = False

        chkProduct.Items(0).Selected = False
        chkProduct.Items(1).Selected = False
        chkProduct.Items(2).Selected = False
        chkProduct.Items(3).Selected = False

        chkInvoice.Items(0).Selected = False
        chkInvoice.Items(1).Selected = False
        chkInvoice.Items(2).Selected = False

        chkValues.Items(0).Selected = False
        chkValues.Items(1).Selected = False
        chkValues.Items(2).Selected = False
        chkValues.Items(3).Selected = False
        chkValues.Items(4).Selected = False

        chkTradeAnalysis.Items(0).Selected = False
        chkTradeAnalysis.Items(1).Selected = False
        chkTradeAnalysis.Items(2).Selected = False
        chkTradeAnalysis.Items(3).Selected = False
        chkTradeAnalysis.Items(4).Selected = False
        chkTradeAnalysis.Items(5).Selected = False
        chkTradeAnalysis.Items(6).Selected = False
        chkTradeAnalysis.Items(7).Selected = False
        chkTradeAnalysis.Items(8).Selected = False
        chkTradeAnalysis.Items(9).Selected = False
        chkTradeAnalysis.Items(10).Selected = False
        chkTradeAnalysis.Items(11).Selected = False
        chkTradeAnalysis.Items(12).Selected = False
        chkTradeAnalysis.Items(13).Selected = False

        chkService.Checked = False
        chkSteering.Checked = False

        ddlFrom.SelectedIndex = 0
        ddlTo.SelectedIndex = 0

        chkDateComparison.Checked = False

        sGridLayout = ""

        lblTooManyParts.Text = ""
        lblPartCount.Text = "Count:0"

        chkService.Checked = False
        chkSteering.Checked = False
        chkBrake.Checked = False
        chkClutch.Checked = False
        chkExhaust.Checked = False
        chkElectrical.Checked = False
        chkHeating.Checked = False
        chkConsumables.Checked = False

        chkServiceList.Items(0).Selected = False
        chkServiceList.Items(1).Selected = False
        chkServiceList.Items(2).Selected = False
        chkServiceList.Items(3).Selected = False
        chkServiceList.Items(4).Selected = False
        chkServiceList.Items(5).Selected = False
        chkServiceList.Items(6).Selected = False
        chkServiceList.Items(7).Selected = False

        chkSteeringList.Items(0).Selected = False
        chkSteeringList.Items(1).Selected = False
        chkSteeringList.Items(2).Selected = False
        chkSteeringList.Items(3).Selected = False
        chkSteeringList.Items(4).Selected = False
        chkSteeringList.Items(5).Selected = False
        chkSteeringList.Items(6).Selected = False

        chkBrakeList.Items(0).Selected = False
        chkBrakeList.Items(1).Selected = False
        chkBrakeList.Items(2).Selected = False
        chkBrakeList.Items(3).Selected = False
        chkBrakeList.Items(4).Selected = False
        chkBrakeList.Items(5).Selected = False

        chkClutchList.Items(0).Selected = False
        chkClutchList.Items(1).Selected = False
        chkClutchList.Items(2).Selected = False
        chkClutchList.Items(3).Selected = False
        chkClutchList.Items(4).Selected = False
        chkClutchList.Items(5).Selected = False

        chkExhaustList.Items(0).Selected = False
        chkExhaustList.Items(1).Selected = False
        chkExhaustList.Items(2).Selected = False
        chkExhaustList.Items(3).Selected = False

        chkElectricalList.Items(0).Selected = False
        chkElectricalList.Items(1).Selected = False
        chkElectricalList.Items(2).Selected = False
        chkElectricalList.Items(3).Selected = False

        chkHeatingList.Items(0).Selected = False
        chkHeatingList.Items(1).Selected = False
        chkHeatingList.Items(2).Selected = False

        chkMainPanelsList.Items(0).Selected = False
        chkMainPanelsList.Items(1).Selected = False
        chkMainPanelsList.Items(2).Selected = False
        chkMainPanelsList.Items(3).Selected = False
        chkMainPanelsList.Items(4).Selected = False
        chkMainPanelsList.Items(5).Selected = False
        chkMainPanelsList.Items(6).Selected = False
        chkMainPanelsList.Items(7).Selected = False

        chkMirrorsList.Items(0).Selected = False
        chkMirrorsList.Items(1).Selected = False
        chkMirrorsList.Items(2).Selected = False

        chkBumpersList.Items(0).Selected = False
        chkBumpersList.Items(1).Selected = False
        chkBumpersList.Items(2).Selected = False

        chkLightingList.Items(0).Selected = False
        chkLightingList.Items(1).Selected = False
        chkLightingList.Items(2).Selected = False
        chkLightingList.Items(3).Selected = False

        chkGrillesList.Items(0).Selected = False
        chkGrillesList.Items(1).Selected = False
        chkOtherList.Items(0).Selected = False

        chkOtherList.Items(1).Selected = False
        chkOtherList.Items(2).Selected = False
        chkOtherList.Items(3).Selected = False
        chkOtherList.Items(4).Selected = False
        chkOtherList.Items(5).Selected = False
        chkOtherList.Items(6).Selected = False
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        ddlQueries.Enabled = False
        btnLoad.Visible = False
        btnClear.Visible = False
        btnDelete.Visible = False
        btnDelOK.Visible = True
        btnDelCancel.Visible = True
    End Sub

    Protected Sub btnDelCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelCancel.Click
        ddlQueries.Enabled = True
        btnLoad.Visible = True
        btnClear.Visible = True
        btnDelete.Visible = True
        btnDelOK.Visible = False
        btnDelCancel.Visible = False
    End Sub

    Protected Sub btnDelOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelOK.Click

        Dim sSQL As String

        If ddlQueries.SelectedIndex > 0 Then
            sSQL = "DELETE FROM Toyota_AdvQueries WHERE Id = " & ddlQueries.Value
            Call RunNonQueryCommand(sSQL)
            ddlQueries.Items.Clear()
            Call GetQueryDefnDevEx(ddlQueries, Session("UserID"))
            'ddlQueries.Items.Add("Please select...")
            ' ddlQueries.SelectedIndex = 0
        End If

        ddlQueries.Enabled = True
        btnLoad.Visible = True
        btnClear.Visible = True
        btnDelete.Visible = True
        btnDelOK.Visible = False
        btnDelCancel.Visible = False

    End Sub

    Protected Sub btnExportToExcel_Click()

        Dim nCols As Integer
        Dim nRows As Integer

        nCols = ASPXPivotResults.ColumnCount
        nRows = ASPXPivotResults.RowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            ASPxPivotGridExporter1.ExportXlsToResponse("Results", True)

        End If

    End Sub

    Protected Sub TreeListStyles(sender As Object, e As EventArgs)
        Call TreeList_Styles(sender)
    End Sub

    Protected Sub PivotGridStyles(sender As Object, e As EventArgs)
        Call PivotGrid_Styles(sender)
    End Sub

    'Protected Sub ttlTradeAnalysis_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tlTradeAnalysis.SelectionChanged
    '    Dim treeList As DevExpress.Web.ASPxTreeList.ASPxTreeList = CType(sender, DevExpress.Web.ASPxTreeList.ASPxTreeList)
    '    Dim iterator As DevExpress.Web.ASPxTreeList.TreeListNodeIterator = treeList.CreateNodeIterator()
    '    Dim sTradeAnalysis As String = ""
    '    Dim sTradeAnalysis2 As String = ""
    '    Session("ProductCategoryList") = ""
    '    Session("ProductCategoryList2") = ""
    '    Do While iterator.Current IsNot Nothing
    '        Dim node As TreeListNode = iterator.Current
    '        If node.Selected And node.Level = 3 Then
    '            If isEmpty(sTradeAnalysis) = False Then
    '                sTradeAnalysis = sTradeAnalysis & "," & "'" & Trim(node.DataItem(6).ToString) & "'"
    '                sTradeAnalysis2 = sTradeAnalysis2 & vbCrLf & Trim(node.DataItem(6).ToString)
    '            Else
    '                sTradeAnalysis = "'" & Trim(node.DataItem(6).ToString) & "'"
    '                sTradeAnalysis2 = Trim(node.DataItem(6).ToString)
    '            End If

    '        End If
    '        iterator.GetNext()
    '    Loop
    '    Session("ProductCategoryList") = sTradeAnalysis
    '    Session("ProductCategoryList2") = sTradeAnalysis2

    '    End Sub

    Protected Sub ASPxCallback1_Callback(ByVal source As Object, ByVal e As DevExpress.Web.CallbackEventArgs) Handles ASPxCallback1.Callback
        ASPxCallback1.JSProperties("cppostcodelist") = Session("ProductCategoryList2")
    End Sub

    'Private Sub tlTradeAnalysis_Init(sender As Object, e As EventArgs) Handles tlTradeAnalysis.Init
    '    tlTradeAnalysis.ExpandToLevel(2)
    'End Sub

    'Private Sub LoadTreeView()
    '    '   'Glass','Mirrors','Windscreens'
    '    'Loop Through the textstring
    '    Dim sTreeListString As String = Session("TradeAnalysisList")
    '    Dim nMax As Integer = Len(sTreeListString)
    '    Dim nPos As Integer = 1
    '    Dim sThisString As String = ""
    '    Dim sThisWord As String = ""
    '    Dim bQuotesOpen As Boolean = False
    '    Dim sThisNode As DevExpress.Web.ASPxTreeList.TreeListNode
    '    Dim nKey As Integer = 0

    '    'tlTradeAnalysis.ExpandAll()

    '    While nPos <= nMax
    '        sThisString = Mid(sTreeListString, nPos, 1)
    '        If sThisString = "'" Then
    '            If bQuotesOpen = True Then
    '                bQuotesOpen = False

    '                nKey = GetDataDecimal("SELECT id FROM NissanTradeAnalysisTree WHERE showline = '" & sThisWord & "'")
    '                ' Now Find This Word on the treelist and select that node
    '                sThisNode = tlTradeAnalysis.FindNodeByKeyValue(nKey)
    '                sThisNode.Selected = True

    '            Else
    '                bQuotesOpen = True
    '            End If
    '        Else
    '            sThisWord = sThisWord & sThisString
    '        End If
    '        nPos = nPos + 1






    '    End While
    'End Sub


    Protected Sub CheckedChanged(sender As Object, e As EventArgs) Handles _
            chkService.CheckedChanged,
            chkSteering.CheckedChanged,
            chkBrake.CheckedChanged,
            chkClutch.CheckedChanged,
            chkExhaust.CheckedChanged,
            chkElectrical.CheckedChanged,
            chkMirrors.CheckedChanged,
            chkConsumables.CheckedChanged,
            chkLighting.CheckedChanged,
            chkMirrors.CheckedChanged,
            chkBumpers.CheckedChanged,
            chkMainPanels.CheckedChanged,
            chkGrilles.CheckedChanged,
            chkOther.CheckedChanged

        If sender.id = "chkService" Then
            If chkService.Checked Then
                chkServiceList.SelectAll()
            Else
                chkServiceList.UnselectAll()
            End If
        End If

        If sender.id = "chkSteering" Then
            If chkSteering.Checked Then
                chkSteeringList.SelectAll()
            Else
                chkSteeringList.UnselectAll()
            End If
        End If

        If sender.id = "chkBrake" Then
            If chkBrake.Checked Then
                chkBrakeList.SelectAll()
            Else
                chkBrakeList.UnselectAll()
            End If
        End If

        If sender.id = "chkClutch" Then
            If chkClutch.Checked Then
                chkClutchList.SelectAll()
            Else
                chkClutchList.UnselectAll()
            End If
        End If

        If sender.id = "chkExhaust" Then
            If chkExhaust.Checked Then
                chkExhaustList.SelectAll()
            Else
                chkExhaustList.UnselectAll()
            End If
        End If

        If sender.id = "chkElectrical" Then
            If chkElectrical.Checked Then
                chkElectricalList.SelectAll()
            Else
                chkElectricalList.UnselectAll()
            End If
        End If

        If sender.id = "chkMirrors" Then
            If chkMirrors.Checked Then
                chkMirrorsList.SelectAll()
            Else
                chkMirrorsList.UnselectAll()
            End If
        End If


        ' Nothing needed for consumables

        If sender.id = "chkLighting" Then
            If chkLighting.Checked Then
                chkLightingList.SelectAll()
            Else
                chkLightingList.UnselectAll()
            End If
        End If

        If sender.id = "chkMirrors" Then
            If chkMirrors.Checked Then
                chkMirrorsList.SelectAll()
            Else
                chkMirrorsList.UnselectAll()
            End If
        End If

        If sender.id = "chkBumpers" Then
            If chkBumpers.Checked Then
                chkBumpersList.SelectAll()
            Else
                chkBumpersList.UnselectAll()
            End If
        End If

        If sender.id = "chkMainPanels" Then
            If chkMainPanels.Checked Then
                chkMainPanelsList.SelectAll()
            Else
                chkMainPanelsList.UnselectAll()
            End If
        End If

        If sender.id = "chkGrilles" Then
            If chkGrilles.Checked Then
                chkGrillesList.SelectAll()
            Else
                chkGrillesList.UnselectAll()
            End If
        End If

        If sender.id = "chkOther" Then
            If chkOther.Checked Then
                chkOtherList.SelectAll()
            Else
                chkOtherList.UnselectAll()
            End If
        End If


        Call UpdateTradeAnalysisSelection()

    End Sub
    Protected Sub chkList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _
            chkServiceList.SelectedIndexChanged,
            chkSteeringList.SelectedIndexChanged,
            chkBrakeList.SelectedIndexChanged,
            chkClutchList.SelectedIndexChanged,
            chkExhaustList.SelectedIndexChanged,
            chkElectricalList.SelectedIndexChanged,
            chkMirrorsList.SelectedIndexChanged,
            chkLightingList.SelectedIndexChanged,
            chkMirrorsList.SelectedIndexChanged,
            chkBumpersList.SelectedIndexChanged,
            chkMainPanelsList.SelectedIndexChanged,
            chkGrillesList.SelectedIndexChanged,
            chkOtherList.SelectedIndexChanged

        Call UpdateTradeAnalysisSelection()

    End Sub

    Sub UpdateTradeAnalysisSelection()

        memTAItems.Text = ""
        Session("TAList") = ""

        Dim nLoop As Integer = 0
        For nLoop = 0 To 7
            If chkServiceList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkServiceList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 6
            If chkSteeringList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkSteeringList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 5
            If chkBrakeList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkBrakeList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 5
            If chkClutchList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkClutchList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 3
            If chkExhaustList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkExhaustList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 3
            If chkElectricalList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkElectricalList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 2
            If chkMirrorsList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkMirrorsList.Items(nLoop).Text & vbCrLf
            End If
        Next

        If chkConsumables.Checked Then
            memTAItems.Text = memTAItems.Text & "Consumables" & vbCrLf
        End If

        nLoop = 0
        For nLoop = 0 To 3
            If chkLightingList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkLightingList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 2
            If chkMirrorsList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkMirrorsList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 2
            If chkBumpersList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkBumpersList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 7
            If chkMainPanelsList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkMainPanelsList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 1
            If chkGrillesList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkGrillesList.Items(nLoop).Text & vbCrLf
            End If
        Next

        nLoop = 0
        For nLoop = 0 To 6
            If chkOtherList.Items(nLoop).Selected = True Then
                memTAItems.Text = memTAItems.Text & chkOtherList.Items(nLoop).Text & vbCrLf
            End If
        Next

    End Sub


End Class
