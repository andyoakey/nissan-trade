﻿Imports System.Net.Mail
Imports System.Data
Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text
Imports DevExpress.Web

Partial Class LoadBadEmailsFile

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Load Bad Emails File"

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Load Bad Emails File", "Load Bad Emails File")
        End If

        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If




    End Sub

    Private Sub uplCSV_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs) Handles uplCSV.FileUploadComplete

        Dim bulkloadFolder As String = GetDataString("select longtextvalue from systemcontrol where id = 18")
        Dim fileName As String = Trim(e.UploadedFile.FileName)
        e.UploadedFile.SaveAs(bulkloadFolder + fileName)

        ' File Saved in Bulk Load Folder where sqlserver can see it
        Dim sSql As String = "exec sp_BulkLoad_BadEmailFile '" & fileName & "'"
        Call RunNonQueryCommand(sSql)

        ' Rename it so can be archived
        Dim newfilename As String = Replace(fileName, ".csv", "") & "_" & TextDateFileNameHMS(Now()) & ".csv"
        RenameFile(bulkloadFolder, fileName, newfilename)

    End Sub
End Class
