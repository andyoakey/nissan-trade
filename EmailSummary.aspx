﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EmailSummary.aspx.vb" Inherits="EmailSummary" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      
	<div style="position: relative; font-family: Calibri; text-align: left;" >


       
      <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblTitle" 
                runat ="server" 
                Text="Email Summary" />

       <br />


    <dx:ASPxLabel
        ID="lblInfo" 
        runat="server" 
        >
    </dx:ASPxLabel>
   
       <br />
    <dx:ASPxLabel
        ID="lblInfo2" 
        Font-Size="X-Small"
        Font-Italic="true"
        runat="server" 
        >
    </dx:ASPxLabel>
       <br />
       <br />

    <dx:ASPxGridView 
        ID="gridMain" 
        runat="server" 
        OnLoad="gridStyles"
        AutoGenerateColumns="False"  
        Styles-AlternatingRow-BackColor = "GhostWhite"
        Styles-Header-Wrap="True"
        Styles-Header-HorizontalAlign="Center"
        Styles-Cell-HorizontalAlign="Center"
        Styles-Footer-HorizontalAlign="Center"
        SettingsBehavior-ProcessSelectionChangedOnServer="true"
        ShowHeaderFilterBlankItems="false"
        DataSourceID="dsMain" 
        visible="true" 
        width="100%"       >

        <SettingsPager 
            Mode="ShowPager"
            PageSize="16" 
            ShowDefaultImages="True" 
            AllButton-Visible="true"/>
    
        <Settings 
            ShowHeaderFilterBlankItems="false"
            ShowFilterRowMenu="False" 
            ShowFooter="True" 
            ShowGroupedColumns="False"
            ShowGroupFooter="Hidden" 
            ShowGroupPanel="False" 
            ShowHeaderFilterButton="False"
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False" />
    
        <SettingsBehavior 
            AllowSort="True" 
            AutoFilterRowInputDelay="12000" 
            ColumnResizeMode="Control" />

        <Columns>

            <dx:GridViewDataTextColumn 
                Caption="Region" 
                FieldName="region" 
                ExportWidth="100"
                VisibleIndex="0"
                Visible ="true" 
                Settings-AllowHeaderFilter="True"
                Width="8%">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Zone" 
                FieldName="zone" 
                ExportWidth="100"
                VisibleIndex="1"
                Settings-AllowHeaderFilter="True"
                Visible ="true" 
                Width="8%">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                ExportWidth="200"
                FieldName="dealer" 
                VisibleIndex="2"
                Visible ="true" 
                Width="24%">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign = "Center" Caption="All A/Cs" >
                <Columns>
   
                    <dx:GridViewDataTextColumn 
                            Caption="Total A/Cs" 
                            FieldName="count_total" 
                            PropertiesTextEdit-DisplayFormatString="##,##0"
                        ExportWidth="150"
                            VisibleIndex="3"
                            Visible ="true" 
                            Width="10%">
                            <FooterCellStyle HorizontalAlign = "Center" />
                     </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="A/Cs with valid email" 
                        ExportWidth="150"
                        FieldName="count_email" 
                        PropertiesTextEdit-DisplayFormatString="##,##0"
                        VisibleIndex="4"
                        Visible ="true" 
                        Width="10%">
                    </dx:GridViewDataTextColumn>
            
                    <dx:GridViewDataTextColumn 
                        Caption="% A/Cs with valid email" 
                        FieldName="percent_email" 
                        ExportWidth="150"
                        VisibleIndex="5"
                        PropertiesTextEdit-DisplayFormatString="0.0%"
                        Visible ="true" 
                        Width="10%">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn HeaderStyle-HorizontalAlign = "Center" Caption="Active A/Cs (Spent in last 3 months)" >
                <Columns>
                    <dx:GridViewDataTextColumn 
                        Caption="Total Active A/Cs" 
                        FieldName="count_active" 
                        ExportWidth="150"
                        PropertiesTextEdit-DisplayFormatString="##,##0"
                        VisibleIndex="5"
                        Visible ="true" 
                        Width="10%">
                    </dx:GridViewDataTextColumn>
            
                    <dx:GridViewDataTextColumn 
                        Caption="Active A/Cs with valid email" 
                        FieldName="count_active_email" 
                        PropertiesTextEdit-DisplayFormatString="##,##0"
                        VisibleIndex="6"
                        ExportWidth="150"
                        Visible ="true" 
                        Width="10%">
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn 
                        Caption="% Active A/Cs with valid email" 
                        FieldName="percent_active_email" 
                        ExportWidth="200"
                        PropertiesTextEdit-DisplayFormatString="0.0%"
                        VisibleIndex="7"
                        Visible ="true" 
                        Width="10%">
                    </dx:GridViewDataTextColumn>
                    
                </Columns>
            </dx:GridViewBandColumn>

        </Columns>
    
       <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0"  FieldName = "count_total" SummaryType="Sum"   />
                <dx:ASPxSummaryItem DisplayFormat="##,##0"  FieldName = "count_email" SummaryType="Sum"   />
                <dx:ASPxSummaryItem DisplayFormat="0.0%"  FieldName = "percent_email" SummaryType="Average"   />
                <dx:ASPxSummaryItem DisplayFormat="##,##0"  FieldName = "count_active" SummaryType="Sum"   />
                <dx:ASPxSummaryItem DisplayFormat="##,##0"  FieldName = "count_active_email" SummaryType="Sum"   />
                <dx:ASPxSummaryItem DisplayFormat="0.0%"  FieldName = "percent_active_email" SummaryType="Average"   />
        </TotalSummary>

       </dx:ASPxGridView>

    </div>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridMain" >
     </dx:ASPxGridViewExporter>
    
    <asp:SqlDataSource  
        ID="dsMain" 
        runat="server"  
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_EmailSummary" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
                <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
                <asp:SessionParameter Name="ncurrentperiod" SessionField="currentperiod" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
   
</asp:Content>


