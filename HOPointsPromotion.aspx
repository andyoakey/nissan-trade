<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HOPointsPromotion.aspx.vb" Inherits="HOPointsPromotion" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel 
                ID="lblPageTitle" 
                Font-Size="Larger"
                runat="server" 
                Text="Points Promotion 2018/19" />
        </div>

        <table id="trSelectionStuff" Width="100%">
            <tr>
                <td width="10%" >
                    <dx:ASPxLabel 
                        ID="ASPxLabel5" 
                        runat="server" 
                        Text="From" />
                </td>
                
                <td width="10%" >
                    <dx:ASPxLabel 
                        ID="ASPxLabel1" 
                        runat="server" 
                        Text="To" />
                </td>

                <td width="80%"  align="Left">
                </td>
           </tr>

            <tr runat="server"  >
                <td width="10%" >
                    <dx:ASPxComboBox 
                        id ="ddlFrom"
                        AutoPostBack="True"
                        runat="server"
                        width="90%">
                      </dx:ASPxComboBox>
                </td>
                
                <td width="10%" >
                    <dx:ASPxComboBox 
                        id ="ddlTo"
                        AutoPostBack="True"
                        runat="server"
                        width="90%">
                    </dx:ASPxComboBox>
                </td>

                <td width="80%"  align="Left">
                </td>
               </tr>
        </table>

        <br />
        
        <dx:ASPxGridView 
            ID="gridPointsPromotion" 
            onload="gridstyles"
            cssclass="grid_styles"
            runat="server" 
            DataSourceID="dsPoints"  
            AutoGenerateColumns="False"  
            Styles-Header-HorizontalAlign="Center" 
            Styles-Header-Wrap="True" 
            Styles-Cell-HorizontalAlign="Center" 
            Style="position: relative;" Width="100%" >
       
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16">
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign="Center" />

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False" 
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" 
                ShowFooter="True" />

            <SettingsBehavior 
                AllowSort="True" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Columns>

                <dx:GridViewBandColumn Caption="Dealer" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Code" 
                    FieldName="dealercode" 
                    ReadOnly="True" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="0" 
                    ExportWidth="100"
                    Width="5%"  
                    Visible="true" />
     
                        <dx:GridViewDataTextColumn 
                    Caption="Name" 
                    FieldName="dealername" 
                    ReadOnly="True" 
                    VisibleIndex="1" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-Wrap="False"
                    Width="14%"  
                    ExportWidth="200" />
                      
                        <dx:GridViewDataTextColumn 
                    Caption="Zone" 
                    FieldName="zone" 
                    ReadOnly="True" 
                    VisibleIndex="2" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Width="5%"  
                    ExportWidth="100" />
                          </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Remanufactured Parts" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    FieldName="re_unitssold" 
                    width="4%"
                    ReadOnly="True" 
                    VisibleIndex="2"
                    ExportWidth="100"                             
                    Visible="true" />

                    <dx:GridViewDataTextColumn 
                    Caption="Points Earned (10 per unit)" 
                    FieldName="re_points" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="3" 
                    Visible="true" />

                    <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    FieldName="re_value" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="3" 
                    PropertiesTextEdit-DisplayFormatString="C2"/>

                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Service Kits" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    FieldName="sk_unitssold" 
                    width="4%"
                    ReadOnly="True" 
                    VisibleIndex="4" 
                    ExportWidth="100"
                    Visible="true" />

                     <dx:GridViewDataTextColumn 
                    Caption="Points Earned (10 per kit)" 
                    FieldName="sk_points" 
                    width="7%"
                    ExportWidth="200"
                    ReadOnly="True" 
                    VisibleIndex="5" 
                    Visible="true" />

                    <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    FieldName="sk_value" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="5" 
                    PropertiesTextEdit-DisplayFormatString="C2"/>



                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Consumables" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    FieldName="con_unitssold" 
                    width="4%"
                    ExportWidth="100"
                    ReadOnly="True" 
                    VisibleIndex="6" 
                    Visible="true" />

                        <dx:GridViewDataTextColumn 
                    Caption="Points Earned (5 per unit)" 
                    FieldName="con_points" 
                    width="7%"
                    ReadOnly="True"
                     ExportWidth="200"                             
                    VisibleIndex="7" 
                    Visible="true" />

                        <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    FieldName="con_value" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="7" 
                    PropertiesTextEdit-DisplayFormatString="C2"/>




                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Braking" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    FieldName="br_unitssold" 
                    width="4%"
                    ExportWidth="100"
                    ReadOnly="True" 
                    VisibleIndex="6" 
                    Visible="true" />

                        <dx:GridViewDataTextColumn 
                    Caption="Points Earned (10 per unit)" 
                    FieldName="br_points" 
                    width="7%"
                    ReadOnly="True"
                     ExportWidth="200"                             
                    VisibleIndex="7" 
                    Visible="true" />

                        <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    FieldName="br_value" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="7" 
                    PropertiesTextEdit-DisplayFormatString="C2"/>




                  </Columns>
                </dx:GridViewBandColumn>
   
                <dx:GridViewBandColumn Caption="Direct Mailers" HeaderStyle-HorizontalAlign="Center">
                  <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Units" 
                    FieldName="dm_unitssold" 
                    width="4%"
                    ExportWidth="100"
                    ReadOnly="True" 
                    VisibleIndex="9" 
                    Visible="true" />
                    
                    <dx:GridViewDataTextColumn 
                    Caption="Points Earned (10 per unit)" 
                    FieldName="dm_points" 
                    width="7%"
                    ExportWidth="200"
                    ReadOnly="True" 
                    VisibleIndex="10" 
                    Visible="true" />

                        <dx:GridViewDataTextColumn 
                    Caption="Sale Value" 
                    FieldName="dm_value" 
                    width="7%"
                    ExportWidth="200"    
                    ReadOnly="True" 
                    VisibleIndex="10" 
                    PropertiesTextEdit-DisplayFormatString="C2"/>

                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Data Submission" HeaderStyle-HorizontalAlign="Center" Name ="bandDataSubmission" Visible="false">
                      <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Submission %" 
                    FieldName="sub_pc" 
                    width="6%"
                    ExportWidth="150"
                    ReadOnly="True" 
                    VisibleIndex="11" 
                    Visible="true" />
                    
                              <dx:GridViewDataTextColumn 
                    Caption="Points" 
                    FieldName="sub_points" 
                    width="4%"
                    ExportWidth="150"
                    ReadOnly="True" 
                    VisibleIndex="12" 
                    Visible="true" />
                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Outbound Calls" HeaderStyle-HorizontalAlign="Center"  Name ="bandOutbound" Visible="false">
                      <Columns>
                        <dx:GridViewDataTextColumn 
                    Caption="Additional Calls" 
                    FieldName="out_calls" 
                    ExportWidth="150"
                    width="6%"
                    ReadOnly="True" 
                    VisibleIndex="13" 
                    Visible="true" />

                   <dx:GridViewDataTextColumn 
                    Caption="Points" 
                    FieldName="out_points" 
                    width="4%"
                    ExportWidth="150"
                    ReadOnly="True" 
                    VisibleIndex="14" 
                    Visible="true" />
                  </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Total Points" 
                    FieldName="total_points" 
                    width="5%"
                    ExportWidth="150"
                    ReadOnly="True" 
                    VisibleIndex="15" 
                    Visible="true" />
  
            </Columns>

            <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="re_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="re_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="C0" FieldName="re_value" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="sk_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="sk_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="C0" FieldName="sk_value" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="con_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="con_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="C0" FieldName="con_value" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="br_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="br_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="C0" FieldName="br_value" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="dm_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="dm_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="C0" FieldName="dm_value" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="sub_pc" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="sub_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="out_unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="out_points" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="total_points" SummaryType="Sum" />
            </TotalSummary>

            <Styles Footer-HorizontalAlign="Center" />

  
        </dx:ASPxGridView>
       
    </div>
    
    <asp:SqlDataSource 
        ID="dsPoints" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_PointsPromotion_HO_Summary" 
        SelectCommandType="StoredProcedure">
       <SelectParameters>
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridPointsPromotion" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>





