﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ServiceKitYOYDetail.aspx.vb" Inherits="ServiceKitYOYDetail" %>
<%@ MasterType VirtualPath="~/MasterPage.master" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

    <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

 <div style="position: relative; font-family: Calibri; text-align: left;" >
    <div class="row">
         <div id="divTitle" style="position:relative; left:5px">
                  <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Service Kit Invoices" />
               </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <dx:ASPxGridView ID="gridInvoices" runat="server" 
            AutoGenerateColumns="False" 
            style="position: relative; left: 5px" 
            Width="100%" 
            OnCustomColumnDisplayText="gridInvoices_OnCustomColumnDisplayText">
                  
            <Settings 
                ShowFooter="True" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="Hidden"
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"  
                ShowHeaderFilterBlankItems="false"
                ShowFilterRow="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" 
                
                />

            <SettingsBehavior 
                AllowSort="True" 
                ColumnResizeMode="Control"
                />

               <SettingsPager PageSize="16" AllButton-Visible="true"></SettingsPager>



            <Columns>

                <dx:GridViewDataDateColumn Caption="Date" FieldName="InvoiceDate" ToolTip=""
                    VisibleIndex="0" Width="10%" ExportWidth="150">
                    
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList"    />
                    
                    
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle"  />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn Caption="Dealer" FieldName="DealerCode" ToolTip=""
                    VisibleIndex="1" Width="5%" ExportWidth="150">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Customer" FieldName="CustomerName" ToolTip=""
                    VisibleIndex="2" Width="20%" ExportWidth="150">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Left" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="DMS Ref." FieldName="DMSRef" ToolTip="" ExportWidth="150"
                    VisibleIndex="3" Width="10%">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Account Name" FieldName="AccountName" ToolTip="" ExportWidth="200"
                    VisibleIndex="4" Width="20%">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Left" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Service Kits" CellStyle-HorizontalAlign="Right" ExportWidth="150"
                    FieldName="servicekit_count" ToolTip="The number of service kits on this invoice"
                    VisibleIndex="6">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Sales Value" CellStyle-HorizontalAlign="Right" ExportWidth="150"
                    FieldName="SaleValue" ToolTip="The total value of all parts sold"
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>
 
                <dx:GridViewDataTextColumn Caption="Cost Value" CellStyle-HorizontalAlign="Right"  ExportWidth="150"
                    FieldName="CostValue" ToolTip="The total cost of all parts purchased"
                    VisibleIndex="8">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Margin (%)" CellStyle-HorizontalAlign="Right" ExportWidth="150"
                    FieldName="Margin" ToolTip="The total percentage margin made on sales of all parts"
                    VisibleIndex="9">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total Invoices: {0}" FieldName="CustomerName" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="FreeOil" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="SaleValue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="CostValue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="Margin" ShowInColumn="Margin" SummaryType="Custom" />
            </TotalSummary>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ExportMode="Expanded" />

        </dx:ASPxGridView>
        </div>
    </div>
    
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridInvoices"/>

</asp:Content>


