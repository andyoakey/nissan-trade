﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Feedback.aspx.vb" Inherits="Feedback" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxsp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                Font-Size="Larger"
                runat="server" 
                Text="Contact Us" />
        </div>

        <table  id="ActivityTable" style="position: relative;" width="100%">

            <tr>
                <td valign="top" colspan="2">
                      <dx:ASPxLabel
                    ID="lblYourEmail" 
                    runat="server" 
                    Text="Your email address" 
                    Width="464px" 
                    Style="text-align: center;"/>
                </td>
            </tr>
        
            <tr>
                <td valign="top" colspan="2">
                    <dx:ASPxTextBox
                        ID="txtEmail" 
                        runat="server" 
                        ReadOnly="true" 
                        Width="250px" />
                </td>
            </tr>

            <tr>
                <td valign="top" colspan="2">
                    <dx:ASPxLabel
                        ID="lblYourComments" 
                        runat="server" 
                        Text="Your Comments" 
                        Width="464px"/>
                </td>
            </tr>
            
            <tr>
                <td valign="top" colspan="2">
                    <dx:ASPxMemo
                        ID="txtFeedBack" 
                        runat="server" 
                        Height="120px" 
                        Width="100%"/>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <br/>
                </td>
            </tr>

            <tr>
                <td valign="top" colspan="2" align="right">
                    <dx:ASPxButton 
                        ID="checkButton" 
                        runat="server" 
                        ClientInstanceName="checkButton" 
                        ClientEnabled="True" 
                        Text="Check Spelling ..." 
                        AutoPostBack="False" 
                        Width="90px" >
                        <ClientSideEvents Click="function(s, e) { spellChecker.CheckElementsInContainer(document.getElementById('ActivityTable')) }" />
                    </dx:ASPxButton>                            

                    <dx:ASPxButton 
                        ID="btnSend" 
                        runat="server" 
                        Text="Send" 
                        Width="100px">
                    </dx:ASPxButton>
                </td>
            </tr>

        </table>
        
        <br />

    </div>

       <dx:ASPxPopupControl 
        ID="PopSent" 
        Width="205px" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopSent" 
        ShowHeader="True" 
        HeaderText="Support Email"
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
                <dx:ASPxLabel
                    ID="lblSendMsg" 
                    runat="server" 
                    Text="Your comments have been emailed to the website support team." 
                    Width="464px" 
                    Style="text-align: center;"/>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

   <dxsp:ASPxSpellChecker 
        ID="ASPxSpellChecker2" 
        runat="server" 
        ClientInstanceName="spellChecker" 
        Culture="English (United States)">
        <Dictionaries>
            <dxsp:ASPxSpellCheckerISpellDictionary AlphabetPath="~/App_Data/Dictionaries/EnglishAlphabet.txt"
                GrammarPath="~/App_Data/Dictionaries/english.aff" 
                DictionaryPath="~/App_Data/Dictionaries/american.xlg"
                CacheKey="ispellDic" 
                Culture="English (United States)" 
                EncodingName="Western European (Windows)">
            </dxsp:ASPxSpellCheckerISpellDictionary>
        </Dictionaries>
        <ClientSideEvents 
            BeforeCheck="function(s, e) { checkButton.SetEnabled(false);}"
            AfterCheck="function(s, e) { checkButton.SetEnabled(true);}" 
            CheckCompleteFormShowing="function(s, e) {e.cancel=true;} " />
    </dxsp:ASPxSpellChecker>


    <asp:SqlDataSource ID="dsTPSMs" runat="server" SelectCommand="p_TPSMContactList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>




</asp:Content>
