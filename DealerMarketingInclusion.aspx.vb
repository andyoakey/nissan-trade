﻿Imports DevExpress.Web.Data

Partial Class DealerMarketingInclusion
    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        Dim master_label As DevExpress.Web.ASPxLabel
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_label = CType(Master.FindControl("lblDealerOnly"), DevExpress.Web.ASPxLabel)
        master_btnExcel.Visible = True

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub
    Protected Sub ExportToExcel()
        Dim sFileName As String = "DealerMarketingInclusion"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub gridDealer_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridDealer.HtmlDataCellPrepared

        If e.DataColumn.FieldName = "Exclude_Text" Then
            If e.CellValue = "No" Then
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = GlobalVars.g_Color_White
                e.Cell.BackColor = GlobalVars.g_Color_Green
            Else
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = GlobalVars.g_Color_White
                e.Cell.BackColor = GlobalVars.g_Color_Red
            End If
        End If

    End Sub

    Private Sub DealerMarketingInclusion_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            ' gridDealer.StartEdit(2)
        End If

    End Sub

    Private Sub gridDealer_RowUpdating(sender As Object, e As ASPxDataUpdatingEventArgs) Handles gridDealer.RowUpdating
        Dim sDealerCode = Left(e.OldValues.Item(3).ToString, 4)
        Dim sOldValue As String = e.OldValues.Item(4).ToString
        Dim sOldNotes As String = e.OldValues.Item(5).ToString
        Dim sNewValue As String = e.NewValues.Item(4).ToString
        Dim sNewNotes As String = e.NewValues.Item(5).ToString
        Dim nOldValue As Integer = 0
        If sOldValue = "Yes" Or sOldValue = "1" Then nOldValue = 1
        Dim nNewValue As Integer = 0
        If sNewValue = "Yes" Or sNewValue = "1" Then nNewValue = 1


        Dim sSql As String = "Exec sp_UpdateDealerMarketingInclusion " & Session("UserID") & ",'" & sDealerCode & "'," & nOldValue & "," & nNewValue & ",'" & sOldNotes & "','" & sNewNotes & "'"

        Call RunNonQueryCommand(sSql)

        gridDealer.CancelEdit()
        gridDealer.DataBind()
    End Sub
End Class
