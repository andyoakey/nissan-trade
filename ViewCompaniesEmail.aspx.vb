﻿Imports System.Data
Imports System.Net.Mail

Partial Class ViewCompaniesEmail

    Inherits System.Web.UI.Page

    Dim nCustomerId As Long
    Dim sEmail As String
    Dim inComingEmail As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nCustomerId = Request.QueryString(0)

        If Not Page.IsPostBack Then
            sEmail = Request.QueryString(1)
            txtEmail.Text = sEmail
            inComingEmail = sEmail
        End If

    End Sub

    Protected Sub btnEmailSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailSave.Click

        '   If IsValidEmail(txtEmail.Text) = False Then
        '  Exit Sub
        ' End If

        Dim sSQL As String = "Update Customer SET EmailAddress = '" & txtEmail.Text & "' WHERE Id = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sSQL)

        Dim sSQL2 As String = "Update CustomerDealer SET ContactEmailAddress = '" & txtEmail.Text & "' WHERE customerid = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sSQL2)

        Dim sSQL3 As String = ""
        If Trim(txtEmail.Text) <> "" Then
            sSQL3 = "Update NissanExtraFields SET Email = '" & txtEmail.Text & "',Email_Status = 'Valid'  WHERE customerid = " & Trim(Str(nCustomerId))
        Else
            sSQL3 = "Update NissanExtraFields SET Email = '',Email_Status = 'Blank'  WHERE customerid = " & Trim(Str(nCustomerId))
        End If
        Call RunNonQueryCommand(sSQL3)

        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Email Address Updated", "Customer Id : " & nCustomerId & ", New Email: " & txtEmail.Text)
        Dim startUpScript As String = String.Format("window.parent.HideEmailEdit();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)

    End Sub

End Class
