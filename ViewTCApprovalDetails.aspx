﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewTCApprovalDetails.aspx.vb" Inherits="ViewTCApprovalDetails" %>

<%--<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>--%>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0"  
            Width="976px" >

            <ContentStyle>
                <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
            </ContentStyle>

            <TabPages>

                <dxtc:TabPage Text="Customer Details">

                    <ContentCollection>

                        <dxw:ContentControl ID="ContentControl1" runat="server">
                                
                            <table id="trStatusMessages" width="956px" style="font-size: x-small">
                                <tr>
                                    <td style="width:60%" align="left" valign="middle">
                                    </td>
                                    <td align="right" valign="middle" >
                                        <asp:Label ID="lblApproval" runat="server" Text="Awaiting TPSM approval" 
                                            style="font-weight: 700; font-size: small; text-align: right;" 
                                            Visible="False">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            
                            <table id="trCustomerDetails" width="956px" style="font-size: x-small">
                            
                                <%----------------------------------------------------------------------------------%>                            
                                <tr id="trCentreDetails" runat="server" >
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="Label1" runat="server" Text="Centre" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtDealerCode" runat="server" Width="300px" TabIndex="1" ReadOnly="true"  />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>

                                <%----------------------------------------------------------------------------------%>                            
                                <tr id="trTradeClub" runat="server" >
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblTCNumber" runat="server" Text="Trade Club #" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtTradeClub" runat="server" Width="100px" TabIndex="2" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>

                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblBusinessName" runat="server" Text="Business Name" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtBusinessName" runat="server" Width="300px" TabIndex="3" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblBusinessType" runat="server" Text="Business Type" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlBusinessType" runat="server" Width="305px" ReadOnly="true"
                                             >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                

                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblAddress1" runat="server" Text="Address" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress1" runat="server" Width="300px" TabIndex="1" ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblExperianCategory" runat="server" Text="Experian Category" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtYPCategory" runat="server" Width="300px" TabIndex="1"  
                                            ReadOnly="true"/>
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress2" runat="server" Width="300px" TabIndex="9" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                    </td>
                                    <td align="left">
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress3" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblTelephoneNumber" runat="server" Text="Telephone Number" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTelephone" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress4" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblMobileNumber" runat="server" Text="Mobile Number" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtMobile" runat="server" Width="300px" TabIndex="15" 
                                            ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblTown" runat="server" Text="Town" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress5" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblFaxNumber" runat="server" Text="Fax Number" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFaxNumber" runat="server" Width="300px" TabIndex="16" 
                                            ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblCounty" runat="server" Text="County" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtAddress6" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblEmailAddress" runat="server" Text="Email Address" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmailAddress" runat="server" Width="300px" TabIndex="17" 
                                            ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblPostCode" runat="server" Text="Postcode" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtPostCode" runat="server" Width="100px" TabIndex="14" 
                                                        ReadOnly="true" />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCDA" runat="server" Text="" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblWebsite" runat="server" Text="Website" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtWebSite" runat="server" Width="300px" TabIndex="18" 
                                            ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                    </td>
                                    <td style="width: 328px" align="right">
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                    </td>
                                    <td align="right">
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblProprietorsName" runat="server" Text="Proprietor's Name" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtProprietorName" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblProprietorsJobTitle" runat="server" Text="Prop. Job Title" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProprietorJobTitle" runat="server" Width="300px" 
                                            TabIndex="19" ReadOnly="true" />
                                    </td>
                                </tr>
                                
                                <%----------------------------------------------------------------------------------%>                            
                                <tr>
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblKeyContactName" runat="server" Text="Key Contact Name" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:TextBox ID="txtKeyContactName" runat="server" Width="300px" TabIndex="1" 
                                            ReadOnly="true" />
                                    </td>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                        <asp:Label ID="lblKeyContactJobTitle" runat="server" Text="Key Contact Job Title" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtKeyContactJobTitle" runat="server" Width="300px" 
                                            TabIndex="20" ReadOnly="true" />
                                    </td>
                                </tr>
                                
                            </table>
                            
                            <table id="tabMailFlags" width="956px" style="font-size: x-small">
                                <tr>
                                    <td style="width: 150px; font-size: x-small;" align="right">
                                    </td>
                                    <td style="width: 834px" align="left">
                                        <asp:CheckBoxList ID="cblMailFlags1" runat="server"  
                                            RepeatColumns="4" TextAlign="Left" Font-Size="Small" ReadOnly="true" 
                                            TabIndex="21">
                                            <asp:ListItem Value="0" Text="Safe to Mail?" />
                                            <asp:ListItem Value="1" Text="Safe to Phone?" />
                                        </asp:CheckBoxList>
                                        <asp:CheckBoxList ID="cblMailFlags2" runat="server"  
                                            RepeatColumns="4" TextAlign="Left" Font-Size="Small" ReadOnly="true" 
                                            TabIndex="22">
                                            <asp:ListItem Value="2" Text="Safe to Fax?" />
                                            <asp:ListItem Value="3" Text="Safe to EMail?" />
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>

                            <%----------------------------------------------------------------------------------%>                            
                            <table id="tabReason" width="956px" style="font-size: x-small" runat="server"> 
                                <tr id="tr8" runat="server" >
                                    <td style="width: 15%; font-size: x-small;" align="right">
                                        <asp:Label ID="Label7" runat="server" Text="Reason for removal"  
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtReason" runat="server" Width="300px" TabIndex="1" ReadOnly="true"  />
                                    </td>
                                </tr>
                            </table>

                            <%----------------------------------------------------------------------------------%>                            
                            <table id="tabMessage" width="956px" style="font-size: x-small">
                                <tr>
                                    <td style="font-size: x-small;" align="left">
                                        <asp:Label ID="lblStatusMessage" runat="server" Text="Status Message"                                             
                                            style="font-weight: 700; font-size: small; color: #CC0000; text-align: center;">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>
                            
                            <%----------------------------------------------------------------------------------%>                            
                            <table id="tabButtons" width="956px">
                                <tr runat="server" id="tr15" style="height: 30px">
                                    <td style="width: 5%" align="left">
                                        <dxe:ASPxButton ID="btnBack" runat="server" Text="Back"   Style="text-align: left">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="width: 5%" align="left">
                                        <dxe:ASPxButton ID="btnApprove" runat="server" Text="Approve"  Style="text-align: left" Width="75px" 
                                            Visible="True">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td style="width: 5%" align="left">
                                        <dxe:ASPxButton ID="btnDecline" runat="server" Text="Decline"  Style="text-align: left" Width="75px" 
                                            Visible="True">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td align="right" valign="middle">
                                        <asp:Label ID="lblDeleteMessage" runat="server" Text="Are you sure you want to remove this Customer from the Trade Club?"                                             
                                            style="font-weight: 700; font-size: small; color: #CC0000; text-align: center;">
                                        </asp:Label>
                                    </td>
                                    <td align="right" style="width: 5%">
                                        <dxe:ASPxButton ID="btnSave" runat="server" Text="OK"  Style="text-align: left" Visible="false">
                                        </dxe:ASPxButton>
                                    </td>
                                    <td align="right" style="width: 5%">
                                        <dxe:ASPxButton ID="btnCancel" runat="server" Text="Cancel"  Style="text-align: left" Visible="false">
                                        </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table id="tabStatusLabels" width="956px" 
                                style="font-size: small; color: #FFFFFF;" bgcolor="#006666">
                                <tr>
                                    <td style="width: 150px;" align="right">
                                        Created By
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:Label ID="lblCreated" runat="server" Text="">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 150px;" align="right">
                                        Last Updated By
                                    </td>
                                    <td style="width: 328px" align="left">
                                        <asp:Label ID="lblUpdated" runat="server" Text="">
                                        </asp:Label>
                                    </td>
                                </tr>
                            </table>

                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                
                <dxtc:TabPage Text="Centre Specific Info">

                    <ContentCollection>

                        <dxw:ContentControl ID="ContentControl2" runat="server">
                        
                            <table id="tabCentreSpec1" width="956px" style="font-size: x-small">
                                <%----------------------------------------------------------------------------------%>                            
                                
                                <tr id="Tr1" runat="server">
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblVanRoute" runat="server" Text="Van Route" Width="180px"
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 150px" align="left">
                                        <asp:TextBox ID="txtVanRoute" runat="server" Width="100px" TabIndex="23" 
                                            MaxLength="10" ReadOnly="True" />
                                    </td>
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblUser1" runat="server" Text="User Field 1" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserField1" runat="server" Width="350px" TabIndex="27" Readonly="true" 
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr2" runat="server">
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblDriveTime" runat="server" Text="Drive Time (mins)" Width="180px"
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <asp:TextBox ID="txtDriveTime" runat="server" Width="100px" TabIndex="24" 
                                            MaxLength="10" ReadOnly="True" />
                                    </td>
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblUser2" runat="server" Text="User Field 2" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserField2" runat="server" Width="350px" TabIndex="28" Readonly="true"
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr3" runat="server">
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblDistance" runat="server" Text="Distance (miles)" Width="180px"
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <asp:TextBox ID="txtDistance" runat="server" Width="100px" TabIndex="26" 
                                            MaxLength="10" ReadOnly="True" />
                                    </td>
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblUser3" runat="server" Text="User Field 3" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserField3" runat="server" Width="350px" TabIndex="29"  Readonly="true"
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr4" runat="server">
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblSalesRep" runat="server" Text="Sales Rep" Width="180px"
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <asp:TextBox ID="txtSalesRep" runat="server" Width="100px" TabIndex="4"  
                                            MaxLength="10" ReadOnly="True"/>
                                    </td>
                                    <td style="width: 20%; font-size: x-small;" align="right">
                                        <asp:Label ID="lblUser4" runat="server" Text="User Field 4" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserField4" runat="server" Width="350px" TabIndex="1" Readonly="true"
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                            </table>
                            
                            <table id="tabCentreSpec2" width="956px" style="font-size: x-small">
                            
                                <tr id="Tr5" runat="server">
                                    <td style="width: 20%; font-size: small;" align="right">
                                        <asp:Label ID="Label3" runat="server" Text="Alternative Contact Name" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtContactNameSpec" runat="server" Width="690px" TabIndex="31" 
                                            MaxLength="100" ReadOnly ="True"/>
                                    </td>
                                </tr>
                                
                                <tr id="Tr6" runat="server">
                                    <td style="width: 20%; font-size: small;" align="right">
                                        <asp:Label ID="Label5" runat="server" Text="Alternative Telephone" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTelephoneSpec" runat="server" Width="690px" TabIndex="33" 
                                            MaxLength="100" ReadOnly ="True" />
                                    </td>
                                </tr>
                                
                                <tr id="Tr7" runat="server">
                                    <td style="width: 20%; font-size: small;" align="right" valign="top" >
                                        <asp:Label ID="lblNotes" runat="server" Text="Notes" 
                                            style="font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNotes" runat="server" Width="690px" TabIndex="5" 
                                            ReadOnly="true" Height="250px" Rows="1" TextMode="MultiLine"  />
                                    </td>
                                </tr>
                                
                            </table>

                        </dxw:ContentControl>

                    </ContentCollection>

                </dxtc:TabPage>

                <dxtc:TabPage Text="Services">
                    <ContentCollection>
                        <dxw:ContentControl ID="ContentControl4" runat="server">

                            <%----------------------------------------------------------------------------------%>                            
                            <table id="tabServices" width="956px" style="font-size: x-small">
                                <tr>
                                    
                                    <td style="width: 240px; font-size: x-small;" align="left">
                                        <asp:CheckBoxList ID="cblServices" runat="server"  
                                            RepeatColumns="1" TextAlign="right" Font-Size="Small" Enabled="False">
                                            <asp:ListItem Value="0" Text="Service/Repair" />
                                            <asp:ListItem Value="1" Text="MOT" />
                                            <asp:ListItem Value="2" Text="Tyres" />
                                            <asp:ListItem Value="3" Text="Diagnostics" />
                                            <asp:ListItem Value="4" Text="Air Conditioning" />
                                            <asp:ListItem Value="5" Text="Electrical Repairs" />
                                        </asp:CheckBoxList>
                                    </td>
                                    
                                    <td valign="top" >
                                        <table>
                                            <tr>
                                                <td style="width: 128px; font-size: x-small;" align="right">
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px; font-size: x-small;" align="right">
                                                    <asp:Label ID="lblRamps" runat="server" Text="Ramps/Bays" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dxe:ASPxSpinEdit ID="spinRamps" runat="server" Height="21px" Number="0" 
                                                        Width="50px" ReadOnly="true" TabIndex="35" >
                                                    </dxe:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px; font-size: x-small;" align="right">
                                                    <asp:Label ID="Label2" runat="server" Text="Technicians" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                                <td style="width: 50px" align="right">
                                                    <dxe:ASPxSpinEdit ID="spinTechnicians" runat="server" Height="21px" Number="0" Width="50px" ReadOnly="true" TabIndex ="36">
                                                    </dxe:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px; font-size: x-small;" align="right">
                                                    <asp:Label ID="Label4" runat="server" Text="Vehicles Repaired/Wk" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dxe:ASPxSpinEdit ID="spinVehicleRepairs" runat="server" Height="21px" Number="0" Width="50px" ReadOnly="true" TabIndex ="37">
                                                    </dxe:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px; font-size: x-small; font-weight: 700;" align="right">
                                                    <asp:Label ID="Label6" runat="server" Text="Vehicles Repaired/Wk" 
                                                        style="font-weight: normal; font-size: small">
                                                    </asp:Label>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dxe:ASPxSpinEdit ID="spinToyotaRepairs" runat="server" Height="21px" Number="0" Width="50px" ReadOnly="true" TabIndex ="38">
                                                    </dxe:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                    <td valign="top" >
                                        <table id="tabCurrentMotorFactors" style="font-size: x-small">
                                            <tr>
                                                <td style="width: 60%; font-size: x-small;" align="left">
                                                    <asp:Label ID="lblMotorFactors" runat="server" Text="Motor Factors" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                                <td style="font-size: x-small;" align="left">
                                                    <asp:Label ID="lblSpend" runat="server" Text="Spend" 
                                                        style="font-size: small">
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width: 60%; font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtMotorFactor1" runat="server" Width="300px" TabIndex="39" 
                                                        ReadOnly="true" />
                                                </td>
                                                <td style="font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtSpend1" runat="server" Width="125px" TabIndex="40" 
                                                        ReadOnly="true" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width: 60%; font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtMotorFactor2" runat="server" Width="300px" TabIndex="42" 
                                                        ReadOnly="true" />
                                                </td>
                                                <td style="font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtSpend2" runat="server" Width="125px" TabIndex="1" ReadOnly="true" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 60%; font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtMotorFactor3" runat="server" Width="300px" TabIndex="43" 
                                                        ReadOnly="true" />
                                                </td>
                                                <td style="font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtSpend3" runat="server" Width="125px" TabIndex="44" 
                                                        ReadOnly="true" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 60%; font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtMotorFactor4" runat="server" Width="300px" TabIndex="46" 
                                                        ReadOnly="true" />
                                                </td>
                                                <td style="font-size: x-small;" align="left">
                                                    <asp:TextBox ID="txtSpend4" runat="server" Width="125px" TabIndex="47" 
                                                        ReadOnly="true" />
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                                        
                            <table>
                                <tr>
                                    <td valign="middle" >
                                        <asp:Label ID="lblOther" runat="server" Text="Other Specialism" style="padding-left:5px; font-size: small">
                                        </asp:Label>
                                    </td>
                                    <td valign="middle" >
                                        <asp:TextBox ID="txtOther" runat="server" Width="300px" TabIndex="48" 
                                            ReadOnly="true"  />
                                    </td>
                                </tr>
                            </table>
                            <br />

                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>

            </TabPages>
            
            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>

        </dxtc:ASPxPageControl>
        <br />

    </div>
    
</asp:Content>


