﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HOPointsPromotionPartsList.aspx.vb" Inherits="HOPointsPromotionPartsList" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Points Promotion Parts List" />
        </div>

      <div id="divNote" style="position:relative;">
           <dx:ASPxLabel 
                Font-Italic="true"
                ID="lblNote" runat="server" 
                Text="This report shows the Current List of Items that Score points in the Head Office Points Promotion. The Parts Are Sorted By Units Sold in the last 12 months."></dx:ASPxLabel>
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsList" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="20" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />

        <Styles Footer-HorizontalAlign="Center" />
                      
        <Columns>

            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Report Section" 
                FieldName="reportsection">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="8%" 
                exportwidth="250"
                Caption="Service Type" 
                FieldName="servicetype">
            </dx:GridViewDataTextColumn>
      
            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Product Code" 
                FieldName="productcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="15%" 
                exportwidth="250"
                Caption="Product Description" 
                FieldName="productdescription">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Part Number" 
                FieldName="partnumber">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="5" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="15%" 
                exportwidth="250"
                Caption="Part Description" 
                FieldName="partdescription">
            </dx:GridViewDataTextColumn>

            <dx:GridViewBandColumn Caption="National Sales Last 12 months" HeaderStyle-HorizontalAlign="Center">
                    <columns>
                        <dx:GridViewDataTextColumn
                            VisibleIndex="5" 
                            Settings-AllowSort="True"
                            Settings-AllowGroup="False"
                            Width="8%" 
                            exportwidth="250"
                            CellStyle-HorizontalAlign="Center"
                            Caption="Units Sold" 
                            FieldName="unitssold">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn
                            VisibleIndex="6" 
                            Settings-AllowSort="True"
                            Settings-AllowGroup="False"
                            Width="8%" 
                            CellStyle-HorizontalAlign="Center"
                            exportwidth="250"
                            PropertiesTextEdit-DisplayFormatString="£#,##0" 
                            Caption="Sale Value" 
                            FieldName="salevalue">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn
                            VisibleIndex="7" 
                            Settings-AllowSort="True"
                            Settings-AllowGroup="False"
                            Width="8%" 
                            CellStyle-HorizontalAlign="Center"
                            exportwidth="250"
                            PropertiesTextEdit-DisplayFormatString="£#,##0" 
                            Caption="Cost To Dealer" 
                            FieldName="costvalue">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn
                            VisibleIndex="8" 
                            Settings-AllowSort="True"
                            Settings-AllowGroup="False"
                            CellStyle-HorizontalAlign="Center"
                            Width="8%" 
                            exportwidth="250"
                            PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                            Caption="Avg. Margin" 
                            FieldName="margin">
                        </dx:GridViewDataTextColumn>

                    </columns>
           </dx:GridViewBandColumn>


         </Columns>

        <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="unitssold" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="£##,##0" FieldName="salevalue" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="£##,##0" FieldName="costvalue" SummaryType="Sum" />
            </TotalSummary>


        
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsList" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_CurrentPointsPromotionList" 
            SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>

        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

