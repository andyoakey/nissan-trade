﻿Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class NationwideAccountSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Session("ReturnProgram") = "NationwideAccountSearch.aspx"

        If Page.IsPostBack = False Then
            Session("SpendOnly_Flag") = 1
        End If

        gridPossibles.DataBind()


    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "NationwideAccountSearch"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Private Sub gridPossibles_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridPossibles.HtmlDataCellPrepared

        If e.DataColumn.FieldName = "customerdealerid" Then
            If e.GetValue("siteid") > 0 Then
                e.Cell.Controls.Clear()
            End If
        End If

    End Sub

    Private Sub chkSpendOnly_ValueChanged(sender As Object, e As EventArgs) Handles chkSpend.ValueChanged

        If chkSpend.Checked = True Then
            Session("SpendOnly_Flag") = 1
        Else
            Session("SpendOnly_Flag") = 0
        End If

    End Sub
End Class
