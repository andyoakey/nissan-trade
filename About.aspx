﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="About.aspx.vb" Inherits="About" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="About TFP" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <table id="trAbout" width="450px">

            <tr runat="server" style="height:30px" valign="top">
                    <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%; font-size: small; text-align: left;" >
                    Application Version
                </td>
                <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%;" 
                        align="left" >
                    <asp:Label ID="lblApplicationVersion" runat="server" Text="Label" style="font-size: small">
                    </asp:Label>
                </td>
            </tr>

            <tr id="Tr1" runat="server" style="height:30px" valign="top">
                    <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%; font-size: small; text-align: left;" >
                    Database IP Address
                </td>
                <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%;" 
                        align="left" >
                    <asp:Label ID="lblDBIP" runat="server" Text="Label" style="font-size: small">
                    </asp:Label>
                </td>
            </tr>

            <tr id="Tr2" runat="server" style="height:30px" valign="top">
                    <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%; font-size: small; text-align: left;" >
                    Database Name
                </td>
                <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%;" 
                        align="left" >
                    <asp:Label ID="lblDBName" runat="server" Text="Label" style="font-size: small">
                    </asp:Label>
                </td>
            </tr>

            <tr id="Tr3" runat="server" style="height:30px" valign="top">
                    <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%; font-size: small; text-align: left;" >
                    Last Website Refresh
                </td>
                <td style="border-style: solid; border-width: 1px; padding: 1px 4px; width:25%;" 
                        align="left" >
                    <asp:Label ID="lblWebRefresh" runat="server" Text="Label" style="font-size: small">
                    </asp:Label>
                </td>
            </tr>

        </table> 
        <br />
        
    </div>
    
</asp:Content>

