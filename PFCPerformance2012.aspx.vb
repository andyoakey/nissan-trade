﻿Partial Class PFCPerformance2012

    Inherits System.Web.UI.Page

    Dim nSale1 As Double
    Dim nCost1 As Double
    Dim nSale2 As Double
    Dim nCost2 As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "NissanDATA PARTS - PFC Performance Report 2013"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "PFC Performance Report 2013", "PFC Performance Report 2013")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsPFCPerf_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsPFCPerf.Init
        dsPFCPerf.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridPFCPerf_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridPFCPerf.HtmlDataCellPrepared

        Dim nThisValue As Decimal

        If e.DataColumn.Index >= 3 And e.DataColumn.Index <= 4 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index >= 5 And e.DataColumn.Index <= 6 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 7 Or e.DataColumn.Index = 8 Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            If nThisValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            ElseIf nThisValue < 0 Then
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

    Private Sub RenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)
        e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then
            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        Else
            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Index >= 3 And e.Column.Index <= 4 Then
                e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                e.TextValueFormatString = "#,##0"
            End If

            If e.Column.Index >= 5 And e.Column.Index <= 6 Then
                e.BrickStyle.BackColor = System.Drawing.Color.LightGreen
                e.TextValueFormatString = "#,##0"
            End If

            If e.Column.Index >= 7 And e.Column.Index <= 8 Then
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value)
                End If
                If nThisValue > 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Green
                ElseIf nThisValue < 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                End If
                e.TextValueFormatString = "#,##0"
            End If

        End If

    End Sub



    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call RenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String
        sFileName = "PFCPerformance2013_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.WriteXlsxToResponse(fileName:=sFileName)
    End Sub

End Class
