﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PartInfo.aspx.vb" Inherits="PartInfo" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <table width="100%">
            <tr>
                <td>
                    <div id="divTitle" style="position:relative;">
                       <dx:ASPxLabel 
                            Font-Size="Larger"
                            ID="lblPageTitle" 
                            runat="server" 
                            Text="Part Information" />
                    </div>
       
                    <br />
                </td>
            </tr>
        </table>

         

        <table id="trSelectionStuff" style="position: relative;" Width="969px">
    
            <tr id="Tr1" runat="server" style="height:30px;">
                <td style="width:10%; font-size: small;" align="left" >
                       <dx:ASPxLabel 
                        ID="ASPxLabel1" 
                        runat="server" 
                        Text="Part Number / Description" />
                </td>

                <td style="width:90%; font-size: small;" align="left" >
                </td>
            </tr>

            <tr id="Tr6" runat="server" style="height:30px;">

                <td style="width:10%; font-size: small;" align="left" >
                    <dx:ASPxTextBox 
                        ID="txtPartNumber" 
                        runat="server" 
                        Width="170px" >
                    </dx:ASPxTextBox>
                </td>

                <td style="width:90%; font-size: small;" align="left" >
                    <dx:ASPxButton 
                        ID="btnSearch" 
                        runat="server" 
                        Text="Search" 
                        Width="100px">
                    </dx:ASPxButton>
                </td>
            </tr>

        </table> 
        <br />
        
        <dx:ASPxGridView 
            ID="gridPartInfo" 
            runat="server" 
            AutoGenerateColumns="False" 
            Visible="True" 
            OnLoad="gridStyles"
            DataSourceID="dsPartInfo" 
            style="position: relative;" Width="100%" >

            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                PageSize="16"
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true">
            </SettingsPager>

            <Settings 
                ShowFooter="False" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="Hidden"
                ShowGroupPanel ="False" 
                ShowHeaderFilterButton="False"  
                ShowFilterRow="False" 
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True"/>

            <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Part Number" 
                    FieldName="PartNumber" 
                    VisibleIndex="0" 
                    Width="14%"
                    ExportWidth="150"
                    CellStyle-HorizontalAlign ="Center" 
                    HeaderStyle-HorizontalAlign="Center">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Part Description" 
                    FieldName="PartDescription" 
                    VisibleIndex="1" 
                    Width="14%"
                    ExportWidth="150"
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Discount Code" 
                    FieldName="DiscountCode" 
                    VisibleIndex="2" 
                    Width="14%"
                    ExportWidth="150"
                    Settings-HeaderFilterMode="CheckedList" 
                    Settings-AllowHeaderFilter="True" 
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Retail Price" 
                    FieldName="RetailPrice" 
                    VisibleIndex="3" 
                    Width="14%"
                    ExportWidth="150"
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Product Group" 
                    FieldName="PFC" 
                    VisibleIndex="4" 
                    Width="14%" 
                    ExportWidth="150"
                    Settings-HeaderFilterMode="CheckedList" 
                    Settings-AllowHeaderFilter="True" 
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Group Desciption" 
                    FieldName="PFCDescription" 
                    VisibleIndex="5" 
                    ExportWidth="150"
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center"
                    Width="14%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Product Family" 
                    FieldName="ProductGroup" 
                    VisibleIndex="6" 
                    ExportWidth="150"
                    CellStyle-HorizontalAlign="Center" 
                    HeaderStyle-HorizontalAlign="Center"
                    Width="14%" 
                    Settings-HeaderFilterMode="CheckedList" 
                    Settings-AllowHeaderFilter="True" >
                </dx:GridViewDataTextColumn>

            </Columns>

        </dx:ASPxGridView>
      
    </div>
    
    <asp:SqlDataSource ID="dsPartInfo" runat="server" SelectCommand="p_PartSearch" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sPartNumber" SessionField="PartNumber" Type="String"   />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridPartInfo">
    </dx:ASPxGridViewExporter>

</asp:Content>

