﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewComparisonReport.aspx.vb" Inherits="ViewComparisonReport" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel 
                    ID="lblPageTitle" 
                    runat="server" 
                    Font-Size="Larger"
                    Text="Sales Comparison Reporting"  />
       </div>
       
        <table id="trSelectionStuff"  style="position: relative; margin-bottom: 10px;">
            <tr id="Tr2" runat="server" style="height:30px;">
                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="lblType"
        runat="server" 
        Text="Month">
    </dx:ASPxLabel>
                </td>
    
                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="ASPxLabel1"
        runat="server" 
        Text="Compare">
    </dx:ASPxLabel>
                </td>

                <td style="width: 170px;" align="left" valign="bottom">
	                    <dx:ASPxLabel 
        id="ASPxLabel2"
        runat="server" 
        Text="Comparison Type">
    </dx:ASPxLabel>
	            </td>
    
                <td style="width: 170px;" align="left" valign="bottom">
                        <dx:ASPxLabel 
        id="ASPxLabel3"
        runat="server" 
        Text="Service Description">
    </dx:ASPxLabel>
                </td>

     <%--           <td style="width: 170px;" align="left" valign="bottom">
                    <dx:ASPxLabel 
                        id="ASPxLabel6"
                        runat="server" 
                        Text="Values To Report">
                    </dx:ASPxLabel>
                </td>--%>

                <td style="width: 170px;" align="left" valign="bottom">
                        <dx:ASPxLabel 
        id="ASPxLabel4"
        runat="server" 
        Text="Report By">
    </dx:ASPxLabel>
                </td>
        
                <td style="width: 170px;" align="left" valign="bottom">
                        <dx:ASPxLabel 
        id="ASPxLabel5"
        runat="server" 
        Text="Customers">
    </dx:ASPxLabel>
                </td>
            
                <td style="width: 170px;" align="left" valign="bottom">
                                         <dx:ASPxLabel 
        id="lblExDealers"
        runat="server" 
        Text="Show Ex-Dealers ?">
    </dx:ASPxLabel>
   
                </td>
            
            </tr>
        
            <tr id="Tr3" runat="server" style="height:30px;">
                
                <td style="width: 170px;" align="left" valign="top">
                     <dx:ASPxComboBox
                        ID="ddlFrom"
                        Width="150px"
                        runat="server" 
                        ValueField="ID"
                        TextField="PeriodName"
                        SelectedIndex="0"
                        DataSourceID="dsMonths"
                        AutoPostBack="True"/>
                   </td>
            
                <td style="width: 170px;" align="left" valign="top">

                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType1"
                        Width="150px"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Month on month" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Quarterly" Value="1"/>
                            <dx:ListEditItem Text="Yearly" Value="2"/>
                            <dx:ListEditItem Text="Year to date" Value="3"/>
                            <dx:ListEditItem Text="Financial YTD" Value="4"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                
                <td style="width: 170px;" align="left" valign="top">
                    <dx:ASPxComboBox
                        runat="server" 
                        Width="150px"
                        ID="ddlType2"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Consecutive" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Year on year" Value="1"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                
                <td style="width:170px" align="left" valign="top">
                      <dx:ASPxComboBox
                        runat="server" 
                        Width="150px"
                        ID="ddlTA"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Total Spend" Value="ALL" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="ACC"/>
                            <dx:ListEditItem Text="Additional Product" Value="OTH"/>
                            <dx:ListEditItem Text="Damage" Value="DAM"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="EXM"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="MEC"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="ROM"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                
        <%--        <td style="width:170px" align="left" valign="top">
                      <dx:ASPxComboBox
                        runat="server" 
                        Width="150px"
                        ID="ddlReportValue"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Invoiced Sale Value" Value="1" Selected="true" />
                            <dx:ListEditItem Text="Dealer Cost Value" Value="2"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>--%>
      
                <td style="width: 170px;" align="left" valign="top">
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType3"
                        Width="150px"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Zone" Value="0" />
                            <dx:ListEditItem Text="Dealer" Value="1" Selected="true" />
                            <dx:ListEditItem Text="Customer" Value="2" />
                            <dx:ListEditItem Text="Product Category" Value="3" />
                            <dx:ListEditItem Text="Product Group" Value="4" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>
                
                <td style="width: 170px;" align="left" valign="top">
                          <dx:ASPxComboBox
                        runat="server" 
                        Width="150px"
                        ID="ddlCustomerType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Focus only" Value="1" />
                            <dx:ListEditItem Text="Excluding Focus" Value="2"/>
                         </Items>
                    </dx:ASPxComboBox>           
                </td>

                <td style="width: 170px;"  align="left" valign="top">
                          <dx:ASPxComboBox
                        runat="server" 
                        Width="150px"
                        ID="ddlDealerType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Live Dealers Only" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Live and Ex Dealers" Value="1" />
                            <dx:ListEditItem Text="Ex Dealers Only" Value="2"/>
                         </Items>
                    </dx:ASPxComboBox>           
         
                    
                    
                 </td>
       


            </tr>

        </table>

        <dx:ASPxLabel 
             ID="lblExFocus" 
             runat="server" 
             Font-Italic="true"
             Text="Important:  Figures for Ex-Dealers can NOT be split into Focus and Non-Focus." />

        <dx:ASPxGridView 
            ID="gridSales" 
            runat="server" 
            onload="GridStyles"
            DataSourceID="dsViewSales"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            Visible="False"
            AutoGenerateColumns="False" 
            style="position: relative;" Width="100%" >
       
            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="True"
                ShowHeaderFilterBlankItems="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                ShowFooter="true"
                UseFixedTableLayout="True" />
     
            <SettingsBehavior 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Styles Footer-HorizontalAlign ="Center" />

            <SettingsPager PageSize="15">
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Columns>
                
                <dx:GridViewDataTextColumn 
                    Caption="" 
                    FieldName="Basis" 
                    VisibleIndex="0" 
                    Width="19%" 
                    ExportWidth="300">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left"/>
                </dx:GridViewDataTextColumn>
                
           
                <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndSale" HeaderStyle-Font-Size="Larger">
                         <Columns>
                            <dx:GridViewDataTextColumn 
                    Caption="Last Month" 
                    FieldName="Sales1" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    VisibleIndex="1" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="This Month" 
                    FieldName="Sales2" 
                    VisibleIndex="2" 
                    Width="9%" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="Variance" 
                    FieldName="SalesVariance" 
                    Name="SalesVariance" 
                    VisibleIndex="3" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                       </Columns>
                </dx:GridViewBandColumn>
       
                <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndCost" HeaderStyle-Font-Size="Larger">
                         <Columns>
                            <dx:GridViewDataTextColumn 
                    Caption="Last Month" 
                    FieldName="Cost1" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    VisibleIndex="1" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="This Month" 
                    FieldName="Cost2" 
                    VisibleIndex="2" 
                    Width="9%" 
                    PropertiesTextEdit-DisplayFormatString="c0"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="Variance" 
                    FieldName="CostVariance" 
                    Name="CostVariance" 
                    VisibleIndex="3" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                       </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center" Name="bndMargin" HeaderStyle-Font-Size="Larger">
                         <Columns>
                            <dx:GridViewDataTextColumn 
                    Caption="Last Month" 
                    FieldName="Margin1" 
                    PropertiesTextEdit-DisplayFormatString="0.00%"                     
                    VisibleIndex="1" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="This Month" 
                    FieldName="Margin2" 
                    VisibleIndex="2" 
                    Width="9%" 
                    PropertiesTextEdit-DisplayFormatString="0.00%"                     
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    Caption="Variance" 
                    FieldName="MarginVariance" 
                    Name="MarginVariance" 
                    VisibleIndex="3" 
                    Width="9%" 
                    ExportWidth="150">
                    <Settings AllowHeaderFilter="False" />
                    <PropertiesTextEdit DisplayFormatString="0.00%">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataTextColumn>
                       </Columns>
                </dx:GridViewBandColumn>
         
            
            </Columns>

            <TotalSummary>
                  <dx:ASPxSummaryItem DisplayFormat="c0" FieldName="Sales1" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c0" FieldName="Sales2" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName ="SalesVariance" SummaryType="Custom"  />
                  <dx:ASPxSummaryItem DisplayFormat="c0" FieldName="Cost1" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="c0" FieldName="Cost2" SummaryType="Sum" />
                  <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName ="CostVariance" SummaryType="Custom"  />
                  <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="Margin1" SummaryType="Custom" />
                  <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="Margin2" SummaryType="Custom" />
                  <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName ="MarginVariance" SummaryType="Custom"  />
            </TotalSummary>

        </dx:ASPxGridView>
     
    </div>
    
    <asp:SqlDataSource ID="dsViewSales" runat="server" SelectCommand="sp_SalesComparisonReport" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nToPeriod" SessionField="PRToPeriod" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nType1" SessionField="PRType1" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nType2" SessionField="PRType2" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nType3" SessionField="PRType3" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="sProductFamily" SessionField="ProductFamily" Type="String" Size="50"  />
            <asp:SessionParameter DefaultValue="" Name="nCustomerType" SessionField="CustomerType" Type="Int32" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="nExDealer_flag" SessionField="ExDealers_flag" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsMonths" 
        runat="server" 
        SelectCommand="p_Get_Months" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridSales">
    </dx:ASPxGridViewExporter>


</asp:Content>


