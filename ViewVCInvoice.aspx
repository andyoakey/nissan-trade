﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewVCInvoice.aspx.vb" Inherits="ViewVCInvoice" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxw" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <div style="font-family: 'Calibri,Verdana';">

            <table style="width: 100%; height:25px; margin-top:10px; border-style:solid">
                <tr>
                    <td>
                        <dxe:ASPxLabel ID="lbl1" runat="server" Text="Centre"  Font-Bold="true" />
                    </td>
                    <td style="width:30%">
                        <dxe:ASPxLabel ID="lbl2" runat="server" Text="Name" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lbl3" runat="server" Text="Invoice Number" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lbl4" runat="server" Text="Date"  Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lbl5" runat="server" Text="Dept" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lbl6" runat="server" Text="Sale Type" Font-Bold="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxLabel ID="lblDealerCode" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblCentreName" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblInvoiceNumber" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblInvoiceDate" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblDepartment" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblSaleType" runat="server" />
                    </td>
                </tr>
            </table>
        
            <table style="width: 100%; height:25px; margin-top:10px; border-style:solid">
                <tr>
                    <td style="width:25%">
                        <dxe:ASPxLabel ID="ASPxLabel1" runat="server" Text="Account Name"  Font-Bold="true" />
                    </td>
                    <td style="width:60%">
                        <dxe:ASPxLabel ID="ASPxLabel2" runat="server" Text="Address" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="ASPxLabel3" runat="server" Text="Code" Font-Bold="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxLabel ID="lblAccountName" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblAccountAddress" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblAccountCode" runat="server" />
                    </td>
                </tr>
            </table>


            <table style="width: 100%; height:25px; margin-top:10px; border-style:solid">
                <tr>
                    <td style="width:25%">
                        <dxe:ASPxLabel ID="ASPxLabel8" runat="server" Text="Company Name"  Font-Bold="true" />
                    </td>
                    <td style="width:60%">
                        <dxe:ASPxLabel ID="ASPxLabel9" runat="server" Text="Address" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="ASPxLabel10" runat="server" Text="Magic" Font-Bold="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxLabel ID="lblCompanyName" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblCompanyAddress" runat="server" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblCompanyMagicNumber" runat="server" />
                    </td>
                </tr>
            </table>

            <dxwgv:ASPxGridView Font-Size="12px" Font-Names="Calibri,Verdana" ID="gridInvoiceDetailParts"
                runat="server" Width="100%" DataSourceID="dsInvoiceDetailParts" AutoGenerateColumns="False" style="margin-top:10px">

                <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                    ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                    ShowStatusBar="Hidden" ShowTitlePanel="True" ShowFooter="True" />

                <SettingsText Title="Parts Sold" />

                <SettingsPager PageSize="16" Mode="ShowPager" AllButton-Visible="true">
                </SettingsPager>

                <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control"
                    AllowDragDrop="False" AllowGroup="False" />

                <Styles>
                    <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Footer Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Cell Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                </Styles>

                <Columns>
                    <dxwgv:GridViewDataTextColumn Name="FranchiseCode" Caption="Fran. Code" FieldName="FranchiseCode"
                        VisibleIndex="0" ExportWidth="100" Width="10%">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="PartNumber" Caption="Part" FieldName="PartNumber"
                        VisibleIndex="1" ExportWidth="100" Width="12%" ToolTip="Textlines">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="Description" Caption="Description" FieldName="Description"
                        VisibleIndex="2" ExportWidth="100" ToolTip="Textlines">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="Quantity" Caption="Quantity" FieldName="Quantity"
                        VisibleIndex="6" ExportWidth="150" Width="12%">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="PriceFileValue" Caption="TGB Retail" FieldName="PriceFileValue"
                        VisibleIndex="7" ExportWidth="150" Width="12%">
                        <PropertiesTextEdit DisplayFormatString="&#163;#,##0.00" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="RetailValue" Caption="Retail Value" FieldName="RetailValue"
                        VisibleIndex="8" ExportWidth="150" Width="12%">
                        <PropertiesTextEdit DisplayFormatString="&#163;#,##0.00" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="SaleValue" Caption="Sale Value" FieldName="SaleValue"
                        VisibleIndex="9" ExportWidth="150" Width="12%">
                        <PropertiesTextEdit DisplayFormatString="&#163;#,##0.00" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Name="DiscountPerc" Caption="Discount" FieldName="DiscountPerc"
                        VisibleIndex="10" ExportWidth="150" Width="12%">
                        <PropertiesTextEdit DisplayFormatString="#,##0.00%" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>

                <TotalSummary>
                    <dxwgv:ASPxSummaryItem FieldName="PriceFileValue" DisplayFormat="&#163;#,##0.00" SummaryType="Sum" ShowInColumn="PriceFileValue" />
                    <dxwgv:ASPxSummaryItem FieldName="RetailValue" DisplayFormat="&#163;#,##0.00" SummaryType="Sum" ShowInColumn="RetailValue" />
                    <dxwgv:ASPxSummaryItem FieldName="SaleValue" DisplayFormat="&#163;#,##0.00" SummaryType="Sum" ShowInColumn="SaleValue" />
                    <dxwgv:ASPxSummaryItem FieldName="DiscountPerc" DisplayFormat="#,##0.00%" SummaryType="Custom" ShowInColumn="DiscountPerc" />
                </TotalSummary>

            </dxwgv:ASPxGridView>

            <table style="width: 100%; height:25px; margin-top:10px; border-style:solid">
                <tr>
                    <td style="width:75%">
                        <dxe:ASPxLabel ID="ASPxLabel11" runat="server" Text="" />
                    </td>
                    <td style="width:20%">
                        <dxe:ASPxLabel ID="ASPxLabel12" runat="server" Text="Total" Font-Bold="true" />
                    </td>
                    <td>
                        <dxe:ASPxLabel ID="lblTotal" runat="server" Text="" Font-Bold="true" />
                    </td>
                </tr>
            </table>

        </div>

        <asp:sqldatasource id="dsInvoiceDetailParts" runat="server" selectcommand="p_VCInvoicePartsList" selectcommandtype="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="nEventId" SessionField="SearchEventId" Type="Int32" />
            </SelectParameters>
        </asp:sqldatasource>
    
    </form>
</body>
</html>
