﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report5.aspx.vb" Inherits="report5" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <dx:ASPxLabel 
            ID="lblPageTitle" 
            Font-Size="Larger"
            Text="Campaigns"
            Style="position: absolute; top: 5px;  z-index: 1;" runat="server">
        </dx:ASPxLabel>
 
<%--        <dx:ASPxLabel 
            Font-Size="18px" 
            Font-Names="Calibri,Verdana" 
            ID="lblSubTitle" 
            Text="Campaign Sales & Reward Summary YTD"
            Style="left: 125px; position: absolute; top: 5px; font-weight: 700; font-size: large; z-index: 1;" runat="server">
        </dx:ASPxLabel>--%>

        <%-- Icon Buttons --%>
        <dx:ASPxButton 
            ID="btn1" 
            BackColor="White"
            runat="server" 
            Style="left: 0px; position: absolute; top: 40px; z-index: 2;"
            EnableDefaultAppearance="false" 
            Text="" 
            CssClass="special" 
            HorizontalPosition="True"
            Border-BorderStyle="None" 
            Image-Url="~/Images2014/TradePriceSupport.png" 
            Image-UrlHottracked="~/Images2014/TradePriceSupport_Hover.png">
        </dx:ASPxButton>

<%--        <dx:ASPxButton ID="btn2" runat="server" Style="left: 0px; position: absolute; top: 160px; z-index: 2;"
            EnableDefaultAppearance="false"
            Text="" CssClass="special" Visible="False" Border-BorderStyle="None" Image-Url="~/Images2014/BrakingSalesCamp.png"
            Image-UrlHottracked="~/Images2014/BrakingSalesCamp_Hover.png">
        </dx:ASPxButton>

        <dx:ASPxButton ID="btn3" runat="server" Style="left: 0px; position: absolute; top: 280px; z-index: 2;" EnableDefaultAppearance="false" 
            Text="" CssClass ="special" Border-BorderStyle="None" Image-Url="~/Images2014/ClutchCampaign2.png"
            Image-UrlHottracked="~/Images2014/ClutchCampaign2_Hover.png"  >
        </dx:ASPxButton>

        <dx:ASPxButton ID="btn4" runat="server" Style="left: 0px; position: absolute; top: 400px; z-index: 2;" EnableDefaultAppearance="false" 
            Text="" CssClass ="special" Border-BorderStyle="None" Image-Url="~/Images2014/TradeChallenge2.png"
            Image-UrlHottracked="~/Images2014/TradeChallenge2_Hover.png"  >
        </dx:ASPxButton>--%>

  <%--      <dx:ASPxGridView ID="gridSummary" runat="server" Width="846px" AutoGenerateColumns="False"
            Style="left: 125px; position: absolute; top: 42px; z-index: 4" DataSourceID="dsCampaignSummary">

            <Settings
                ShowFooter="True" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded"
                UseFixedTableLayout="True" VerticalScrollableHeight="300"
                ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False"
                ShowFilterRowMenu="False" ShowHeaderFilterBlankItems="false" />

            <SettingsPager Visible="False">
            </SettingsPager>

           <Columns>

                <dx:GridViewDataTextColumn Caption="Product" FieldName="Promotion" VisibleIndex="0" Width="15%" ExportWidth="150">
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewBandColumn Caption="Q1 2016" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Units" FieldName="Q1_Units" VisibleIndex="1" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Reward Value" FieldName="Q1_Value" VisibleIndex="2" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Q2 2016" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Units" FieldName="Q2_Units" VisibleIndex="1" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Reward Value" FieldName="Q2_Value" VisibleIndex="2" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Q3 2016" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Units" FieldName="Q3_Units" VisibleIndex="1" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Reward Value" FieldName="Q3_Value" VisibleIndex="2" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Q4 2016" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Units" FieldName="Q4_Units" VisibleIndex="1" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Reward Value" FieldName="Q4_Value" VisibleIndex="2" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Totals YTD" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Units" FieldName="Total_Units" VisibleIndex="1" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Reward Value" FieldName="Total_Value" VisibleIndex="2" ExportWidth="120">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

            </Columns>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <TotalSummary>
                <dx:ASPxSummaryItem FieldName="Q1_Units" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Q1_Units" />
                <dx:ASPxSummaryItem FieldName="Q1_Value" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Q1_Value" />
                <dx:ASPxSummaryItem FieldName="Q2_Units" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Q2_Units" />
                <dx:ASPxSummaryItem FieldName="Q2_Value" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Q2_Value" />
                <dx:ASPxSummaryItem FieldName="Q3_Units" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Q3_Units" />
                <dx:ASPxSummaryItem FieldName="Q3_Value" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Q3_Value" />
                <dx:ASPxSummaryItem FieldName="Q4_Units" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Q4_Units" />
                <dx:ASPxSummaryItem FieldName="Q4_Value" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Q4_Value" />
                <dx:ASPxSummaryItem FieldName="Total_Units" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Total_Units" />
                <dx:ASPxSummaryItem FieldName="Total_Value" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Total_Value" />
            </TotalSummary>

        </dx:ASPxGridView>--%>

    </div>

    <asp:SqlDataSource ID="dsCampaignSummary" runat="server" SelectCommand="p_Campaigns_2016_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridSummary" 
        PreserveGroupRowStates="False">
    </dx:ASPxGridViewExporter>

</asp:Content>


