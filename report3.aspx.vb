﻿Imports System.Data
Imports DevExpress.XtraCharts

Partial Class report3

    Inherits System.Web.UI.Page

    Dim sErrorMessage As String = ""

    Dim dsGraph As DataSet
    Dim dsSummary As DataSet
    Dim dsBreakdown As DataSet
    Dim da As New DatabaseAccess
    Dim htin As New Hashtable
    Dim htinBreakdown As New Hashtable
    Dim htinSummary As New Hashtable
    'Dim nThisYear As Integer = CurrentYear()
    Dim nThisYear As Integer = GetDataDecimal("SELECT CalendarYear FROM DataPeriods WHERE Id = (SELECT MAX(Id) +1 FROM DataPeriods WHERE PeriodStatus = 'C')")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Reports"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Reports", "Reports")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        'btn6.Visible = (Session("SelectionLevel") = "C")
        Call ChartLoad()

        Dim nButtonHeight As Integer = Session("ButtonHeight")
        Dim nButtonWidth As Integer = 100

        btn1.Image.Width = nButtonWidth
        btn2.Image.Width = nButtonWidth
        btn3.Image.Width = nButtonWidth
        btn4.Image.Width = nButtonWidth
        btn5.Image.Width = nButtonWidth
        btn6.Image.Width = nButtonWidth
        btn7.Image.Width = nButtonWidth
        btn8.Image.Width = nButtonWidth
        btn9.Image.Width = nButtonWidth



    End Sub

    Protected Sub IconButtons_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click, btn7.Click, btn8.Click, btn9.Click, btn6.Click

        Select Case sender.id
            Case "btn1"
                Response.Redirect("ViewSalesbyPFC.aspx")
            Case "btn2"
                Response.Redirect("PFCPerformance.aspx")
            Case "btn3"
                Response.Redirect("ViewBestSellingParts.aspx")
            Case "btn4"
                Response.Redirect("ViewComparisonReport.aspx")
            Case "btn5"
                Response.Redirect("ServiceKitYOY.aspx")
            Case "btn6"
                Response.Redirect("ServiceKitVA.aspx")
            Case "btn7"
                Response.Redirect("CoreStockReport.aspx")
            Case "btn8"
                Response.Redirect("VAReport.aspx")
            Case "btn9"
                'Response.Redirect("RemanSales.aspx")
                Response.Redirect("RemanReport.aspx")   ' Changed 27/09/17 to match VA Report
        End Select
    End Sub

    Sub ChartLoad()

        Dim i As Integer
        Dim nThisValue As Integer
        Dim myPalette As New Palette("ChartPalette")

        While WebChartControl1.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End While

                Dim series1 As New Series
        Dim view1 As New SideBySideBarSeriesView

        series1.ArgumentScaleType = ScaleType.Qualitative
        series1.Label.Visible = True
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.FixedPoint
        series1.PointOptions.ValueNumericOptions.Precision = 0

        series1.View = view1
        series1.LegendText = Trim(Str(nThisYear - 1))

        Dim series2 As New Series
        Dim view2 As New SideBySideBarSeriesView

        series2.ArgumentScaleType = ScaleType.Qualitative
        series2.Label.Visible = True
        series2.PointOptions.ValueNumericOptions.Format = NumericFormat.FixedPoint
        series2.PointOptions.ValueNumericOptions.Precision = 0
        series2.View = view2
        series2.LegendText = Trim(Str(nThisYear))

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nYear", nThisYear)
        htin.Add("@nType", 1)

        dsGraph = da.Read(sErrorMessage, "p_ServiceKitData", htin)
        With dsGraph.Tables(0).Rows(0)

            For i = 1 To 12
                nThisValue = Val(.Item("LastYear_" & Trim(Str(i))))
                series1.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
                nThisValue = Val(.Item("ThisYear_" & Trim(Str(i))))
                series2.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
            Next i

        End With

        WebChartControl1.Series.Add(series1)
        WebChartControl1.Series.Add(series2)

        Dim diagram As XYDiagram = CType(WebChartControl1.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Month"
        diagram.AxisX.Title.Visible = True
        diagram.AxisX.Range.SideMarginsEnabled = True

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Total Kits"
        diagram.AxisY.Title.Visible = True
        diagram.AxisY.Range.SideMarginsEnabled = True
        diagram.AxisY.NumericOptions.Format = NumericFormat.FixedPoint
        diagram.AxisY.NumericOptions.Precision = 0

        WebChartControl1.Legend.Visible = True

        WebChartControl1.Visible = True

    End Sub

End Class
