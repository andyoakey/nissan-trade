﻿Imports System.Data

Partial Class NationwideAssign

    Inherits System.Web.UI.Page

    Dim sDealerCode As String, sInvoiceNumber As String
    Dim da As New DatabaseAccess, ds As DataSet, dsC As DataSet, sErr, sSQL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Nationwide Assign"

    End Sub

    Private Sub NationwideAssign_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete


        '  Hide all Master Page stuff EXCEPT back button, so only way out is by pressing back button ( or closing browser window which does the same thing)
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = False

        Dim master_btnBack As DevExpress.Web.ASPxButton
        master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
        master_btnBack.Visible = True

        Dim master_menu As DevExpress.Web.ASPxMenu
        master_menu = CType(Master.FindControl("ASPxMenu1"), DevExpress.Web.ASPxMenu)
        master_menu.Visible = False

        Dim master_btnMastCustSeach As DevExpress.Web.ASPxButton
        master_btnMastCustSeach = CType(Master.FindControl("btnMastCustSeach"), DevExpress.Web.ASPxButton)
        master_btnMastCustSeach.Visible = False

        Dim master_ddlAccessPoint As DevExpress.Web.ASPxComboBox
        master_ddlAccessPoint = CType(Master.FindControl("ddlAccessPoint"), DevExpress.Web.ASPxComboBox)
        master_ddlAccessPoint.Visible = False

        If Session("BackClicked") = True Then
            Session("BackClicked") = False
            If Session("ReturnProgram") Is Nothing Then
                Response.Redirect("NationwideAccountSearch.aspx")
            Else
                Response.Write("<script language='javascript'> { window.close();}</script>")
            End If
        End If
        '  End Handle Master Page Button Clicks

        Session("ThisCustomerDealerID") = Request.QueryString.ToString

        Session("ThisSiteID") = GetDataDecimal("Select siteid from NationwideCustomerDealer where customerdealerid = " & Session("ThisCustomerDealerID"))


    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub ASPxGridView1_DataBound(ByVal sender As Object, ByVal e As EventArgs) Handles gridSites.DataBound
        Dim gridView As DevExpress.Web.ASPxGridView = TryCast(sender, DevExpress.Web.ASPxGridView)
        If Session("ThisSiteID") Is Nothing Or Session("ThisSiteID") = 0 Then
        Else
            gridView.Selection.SelectRow(Session("ThisSiteID") - 1)
        End If
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        Dim sSql As String = "sp_Insert_NationwideCustomerDealer " & Session("ThisCustomerDealerID") & "," & Session("ThisSiteID")
        RunNonQueryCommand(sSql)
        Response.Write("<script language='javascript'> { window.close();}</script>")
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Dim sSql As String = "Insert Nationwide_DoNotInclude (customerdealerid) values (" & Session("ThisCustomerDealerID") & ")"
        RunNonQueryCommand(sSql)
        Response.Write("<script language='javascript'> { window.close();}</script>")
    End Sub

    Private Sub gridSites_SelectionChanged(sender As Object, e As EventArgs) Handles gridSites.SelectionChanged
        If sender.GetSelectedFieldValues("siteid").count > 0 Then
            Session("ThisSiteID") = sender.GetSelectedFieldValues("siteid")(0)
        Else
            Session("ThisSiteID") = 0
        End If

    End Sub
End Class
