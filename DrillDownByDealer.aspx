 <%@ Page AutoEventWireup="false" CodeFile="DrillDownByDealer.aspx.vb" Inherits="DrillDownByDealer"    Language="VB" MasterPageFile="~/MasterPage.master" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div id="divTitle" style="position:relative; top:0px">
       <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Drill Down By Dealer" />
    </div>

    <br />

    <div id="divMain" style="position:relative; top:0px">
          <table id="trSelectionStuff" Width="100%" style="position:relative; margin-bottom: 10px;">
          
          <tr id="Tr2" runat="server" >
                <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblFrom"
                            runat="server" 
                            Text="Month From">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblTo"
                            runat="server" 
                            Text="Month To">
                    </dx:ASPxLabel>
                </td>

                 <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblTradeAnalysisGroup"
                            runat="server" 
                            Text="Trade Analysis Group">
                    </dx:ASPxLabel>
                </td>
                
                 <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblProductGroup"
                            runat="server" 
                            Text="Product Group">
                    </dx:ASPxLabel>
                </td>

                 <td align="left" width="44%" >
                 </td>

                </tr>

                <tr id="Tr1" runat="server" >

                    <td align="left" width="14%" valign="top" >
                         <dx:ASPxComboBox
                                ID="ddlFrom" 
                                runat="server" 
                                AutoPostBack="True">
                          </dx:ASPxComboBox>
                     </td>

                    <td align="left" width="14%" valign="top">
                         <dx:ASPxComboBox
                                ID="ddlTo" 
                                runat="server" 
                                AutoPostBack="True">
                        </dx:ASPxComboBox>
                    </td>

                <td align="left" >
                    <dx:ASPxComboBox
                        ID="ddlTA"
                        ValueField="TradeAnalysis"
                        TextField="TradeAnalysis"
                        runat="server" 
                        DataSourceID="dsTradeAnalysis"
                        SelectedIndex="0"
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                </td>

            <td align="left" >
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlProductGroup"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All" Value="ALL" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="ACC"/>
                            <dx:ListEditItem Text="Damage" Value="DAM"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="EXM"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="MEC"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="ROM"/>
                            <dx:ListEditItem Text="Additional Product" Value="OTH"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                    <td align="left" width="44%" >
                    </td>


            </tr>
        </table>
    </div>

    <dxwtl:ASPxTreeList 
        ID="tlDealerDrillDown" 
        runat="server" 
        AutoGenerateColumns="False"  
        Styles-AlternatingNode-BackColor="WhiteSmoke"
        DataSourceID="dsDealerDrillDown" 
        Settings-GridLines="None"
        KeyFieldName="ID" 
        Width = "1000px"
        ParentFieldName="parent_ID" >

       <Settings SuppressOuterGridLines="False"  GridLines="Both"  />
        
       <SettingsBehavior AllowDragDrop="False" AllowSort="False" />

        
       <Columns>
  
       
            
                <dxwtl:TreeListDataColumn 
                FieldName="dataperiod_id" 
                Visible="False" 
                VisibleIndex="0" >
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
                <GroupFooterCellStyle HorizontalAlign="Center">
                </GroupFooterCellStyle>
                <FooterCellStyle HorizontalAlign="Center">
                </FooterCellStyle>
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                FieldName="treelevel" 
                Visible="False" 
                VisibleIndex="0">
                <CellStyle HorizontalAlign="Center">
                </CellStyle>
                <GroupFooterCellStyle HorizontalAlign="Center">
                </GroupFooterCellStyle>
                <FooterCellStyle HorizontalAlign="Center">
                </FooterCellStyle>
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                Caption="Region/Zone/Dealer" 
                FieldName="nodename" 
                ExportWidth="350"
                VisibleIndex="0">
                <HeaderStyle Wrap="True" />
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                FieldName="totalsalesthis" 
                ExportWidth="150"
                VisibleIndex="1" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle Wrap="True" />
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                DisplayFormat="&#163;##,##0"
                ExportWidth="150"
                FieldName="totalsaleslast" 
                VisibleIndex="2" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle Wrap="True" />
            </dxwtl:TreeListDataColumn>
  
            <dxwtl:TreeListDataColumn 
                Caption="YoY Variance" 
                ExportWidth="150"
                FieldName="yonysalesabs" 
                DisplayFormat="&#163;##,##0"
                VisibleIndex="3" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle Wrap="True" />
            </dxwtl:TreeListDataColumn>
            
           <dxwtl:TreeListDataColumn 
                Caption="YoY % Variance" 
                ExportWidth="150"
                DisplayFormat="{0:###.00%}"
                FieldName="yonysalespc" 
                Name="yonysalespc" 
                VisibleIndex="4" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle Wrap="True" />
            </dxwtl:TreeListDataColumn>

        </Columns>
    </dxwtl:ASPxTreeList>
    
    <asp:SqlDataSource 
        ID="dsDealerDrillDown" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_FullDrillDown" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nfrom" SessionField="nfrom" Type="Int32" />
            <asp:SessionParameter Name="nto" SessionField="nTo" Type="Int32" />
            <asp:SessionParameter Name="nProductLevel" SessionField="nProductLevel" Type="Int32" />
            <asp:SessionParameter Name="sTradeAnalysis" SessionField="TA" Type="String" Size="50"  />
            <asp:SessionParameter Name="slevel4_code" SessionField="Level4_code" Type="String" Size="3"/>
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dxwtl:ASPxTreeListExporter 
        ID="ASPxTreeListExporter1" 
        runat="server" 
        TreeListID="tlDealerDrillDown">
    </dxwtl:ASPxTreeListExporter>

    <asp:SqlDataSource 
        ID="dsServiceTypes" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        runat="server" 
        SelectCommand="sp_EuroFamilyList"
        SelectCommandType="StoredProcedure" />
     
    <asp:SqlDataSource 
        ID="dsProductGroups" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        runat="server" 
        SelectCommand="sp_Level2_List"
        SelectCommandType="StoredProcedure" /> 
 
    <asp:SqlDataSource 
        ID="dsParts" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        runat="server" 
        SelectCommand="sp_PartsSold"
        SelectCommandType="StoredProcedure" />
 
    
    <asp:SqlDataSource 
        ID="dsTradeAnalysis" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeAnalysisProducts" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    
</asp:Content>

