﻿Imports System.Data
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports DevExpress.Web
Imports DevExpress.Export

Partial Class MCChall2011Breakdown

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "NissanDATA PARTS - Mechanical Challenge 2015 - Centre Breakdown"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        lblCentreDetails.Text = "Mechanical Challenge 2015 Breakdown : " & Request.QueryString(0) & " " & GetDataString("SELECT CentreName FROM CentreMaster WHERE DealerCode = '" & Request.QueryString(0) & "'")

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsCampaigns_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns.Init
        Session("MCDealerCode") = Request.QueryString(0)
        dsCampaigns.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCampaignsSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaignsSummary.Init
        Session("MCDealerCode") = Request.QueryString(0)
        dsCampaignsSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared

        If e.DataColumn.Index = 2 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 3 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 4 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 5 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 6 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 7 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If

    End Sub

    Protected Sub Summary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.Index = 1 Then
            If e.GetFieldValue("RowDescription") = "Achievement" Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "£#,##0")
            End If
        End If

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("MCDealerCode") = Nothing
    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.FileName = "MechanicalChallenge_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

End Class

