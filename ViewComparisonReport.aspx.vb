﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewComparisonReport

    Inherits System.Web.UI.Page
    Dim nSales1 As Decimal = 0
    Dim nSales2 As Decimal = 0
    Dim nCost1 As Decimal = 0
    Dim nCost2 As Decimal = 0

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Sales Comparison Reporting"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            '  Call GetMonthsWholeDevEx(ddlFrom)

            Session("PRToPeriod") = GetDataDecimal("select dbo.getMaxInvoiceDataPeriod()")    ' This returns the max month for which we have data in web_qube  - Important on first day of month when no data for this month loaded yet.

            Session("PRType1") = 0
            Session("PRType2") = 0
            Session("PRType3") = 1    ' Default to Dealer
            Session("ProductFamily") = "ALL"
            Session("CustomerType") = 0
            Session("ExDealers_flag") = 0  ' Defaults to Live Dealers Only
            gridSales.DataBind()
            gridSales.Visible = True

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Comparison Report", "Viewing Comparison Report")
        End If

        lblExFocus.Visible = False


        If Session("UserLevel") = "C" Or Session("UserLevel") = "G" Then  '  Hide Ex Dealers Option
            Me.lblExDealers.Visible = False
            Me.ddlDealerType.Visible = False
            lblExFocus.Visible = False
        Else
            Me.lblExDealers.Visible = True
            Me.ddlDealerType.Visible = True
            If Session("CustomerType") > 0 Or Session("ExDealers_flag") > 0 Then
                lblExFocus.Visible = True
            End If
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub gridSales_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridSales.DataBound

        Select Case Session("PRType1")

            Case 0
                gridSales.Columns("Sales2").Caption = GetPeriodName(CInt(Session("PRToPeriod")))
                If Session("PRType2") = 0 Then
                    gridSales.Columns("Sales1").Caption = GetPeriodName(CInt(Session("PRToPeriod")) - 1)
                Else
                    gridSales.Columns("Sales1").Caption = GetPeriodName(CInt(Session("PRToPeriod")) - 12)
                End If

            Case 1
                gridSales.Columns("Sales2").Caption = "Current 3M"
                If Session("PRType2") = 0 Then
                    gridSales.Columns("Sales1").Caption = "Previous 3M"
                Else
                    gridSales.Columns("Sales1").Caption = "Same 3M last year"
                End If

            Case 2
                gridSales.Columns("Sales1").Caption = "Current 12M"
                If Session("PRType2") = 0 Then
                    gridSales.Columns("Sales1").Caption = "Previous 12M"
                Else
                    gridSales.Columns("Sales1").Caption = "Same 12M last year"
                End If

            Case 3
                gridSales.Columns("Sales2").Caption = "Current YTD"
                gridSales.Columns("Sales1").Caption = "YTD last year"

            Case 4
                gridSales.Columns("Sales2").Caption = "Current Financial YTD"
                gridSales.Columns("Sales1").Caption = "Financial YTD last year"

            Case Else

        End Select

        gridSales.Columns("Cost1").Caption = gridSales.Columns("Sales1").Caption
        gridSales.Columns("Cost2").Caption = gridSales.Columns("Sales2").Caption
        gridSales.Columns("Margin1").Caption = gridSales.Columns("Sales1").Caption
        gridSales.Columns("Margin2").Caption = gridSales.Columns("Sales2").Caption

        Select Case CInt(Session("PRType3"))

            Case 0
                gridSales.Columns(0).Caption = "Zone"
            Case 1
                gridSales.Columns(0).Caption = "Dealer"
            Case 2
                gridSales.Columns(0).Caption = "Customer"
            Case 3
                gridSales.Columns(0).Caption = "Product Category"
            Case 4
                gridSales.Columns(0).Caption = "Product Group"

        End Select

        Dim sServiceTypeText As String = ddlTA.SelectedItem.Text

        gridSales.Columns("bndSale").Caption = "Invoiced Value (" & sServiceTypeText & ")"
        gridSales.Columns("bndCost").Caption = "Dealer Cost (" & sServiceTypeText & ")"
        gridSales.Columns("bndMargin").Caption = "Margin (" & sServiceTypeText & ")"



    End Sub

    Protected Sub gridSales_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSales.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0
        Dim sThisDealerCode As String


        If e.DataColumn.Caption = "" Then
            e.Cell.Width = 2
        End If

        If e.DataColumn.Caption = "Variance" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.Font.Bold = True
            e.Cell.ForeColor = System.Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.ForeColor = System.Drawing.Color.Black
            ElseIf nThisValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            Else
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

        If e.DataColumn.Caption = "Dealer" Then
            sThisDealerCode = Left(e.CellValue, 4)
            If isExDealer(sThisDealerCode) Then
                e.Cell.BackColor = System.Drawing.Color.Yellow
            End If
        End If

    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "Sales1" Or e.Column.FieldName = "Sales2" Then
            e.DisplayText = Format(e.Value, "£#,##0")
        End If
        If e.Column.FieldName = "Variance" Then
            e.DisplayText = Format(e.Value, "#,##0.00") & "%"
        End If

    End Sub

    Protected Sub dsViewSales_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSales.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        dsViewSales.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged, ddlType1.SelectedIndexChanged, ddlType2.SelectedIndexChanged, ddlType3.SelectedIndexChanged, ddlTA.SelectedIndexChanged  ', ddlReportValue.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Private Sub RefreshGrid()
        Session("PRToPeriod") = ddlFrom.SelectedItem.Value
        Session("PRType1") = ddlType1.SelectedItem.Value
        Session("PRType2") = ddlType2.SelectedItem.Value
        Session("PRType3") = ddlType3.SelectedItem.Value
        Session("ProductFamily") = ddlTA.SelectedItem.Value
        Session("FocusType") = ddlCustomerType.Value
        'Session("ValueType") = ddlReportValue.Value

        gridSales.DataBind()
        gridSales.Visible = True
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Call GlobalRenderBrick(e)

        Dim nThisValue As Decimal = 0

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            If e.Column.Name = "Sales1" Or e.Column.Name = "Sales2" Then
                e.TextValueFormatString = "c0"
            End If

            If e.Column.Name = "Variance" Then
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value)
                End If
                e.TextValue = Format(e.Value, "#,##0.00") & "%"
                If nThisValue = 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.White
                    e.BrickStyle.ForeColor = System.Drawing.Color.Black
                ElseIf nThisValue > 0 Then
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                    e.BrickStyle.BackColor = System.Drawing.Color.Green
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                End If
            End If

        End If

    End Sub

    Protected Sub ddlCustomerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCustomerType.SelectedIndexChanged
        Session("CustomerType") = ddlCustomerType.SelectedIndex
    End Sub

    Protected Sub ddlDealerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDealerType.SelectedIndexChanged
        Session("ExDealers_flag") = ddlDealerType.SelectedIndex
    End Sub

    Protected Sub btnExcel_Click()

        ASPxGridViewExporter1.FileName = "SalesComparisonReport"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub dsMonths_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsMonths.Init
        dsMonths.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Public Function isExDealer(sDealerCode As String) As Boolean
        ' Returns true if the dealer is no longer on the scheme
        Return DataExists("Select * from v_ExCentreMaster where dealercode ='" & sDealerCode & "'")

    End Function

    Protected Sub grid_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridSales.CustomSummaryCalculate

        Dim Margin1 As Decimal = 0
        Dim Margin2 As Decimal = 0


        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nSales1 = 0
            nSales2 = 0
            nCost1 = 0
            nCost2 = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nSales1 = nSales1 + e.GetValue("Sales1")
            nSales2 = nSales2 + e.GetValue("Sales2")
            nCost1 = nCost1 + e.GetValue("Cost1")
            nCost2 = nCost2 + e.GetValue("Cost2")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "SalesVariance" Then
                If nSales1 > 0 And nSales2 > 0 Then
                    e.TotalValue = GetDataDecimal("select dbo.PercChange(" & nSales2 & "," & nSales1 & ")")
                End If
            End If

            If objSummaryItem.FieldName = "CostVariance" Then
                If nCost1 > 0 And nCost2 > 0 Then
                    e.TotalValue = GetDataDecimal("select dbo.PercChange(" & nCost2 & "," & nCost1 & ")")
                End If
            End If

            If objSummaryItem.FieldName = "Margin1" Then
                If nSales1 > 0 And nCost1 > 0 Then
                    Margin1 = GetDataDecimal("select dbo.PercChange(" & nSales1 & "," & nCost1 & ")")
                    e.TotalValue = Margin1
                End If
            End If

            If objSummaryItem.FieldName = "Margin2" Then
                If nSales2 > 0 And nCost2 > 0 Then
                    Margin2 = GetDataDecimal("select dbo.PercChange(" & nSales2 & "," & nCost2 & ")")
                    e.TotalValue = Margin2
                End If
            End If

            If objSummaryItem.FieldName = "MarginVariance" Then
                e.TotalValue = GetDataDecimal("select dbo.PercChange(" & nSales2 & "," & nCost2 & ")") - GetDataDecimal("select dbo.PercChange(" & nSales1 & "," & nCost1 & ")")
            End If

        End If

    End Sub

End Class
