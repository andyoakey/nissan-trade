﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports System.Data
Imports DevExpress.Web

Partial Class ViewSalesbyPFC

    Inherits System.Web.UI.Page

    Dim nTotalSale As Decimal
    Dim nTotalCost As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Spend Analysis by PFC"
        Session("ReturnProgram") = "ViewSalesbyPFC.aspx"

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Not IsPostBack Then
            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
            Session("AnalysisType") = 1
            Session("MonthFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus <> 'C'")
            Session("MonthTo") = Session("MonthFrom")
            Session("FocusType") = 0
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales by PFC", "Viewing Sales by PFC")
        End If

        If ddlTA.Value = 3 Then
            gridSales.Columns("code").Caption = "Analysis Group"
            gridSales.Columns("description").Caption = "Analysis Line"
        Else
            gridSales.Columns("code").Caption = "Code"
            gridSales.Columns("description").Caption = "Description"
        End If

        If Session("AnalysisType") = 2 Then
            gridSales.Columns("code").Visible = False
        Else
            gridSales.Columns("code").Visible = True
        End If

    End Sub

    Protected Sub dsViewSales_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSales.Init
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        dsViewSales.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsViewSalesLevel1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel1.Init
        dsViewSalesLevel1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsViewSalesLevel2_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel2.Init
        dsViewSalesLevel2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub
    Protected Sub dsViewSalesLevel3_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel3.Init
        dsViewSalesLevel3.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridDrillDown_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("ReportIdentifier") = sKey
    End Sub

    Protected Sub gridDrillDown2_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("PFC") = sKey
    End Sub

    Protected Sub gridDrillDown3_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("PartNumber") = sKey
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "SaleValue" Or e.Column.FieldName = "CostValue" Then
            e.DisplayText = Format(e.Value, "£#,##0")
        End If
        If e.Column.FieldName = "Quantity" Then
            e.DisplayText = Format(e.Value, "#,##0")
        End If
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub ddlTA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged
        Session("AnalysisType") = ddlTA.Value
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub gridSales_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridSales.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nTotalSale = 0
            nTotalCost = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nTotalSale = nTotalSale + e.GetValue("salevalue")
            nTotalCost = nTotalCost + e.GetValue("costvalue")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            If nTotalSale <> 0 And nTotalCost <> 0 Then
                e.TotalValue = ((nTotalSale - nTotalCost) / nTotalSale)
            Else
                e.TotalValue = 0
            End If
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("FocusType") = ddlType.Value
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "SalesReporting"
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "invoicenumber").ToString()
        Dim dealerCode = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "dealercode").ToString()

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub


End Class


