﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraCharts
Imports DevExpress.XtraCharts.Web

Partial Class TCSpendComparison

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Trade Club Member Spends"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            ddlYear.Items.Add("2011")
            ddlYear.Items.Add("2010")
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "TC Member Spends by Month", "Viewing Trade Club Member Spends By Month")
        End If

    End Sub

    Protected Sub dsSpendByTA_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSpendsByTA.Init
        If Session("TCYear") Is Nothing Then
            Session("TCYear") = 2011
        End If
        If Session("TradeAnalysis") Is Nothing Then
            Session("TradeAnalysis") = "ALL"
        End If
        dsSpendsByTA.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridSpendByTA_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 0 Then
            e.DisplayText = NicePeriodName(e.Value)
        End If
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("TCYear") = ddlYear.SelectedItem.Text
    End Sub

    Sub ChartLoad()

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable
        Dim i As Integer
        Dim nThisValue As Integer

        While WebChartControl1.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End While

        If WebChartControl1.Titles.Count = 0 Then
            Dim webTitle As New ChartTitle
            webTitle.Text = "Trade Club Spends Vs Non-Trade Club Spends by Month"
            WebChartControl1.Titles.Add(webTitle)
        End If

        Dim series1 As New Series
        Dim view1 As New SideBySideBarSeriesView

        series1.ArgumentScaleType = ScaleType.Qualitative
        series1.Label.Visible = True
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Currency
        series1.View = view1
        series1.LegendText = "Non-TC"

        Dim series2 As New Series
        Dim view2 As New SideBySideBarSeriesView

        series2.ArgumentScaleType = ScaleType.Qualitative
        series2.Label.Visible = True
        series2.PointOptions.ValueNumericOptions.Format = NumericFormat.Currency
        series2.View = view2
        series2.LegendText = "Trade Club"

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nYear", Session("TCYear"))
        htin.Add("@sTypeOfParts", Session("TradeAnalysis"))

        ds = da.Read(sErrorMessage, "p_TC_Spend_Comparison", htin)
        With ds.Tables(0).Rows(0)

            For i = 1 To 12
                nThisValue = Val(.Item("Month" & Trim(Str(i) & "_Diff")))
                series1.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
                nThisValue = Val(.Item("Month" & Trim(Str(i) & "_TC")))
                series2.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
            Next i

        End With

        WebChartControl1.Series.Add(series1)
        WebChartControl1.Series.Add(series2)

        Dim diagram As XYDiagram = CType(WebChartControl1.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Month"
        diagram.AxisX.Title.Visible = True
        diagram.AxisX.Range.SideMarginsEnabled = True

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Total Sales £"
        diagram.AxisY.Title.Visible = True
        diagram.AxisY.Range.SideMarginsEnabled = True
        diagram.AxisY.NumericOptions.Format = NumericFormat.Currency

        WebChartControl1.Legend.Visible = True

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Call ChartLoad()
    End Sub

    Protected Sub ddlTA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged
        Session("TradeAnalysis") = ddlTA.SelectedValue
    End Sub

End Class
