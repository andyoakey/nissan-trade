﻿
Partial Class FullCompanyExport

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If
    End Sub

    Protected Sub ExportToExcel()

        Dim sFileName As String = "FullCompanyExport"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)

    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilters.Click
        gridFullCompanyExport.FilterExpression = ""
    End Sub

End Class
