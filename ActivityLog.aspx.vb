﻿Partial Class ActivityLog

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Activity Log"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call GetMonthsAll(ddlFrom)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Activity Log", "Viewing Activity Log")
        End If

    End Sub

    Protected Sub dsActivityLog_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsActivityLog.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE StartDate <= GetDate()")
        End If
        dsActivityLog.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.SelectedValue
    End Sub

End Class
