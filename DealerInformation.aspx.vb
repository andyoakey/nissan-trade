﻿Imports System.Data
Imports System.Net
Imports System.Net.Mail
Imports System.Collections

Partial Class DealerInformation

    Inherits System.Web.UI.Page
    Public ThisDealerCode As String
    Public ThisDealerName As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If
    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Dealer Information"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Session("SelectionLevel") <> "C" Then
            lblCentreError.Visible = True
            lblPageTitle.Visible = False
            formData.Visible = False
        Else
            lblCentreError.Visible = False
            lblPageTitle.Visible = True
            formData.Visible = True
            ThisDealerCode = Trim(Session("SelectionId"))
            ThisDealerName = Trim(GetDataString("Select centrename From CentreMaster where dealercode = '" & ThisDealerCode & "'"))
            lblPageTitle.Text = "Dealer Information for " & ThisDealerName & " (" & ThisDealerCode & ")"

            Call LoadData()
        End If

    End Sub

    Sub LoadData()
        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@DealerCode", ThisDealerCode)

        ds = da.Read(sErrorMessage, "sp_SelectCentreMaster", htin)
        With ds.Tables(0).Rows(0)
            txtDealerName.Text = .Item("centrename").ToString
            txtMarketingName.Text = .Item("marketingname").ToString
            txtTel.Text = .Item("telephonenumber").ToString
            txtFax.Text = .Item("faxnumber").ToString
            txtAddress1.Text = .Item("address1").ToString
            txtAddress2.Text = .Item("address2").ToString
            txtAddress3.Text = .Item("address3").ToString
            txtAddress4.Text = .Item("address4").ToString
            txtPostCode.Text = .Item("postcode").ToString
            txtContact.Text = .Item("contact").ToString
            txtPosition.Text = .Item("position").ToString
            txtEmail.Text = .Item("email").ToString
            txtOpeningHours.Text = .Item("openinghours").ToString
        End With

    End Sub

    Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim sSQL As String = " EXEC sp_UpdateCentreMaster "
        sSQL = sSQL & "'" & Trim(Session("SelectionId")) & "',"
        sSQL = sSQL & "'" & txtDealerName.Text & "',"
        sSQL = sSQL & "'" & txtMarketingName.Text & "',"
        sSQL = sSQL & "'" & txtTel.Text & "',"
        sSQL = sSQL & "'" & txtFax.Text & "',"
        sSQL = sSQL & "'" & txtContact.Text & "',"
        sSQL = sSQL & "'" & txtPosition.Text & "',"
        sSQL = sSQL & "'" & txtEmail.Text & "',"
        sSQL = sSQL & "'" & txtAddress1.Text & "',"
        sSQL = sSQL & "'" & txtAddress2.Text & "',"
        sSQL = sSQL & "'" & txtAddress3.Text & "',"
        sSQL = sSQL & "'" & txtAddress4.Text & "',"
        sSQL = sSQL & "'" & txtPostCode.Text & "',"
        sSQL = sSQL & "'" & txtOpeningHours.Text & "'"


        Call RunNonQueryCommand(sSQL)

    End Sub

    Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("Report4.aspx")
    End Sub


End Class
