﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MCChall.aspx.vb" Inherits="MCChall2012" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="971px">
                <tr>
                    <td align="left" >
                        <asp:Label ID="lblPageTitle" runat="server" Text="Trade Challenge 2016" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right" >
                        <dxe:ASPxLabel ID="lblLeague" runat="server" Text="View  :  " style="font-weight: 700; font-size:small ">
                        </dxe:ASPxLabel>
                        <asp:DropDownList ID="ddlLeague" runat="server" AutoPostBack="True"  
                            Width="180px"   >
                            <asp:ListItem>Burns</asp:ListItem>
                            <asp:ListItem>Smithers</asp:ListItem>
                            <asp:ListItem>Skinner</asp:ListItem>
                            <asp:ListItem>Simpson</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridView ID="gridCampaigns" runat="server" width="971px" OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            DataSourceID="dsCampaigns"  AutoGenerateColumns="False" KeyFieldName="DealerCode" 
            style="position:relative; top:20px; left:5px">
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded"  />
            
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <styles>
                <Header Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                <Footer Wrap="True" HorizontalAlign="Center" Font-Size="Small" VerticalAlign="Middle" />
                <Cell HorizontalAlign="Center" Font-Size="Small" />
            </styles>

            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Ranking" FieldName="Ranking" VisibleIndex="1" Width="10%" ExportWidth="50" />

                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="2" Width="10%" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone" VisibleIndex="3" Width="10%" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="League" FieldName="League" VisibleIndex="4" Width="12%" Visible="false" ExportWidth="150" />

                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="5" ExportWidth="300">
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Achievement"  FieldName="Achievement" VisibleIndex="6" Width="10%" ExportWidth="150" />

            </Columns>
            
            <Templates>
                <DetailRow>

                    <dxwgv:ASPxGridView ID="gridDrillDown" runat="server" width="100%"
                        DataSourceID="dsDrillDown"  AutoGenerateColumns="False" KeyFieldName="DealerCode" 
                        OnHtmlDataCellPrepared="gridDrillDown_HtmlDataCellPrepared"
                        OnBeforePerformDataSelect="gridDetail_BeforePerformDataSelect"> 
       
                        <SettingsPager PageSize="120" Mode="ShowAllRecords">
                        </SettingsPager>
            
                        <Settings
                            ShowFooter="True"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                            ShowGroupFooter="VisibleIfExpanded" ShowVerticalScrollBar="True" 
                            UseFixedTableLayout="True" VerticalScrollableHeight="300"  />
            
                        <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                            ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
                        <Styles>
                            <Header Wrap="True" HorizontalAlign="Center" />
                            <Cell Font-Size="Small" HorizontalAlign="Center" />
                            <Footer HorizontalAlign="Center" VerticalAlign="Middle" />
                        </Styles>

                        <Columns>
                
                            <dxwgv:GridViewDataTextColumn Caption="PFC" FieldName="PFC" VisibleIndex="0" Width="5%" ExportWidth="100" />

                            <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="PFCDescription" VisibleIndex="1" Width="25%" ExportWidth="200" />

                            <dxwgv:GridViewDataTextColumn Caption="Jul" FieldName="Month1" VisibleIndex="2" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Aug" FieldName="Month2" VisibleIndex="3" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Sep" FieldName="Month3" VisibleIndex="4" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Oct" FieldName="Month4" VisibleIndex="5" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Nov" FieldName="Month5" VisibleIndex="6" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Dec" FieldName="Month6" VisibleIndex="7" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="TotalSpend" VisibleIndex="8" ExportWidth="120">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                            </dxwgv:GridViewDataTextColumn>

                        </Columns>
            
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month1" SummaryType="Sum"  />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month2" SummaryType="Sum"  />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month3" SummaryType="Sum"  />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month4" SummaryType="Sum" />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month5" SummaryType="Sum"  />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month6" SummaryType="Sum"  />
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="TotalSpend" SummaryType="Sum"  />
                        </TotalSummary>

                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>

                        <SettingsDetail ShowDetailButtons="False" />

                    </dxwgv:ASPxGridView>

                    <table width="100%" style="position:relative;top:10px">
                        <tr>
                            <td align="left" valign="top" style="width:50%">
                            </td>
                            <td align="right" valign="top" >

                                <dxwgv:ASPxGridView ID="gridCampaignsSummary" runat="server" width="376px" 
                                    OnCustomColumnDisplayText="Summary_OnCustomColumnDisplayText" 
                                    DataSourceID="dsCampaignsSummary"  AutoGenerateColumns="False">
                   
                                    <SettingsPager PageSize="120" Mode="ShowAllRecords">
                                    </SettingsPager>
                        
                                    <Settings
                                        ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                                        ShowGroupFooter="VisibleIfExpanded" VerticalScrollableHeight="350" 
                                        ShowColumnHeaders="False"  />
                        
                                    <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                                        ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                                    <Styles>
                                        <Header Wrap="True" HorizontalAlign="Center" />
                                        <Cell Font-Size="Small" HorizontalAlign="Center" />
                                        <Footer HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </Styles>

                                   <Columns>
                            
                                        <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowDescription" VisibleIndex="0" Width="60%" />

                                        <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue" VisibleIndex="1" />

                                    </Columns>
                        
                                    <StylesEditors>
                                        <ProgressBar Height="25px">
                                        </ProgressBar>
                                    </StylesEditors>

                                    <SettingsDetail ShowDetailButtons="False" />

                                </dxwgv:ASPxGridView>
                            </td>
                        </tr>
        
                    </table>

                </DetailRow>
            </Templates>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailRow="true" ShowDetailButtons="True" ExportMode="Expanded" AllowOnlyOneMasterRowExpanded="True" />

        </dxwgv:ASPxGridView>

        <dxe:ASPxLabel ID="ASPxLabel1" runat="server" Text="* Final league positions are subject to audit" Style="position:relative;top:20px">
        </dxe:ASPxLabel>
        
    </div>
    
    <asp:SqlDataSource ID="dsCampaigns" runat="server" SelectCommand="p_MCChallenge2016" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sLeague" SessionField="League" Type="String" Size="20" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsDrillDown" runat="server" SelectCommand="p_MCChallenge2016_DealerDetail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="MCDealerCode" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCampaignsSummary" runat="server" SelectCommand="p_MCChallenge2016_DealerDetail_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="MCDealerCode" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>




