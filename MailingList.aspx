﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MailingList.aspx.vb" Inherits="MailingList" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                />
        </div>

        <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Settings-HorizontalScrollBarMode="Auto"
        Styles-Cell-Wrap="False"
        Font-Size="XX-Small"
        Width=" 100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="25" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="False"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="50"
                Caption="id" 
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="1"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="mailingurn" 
                FieldName="mailingurn">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="dealergroup" 
                FieldName="dealergroup">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="3"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="dealercode" 
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="4"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyername" 
                FieldName="dealerflyername">
            </dx:GridViewDataTextColumn>
           
            <dx:GridViewDataTextColumn
                VisibleIndex="5"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyeraddress1" 
                FieldName="dealerflyeraddress1">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="6"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyeraddress2" 
                FieldName="dealerflyeraddress2">
            </dx:GridViewDataTextColumn>
       
            <dx:GridViewDataTextColumn
                VisibleIndex="7"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyeraddress3" 
                FieldName="dealerflyeraddress3">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="8"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyerposttown" 
                FieldName="dealerflyerposttown">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="9"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="dealerflyercounty" 
                FieldName="dealerflyercounty">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="10"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="dealerflyerpostcode" 
                FieldName="dealerflyerpostcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="11"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="300"
                Caption="dealerflyeremailaddress" 
                FieldName="dealerflyeremailaddress">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="12"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="dealerflyerphone" 
                FieldName="dealerflyerphone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="13"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="dealerflyerfax" 
                FieldName="dealerflyerfax">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="14"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="300"
                Caption="dealerflyercontactname" 
                FieldName="dealerflyercontactname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="15"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="contact_salutation" 
                FieldName="contact_salutation">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="16"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="300"
                Caption="contact_name" 
                FieldName="contact_name">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="17"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="contact_jobtitle" 
                FieldName="contact_jobtitle">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="18"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="300"
                Caption="contact_email" 
                FieldName="contact_email">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="19"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="300"
                Caption="businessname" 
                FieldName="businessname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="20"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="250"
                Caption="address1" 
                FieldName="address1">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="21"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="250"
                Caption="address2" 
                FieldName="address2">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="22"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="250"
                Caption="address3" 
                FieldName="address3">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="23"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="posttown" 
                FieldName="posttown">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="24"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="county" 
                FieldName="county">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="25"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="postcode" 
                FieldName="postcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="26"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="refcode" 
                FieldName="refcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="27"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="prospect_flag" 
                FieldName="prospect_flag">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="28"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="businesstype" 
                FieldName="businesstype">
            </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn
                VisibleIndex="29"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="250"
                Caption="flyeropeninghours" 
                FieldName="flyeropeninghours">
            </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn
                VisibleIndex="30"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                PropertiesTextEdit-DisplayFormatString="c2"
                exportwidth="100"
                Caption="spendthisyear" 
                FieldName="spendthisyear">
            </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn
                VisibleIndex="31"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="mailingstatus" 
                FieldName="mailingstatus">
            </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn
                VisibleIndex="32"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                CellStyle-HorizontalAlign="Center"
                exportwidth="100"
                Caption="mailingterritory" 
                FieldName="mailingterritory">
            </dx:GridViewDataTextColumn>


         </Columns>
        
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsFocus" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_MakeMailingList_Focus" 
            SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>

        <asp:SqlDataSource 
            ID="dsEmail" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_MakeMailingList_Email" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="spend_flag " SessionField="Spend_Flag" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>


        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

