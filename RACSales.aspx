<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RACSales.aspx.vb" Inherits="RACSales" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
        
       <div id="divTitle" style="position:relative; top:0px">
            <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="RAC Account Sales" />
       </div>

       <br />

        <table id="trSelectionStuff" Width="40%" style="position:relative; margin-bottom: 10px;">
            <tr id="Tr2" runat="server" >
   
                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblFrom"
                            runat="server" 
                            Text="Month From">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblTo"
                            runat="server" 
                            Text="Month To">
                    </dx:ASPxLabel>
                </td>


                </tr>

                <tr id="Tr1" runat="server" >

                    <td align="left" width="50%" valign="top" >
                         <dx:ASPxComboBox
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                     </td>

                    <td align="left" width="50%" valign="top">
                         <dx:ASPxComboBox
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                    </td>
                
             </tr>
        </table>
        
        <dx:ASPxGridView 
            ID="gridRACSales" 
            runat="server" 
            onload="gridStyles"
            cssclass="grid_styles"
            AutoGenerateColumns="False" 
            DataSourceID="dsRACSales" 
            KeyFieldName="code" 
            Width="100%"> 

            <SettingsDetail  ShowDetailRow="false"/>
            
            <Settings 
                ShowHeaderFilterBlankItems="false"
                ShowFilterRow="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior 
                AllowSort="true" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <SettingsPager  Mode="ShowPager" PageSize="16" >
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign ="Center" />

            <Columns>

                <dx:GridViewBandColumn Caption="Identification Details"  HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                            <dx:GridViewDataTextColumn 
                    VisibleIndex="0"
                    FieldName="dealername"
                    Name="dealername"
                    caption="Dealer"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="15%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="1"
                    FieldName="dealercode"
                    Name="dealercode"
                    caption="Code"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="5%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="2"
                    FieldName="link"
                    Name="link"
                    caption="A/C Code"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="3"
                    FieldName="qubeurn"
                    Name="qubeurn"
                    caption="Qube URN"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataTextColumn>

                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Invoice Details"  HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                            <dx:GridViewDataTextColumn 
                    VisibleIndex="4"
                    FieldName="invoicenumber"
                    Name="invoicenumber"
                    caption="Invoice No."
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataDateColumn 
                    VisibleIndex="5"
                    FieldName="invoicedate"
                    Name="invoicedate"
                    caption="Date"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataDateColumn>


                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Product Details"  HeaderStyle-HorizontalAlign="Center">

                    <Columns>
                            <dx:GridViewDataTextColumn 
                    VisibleIndex="6"
                    FieldName="partnumber"
                    Name="partnumber"
                    caption="Part No."
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="8%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataDateColumn 
                    VisibleIndex="7"
                    FieldName="partdescription"
                    Name="partdescription"
                    caption="Description."
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="10%" >
                </dx:GridViewDataDateColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="9"
                    FieldName="unitretailprice"
                    Name="unitretailprice"
                    caption="RRP"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" >
                   <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="10"
                    FieldName="unitsaleprice"
                    Name="unitsaleprice"
                    caption=" Sale Price"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" >
                   <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="11"
                    FieldName="unitcostprice"
                    Name="unitcostprice"
                    caption="Cost Price"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" 
                                >
                   <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>

               <dx:GridViewBandColumn Caption="Transaction Value"  HeaderStyle-HorizontalAlign="Center">

                    <Columns>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="13"
                    FieldName="quantity"
                    Name="quantity"
                    caption="Units"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" >
                     <PropertiesTextEdit DisplayFormatString="##,##0"/>
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="14"
                    FieldName="retailvalue"
                    Name="retailvalue"
                    caption="Retail Value"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" >
                   <PropertiesTextEdit DisplayFormatString="C2"/>
                </dx:GridViewDataTextColumn>
      
                            <dx:GridViewDataTextColumn 
                    VisibleIndex="15"
                    FieldName="salevalue"
                    Name="salevalue"
                    caption="Sale Value"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="7%" >
                   <PropertiesTextEdit DisplayFormatString="C2"/>
                </dx:GridViewDataTextColumn>

                   </Columns>
                </dx:GridViewBandColumn>


            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum"  />
                <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="retailvalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="salevalue" SummaryType="Sum" />
             </TotalSummary>
  
        </dx:ASPxGridView>
         
    </div>
    
    <asp:SqlDataSource ID="dsRACSales" runat="server" SelectCommand="sp_RAC_Sales" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridRACSales" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>
