	function calcParallax(tileheight, speedratio, scrollposition) {
			  return ((tileheight) - (Math.floor(scrollposition / speedratio) % (tileheight+1)));
			}
			$(function() {				
				window.onscroll = function() {
					var posX = (document.documentElement.scrollLeft) ? document.documentElement.scrollLeft : window.pageXOffset;
					var posY = (document.documentElement.scrollTop) ? document.documentElement.scrollTop : window.pageYOffset;
				}
				$('.nav, .folio, .welcome, #baselinks').localScroll( {
					duration: 2000,					
					onBefore: function(e, anchor, $target) {
					    $('.outer').removeClass('important');
					},
					onAfter: function(e, anchor, $target) {
					    $(e).addClass('important');
					}
				} );									
				$('.outer').each(function() {
					var wrapper = this.id;
					$('.subnav', this).localScroll( {
						target: $('.content', this), 
						duration: 1500,
						hash: false,
						axis: 'xy',
						queue: true,
						onBefore: function(e, anchor, $target, $wrapper){ 
							$('.' + wrapper + 'scrolling').removeClass(wrapper + 'scrolling');
							$(this).addClass(wrapper + 'scrolling');
							this.blur();
						}
					} );
					$('li.sub p, li.sub li', this).localScroll( {
						target: $('.content', this), 
						axis: 'xy',
						queue: true,
						hash: false,
						duration: 1500
					} );					
				} );				
				$('ul.automotive,ul.database_design,ul.website_development,ul.database_hosting').hide();
				$('a.automotive').click(function() {
					if ($("ul.database_design").is(":visible")) 
						$("ul.database_design").slideUp();
					if ($("ul.website_development").is(":visible")) 
						$("ul.website_development").slideUp();
					if ($("ul.database_hosting").is(":visible")) 
						$("ul.database_hosting").slideUp();	
					if ($("ul.automotive").is(":hidden"))
						$("ul.automotive").slideDown();
				} );
				$('a.database_design').click(function() {
					if ($("ul.automotive").is(":visible")) 
						$("ul.automotive").slideUp();
					if ($("ul.website_development").is(":visible")) 
						$("ul.website_development").slideUp();
					if ($("ul.database_hosting").is(":visible")) 
						$("ul.database_hosting").slideUp();	
					if ($("ul.database_design").is(":hidden"))
						$("ul.database_design").slideDown();
				} );
				$('a.website_development').click(function() {
					if ($("ul.database_design").is(":visible")) 
						$("ul.database_design").slideUp();
					if ($("ul.database_hosting").is(":visible")) 
						$("ul.database_hosting").slideUp();	
					if ($("ul.automotive").is(":visible")) 
						$("ul.automotive").slideUp();
					if ($("ul.website_development").is(":hidden"))
						$("ul.website_development").slideDown();
				} );	
				$('a.database_hosting').click(function() {
					if ($("ul.database_design").is(":visible")) 
						$("ul.database_design").slideUp();
					if ($("ul.website_development").is(":visible")) 
						$("ul.website_development").slideUp();	
					if ($("ul.automotive").is(":visible")) 
						$("ul.automotive").slideUp();
					if ($("ul.database_hosting").is(":hidden"))
						$("ul.database_hosting").slideDown();
				} );	
				setupZoom();				
				$('#foliothumbs').cycle();		
			} );	