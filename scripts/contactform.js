
/* 
 * jQuery/AJAX Validated Contact Form
 *
 * Author: Chris Wheeler for youlove.us
 * Requires: jQuery and jQuery form plugin
 *
 */

post_form = function(form) {
	$.ajax({
		url: '/ajax/contact.php',
		data: $(form).formSerialize(),
		success: function(xml) {
			switch ($("status", xml).text()) {
				case '1':
					// Message sent OK
					alert($("message", xml).text());
					break;
				case '2':
					// One of the fields didnt validate
					alert($("message", xml).text());
					// Add a red border to the dirty field
					$("#" + $("field", xml).text()).css('border', '1px solid #dd0000');
					// Set an even to reset the border when the field is clicked
					$("#" + $("field", xml).text()).bind('focus', function () {
						$(this).css('border', '1px solid #333333');
					});
					break;
				default:
					// Something bad happened, and it wasn't a validation error
					alert($("message", xml).text());
					break;
			}
		},
		error: function(xml, message) {
			alert('Communication Error');
		}
	});
}
