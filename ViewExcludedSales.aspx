﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewExcludedSales.aspx.vb" Inherits="ViewExcludedSales" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

        <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>



<div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:0px">
                 <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Excluded Sales" />
        </div>

        <br />

       
          <dx:ASPxGridView ID="gridSummary" 
                        runat="server" 
                        DataSourceID="dsExcludedSalesSummary"  
                        AutoGenerateColumns="False"       
                        KeyFieldName="CustomerDealerId" 
                        OnCustomColumnDisplaytext="gridExcludedInvoices_OnCustomColumnDisplayText">
                   
                        <SettingsPager Visible="False">
                        </SettingsPager>

                        <Settings ShowFilterBar="Hidden" ShowFilterRowMenu="false" />

                        <Styles>
                            <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" ImageSpacing="5px" SortingImageSpacing="5px" />
                            <Cell HorizontalAlign="Center" />
                            <LoadingPanel ImageSpacing="10px" />
                        </Styles>                   

                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>

                        <Columns>
                        
                            <dx:GridViewDataTextColumn Caption="Excluded Sales (YTD)"  FieldName="ExcludedSales" VisibleIndex="0" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Included Sales (YTD)" FieldName="IncludedSales" VisibleIndex="1" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Total Sales (YTD)" FieldName="TotalSales" VisibleIndex="2" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn Caption="Excluded (%)" FieldName="ExcludedPerc" VisibleIndex="3" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="0.00">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>

                        </Columns>
                        
                    </dx:ASPxGridView>        
      
        <br />

          <dx:ASPxGridView ID="gridSales" 
            runat="server" 
            OnLoad ="gridStyles"
            CssClass="grid_styles"
            DataSourceID="dsExcludedSales"  
            AutoGenerateColumns="False"       
            KeyFieldName="CustomerDealerId"  >

            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16" />
            
            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />

            <Styles>
                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                <Cell HorizontalAlign="Center" />
            </Styles>

            <Columns>
                
                <dx:GridViewDataTextColumn Caption="Dealer" FieldName="DealerCode"  VisibleIndex="0" Width="5%"  ExportWidth="100"  Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList"/>
                
                <dx:GridViewDataTextColumn Caption="A/C Code" FieldName="DMSRef"  VisibleIndex="1" Width="10%"  ExportWidth="100" />

                <dx:GridViewDataTextColumn Caption="Name" FieldName="AccountName"  VisibleIndex="2"  Width="20%" ExportWidth="300"   CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/>
                
                <dx:GridViewDataTextColumn Caption="Full Address" FieldName="AccountFullAddress"  VisibleIndex="3"  Width="30%" ExportWidth="200" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"/> 
                
                <dx:GridViewDataTextColumn Caption="Postcode" FieldName="AccountPostCode" VisibleIndex="8" Width="7%" ExportWidth="100" />
                
                <dx:GridViewDataTextColumn Caption="Telephone Number" FieldName="AccountTelephone" Width="10%" VisibleIndex="9"   ExportWidth="200" />

                <dx:GridViewDataTextColumn Caption="Reason" FieldName="Reason"  VisibleIndex="10" Width="10%"  ExportWidth="200" Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList"/>
                
                <dx:GridViewDataTextColumn Caption="Sales (YTD)" FieldName="ExcludedSales" VisibleIndex="11" Width="8%"  ExportWidth="150" ToolTip="These Sales are Excluded from the Trade Scheme" >
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

            </Columns>

            <Templates>
            
                <DetailRow>
                      <br />
                      <dx:ASPxGridView 
                       
                          ID="gridExcludedInvoices" 
                          runat="server"  
                          OnLoad ="gridStylessearchoff"
                          CssClass="grid_styles"
                          OnCustomColumnDisplaytext="gridExcludedInvoices_OnCustomColumnDisplayText" 
                          OnBeforePerformDataSelect ="gridExcludedInvoices_BeforePerformDataSelect" 
                          AutoGenerateColumns="False" 
                          DataSourceID="dsExcludedInvoices">
                   
                         <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="16" />
            
                         <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />


                        <Styles>
                            <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                            <Cell HorizontalAlign="Center" />
                        </Styles>

                        <Columns>
                        
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="InvoiceDate" VisibleIndex="1"  ExportWidth="150" />

                            <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                                <DataItemTemplate>
                                    <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                                Caption="Units" 
                                FieldName="Quantity"
                                ToolTip="The number of units sold within this PFC" 
                                VisibleIndex="3"  
                                PropertiesTextEdit-DisplayFormatString="N0"
                                ExportWidth="150">
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn 
                                Caption="Sales Value" 
                                FieldName="SaleValue" 
                                ToolTip="The total value of all parts sold"
                                VisibleIndex="4"  
                                ExportWidth="150">
                                <PropertiesTextEdit DisplayFormatString="C2"/>
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn 
                                Caption="Cost Value" 
                                FieldName="CostValue" 
                                ToolTip="The total cost of all parts purchased"
                                VisibleIndex="5"  
                                ExportWidth="150">
                                <PropertiesTextEdit DisplayFormatString="C2"/>
                            </dx:GridViewDataTextColumn>
                            
                            <dx:GridViewDataTextColumn 
                                Caption="Margin (%)"
                                FieldName="Margin" 
                                ToolTip="The total percentage margin made on sales of all parts"
                                VisibleIndex="6"  
                                ExportWidth="150">
                                <PropertiesTextEdit DisplayFormatString="0.00"/>
                            </dx:GridViewDataTextColumn>

                        </Columns>

                        <TotalSummary>
                              <dx:ASPxSummaryItem DisplayFormat="N0" FieldName="Quantity" SummaryType="Sum" />
                              <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="SaleValue" SummaryType="Sum" />
                              <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="CostValue" SummaryType="Sum" />
                        </TotalSummary>
                         
                        <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />

                        <Styles>
                            <Header HorizontalAlign="Center"/>
                            <Footer HorizontalAlign="Center" />
                            <Cell HorizontalAlign="Center"/>
                        </Styles>
                        
                    </dx:ASPxGridView>
                    <br />
                
                </DetailRow>
                
            </Templates>

              <TotalSummary>
                    <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="ExcludedSales" SummaryType="Sum" />
              </TotalSummary>



            <SettingsDetail ShowDetailRow="True" ExportMode="Expanded" />

        </dx:ASPxGridView>
       
        <br />
        
    </div>
       
    <asp:SqlDataSource ID="dsExcludedSalesSummary" runat="server" SelectCommand="p_ExcludedSales_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsExcludedSales" runat="server" SelectCommand="p_ExcludedSales" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>
 
    <asp:SqlDataSource ID="dsExcludedInvoices" runat="server" SelectCommand="p_Account_Excluded_Invoices" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nCustomerDealerId" SessionField="CustomerDealerId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridSales" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

 </asp:Content>

