﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="SpendbyTA.aspx.vb" Inherits="SpendbyTA" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
    
<%--<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>--%>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
    

    
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans';
        left: 0px; width: 976px; text-align: left;">
        
        <div id="divTitle" style="position: relative; left: 5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Sales Trends" Style="font-weight: 700;
                font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <table id="trSelectionStuff" Width="100%">
            <tr id="Tr3" runat="server" style="height: 30px;">
                <td style="font-size: small;" align="left">
                    Analyse by
                </td>
                <td style="font-size: small;" align="left">
                    <asp:DropDownList ID="ddlTA" runat="server" AutoPostBack="True" Width="180px"  >
                        <asp:ListItem>Product Category</asp:ListItem>
                        <asp:ListItem>Product Group</asp:ListItem>
                        <asp:ListItem>Mech Comp Breakdown</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="right" >
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Width="250px" 
                          >
                        <asp:ListItem>All Customers</asp:ListItem>
                        <asp:ListItem>Trade Club Members only</asp:ListItem>
                        <asp:ListItem>Not Trade Club Members</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        
        <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="100%">
            <TabPages>
                <%------------------------------------------------------------------------------------------
                ' TAB 1
                ------------------------------------------------------------------------------------------%>
                <dxtc:TabPage Text="Data">
                    <ContentCollection>
                        <dxw:ContentControl ID="ContentControl1" runat="server">
                            <dxwgv:ASPxGridView ID="gridSpendByTA" runat="server" DataSourceID="dsSpendsByTA"
                                AutoGenerateColumns="False" OnCustomColumnDisplayText="gridSpendByTA_OnCustomColumnDisplayText"
                                
                                Width="956px">
                                <SettingsPager PageSize="16" Mode="ShowAllRecords" Visible="False">
                                    <AllButton Text="All">
                                    </AllButton>
                                    <NextPageButton Text="Next &gt;">
                                    </NextPageButton>
                                    <PrevPageButton Text="&lt; Prev">
                                    </PrevPageButton>
                                </SettingsPager>
                                <Settings ShowGroupButtons="False" ShowStatusBar="Hidden" ShowTitlePanel="True" UseFixedTableLayout="True" />
                                <Columns>
                                    <dxwgv:GridViewDataTextColumn VisibleIndex="0" FieldName="PeriodName" ReadOnly="True"
                                        Width="10%">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Accessories" FieldName="SEG Accessories" VisibleIndex="3">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Body" FieldName="SEG Body" VisibleIndex="4">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Lighting" FieldName="SEG Lighting" VisibleIndex="5">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Mech Comp" FieldName="SEG Mech Comp" VisibleIndex="6">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Mechanical" FieldName="SEG Mechanical" VisibleIndex="7">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Other" FieldName="SEG Other" VisibleIndex="8">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="SEG Total" VisibleIndex="9">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Braking" FieldName="KPC Braking" VisibleIndex="10"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Clutch" FieldName="KPC Clutch" VisibleIndex="11"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Cooling" FieldName="KPC Cooling" VisibleIndex="12"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="ESU" FieldName="KPC ESU" VisibleIndex="13"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Exhaust" FieldName="KPC Exhaust" VisibleIndex="14"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Maintenance" FieldName="KPC Key Maintenance"
                                        VisibleIndex="15" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Lubricants" FieldName="KPC Lubricants" VisibleIndex="16"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Strg & Susp" FieldName="KPC Strg & Suspension"
                                        VisibleIndex="17" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Next 38" FieldName="KPC Next 38" VisibleIndex="18"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Other Business" FieldName="KPC Other" VisibleIndex="19"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="KPC Total" VisibleIndex="20"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Body" FieldName="PG  Body" VisibleIndex="21"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Brake" FieldName="PG  Brake" VisibleIndex="22"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Clutch" FieldName="PG  Clutch" VisibleIndex="23"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Electrical" FieldName="PG  Electrical" VisibleIndex="24"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Engine/Gearbox" FieldName="PG  Engine/Gearbox"
                                        VisibleIndex="25" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Exhaust" FieldName="PG  Exhaust" VisibleIndex="26"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Fuel" FieldName="PG  Fuel" VisibleIndex="27"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Heating/Cooling" FieldName="PG  Heating/Cooling"
                                        VisibleIndex="28" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Lighting" FieldName="PG  Lighting" VisibleIndex="29"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Oil" FieldName="PG  Oil" VisibleIndex="30"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Service" FieldName="PG  Service" VisibleIndex="31"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Steering/Susp" FieldName="PG  Steering/Suspensi"
                                        VisibleIndex="32" Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Other" FieldName="PG  Other" VisibleIndex="33"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="PG  Total" VisibleIndex="34"
                                        Visible="false">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                        <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dxwgv:GridViewDataTextColumn>
                                </Columns>
                                <Images>
                                    <CollapsedButton Height="12px" 
                                        Width="11px" />
                                    <ExpandedButton Height="12px" 
                                        Width="11px" />
                                    <DetailCollapsedButton Height="12px" 
                                        Width="11px" />
                                    <DetailExpandedButton Height="12px" 
                                        Width="11px" />
                                    <FilterRowButton Height="13px" Width="13px" />
                                </Images>
                                <Styles>
                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                    </Header>
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                </Styles>
                                <StylesEditors>
                                    <ProgressBar Height="25px">
                                    </ProgressBar>
                                </StylesEditors>
                            </dxwgv:ASPxGridView>
                            <br />
                            <br />
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
                
                <%------------------------------------------------------------------------------------------
                ' TAB 2
                ------------------------------------------------------------------------------------------%>
                <dxtc:TabPage Text="Chart">
                    <ContentCollection>
                        <dxw:ContentControl ID="ContentControl2" runat="server">
                            <dxchartsui:WebChartControl ID="WebChart1" runat="server" Visible="True" DataSourceID="dsSpendsByTA"
                                Height="400px" Width="956px" PaletteName="Office">
                                <seriesserializable>
                                    <cc1:Series  Name="Accessories" 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Accessories" LegendText="Accessories">
                                        <ViewSerializable>
                                        <cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
                                        </ViewSerializable>
                                        <LabelSerializable>
                                        <cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                        </OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
                                        </LabelSerializable>
                                        <PointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </LegendPointOptionsSerializable>
                                    </cc1:Series>
                                    <cc1:Series  Name="Body" 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Body" LegendText="Body">
                                        <ViewSerializable>
                                        <cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
                                        </ViewSerializable>
                                        <LabelSerializable>
                                        <cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                        </OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
                                        </LabelSerializable>
                                        <PointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </LegendPointOptionsSerializable>
                                    </cc1:Series>
                                    <cc1:Series  Name="Lighting" 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Lighting" LegendText="Lighting">
                                        <ViewSerializable>
                                        <cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
                                        </ViewSerializable>
                                        <LabelSerializable>
                                        <cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                        </OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
                                        </LabelSerializable>
                                        <PointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </LegendPointOptionsSerializable>
                                    </cc1:Series>
                                    <cc1:Series  Name="Mech Comp." 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Mech Comp" LegendText="Mech Comp.">
                                        <ViewSerializable>
                                        <cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
                                        </ViewSerializable>
                                        <LabelSerializable>
                                        <cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                        </OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
                                        </LabelSerializable>
                                        <PointOptionsSerializable>
                                        <cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
                                        </PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
                                        </LegendPointOptionsSerializable>
                                    </cc1:Series>
                                    <cc1:Series  Name="Mechanical" 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Mechanical" LegendText="Mechanical">
                                        <ViewSerializable>
                                        <cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
                                        </ViewSerializable>
                                        <LabelSerializable>
                                        <cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
                                        <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                                        </OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>
                                    <cc1:Series  Name="Other" 
                                         ArgumentDataMember="PeriodName"
                                        ValueDataMembersSerializable="SEG Other" LegendText="Other">
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                            Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>
                                </seriesserializable>
                                <seriestemplate>
                                    <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                    </cc1:LineSeriesView>
</ViewSerializable>
                                    <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" >
                                        <FillStyle >
                                            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                        </FillStyle>
                                    </cc1:PointSeriesLabel>
</LabelSerializable>
                                    <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</PointOptionsSerializable>
                                    <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                </seriestemplate>
                                <diagramserializable>
<cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                        <label angle="45"></label>
                                        <range sidemarginsenabled="True"></range>
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                        <range sidemarginsenabled="True"></range>
                                    </axisy>
                                </cc1:XYDiagram>
</diagramserializable>
                                <fillstyle>
                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                </fillstyle>
                            </dxchartsui:WebChartControl>
                            <dxchartsui:WebChartControl ID="WebChart2" runat="server" Visible="false" DataSourceID="dsSpendsByTA"
                                Height="400px" Width="956px" PaletteName="Office">
                                <seriesserializable>

                                    <cc1:Series  
					                    Name="Body"
					                    LegendText="Body" 
                                        ValueDataMembersSerializable="PG  Body" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Brake"
					                    LegendText="Brake" 
                                        ValueDataMembersSerializable="PG  Brake" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Clutch"
					                    LegendText="Clutch" 
                                        ValueDataMembersSerializable="PG  Clutch" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
				                        Name="Electrical"
				                        LegendText="Electrical" 
                                        ValueDataMembersSerializable="PG  Electrical" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Engine/Gearbox"
					                    LegendText="Engine/Gearbox" 
                                        ValueDataMembersSerializable="PG  Engine/Gearbox" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Exhaust"
					                    LegendText="Exhaust" 
                                        ValueDataMembersSerializable="PG  Exhaust" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Fuel"
					                    LegendText="Fuel" 
                                        ValueDataMembersSerializable="PG  Fuel" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Heating/Cooling"
					                    LegendText="Heating/Cooling" 
                                        ValueDataMembersSerializable="PG  Heating/Cooling" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Lighting"
					                    LegendText="Lighting" 
                                        ValueDataMembersSerializable="PG  Lighting" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Oil"
					                    LegendText="Oil" 
                                        ValueDataMembersSerializable="PG  Oil" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Service"
					                    LegendText="Service" 
                                        ValueDataMembersSerializable="PG  Service" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Steering/Suspensi"
					                    LegendText="Steering/Suspensi" 
                                        ValueDataMembersSerializable="PG  Steering/Suspensi" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Other"
					                    LegendText="Other" 
                                        ValueDataMembersSerializable="PG  Other" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>
                                </seriesserializable>
                                <seriestemplate>
                                    <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                    </cc1:LineSeriesView>
</ViewSerializable>
                                    <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" >
                                        <FillStyle >
                                            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                        </FillStyle>
                                    </cc1:PointSeriesLabel>
</LabelSerializable>
                                    <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</PointOptionsSerializable>
                                    <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                </seriestemplate>
                                <diagramserializable>
<cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                        <label angle="45"></label>
                                        <range sidemarginsenabled="True"></range>
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                        <range sidemarginsenabled="True"></range>
                                    </axisy>
                                </cc1:XYDiagram>
</diagramserializable>
                                <fillstyle>
                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                </fillstyle>
                            </dxchartsui:WebChartControl>
                            <dxchartsui:WebChartControl ID="WebChart3" runat="server" Visible="false" DataSourceID="dsSpendsByTA"
                                Height="400px" Width="956px" PaletteName="Office">
                                <seriesserializable>

                                    <cc1:Series  
					                    Name="Braking"
					                    LegendText="Braking" 
                                        ValueDataMembersSerializable="KPC Braking" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Clutch"
					                    LegendText="Clutch" 
                                        ValueDataMembersSerializable="KPC Clutch" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Cooling"
					                    LegendText="Cooling" 
                                        ValueDataMembersSerializable="KPC Cooling" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
				                        Name="ESU"
				                        LegendText="ESU" 
                                        ValueDataMembersSerializable="KPC ESU" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Exhaust"
					                    LegendText="Exhaust" 
                                        ValueDataMembersSerializable="KPC Exhaust" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Key Maintenance"
					                    LegendText="Key Maintenance" 
                                        ValueDataMembersSerializable="KPC Key Maintenance" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Lubricants"
					                    LegendText="Lubricants" 
                                        ValueDataMembersSerializable="KPC Lubricants" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Strg & Suspension"
					                    LegendText="Strg & Suspension" 
                                        ValueDataMembersSerializable="KPC Strg & Suspension" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                    <cc1:Series  
					                    Name="Next 38"
					                    LegendText="Next 38" 
                                        ValueDataMembersSerializable="KPC Next 38" 
					                    
					                    ArgumentDataMember="PeriodName"					
                                        >
                                        <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                        </cc1:LineSeriesView>
</ViewSerializable>
                                        <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" 
                                             Visible="False">
                                            <FillStyle >
                                                <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                            </FillStyle>
                                        </cc1:PointSeriesLabel>
</LabelSerializable>
                                        <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                        </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                    </cc1:Series>

                                </seriesserializable>
                                <seriestemplate>
                                    <ViewSerializable>
<cc1:LineSeriesView HiddenSerializableString="to be serialized">
                                    </cc1:LineSeriesView>
</ViewSerializable>
                                    <LabelSerializable>
<cc1:PointSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" >
                                        <FillStyle >
                                            <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                        </FillStyle>
                                    </cc1:PointSeriesLabel>
</LabelSerializable>
                                    <PointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</PointOptionsSerializable>
                                    <LegendPointOptionsSerializable>
<cc1:PointOptions HiddenSerializableString="to be serialized">
                                    </cc1:PointOptions>
</LegendPointOptionsSerializable>
                                </seriestemplate>
                                <diagramserializable>
<cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                        <label angle="45"></label>
                                        <range sidemarginsenabled="True"></range>
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                        <range sidemarginsenabled="True"></range>
                                    </axisy>
                                </cc1:XYDiagram>
</diagramserializable>
                                <fillstyle>
                                    <OptionsSerializable>
<cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
</OptionsSerializable>
                                </fillstyle>
                            </dxchartsui:WebChartControl>
                            <br />
                        </dxw:ContentControl>
                    </ContentCollection>
                </dxtc:TabPage>
            </TabPages>
        </dxtc:ASPxPageControl>
        <br />
    </div>
    <asp:SqlDataSource ID="dsSpendsByTA" runat="server" SelectCommand="p_Sales_Trends"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nTradeClubType" SessionField="TradeClubType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridSpendByTA">
    </dxwgv:ASPxGridViewExporter>
    
</asp:Content>
