﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HOBonusReport_1920.aspx.vb" Inherits="HOBonusReport_1920" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <dx:ASPxPopupControl 
        ID="popSupportingInfo" 
        runat="server"  
        Width ="800px"  
        Height="500px" 
        HeaderText="Payment Bands and Supporting Information"
        ShowOnPageLoad="False" 
        ClientInstanceName="popSupportingInfo" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        CloseOnEscape="True">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="popSupportingInfoContent" runat="server" CssClass="invoice">

                <dx:ASPxGridView 
                                ID="gridBonusBandings" 
                                DataSourceID="dsBonusBanding"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     
                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                        
                                        <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Dealer/Group Performance Banding">
                                            <Columns>

                                                <dx:GridViewDataTextColumn 
                                              Fieldname="id" Visible="false"/>

                                                <dx:GridViewDataTextColumn 
                                              Caption="Band Name" 
                                              FieldName="banding"
                                              Name="banding"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              CellStyle-Wrap ="False"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                                            
                                                <dx:GridViewDataTextColumn 
                                              Caption="Band Description" 
                                              FieldName="comment"
                                              Name="comment"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="True"
                                              ExportWidth="200"
                                              Width= "32%"
                                              VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                                            
                                            </Columns>
                                       </dx:GridViewBandColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="No Band"  
                                              FieldName="band0_text"
                                              Name="band0_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap ="False"
                                              ToolTip="Must Grow more than this for ANY payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 1 Range" 
                                              FieldName="band1_text"
                                              Name="band1_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="False"
                                              ToolTip="% Growth Range Required to Earn Band 1 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 2 Range" 
                                              FieldName="band2_text"
                                              Name="band2_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-Wrap="False"
                                              CellStyle-HorizontalAlign="Center"
                                              ToolTip="% Growth Range Required to Earn Band 2 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 3 Range" 
                                              FieldName="band3_text"
                                              Name="band3_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="False"
                                              ToolTip="% Growth Range Required to Earn Band 3 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
                                </dx:ASPxGridView>   

                <br />

                 <dx:ASPxGridView 
                                ID="gridPaymentScale" 
                                DataSourceID="dsBonusPayments"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     

                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>

                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Sales Performance Reward">
                                                <Columns>

                                                    <dx:GridViewDataTextColumn 
                                              Caption="Sales By Quarter" 
                                              FieldName="qsales_bandname"
                                              Name="qsales_bandname"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              ToolTip="Total Sales By Quarter"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                        
                                            <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 1" 
                                              FieldName="payband_1"
                                              Name="payband_1"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                                            
                                            <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 2" 
                                              FieldName="payband_2"
                                              Name="payband_2"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                           <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 3" 
                                              FieldName="payband_3"
                                              Name="payband_3"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>

                                                 </Columns>

                                             </dx:GridViewBandColumn>


                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Operating Standards">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn 
                                              Caption="Payment Scale" 
                                              FieldName="standardsreward"
                                              Name="standardsreward"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap="True"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              Tooltip="Subject To Audit"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                                </Columns>
                                             </dx:GridViewBandColumn>

                               </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
                                </dx:ASPxGridView>   
   
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

  <div style="position: relative; font-family: Calibri; text-align: left;" >
    
    <div id="divTitle" style="position:relative;">
            
           <table style="width:100%">
                <tr>
                        <td style="width:30%" align="left">
                                        <dx:ASPxLabel
                                        ID="lblPageTitle" 
                                        runat="server" 
                                        Text="Head Office Bonus Summary FY 2019/20" 
                                        Font-Size="Larger" />
                        </td>

                        <td style="width:20%" align="right">
                                      <dx:ASPxButton id ="btnInfo" runat ="server" Font-Names="Calibri" Font-Size="Medium" AutoPostBack="true" Text="Supporting Information"/>
                        </td>
                </tr>
          </table>

    </div>


    <br />

    <dx:ASPxGridView 
        ID="gridDealerBonus" 
        DataSourceID="dsDealerBonus"
        runat="server" 
        Styles-Header-HorizontalAlign="Center"
        Styles-Header-Wrap="True"
        Styles-Cell-Wrap="False"
        Styles-CellStyle-HorizontalAlign="Center"
        AutoGenerateColumns="False"  
        Settings-UseFixedTableLayout="true"
        CssClass="grid_styles">
        
        <Settings ShowGroupButtons="False" ShowStatusBar="Hidden" />

        <SettingsPager  Visible="true" AllButton-Visible="true" PageSize="20"/>

        <SettingsSearchPanel Visible="true" />

        <SettingsBehavior AllowSort="true" AllowDragDrop="False" />

        <Settings ShowFooter="true" />

        <Styles Footer-HorizontalAlign ="Center"  Footer-Font-Bold="true"  AlternatingRow-BackColor="WhiteSmoke" Footer-BackColor="#808080" Footer-ForeColor="White"/>
        
        <Columns>
            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Dealer Information" Name="colBandDealer">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="0" Caption="Dealer Name" FieldName="dealername" CellStyle-HorizontalAlign="Left" ExportWidth="250" Width= "12%"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="1" Caption="Code" FieldName="dealercode" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "4%"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="2" Caption="Zone" FieldName="zone" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "4%" Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="3" Caption="Bonus Band" FieldName="bonusband" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "10%" Settings-AllowHeaderFilter="True" Settings-HeaderFilterMode="CheckedList"/>
                </Columns>
            </dx:GridViewBandColumn>
        
            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Quarter 1 (Apr-Jun 2019)" Name="colBandQ1">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="4" Caption="Payment Band" FieldName="q1_band" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%"  />
                    <dx:GridViewDataTextColumn VisibleIndex ="5" Caption="Sales Bonus" FieldName="q1_reward" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="6" Caption="Standards Bonus" FieldName="q1_standards" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                </Columns>
            </dx:GridViewBandColumn>
        
            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Quarter 2 (Jul-Sep 2019)" Name="colBandQ2">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="7" Caption="Payment Band" FieldName="q2_band" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%"  />
                    <dx:GridViewDataTextColumn VisibleIndex ="8" Caption="Sales Bonus" FieldName="q2_reward" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="9" Caption="Standards Bonus" FieldName="q2_standards" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Quarter 3 (Oct-Dec 2019)" Name="colBandQ3">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="10" Caption="Payment Band" FieldName="q3_band" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%"  />
                    <dx:GridViewDataTextColumn VisibleIndex ="11" Caption="Sales Bonus" FieldName="q3_reward" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="12" Caption="Standards Bonus" FieldName="q3_standards" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Quarter 4 (Jan-Mar 2020)" Name="colBandQ4">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="13" Caption="Payment Band" FieldName="q4_band" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%"  />
                    <dx:GridViewDataTextColumn VisibleIndex ="14" Caption="Sales Bonus" FieldName="q4_reward" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="15" Caption="Standards Bonus" FieldName="q4_standards" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="FY 2019/20 Dealer Totals" Name="colBandYear">
                <Columns>
                    <dx:GridViewDataTextColumn VisibleIndex ="16" Caption="Total Sales Bonus" FieldName="year_reward" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                    <dx:GridViewDataTextColumn VisibleIndex ="17" Caption="Total Standards Bonus" FieldName="year_standards" CellStyle-HorizontalAlign="Center" ExportWidth="100" Width= "5%" PropertiesTextEdit-DisplayFormatString="c0"/>
                </Columns>
            </dx:GridViewBandColumn>

        </Columns>

        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="bonusband" SummaryType="Count"    DisplayFormat ="National Totals: "  />
            <dx:ASPxSummaryItem FieldName ="q1_reward" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q1_standards" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q2_reward" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q2_standards" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q3_reward" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q3_standards" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q4_reward" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="q4_standards" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="year_reward" SummaryType="Sum" DisplayFormat ="c0" />
            <dx:ASPxSummaryItem FieldName ="year_standards" SummaryType="Sum" DisplayFormat ="c0" />
        </TotalSummary>

        </dx:ASPxGridView>   
  
  </div>

<div>

    <asp:SqlDataSource 
        ID="dsDealerBonus" 
        runat="server" 
        SelectCommand="sp_HO_Bonus_1920" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

      <asp:SqlDataSource ID="dsBonusPayments" runat="server" SelectCommand="sp_BonusPaymentsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
                <asp:Parameter Name="finyear" DefaultValue="19/20"  Type="String" Size="5" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsBonusBanding" runat="server" SelectCommand="sp_BonusBandingsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
                <asp:Parameter Name="finyear" DefaultValue="19/20"  Type="String" Size="5" />
                <asp:SessionParameter Name="dealerband" SessionField="dealerband" Type="Int16" Size="1" />
        </SelectParameters>
     </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1"
        runat="server" 
        GridViewID="gridDealerBonus">
    </dx:ASPxGridViewExporter>



</div>

</asp:Content>

