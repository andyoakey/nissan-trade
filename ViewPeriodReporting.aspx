﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewPeriodReporting.aspx.vb" Inherits="ViewPeriodReporting" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>


    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Selective Period Reporting" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <table id="trSelectionStuff" width="976px">
            <tr id="Tr3" runat="server" style="height:30px;">
                <td style="font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlFrom" runat="server" AutoPostBack="True" Width="180px"   >
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlType1" runat="server" AutoPostBack="True" 
                        Width="180px"   >
                        <asp:ListItem>Month on month</asp:ListItem>
                        <asp:ListItem>Quarterly</asp:ListItem>
                        <asp:ListItem>Yearly</asp:ListItem>
                        <asp:ListItem>Year to date</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="font-size: small; " align="left" >
                    <asp:DropDownList ID="ddlType2" runat="server" AutoPostBack="True" 
                        Width="180px"   >
                        <asp:ListItem>Consecutive</asp:ListItem>
                        <asp:ListItem>Year on year</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left" >
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Width="250px" 
                          >
                        <asp:ListItem>All Customers</asp:ListItem>
                        <asp:ListItem>Trade Club Members only</asp:ListItem>
                        <asp:ListItem>Not Trade Club Members</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />

        <dxwgv:ASPxGridView ID="gridSales" runat="server" DataSourceID="dsViewSales"
         OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" Visible="True"
            AutoGenerateColumns="False" KeyFieldName="Category" Width="576px" 
            >
       
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False" />
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
            
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            <SettingsPager Visible="False">
            </SettingsPager>
            <Images >
                <CollapsedButton Height="12px" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="" FieldName="Category" ReadOnly="True" VisibleIndex="0">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Last Month" FieldName="LastMonth" VisibleIndex="1">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="This Month" FieldName="ThisMonth" VisibleIndex="2">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Variance" FieldName="Variance" VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

        </dxwgv:ASPxGridView>
        <br />
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsViewSales" runat="server" SelectCommand="p_SpendSummary1" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nToPeriod" SessionField="PRToPeriod" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nType1" SessionField="PRType1" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nType2" SessionField="PRType2" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTradeClubType" SessionField="TradeClubType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server"  GridViewID="gridSales">
    </dxwgv:ASPxGridViewExporter>


</asp:Content>

