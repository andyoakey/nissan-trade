Partial Class HeadOfficeSummary2
    Inherits System.Web.UI.Page
    Dim querytitle As String = ""

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Page.IsPostBack = False Then
            Session("nDataPeriod") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O'") '+ 1

            ' This returns the last month in the current financial year
            Session("nDataPeriod") = GetDataString("SELECT max(id) from DataPeriods  where financialyear in (select financialyear FROM DataPeriods where id = " & Session("nDataPeriod") & ")")
            Session("nDataPeriod") = Session("nDataPeriod") - 1

            Session("Level4_Code") = "ALL"
            Session("TradeAnalysis") = "All Products"
            Call RefreshGrid()
            tlDealerDrillDown.ExpandToLevel(2)
        End If

        Dim sThisFinYear As String = GetDataString("Select financialyear from dataperiods where id = " & Session("nDataPeriod"))
        Dim xYear As String = Mid(sThisFinYear, 3, 2)
        Dim yYear As String = Right(Trim(sThisFinYear), 2)

        tlDealerDrillDown.Columns("apr_sales").Caption = "April 20" & xYear
        tlDealerDrillDown.Columns("may_sales").Caption = "May 20" & xYear
        tlDealerDrillDown.Columns("jun_sales").Caption = "June 20" & xYear
        tlDealerDrillDown.Columns("jul_sales").Caption = "July 20" & xYear
        tlDealerDrillDown.Columns("aug_sales").Caption = "August 20" & xYear
        tlDealerDrillDown.Columns("sep_sales").Caption = "September 20" & xYear
        tlDealerDrillDown.Columns("oct_sales").Caption = "October 20" & xYear
        tlDealerDrillDown.Columns("nov_sales").Caption = "November 20" & xYear
        tlDealerDrillDown.Columns("dec_sales").Caption = "December 20" & xYear

        tlDealerDrillDown.Columns("jan_sales").Caption = "January 20" & yYear
        tlDealerDrillDown.Columns("feb_sales").Caption = "February 20" & yYear
        tlDealerDrillDown.Columns("mar_sales").Caption = "March 20" & yYear
        tlDealerDrillDown.Columns("tot_sales").Caption = "FY Total "


        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If


    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "DealerDrillDownByMonth"
        ASPxTreeListExporter1.FileName = sFileName
        ASPxTreeListExporter1.WriteXlsxToResponse()
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.ASPxTreeListExportRenderBrickEventArgs) Handles ASPxTreeListExporter1.RenderBrick

        Dim nThisValue As Decimal = 0
        Dim nThisColCount As Integer = 0

        Call GlobalRenderBrickTreeList(e)

    End Sub

    Protected Sub TreeListStyles(sender As Object, e As EventArgs)
        Call TreeList_Styles(sender)
    End Sub

    Protected Sub cboFY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFY.SelectedIndexChanged
        Select Case cboFY.SelectedIndex
            Case 0
                Session("nDataPeriod") = 146    'will show as Mar 18
            Case 1
                Session("nDataPeriod") = 134    'will show as Mar 17
            Case 2
                Session("nDataPeriod") = 122  'will show as Mar 16
            Case 3
                Session("nDataPeriod") = 110   'will show as Mar 15
            Case 4
                Session("nDataPeriod") = 98    'will show as Mar 14
            Case 5
                Session("nDataPeriod") = 86   ' will show as Mar 13
        End Select
    End Sub

    Protected Sub ddlSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged, ddlProductGroup.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Private Sub RefreshGrid()
        Session("Level4_Code") = ddlProductGroup.Value

        Session("TA") = ddlTA.Value
        If Session("TA") Is Nothing Then
            Session("TA") = "All Products"
        End If
        tlDealerDrillDown.DataBind()
    End Sub

    Private Sub cboViewLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboViewLevel.SelectedIndexChanged
        tlDealerDrillDown.CollapseAll()
        tlDealerDrillDown.ExpandToLevel(sender.value)
    End Sub
End Class
