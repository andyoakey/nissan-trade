﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report2.aspx.vb" Inherits="report2" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges" tagprefix="dxg" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges.Gauges" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges.Gauges.Linear" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges.Gauges.Circular" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges.Gauges.State" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGauges.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGauges.Gauges.Digital" tagprefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width:976px; margin: 0 auto; text-align: left;" >
    

    <dxe:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" ID="lblPageTitle" Text="Sales Analysis"
        Style="left: 5px; position: absolute; top: 5px; z-index: 1;" runat="server">
    </dxe:ASPxLabel>
    
    <%-- Icon Buttons --%>
    <dxe:ASPxButton ID="btn1" runat="server" Style="left: 0px; position: absolute; top: 40px;
        z-index: 2;"  EnableDefaultAppearance="false"  Text="" CssClass ="special" HorizontalPosition="True"
        Border-BorderStyle="None" Image-Url="~/Images2014/ProductSalesAnalysis.png" Image-UrlHottracked="~/Images2014/ProductSalesAnalysis_Hover.png">
    </dxe:ASPxButton>
    <dxe:ASPxButton ID="btn2" runat="server" Style="left: 125px; position: absolute;
        top: 40px; z-index: 2;" EnableDefaultAppearance="false" 
        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/CustomerSalesAnalysis.png"
        Image-UrlHottracked="~/Images2014/CustomerSalesAnalysis_Hover.png">
    </dxe:ASPxButton>
   <dxe:ASPxButton ID="btn3" runat="server" Style="left: 0px; position: absolute; top: 160px;
        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
        Border-BorderStyle="None" Image-Url="~/Images2014/BestSellingParts.png" Image-UrlHottracked="~/Images2014/BestSellingParts_Hover.png">
    </dxe:ASPxButton>
    <dxe:ASPxButton ID="btn4" runat="server" Style="left: 125px; position: absolute;
        top: 160px; z-index: 2;" EnableDefaultAppearance="false" 
        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/HighestValueCustomers.png"
        Image-UrlHottracked="~/Images2014/HighestValueCustomers-Hover.png">
    </dxe:ASPxButton>
    <dxe:ASPxButton ID="btn5" runat="server" Style="left: 0px; position: absolute; top: 280px;
        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
        Border-BorderStyle="None" Image-Url="~/Images2014/AdvancedReportDesigner.png" Image-UrlHottracked="~/Images2014/AdvancedReportDesigner_Hover.png">
    </dxe:ASPxButton>
    <%--<dxe:ASPxButton ID="btn6" runat="server" Style="left: 125px; position: absolute; top: 280px;
        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
        Border-BorderStyle="None" Image-Url="~/Images2014/tyre-target.png" Image-UrlHottracked="~/Images2014/tyre-target-hover.png">
    </dxe:ASPxButton>--%>
    
    
    </div>
</asp:Content>

