﻿Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Web

Partial Class RemanReport
    Inherits System.Web.UI.Page
    Dim sThisMonthName As String
    Dim nSales_this As Decimal = 0
    Dim nCost_this As Decimal = 0
    Dim nSales_last As Decimal = 0
    Dim nCost_last As Decimal = 0

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Session("nDataPeriod") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'") + 1
        sThisMonthName = GetDataString("Select periodname from dataperiods where id = " & Session("nDataPeriod") + 1)

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        If Page.IsPostBack = False Then
            Session("ReportType") = 1
            Session("RemanFromPeriod") = Session("nDataPeriod")
            Session("RemanToPeriod") = Session("nDataPeriod")
            ddlSelectFrom.SelectedIndex = 0
            ddlSelectTo.SelectedIndex = 0
            Session("DealerCodeChanged") = 1
            Session("RefCode") = ""
            Session("everpresent_flag") = 1
        End If

        If Session("DealerCodeChanged") = 1 Then
            Session("CoreStockDealerCode") = Session("DealerCode")
            Session("DealerCodeChanged") = 0
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        'If IsLiveNissanDealer(Session("DealerCode")) = True Then
        'gridProductGroup.Columns("bandDealers").Visible = False
        'gridPartNumber.Columns("bandDealers").Visible = False
        'Else
        gridProductGroup.Columns("bandDealers").Visible = True
        gridPartNumber.Columns("bandDealers").Visible = True
        'End If

        gridDealer.Visible = False
        gridCustomer.Visible = False
        gridProductGroup.Visible = False
        gridPartNumber.Visible = False

        If Session("ReportType") = 1 Then
            gridDealer.DataBind()
            gridDealer.Visible = True
        End If

        If Session("ReportType") = 2 Then
            gridCustomer.DataBind()
            gridCustomer.Visible = True
        End If

        If Session("ReportType") = 3 Then
            gridProductGroup.DataBind()
            gridProductGroup.Visible = True
        End If

        If Session("ReportType") = 4 Then
            gridPartNumber.DataBind()
            gridPartNumber.Visible = True
        End If

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = ""
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        Select Case Session("ReportType")
            Case 1
                Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
                sFileName = "RemanReport_Dealer_" & Session("DealerCode")
                composite.Links.AddRange(New Object() {linkResults1})
            Case 2
                Dim linkResults2 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter2)
                sFileName = "RemanReport_Customer_" & Session("DealerCode")
                composite.Links.AddRange(New Object() {linkResults2})
            Case 3
                Dim linkResults3 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter3)
                composite.Links.AddRange(New Object() {linkResults3})
                sFileName = "RemanReport_ProductGroup_" & Session("DealerCode")
            Case 4
                Dim linkResults4 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter4)
                composite.Links.AddRange(New Object() {linkResults4})
                sFileName = "VAReportk_Partnumber_" & Session("DealerCode")
        End Select

        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub
    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles _
        ASPxGridViewExporter1.RenderBrick,
        ASPxGridViewExporter2.RenderBrick,
        ASPxGridViewExporter3.RenderBrick,
        ASPxGridViewExporter4.RenderBrick

        Call GlobalRenderBrick(e)

        If e.Value Is Nothing Then
        Else
            If e.Column.Caption = "Variance" Then

                If e.Value > 0 Then
                    e.BrickStyle.ForeColor = Color.Green
                End If

                If e.Value = 0 Then
                    e.BrickStyle.BackColor = Color.White
                End If

                If e.Value < 0 Then
                    e.BrickStyle.ForeColor = Color.Red
                End If

            End If
        End If
    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStylesSearchOff(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    '  Protected Sub ComboStyles(sender As Object, e As EventArgs)
    ' Call Combo_Styles(sender)
    'End Sub
    'Protected Sub tabReport_ActiveTabChanged(ByVal source As Object, ByVal e As DevExpress.Web.TabControlEventArgs) Handles tabReport.ActiveTabChanged
    '    Session("ReportType") = e.Tab.Index + 1
    'End Sub


    Protected Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles _
         gridDealer.HtmlDataCellPrepared,
         gridCustomer.HtmlDataCellPrepared,
         gridProductGroup.HtmlDataCellPrepared,
         gridPartNumber.HtmlDataCellPrepared

        If e.DataColumn.Caption = "Variance" Then
            If e.CellValue = 0 Then
                e.Cell.ForeColor = Color.White
            End If
            If e.CellValue > 0 Then
                e.Cell.ForeColor = Color.Green
            End If
            If e.CellValue < 0 Then
                e.Cell.ForeColor = Color.Red
            End If
        End If


    End Sub
    Protected Sub ddlSelect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectFrom.SelectedIndexChanged, ddlSelectTo.SelectedIndexChanged
        Session("RemanFromPeriod") = GetDataString("select id from DataPeriods where periodname = '" & Left(Me.ddlSelectFrom.Text, 8) & "'")
        Session("RemanToPeriod") = GetDataString("select id from DataPeriods where periodname = '" & Left(Me.ddlSelectTo.Text, 8) & "'")
    End Sub

    Public Sub createblankcell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.White)
        brick.Text = " "
        brick.Rect = New RectangleF(0, 0, 2000, 20)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell1(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "By Dealer"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell2(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "By Customer"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell3(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "By Product Group"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell4(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "By Part"
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    'Private Sub createheadercell5(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
    '    Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
    '    brick.Font = New System.Drawing.Font("Verdana", 14, FontStyle.Underline)
    '    brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
    '    brick.Text = tabReport.Tabs(4).Text
    '    brick.Rect = New RectangleF(0, 0, 2000, 40)
    '    e.Graph.DrawBrick(brick)
    'End Sub

    Protected Sub gridAccount_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("Refcode") = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
    End Sub

    Private Sub ddlReportType_ValueChanged(sender As Object, e As EventArgs) Handles ddlReportType.ValueChanged
        Session("ReportType") = sender.value
    End Sub

    Protected Sub grid_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles _
        gridDealer.CustomSummaryCalculate,
        gridCustomer.CustomSummaryCalculate,
        gridProductGroup.CustomSummaryCalculate,
        gridPartNumber.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nSales_this = 0
            nSales_last = 0
            nCost_this = 0
            nCost_last = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nSales_this = nSales_this + e.GetValue("sales_this")
            nCost_this = nCost_this + e.GetValue("cost_this")
            nSales_last = nSales_last + e.GetValue("sales_last")
            nCost_last = nCost_last + e.GetValue("cost_last")

        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "margin_this" Then
                If nCost_this > 0 And nSales_this > 0 Then
                    e.TotalValue = (nSales_this / nCost_this) / 10
                End If
            End If

            If objSummaryItem.FieldName = "margin_variance" Then

                Dim nCostLastValue As Decimal = 0
                If nCost_last > 0 And nSales_last > 0 Then
                    nCostLastValue = (nSales_last / nCost_last) / 10
                End If

                Dim nCostThisValue As Decimal = 0
                If nCost_this > 0 And nSales_this > 0 Then
                    nCostThisValue = (nSales_this / nCost_this) / 10
                End If

                e.TotalValue = nCostThisValue - nCostLastValue
            End If

        End If

    End Sub

    Protected Overridable Sub PrepareMarginFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs)
        e.Values.Clear()

        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If
        e.AddValue(String.Format("Sold At Loss", 0), String.Empty, String.Format("[margin_this] <{0}", 0))
        e.AddValue(String.Format("from {0}% to {1}%", 0, 4.9), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0, 0.049))
        e.AddValue(String.Format("from {0}% to {1}%", 5, 9.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.05, 0.099))
        e.AddValue(String.Format("from {0}% to {1}%", 10, 10.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.1, 0.109))
        e.AddValue(String.Format("from {0}% to {1}%", 11, 11.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.11, 0.1199))
        e.AddValue(String.Format("from {0}% to {1}%", 12, 12.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.12, 0.1299))
        e.AddValue(String.Format("from {0}% to {1}%", 13, 13.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.13, 0.1399))
        e.AddValue(String.Format("from {0}% to {1}%", 14, 14.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.14, 0.1499))
        e.AddValue(String.Format("from {0}% to {1}%", 15, 19.99), String.Empty, String.Format("[margin_this] > {0} and [margin_this] < {1}", 0.15, 0.1999))
        e.AddValue(String.Format(">= {0}%", 20), String.Empty, String.Format("[margin_this] >= {0}", 0.2))
    End Sub

    Protected Overridable Sub PrepareVarianceFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs)
        e.Values.Clear()

        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If

        If e.Column.FieldName = "margin_variance" Then
            e.AddValue(String.Format("Small Rise (up to 1%)", 0), String.Empty, String.Format("[margin_variance] >={0}  and [margin_variance] < {1}", 0, 0.009))
            e.AddValue(String.Format("Large Rise (1%+)", 0), String.Empty, String.Format("[margin_variance] >={0}", 0.01))
            e.AddValue(String.Format("Small Drop (up to 1%)", 0), String.Empty, String.Format("[margin_variance] <={0}  and [margin_variance] > {1}", 0, -0.009))
            e.AddValue(String.Format("Large Drop (1%+)", 0), String.Empty, String.Format("[margin_variance] <={0}", -0.01))
        End If
    End Sub

    Protected Overridable Sub PrepareDecreaseIncreaseFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs, sfieldname As String)
        e.Values.Clear()
        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If
        e.AddValue(String.Format("Decrease", 0), String.Empty, String.Format("[" & sfieldname & "]  <{0}", 0))
        e.AddValue(String.Format("Increase", 0), String.Empty, String.Format("[" & sfieldname & "]  >={0}", 0))
    End Sub

    Protected Overridable Sub PrepareSmallNumericFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs, sfieldname As String)
        e.Values.Clear()
        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If
        e.AddValue(String.Format("from {0} to {1}", 0, 9), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 0, 9))
        e.AddValue(String.Format("from {0} to {1}", 10, 19), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 10, 19))
        e.AddValue(String.Format("from {0} to {1}", 20, 29), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 20, 39))
        e.AddValue(String.Format("from {0} to {1}", 30, 49), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 30, 49))
        e.AddValue(String.Format("from {0} to {1}", 50, 99), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 50, 99))
        e.AddValue(String.Format("100+", 100), String.Empty, String.Format("[" & sfieldname & "]  >={0}", 100))
    End Sub

    Protected Overridable Sub PrepareLargeNumericFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs, sfieldname As String)
        e.Values.Clear()
        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If
        e.AddValue(String.Format("from {0} to {1}", 0, 99), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 0, 99))
        e.AddValue(String.Format("from {0} to {1}", 100, 499), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 100, 499))
        e.AddValue(String.Format("from {0} to {1}", 500, 999), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 500, 999))
        e.AddValue(String.Format("from {0} to {1}", 1000, 5000), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 1000, 5000))
        e.AddValue(String.Format("5000+", 100), String.Empty, String.Format("[" & sfieldname & "]  >={0}", 5000))
    End Sub

    Protected Overridable Sub PrepareGBPFilterItems(ByVal e As ASPxGridViewHeaderFilterEventArgs, sfieldname As String)
        e.Values.Clear()
        If e.Column.Settings.HeaderFilterMode = HeaderFilterMode.List Then
            e.AddShowAll()
        End If
        e.AddValue(String.Format("from £{0} to £{1}", 0, 99), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 0, 99))
        e.AddValue(String.Format("from £{0} to £{1}", 100, 499), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 100, 499))
        e.AddValue(String.Format("from £{0} to £{1}", 500, 999), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 500, 999))
        e.AddValue(String.Format("from £1k to £5k", 1000, 4999), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 1000, 4999))
        e.AddValue(String.Format("from £5k to £10k", 5000, 9999), String.Empty, String.Format("[" & sfieldname & "] > {0} and [" & sfieldname & "]< {1}", 5000, 9999))
        e.AddValue(String.Format("£10k+", 100), String.Empty, String.Format("[" & sfieldname & "]  >={0}", 10000))
    End Sub

    Protected Sub grid_HeaderFilterFillItems(ByVal sender As Object, ByVal e As ASPxGridViewHeaderFilterEventArgs)

        Dim bFilterOn As Boolean = False

        If e.Column.FieldName = "margin_this" Then
            PrepareMarginFilterItems(e)
            bFilterOn = True
        End If

        If e.Column.FieldName = "margin_variance" And bFilterOn = False Then
            PrepareVarianceFilterItems(e)
            bFilterOn = True
        End If

        If Right(e.Column.FieldName, 8) = "variance" And bFilterOn = False Then
            PrepareDecreaseIncreaseFilterItems(e, e.Column.FieldName)
            bFilterOn = True
        End If

        If Left(e.Column.FieldName, 5) = "units" And bFilterOn = False Then
            Call PrepareSmallNumericFilterItems(e, e.Column.FieldName)
            bFilterOn = True
        End If

        If Left(e.Column.FieldName, 5) = "sales" And bFilterOn = False Then
            Call PrepareGBPFilterItems(e, e.Column.FieldName)
        End If

        If Left(e.Column.FieldName, 6) = "dealer" And bFilterOn = False Then
            If e.Column.FieldName <> "dealercode" And e.Column.FieldName <> "dealername" And e.Column.FieldName <> "dealer" Then
                Call PrepareLargeNumericFilterItems(e, e.Column.FieldName)
            End If
        End If

        If Left(e.Column.FieldName, 8) = "customer" And bFilterOn = False Then
            Call PrepareLargeNumericFilterItems(e, e.Column.FieldName)
        End If


    End Sub
    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilters.Click
        gridDealer.FilterExpression = " "
        gridDealer.SearchPanelFilter = " "

        gridCustomer.FilterExpression = " "
        gridCustomer.SearchPanelFilter = " "

        gridProductGroup.FilterExpression = " "
        gridProductGroup.SearchPanelFilter = " "

        gridPartNumber.FilterExpression = " "
        gridPartNumber.SearchPanelFilter = " "
    End Sub

    Private Sub chkLikeForLike_ValueChanged(sender As Object, e As EventArgs) Handles chkLikeForLike.ValueChanged
        If chkLikeForLike.Value = True Then
            Session("everpresent_flag") = 1
        Else
            Session("everpresent_flag") = 0
        End If
    End Sub
End Class






