﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="DataSubmissionDrillDown.aspx.vb" Inherits="DataSubmissionDrillDown" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
      
    <table width="100%">
        <tr>
            <td>
                <div id="divTitle" style="position:relative; left:5px">
                    <dx:ASPxLabel
                        ID="lblPageTitle" 
                        Font-Size="Larger"
                        runat="server" 
                        Text="Data Submission Details" />
               </div>
 
               <br /> 
            </td>
        </tr>
        <tr>
            <table id="tabCharts" width="100%">
            <tr style="height:420px">
                <td style="width:60%" valign="top" >
                
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server"
                         PaletteBaseColorNumber="1" CssClass="graph_respond" Height="620px" Width="736px" AppearanceNameSerializable="Northern Lights" CrosshairEnabled="True" SideBySideEqualBarWidth="False">
                        
                        <SeriesSerializable>
                        
                            <cc1:Series ArgumentDataMember="DisplayDate" 
                                 Name="Series 1" 
                                 
                                 
                                ValueDataMembersSerializable="TransCount" LabelsVisibility="True" SeriesPointsSorting="Ascending">
                                <ViewSerializable>
                            <cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
                                                            </cc1:SideBySideBarSeriesView>
                            </ViewSerializable>
                                                            <LabelSerializable>
                            <cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" Font="Calibri, 10pt" Position="Top" ResolveOverlappingMode="Default" ShowForZeroValues="True" 
                                                                >
                                                                <FillStyle >
                                                                    <OptionsSerializable>
                            <cc1:SolidFillOptions HiddenSerializableString="to be serialized" />
                            </OptionsSerializable>
                                                                </FillStyle>
                                                            </cc1:SideBySideBarSeriesLabel>
                            </LabelSerializable>
                                                            <PointOptionsSerializable>
                            <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                            </cc1:PointOptions>
                            </PointOptionsSerializable>
                                                            <LegendPointOptionsSerializable>
                            <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                            </cc1:PointOptions>
                            </LegendPointOptionsSerializable>
                                                        </cc1:Series>
                                                    </SeriesSerializable>

                                                    <SeriesTemplate  
                             
                                                        >
                            
                                                        <ViewSerializable>
                            <cc1:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
                                                        </cc1:SideBySideBarSeriesView>
                            </ViewSerializable>

                                                        <LabelSerializable>
                            <cc1:SideBySideBarSeriesLabel HiddenSerializableString="to be serialized" LineVisible="True" >
                                                            <FillStyle >
                                                                <OptionsSerializable>
                            <cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
                            </OptionsSerializable>
                                                            </FillStyle>
                                                        </cc1:SideBySideBarSeriesLabel>
                            </LabelSerializable>

                                                        <PointOptionsSerializable>
                            <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                        </cc1:PointOptions>
                            </PointOptionsSerializable>

                                                        <LegendPointOptionsSerializable>
                            <cc1:PointOptions HiddenSerializableString="to be serialized">
                                                        </cc1:PointOptions>
                            </LegendPointOptionsSerializable>
                            
                                                    </SeriesTemplate>

                                                    <DiagramSerializable>
                            <cc1:XYDiagram PaneLayoutDirection="Horizontal" Rotated="True" LabelsResolveOverlappingMinIndent="3">
                                                        <axisx visibleinpanesserializable="-1" visibility="True" reverse="True">
                                
                                                            <tickmarks minorvisible="False" />
                                
                            <label font="Calibri, 10pt" textalignment="Near">
                                                            <resolveoverlappingoptions allowhide="False" allowstagger="False" />
                                                            </label>
                                

                            <range sidemarginsenabled="True" scrollingrange-maxvalueserializable="4" scrollingrange-minvalueserializable="0"></range>
                            
                            <VisualRange AutoSideMargins="True" auto="False" maxvalueserializable="4" minvalueserializable="0"></VisualRange>

                            <WholeRange AutoSideMargins="True" auto="False" maxvalueserializable="4" minvalueserializable="0"></WholeRange>
                           
        
                            
                                                            <numericscaleoptions autogrid="False" customgridalignment="0.30000000000000004" gridalignment="Custom" gridspacing="0.5" />
                           
        
                            
                            </axisx>


                            <axisy visibleinpanesserializable="-1">
                                
                            <range sidemarginsenabled="True"></range>
                            
                            <VisualRange AutoSideMargins="True"></VisualRange>

                            <WholeRange AutoSideMargins="True"></WholeRange>
                            
                            </axisy>


                            </cc1:XYDiagram>
                            </DiagramSerializable>

                                                    <FillStyle >
                                                    <OptionsSerializable>
                            <cc1:SolidFillOptions HiddenSerializableString="to be serialized"></cc1:SolidFillOptions>
                            </OptionsSerializable>
                                                        </FillStyle>
                                                    <Legend Visible="False">
                                                    </Legend>
                        
                        
                                                    <titles>
                                                                     <cc1:ChartTitle Text="Daily Invoice Count"  Visibility="True" Font="Calibri, 10pt" />
                                                        </titles>
              
                                                                <palettewrappers>

                                                                     <dxchartsui:PaletteWrapper Name="Palette 1" ScaleMode="Repeat">
                                                                         <palette>
                                                                             <cc1:PaletteEntry />
                                                                         </palette>
                                                                     </dxchartsui:PaletteWrapper>
                        
                                                                     <dxchartsui:PaletteWrapper Name="Palette 2" ScaleMode="Repeat">
                                                                         <palette>
                                                                             <cc1:PaletteEntry Color="58, 142, 192" Color2="58, 142, 192" />
                                                                       </palette>
                                                                    </dxchartsui:PaletteWrapper>
                                                               </palettewrappers>                        
                        
                                                </dxchartsui:WebChartControl>
             
                                            </td>





                                            <td valign="top" align="right" style="height:420px">

                                                <dx:ASPxGridView 
                                                    ID="gridData" 
                                                    runat="server" 
                                                    onload="GridStyles"
                                                    AutoGenerateColumns="False"  
                                                    Width="100%">

                                                    <SettingsPager Visible="False" Mode="ShowAllRecords" PageSize="32">
                                                    </SettingsPager>

                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Date" FieldName="DisplayDate" Width="175px" 
                                                            VisibleIndex="0">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn Caption="Invoices" FieldName="TransCount" 
                                                            VisibleIndex="1" Width="75px">
                                                            <PropertiesTextEdit DisplayFormatString="#,##0">
                                                            </PropertiesTextEdit>
                                                            <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                            <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>

                                                        <dx:GridViewDataTextColumn Caption="Sales" FieldName="TransValue" 
                                                            VisibleIndex="2" Width="100px">
                                                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                                                            </PropertiesTextEdit>
                                                            <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                                            <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
           
                                                    <Settings VerticalScrollableHeight="50" />
        
                                                    </dx:ASPxGridView>
                                            </td>
  
                                            </tr>
                                    </table>
        </tr>
    </table>
        
</div>        
</asp:Content>
          
      
