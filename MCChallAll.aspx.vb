﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class MCChall2011All

    Inherits System.Web.UI.Page

    Protected Sub dsDrillDown_Init(sender As Object, e As EventArgs) Handles dsDrillDown.Init
        dsDrillDown.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCampaignsSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaignsSummary.Init
        dsCampaignsSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim i As Integer

        Me.Title = "Toyota PARTS website - Campaign Reporting"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), Me.Title, Me.Title)
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub dsCampaigns_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns.Init
        dsCampaigns.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared
        If e.DataColumn.FieldName = "LeaguePosition" Then
            If e.GetValue("LeaguePosition") <= 3 Then
                e.Cell.Font.Bold = True
                e.Cell.BackColor = System.Drawing.Color.LightGray
            End If
        End If
    End Sub

    Protected Sub gridCampaigns_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs) Handles gridCampaigns.DetailRowGetButtonVisibility
        e.ButtonState = GridViewDetailRowButtonState.Hidden
        If CanViewDealer(Session("SelectionLevel"), Session("SelectionId"), e.KeyValue()) Then
            e.ButtonState = GridViewDetailRowButtonState.Visible
        End If
    End Sub

    Private Sub RefreshGrid()
        gridCampaigns.DataBind()
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 6 Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub gridDetail_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("MCDealerCode") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Protected Sub ddlLeague_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeague.SelectedIndexChanged
        If ddlLeague.SelectedIndex = 1 Then
            Response.Redirect("MCChall.aspx")
        End If
        If ddlLeague.SelectedIndex = 2 Then
            Response.Redirect("MCChallByZone.aspx?0")
        End If
        If ddlLeague.SelectedIndex = 3 Then
            Response.Redirect("MCChallByZone.aspx?1")
        End If
    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.FileName = "TradeChallenge_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub Summary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.Index = 1 Then
            If e.GetFieldValue("RowDescription") = "Achievement" Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "£#,##0")
            End If
        End If

    End Sub

    Protected Sub gridDrillDown_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.Index = 2 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 3 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 4 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 5 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If
        If e.DataColumn.Index = 6 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
        End If
        If e.DataColumn.Index = 7 Then
            e.Cell.BackColor = System.Drawing.Color.LightGreen
        End If

    End Sub

End Class
