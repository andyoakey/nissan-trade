﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports System
Imports System.Data
Imports System.Data.SqlClient

Partial Class TradeLoyaltyNew

    Inherits System.Web.UI.Page

    Dim nRetailValue(4) As Double
    Dim nDiscountValue(4) As Double
    Dim nThisYear As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim i As Integer

        Me.Title = "NissanDATA PARTS - Trade Loyalty"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), Me.Title, Me.Title)
            For i = Session("CurrentYear") To 2015 Step -1
                ddlYear.Items.Add(Trim(Str(i)))
            Next
            Session("TLYear") = Session("CurrentYear")
        End If
        If Session("UserEmailAddress").ToString().Contains("@qubedata.") Then
            btnNew.Visible = True
            gridMembers.Columns(8).Visible = True
            gridMembers.Columns(9).Visible = True
        Else
            btnNew.Visible = False
            gridMembers.Columns(8).Visible = False
            gridMembers.Columns(9).Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim dsMembers As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Dim htin As New Hashtable
        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        dsMembers = da.Read(sErrorMessage, "p_TradeLoyalty_Member_Count", htin)
        If Val(dsMembers.Tables(0).Rows(0).Item(0).ToString) > 0 Then
            ASPxPageControl1.Visible = True
            lblNoCust.Visible = False
        Else
            ASPxPageControl1.Visible = False
            lblNoCust.Visible = True
        End If

    End Sub

    Protected Sub dsLoyalty_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyalty.Init
        dsLoyalty.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLoyaltyDetail_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyaltyDetail.Init
        dsLoyaltyDetail.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLoyaltyDrillDownSpends_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyaltyDrillDownSpends.Init
        dsLoyaltyDrillDownSpends.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLoyaltyDrillDownRebates_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyaltyDrillDownRebates.Init
        dsLoyaltyDrillDownRebates.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLoyaltyDrillDown2_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyaltyDrillDown2.Init
        dsLoyaltyDrillDown2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLoyaltyMembers_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLoyaltyMembers.Init
        dsLoyaltyMembers.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nValue As Double

        Call GlobalRenderBrick(e)

        If e.Column.Name = "VariancePerc" Then
            If e.GetValue("LastYear") <> 0 Then
                If e.Value >= 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.LightGreen
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                End If
            End If
        End If

        If e.Column.Grid.ID = "gridLoyaltyDrillDown" Or e.Column.Grid.ID = "gridLoyaltyDrillDown1" Then
            If e.GetValue("ThisYear") > 0 And e.GetValue("Rebate") = 0 Then
                e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                e.BrickStyle.ForeColor = System.Drawing.Color.White
            End If
        End If

        If e.Column.Grid.ID = "gridLoyaltyDrillDown2" Then
            If e.Column.Name = "DiscountPerc" Then
                nValue = IIf(IsDBNull(e.GetValue("AgreedDiscount")), 0, e.GetValue("AgreedDiscount"))
                If e.Value < nValue Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                End If
            End If
        End If

    End Sub

    Protected Sub gridLoyalty_CustomColumnDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs) Handles gridLoyalty.CustomColumnDisplayText
        If e.Column.Index > 0 Then
            If e.GetFieldValue("LastYear") = 0 And e.GetFieldValue("ThisYear") = 0 Then
                e.DisplayText = ""
            End If
        End If
    End Sub

    Protected Sub gridLoyalty_DetailRowGetButtonVisibility(sender As Object, e As ASPxGridViewDetailRowButtonEventArgs)
        If (e.VisibleIndex > -1) Then
            Dim gv As ASPxGridView = CType(sender, ASPxGridView)
            If gv.GetRowValues(e.VisibleIndex, "ThisYear") = 0 Then
                e.ButtonState = GridViewDetailRowButtonState.Hidden
            End If
        End If
    End Sub

    Protected Sub AllGrids_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)
        If e.GetValue("ThisYear") > 0 And e.GetValue("Rebate") = 0 Then
            e.Cell.BackColor = System.Drawing.Color.Tomato
            e.Cell.ForeColor = System.Drawing.Color.White
        End If
        If e.DataColumn.Name = "VariancePerc" Then
            If e.CellValue >= 0 Then
                e.Cell.BackColor = System.Drawing.Color.LightGreen
            Else
                e.Cell.BackColor = System.Drawing.Color.Tomato
                e.Cell.ForeColor = System.Drawing.Color.White
            End If
        End If
    End Sub

    Protected Sub DetailGrid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)
        If e.GetValue("CentreRetail") > 0 And e.GetValue("Rebate") = 0 Then
            e.Cell.BackColor = System.Drawing.Color.Tomato
            e.Cell.ForeColor = System.Drawing.Color.White
        End If
    End Sub

    Protected Sub InvoiceGrid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)
        Dim nValue As Double
        If e.DataColumn.Name = "DiscountPerc" Then
            nValue = IIf(IsDBNull(e.GetValue("AgreedDiscount")), 0, e.GetValue("AgreedDiscount"))
            If e.CellValue < nValue Then
                e.Cell.BackColor = System.Drawing.Color.Tomato
                e.Cell.ForeColor = System.Drawing.Color.White
            End If
        End If
    End Sub

    Private Sub RefreshGrid()
        gridLoyalty.DataBind()
    End Sub

    Protected Sub btnExcel_Click()
        If ASPxPageControl1.ActiveTabIndex = 0 Then
            ASPxGridViewExporter1.FileName = "TradeLoyaltyRebates_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        Else
            ASPxGridViewExporter2.FileName = "TradeLoyaltyMembers_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
            ASPxGridViewExporter2.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        End If
    End Sub

    Protected Sub gridLoyaltyDetail_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TLMonth") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Protected Sub gridLoyaltyDrillDown1_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TLMemberId") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowFieldValues("TradeLoyaltyMemberId")
    End Sub

    Protected Sub ASPxGridViewExporter2_RenderBrick(sender As Object, e As ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter2.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub EditLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sTradeLoyaltyMemberId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "TradeLoyaltyMemberId").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}", "TradeLoyaltyAdd.aspx", sTradeLoyaltyMemberId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/vcard_edit.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ RegEditClick('{0}'); }}", sContentUrl)

    End Sub

    Protected Sub DeleteLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sTradeLoyaltyMemberId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "TradeLoyaltyMemberId").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}", "TradeLoyaltyRemove.aspx", sTradeLoyaltyMemberId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/user_block.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ RegDelClick('{0}'); }}", sContentUrl)

    End Sub

    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        PopupUserEdit.ContentUrl = "TradeLoyaltyAdd.aspx?Id=0"
        PopupUserEdit.ShowOnPageLoad = True
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sEventId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "EventId").ToString()
        Dim sInvoiceNumber As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim sContentUrl As String = String.Format("{0}?EventId={1}", "ViewVCInvoice.aspx", sEventId)

        link.NavigateUrl = "javascript:void(0);"
        link.Text = sInvoiceNumber
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}'); }}", sContentUrl)

    End Sub

    Protected Sub gridMembers_CustomColumnDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs) Handles gridMembers.CustomColumnDisplayText
        If e.Column.FieldName = "TradeLoyaltyMemberId" Then
            e.DisplayText = LinkString(e.Value)
        End If
    End Sub

    Private Function LinkString(nTradeLoyaltyMemberId As Integer) As String

        Dim dsMembers As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim sMembers As String = ""
        Dim i As Integer
        Dim htin As New Hashtable

        htin.Add("@nTradeLoyaltyMemberId", nTradeLoyaltyMemberId)
        dsMembers = da.Read(sErrorMessage, "p_TradeLoyalty2_Links", htin)
        For i = 0 To dsMembers.Tables(0).Rows.Count - 1
            If i > 0 Then
                sMembers = sMembers & "<br />"
            End If
            sMembers = sMembers & dsMembers.Tables(0).Rows(i).Item(1) & " : " & dsMembers.Tables(0).Rows(i).Item(0)
        Next
        LinkString = sMembers

    End Function

    Protected Sub LoyaltyGrids_CustomSummaryCalculate(ByRef e As DevExpress.Data.CustomSummaryEventArgs, ByRef nRetailValue As Double, ByRef nDiscountValue As Double, sField1 As String, sField2 As String, sField3 As String)

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nRetailValue = 0
            nDiscountValue = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nRetailValue = nRetailValue + e.GetValue(sField1)
            nDiscountValue = nDiscountValue + e.GetValue(sField2)
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then

            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = sField3 Then
                If nRetailValue <> 0 And nDiscountValue <> 0 Then
                    e.TotalValue = (nDiscountValue / nRetailValue)
                Else
                    e.TotalValue = 0
                End If
            End If

        End If

    End Sub

    Protected Sub gridLoyalty_CustomSummaryCalculate(sender As Object, e As DevExpress.Data.CustomSummaryEventArgs) Handles gridLoyalty.CustomSummaryCalculate
        Call LoyaltyGrids_CustomSummaryCalculate(e, nRetailValue(0), nDiscountValue(0), "LastYear", "Variance", "VariancePerc")
    End Sub

    Protected Sub gridLoyaltyDetail_CustomSummaryCalculate(sender As Object, e As DevExpress.Data.CustomSummaryEventArgs)
        Call LoyaltyGrids_CustomSummaryCalculate(e, nRetailValue(1), nDiscountValue(1), "LastYear", "Variance", "VariancePerc")
    End Sub

    Protected Sub gridLoyaltyDetail1_CustomSummaryCalculate(sender As Object, e As DevExpress.Data.CustomSummaryEventArgs)
        Call LoyaltyGrids_CustomSummaryCalculate(e, nRetailValue(2), nDiscountValue(2), "LastYear", "Variance", "VariancePerc")
    End Sub

    Protected Sub gridLoyaltyDrillDown1_CustomSummaryCalculate(sender As Object, e As DevExpress.Data.CustomSummaryEventArgs)
        Call LoyaltyGrids_CustomSummaryCalculate(e, nRetailValue(3), nDiscountValue(3), "RetailValue", "Discount", "DiscountPerc")
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("TLYear") = ddlYear.SelectedItem.Text
    End Sub

End Class
