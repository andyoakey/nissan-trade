﻿Partial Class EmailSummary
    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        gridMain.SettingsText.Title = Me.Page.Title

        lblInfo.Text = "80% of all Active customers need to have a valid* email address"
        lblInfo2.Text = "*Valid means correctly formatted and not returned as Rejected.  Where an email has been captured but Customer has opted out this is counted as valid."

        Dim dDateActiveFrom = GetDataDate("select startdate from dataperiods where id = (" & Session("currentperiod") & "- 3)")
        Session("DealerCode") = Session("SelectionId")
        gridMain.Columns(4).Caption = "Active A/Cs (Spent in last 90 Days)"

        If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then
            gridMain.Columns(0).Visible = False
            gridMain.Columns(1).Visible = False
        Else
            gridMain.Columns(0).Visible = True
            gridMain.Columns(1).Visible = True
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Dim nThisValue As Decimal = 0
        Call GlobalRenderBrick(e)
        If e.RowType = DevExpress.Web.GridViewRowType.Data Then
            If e.Column.Caption = "% Active A/Cs with email" Then
                e.BrickStyle.ForeColor = Drawing.Color.White
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value) * 100
                    If nThisValue < 80 Then
                        e.BrickStyle.BackColor = Drawing.Color.Red
                    Else
                        e.BrickStyle.BackColor = Drawing.Color.Green
                    End If
                End If
            End If
        End If

    End Sub

    Protected Sub gridDashboardTotal_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridMain.HtmlDataCellPrepared
        Dim nThisValue As Decimal = 0

        If e.DataColumn.FieldName = "percent_active_email" Then
            e.Cell.ForeColor = Drawing.Color.White
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue) * 100
                If nThisValue < 80 Then
                    e.Cell.BackColor = Drawing.Color.Red
                Else
                    e.Cell.BackColor = Drawing.Color.Green
                End If
            End If
        End If
    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "EmailSummaryDealer" & Session("DealerCode")
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
