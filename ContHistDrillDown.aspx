﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContHistDrillDown.aspx.vb" Inherits="ContHistDrillDown" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link rel="stylesheet" href="styles/master.css" />

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dxe:ASPxButton ID="btnExcel" AutoPostBack="true" ClientVisible="true" ClientInstanceName="btnExcel"
            runat="server" EnableDefaultAppearance="false" ToolTip="Export To Excel"
            Text="" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/Excel.png"
            Image-UrlHottracked="~/Images2014/Excel-clicked.png" Image-UrlPressed="~/Images2014/Excel-clicked.png"
            Image-Height="30px" CssClass="special">
        </dxe:ASPxButton>

        <dxwgv:ASPxGridView ID="gridContHistDrillDown" runat="server" style="position:relative; left:5px; "  Font-Size="7pt" AutoGenerateColumns="False" Width="99%">
            
            <Styles>
                <Header Font-Bold = "True" ImageSpacing="5px" SortingImageSpacing="5px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" Font-Size = "7pt" />
                <Footer Font-Bold = "True" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" Font-Size = "7pt" />
                <Cell   HorizontalAlign="Center" Wrap="True" />
            </Styles>

            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>

            <Settings
                ShowFooter="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False"
                ShowGroupFooter="VisibleIfExpanded"
                UseFixedTableLayout="True" VerticalScrollableHeight="460"
                ShowVerticalScrollBar="True" />

            <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000"
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
           
            <Columns>
            
                <dxwgv:GridViewDataTextColumn Caption="Date" FieldName="ContactDate" Name="ContactDate" VisibleIndex="0" ExportWidth="100"/>
                
                <dxwgv:GridViewDataTextColumn Caption="Type" FieldName="ContactType" Name="ContactType" VisibleIndex="1" ExportWidth="100" />

                <dxwgv:GridViewDataHyperLinkColumn Caption="Name" FieldName="CustomerId" Name="CustomerId" VisibleIndex="2" Width="25%" ExportWidth="300">
                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" Wrap="True" />
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="ViewCustomerDetails.aspx?0{0}" Target="_top" TextField="BusinessName">
                        <Style Font-Size="X-Small" HorizontalAlign="Left" />
                    </PropertiesHyperLinkEdit>
                </dxwgv:GridViewDataHyperLinkColumn>

                <dxwgv:GridViewDataTextColumn Caption="Message" FieldName="ContactMessage" Name="ContactMessage" VisibleIndex="3" Width="35%" ExportWidth="300">
                    <HeaderStyle Wrap="False" HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left" Wrap="True" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Contacted By" FieldName="WebUserName" Name="WebUserName" VisibleIndex="4"  ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="T/C" FieldName="TradeClubMember" Name="TradeClubMember" VisibleIndex="5" Width="5%" />

                <dxwgv:GridViewDataTextColumn Caption="CustomerId" FieldName="CustomerId" Name="CustomerId2" VisibleIndex="6" Visible="False" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="Follow up" FieldName="ReminderDate" Name="ReminderDate" VisibleIndex="7" ExportWidth="100" />

            </Columns>
           
        </dxwgv:ASPxGridView>

        <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridContHistDrillDown"  PreserveGroupRowStates="False">
        </dxwgv:ASPxGridViewExporter>

    </div>
    </form>
</body>
</html>
