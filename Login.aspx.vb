﻿Imports System.Data
Imports System.Text
Imports System.Security.Cryptography
Imports System.Net.Mail
Imports DevExpress.Data.Linq
Imports QubeSecurity

Partial Class Login

    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        DevExpress.Web.ASPxWebControl.SetIECompatibilityMode(8)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site"

        If Request.UrlReferrer Is Nothing AndAlso Not IsPostBack Then
            Session.Clear()
            Session.Abandon()
        End If

        If Request.QueryString("msg") <> "" Then
            divPwdChangedNotice.Visible = True
        End If

        Dim txtUsername As TextBox = CType(login.FindControl("Username"), TextBox)
        Dim txtPassword As TextBox = CType(login.FindControl("Password"), TextBox)
        Dim btnLogin As Button = CType(login.FindControl("btnLogin"), Button)

        If IsSystemOpen() Then
            If TestDBConnection() = False Then
                txtUsername.Enabled = False
                txtPassword.Enabled = False
                btnLogin.Text = "CLOSED FOR MAINTENANCE"
            End If
        Else
            txtUsername.Enabled = False
            txtPassword.Enabled = False
            btnLogin.Text = "SYSTEM UNAVAILABLE! PLEASE TRY LATER"
        End If

        Response.Cookies("ValueChain").Expires = DateTime.Now.AddDays(-1)

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim returnValue As String = Authentication.LoginMigrateUser(login.UserName, login.Password)

        If Not String.IsNullOrWhiteSpace(returnValue) Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Login", "Unsuccessful login attempt - " & login.UserName)
            lblErrorMessage.Text = returnValue
            divErrorMessage.Visible = True
        Else
            'Valid login
            Call SetSessionVars(login.UserName)

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Login", "Successful login.")
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Login IP", Request.UserHostAddress)
            
            FormsAuthentication.RedirectFromLoginPage(login.UserName, False)
        End If

    End Sub


    Private Sub SetSessionVars(ByVal username As String)

        Dim user As MembershipUser = Membership.GetUser(username)
        Dim dtWebUser As DataTable = WebUser.GetUser(user)

        Session("UserID") = dtWebUser.Rows(0).Item("UserId")
        Session("UserLevel") = Left(dtWebUser.Rows(0).Item("AccessLevel"), 1)
        Session("SelectionLevel") = Left(dtWebUser.Rows(0).Item("AccessLevel"), 1)
        Session("SelectionId") = Mid(dtWebUser.Rows(0).Item("AccessLevel"), 2)
        Session("DealerCode") = Session("SelectionId")
        Session("SuperUser") = dtWebUser.Rows(0).Item("SuperUser")
        Session("SuperAdmin") = If(user.Email.Contains("qubedata"), True, False)
        Session("UserEmailAddress") = user.Email
        Session("JustLoggedIn") = 1
        Session("CurrentSelection") = 0
        Session("CurrentPeriod") = CurrentPeriodId()
        Session("CurrentYear") = CurrentYear()
        Session("CurrentMonth") = CurrentMonth()
        Session("ButtonHeight") = 110
        Session("ButtonWidth") = 112


    End Sub


    Private Function TestDBConnection() As Boolean
        Dim da As New DatabaseAccess
        Dim ds As DataSet
        Dim sErr As String = ""
        Try
            ds = da.ExecuteSQL(sErr, "SELECT * FROM System")
        Catch ex As Exception
            sErr = ex.Message
        End Try
        If sErr.Length <> 0 Then
            TestDBConnection = False
        Else
            TestDBConnection = True
        End If
    End Function


    Private Function IsSystemOpen() As Boolean
        Dim objFSO = Server.CreateObject("Scripting.FileSystemObject")
        Dim objTextStream = objFSO.OpenTextFile(Request.ServerVariables("APPL_PHYSICAL_PATH") & "SysStatus.txt", 1)
        Dim sSysUpOrDown As String = UCase(Trim(objTextStream.Readline))
        Dim sMessage As String = Trim(objTextStream.Readline)
        If sSysUpOrDown = "CLOSED" Then
            IsSystemOpen = False
        Else
            IsSystemOpen = True
        End If
    End Function


    Private Function GetManufacturer() As String

        Dim objFSO = Server.CreateObject("Scripting.FileSystemObject")
        Dim objTextStream = objFSO.OpenTextFile(Request.ServerVariables("APPL_PHYSICAL_PATH") & "Manu.txt", 1)
        Dim sManu As String = UCase(Trim(objTextStream.Readline))
        GetManufacturer = sManu

    End Function


    Protected Sub btnSendPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendPassword.Click

        System.Threading.Thread.Sleep(2000)
        
        Dim username As String = txtEmailAddress.Text
        'Dim returnMessage = "If we find a user account with that email address you will receive a password reset link."
        
        Dim returnMessage as string = Authentication.ResetPassword(username)

        lblErrorMessage.Text = returnMessage
        divErrorMessage.Visible = True
        btnSendPassword.Enabled = True
        txtEmailAddress.Text = ""

    End Sub
    

End Class

