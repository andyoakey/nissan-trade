﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting


Partial Class ViewBestSellingParts

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Best Selling Parts"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Best Selling Parts", "Best Selling Parts")
            ddlTA.SelectedIndex = 0
            Session("Focus") = 0
            Session("TA") = "All Products"
            Call RefreshGrid()
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        dsTradeAnalysis.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsBestParts_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsBestParts.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        dsBestParts.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub
    Protected Sub ddlSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged, ddlTA.SelectedIndexChanged, ddlPeriod.SelectedIndexChanged, ddlTopN.SelectedIndexChanged, ddlBasis.SelectedIndexChanged, ddlType.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Name = "Margin" Then
            If ddlBasis.SelectedIndex = 3 Then
                e.DisplayText = Format(e.Value, "£#,##0")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If
    End Sub

    Private Sub RefreshGrid()
        Session("Level4_Code") = ddlProductGroup.Value

        If ddlTA.Value Is Nothing Then
        Else
            Session("TA") = ddlTA.Value
        End If

        Session("Period") = ddlPeriod.Value

        Session("Items") = ddlTopN.Value

        Session("Basis") = ddlBasis.Value

        If ddlType.Value Is Nothing Then
        Else
            Session("Focus") = ddlType.Value
        End If

        gridBestParts.DataBind()
        If ddlBasis.SelectedIndex = 3 Then
            gridBestParts.Columns("Margin").Caption = "Profit (£)"
        Else
            gridBestParts.Columns("Margin").Caption = "Margin (%)"
        End If
    End Sub

    Protected Sub ExpBestParts_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ExpBestParts.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        ExpBestParts.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStylesSearchOff(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

End Class
