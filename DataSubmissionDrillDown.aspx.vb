﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail
Imports Microsoft.VisualBasic
Imports DevExpress.XtraCharts

Partial Class DataSubmissionDrillDown

    Inherits System.Web.UI.Page

    Dim sDealerCode As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - DMS Data Drill Down"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        sDealerCode = Request.QueryString(0)
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "DMS Submissions Drill Down", "DMS Submission Breakdown : " & sDealerCode)
        End If
        Call CreateChart()
    End Sub

    Private Sub CreateChart()

        ' Create a pie series.
        Dim series1 As New Series("Detailed Month Breakdown for Dealer: " & sDealerCode, ViewType.Bar)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@sDealerCode", sDealerCode)
        htin.Add("@nMonth", Session("MonthFrom"))
        ds = da.Read(sErrorMessage, "p_Full_Transaction_Chart", htin)
        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                series1.Points.Add(New SeriesPoint(.Rows(i).Item("DisplayDate"), .Rows(i).Item("TransCount")))
            Next
        End With

        ' Add the series to the chart.
        If WebChartControl1.Series.Count > 0 Then
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End If

        WebChartControl1.Series.Add(series1)

        ' Adjust the point options of the series.

        Dim objTitle As New DevExpress.XtraCharts.ChartTitle
        WebChartControl1.Titles.Clear()
        WebChartControl1.Titles.Add(objTitle)

        gridData.DataSource = ds.Tables(0)
        gridData.DataBind()

    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
