﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail
Imports Microsoft.VisualBasic
Imports DevExpress.XtraCharts

Partial Class CustomerReport

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Customer Report"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer Report", "Viewing Customer Report")
        End If
    End Sub

    Private Sub CreateChart()

        ' Create a pie series.
        Dim series1 As New Series("Pie Chart - Customer Status Breakdown", ViewType.Pie3D)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        ds = da.Read(sErrorMessage, "p_CustomerTrends_PieChart", htin)
        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                series1.Points.Add(New SeriesPoint(.Rows(i).Item("RowTitle"), .Rows(i).Item("CustCount")))
            Next
        End With

        ' Add the series to the chart.
        If WebChartControl1.Series.Count > 0 Then
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End If

        WebChartControl1.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")
        WebChartControl1.Series.Add(series1)

        ' Adjust the point options of the series.
        series1.PointOptions.PointView = PointView.ArgumentAndValues
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent
        series1.PointOptions.ValueNumericOptions.Precision = 0

        ' Access the view-type-specific options of the series.
        Dim myView As Pie3DSeriesView = CType(series1.View, Pie3DSeriesView)

        ' Show a title for the series.
        myView.Titles.Add(New SeriesTitle())
        myView.Titles(0).Text = series1.Name

        ' Specify a data filter to explode points.
        myView.ExplodedPointsFilters.Add(New SeriesPointFilter _
            (SeriesPointKey.Value_1, DataFilterCondition.GreaterThanOrEqual, 9))
        myView.ExplodedPointsFilters.Add(New SeriesPointFilter _
            (SeriesPointKey.Argument, DataFilterCondition.NotEqual, "Others"))
        myView.ExplodeMode = PieExplodeMode.UseFilters
        myView.ExplodedDistancePercentage = 30

        ' Hide the legend (if necessary).
        WebChartControl1.Legend.Visible = False

        gridData.DataSource = ds.Tables(0)
        gridData.DataBind()

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Call CreateChart()
    End Sub

End Class
