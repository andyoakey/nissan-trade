﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NationwideAssign.aspx.vb" Inherits="NationwideAssign"  MasterPageFile="~/MasterPage.master"%>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

<div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative; left:0px">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                runat="server" 
                Text="Assign Account To A Nationwide Site " 
                Font-Size="Larger" />
        </div>

    <br />

    <table width="100%">
        <tr>
            <td width ="30%" valign="top">
                 <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" DataSourceID="dsSingleNationwide">
        <Items>
         <dx:LayoutGroup Caption="Account Information" GroupBoxDecoration="Box" >
                <Items>
                    <dx:LayoutItem Caption="Dealer Code" FieldName="dealercode">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtDealerCode" runat="server" Width="200px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
          
                    <dx:LayoutItem Caption="Dealer Name" FieldName="dealername">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtDealerName" runat="server" Width="200px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="A/C Code" FieldName="link">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtAccountCode" runat="server" Width="200px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="A/C Name" FieldName="accountname">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtAccountName" runat="server" Width="300px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="A/C Address" FieldName="fulladdress">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtAddress" runat="server" Width="300px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="Postcode" FieldName="postcode">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtPostCode" runat="server" Width="100px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="Email Address" FieldName="email">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtEmail" runat="server" Width="300px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="Nationwide Site ID" FieldName="siteid">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="txtSiteID" runat="server" Width="100px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

                    <dx:LayoutItem Caption="Nationwide Site Name" FieldName="sitename">
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="300px" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>
                </Items>
            </dx:LayoutGroup>
        </Items>

        <Items>
         <dx:LayoutGroup Caption="Action" GroupBoxDecoration="Box" >

                <Items>
                    <dx:LayoutItem Caption="Possible Actions" >
                        <LayoutItemNestedControlCollection>
                            <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxButton  
                                    ID="btnCreate"
                                    Text="Create Link" 
                                    runat="server" />

                                <dx:ASPxButton  
                                    ID="btnRemove"
                                    Text="Remove From Possible List" 
                                    runat="server" />
                            </dx:LayoutItemNestedControlContainer>
                        </LayoutItemNestedControlCollection>
                    </dx:LayoutItem>

          
                </Items>
            </dx:LayoutGroup>
        </Items>




    </dx:ASPxFormLayout>
            </td>
  
            <td width ="70%">
                 <dx:ASPxGridView  
        ID="gridSites" 
        CssClass="grid_styles"
        OnLoad="GridStyles" 
        style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsNationwideList" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Styles-Footer-HorizontalAlign="Center"
        Styles-SelectedRow-BackColor="#0066ff"
        Styles-SelectedRow-Forecolor="White"
        SettingsText-Title="Nationwide Sites"
        KeyFieldName="siteid"
        Width=" 100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  AllowSelectSingleRowOnly="true" />

        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true">
        </SettingsPager>

        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="False"
            ShowTitlePanel="True" />
                      
        <Columns>

             <dx:GridViewCommandColumn 
                   ShowSelectCheckbox="True" 
                   Caption="Select Site"
                   Width  ="10%" 
                   VisibleIndex="0">
             </dx:GridViewCommandColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="100"
                Caption="Site ID" 
                Name="siteid"
                FieldName="siteid">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                Width="25%" 
                exportwidth="300"
                Caption="Site Name" 
                FieldName="sitename">
            </dx:GridViewDataTextColumn>
  
            <dx:GridViewDataTextColumn
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                Width="20%" 
                exportwidth="200"
                Caption="Telephone" 
                FieldName="tel">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                Width="10%" 
                exportwidth="200"
                Caption="Postcode" 
                FieldName="postcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="5" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                Width="25%" 
                exportwidth="200"
                Caption="Email Address" 
                FieldName="email">
            </dx:GridViewDataTextColumn>


         </Columns>
                    
    </dx:ASPxGridView>
            </td>
        </tr>

    </table>
</div>

  <asp:SqlDataSource 
        ID="dsSingleNationwide" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_SelectNationwideSingle" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter  DefaultValue=""  Name="ncustomerdealerid"  SessionField ="ThisCustomerDealerID" Type="Int64" />
        </SelectParameters>
    </asp:SqlDataSource>

  <asp:SqlDataSource 
        ID="dsNationwideList" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_NationwideSiteList" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
 



</asp:Content>
