﻿Imports System.Data
Imports System.Net.Mail

Partial Class VisitActionActivityEdit

    Inherits System.Web.UI.Page

    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nId = Val(Request.QueryString(0))
        If Not Page.IsPostBack Then
            If nId <> 0 Then
                Call FetchDetails()
            End If
        End If

    End Sub

    Private Sub FetchDetails()

        Dim da As New DatabaseAccess, ds As DataSet, sErr, sSQL As String

        sSQL = "SELECT * FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId))
        ds = da.ExecuteSQL(sErr, sSQL)

        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                txtActivity.Text = "" & .Item("Activity")
                txtComments.Text = "" & .Item("Comments")
                txtResponsibility.Text = "" & .Item("Responsibility")
                txtTargetDate.Date = CDate("" & .Item("TargetDate"))
            End With
        End If

    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewVisitLine.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim nVARId = Val(Request.QueryString(0))
        Dim sTargetDate As String = Format(CDate(txtTargetDate.Text), "dd-MMM-yyyy")

        If txtActivity.Text <> "" And txtComments.Text <> "" And txtResponsibility.Text <> "" And sTargetDate <> "" Then

            htIn.Add("@nId", nId)
            htIn.Add("@sActivity", txtActivity.Text)
            htIn.Add("@sComments", txtComments.Text)
            htIn.Add("@sResponsibility", txtResponsibility.Text)
            htIn.Add("@sTargetDate", sTargetDate)
            htIn.Add("@nUserId", Session("UserId"))

            nRes = da.Update(sErr, "p_VAR_Detail_Upd", htIn)

            If sErr.Length = 0 Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Activity updated", "Id : " & nId)
                Dim startUpScript As String = String.Format("window.parent.HideVisitActionActivityWindow();")
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
            Else
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If
        End If

    End Sub

End Class
