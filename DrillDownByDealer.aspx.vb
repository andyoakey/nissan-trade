Partial Class DrillDownByDealer
    Inherits System.Web.UI.Page
    Dim sThisPeriod As String = ""
    Dim sLastPeriod As String = ""
    Dim querytitle As String = ""

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Page.IsPostBack = False Then
            Session("nFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O'")
            Session("nTo") = Session("nFrom")
            Session("nProductLevel") = 0
            Session("TA") = "All Products"
            Session("Level4_Code") = "ALL"
            tlDealerDrillDown.DataBind()
            tlDealerDrillDown.ExpandToLevel(2)
            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
        End If

        If Session("nFrom") <> Session("nTo") Then
            sThisPeriod = GetPeriodName(Session("nFrom")) & " to " & GetPeriodName(Session("nTo"))
            If Session("nFrom") = GetDataDecimal("Select min(id) from dataperiods where periodstatus = 'U'") Then
                sThisPeriod = GetPeriodName(Session("nFrom")) & " to " & GetPeriodName(Session("nTo")) & " (to date)"
            End If
            sLastPeriod = GetPeriodName(Session("nFrom") - 12) & " to " & GetPeriodName(Session("nTo") - 12)
        Else
            sThisPeriod = GetPeriodName(Session("nFrom"))
            If Session("nFrom") = GetDataDecimal("Select min(id) from dataperiods where periodstatus = 'U'") Then
                sThisPeriod = GetPeriodName(Session("nFrom")) & " (to date)"
            End If
            sLastPeriod = GetPeriodName(Session("nFrom") - 12)
        End If

        Me.tlDealerDrillDown.Columns(0).Caption = "Region/Zone/Dealer"
        Me.tlDealerDrillDown.Columns("totalsalesthis").Caption = sThisPeriod
        Me.tlDealerDrillDown.Columns("totalsaleslast").Caption = sLastPeriod

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        If Session("nProductLevel") = 0 Then
            ddlTA.SelectedIndex = 0
            ddlProductGroup.SelectedIndex = 0
        End If

        If Session("nProductLevel") = 1 Then
            ddlProductGroup.SelectedIndex = 0
        End If

        If Session("nProductLevel") = 2 Then
            ddlTA.SelectedIndex = 0
        End If

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "DealerDrillDown"
        ASPxTreeListExporter1.FileName = sFileName
        ASPxTreeListExporter1.WriteXlsxToResponse()
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.ASPxTreeListExportRenderBrickEventArgs) Handles ASPxTreeListExporter1.RenderBrick

        Dim nThisValue As Decimal = 0
        Dim nThisColCount As Integer = 0

        Call GlobalRenderBrickTreeList(e)

        If e.Column.Name = "yonysalespc" Then
            If e.RowKind <> DevExpress.Web.ASPxTreeList.TreeListRowKind.Header Then
                If IsDBNull(e.TextValue) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.TextValue)
                End If
                e.BrickStyle.ForeColor = Drawing.Color.White
                If nThisValue = 0 Then
                    e.BrickStyle.BackColor = Drawing.Color.White
                    e.BrickStyle.ForeColor = Drawing.Color.Black
                ElseIf nThisValue > 1 Then
                    e.BrickStyle.BackColor = Drawing.Color.Green
                ElseIf nThisValue > 0.9 Then
                    e.BrickStyle.BackColor = Drawing.Color.Orange
                Else
                    e.BrickStyle.BackColor = Drawing.Color.Red
                End If
            End If
        End If

    End Sub

    Protected Sub tlDealerDrillDown_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxTreeList.TreeListHtmlDataCellEventArgs) Handles tlDealerDrillDown.HtmlDataCellPrepared

        If e.Column.Name = "yonysalespc" Then
            Dim nThisValue As Decimal = 0
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.ForeColor = Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = Drawing.Color.White
                e.Cell.ForeColor = Drawing.Color.Black
            ElseIf nThisValue > 1 Then
                e.Cell.BackColor = Drawing.Color.Green
            ElseIf nThisValue > 0.9 Then
                e.Cell.BackColor = Drawing.Color.Orange
            Else
                e.Cell.BackColor = Drawing.Color.Red
            End If
        End If

    End Sub

    Protected Sub ddlSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged, ddlProductGroup.SelectedIndexChanged

        If sender.id = "ddlProductGroup" Then
            Session("nProductLevel") = 2
            Session("Level4_Code") = ddlProductGroup.Value
            If Session("Level4_Code") = "ALL" Then
                Session("nProductLevel") = 0
            End If
        End If

        If sender.id = "ddlTA" Then
            Session("nProductLevel") = 1
            Session("TA") = ddlTA.Value
            If Session("TA") Is Nothing Or Session("TA") = "All Products" Then
                Session("nProductLevel") = 0
            End If
        End If

        tlDealerDrillDown.DataBind()
        tlDealerDrillDown.ExpandToLevel(2)
    End Sub

    Private Sub ddlFrom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("nFrom") = ddlFrom.Value
    End Sub
    Private Sub ddlTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("nTo") = ddlTo.Value
    End Sub

End Class
