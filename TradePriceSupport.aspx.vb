Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Web

Partial Class TradePriceSupport

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        lblSubTitle.Text = "* Selected sales period versus corresponding period 12 months earlier."

        Session("nDataPeriod") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'") + 1


        If Page.IsPostBack = False Then
            Session("TPSFrom") = 126
            Session("TPSTo") = Session("nDataPeriod")
            cboMonthFrom.SelectedIndex = Session("nDataPeriod") - 127

            Session("TPSPeriod") = 0
            Session("ActiveTab") = 0
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub

    Protected Sub ExportToExcel()
        Select Case Session("ActiveTab")
            Case 0
                Call ExportToExcel1("TradePriceSupport_Dealers", ASPxGridViewExporter1)
            Case 1
                Call ExportToExcel1("TradePriceSupport_ProductGroups", ASPxGridViewExporter2)
            Case 2
                Call ExportToExcel1("TradePriceSupport_Parts", ASPxGridViewExporter3)
        End Select
    End Sub

    Protected Sub ExportToExcel1(sFileName As String, sExporter As ASPxGridViewExporter)
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(sExporter)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub pageControl_ActiveTabChanged(source As Object, e As TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged
        Session("ActiveTab") = e.Tab.Index
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles _
        ASPxGridViewExporter1.RenderBrick,
        ASPxGridViewExporter2.RenderBrick,
        ASPxGridViewExporter3.RenderBrick
        Call GlobalRenderBrick(e)

        Dim nThisValue As Integer = 0

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            If (e.Column.VisibleIndex = 9 Or e.Column.VisibleIndex = 10) And (e.Column.Caption = "Units" Or e.Column.Caption = "Invoice Value") Then
                e.BrickStyle.ForeColor = Color.White
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value)
                End If
                If nThisValue = 0 Then
                    e.BrickStyle.BackColor = Color.LightGray
                    e.BrickStyle.ForeColor = Color.Black
                ElseIf nThisValue >= 1 Then
                    e.BrickStyle.BackColor = Color.Green
                ElseIf nThisValue >= 0.9 Then
                    e.BrickStyle.BackColor = Color.Orange
                    e.BrickStyle.ForeColor = Color.Black
                Else
                    e.BrickStyle.BackColor = Color.Red
                End If
            End If

            If e.Column.Caption = "Rebate Earned" And e.Column.VisibleIndex = 12 Then
                e.BrickStyle.ForeColor = Color.White
                e.BrickStyle.BackColor = Color.Green
            End If

            If e.Column.Caption = "Rebate Missed" And e.Column.VisibleIndex = 12 Then
                e.BrickStyle.ForeColor = Color.White
                e.BrickStyle.BackColor = Color.Red
            End If

        End If
    End Sub
    Protected Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSummary_Dealer.HtmlDataCellPrepared
        Dim nThisValue As Integer = 0

        If Mid(e.DataColumn.FieldName, 1, 6) = "uplift" Then
            e.Cell.ForeColor = Color.White

            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If

            If nThisValue < 0 Then
                e.Cell.BackColor = Color.Crimson
            End If

            If nThisValue = 0 Then
                e.Cell.ForeColor = Color.Black
            End If

            If nThisValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If

        End If
    End Sub

    Protected Sub gridInvoices_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("TPSDealerCode") = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
        Session("DealerCode") = Session("AccessCode")
    End Sub

    Protected Sub gridTransactions_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKeyField As String = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
        Session("TPSRefCode") = Microsoft.VisualBasic.Left(sKeyField, InStr(sKeyField, "|") - 1)
        Session("TPSInvoiceNumber") = Mid(sKeyField, InStr(sKeyField, "|") + 1)
    End Sub

    Protected Sub gridSummary_PG_Dealer_BeforePerformDataSelect(sender As Object, e As EventArgs)
        ' Session("TPSDealerCode") = Session("AccessCode")
        Session("TPPartNumber") = "ALL"
        Dim sKeyField As String = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
        Session("TPSLevel2_Code") = sKeyField
    End Sub

    Protected Sub gridSummary_Part_Dealer_BeforePerformDataSelect(sender As Object, e As EventArgs)
        'Session("TPSDealerCode") = Session("AccessCode")
        Session("TPSLevel2_Code") = "ALL"
        Dim sKeyField As String = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
        Session("TPPartNumber") = sKeyField
    End Sub

    Protected Sub gridTransactions_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        '     If Trim(e.GetValue("colour_flag")) <> "white" Then
        '          e.Cell.BackColor = Color.Yellow
        '           e.Cell.Font.Bold = True
        '        End If

        If e.DataColumn.FieldName = "rebate_value" Then
            If Trim(e.GetValue("colour_flag")) = "green" Then
                e.Cell.BackColor = Color.Green
                e.Cell.ForeColor = Color.White
                e.Cell.Font.Bold = True
            End If

            If Trim(e.GetValue("colour_flag")) = "red" Then
                e.Cell.BackColor = Color.Crimson
                e.Cell.ForeColor = Color.White
                e.Cell.Font.Bold = True
            End If
        End If

    End Sub

    Protected Sub gridInvoices_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "applicablerebate" Then
            If e.GetValue("campaignprice") >= e.GetValue("nettprice") Then
                e.Cell.BackColor = Color.Green
                e.Cell.ForeColor = Color.White
                e.Cell.Font.Bold = True
            End If

            If e.GetValue("campaignprice") < e.GetValue("nettprice") Then
                e.Cell.BackColor = Color.Crimson
                e.Cell.ForeColor = Color.White
                e.Cell.Font.Bold = True
            End If
        End If

    End Sub

    Protected Sub cboMonth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMonthFrom.SelectedIndexChanged, cboMonthTo.SelectedIndexChanged
        Session("TPSFrom") = cboMonthFrom.Value
        Session("TPSTo") = cboMonthTo.Value
    End Sub

    Protected Sub gridInvoicesGood_Unload(sender As Object, e As EventArgs)
        Session("TPSDealerCode") = Session("AccessCode")
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStyles2(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

End Class
