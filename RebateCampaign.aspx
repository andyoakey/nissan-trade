﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RebateCampaign.aspx.vb" Inherits="RebateCampaign" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


     <div style="position: relative; font-family: Calibri; text-align: left;" >

       <div id="divTitle" style="position:relative;">
                    <dx:ASPxLabel 
                        Font-Size="Larger"
                        ID="lblPageTitle" 
                        runat="server" 
                        Text="Parts Basket Campaign" />
       </div>
    
       <br />  

        <table style="width: 50%;">
                 <tr runat="server"  >
                <td width="150px" >
                    From
                </td>

                <td width="30px">

                </td>
                
                <td width="150px" >
                    To
                </td>

                <td width="30px">

                </td>
        
                <td width="300px">
                Round to Nearest £
                </td>
            </tr>

            <tr runat="server"  >
                <td width="150px" >
                <dx:ASPxComboBox 
                        id ="ddlFrom"
                        AutoPostBack="True"
                        runat="server">
                    </dx:ASPxComboBox>
                </td>

                <td width="30px">

                </td>
                
                <td width="150px" >
                    <dx:ASPxComboBox 
                        id ="ddlTo"
                        AutoPostBack="True"
                        runat="server">
                    </dx:ASPxComboBox>
                </td>

                <td width="30px">

                </td>
       
               <td width="300px">
                        <dx:ASPxCheckBox
                ID="chkRound"               
                runat="server"
                AutoPostBack="true"
                OnCheckedChanged="chkRound_CheckedChanged"
                Checked="false"
                Text=" ">
                        </dx:ASPxCheckBox>
                </td>
            </tr>
       </table>        





       <br />

        <dx:ASPxGridView 
            ID="gridCampaigns" 
            runat="server" 
            Width="100%" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode" 
            Style="margin-left:5px">

            <SettingsPager AllButton-Visible="true" Mode="ShowPager" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="12" />

            <Settings ShowFooter="True" ShowHeaderFilterButton="False" ShowStatusBar="Auto" ShowTitlePanel="False" ShowFilterBar="Hidden"
                ShowGroupFooter="VisibleIfExpanded" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000"
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

            <Styles>
                <Header Wrap="True" HorizontalAlign="Center" />
                <Cell HorizontalAlign="Center" />
                <Footer HorizontalAlign="Center" />
            </Styles>

            <Columns>

                <dx:GridViewDataTextColumn  Caption="Dealer" FieldName="DealerCode" VisibleIndex="0"  Width="4%" ExportWidth="100" />
                <dx:GridViewDataTextColumn Caption="Name" FieldName="DealerName" VisibleIndex="1" Width="15%" CellStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" CellStyle-Wrap="False" ExportWidth="200" />

                <dx:GridViewBandColumn Caption="Rebated Sales"> 
                    <Columns>
                        <dx:GridViewDataTextColumn 
                            Caption="Units Sold" 
                            FieldName="Rebated_Units" 
                            Name="Rebated_Units" 
                            VisibleIndex="2" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Invoiced Value" 
                            FieldName="Rebated_Value" 
                            Name="Rebated_Value" 
                            VisibleIndex="3" 
                            ExportWidth="100" 
                            Width="6%"/>

                        <dx:GridViewDataTextColumn 
                            Caption="Cost To Dealer" 
                            FieldName="Rebated_Cost" 
                            Name="Rebated_Cost" 
                            VisibleIndex="4" 
                            ExportWidth="100" 
                            Width="6%"/>
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Rebate Payable" 
                            FieldName="Rebate" 
                            Name="Rebate" 
                            VisibleIndex="5" 
                            ExportWidth="100" 
                            Width="6%"/>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Non-rebated Sales"> 
                    <Columns>
                        <dx:GridViewDataTextColumn 
                            Caption="Units Sold" 
                            FieldName="Missed_Units" 
                            Name="Missed_Units" 
                            VisibleIndex="6" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />

                        <dx:GridViewDataTextColumn 
                            Caption="Invoiced Value" 
                            FieldName="Missed_Value" 
                            Name="Missed_Value" 
                            VisibleIndex="7" 
                            ExportWidth="100" 
                            Width="6%"/>

                        <dx:GridViewDataTextColumn 
                            Caption="Cost To Dealer" 
                            FieldName="Missed_Cost" 
                            Name="Missed_Cost" 
                            VisibleIndex="8" 
                            ExportWidth="100" 
                            Width="6%" />
                        
                        <dx:GridViewDataTextColumn 
                            Caption="Rebate Missed" 
                            FieldName="Missed_Rebate" 
                            Name="Missed_Rebate" 
                            VisibleIndex="9" 
                            ExportWidth="100" 
                            Width="6%"/>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Total Campaign Sales" ToolTip="Compares Sales of Rebate Items With 12 Months Ago"> 
                    <Columns>
                        <dx:GridViewBandColumn Caption="Units Sold" > 
                            <Columns>
                                <dx:GridViewDataTextColumn 
                            Caption="This Year" 
                            FieldName="Units_This" 
                            Name="Units_This" 
                            VisibleIndex="14" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                                    
                                <dx:GridViewDataTextColumn 
                            Caption="Last Year" 
                            FieldName="Units_Last" 
                            Name="Units_Last" 
                            VisibleIndex="15" 
                            ExportWidth="100" 
                            Width="4%"
                            PropertiesTextEdit-DisplayFormatString="#,##0" />
                          
                                <dx:GridViewDataTextColumn 
                            Caption="Uplift" 
                            FieldName="Units_Diff" 
                            Name="Units_Diff" 
                            VisibleIndex="16" 
                            ExportWidth="100"
                            Width="5%" 
                            PropertiesTextEdit-DisplayFormatString="#,##0" />

                                <dx:GridViewDataTextColumn 
                            Caption="% Growth" 
                            FieldName="Units_DiffPerc" 
                            Name="Units_DiffPerc" 
                            VisibleIndex="17" 
                            ExportWidth="100" 
                            Width="5%"
                            PropertiesTextEdit-DisplayFormatString="0.0%"/>
                           </Columns>
                       </dx:GridViewBandColumn>
                    
                        <dx:GridViewBandColumn Caption="Cost To Dealer"> 
                            <Columns>
                                <dx:GridViewDataTextColumn 
                            Caption="This Year" 
                            FieldName="Cost_This" 
                            Name="Cost_This" 
                            VisibleIndex="10" 
                            ExportWidth="100"
                            Width="6%" />

                                <dx:GridViewDataTextColumn 
                            Caption="Last Year" 
                            FieldName="Cost_Last" 
                            Name="Cost_Last" 
                            VisibleIndex="11" 
                            ExportWidth="100" 
                            Width="6%"/>
                          
                                <dx:GridViewDataTextColumn 
                            Caption="Uplift" 
                            FieldName="Cost_Diff" 
                            Name="Cost_Diff" 
                            VisibleIndex="12" 
                            ExportWidth="100" 
                            Width="5%"/>

                                <dx:GridViewDataTextColumn 
                            Caption="% Growth" 
                            FieldName="Cost_DiffPerc" 
                            Name="Cost_DiffPerc" 
                            VisibleIndex="13" 
                            ExportWidth="100" 
                            Width="5%"
                            PropertiesTextEdit-DisplayFormatString="0.0%"/>
                           </Columns>
                       </dx:GridViewBandColumn>
                    </Columns>
             </dx:GridViewBandColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Rebated_Units" ShowInColumn="Rebated_Units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebated_Value" ShowInColumn="Rebated_Value" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebated_Cost" ShowInColumn="Rebated_Cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Rebate" ShowInColumn="Rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Missed_Units" ShowInColumn="Missed_Units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Missed_Value" ShowInColumn="Missed_Value" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0"   FieldName="Missed_Cost" ShowInColumn="Missed_Cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Missed_Rebate" ShowInColumn="Missed_Rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_This" ShowInColumn="Cost_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_Last" ShowInColumn="Cost_Last" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_This" ShowInColumn="Units_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_Last" ShowInColumn="Units_Last" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Cost_Diff" ShowInColumn="Cost_Diff" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Units_Diff" ShowInColumn="Units_Diff" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Cost_DiffPerc" SummaryType="Custom" ShowInColumn="Cost_DiffPerc" />
                <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Units_DiffPerc" SummaryType="Custom" ShowInColumn="Units_DiffPerc" />
            </TotalSummary>

		<%--
		 Removed by JIRA NMGBM-216, replaced with Cost_DiffPerc 
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Value_This" ShowInColumn="Value_This" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Value_Last" ShowInColumn="Value_Last" SummaryType="Sum" />
		 <dx:ASPxSummaryItem DisplayFormat="0.0%" FieldName="Value_DiffPerc" SummaryType="Custom" ShowInColumn="Value_DiffPerc" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="Value_Diff" ShowInColumn="Value_Diff" SummaryType="Sum" />
		--%>

            <SettingsDetail AllowOnlyOneMasterRowExpanded="True" ShowDetailRow="True" ExportMode="Expanded" />


                        <Templates>

                <DetailRow>

                    <dx:ASPxPageControl ID="pageControlDetail" runat="server" ActiveTabIndex="0" Width="100%" TabAlign="Left"  CssClass="page_tabs" OnLoad="pageControlDetail_Load">
                             
                        <TabPages>

                            <dx:TabPage Text="Rebated Sales" name="tab1">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                        <br />
                                             <dx:ASPxGridView 
                                                ID="gridDrillDown0"  
                                                Settings-ShowHeaderFilterBlankItems="False"
                                                runat="server" 
                                                AutoGenerateColumns="False" 
                                                DataSourceID="dsDrillDown0" 
                                                OnBeforePerformDataSelect="gridDrillDown_BeforePerformDataSelect"                                             
                                                OnHtmlDataCellPrepared="gridDrillDown_OnHtmlDataCellPrepared" 
                                                OnDataBinding="gridDrillDown_DataBinding"
                                                Width="100%">
                                            
                                                <SettingsPager PageSize="12" Mode="ShowAllRecords" AllButton-Visible="true">
                                                </SettingsPager>
        
                                                <Settings 
                                                    ShowFilterRow="False" 
                                                    ShowFilterRowMenu="False" 
                                                    ShowGroupedColumns="False"
                                                    ShowGroupFooter="VisibleIfExpanded" 
                                                    ShowGroupPanel="False" 
                                                    ShowHeaderFilterButton="False"
                                                    ShowStatusBar="Hidden" 
                                                    ShowTitlePanel="False" 
                                                    ShowFooter="True" 
                                                    HorizontalScrollBarMode="Hidden"  />
            
                                            <SettingsBehavior 
                                                AllowSort="False" 
                                                AutoFilterRowInputDelay="12000" 
                                                ColumnResizeMode="Control"
                                                AllowDragDrop="False" 
                                                AllowGroup="False" />

                                            <Styles>
                                                <Header HorizontalAlign="Center" Wrap="True" />
                                                <Cell HorizontalAlign="Center" Wrap="True" />
                                                <Footer HorizontalAlign="Center" Wrap="True" />
                                            </Styles>        
            
                                            <Columns>

                                                <dx:GridViewDataDateColumn Caption="Date" FieldName="InvoiceDate" VisibleIndex="0" ExportWidth="100" />

                                                <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                    
                                                <dx:GridViewDataTextColumn Caption="Customer" FieldName="CustomerName" VisibleIndex="1" ExportWidth="300" Width="13%" CellStyle-HorizontalAlign="Left" CellStyle-Wrap="False"/>

                                                <dx:GridViewDataTextColumn Caption="Focus ?" FieldName="Focus" VisibleIndex="1" ExportWidth="300" Width="7%" CellStyle-HorizontalAlign="Center" Settings-AllowHeaderFilter="True"  CellStyle-Wrap="False"/>

                                                
                                                 <dx:GridViewDataTextColumn Caption="Part Number" FieldName="PartNumber" ExportWidth="75" VisibleIndex="2" Width="12%" />

                                                <dx:GridViewDataTextColumn Caption="Quantity" FieldName="Quantity" ExportWidth="100" VisibleIndex="3" 
                                                    PropertiesTextEdit-DisplayFormatString="#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Value" FieldName="TotalSaleValue" ExportWidth="100" VisibleIndex="4" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Dealer Cost" FieldName="TotalCostValue" ExportWidth="100" VisibleIndex="5" 
                                                    ToolTip="Dealer Cost Price as provided by NMGB. This may differ from the Cost Price shown on your invoice as your DMS system may have applied an average cost price."
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Each" FieldName="UnitSaleValue" ExportWidth="100"  VisibleIndex="6" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Campaign Price" FieldName="RebatePrice" ExportWidth="100"  VisibleIndex="7" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Rebate" FieldName="ActualRebate" ExportWidth="100" VisibleIndex="8" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit" FieldName="nettprofit" ExportWidth="100"  VisibleIndex="8"  ToolTip="Calculated as (Sale Value + Rebate) - Dealer Cost"
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit %" FieldName="nettprofit_pc" ExportWidth="100"  VisibleIndex="9"  ToolTip="Calculated as (Sale Value + Rebate) / Dealer Cost"
                                                    PropertiesTextEdit-DisplayFormatString="0.0%">
                                                </dx:GridViewDataTextColumn>

                                            </Columns> 
                                    
                                                 
                                           <TotalSummary>
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalSaleValue" ShowInColumn="TotalSaleValue" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalCostValue" ShowInColumn="TotalCostValue" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="ActualRebate" ShowInColumn="ActualRebate" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="nettprofit" ShowInColumn="nettprofit" SummaryType="Sum" />
                                            </TotalSummary>

                                            <SettingsDetail IsDetailGrid="True" ExportMode="Expanded" />
                                            
                                        </dx:ASPxGridView>
                                        <br />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                            <dx:TabPage Text="Non-Rebated Sales" name="tab2">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl2" runat="server">
                                        <br />
                                             <dx:ASPxGridView 
                                                ID="gridDrillDown1"  
                                                Settings-ShowHeaderFilterBlankItems="False"
                                                runat="server" 
                                                AutoGenerateColumns="False" 
                                                DataSourceID="dsDrillDown1" 
                                                OnBeforePerformDataSelect="gridDrillDown_BeforePerformDataSelect"                                             
                                                OnHtmlDataCellPrepared="gridDrillDown_OnHtmlDataCellPrepared" 
                                                OnDataBinding="gridDrillDown_DataBinding"
                                                Width="100%">
                                            
                                                <SettingsPager PageSize="12" Mode="ShowAllRecords" AllButton-Visible="true">
                                                </SettingsPager>
        
                                                <Settings 
                                                    ShowFilterRow="False" 
                                                    ShowFilterRowMenu="False" 
                                                    ShowGroupedColumns="False"
                                                    ShowGroupFooter="VisibleIfExpanded" 
                                                    ShowGroupPanel="False" 
                                                    ShowHeaderFilterButton="False"
                                                    ShowStatusBar="Hidden" 
                                                    ShowTitlePanel="False" 
                                                    ShowFooter="True" 
                                                    HorizontalScrollBarMode="Hidden"  />
            
                                            <SettingsBehavior 
                                                AllowSort="False" 
                                                AutoFilterRowInputDelay="12000" 
                                                ColumnResizeMode="Control"
                                                AllowDragDrop="False" 
                                                AllowGroup="False" />

                                            <Styles>
                                                <Header HorizontalAlign="Center" Wrap="True" />
                                                <Cell HorizontalAlign="Center" Wrap="True" />
                                                <Footer HorizontalAlign="Center" Wrap="True" />
                                            </Styles>        
            
                                            <Columns>

                                                <dx:GridViewDataDateColumn Caption="Date" FieldName="InvoiceDate" VisibleIndex="0" ExportWidth="100" />

                                                <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="3" ExportWidth="150">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Customer" FieldName="CustomerName" VisibleIndex="1" ExportWidth="300" Width="13%" CellStyle-HorizontalAlign="Left" CellStyle-Wrap="False"/>

                                                <dx:GridViewDataTextColumn Caption="Focus ?" FieldName="Focus" VisibleIndex="1" ExportWidth="300" Width="7%" CellStyle-HorizontalAlign="Center" Settings-AllowHeaderFilter="True"  CellStyle-Wrap="False"/>

                                                <dx:GridViewDataTextColumn Caption="Part Number" FieldName="PartNumber" ExportWidth="75" VisibleIndex="2" Width="12%" />

                                                <dx:GridViewDataTextColumn Caption="Quantity" FieldName="Quantity" ExportWidth="100" VisibleIndex="3" 
                                                    PropertiesTextEdit-DisplayFormatString="#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Value" FieldName="TotalSaleValue" ExportWidth="100" VisibleIndex="4" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Dealer Cost" FieldName="TotalCostValue" ExportWidth="100" VisibleIndex="5" 
                                                    ToolTip="Dealer Cost Price as provided by NMGB. This may differ from the Cost Price shown on your invoice as your DMS system may have applied an average cost price."
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Each" FieldName="UnitSaleValue" ExportWidth="100"  VisibleIndex="6" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Campaign Price" FieldName="RebatePrice" ExportWidth="100"  VisibleIndex="7" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Missed Rebate" FieldName="ActualRebate" ExportWidth="100" VisibleIndex="8" Tooltip="Amount that would have been paid by NMGB if Rebate Price had been used."
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit" FieldName="nettprofit" ExportWidth="100"  VisibleIndex="9"  
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit %" FieldName="nettprofit_pc" ExportWidth="100"  VisibleIndex="10" 
                                                    PropertiesTextEdit-DisplayFormatString="0.0%">
                                                </dx:GridViewDataTextColumn>
                            </Columns> 

                                            <TotalSummary>
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalSaleValue" ShowInColumn="TotalSaleValue" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalCostValue" ShowInColumn="TotalCostValue" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="ActualRebate" ShowInColumn="ActualRebate" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="nettprofit" ShowInColumn="nettprofit" SummaryType="Sum" />
                                            </TotalSummary>

                                            <SettingsDetail IsDetailGrid="True" ExportMode="Expanded" />
                                            
                                        </dx:ASPxGridView>
                                        <br />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                            <dx:TabPage Text="Other Parts - No Rebate" name="tab3">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl3" runat="server">
                                        <br />
                                             <dx:ASPxGridView 
                                                ID="gridDrillDown2"   
                                                runat="server" 
                                                Settings-ShowHeaderFilterBlankItems="False"
                                                AutoGenerateColumns="False" 
                                                DataSourceID="dsDrillDown2" 
                                                OnDataBinding="gridDrillDown_DataBinding"
                                                OnBeforePerformDataSelect="gridDrillDown_BeforePerformDataSelect"                                             
                                                OnHtmlDataCellPrepared="gridDrillDown_OnHtmlDataCellPrepared" 
                                                Width="100%">
                                            
                                                <SettingsPager PageSize="12" Mode="ShowAllRecords" AllButton-Visible="true">
                                                </SettingsPager>
        
                                                <Settings 
                                                    ShowFilterRow="False" 
                                                    ShowFilterRowMenu="False" 
                                                    ShowGroupedColumns="False"
                                                    ShowGroupFooter="VisibleIfExpanded" 
                                                    ShowGroupPanel="False" 
                                                    ShowHeaderFilterButton="False"
                                                    ShowStatusBar="Hidden" 
                                                    ShowTitlePanel="False" 
                                                    ShowFooter="True" 
                                                    HorizontalScrollBarMode="Hidden"  />
            
                                            <SettingsBehavior 
                                                AllowSort="False" 
                                                AutoFilterRowInputDelay="12000" 
                                                ColumnResizeMode="Control"
                                                AllowDragDrop="False" 
                                                AllowGroup="False" />

                                            <Styles>
                                                <Header HorizontalAlign="Center" Wrap="True" />
                                                <Cell HorizontalAlign="Center" Wrap="True" />
                                                <Footer HorizontalAlign="Center" Wrap="True" />
                                            </Styles>        
            
                                            <Columns>

                                                <dx:GridViewDataDateColumn Caption="Date" FieldName="InvoiceDate" VisibleIndex="0" ExportWidth="100" />

                                                <dx:GridViewDataHyperLinkColumn 
                                                        Caption="Invoice" 
                                                        FieldName="id" 
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowHeaderFilter="True"
                                                        PropertiesHyperLinkEdit-Text="id" 
                                                        PropertiesHyperLinkEdit-Target="_blank"
                                                        PropertiesHyperLinkEdit-TextField="id" 
                                                        PropertiesHyperLinkEdit-TextFormatString="{0}"
                                                        PropertiesHyperLinkEdit-NavigateUrlFormatString="ViewInvoice.aspx?{0}" 
                                                        VisibleIndex="3"
                                                        Width="10%" 
                                                        ToolTip="The Invoice Number" 
                                                        ExportWidth="100">
                                                        <PropertiesHyperLinkEdit 
                                                            Text="InvoiceNumber" 
                                                            Target="_blank"
                                                            NavigateUrlFormatString="ViewInvoice.aspx?{0}" 
                                                            TextField="InvoiceNumber">
                                                        </PropertiesHyperLinkEdit>
                                                </dx:GridViewDataHyperLinkColumn>
                                                
                                                <dx:GridViewDataTextColumn Caption="Customer" FieldName="CustomerName" VisibleIndex="1" ExportWidth="300" Width="13%" CellStyle-HorizontalAlign="Left" CellStyle-Wrap="False"/>

                                                <dx:GridViewDataTextColumn Caption="Focus ?" FieldName="Focus" VisibleIndex="1" ExportWidth="300" Width="7%" CellStyle-HorizontalAlign="Center" Settings-AllowHeaderFilter="True" CellStyle-Wrap="False"/>

                                                <dx:GridViewDataTextColumn Caption="Part Number" FieldName="PartNumber" ExportWidth="75" VisibleIndex="3" Width="12%" />

                                                <dx:GridViewDataTextColumn Caption="Quantity" FieldName="Quantity" ExportWidth="100" VisibleIndex="4" 
                                                    PropertiesTextEdit-DisplayFormatString="#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Value" FieldName="TotalSaleValue" ExportWidth="100" VisibleIndex="5" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Dealer Cost" FieldName="TotalCostValue" ExportWidth="100" VisibleIndex="6" 
                                                    ToolTip="Dealer Cost Price as provided by NMGB. This may differ from the Cost Price shown on your invoice as your DMS system may have applied an average cost price."
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Sale Each" FieldName="UnitSaleValue" ExportWidth="100"  VisibleIndex="7" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Campaign Price" FieldName="RebatePrice" ExportWidth="100"  VisibleIndex="8" 
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit" FieldName="nettprofit" ExportWidth="100"  VisibleIndex="9"  
                                                    PropertiesTextEdit-DisplayFormatString="&#163;#,##0">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Nett Profit %" FieldName="nettprofit_pc" ExportWidth="100"  VisibleIndex="10"  
                                                    PropertiesTextEdit-DisplayFormatString="0.0%">
                                                </dx:GridViewDataTextColumn>
    
                                            </Columns> 

                                            <TotalSummary>
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalSaleValue" ShowInColumn="TotalSaleValue" SummaryType="Sum" />
                                                <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="TotalCostValue" ShowInColumn="TotalCostValue" SummaryType="Sum" />
                                                 <dx:ASPxSummaryItem DisplayFormat="&#163;#,##0" FieldName="nettprofit" ShowInColumn="nettprofit" SummaryType="Sum" />
                                           </TotalSummary>

                                            <SettingsDetail IsDetailGrid="True" ExportMode="Expanded" />
                                            
                                        </dx:ASPxGridView>
                                          <br />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                        </TabPages>
                    </dx:ASPxPageControl>

                </DetailRow>

            </Templates>


        </dx:ASPxGridView>
    
    </div>

    <asp:sqldatasource id="dsDrillDown0" runat="server" selectcommand="p_RebateCampaignDetail" selectcommandtype="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="RebateDealer" Type="String" Size="20" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int16" Size="4" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int16" Size="4" />
            <asp:Parameter Name="nDetailType" DefaultValue="1" Type="Int16"  />
        </SelectParameters>
    </asp:sqldatasource>

    <asp:sqldatasource id="dsDrillDown1" runat="server" selectcommand="p_RebateCampaignDetail" selectcommandtype="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="RebateDealer" Type="String" Size="20" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int16" Size="4" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int16" Size="4" />
            <asp:Parameter Name="nDetailType" DefaultValue="2" Type="Int16"  />
        </SelectParameters>
    </asp:sqldatasource>

    <asp:sqldatasource id="dsDrillDown2" runat="server" selectcommand="p_RebateCampaignDetail" selectcommandtype="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="RebateDealer" Type="String" Size="20" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int16" Size="4" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int16" Size="4" />
            <asp:Parameter Name="nDetailType" DefaultValue="3" Type="Int16"  />
        </SelectParameters>
    </asp:sqldatasource>
    
     <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False" OnRenderBrick="ASPxGridViewExporter1_RenderBrick">
    </dx:ASPxGridViewExporter>

</asp:Content>




