﻿Partial Class ActivityChecker

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Activity Checker"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Call GetMonths(ddlMonth)
            Call GetUsers(ddlUsers)
            ddlUsers.Items.Insert(0, "Please select...")
            ddlUsers.SelectedIndex = 0
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Activity Checker", "Viewing Activity Checker")
        End If

    End Sub

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Session("MonthFrom") = ddlMonth.SelectedValue
    End Sub

    Protected Sub dsSubmissions_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSubmissions.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsSubmissions.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsDrillDown_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDrillDown.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsDrillDown.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsYTDSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsYTDSummary.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsYTDSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsActivityLog_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsActivityLog.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsActivityLog.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridSubmissions_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridSubmissions.DataBound

        Dim nDays As Integer
        Dim i As Integer
        Dim sThisCaption As String

        nDays = GetDaysInMonth(Session("MonthFrom"))
        For i = 4 To gridSubmissions.Columns.Count - 1
            gridSubmissions.Columns(i).VisibleIndex = i
            If (i - 3) > nDays Then
                gridSubmissions.Columns(i).Visible = False
            Else
                gridSubmissions.Columns(i).Visible = True
                sThisCaption = GetDayCaption(Session("MonthFrom"), i - 3)
                gridSubmissions.Columns(i).Caption = sThisCaption
                If Left(sThisCaption, 1) = "S" Then
                    gridSubmissions.Columns(i).HeaderStyle.ForeColor = Drawing.Color.Yellow
                Else
                    gridSubmissions.Columns(i).HeaderStyle.ForeColor = Drawing.Color.White
                End If
            End If
        Next

    End Sub

    Protected Sub gridSubmissions_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSubmissions.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0
        Dim sDay As String

        If e.DataColumn.FieldName = "Centre" Or _
           e.DataColumn.FieldName = "CentreName" Or _
           e.DataColumn.FieldName = "DayInfo" Or _
           e.DataColumn.FieldName = "TotalHits" Then
            Exit Sub
        End If

        sDay = Mid(e.DataColumn.FieldName, 2)
        If IsDBNull(e.CellValue) = True Then
            nThisValue = 0
        Else
            nThisValue = CDec(e.CellValue)
        End If

        e.Cell.ForeColor = Drawing.Color.White
        If nThisValue <> 0 Then
            e.Cell.BackColor = Drawing.Color.Green
            e.Cell.ForeColor = Drawing.Color.Green
            e.Cell.ToolTip = Format(nThisValue, "#,##0") & " hit(s)"
        Else
            e.Cell.BackColor = Drawing.Color.Red
            e.Cell.ForeColor = Drawing.Color.Red
            e.Cell.ToolTip = "No activity"
        End If

    End Sub

    Protected Sub gridDrillDown_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("DealerCode") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Protected Sub gridSummary_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSummary.HtmlDataCellPrepared

        If e.DataColumn.Index = 2 Or e.DataColumn.Index = 3 Then
            e.Cell.BackColor = Drawing.Color.LightGreen
        End If

        If e.DataColumn.Index = 4 Or e.DataColumn.Index = 5 Then
            e.Cell.BackColor = Drawing.Color.LightBlue
        End If

    End Sub

    Protected Sub ddlUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUsers.SelectedIndexChanged
        Session("ActivityUser") = ddlUsers.SelectedValue
    End Sub

End Class
