﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VisitActionsDate.aspx.vb" Inherits="VisitActionsDate" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <dxe:ASPxCalendar ID="calDate" runat="server"></dxe:ASPxCalendar>

        <dxe:ASPxButton ID="btnDateSave" ClientInstanceName="btnDateSave" runat="server" Text="Save" Width="90px" ImagePosition="Top" Font-Names="Calibri,Verdana" 
            style="text-align: right; margin-top:10px">
        </dxe:ASPxButton>

    </form>
</body>
</html>
