﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ResetPassword.aspx.vb" Inherits="ResetPassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head id="Head1" runat="server">

    <meta charset="utf-8">
    <title>Reset Password - Nissan Trade Parts</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <link rel="stylesheet" href="assets/css/normalize.min.css" />
    <link rel="stylesheet" href="styles/font-awesome.min.css" />
    <link id="mainsplash" runat="server" rel="stylesheet" href="assets/css/nissan/splash.css" />
    <link rel="shortcut icon" href="assets/images/nissan/favicon.ico?v=2" />

    <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="assets/js/html5shiv.js"><\/script>')</script>
    <![endif]-->
    <style type="text/css">
        .modal--medium  .modal-dialog{
            width: 800px !important;
            margin: 30px auto;
        }

        /*#termsContentHolder, #privacyContentHolder {
            max-height: 640px !important;
            overflow-y: scroll !important;
            padding: 0 5px 10px;
        }*/
		
		.modal-content .panel_box__content {
            position: relative;
            min-height: 100px;
            max-height: calc(100vh - 300px);
            overflow-x: hidden;
            overflow-y: scroll;
            width: 100%;
        }
    </style>

</head>

<body id="body" runat="server">

    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
    <form id="page_form" class="login" runat="server">
        <div class="container-fluid">
            <div class="header">
                <div class="row">
                    <div class="col-md-3">
                        <img src="Images2014/new_logo_dark.png" class="img-responsive" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-4">
                    <asp:MultiView runat="server" ID="mvResetPassword">

                        <!-- Resend passsword -->
                        <asp:View runat="server" ID="vw1">
                            <h2>Reset your password</h2>
                            <br />
                            <br />
                            Please enter your new password below and then click the 'Save' button. Your chosen password must meet the minimum requirements below;
                            <br />
                            <br />
                            <asp:Label runat="server" ID="lblRequirements"></asp:Label>
                            <br />
                            <asp:Panel runat="server" id="pnlResetPassword" DefaultButton="btnSave">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                        <asp:TextBox runat="server" ID="txtPassword1" placeholder="New password" CssClass="form-control required" TextMode="Password" TabIndex="1"></asp:TextBox>
                                        <%--<input type="password" name="Password" id="Password" runat="server" placeholder="Password" class="form-control required" />--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                        <asp:TextBox runat="server" ID="txtPassword2" placeholder="Confirm your new password" CssClass="form-control required" TextMode="Password" TabIndex="2"></asp:TextBox>
                                        <%--<input type="password" name="Password" id="Password" runat="server" placeholder="Password" class="form-control required" />--%>
                                    </div>
                                </div>
                                <%--<div class="form-group">
                                <asp:CheckBox runat="server" ID="chkTermsConditionsRead" />Please confirm you have read our Terms & Conditions
                            </div>--%>
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnSave" CssClass="form-control login_button" Text="Save" CommandName="Save" OnClick="btnSave_Click" TabIndex="3" />
                                </div>

                                <div class="form-group alt_buttons">
                                    <div class="row">
                                        <%--<div class="col-md-4">
                                        <button type="button" class="form-control alt" data-toggle="modal" data-target="#termsConditions" id="btnTermsConditions">Terms & Conditions</button>
                                    </div>--%>
                                        <div class="col-md-12">
                                            <button type="button" class="form-control alt" data-toggle="modal" data-target="#privacyPolicy" id="btnPrivacyPolicy">Privacy Policy</button>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            

                        </asp:View>

                        <!-- Resend reset link -->
                        <asp:View runat="server" ID="vw2">
                            <h2>Reset your password</h2>
                            <br />
                            <br />
                            <span class="errors">Your password reset link has expired!</span><br />
                            <br />
                            Please enter your username below and try again.
                            <br />
                            <br />
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <asp:TextBox runat="server" ID="txtUsername" placeholder="Username" CssClass="form-control required"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnSendResetEmail" CssClass="form-control login_button" Text="Reset Password" OnClick="btnSendResetEmail_OnClick" runat="server" />
                            </div>
                        </asp:View>

                    </asp:MultiView>

                    <div class="alert alert-danger fade in" role="alert" style="margin-bottom: 20px; padding: 10px; font-size: 14px; line-height: 18px;" runat="server" id="divErrorMessage" visible="false">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <asp:Label class="errors" ID="lblErrorMessage" runat="server" Style="text-align: left;"></asp:Label>
                    </div>
                    <div class="alert alert-info fade in" role="alert" style="margin-bottom: 20px; padding: 10px; font-size: 14px; line-height: 18px;" runat="server" id="divSuccessMessage" visible="false">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <asp:Label ID="lblSuccessMessage" runat="server" Style="text-align: left;" Text="If we find a user with those credentials you will receive a password reset email witin the hour."></asp:Label>
                    </div>

                </div>
            </div>
        </div>

        <!-- PRIVACY POLICY Modal -->
        <div class="modal modal--medium fade" id="privacyPolicy" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="panel_box">
                        <div class="panel_box__header">
                            <h3>Privacy Policy</h3>
                            <div class="panel_box__header__add">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                        <div class="panel_box__content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="privacyContentHolder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                sessionStorage.clear();

                $('#privacyContentHolder').load('DocumentLibrary/PrivacyPolicy.html');
                $('#termsContentHolder').load('DocumentLibrary/TermsConditions.html');
            });
        </script>

    </form>
</body>

</html>

