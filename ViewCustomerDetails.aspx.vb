﻿Imports System.Data
Imports DevExpress.Web

Partial Class ViewCustomerDetails

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, ds2 As DataSet, ds3 As DataSet, sErr, sSQL As String, nCustomerId As Integer
    Dim dsResults As DataSet
    Dim bSeeOtherButton As Boolean
    Dim sErrorMessage As String = ""
    Dim htin As New Hashtable
    Dim sCustomerType As String = ""

    Protected Sub dsHistory_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsHistory.Init
        dsHistory.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        Session("CustomerId") = Val(Request.QueryString(0))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        On Error GoTo 0

        Session("CallingModule") = "ViewCustomerDetails.aspx - Load"

        Me.Title = "Nissan Trade Site - View Customer Details"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
            Session("ContactType") = 0
        End If

        If ddlSpendYear.Items.Count = 0 Then
            For i = Session("CurrentYear") To 2012 Step -1
                ddlSpendYear.Items.Add(Trim(Str(i)))
            Next
        End If

        nCustomerId = Val(Request.QueryString(0))
        If Not IsPostBack Then
            ddlSpendYear.SelectedIndex = 0
            lblStatusMessage.Text = ""
            bSeeOtherButton = False
            If nCustomerId <> 0 Then
                Call FetchCompanyDetails()
                Call LoadSpendProfile()
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "View Company Details", "Viewing Company : " & Replace(txtBusinessName.Text, "'", ""))
                If Session("CustomerCalledFrom") = "HomePage" Then
                    ASPxPageControl1.ActiveTabIndex = 4
                End If
            End If
        End If

    End Sub

    Sub FetchCompanyDetails()

        Dim i As Integer
        Dim bOnTradeClub As Boolean = False
        Dim sVanRoute As String = ""
        Dim sDriveTime As String = ""
        Dim sDistance As String = ""
        Dim sSalesRep As String = ""
        Dim sNotes As String = ""
        Dim nApprovalStatus As Integer = 0
        Dim sContactName As String = ""
        Dim sTelephoneNumber As String = ""
        Dim sUserField1 As String = ""
        Dim sUserField2 As String = ""
        Dim sUserField3 As String = ""
        Dim sUserField4 As String = ""

        Session("CallingModule") = "ViewCustomerDetails.aspx - Fetch Company Details"

        btnSave.Text = "Save"
        lblStatusMessage.Text = ""

        Call EnabledEdit(False)
        Call ShowCentreSpecificFields(False)
        Call ShowTradeClubFields(False)

        '/ -----------------------------------------------------------------------------------------------
        '/ Standard Customer fields
        '/ -----------------------------------------------------------------------------------------------
        txtBusinessName.Text = ""
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtAddress3.Text = ""
        txtAddress4.Text = ""
        txtAddress5.Text = ""
        txtAddress6.Text = ""
        txtPostCode.Text = ""
        lblExperianURN.Visible = False
        ddlBusinessType.SelectedIndex = 0
        txtTelephone.Text = ""
        txtProprietorName.Text = ""
        txtProprietorJobTitle.Text = ""
        txtKeyContactName.Text = ""
        txtKeyContactJobTitle.Text = ""
        lblCreated.Text = ""
        lblUpdated.Text = ""


        '/ -----------------------------------------------------------------------------------------------
        '/ Centre Specific Info
        '/ -----------------------------------------------------------------------------------------------
        txtVanRoute.Text = ""
        txtDriveTime.Text = ""
        txtDistance.Text = ""
        txtSalesRep.Text = ""
        txtUserField1.Text = ""
        txtUserField2.Text = ""
        txtUserField3.Text = ""
        txtUserField4.Text = ""
        txtContactNameSpec.Text = ""
        txtTelephoneSpec.Text = ""
        txtNotes.Text = ""

        '/ -----------------------------------------------------------------------------------------------
        '/ Trade Club stuff
        '/ -----------------------------------------------------------------------------------------------
        txtMobile.Text = ""
        txtFaxNumber.Text = ""
        txtEmailAddress.Text = ""
        txtWebSite.Text = ""
        spinRamps.Number = 0
        spinTechnicians.Number = 0
        spinVehicleRepairs.Number = 0
        spinToyotaRepairs.Number = 0
        cblServices.Items(0).Selected = False
        cblServices.Items(1).Selected = False
        cblServices.Items(2).Selected = False
        cblServices.Items(3).Selected = False
        cblServices.Items(4).Selected = False
        cblServices.Items(5).Selected = False
        txtOther.Text = ""
        txtMotorFactor1.Text = ""
        txtSpend1.Text = ""
        txtMotorFactor2.Text = ""
        txtSpend2.Text = ""
        txtMotorFactor3.Text = ""
        txtSpend3.Text = ""
        txtMotorFactor4.Text = ""
        txtSpend4.Text = ""

        sSQL = "SELECT * FROM v_Customer WHERE Id = " & Str(nCustomerId)
        ds = da.ExecuteSQL(sErr, sSQL)
        If ds.Tables(0).Rows.Count = 1 Then

            With ds.Tables(0).Rows(0)

                '/ -----------------------------------------------------------------------------------------------
                '/ Standard Customer fields
                '/ -----------------------------------------------------------------------------------------------
                ASPxPageControl1.TabPages(0).Text = "Customer Details (#" & Trim(Str(nCustomerId)) & ")"
                txtBusinessName.Text = .Item("BusinessName").ToString
                txtAddress1.Text = .Item("Address1").ToString
                txtAddress2.Text = .Item("Address2").ToString
                txtAddress3.Text = .Item("Address3").ToString
                txtAddress4.Text = .Item("Address4").ToString
                txtAddress5.Text = .Item("Address5").ToString
                txtAddress6.Text = .Item("Address6").ToString
                txtPostCode.Text = .Item("PostCode").ToString
                sCustomerType = "" & .Item("CustomerType").ToString

                If Trim(.Item("BusinessURN").ToString) <> "" Then
                    txtYPCategory.Visible = True
                    txtYPURN.Visible = True
                    lblExperianCategory.Visible = True
                    lblExperianURN.Visible = True
                    txtYPCategory.Text = .Item("YPCategory").ToString & " - " & .Item("YPDescription").ToString
                    txtYPURN.Text = .Item("BusinessURN").ToString
                End If
                Call GetBusinessTypes2dx(ddlBusinessType, .Item("BusinessTypeId"))
                For i = 0 To ddlBusinessType.Items.Count - 1
                    If ddlBusinessType.Items(i).Value = .Item("BusinessTypeId") Then
                        ddlBusinessType.SelectedIndex = i
                    End If
                Next
                txtTelephone.Text = .Item("TelephoneNumber").ToString
                txtMobile.Text = .Item("MobileNumber").ToString
                txtFaxNumber.Text = .Item("FaxNumber").ToString
                txtWebSite.Text = .Item("WebsiteAddress").ToString
                txtProprietorName.Text = .Item("ProprietorsName").ToString
                txtProprietorJobTitle.Text = .Item("ProprietorsJobTitle").ToString
                txtKeyContactName.Text = .Item("KeyContactName").ToString
                txtKeyContactJobTitle.Text = .Item("KeyContactJobTitle").ToString
                lblCreated.Text = "Created By " & Trim(.Item("CreatedUserName").ToString) & " " & .Item("CreatedDate").ToString
                lblUpdated.Text = "Last Updated By " & Trim(.Item("UpdatedUserName").ToString) & " " & .Item("LastUpdatedDate").ToString

                '/ -----------------------------------------------------------------------------------------------
                '/ Centre Specific Info
                '/ -----------------------------------------------------------------------------------------------
                If Session("SelectionLevel") = "C" Then

                    Call GetCentreSpecificInfo(nCustomerId, Session("SelectionId"), sVanRoute, sDriveTime, sDistance, _
                                               sSalesRep, sContactName, sTelephoneNumber, sUserField1, sUserField2, _
                                               sUserField3, sUserField4, sNotes)

                    Call ShowCentreSpecificFields(True)

                    txtVanRoute.Text = sVanRoute
                    txtDriveTime.Text = Format(Val(sDriveTime), "0.00")
                    txtDistance.Text = Format(Val(sDistance), "0.00")
                    txtSalesRep.Text = sSalesRep
                    txtContactNameSpec.Text = sContactName
                    txtTelephoneSpec.Text = sTelephoneNumber
                    txtUserField1.Text = sUserField1
                    txtUserField2.Text = sUserField2
                    txtUserField3.Text = sUserField3
                    txtUserField4.Text = sUserField4
                    txtNotes.Text = sNotes

                    ' Focus Stuff For Nissan
                    Dim sFocusSQL As String = "SELECT bluegrassfocus_flag FROM nissanextrafields where dealercode = '" & Trim(Session("SelectionID")) & "' AND customerid = " & Trim(Str(nCustomerId))
                    Dim nFocus As Integer = GetDataDecimal(sFocusSQL)
                    cboFocus.SelectedIndex = nFocus
                    ' End of Focus Stuff For Nissan

                    ' Email Stuff for Nissan   ( taken from CustomerDealer not Customer)
                    Dim sEmailSQL As String = "SELECT email FROM nissanextrafields where dealercode = '" & Trim(Session("SelectionID")) & "' AND customerid = " & Trim(Str(nCustomerId))
                    txtEmailAddress.Text = GetDataString(sEmailSQL)
                    ' End of Email Stuff For Nissan


                    lblUser1.Text = GetDataString("SELECT Userfield1 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
                    lblUser2.Text = GetDataString("SELECT Userfield2 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
                    lblUser3.Text = GetDataString("SELECT Userfield3 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
                    lblUser4.Text = GetDataString("SELECT Userfield4 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")

                    If lblUser1.Text = "" Then
                        txtUserField1.Visible = False
                    End If
                    If lblUser2.Text = "" Then
                        txtUserField2.Visible = False
                    End If
                    If lblUser3.Text = "" Then
                        txtUserField3.Visible = False
                    End If
                    If lblUser4.Text = "" Then
                        txtUserField4.Visible = False
                    End If

                Else
                    Call ShowCentreSpecificFields(False)
                End If

                Session("CustomerDealerID") = GetDataDecimal("Select ID From CustomerDealer where customerid = " & Session("CustomerId") & " and DealerCode = '" & Trim(Session("SelectionID")) & "' And ID In (Select customerdealerid from web_qube)")


                If Session("CustomerDealerID") <> 0 Then


                    ' Add in the Consent Details
                    lblConsentUser.Text = "Last Updated By "

                    sSQL = "EXEC sp_SelectCustomerDealerConsent " & Session("CustomerDealerID")
                    ds = da.ExecuteSQL(sErr, sSQL)
                    If ds.Tables(0).Rows.Count = 1 Then

                        boxContact_Ticked.Visible = vbFalse
                        boxContact_Empty.Visible = vbFalse
                        boxPost_Ticked.Visible = vbFalse
                        boxPost_GreyedOut.Visible = vbFalse
                        boxPost_Empty.Visible = vbFalse
                        boxEmail_Ticked.Visible = vbFalse
                        boxEmail_GreyedOut.Visible = vbFalse
                        boxEmail_Empty.Visible = vbFalse
                        boxPhone_Ticked.Visible = vbFalse
                        boxPhone_GreyedOut.Visible = vbFalse
                        boxPhone_Empty.Visible = vbFalse
                        boxSMS_Ticked.Visible = vbFalse
                        boxSMS_GreyedOut.Visible = vbFalse
                        boxSMS_Empty.Visible = vbFalse

                        lblCustomerConsent.Visible = True
                        lblConsentUser.Visible = True
                        lblNoContact.Visible = True
                        lblEmail.Visible = True
                        lblPost.Visible = True
                        lblPhone.Visible = True
                        lblSMS.Visible = True


                        With ds.Tables(0).Rows(0)

                            lblConsentUser.Text = " ( Last Updated By " & .Item("lastupdatedby") & " )"
                            If .Item("nocontact_flag") = 1 Then
                                boxContact_Ticked.Visible = vbTrue
                            Else
                                boxContact_Empty.Visible = vbTrue
                            End If

                            If .Item("postallowed_flag") = 1 Then
                                boxPost_Ticked.Visible = vbTrue
                            Else
                                boxPost_Empty.Visible = vbTrue
                            End If

                            If .Item("emailallowed_flag") = 1 Then
                                boxEmail_Ticked.Visible = vbTrue
                            Else
                                boxEmail_Empty.Visible = vbTrue
                            End If

                            If .Item("phoneallowed_flag") = 1 Then
                                boxPhone_Ticked.Visible = vbTrue
                            Else
                                boxPhone_Empty.Visible = vbTrue
                            End If

                            If .Item("SMSallowed_flag") = 1 Then
                                boxSMS_Ticked.Visible = vbTrue
                            Else
                                boxSMS_Empty.Visible = vbTrue
                            End If

                        End With
                    End If
                End If


                '/ ---------------------------------------------------------------------------------------------------------
                '/ If the Customer is a Trade Club member, then check to see if you can display all the Trade Club fields
                '/ If we are at National, Regional or TPSM level, then we can just show all the fields.
                '/ If we are at Centre level, then we can only show the fields if the Customers is registered to the Centre.
                '/ ---------------------------------------------------------------------------------------------------------
                If .Item("TradeClubNumber").ToString <> "" Then

                            '/ If we are at Customer level, check the CustomerTradeClubMembership table.
                            If Session("SelectionLevel") = "C" Then
                                Call GetTradeClubStatus(nCustomerId, Session("SelectionId"), bOnTradeClub, nApprovalStatus)
                            Else
                                If Session("SelectionLevel") = "N" Or
                           Session("SelectionLevel") = "R" Or
                           Session("SelectionLevel") = "T" Or
                           Session("SelectionLevel") = "Z" Then
                                    bOnTradeClub = True
                                    nApprovalStatus = 1
                                End If
                            End If

                            If bOnTradeClub Then

                                Call ShowTradeClubFields(True)

                                spinRamps.Number = .Item("NoRamps").ToString
                                spinTechnicians.Number = .Item("NoTech").ToString
                                spinVehicleRepairs.Number = .Item("NoRepairs").ToString
                                spinToyotaRepairs.Number = .Item("NoToyotaRepairs").ToString

                                cblServices.Items(0).Selected = (.Item("Flag1").ToString = 1)
                                cblServices.Items(1).Selected = (.Item("Flag2").ToString = 1)
                                cblServices.Items(2).Selected = (.Item("Flag3").ToString = 1)
                                cblServices.Items(3).Selected = (.Item("Flag4").ToString = 1)
                                cblServices.Items(4).Selected = (.Item("Flag5").ToString = 1)
                                cblServices.Items(5).Selected = (.Item("Flag6").ToString = 1)

                                txtOther.Text = .Item("Specialism").ToString

                                txtMotorFactor1.Text = .Item("Factor1").ToString
                                txtSpend1.Text = Format(Val(.Item("Spend1").ToString), "#,##0.00")
                                txtMotorFactor2.Text = .Item("Factor2").ToString
                                txtSpend2.Text = Format(Val(.Item("Spend2").ToString), "#,##0.00")
                                txtMotorFactor3.Text = .Item("Factor3").ToString
                                txtSpend3.Text = Format(Val(.Item("Spend3").ToString), "#,##0.00")
                                txtMotorFactor4.Text = .Item("Factor4").ToString
                                txtSpend4.Text = Format(Val(.Item("Spend4").ToString), "#,##0.00")

                            End If

                        End If

                    End With

                End If

        btnCustBack.Visible = True
        btnEdit.Visible = True
        btnEdit.Enabled = True
        btnOther.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False

        If Session("SelectionLevel") = "C" Then
            If IsHiddenCustomer(Session("SelectionId"), nCustomerId) Then
                btnOther.Visible = True
                btnOther.Text = "Unhide"
                btnEdit.Enabled = False
            Else
            End If
        End If

    End Sub

    Private Sub ShowCentreSpecificFields(ByVal bOn As Boolean)

        Session("CallingModule") = "ViewCustomerDetails.aspx - ShowCentreSpecificFields"

        ASPxPageControl1.TabPages(1).Enabled = bOn  '/ Centre specific fields
        ASPxPageControl1.TabPages(2).Visible = False
        ASPxPageControl1.TabPages(4).Enabled = bOn  '/ Contact History
    End Sub

    Private Sub ShowTradeClubFields(ByVal bOn As Boolean)
        'trTradeClub.Visible = bOn
        '/ASPxPageControl1.TabPages(2).Enabled = bOn
    End Sub

    Private Sub EnabledEdit(ByVal bOn As Boolean)

        lblStatusMessage.Text = ""

        '/ ----------------------------------------------
        '/ These items can only be changed by a SuperUser
        '/ ----------------------------------------------
        'txtBusinessName.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress1.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress2.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress3.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress4.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress5.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtAddress6.Enabled = bOn And (Session("SuperUser") = "Y")
        'txtPostCode.Enabled = bOn And (Session("SuperUser") = "Y")
        'cblMailFlags1.Items(0).Enabled = bOn And (Session("SuperUser") = "Y")
        'cblMailFlags1.Items(1).Enabled = bOn And (Session("SuperUser") = "Y")

        If bOn Then

            'If Session("SuperUser") <> "Y" Then
            '    txtBusinessName.BackColor = Drawing.Color.Gray
            '    txtAddress1.BackColor = Drawing.Color.Gray
            '    txtAddress2.BackColor = Drawing.Color.Gray
            '    txtAddress3.BackColor = Drawing.Color.Gray
            '    txtAddress4.BackColor = Drawing.Color.Gray
            '    txtAddress5.BackColor = Drawing.Color.Gray
            '    txtAddress6.BackColor = Drawing.Color.Gray
            '    txtPostCode.BackColor = Drawing.Color.Gray
            'End If

            'If ddlBusinessType.Enabled Then
            '    ddlBusinessType.BackColor = Drawing.Color.White
            'Else
            '    ddlBusinessType.BackColor = Drawing.Color.Gray
            'End If

            'txtYPCategory.BackColor = Drawing.Color.Gray
            'txtYPURN.BackColor = Drawing.Color.Gray

        Else

            'txtBusinessName.BackColor = Drawing.Color.White
            'txtAddress1.BackColor = Drawing.Color.White
            'txtAddress2.BackColor = Drawing.Color.White
            'txtAddress3.BackColor = Drawing.Color.White
            'txtAddress4.BackColor = Drawing.Color.White
            'txtAddress5.BackColor = Drawing.Color.White
            'txtAddress6.BackColor = Drawing.Color.White
            'txtPostCode.BackColor = Drawing.Color.White
            'txtKeyContactName.BackColor = Drawing.Color.White
            'ddlBusinessType.BackColor = Drawing.Color.White

            'txtYPCategory.BackColor = Drawing.Color.Gray
            'txtYPURN.BackColor = Drawing.Color.Gray

        End If

        '/ these can be edited by anyone
        txtTelephone.Enabled = bOn
        txtMobile.Enabled = bOn
        txtFaxNumber.Enabled = bOn
        txtWebSite.Enabled = bOn
        txtEmailAddress.Enabled = bOn
        ddlBusinessType.Enabled = bOn And (Trim(txtYPURN.Text) = "")
        txtProprietorName.Enabled = bOn
        txtProprietorJobTitle.Enabled = bOn
        txtKeyContactName.Enabled = bOn
        txtKeyContactJobTitle.Enabled = bOn
        cboFocus.Enabled = bOn

        'cblMailFlags2.Items(0).Enabled = bOn
        'cblMailFlags2.Items(1).Enabled = bOn

        'cblServices.Items(0).Enabled = bOn
        'cblServices.Items(1).Enabled = bOn
        'cblServices.Items(2).Enabled = bOn
        'cblServices.Items(3).Enabled = bOn
        'cblServices.Items(4).Enabled = bOn
        'cblServices.Items(5).Enabled = bOn

        txtOther.Enabled = bOn

        spinRamps.Enabled = bOn
        spinTechnicians.Enabled = bOn
        spinToyotaRepairs.Enabled = bOn
        spinVehicleRepairs.Enabled = bOn

        txtMotorFactor1.Enabled = bOn
        txtSpend1.Enabled = bOn
        txtMotorFactor2.Enabled = bOn
        txtSpend2.Enabled = bOn
        txtMotorFactor3.Enabled = bOn
        txtSpend3.Enabled = bOn
        txtMotorFactor4.Enabled = bOn
        txtSpend4.Enabled = bOn

        txtVanRoute.Enabled = bOn
        txtDriveTime.Enabled = bOn
        txtDistance.Enabled = bOn
        txtSalesRep.Enabled = bOn
        txtContactNameSpec.Enabled = bOn
        txtTelephoneSpec.Enabled = bOn
        txtUserField1.Enabled = bOn
        txtUserField2.Enabled = bOn
        txtUserField3.Enabled = bOn
        txtUserField4.Enabled = bOn
        txtNotes.Enabled = bOn

    End Sub

    Private Function ValidateCustomerDetails() As Boolean

        ValidateCustomerDetails = True

        Call LabelHighlight(lblKeyContactName, False)
        Call LabelHighlight(lblBusinessType, False)
        Call LabelHighlight(lblTelephoneNumber, False)
        Call LabelHighlight(lblTown, False)
        Call LabelHighlight(lblPostCode, False)
        Call LabelHighlight(lblAddress1, False)
        Call LabelHighlight(lblBusinessName, False)

        'If Session("SuperUser") = "Y" Then
        '    txtBusinessName.BackColor = Drawing.Color.White
        '    txtAddress1.BackColor = Drawing.Color.White
        '    txtPostCode.BackColor = Drawing.Color.White
        'End If

        ddlBusinessType.BackColor = Drawing.Color.White
        txtKeyContactName.BackColor = Drawing.Color.White
        txtTelephone.BackColor = Drawing.Color.White

        If NeedsValidating(txtTelephone) And Len(Trim(txtTelephone.Text)) < 10 Then
            Call LabelHighlight(lblTelephoneNumber, True)
            ValidateCustomerDetails = False
            txtTelephone.Focus()
        End If

        If NeedsValidating(txtAddress5) And Trim(txtAddress5.Text) = "" Then
            Call LabelHighlight(lblTown, True)
            ValidateCustomerDetails = False
            txtAddress5.Focus()
        End If

        If NeedsValidating(txtPostCode) And Trim(txtPostCode.Text) = "" Then
            Call LabelHighlight(lblPostCode, True)
            ValidateCustomerDetails = False
            txtPostCode.Focus()
        End If

        If NeedsValidating(txtAddress1) And Trim(txtAddress1.Text) = "" Then
            Call LabelHighlight(lblAddress1, True)
            ValidateCustomerDetails = False
            txtAddress1.Focus()
        End If

        If NeedsValidating(txtBusinessName) And Trim(txtBusinessName.Text) = "" Then
            Call LabelHighlight(lblBusinessName, True)
            ValidateCustomerDetails = False
            txtBusinessName.Focus()
        End If


    End Function

    Private Sub SaveCustomerDetails()

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim htInTC As New Hashtable
        Dim htInCentreSpec As New Hashtable
        Dim htInTCLink As New Hashtable
        Dim nRes As Long

        Session("CallingModule") = "ViewCustomerDetails.aspx - SaveCustomerDetails"

        htIn.Add("@sBusinessName", txtBusinessName.Text)
        htIn.Add("@sAddress1", txtAddress1.Text)
        htIn.Add("@sAddress2", txtAddress2.Text)
        htIn.Add("@sAddress3", txtAddress3.Text)
        htIn.Add("@sAddress4", txtAddress4.Text)
        htIn.Add("@sAddress5", txtAddress5.Text)
        htIn.Add("@sAddress6", txtAddress6.Text)
        htIn.Add("@sPostCode", txtPostCode.Text)
        htIn.Add("@sTelephoneNumber", txtTelephone.Text)
        htIn.Add("@sMobileNumber", txtMobile.Text)
        htIn.Add("@sFaxNumber", txtFaxNumber.Text)
        htIn.Add("@sWebsite", txtWebSite.Text)
        htIn.Add("@sEmailAddress", txtEmailAddress.Text)
        htIn.Add("@sProprietorsName", txtProprietorName.Text)
        htIn.Add("@sProprietorsJobTitle", txtProprietorJobTitle.Text)
        htIn.Add("@sKeyContactName", txtKeyContactName.Text)
        htIn.Add("@sKeyContactJobTitle", txtKeyContactJobTitle.Text)
        htIn.Add("@nBusinessTypeId", ddlBusinessType.SelectedItem.Value)
        htIn.Add("@sSafeToMail", "Y")
        htIn.Add("@sSafeToPhone", "Y")
        htIn.Add("@nUserId", Session("UserId"))
        htIn.Add("@nCustomerId", nCustomerId)
        nRes = da.Update(sErr, "p_Customer_Upd", htIn)
        If sErr.Length <> 0 Then
            Session("ErrorToDisplay") = sErr
            Session("CallingModule") = "ViewCustomerDetails.aspx - SaveCustomerDetails Line 589"
            Response.Redirect("dbError.aspx")
        End If

        If Session("SelectionLevel") = "C" Then
            htInCentreSpec.Add("@sDealerCode", Session("SelectionId"))
            htInCentreSpec.Add("@nCustomerId", nCustomerId)
            htInCentreSpec.Add("@sVanRoute", txtVanRoute.Text)
            htInCentreSpec.Add("@sDriveTime", txtDriveTime.Text)
            htInCentreSpec.Add("@sDistance", txtDistance.Text)
            htInCentreSpec.Add("@sSalesRep", txtSalesRep.Text)
            htInCentreSpec.Add("@sContactName", txtContactNameSpec.Text)
            htInCentreSpec.Add("@sTelephoneNumber", txtTelephoneSpec.Text)
            htInCentreSpec.Add("@sUserField1", txtUserField1.Text)
            htInCentreSpec.Add("@sUserField2", txtUserField2.Text)
            htInCentreSpec.Add("@sUserField3", txtUserField3.Text)
            htInCentreSpec.Add("@sUserField4", txtUserField4.Text)
            htInCentreSpec.Add("@sNotes", txtNotes.Text)
            nRes = da.Update(sErr, "p_Customer_Upd_CentreSpecific", htInCentreSpec)
            If sErr.Length > 0 Then
                Session("ErrorToDisplay") = sErr
                Session("CallingModule") = "ViewCustomerDetails.aspx - SaveCustomerDetails Line 610"
                Response.Redirect("dbError.aspx")
            End If
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Update customer specific data", "Company : " & txtBusinessName.Text)
        End If

        ' Focus Stuff For Nissan
        Session("CallingModule") = "ViewCustomerDetails.aspx - Focus Stuff For Nissan"
        Dim nFocus As Integer = cboFocus.SelectedIndex
        If txtTelephone.Text = "" Then
            nFocus = 0
        End If
        Dim sFocusSQL As String = "UPDATE nissanextrafields SET bluegrassfocus_flag = " & nFocus & " where dealercode = '" & Trim(Session("SelectionID")) & "' AND customerid = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sFocusSQL)
        ' End of Focus Stuff For Nissan

        ' Email Stuff For Nissan
        Session("CallingModule") = "ViewCustomerDetails.aspx - Email Status"
        Dim sEmailStatus As String = GetDataString("select dbo.getEmailStatus2('" & txtEmailAddress.Text & "')")
        Dim sEmailSQL1 As String = "UPDATE nissanextrafields SET email  ='" & txtEmailAddress.Text & "', email_status = '" & sEmailStatus & "' where dealercode = '" & Trim(Session("SelectionID")) & "' AND customerid = " & Trim(Str(nCustomerId))
        Dim sEmailSQL2 As String = "UPDATE customerdealer SET contactemailaddress  ='" & txtEmailAddress.Text & "' where dealercode = '" & Trim(Session("SelectionID")) & "' AND customerid = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sEmailSQL1)
        Call RunNonQueryCommand(sEmailSQL2)
        ' End of Email Stuff For Nissan

        Call FetchCompanyDetails()
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Update Customer data", "Company : " & txtBusinessName.Text)
        lblStatusMessage.Text = "Your changes have been saved."

    End Sub

    Private Sub Unhide()

        Dim sSQL As String
        sSQL = "DELETE FROM CustomerDealerHidden WHERE DealerCode = '" & Session("SelectionId") & "' AND CustomerId = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sSQL)
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer unhidden", "Company : " & txtBusinessName.Text)
        Call FetchCompanyDetails()
        lblStatusMessage.Text = "This Customer has now been unhidden."

    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click


        Dim strMessage As String = ""
        Dim strScript As String = "<script language=JavaScript>"

        If ValidateCustomerDetails() Then
            Call SaveCustomerDetails()
        Else
            lblStatusMessage.Text = "Please correct the highlighed errors."
        End If

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtBusinessName.BackColor = Drawing.Color.White
        txtAddress1.BackColor = Drawing.Color.White
        txtPostCode.BackColor = Drawing.Color.White
        txtTelephone.BackColor = Drawing.Color.White
        txtKeyContactName.BackColor = Drawing.Color.White
        lblStatusMessage.Text = ""
        Call FetchCompanyDetails()
        Call EnabledEdit(False)
    End Sub

    Protected Sub btnCustBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCustBack.Click
        Session("CompanyReturnKeyValue") = nCustomerId
        Response.Redirect("ViewCompanies.aspx")
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Call EnabledEdit(True)
        btnCustBack.Visible = False
        btnEdit.Visible = False
        btnOther.Visible = False
        lblStatusMessage.Text = ""
        btnSave.Text = "Save"
        btnSave.Visible = True
        btnCancel.Visible = True
    End Sub

    Protected Sub btnOther_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOther.Click

        Dim sTCNumber As String

        If btnOther.Text = "Unhide" Then
            btnCustBack.Visible = False
            btnEdit.Visible = False
            btnOther.Visible = False
            btnSave.Text = "Unhide"
            btnSave.Visible = True
            btnCancel.Visible = True
            Call Unhide()
        End If


        If btnOther.Text = "Add to Trade Club" Then
            sTCNumber = GetDataString("SELECT ISNULL(TradeClubNumber,'') AS TradeClubNumber FROM Customer WHERE Id = " & Trim(Str(nCustomerId)))
            Call EnabledEdit(True)
            lblMobileNumber.Visible = True
            txtMobile.Visible = True
            lblFaxNumber.Visible = True
            txtFaxNumber.Visible = True
            lblEmailAddress.Visible = True
            txtEmailAddress.Visible = True
            lblWebsite.Visible = True
            txtWebSite.Visible = True
            ASPxPageControl1.TabPages(1).Enabled = True
            'ASPxPageControl1.TabPages(2).Enabled = True
            btnCustBack.Visible = False
            btnEdit.Visible = False
            btnOther.Visible = False
            lblStatusMessage.Text = ""
            btnSave.Text = "Save"
            btnSave.Visible = True
            btnCancel.Visible = True
        End If

    End Sub

    Protected Sub spinRamps_NumberChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinRamps.NumberChanged
        If spinRamps.Number < 0 Then
            spinRamps.Number = 0
        End If
    End Sub

    Protected Sub spinTechnicians_NumberChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinTechnicians.NumberChanged
        If spinTechnicians.Number < 0 Then
            spinTechnicians.Number = 0
        End If
    End Sub

    Protected Sub spinToyotaRepairs_NumberChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinToyotaRepairs.NumberChanged
        If spinToyotaRepairs.Number < 0 Then
            spinToyotaRepairs.Number = 0
        End If
    End Sub

    Protected Sub spinVehicleRepairs_NumberChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles spinVehicleRepairs.NumberChanged
        If spinVehicleRepairs.Number < 0 Then
            spinVehicleRepairs.Number = 0
        End If
    End Sub

    Private Function IsHiddenCustomer(ByVal sDealerCode As String, ByVal nCustomerId As Integer) As Boolean

        Dim nCount As Long
        Dim sSQL As String

        IsHiddenCustomer = False
        sSQL = "SELECT COUNT(*) AS HiddenCount FROM CustomerDealerHidden WHERE DealerCode = '" & sDealerCode & "' AND CustomerId = " & Trim(Str(nCustomerId))
        nCount = GetDataLong(sSQL)
        If nCount > 0 Then
            IsHiddenCustomer = True
        End If

    End Function

    Private Sub LabelHighlightold(ByVal lbl As Label, ByVal bOn As Boolean)
        With lbl
            lbl.Font.Bold = bOn
            lbl.Font.Underline = bOn
            lbl.Font.Italic = bOn
            If bOn Then
                lbl.ForeColor = Drawing.Color.Red
            Else
                lbl.ForeColor = Drawing.Color.Black
            End If
        End With
    End Sub
    Private Sub LabelHighlight(ByVal lbl As DevExpress.Web.ASPxLabel, ByVal bOn As Boolean)
        With lbl
            lbl.Font.Bold = bOn
            lbl.Font.Underline = bOn
            lbl.Font.Italic = bOn
            If bOn Then
                lbl.ForeColor = Drawing.Color.Red
            Else
                lbl.ForeColor = Drawing.Color.Black
            End If
        End With
    End Sub
    Private Function NeedsValidating(ByVal objControl As Object) As Boolean
        NeedsValidating = False
        If Session("UserLevel") <> "N" And Session("UserLevel") <> "T" And objControl.Enabled Then
            NeedsValidating = True
        End If
    End Function

    Sub LoadSpendProfile()

        Session("CallingModule") = "ViewCustomerDetails.aspx - Load Spend Profile"

        htin.Clear()
        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nCustomerId", nCustomerId)
        htin.Add("@sLevel4_Code", ddlTA.SelectedItem.Value)
        htin.Add("@nYear", Val(ddlSpendYear.SelectedItem.Text))
        dsResults = da.Read(sErrorMessage, "p_CustManager_SpendsNissan", htin)

        gridSales.DataSource = dsResults.Tables(0)
        gridSales.DataBind()

        With gridSpends_M1
            .DataSource = dsResults.Tables(1)
            .DataBind()
            .Columns(0).Caption = "Service"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M2
            .DataSource = dsResults.Tables(2)
            .DataBind()
            .Columns(0).Caption = "Steering/Suspension"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M3
            .DataSource = dsResults.Tables(3)
            .DataBind()
            .Columns(0).Caption = "Brake"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M4
            .DataSource = dsResults.Tables(4)
            .DataBind()
            .Columns(0).Caption = "Clutch/Drivetrain"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M5
            .DataSource = dsResults.Tables(5)
            .DataBind()
            .Columns(0).Caption = "Exhaust"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M6
            .DataSource = dsResults.Tables(6)
            .DataBind()
            .Columns(0).Caption = "Electrical"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M7
            .DataSource = dsResults.Tables(7)
            .DataBind()
            .Columns(0).Caption = "Heating/Cooling"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_M8
            .DataSource = dsResults.Tables(8)
            .DataBind()
            .Columns(0).Caption = "Consumables"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D1
            .DataSource = dsResults.Tables(9)
            .DataBind()
            .Columns(0).Caption = "Main Panels"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D2
            .DataSource = dsResults.Tables(10)
            .DataBind()
            .Columns(0).Caption = "Mirrors and Glass"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D3
            .DataSource = dsResults.Tables(11)
            .DataBind()
            .Columns(0).Caption = "Bumpers and Fixings"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D4
            .DataSource = dsResults.Tables(12)
            .DataBind()
            .Columns(0).Caption = "Lighting"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D5
            .DataSource = dsResults.Tables(13)
            .DataBind()
            .Columns(0).Caption = "Grilles and Mouldings"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        With gridSpends_D6
            .DataSource = dsResults.Tables(14)
            .DataBind()
            .Columns(0).Caption = "Other"
            .Columns(0).HeaderStyle.Font.Size = "10"
            '.Columns(0).HeaderStyle.Font.Bold = True
        End With

        If sCustomerType = "B" Then
            ASPxPageControl2.ActiveTabIndex = 1
        End If

    End Sub

    Protected Sub gridSales_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridSales.CustomColumnDisplayText

        Dim nThisValue As Double
        If e.Column.Index > 0 Then
            If IsDBNull(e.Value) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.Value)
            End If
            If e.GetFieldValue("RowOrder") = 1 Then
                e.DisplayText = Format(nThisValue, "£#,##0")
            ElseIf e.GetFieldValue("RowOrder") = 2 Then
                If (Val(Mid(e.Column.FieldName, 7)) <= Session("CurrentMonth")) Or (Val(ddlSpendYear.SelectedItem.Text) < Session("CurrentYear")) Then
                    e.DisplayText = Format(nThisValue, "£#,##0")
                Else
                    e.DisplayText = ""
                End If
            Else
                If Left(e.Column.FieldName,5) = "Month" Then
                    If (Val(Mid(e.Column.FieldName, 7)) <= Session("CurrentMonth")) Or (Val(ddlSpendYear.SelectedItem.Text) < Session("CurrentYear")) Then
                        e.DisplayText = Format(nThisValue, "0.00%")
                    Else
                        e.DisplayText = ""
                    End If
                ElseIf Left(e.Column.FieldName, 3) = "YTD" Then
                    e.DisplayText = Format(nThisValue, "0.00%")
                End If
            End If
        End If

    End Sub

    Protected Sub gridSales_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSales.HtmlDataCellPrepared

        Dim nThisValue As Double

        Session("CallingModule") = "ViewCustomerDetails.aspx - gridSales.HtmlDataCellPrepared"

        If Left(e.DataColumn.FieldName, 5) = "Month" Then
            If e.GetValue("RowOrder") = 3 Then
                If Val(Mid(e.DataColumn.FieldName, 7)) <= Session("CurrentMonth") Or (Val(ddlSpendYear.SelectedItem.Text) < Session("CurrentYear")) Then
                    If IsDBNull(e.CellValue) = True Then
                        nThisValue = 0
                    Else
                        nThisValue = CDec(e.CellValue)
                    End If
                    e.Cell.ForeColor = System.Drawing.Color.White
                    If nThisValue > 0 Then
                        e.Cell.BackColor = System.Drawing.Color.Green
                    Else
                        e.Cell.BackColor = System.Drawing.Color.Red
                    End If
                End If
            End If
        End If

        If Left(e.DataColumn.FieldName, 3) = "YTD" Then
            If e.GetValue("RowOrder") = 3 Then
                If IsDBNull(e.CellValue) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.CellValue)
                End If
                e.Cell.ForeColor = System.Drawing.Color.White
                If nThisValue > 0 Then
                    e.Cell.BackColor = System.Drawing.Color.Green
                Else
                    e.Cell.BackColor = System.Drawing.Color.Red
                End If
            End If
        End If

    End Sub

    Protected Sub ContactType_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sContactType As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ContactType").ToString()
        Dim sContentUrl As String = ""

        link.NavigateUrl = "javascript:void(0);"
        Select Case sContactType
            Case 0
                link.ImageUrl = "~/Images2014/Phone_Small.png"
            Case 1
                link.ImageUrl = "~/Images2014/Email_Small.png"
            Case 2
                link.ImageUrl = "~/Images2014/CustomerVisit_Small.png"
            Case 3
                link.ImageUrl = "~/Images2014/Mailing_Small.png"
        End Select
        link.ClientSideEvents.Click = ""

    End Sub

    Protected Sub RemoveLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}", "ContactHistoryRemove.aspx", sId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/user_block.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ContactHistRemove('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to remove this Contact History entry."

    End Sub

    Protected Sub gridContactHistory_CustomColumnDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs) Handles gridContactHistory.CustomColumnDisplayText
        If e.Column.FieldName = "ContactDate" Then
            e.DisplayText = Format(CDate(e.Value), "dd-MMM-yyyy")
        End If
        If e.Column.FieldName = "ReminderDate" Then
            If e.GetFieldValue("Reminder") = 1 Then
                e.DisplayText = Format(CDate(e.Value), "dd-MMM-yyyy")
            Else
                e.DisplayText = "-"
            End If
        End If
    End Sub

    Protected Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        PopupNewHistory.HeaderText = "Add new contact line - Phone call"
        txtContactDate.MinDate = DateAdd(DateInterval.Day, -30, Now())
        Session("ContactType") = 0
        Call ClearContactFields()
        PopupNewHistory.ShowOnPageLoad = True
    End Sub

    Protected Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        PopupNewHistory.HeaderText = "Add new contact line - Email"
        txtContactDate.MinDate = DateAdd(DateInterval.Day, -30, Now())
        Session("ContactType") = 1
        Call ClearContactFields()
        PopupNewHistory.ShowOnPageLoad = True
    End Sub

    Protected Sub btn3_Click(sender As Object, e As EventArgs) Handles btn3.Click
        PopupNewHistory.HeaderText = "Add new contact line - Face to Face contact"
        txtContactDate.MinDate = DateAdd(DateInterval.Day, -30, Now())
        Session("ContactType") = 2
        Call ClearContactFields()
        PopupNewHistory.ShowOnPageLoad = True
    End Sub

    Protected Sub btn4_Click(sender As Object, e As EventArgs) Handles btn4.Click
        PopupNewHistory.HeaderText = "Add new contact line - Mail"
        txtContactDate.MinDate = DateAdd(DateInterval.Day, -30, Now())
        Session("ContactType") = 3
        Call ClearContactFields()
        PopupNewHistory.ShowOnPageLoad = True
    End Sub

    Protected Sub btnNewContactSave_Click(sender As Object, e As EventArgs) Handles btnNewContactSave.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim sDealerCode As String = Session("SelectionId")
        Dim sContactDate As String = Format(CDate(txtContactDate.Text), "dd-MMM-yyyy")
        Dim sReminderDate As String = Format(CDate(txtReminderDate.Text), "dd-MMM-yyyy")
        Dim nReminder As Integer = IIf(chkReminder.Checked, 1, 0)
        Dim sMessage As String = txtContact.Text

        If sDealerCode <> "" And sContactDate <> "" Then

            htIn.Add("@sDealerCode", sDealerCode)
            htIn.Add("@nCustomerId", nCustomerId)
            htIn.Add("@sContactDate", sContactDate)
            htIn.Add("@nContactType", Session("ContactType"))
            htIn.Add("@sContactMessage", sMessage)
            htIn.Add("@nReminder", nReminder)
            htIn.Add("@sReminderDate", sReminderDate)
            htIn.Add("@nUserId", Session("UserId"))

            nRes = da.Update(sErr, "p_CustomerDealerContact_Ins", htIn)

            If sErr.Length = 0 Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Contact History added", "Centre : " & sDealerCode & ", Customer Id : " & Trim(Str(nCustomerId)))
                gridContactHistory.DataBind()
                PopupNewHistory.ShowOnPageLoad = False
            Else
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

        End If

    End Sub

    Sub ClearContactFields()
        txtContactDate.Date = CDate(Now)
        chkReminder.Checked = False
        txtReminderDate.Date = CDate(DateAdd(DateInterval.Weekday, 1, Now()))
        txtContact.Text = ""
    End Sub

    Protected Sub ddlSpendYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSpendYear.SelectedIndexChanged
        Call LoadSpendProfile()
    End Sub

    Protected Sub ddlTA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTA.SelectedIndexChanged
        Call LoadSpendProfile()
    End Sub

    Protected Sub SpendGrids_CustomColumnDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
        If Left(e.Column.FieldName, 8) = "Variance" Then
            If e.Value = 0 Then
                e.DisplayText = ""
            End If
        End If
    End Sub

    Protected Sub SpendGrids_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        Dim nThisValue As Double


        On Error GoTo 0
        Session("CallingModule") = "ViewCustomerDetails.aspx - SpendGrids_HtmlDataCellPrepared"

        If Left(e.DataColumn.FieldName, 8) = "Variance" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.ForeColor = System.Drawing.Color.White
            If nThisValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            ElseIf nThisValue <> 0 Then
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

    Protected Sub ASPxPageControl1_ActiveTabChanged(source As Object, e As TabControlEventArgs) Handles ASPxPageControl1.ActiveTabChanged

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub
    
    Private Sub ViewCustomerDetails_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Session("CallingModule") = "ViewCustomerDetails.aspx - LoadComplete"



    End Sub

    Private Sub ViewCustomerDetails_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad

    End Sub
End Class
