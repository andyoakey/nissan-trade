﻿Imports DevExpress.XtraPrinting

Partial Class NationwideSalesByMonthByDealer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim nYear1 As Integer = 2017
        Dim nYear2 As Integer = 2018

        If Page.IsPostBack = False Then
            Session("FYFrom") = GetDataDecimal("select min(id) from dataperiods where financialyear = (select financialyear from dataperiods where id =" & Session("CurrentPeriod") & ")")
        End If

        Select Case Session("FYFrom")
            Case 124
                nYear1 = 2016
                nYear2 = 2017
            Case 136
                nYear1 = 2017
                nYear2 = 2018
            Case 148
                nYear1 = 2017
                nYear2 = 2018
        End Select

        With grid
            .Columns("month1").Caption = "Apr " & nYear1
            .Columns("month2").Caption = "May " & nYear1
            .Columns("month3").Caption = "Jun " & nYear1
            .Columns("month4").Caption = "Jul " & nYear1
            .Columns("month5").Caption = "Aug " & nYear1
            .Columns("month6").Caption = "Sep " & nYear1
            .Columns("month7").Caption = "Oct " & nYear1
            .Columns("month8").Caption = "Nov " & nYear1
            .Columns("month9").Caption = "Dec " & nYear1

            .Columns("month10").Caption = "Jan " & nYear2
            .Columns("month11").Caption = "Feb " & nYear2
            .Columns("month12").Caption = "Mar " & nYear2
        End With


    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "NationwideSalesByMonthByDealer"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Private Sub cboFY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFY.SelectedIndexChanged
        Session("FYFrom") = sender.value
    End Sub
End Class
