﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MCChallByZone.aspx.vb" Inherits="MCChall2011ByZone" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width:976px; margin: 0 auto; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="971px">
                <tr>
                    <td align="left" >
                        <asp:Label ID="lblPageTitle" runat="server" Text="Trade Challenge 2016" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right" >
                        <dxe:ASPxLabel ID="lblLeague" runat="server" Text="View  :  " style="font-weight: 700; font-size:small ">
                        </dxe:ASPxLabel>
                        <asp:DropDownList ID="ddlLeague" runat="server" AutoPostBack="True"  Width="180px"   >
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridView ID="gridCampaigns" runat="server" width="971px" OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            AutoGenerateColumns="False" KeyFieldName="DealerCode" style="position:relative; top:20px; left:5px">
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True" ShowGroupFooter="VisibleIfExpanded"  />
            
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000"  ColumnResizeMode="Disabled" AllowDragDrop="False" AllowGroup="False" />
            
            <Styles>
                <Header Wrap="True" HorizontalAlign="Center" />
                <Cell Font-Size="Small" HorizontalAlign="Center" />
            </Styles>

            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Ranking" FieldName="Ranking" VisibleIndex="0" Width="10%" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone" VisibleIndex="1" Width="100px" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="Total Sales" FieldName="SaleValue" VisibleIndex="2" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Target" FieldName="Target" VisibleIndex="3" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Achievement" FieldName="Achievement" VisibleIndex="4" ExportWidth="120">
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        
    </div>
    
    <asp:SqlDataSource ID="dsCampaigns1" runat="server" SelectCommand="p_MCChallenge2016_ByZone" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCampaigns2" runat="server" SelectCommand="p_MCChallenge2016_ByTPSM" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>

