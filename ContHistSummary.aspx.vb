﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System

Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports System.IO
Imports System.Net.Mime

Partial Class ContHistSummary

    Inherits System.Web.UI.Page

    Dim dsContactTracker As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        'Dim myexcelbutton As DevExpress.Web.ASPxButton
        'myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        'If Not myexcelbutton Is Nothing Then
        '    myexcelbutton.Visible = False
        'End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "NissanDATA PARTS - Contact History Tracker"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), Me.Title, Me.Title)
            Session("ContactHistYear") = Session("CurrentYear")
            For i = Session("CurrentYear") To 2015 Step -1
                ddlYear.Items.Add(Trim(Str(i)))
            Next
        End If

        Call LoadDataChecker()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub gridContHist_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridContHist.CustomColumnDisplayText

        Dim nThisMonth As Integer = Now.Month
        Dim nThisYear As Integer = Session("CurrentYear")

        If e.GetFieldValue("ThisYear") = nThisYear Then
            If Left(e.Column.FieldName, 5) = "Month" Then
                If (e.Column.Index - 2) > nThisMonth Then
                    e.DisplayText = ""
                End If
            End If
        End If

    End Sub

    Protected Sub gridContHist_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridContHist.HtmlDataCellPrepared

        Dim nThisMonth As Integer = Now.Month
        Dim nColMonth As Integer = 0
        Dim nThisYear As Integer = Session("CurrentYear")

        If Left(e.DataColumn.FieldName, 5) = "Month" Then
            nColMonth = Val(Mid(e.DataColumn.FieldName, 7))
            If e.GetValue("ThisYear") < nThisYear Or nColMonth <= nThisMonth Then
                If e.CellValue > 0 Then
                    e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                Else
                    e.Cell.BackColor = GlobalVars.g_Color_Tomato
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                End If
            End If
        End If

        If Left(e.DataColumn.FieldName, 3) = "Six" Then
            If e.CellValue >= 50 Then
                e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
                e.Cell.ForeColor = GlobalVars.g_Color_White
            Else
                e.Cell.BackColor = GlobalVars.g_Color_Tomato
                e.Cell.ForeColor = GlobalVars.g_Color_White
            End If

        End If

    End Sub

    Sub LoadDataChecker()

        Dim htin As New Hashtable

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nContactType", ddlContactType.SelectedItem.Value)
        htin.Add("@nTCMembers", IIf(chkTradeClubOnly.Checked, 1, 0))
        htin.Add("@nThisYear", ddlYear.SelectedItem.Text)
        dsContactTracker = da.Read(sErrorMessage, "p_ContactSummary2", htin)
        gridContHist.DataSource = dsContactTracker.Tables(0)
        gridContHist.DataBind()

    End Sub

    Protected Sub gridContHist_SummaryDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewSummaryDisplayTextEventArgs) Handles gridContHist.SummaryDisplayText

        Dim nThisMonth As Integer = Now.Month
        Dim nColMonth As Integer = 0
        If Left(e.Item.FieldName, 5) = "Month" Then
            nColMonth = Val(Mid(e.Item.FieldName, 7))
            If nColMonth > nThisMonth Then
                e.Text = ""
            End If
        End If

    End Sub

    Protected Sub HistDrillDown_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim sName As String = templateContainer.Column.Name
        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sDealerCode As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "DealerCode").ToString()
        Dim sContentUrl As String = String.Format("{0}?DealerCode={1}&Month={2}&ContactType={3}&TCMembers={4}", "ContHistDrillDown.aspx", sDealerCode, sName, ddlContactType.SelectedItem.Value, IIf(chkTradeClubOnly.Checked, 1, 0))
        Dim nValue As Integer = templateContainer.Grid.GetRowValues(nRowVisibleIndex, sName).ToString()

        link.Text = nValue
        link.ForeColor = GlobalVars.g_Color_White
        If nValue = 0 Then
            link.Font.Underline = False
        Else
            link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewContHistDrillDown('{0}'); }}", sContentUrl)
            link.ToolTip = "Click here to see Contact History for this period."
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisMonth As Integer = Now.Month
        Dim nColMonth As Integer = 0

        Call GlobalRenderBrick(e)

        If e.Column.Name = "CentreName" Then
            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        End If

        If Left(e.Column.Name, 5) = "Month" Then
            nColMonth = Val(Mid(e.Column.Name, 7))
            If nColMonth <= nThisMonth Then
                If e.Value > 0 Then
                    e.BrickStyle.BackColor = GlobalVars.g_Color_YellowGreen
                    e.BrickStyle.ForeColor = GlobalVars.g_Color_White
                Else
                    e.BrickStyle.BackColor = GlobalVars.g_Color_Tomato
                    e.BrickStyle.ForeColor = GlobalVars.g_Color_White
                End If
            End If
        End If

        If Left(e.Column.Name, 3) = "Six" Then
            If e.Value >= 50 Then
                e.BrickStyle.BackColor = GlobalVars.g_Color_YellowGreen
                e.BrickStyle.ForeColor = GlobalVars.g_Color_White
            Else
                e.BrickStyle.BackColor = GlobalVars.g_Color_Tomato
                e.BrickStyle.ForeColor = GlobalVars.g_Color_White
            End If
        End If

    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("ContactHistYear") = ddlYear.SelectedItem.Text
    End Sub
End Class
