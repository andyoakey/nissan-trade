﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports System.Data
Imports DevExpress.Web

Partial Class QuickInvoiceSearch

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Quick Invoice Search"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Session("InvoiceNumber") = "0"
            Session("PartNumber") = "0"
            Session("MonthFrom") = 0
            Call GetMonthsDevX(ddlFrom)
            ddlFrom.SelectedIndex = -1
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Quick Invoice Search", "Viewing Quick Invoice Search")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Session("ReturnProgram") = "QuickInvoiceSearch.aspx"

    End Sub

    Protected Sub dsInvoices_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsInvoices.Init
        dsInvoices.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Private Sub RefreshGrid()

        If isEmpty(txtInvoiceNumber.Text) And isEmpty(txtPartNumber.Text) And isEmpty(ddlFrom.Text) Then
            lblError.Visible = True
            Exit Sub
        End If

        lblError.Visible = False

        If isEmpty(txtInvoiceNumber.Text) = False Then
            Session("InvoiceNumber") = txtInvoiceNumber.Text
        Else
            Session("InvoiceNumber") = "0"
        End If

        If isEmpty(txtPartNumber.Text) = False Then
            Session("PartNumber") = txtPartNumber.Text
        Else
            Session("PartNumber") = "0"
        End If

        If ddlFrom.SelectedIndex > 0 Then
            Session("MonthFrom") = CInt(ddlFrom.Value)
        End If

        gridInvoices.DataSourceID = "dsInvoices"
        gridInvoices.DataBind()


    End Sub

    Protected Sub gridInvoices_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub


    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblError.Visible = False
        Call RefreshGrid()
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        lblError.Visible = False
        txtInvoiceNumber.Text = " "
        txtPartNumber.Text = " "
        Session("InvoiceNumber") = "ThisWillBeEmpty"
        Session("PartNumber") = "ThisWillBeEmpty"
        gridInvoices.DataBind()
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "invoicenumber").ToString()
        Dim dealerCode = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "dealercode").ToString()

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub

End Class
