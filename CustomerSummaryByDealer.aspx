﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerSummaryByDealer.aspx.vb" Inherits="CustomerSummaryByDealer" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:0px">
           <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Customer Summary By Dealer" />
        </div>

        <br />

        <dx:ASPxGridView 
            ID="gridCustSummary" 
            runat="server" 
            OnLoad="GridStyles"
            width="100%" 
            DataSourceID="dsCustSummary"  
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode"
            OnCustomColumnDisplayText="gridCustomerSummary_OnCustomColumnDisplayText">
       
            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="16" />
            
            <Settings ShowHeaderFilterButton="True" ShowStatusBar="Hidden" ShowFooter="true" ShowGroupFooter="VisibleIfExpanded" UseFixedTableLayout="True"   />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Disabled" AllowDragDrop="False" AllowGroup="False" />
                       
            <Styles Header-Wrap ="True" Cell-HorizontalAlign="Center" Header-HorizontalAlign="Center"/>

            <Styles Footer-HorizontalAlign ="Center" />

            <Columns>
                
                <dx:GridViewDataTextColumn 
                    Caption="Zone" 
                    FieldName="Zone" 
                    ExportWidth="100"
                    VisibleIndex="1" 
                    Width="5%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList"  AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Dealer" 
                    FieldName="DealerCode" 
                    ExportWidth="100"
                    VisibleIndex="2" 
                    Width="5%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="True" AllowSort="True" ShowInFilterControl="False"  HeaderFilterMode="CheckedList"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Dealer Name" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-Wrap="False"
                    ExportWidth="200"
                    FieldName="Dealer" 
                    VisibleIndex="3" 
                    Width="15%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="True" AllowSort="True" ShowInFilterControl="False" HeaderFilterMode="CheckedList"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Active (last 90 days)" 
                    FieldName="ActCust" 
                    Width="9%"
                    ExportWidth="100"
                    VisibleIndex="4">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Inactive (91-180 days)" 
                    FieldName="InActCust" 
                    Width="9%"
                    ExportWidth="100"
                    VisibleIndex="5">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Lapsed (181-270 days)" 
                    FieldName="LapsedCust" 
                    Width="9%"
                    ExportWidth="100"
                    VisibleIndex="6">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Prospects (over 270 days)" 
                    FieldName="ProspectCust" 
                    Width="9%"
                    ExportWidth="100"
                    VisibleIndex="7" >
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="New Prospects (never spent)" 
                    FieldName="NewProspectCust" 
                    VisibleIndex="8" 
                    Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Focus Customers" 
                    FieldName="TCCust" 
                    ExportWidth="100"
                    Width="9%"
                    VisibleIndex="9">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Top 10% Customer Sales YTD" 
                    FieldName="Top10" 
                    ExportWidth="100"
                    Width="9%"
                    VisibleIndex="10">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Excluded Sales YTD" 
                    FieldName="Excluded" 
                    ExportWidth="100"
                    Width="9%"
                    VisibleIndex="11">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>
            
                <dx:GridViewDataTextColumn 
                    Caption="Excluded Sales (%)" 
                    FieldName="ExcludedPerc" 
                    ExportWidth="100"
                    Width="9%"
                    VisibleIndex="12">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowInFilterControl="False" />
                </dx:GridViewDataTextColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem FieldName="ActCust" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="ActCust" />
                <dx:ASPxSummaryItem FieldName="InActCust" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="InActCust" />
                <dx:ASPxSummaryItem FieldName="LapsedCust" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="LapsedCust" />
                <dx:ASPxSummaryItem FieldName="ProspectCust" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="ProspectCust" />
                <dx:ASPxSummaryItem FieldName="TCCust" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="TCCust" />
                <dx:ASPxSummaryItem FieldName="Top10" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Top10" />
                <dx:ASPxSummaryItem FieldName="Excluded" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="Excluded" />
           </TotalSummary>



        </dx:ASPxGridView>

        <br />
        
    </div>
    
    <asp:SqlDataSource 
        ID="dsCustSummary" 
        runat="server" 
        SelectCommand="p_Customer_Summary_ByCentre" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridCustSummary" 
        PreserveGroupRowStates="False">
    </dx:ASPxGridViewExporter>

</asp:Content>



