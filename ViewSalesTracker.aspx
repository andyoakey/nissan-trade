<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewSalesTracker.aspx.vb" Inherits="ViewSalesTracker" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <table width="971px">
                <tr>
                    <td align="left" style="width:75%">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Sales Tracker" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right">
                        <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" Width="180px"   >
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <br />

        <asp:Label ID="lblCentreError" runat="server" Text="The Sales Tracker is only available when a Centre is chosen from the Access list." 
            style="font-size: medium" Visible="false">
        </asp:Label>

        <dxwgv:ASPxGridView ID="gridSalesTracker" runat="server" 
            DataSourceID="dsSalesTracker"  AutoGenerateColumns="False" 
            OnCustomColumnDisplayText="gridSalesTracker_OnCustomColumnDisplayText"       
             Width="100%">
       
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="16" Visible="False" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="True" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="True" ShowGroupButtons="False" 
                UseFixedTableLayout="True" />
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>
            
            <TotalSummary>
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="WorkingDaysThisMonth" 
                    SummaryType="Sum" ShowInColumn="Working Days" 
                    ShowInGroupFooterColumn="Working Days" />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="TotalSales" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="TotalSalesTarget" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="TotalSalesForecast" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="MechComp" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="MechCompTarget" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="MechCompForecast" SummaryType="Sum"  />
            </TotalSummary>
        
            <Images >
                <LoadingPanelOnStatusBar >
                </LoadingPanelOnStatusBar>
                <LoadingPanel >
                </LoadingPanel>
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Month" FieldName="PeriodName"
                    VisibleIndex="0" Width="10%" ShowInCustomizationForm="False">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Working Days" FieldName="WorkingDaysThisMonth"
                    VisibleIndex="1" Width="7%" ShowInCustomizationForm="False">
                    <PropertiesTextEdit DisplayFormatString="#0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Total Sales" FieldName="TotalSales" 
                    VisibleIndex="2">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Total Sales Target" FieldName="TotalSalesTarget" 
                    VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Achieved" FieldName="TotalSalesDiff" 
                    VisibleIndex="4">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Daily Sales Average" FieldName="TotalSalesAverage" 
                    VisibleIndex="5">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Forecast Sales using Run Rate" FieldName="TotalSalesForecast" 
                    VisibleIndex="6">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Mechanical Competitive Sales" FieldName="MechComp" 
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Mechanical Competitive  Sales Target" FieldName="MechCompTarget" 
                    VisibleIndex="8">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Achieved" FieldName="MechCompDiff" 
                    VisibleIndex="9">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Daily Sales  Average" FieldName="MechCompAverage" 
                    VisibleIndex="10">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Forecast Sales using Run Rate" FieldName="MechCompForecast" 
                    VisibleIndex="11">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />
        
        <dxwgv:ASPxGridView ID="gridTrackerRunRate" runat="server" 
            DataSourceID="dsTrackerRunRate"  AutoGenerateColumns="False"       
             Width="100%">
       
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="16" Visible="False">
            </SettingsPager>
            
            <Settings ShowColumnHeaders="False"
                ShowFooter="False" ShowGroupPanel="False"
                ShowStatusBar="Hidden" ShowGroupButtons="False" 
                UseFixedTableLayout="True" />
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <Images >
                <CollapsedButton Height="12px" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="" FieldName="TextLine"
                    VisibleIndex="0" Width="100%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />

        <dxwgv:ASPxGridView ID="gridSalesTrackerSummary" runat="server" KeyFieldName="RowOrder" 
            DataSourceID="dsSalesTrackerSummary"  AutoGenerateColumns="False"       
            OnCustomColumnDisplayText="gridSalesTrackerSummary_OnCustomColumnDisplayText"
            Width="100%">
       
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="16" Visible="False">
            </SettingsPager>
            
            <Settings ShowColumnHeaders="False"
                ShowFooter="False" ShowGroupPanel="False"
                ShowStatusBar="Hidden" ShowGroupButtons="False" 
                UseFixedTableLayout="True" />
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <Images >
                <CollapsedButton Height="12px" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowCol1"
                    VisibleIndex="0" Width="17%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue1"
                    VisibleIndex="1">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowCol2"
                    VisibleIndex="2" Width="17%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue2"
                    VisibleIndex="3">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowCol3"
                    VisibleIndex="4" Width="17%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue3"
                    VisibleIndex="5">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowCol4"
                    VisibleIndex="6" Width="17%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Size="X-Small">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue4"
                    VisibleIndex="7">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />
                
    </div>
    
    <asp:SqlDataSource ID="dsSalesTracker" runat="server" SelectCommand="p_SalesTracker_Daily2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsSalesTrackerSummary" runat="server" SelectCommand="p_SalesTracker_Summary_Daily2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsTrackerRunRate" runat="server" SelectCommand="p_SalesTracker_DailyRunRate" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nPeriod" SessionField="MonthFrom" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridSalesTracker" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="gridSalesTrackerSummary" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>


