﻿Imports System.Data
Imports System.Net.Mail

Partial Class VisitActionActivityRemove

    Inherits System.Web.UI.Page

    Dim nType As Integer
    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        nType = Left(Request.QueryString(0), 1)
        nId = Val(Mid(Request.QueryString(0), 2))
    End Sub

    Protected Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim sSQL As String

        If nType = 0 Then
            '/ Remove header and all details
            sSQL = "DELETE FROM Web_VAR_Details WHERE VARId = " & Trim(Str(nId))
            Call RunNonQueryCommand(sSQL)

            sSQL = "DELETE FROM Web_VAR WHERE Id = " & Trim(Str(nId))
            Call RunNonQueryCommand(sSQL)

        Else
            '/ Remove a single detail line
            sSQL = "DELETE FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId))
            Call RunNonQueryCommand(sSQL)

        End If
        Dim startUpScript As String = String.Format("window.parent.HideVisitActionActivityWindow();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

End Class

