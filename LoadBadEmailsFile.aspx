﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LoadBadEmailsFile.aspx.vb" Inherits="LoadBadEmailsFile" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxsp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                Font-Size="Larger"
                runat="server" 
                Text="Load Bad Emails File" />
        </div>


        <div style="position:relative;">
            <dx:ASPxLabel
                ID="lblNotes" 
                runat="server" 
                Text="Important: Make sure the file has been saved in CSV format before loading" />
         </div>


        <div style="position:relative;">
                <dx:ASPxUploadControl 
                               ID="uplCSV" 
                               runat="server" 
                               
                               UploadMode="Auto"
                               UploadStorage="FileSystem"
                               ShowProgressPanel="true" 
                               UploadButton-ImagePosition="Right"
                               ShowClearFileSelectionButton="true"
                               Width="30%"
                               ShowUploadButton="true">
                                <AdvancedModeSettings EnableMultiSelect="False" EnableFileList="False" EnableDragAndDrop="False"  />
                                <ValidationSettings MaxFileSize="10000000"  AllowedFileExtensions=".csv" />
                                  <ClientSideEvents FileUploadComplete="function(s, e) {alert('File Uploaded Successfully');}" />
                            </dx:ASPxUploadControl>
         </div>


    </div>






</asp:Content>
