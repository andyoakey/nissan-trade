﻿Imports System.Data
Imports System.Net
Imports System.Net.Mail
Imports System.Collections

Partial Class ViewPeriodReporting

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Selective Period Reporting"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Call GetMonths(ddlFrom)
            Call RefreshGrid()
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Period Reporting", "Viewing Period Reporting")
        End If

    End Sub

    Protected Sub gridSales_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridSales.DataBound

        Select Case Session("PRType1")

            Case 0
                gridSales.Columns(2).Caption = GetPeriodName(CInt(Session("PRToPeriod")))
                If Session("PRType2") = 0 Then
                    gridSales.Columns(1).Caption = GetPeriodName(CInt(Session("PRToPeriod")) - 1)
                Else
                    gridSales.Columns(1).Caption = GetPeriodName(CInt(Session("PRToPeriod")) - 12)
                End If

            Case 1
                gridSales.Columns(2).Caption = "Current 3M"
                If Session("PRType2") = 0 Then
                    gridSales.Columns(1).Caption = "Previous 3M"
                Else
                    gridSales.Columns(1).Caption = "Same 3M last year"
                End If

            Case 2
                gridSales.Columns(2).Caption = "Current 12M"
                If Session("PRType2") = 0 Then
                    gridSales.Columns(1).Caption = "Previous 12M"
                Else
                    gridSales.Columns(1).Caption = "Same 12M last year"
                End If

            Case 3
                gridSales.Columns(2).Caption = "Current YTD"
                gridSales.Columns(1).Caption = "YTD last year"

            Case Else

        End Select
    End Sub

    Protected Sub gridSales_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSales.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0

        If e.DataColumn.Caption = "" Then
            e.Cell.Width = 2
        End If

        If e.DataColumn.Caption = "Variance" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.Font.Bold = True
            e.Cell.ForeColor = Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = Drawing.Color.White
                e.Cell.ForeColor = Drawing.Color.Black
            ElseIf nThisValue > 0 Then
                e.Cell.BackColor = Drawing.Color.Green
            Else
                e.Cell.BackColor = Drawing.Color.Red
            End If
        End If

    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "LastMonth" Or e.Column.FieldName = "ThisMonth" Then
            If e.GetFieldValue("CatOrder") = 3 Then
                e.DisplayText = Format(e.Value, "#,##0.00") & "%"
            ElseIf e.GetFieldValue("CatOrder") >= 4 Then
                e.DisplayText = Format(e.Value, "#,##0")
            Else
                e.DisplayText = Format(e.Value, "£#,##0")
            End If
        End If
        If e.Column.FieldName = "Variance" Then
            e.DisplayText = Format(e.Value, "#,##0.00") & "%"
        End If

    End Sub

    Protected Sub dsViewSales_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSales.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        dsViewSales.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Protected Sub ddlType1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType1.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Protected Sub ddlType2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType2.SelectedIndexChanged
        Call RefreshGrid()
    End Sub

    Private Sub RefreshGrid()
        Session("PRToPeriod") = ddlFrom.SelectedValue
        Session("PRType1") = ddlType1.SelectedIndex
        Session("PRType2") = ddlType2.SelectedIndex
        gridSales.DataBind()
    End Sub



    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, Drawing.FontStyle.Regular)
        e.BrickStyle.ForeColor = Drawing.Color.Black
        e.BrickStyle.BackColor = Drawing.Color.White

        If e.Column.Name = "LastMonth" Or e.Column.Name = "ThisMonth" Then
            If e.GetValue("Category") = "Gross Margin" Then
                e.TextValue = Format(e.Value, "#,##0.00") & "%"
            ElseIf e.GetValue("Category") = "Product Groups" Or e.GetValue("Category") = "Active Customers" Or e.GetValue("Category") = "Active Accounts" Then
                e.TextValueFormatString = "#,##0"
            Else
                e.TextValueFormatString = "c0"
            End If
        End If

        If e.Column.Name = "Variance" Then
            If IsDBNull(e.Value) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.Value)
            End If
            e.TextValue = Format(e.Value, "#,##0.00") & "%"
            If nThisValue = 0 Then
                e.BrickStyle.BackColor = Drawing.Color.White
                e.BrickStyle.ForeColor = Drawing.Color.Black
            ElseIf nThisValue > 0 Then
                e.BrickStyle.BackColor = Drawing.Color.Green
            Else
                e.BrickStyle.BackColor = Drawing.Color.Red
            End If
        End If

    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("TradeClubType") = ddlType.SelectedIndex
    End Sub

End Class

