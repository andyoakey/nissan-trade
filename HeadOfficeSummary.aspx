﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HeadOfficeSummary.aspx.vb" Inherits="HeadOfficeSummary" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
 
    
   <div style="position: relative; font-family: Calibri; text-align: left;" >

     <div id="divTitle" style="position: relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Trade Summary Report " />

         <br />
         <br />

           <dx:ASPxComboBox
                    ID ="cboFY"
                    DataSourceID="dsFinYears"
                    ValueField="financialyear"
                    TextField="financialyear"
                    runat="server"
                    AutoPostBack="true"
                    SelectedIndex="0">
            </dx:ASPxComboBox>
     </div>
       
    <table width ="100%">
           <tr>
               <td width ="50%">
                     <dx:ASPxLabel 
                        ID="lblNotes1" 
                        runat="server" 
                        width="680px"
                        CssClass="textnotes"
                        Text="Current month values are shown for reference and will build during the month.">
                    </dx:ASPxLabel>
               </td>

               <td width ="50%" align="right">
                   <ul class="inline_list">
                       <li>
                           <dx:aspxcheckbox 
                                runat="server"
                                id ="chkLikeForLike"
                                Text="Like For Like"
                                AutoPostBack="true"
                               ToolTip="Only show dealers with data for this year and last year">
                            </dx:aspxcheckbox>
                       </li>
                       <li>
                           <dx:aspxcheckbox 
                                runat="server"
                                id ="chkLastYear"
                                Text="Show Last Years Figures"
                                Tooltip="Last years figures are always calculated so that the comparisons can be shown, but the actual figures will be hidden unless this box is ticked"
                                AutoPostBack="true">
                            </dx:aspxcheckbox>
                       </li>
                   </ul>  
               </td>
            </tr>
        </table>

    <dx:ASPxTabControl 
        ID="tabReport" 
        ContentStyle-Wrap="True" 
        AutoPostBack="True"
        TabStyle-HorizontalAlign="Center"
        runat="server" 
        Width="100%"
        ActiveTabIndex="0"
        CssClass="page_tabs">
        <Tabs>
            <dx:Tab Text="Sales Performance - Total "/>
            <dx:Tab Text="Sales Performance - RM and EM"/>
            <dx:Tab Text="Dealer Margin"/>
            <dx:Tab Text="Product Category Sales"/>
            <dx:Tab Text="Customer Summary"/>
        </Tabs>
    </dx:ASPxTabControl>
    
    <br /> 

   <%-- Grid 1 (and 1a) --%>    
   <div id="divGrid1" style="position: relative;" runat="server" >
        <dx:ASPxGridView 
        ID="gridPerformanceTotal" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsPerformanceTotal" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="15%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>


        </Columns>
    </dx:ASPxGridView>

       <br />

        <dx:ASPxGridView 
        ID="gridPerformanceTotal2" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsPerformanceTotal2" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dealer Purchases" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="15%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>


        </Columns>
    </dx:ASPxGridView>

        <dx:ASPxLabel 
        Font-Size="X-Small"
        Font-Italic="true"
        ID="lblCostNotes" 
        runat ="server" 
        Text="* These figures are taken from Dealer Sales files for the months indicated and do not imply that the items sold were actually purchased in that month." />
    </div>
    <%-- End of Grid 1--%>    

    <%-- Grid 2--%>    
   <div id="divGrid2" style="position: relative;" runat="server" >
        <dx:ASPxGridView 
        ID="gridPerformanceRMEM" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsPerformanceRMEM" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="17%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>


        </Columns>
    </dx:ASPxGridView>

        <br />

        <dx:ASPxGridView 
        ID="gridPerformanceRMEM2" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsPerformanceRMEM2" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="17%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>


        </Columns>
    </dx:ASPxGridView>

        <dx:ASPxLabel 
        Font-Size="X-Small"
        Font-Italic="true"
        ID="lblCostNotes2" 
        runat ="server" 
        Text="* These figures are taken from Dealer Sales files for the months indicated and do not imply that the items sold were actually purchased in that month." />
    </div>
    <%-- End of Grid 2--%>           

    <%-- Grid 3--%>    
   <div id="divGrid3" style="position: relative;" runat="server" >
        <dx:ASPxGridView 
        ID="gridMargin" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsMargin" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="22%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

        </Columns>
    </dx:ASPxGridView>
    </div>
    <%-- End of Grid 3--%>    
    
    <%-- Grid 4--%>    
   <div id="divGrid4" style="position: relative;" runat="server" >
        <dx:ASPxGridView 
        ID="gridProduct" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsProduct" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="22%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

        </Columns>
    </dx:ASPxGridView>
   
       <br />

        <dx:ASPxGridView 
        ID="gridProduct2" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsProduct2" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="22%"
                CellStyle-Wrap="false"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="7%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="7%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="7%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="7%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="7%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="7%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="7%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="7%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="7%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="7%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="7%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="7%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
            </dx:GridViewDataTextColumn>

        </Columns>
    </dx:ASPxGridView>

        <dx:ASPxLabel 
        Font-Size="X-Small"
        Font-Italic="true"
        ID="lblCostNotes3" 
        runat ="server" 
        Text="* These figures are taken from Dealer Sales files for the months indicated and do not imply that the items sold were actually purchased in that month." />
    </div>
    <%-- End of Grid 4--%>    

    <%-- Grid 5--%>    
   <div id="divGrid5" style="position: relative;" runat="server" >
        <dx:ASPxGridView 
        ID="gridActiveCustomers" 
        OnLoad ="gridStyles"
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsActiveCustomers" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
        <Settings ShowFooter="False"  UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn 
                visible="false"
                FieldName="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Data Shown" 
                HeaderStyle-HorizontalAlign="Left"
                VisibleIndex="0"
                Width="22%"
                ExportWidth="350"
                Name="rowtext"
                FieldName="rowtext">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Apr" 
                Width="6%"
                VisibleIndex="1"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="apr_data"
                FieldName="apr_data">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="May" 
                Width="6%"
                VisibleIndex="2"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="may_data"
                FieldName="may_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jun" 
                Width="6%"
                VisibleIndex="3"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jun_data"
                FieldName="jun_data">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                Caption="Jul" 
                Width="6%"
                VisibleIndex="4"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jul_data"
                FieldName="jul_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Aug" 
                Width="6%"
                VisibleIndex="5"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="aug_data"
                FieldName="aug_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sep" 
                Width="6%"
                VisibleIndex="6"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="sep_data"
                FieldName="sep_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Oct" 
                Width="6%"
                VisibleIndex="7"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="oct_data"
                FieldName="oct_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Nov" 
                Width="6%"
                VisibleIndex="8"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="nov_data"
                FieldName="nov_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Dec" 
                Width="6%"
                VisibleIndex="9"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="dec_data"
                FieldName="dec_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Jan" 
                Width="6%"
                VisibleIndex="10"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="jan_data"
                FieldName="jan_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Feb" 
                Width="6%"
                VisibleIndex="11"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="feb_data"
                FieldName="feb_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Mar" 
                Width="6%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                Name="mar_data"
                FieldName="mar_data">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Total or Avg." 
                Width="8%"
                VisibleIndex="12"
                ExportWidth="100"
                HeaderStyle-HorizontalAlign="Center"
                FieldName="tot_data">
            </dx:GridViewDataTextColumn>

        </Columns>
    </dx:ASPxGridView>
    </div>
    <%-- End of Grid 5--%>    

</div>



    <%-- Data Sources --%>     
    <asp:SqlDataSource 
        ID="dsFinYears" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_ValidFinYears" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsPerformanceTotal" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_Performance_Total" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

       <asp:SqlDataSource 
        ID="dsPerformanceTotal2" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_Performance_Total2" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>


     
    <asp:SqlDataSource 
        ID="dsPerformanceRMEM" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_Performance_RMEM" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
          <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
      
    <asp:SqlDataSource 
        ID="dsPerformanceRMEM2" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_Performance_RMEM2" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
          <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>




    <asp:SqlDataSource 
        ID="dsMargin" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_Margin" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
           <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
              <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsActiveCustomers" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_ActiveCustomers" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsProduct" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_ByProduct" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsProduct2" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_HO_Summary_ByProduct2" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="ndataperiod" SessionField="nDataPeriod" Type="Int32" />
            <asp:SessionParameter Name="everpresent_flag" SessionField="everpresent_flag" Type="Int32" />
            <asp:SessionParameter Name="showlastyear_flag" SessionField="showlastyear_flag" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    

    <%-- Exporters --%>    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridPerformanceTotal">
    </dx:ASPxGridViewExporter>
 
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1a" 
        runat="server" 
        GridViewID="gridPerformanceTotal2">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter2" 
        runat="server" 
        GridViewID="gridPerformanceRMEM">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter2a" 
        runat="server" 
        GridViewID="gridPerformanceRMEM2">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter3" 
        runat="server" 
        GridViewID="gridMargin">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter4" 
        runat="server" 
        GridViewID="gridProduct">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter4a" 
        runat="server" 
        GridViewID="gridProduct2">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter5" 
        runat="server" 
        GridViewID="gridActiveCustomers">
    </dx:ASPxGridViewExporter>

</asp:Content>

