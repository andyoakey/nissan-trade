﻿Imports System.Data
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class VisitActionSummaryReport

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Visit Action Summary Report"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Action Summary Report", "Viewing Visit Action Summary Report")
            For i = Session("CurrentYear") To 2015 Step -1
                ddlYear.Items.Add(Trim(Str(i)))
            Next
            Session("VARYear") = Session("CurrentYear")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsVAR_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVAR.Init
        dsVAR.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ExpGridDealer_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Call GlobalRenderBrick(e)

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            If e.Column.Name = "CompletedOnTime" Then
                If e.Value > 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.White
                    e.BrickStyle.BackColor = System.Drawing.Color.Gold
                End If
            End If

            If e.Column.Name = "CompletedLate" Or e.Column.Name = "Outstanding" Then
                If e.Value > 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.White
                    e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                End If
            End If

            If e.Column.Name = "AwaitingApproval" Then
                If e.Value > 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.White
                    e.BrickStyle.BackColor = System.Drawing.Color.YellowGreen
                End If
            End If

        End If

    End Sub

    Protected Sub btnExcel_Click()

        Dim nCols As Integer
        Dim nRows As Integer

        nCols = gridVAR.Columns.Count - 1
        nRows = gridVAR.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            ASPxGridViewExporter1.FileName = "VisitActionSummaryReport"
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        End If

    End Sub

    Protected Sub gridVAR_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridVAR.HtmlDataCellPrepared

        If e.DataColumn.FieldName = "CompletedOnTime" Then
            If e.CellValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.BackColor = System.Drawing.Color.Gold
            End If
        End If

        If e.DataColumn.FieldName = "CompletedLate" Or e.DataColumn.FieldName = "Outstanding" Then
            If e.CellValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.BackColor = System.Drawing.Color.Tomato
            End If
        End If

        If e.DataColumn.FieldName = "AwaitingApproval" Then
            If e.CellValue > 0 Then
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.BackColor = System.Drawing.Color.YellowGreen
            End If
        End If

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("VARYear") = ddlYear.SelectedItem.Text
    End Sub

End Class
