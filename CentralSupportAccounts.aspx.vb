﻿Imports DevExpress.XtraPrinting

Partial Class CentralSupportAccounts
    Inherits System.Web.UI.Page
    Dim nTotalThisUnits As Integer = 0
    Dim nTotalThisSales As Decimal = 0
    Dim nTotalThisCustomers As Integer = 0
    Dim nTotalPrevUnits As Integer = 0
    Dim nTotalPrevSales As Decimal = 0
    Dim nTotalPrevCustomers As Integer = 0

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim master_lblPageTitle As DevExpress.Web.ASPxLabel
        master_lblPageTitle = CType(Master.FindControl("lblPageTitle"), DevExpress.Web.ASPxLabel)
        master_lblPageTitle.Text = "Centrally Supported Sales"

        Me.Page.Title = master_lblPageTitle.Text
        Session("nDataPeriod") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'") + 1

        If Page.IsPostBack = False Then
            Session("RefCode") = ""
            If IsPostBack = False Then
                Session("FromMonth") = GetDataDecimal("select min(id) from DataPeriods  where financialyear = (Select financialyear from DataPeriods where id = " & Session("nDataPeriod") & ")")
                Session("ToMonth") = Session("nDataPeriod")
                Me.ddlFrom.Text = GetDataString("Select periodname from DataPeriods where id = " & Session("FromMonth"))
                Me.ddlTo.Text = GetDataString("Select periodname from DataPeriods where id = " & Session("ToMonth"))
            End If
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "CentralSupportAccounts-" & Session("OfferDealerCode")
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub GridStyles2(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub gridTransactions_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("thiscustomerdealerid") = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
    End Sub

    Private Sub btnUpdateReport_Click(sender As Object, e As EventArgs) Handles btnUpdateReport.Click
        Dim sFromMonth As String = ddlFrom.Text
        Dim sToMonth As String = ddlTo.Text
        Session("FromMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlFrom.Value, 8)) & "'")
        Session("ToMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlTo.Value, 8)) & "'")
    End Sub

    Private Sub ddlFrom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("FromMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlFrom.Value, 8)) & "'")
    End Sub

    Private Sub ddlTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("ToMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlTo.Value, 8)) & "'")
    End Sub


End Class





