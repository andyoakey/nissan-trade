﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail
Imports Microsoft.VisualBasic
Imports DevExpress.XtraCharts

Partial Class ProductReport

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Me.Title = "Nissan Trade Site - Product Report"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call GetMonths(ddlFrom)
            Call GetMonths(ddlTo)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Product Report", "Viewing Product Report")
            Session("GraphType") = 1
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
            Session("MonthTo") = Session("MonthFrom")
            Session("TradeClubType") = 0
        End If
        Call CreateChart()
    End Sub

    Private Sub CreateChart()

        ' Create a pie series.
        Dim series1 As New Series("Pie Chart - " & ddlTA.SelectedItem.Text, ViewType.Pie3D)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nFrom", Session("MonthFrom"))
        htin.Add("@nTo", Session("MonthTo"))
        htin.Add("@nType", Session("GraphType"))
        htin.Add("@nTradeClubType", Session("TradeClubType"))
        ds = da.Read(sErrorMessage, "p_Sales_Trends_PieChart", htin)
        With ds.Tables(0)
            For i = 0 To .Rows.Count - 1
                series1.Points.Add(New SeriesPoint(.Rows(i).Item("Product"), .Rows(i).Item("SaleValue")))
            Next
        End With

        ' Add the series to the chart.
        If WebChartControl1.Series.Count > 0 Then
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End If

        WebChartControl1.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff")
        WebChartControl1.Series.Add(series1)

        ' Adjust the point options of the series.
        series1.PointOptions.PointView = PointView.ArgumentAndValues
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent
        series1.PointOptions.ValueNumericOptions.Precision = 0

        ' Access the view-type-specific options of the series.
        Dim myView As Pie3DSeriesView = CType(series1.View, Pie3DSeriesView)

        ' Show a title for the series.
        myView.Titles.Add(New SeriesTitle())
        myView.Titles(0).Text = series1.Name

        ' Specify a data filter to explode points.
        myView.ExplodedPointsFilters.Add(New SeriesPointFilter _
            (SeriesPointKey.Value_1, DataFilterCondition.GreaterThanOrEqual, 9))
        myView.ExplodedPointsFilters.Add(New SeriesPointFilter _
            (SeriesPointKey.Argument, DataFilterCondition.NotEqual, "Others"))
        myView.ExplodeMode = PieExplodeMode.UseFilters
        myView.ExplodedDistancePercentage = 30

        ' Hide the legend (if necessary).
        WebChartControl1.Legend.Visible = False

        gridData.DataSource = ds.Tables(0)
        gridData.DataBind()

    End Sub

    Protected Sub ddlTA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged
        Session("GraphType") = ddlTA.SelectedIndex + 1
        Call CreateChart()
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.SelectedValue
        Call CreateChart()
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.SelectedValue
        Call CreateChart()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("TradeClubType") = ddlType.SelectedIndex
    End Sub

End Class
