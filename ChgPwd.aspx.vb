﻿Imports System.Data
Imports System.Net
Imports QubeSecurity

Partial Class ChgPwd

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Change Password"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        If Request.QueryString IsNot Nothing Then
            If Request.QueryString("gdpr") = 1 Then
                lblMessage.Text = "In-line with GDPR we require you to change your password to a more secure one."
                pnlCurrentPassword.Visible = False
            End If
        End If

    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim isGDPR = false

        If Request.QueryString IsNot Nothing Then
            If Request.QueryString("gdpr") = "1" Then
                isGDPR = true
            End If
        End If

        Dim sOldPassword As String = ""
        Dim sPassword1 As String = txtPassword1.Text
        Dim sPassword2 As String = txtPassword2.Text
        Dim sErrorMsg As String = ""

        If isGDPR Then
            
            If sPassword1.Length = 0 Or sPassword2.Length = 0 Then
                sErrorMsg = sErrorMsg & "<li>You must enter your new password in both fields</li>"
            End If
            
        Else
            sOldPassword = txtCurrentPassword.text

			If Not Membership.ValidateUser(Session("UserEmailAddress"), sOldPassword) Then
				sErrorMsg = sErrorMsg & "<li>Your current password is incorrect. Please try again.</li>"
			End If
			
            If sOldPassword.Length = 0 Or sPassword1.Length = 0 Or sPassword2.Length = 0 Then
                sErrorMsg = sErrorMsg & "<li>You must enter your current and new password in the appropriate fields</li>"
            End If
            
            If sPassword2 = sOldPassword Then
                sErrorMsg = sErrorMsg & "<li>Your new password must be different to your current password</li>"
            End If
        
        End If

        If (sPassword1.Length > 0 And sPassword2.Length > 0) And (sPassword1 <> sPassword2) Then
            sErrorMsg = sErrorMsg & "<li>New passwords do not match. Please check them and try again</li>"
        End If

        'If Not chkTermsConditionsRead.Checked Then
        '    sErrorMsg = sErrorMsg & "<li>Please confirm you have read our Terms &amp; Conditions</li>"
        'End If

        If sErrorMsg.Length > 0 Then
            lblErrorMessage.Text = "<ul class=""passwordrequirements"">" & sErrorMsg & "</ul>"
            txtPassword1.Text = ""
            txtPassword2.Text = ""
        Else
            Dim oUser As MembershipUser = Membership.GetUser()
            If oUser IsNot Nothing Then
                If Authentication.CheckPasswordComplexity(Membership.Provider, sPassword2) Then
                    Dim sPassword As String = oUser.ResetPassword("blue")

                    If oUser.ChangePassword(sPassword, sPassword2) Then

                        'resets guid and termsconditions read
                        Authentication.PasswordResetCleanup(oUser.UserName)

                        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "PasswordChanged", "Password Changed Successful")

                        Session.Abandon()
                        FormsAuthentication.SignOut()
                        Response.Redirect("~/Login.aspx?msg=pwdreset")
                    Else
                        lblErrorMessage.Text = "Password could not be changed at this time."
                    End If
                Else
                    lblErrorMessage.Text = String.Format("New password does not meet the minimum requirements of;<br/><ul class=""passwordrequirements""><li>{0} characters in length</li><li>1 UPPER case character</li><li>1 number</li><li>{1} of the following special characters (<em>*[#!@$%^</em>])</li></ul>", Membership.Provider.MinRequiredPasswordLength, Membership.Provider.MinRequiredNonAlphanumericCharacters)
                End If
            Else
                lblErrorMessage.Text = "Password could not be changed at this time."
            End If
        End If

        If lblErrorMessage.Text.Length > 0 Then
            divErrorMessage.Visible = True
        End If

    End Sub
    

End Class
