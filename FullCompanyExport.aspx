﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="FullCompanyExport.aspx.vb" Inherits="FullCompanyExport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

   <div style="position: relative; font-family: Calibri; text-align: left;" >

     <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblTitle" 
                runat ="server" 
                Text="Full Company Export" />
    </div>

   <dx:ASPxButton 
       ID="btnClearFilters" 
        runat="server" 
       BackColor="Transparent"
       ToolTip="Clear All Filters"
       visible="false"
        Text=" " >
        <Border BorderColor="Transparent" BorderStyle="None" />
        <Image Url="~/Images/btnClearFilter.png" UrlHottracked="~/Images/btnClearFilter_Pressed.png" UrlPressed="~/Images/btnClearFilter_Pressed.png" />
  </dx:ASPxButton> 

    <br />

   <dx:ASPxGridView  
            ID="gridFullCompanyExport" 
            runat="server" 
            DataSourceID="dsFullCompany" 
            OnLoad ="gridStyles"
            AutoGenerateColumns="False"  
            Width="100%">

            <SettingsBehavior   
                AllowSort="false" 
                AllowGroup="false"
                AutoFilterRowInputDelay="1000"
                ColumnResizeMode="Control"  />
                        
            <SettingsText       
                CommandClearFilter="Clear" 
                HeaderFilterShowBlanks=" "
                HeaderFilterShowNonBlanks=" " />
            
            <SettingsPager PageSize="15" ShowDefaultImages="False">
                <AllButton Visible="True"/>
                </SettingsPager>
   
            <Styles Cell-Wrap="False"></Styles>

            <Settings  
                ShowHorizontalScrollBar="True"
                UseFixedTableLayout="True" 
                ShowFilterRowMenu="False" 
                ShowFilterRow="False"
                ShowFooter="False"  
                ShowGroupedColumns="False"
                ShowGroupFooter="Hidden" 
                ShowFilterBar="Hidden"
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" />

                <Styles  Cell-HorizontalAlign="Center" Header-HorizontalAlign="Center" />


  
            <Columns>
                
               <dx:GridViewBandColumn  caption="Account Information"  HeaderStyle-HorizontalAlign="Center" >
                   <Columns>

                        <dx:GridViewDataTextColumn 
                    Caption="Region" 
                    FieldName="region"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="0" 
                    ExportWidth="60"
                    Width="60px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Zone" 
                    FieldName="zone"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="1" 
                    ExportWidth="60"
                    Width="60px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Dealer" 
                    FieldName="dealercode"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="2" 
                    ExportWidth="60"
                    Width="60px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Dealer Name" 
                    FieldName="dealername"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    VisibleIndex="3" 
                    ExportWidth="300"
                    Width="200px">
                </dx:GridViewDataTextColumn>
                                
                 <dx:GridViewDataTextColumn 
                    Caption="AC Code" 
                    FieldName="accountcode"
                    ExportWidth="100"
                    VisibleIndex="4" 
                    Width="70px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Company Name" 
                    FieldName="accountname"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    VisibleIndex="5" 
                    ExportWidth="200"
                    Width="150px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Focus ?" 
                    FieldName="bluegrassfocus_text"
                    VisibleIndex="6" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="100"
                    Width="60px">
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Business Type" 
                    FieldName="businesstype"
                    VisibleIndex="7" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="150"
                    Width="120px">
                </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:GridViewBandColumn>                
                
                <dx:GridViewBandColumn  caption="Email Information"  HeaderStyle-HorizontalAlign="Center" >
                   <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Email" 
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    FieldName="email"
                    VisibleIndex="8" 
                    ExportWidth="300"
                    Width="180px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Email Status" 
                    FieldName="email_status"
                    VisibleIndex="9" 
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportWidth="200"
                    Width="100px">
                </dx:GridViewDataTextColumn>

                    </Columns>
                </dx:GridViewBandColumn>                
                
                <dx:GridViewBandColumn  caption="Contact Information"  HeaderStyle-HorizontalAlign="Center" >
                    <Columns>

                    <dx:GridViewDataTextColumn 
                        Caption="Salutation" 
                        FieldName="contactsalutation"
                        VisibleIndex="10" 
                        ExportWidth="100"
                        Width="70px">
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Name" 
                        HeaderStyle-HorizontalAlign="Left"
                        CellStyle-HorizontalAlign="Left"
                        FieldName="contactname"
                        VisibleIndex="11" 
                        ExportWidth="200"
                        Width="130px">
                    </dx:GridViewDataTextColumn>

                   <dx:GridViewDataTextColumn 
                        Caption="Job Title" 
                        FieldName="contactjobtitle"
                        VisibleIndex="12" 
                        ExportWidth="100"
                        Width="200px">
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Telephone" 
                        FieldName="telephone"
                        VisibleIndex="13" 
                        ExportWidth="100"
                        Width="100px">
                    </dx:GridViewDataTextColumn>
                
                    </Columns>    
                </dx:GridViewBandColumn>                

                <dx:GridViewBandColumn  caption="Address Information"  HeaderStyle-HorizontalAlign="Center" >
                    <Columns>


                <dx:GridViewDataTextColumn 
                    Caption="Address 1" 
                    FieldName="address1"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    ExportWidth="200"
                    VisibleIndex="14" 
                    Width="150px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Address 2" 
                    FieldName="address2"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    VisibleIndex="15" 
                    ExportWidth="200"
                    Width="150px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Address 3" 
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    FieldName="address3"
                    VisibleIndex="16" 
                    ExportWidth="200"
                    Width="150px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Post Town" 
                    FieldName="posttown"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    VisibleIndex="17" 
                    ExportWidth="150"
                    Width="150px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="County" 
                    FieldName="county"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-HorizontalAlign="Left"
                    VisibleIndex="18" 
                    ExportWidth="100"
                    Width="100px">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Postcode" 
                    FieldName="postcode"
                    VisibleIndex="19" 
                    ExportWidth="100"
                    Width="80px">
                </dx:GridViewDataTextColumn>

                        </Columns>
                </dx:GridViewBandColumn>                

                <dx:GridViewBandColumn  caption="Sales Financial Year To Date"  HeaderStyle-HorizontalAlign="Center" >
                   <Columns>

                        <dx:GridViewDataTextColumn 
                    Caption="Routine Maintenance" 
                    FieldName="ROM_fytd"
                    VisibleIndex="20" 
                    HeaderStyle-Wrap="True"
                    ExportWidth="100"
                    Width="100px">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>
                        
                    <dx:GridViewDataTextColumn 
                    Caption="Extended Maintenance" 
                    FieldName="EXM_fytd"
                    VisibleIndex="21" 
                    HeaderStyle-Wrap="True"
                    ExportWidth="100"
                    Width="100px">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    Caption="Damage" 
                    FieldName="DAM_fytd"
                            HeaderStyle-Wrap="True"
                    VisibleIndex="22" 
                    ExportWidth="100"
                    Width="100px">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>
                                
                        <dx:GridViewDataTextColumn 
                    Caption="Total" 
                    FieldName="TOT_fytd"
                    VisibleIndex="23" 
                            HeaderStyle-Wrap="True"
                    ExportWidth="100"
                    Width="100px">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                </dx:GridViewDataTextColumn>

                    </Columns>
                </dx:GridViewBandColumn> 

            </Columns>
  
    </dx:ASPxGridView>

</div>
 
   <asp:SqlDataSource 
        ID="dsFullCompany" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_FullCompanyExtract_2019" 
        SelectCommandType="StoredProcedure">
   </asp:SqlDataSource>

   <%-- Exporters --%>    
   <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridFullCompanyExport">
    </dx:ASPxGridViewExporter>
    
</asp:Content>



