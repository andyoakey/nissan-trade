﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SalesTracker.aspx.vb" Inherits="SalesTracker" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpop" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >

        <div id="divTitle" style="position:relative; left:5px">
            <table width="971px">
                <tr>
                    <td style="width:60%" align="left" valign="middle" > 
                        <asp:Label ID="lblPageTitle" runat="server" Text="Monthly Sales Performance" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True"   Width="225px">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridSalesPerf" runat="server" AutoGenerateColumns="False" Width="971px" Style="margin-left:5px">

            <SettingsPager Mode="ShowAllRecords" Visible="False">
            </SettingsPager>

            <Settings ShowFooter="True" UseFixedTableLayout="True" />

            <Styles>
                <Header HorizontalAlign="Center" Wrap="True" />
                <Cell HorizontalAlign="Center" />
                <Footer HorizontalAlign="Center" />
            </Styles>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <Columns>

                <dxwgv:GridViewDataTextColumn Caption="Month" FieldName="PeriodName" Name="PeriodName" ReadOnly="True" VisibleIndex="0" Width="10%" ExportWidth="120" />

                <dxwgv:GridViewDataTextColumn Caption="Working Days" FieldName="WorkingDays" Name="WorkingDays" ReadOnly="True" VisibleIndex="1" Width="10%"  ExportWidth="120" />

                <dxwgv:GridViewDataTextColumn Caption="Last Year" FieldName="LastYear" Name="LastYear" VisibleIndex="2" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="£#,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="This Year" FieldName="ThisYear" Name="ThisYear" VisibleIndex="3" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="£#,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Variance (£)" FieldName="Variance" Name="Variance" VisibleIndex="4" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="£#,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="%" FieldName="VariancePerc" Name="VariancePerc" VisibleIndex="5" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="0.00%" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Average Daily Sales" FieldName="AverageDailySales" Name="AverageDailySales" VisibleIndex="6" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="£#,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Forecast Sales" FieldName="ForecastSales" Name="ForecastSales" VisibleIndex="7" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="£#,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Forecast %" FieldName="ForecastPerc" Name="ForecastPerc" VisibleIndex="8" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="0.00%" />
                </dxwgv:GridViewDataTextColumn>

            </Columns>

            <TotalSummary>
                <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="WorkingDays" ShowInColumn="WorkingDays" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="LastYear" ShowInColumn="LastYear" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="ThisYear" ShowInColumn="ThisYear" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="Variance" ShowInColumn="Variance" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="0.00%" FieldName="VariancePerc" ShowInColumn="VariancePerc" SummaryType="Custom" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="AverageDailySales" ShowInColumn="AverageDailySales" SummaryType="Custom" />
                <dxwgv:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="ForecastSales" ShowInColumn="ForecastSales" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="0.00%" FieldName="ForecastPerc" ShowInColumn="ForecastPerc" SummaryType="Custom" />
            </TotalSummary>

        </dxwgv:ASPxGridView>
        <br />

    </div>
                
    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridSalesPerf" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>



