﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerDedupe.aspx.vb" Inherits="CustomerDedupe" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <table id="trCustomerDetails" Width="100%" >
                <tr>
                    <td align="left" >

                        <dx:ASPxLabel 
                            Font-Size="Larger"
                            ID="ASPxLabel2" 
                            runat ="server" 
                            Text="Customer Deduplication" />
                 
                        <br />
                        
                        <dx:ASPxLabel 
                            ID="Label1" 
                            runat="server" 
                            Text="Select the Primary Customer and press the 'De-dupe' button to continue" />

                    </td>

                    <td align="right" >
                    </td>
                </tr>
        </table>
        
        <dx:ASPxGridView 
            ID="gridCompanies" 
            runat="server" 
            CssClass="grid_styles"
            Onload="GridStyles"
            DataSourceID="dsCompanies"
            AutoGenerateColumns="False" 
            KeyFieldName="Id" 
            Width="100%" >
            
            <SettingsBehavior AllowFocusedRow="True" />
            
            <SettingsPager PageSize="16" Visible="False"/>

            <Columns>

                <dx:GridViewCommandColumn  
                    Caption="Primary" 
                    ShowSelectCheckbox="True"  
                    VisibleIndex="0">
                </dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="BusinessURN"
                    Caption="URN" 
                    VisibleIndex="1" 
                    Visible="false" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="BusinessName"
                    Caption="Name" 
                    VisibleIndex="1" 
                    Width="20%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Address1"
                    Caption="Address 1" 
                    VisibleIndex="2"
                    Visible = "True" 
                    Width="20%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Address2"
                    Caption="Address 2" 
                    VisibleIndex="2"
                    Visible = "True" 
                    Width="20%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="PostCode"
                    Caption="Postcode" 
                    VisibleIndex="3"
                    Visible = "True" 
                    Width="10%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="TelephoneNumber"
                    Caption="Phone" 
                    VisibleIndex="5" 
                    Width="10%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="CustomerStatus"
                    Caption="Customer Type" 
                    VisibleIndex="6" 
                    Width="10%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="TradeClubNumber"
                    Caption="T/C No." 
                    visible="false"
                    VisibleIndex="7" 
                    Width="0%">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataDateColumn 
                    FieldName="LastSpend"
                    Caption="Last purchased" 
                    VisibleIndex="8" 
                    Width="10%">
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Total MTD" 
                    FieldName="SpendMTD" 
                    VisibleIndex="9"
                    Width="10%" 
                    EditFormSettings-Visible="False">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="Total YTD" 
                    FieldName="SpendYTD" 
                    VisibleIndex="10"
                    Width="10%" 
                    EditFormSettings-Visible="False">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Id" 
                    Caption="Customer #" 
                    VisibleIndex="20"
                    Visible = "False">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                    <CellStyle VerticalAlign="Middle">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

            </Columns>
                 
        </dx:ASPxGridView>

        <br />
        
        <table width="100%">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td align="left" style="width:5%" >
                                <dx:ASPxButton 
                                    ID="btnDup" 
                                    runat="server" 
                                    Text="De-dupe" 
                                    Width="100px" >
                                </dx:ASPxButton>
                            </td>
                            <td align="left" >
                                <dx:ASPxLabel 
                                    ID="lblErr1" 
                                    runat="server" 
                                    Text="" 
                                    Visible="true" 
                                    ForeColor="#FF3300">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" >
                    <dx:ASPxButton 
                        ID="btnCancel" 
                        runat="server" 
                        Text="Cancel" 
                        Width="100px">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <br />

   </div>
                
    <asp:SqlDataSource ID="dsCompanies" runat="server" SelectCommand="p_CustomerListDedupe" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sDeDupeIDs" SessionField="DeDupeIDs" Type="String" Size="255" />
        </SelectParameters>
    </asp:SqlDataSource>


</asp:Content>


