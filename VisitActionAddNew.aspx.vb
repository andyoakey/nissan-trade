﻿Imports System.Data
Imports DevExpress.Web

Partial Class VisitActionAddNew

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Visit Actions"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Add New Visit Action", "Add New Visit Action")
        End If
    End Sub

    Protected Sub dsCentres_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCentres.Init
        dsCentres.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub btnNewVisitSave_Click(sender As Object, e As EventArgs) Handles btnNewVisitSave.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim sDealerCode As String = ddlVisitCentre.SelectedItem.Value
        Dim sVisitDate As String = Format(CDate(txtVisitDate.Text), "dd-MMM-yyyy")

        If sDealerCode <> "" And sVisitDate <> "" Then

            htIn.Add("@sDealerCode", sDealerCode)
            htIn.Add("@sVisitDate", sVisitDate)
            htIn.Add("@nUserId", Session("UserId"))

            nRes = da.Update(sErr, "p_VAR_Header_Ins", htIn)

            If sErr.Length = 0 Then
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Visit Action added", "Centre : " & sDealerCode & "/" & sVisitDate)
                Response.Redirect("VisitActions.aspx")
            Else
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If
        End If

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("VisitActions.aspx")
    End Sub

End Class
