﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TradeRewards.aspx.vb" Inherits="TradeRewards" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"  Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
 
     <dx:ASPxPopupControl 
        ID="popSupportingInfo" 
        runat="server"  
        Width ="800px"  
        Height="500px" 
        HeaderText="Payment Bands and Supporting Information"
        ShowOnPageLoad="False" 
        ClientInstanceName="popSupportingInfo" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        CloseOnEscape="True">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="popSupportingInfoContent" runat="server" CssClass="invoice">

                <table class="dxgvTable_Office2010Black" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; table-layout: fixed; overflow: hidden; text-overflow: ellipsis;">
                    <tbody>
                        <tr>
                            <td style="width: 50%;">Operating Standards</td>
                            <td style="width: 50%;"></td>
                        </tr>                       
                        <tr>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Total Trade Sales by Quarter</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Operating Standards Quarterly Support Payment</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>


                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">< £35,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£0</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £35,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £70,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,500</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £140,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£3,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £210,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£4,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £300,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£5,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £410,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£7,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £475,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£8,000</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £540,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£9,000</td>
                        </tr>
                    </tbody>
                </table>

                <table class="dxgvTable_Office2010Black" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; table-layout: fixed; overflow: hidden; text-overflow: ellipsis;">
                    <tbody>
                        <tr>
                            <td style="width: 50%;"> Mechanical Rewards RM/EM/MR/AP</td>
                            <td style="width: 50%;"></td>
                        </tr>              
               	        <tr>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Percentage point penetration growth</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Rebate %</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
	                    <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">< £35,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£0</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">0.5%</td><td class="dxgv" style="border-bottom-width: 0px;">	2.0%</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">1.0%</td><td class="dxgv" style="border-bottom-width: 0px;">	4.0%</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">2.0%</td><td class="dxgv" style="border-bottom-width: 0px;">	5.0%</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">3.0%</td><td class="dxgv" style="border-bottom-width: 0px;">	6.0%</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">4.0%</td><td class="dxgv" style="border-bottom-width: 0px;">	7.5%</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">5.0%</td><td class="dxgv" style="border-bottom-width: 0px;">	9.0%</td>
                        </tr>
                    </tbody>
                </table>

                 <table class="dxgvTable_Office2010Black" cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse; empty-cells: show; table-layout: fixed; overflow: hidden; text-overflow: ellipsis;">
                    <tbody>
                        <tr>
                            <td style="width: 50%;">Body Rewards DM/AC</td>
                            <td style="width: 50%;"></td>
                        </tr> 
               	        <tr>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Total Trade Sales by Quarter</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Sales Performance Reward - Band 1 Payment</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Sales Performance Reward - Band 2 Payment</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="dxgvHeader_Office2010Black" style="text-align: Center; border-top-width: 0px; border-left-width: 0px; cursor: default;">
                                <table cellspacing="0" cellpadding="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: Center;">Sales Performance Reward - Band 3 Payment</td>
                                            <td style="width: 1px; text-align: right;"><span class="dx-vam">&nbsp;</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">< £24,500</td><td class="dxgv" style="border-bottom-width: 0px;">£0</td><td class="dxgv" style="border-bottom-width: 0px;">£0</td><td class="dxgv" style="border-bottom-width: 0px;">	£0</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £24,500</td><td class="dxgv" style="border-bottom-width: 0px;">	£250	</td><td class="dxgv" style="border-bottom-width: 0px;">£375</td><td class="dxgv" style="border-bottom-width: 0px;">	£500</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £50,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£375</td><td class="dxgv" style="border-bottom-width: 0px;">	£500</td><td class="dxgv" style="border-bottom-width: 0px;">	£875</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;"> > £100,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£500	</td><td class="dxgv" style="border-bottom-width: 0px;">£1,000	</td><td class="dxgv" style="border-bottom-width: 0px;">£1,500</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £150,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,000	</td><td class="dxgv" style="border-bottom-width: 0px;">£1,750	</td><td class="dxgv" style="border-bottom-width: 0px;">£2,500</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £210,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,250</td><td class="dxgv" style="border-bottom-width: 0px;">	£2,500</td><td class="dxgv" style="border-bottom-width: 0px;">	£3,750</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £290,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,500</td><td class="dxgv" style="border-bottom-width: 0px;">	£3,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£4,500</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £332,500</td><td class="dxgv" style="border-bottom-width: 0px;">	£1,750</td><td class="dxgv" style="border-bottom-width: 0px;">	£3,500</td><td class="dxgv" style="border-bottom-width: 0px;">	£5,250</td>
                        </tr>
                        <tr class="dxgvEmptyDataRow_Office2010Black">
                            <td class="dxgv" style="border-bottom-width: 0px;">> £380,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£2,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£4,000</td><td class="dxgv" style="border-bottom-width: 0px;">	£6,000</td>
                        </tr>
                    </tbody>
                </table>
   
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    
   <div style="position: relative; font-family: Calibri; text-align: left;" >

     <div id="divTitle" style="position: relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Trade Summary Report " />

         <br />
         <br />

           <dx:ASPxComboBox
                    ID ="cboFY"                   
                    ValueField="financialyear"
                    TextField="financialyear"
                    runat="server"
                    AutoPostBack="true"
                    SelectedIndex="0">
            </dx:ASPxComboBox>
     </div>

        <%-- Grid 1 --%>    
   <div id="divGrid1" style="position: relative;" runat="server" >
        <dx:ASPxGridView
        ID="grdTradeRewards1" 
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%"
        Caption="Body & Accessory Sales Excluding NWCR (used to calculate growth level)">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />
        <Settings ShowFooter="False" UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Caption="Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Caption="Working Days this Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Working Days Completed" FieldName=""></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="Daily Run Rate This Quarter" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="4" Caption="Actual - FY19 (Quarter to Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Caption="Actual - FY19 (Full Quarter)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="6" Caption="Actual - FY20 (Quarter To Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="p"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="7" Caption="Growth Year on Year Projected" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="8" Caption="Projected Sales" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>
            
        </Columns>
    </dx:ASPxGridView>


    </div>
    <%-- End of Grid 1--%>    

        <%-- Grid 2 --%>    
   <div id="div1" style="position: relative;" runat="server" >
        <dx:ASPxGridView
        ID="ASPxGridView1" 
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%"
        Caption="Body & Accessory Sales Including NWCR (used to calculate reward value)	">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />
        <Settings ShowFooter="False" UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Caption="Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Caption="Working Days this Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Working Days Completed" FieldName=""></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="Daily Run Rate This Quarter" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="4" Caption="Actual - FY19 (Quarter to Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Caption="Actual - FY19 (Full Quarter)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="6" Caption="Actual - FY20 (Quarter To Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="p"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="7" Caption="Growth Year on Year Projected" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="8" Caption="Projected Sales" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>
            
        </Columns>
    </dx:ASPxGridView>


    </div>
    <%-- End of Grid 2--%>    

        <%-- Grid 3 --%>    
   <div id="div2" style="position: relative;" runat="server" >
        <dx:ASPxGridView
        ID="ASPxGridView2" 
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%"
        Caption="Mechanical Sales in AOR">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />
        <Settings ShowFooter="False" UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Caption="Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Caption="Working Days this Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Working Days Completed" FieldName=""></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="Daily Run Rate This Quarter" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="4" Caption="Actual - FY19 Mechanical Sales in AOR" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Caption="Actual - FY19 Penetration (Full Quarter)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="6" Caption="Mechanical Sales in AOR - FY20 (Quarter to Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="p"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="7" Caption="Projected Sales" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="8" Caption="Projected Penetration" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>
            
        </Columns>
    </dx:ASPxGridView>


    </div>
    <%-- End of Grid 3--%>  
       
        <%-- Grid 4 --%>    
   <div id="div3" style="position: relative;" runat="server" >
        <dx:ASPxGridView
        ID="ASPxGridView3" 
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%"
        Caption="Body & Accessory Sales Excluding NWCR (used to calculate growth level)">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />
        <Settings ShowFooter="False" UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Caption="Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Caption="Working Days this Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Working Days Completed" FieldName=""></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="Daily Run Rate This Quarter" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="4" Caption="Actual - FY19 (Quarter to Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Caption="Actual - FY19 (Full Quarter)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="6" Caption="Actual - FY20 (Quarter To Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="p"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="7" Caption="Growth Year on Year Projected" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="8" Caption="Projected Sales" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>
            
        </Columns>
    </dx:ASPxGridView>


    </div>
    <%-- End of Grid 4--%>  
       
        <%-- Grid 5 --%>    
   <div id="div4" style="position: relative;" runat="server" >
        <dx:ASPxGridView
        ID="ASPxGridView4" 
        CssClass="grid_styles"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="" 
        Styles-HeaderPanel-Wrap="True"
        Styles-Header-Wrap="True"
        Width="100%"
        Caption="Body & Accessory Sales Excluding NWCR (used to calculate growth level)">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />
        <Settings ShowFooter="False" UseFixedTableLayout="True" ShowTitlePanel="False" />
        <SettingsPager AlwaysShowPager="false" PageSize="20" />

        <Columns>
        
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Caption="Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="1" Caption="Working Days this Quarter" FieldName=""></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Working Days Completed" FieldName=""></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="3" Caption="Daily Run Rate This Quarter" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="4" Caption="Actual - FY19 (Quarter to Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="5" Caption="Actual - FY19 (Full Quarter)" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="6" Caption="Actual - FY20 (Quarter To Date)" FieldName=""><PropertiesTextEdit DisplayFormatString="p"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="7" Caption="Growth Year on Year Projected" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>	
            <dx:GridViewDataTextColumn HeaderStyle-HorizontalAlign="Center" VisibleIndex="8" Caption="Projected Sales" FieldName=""><PropertiesTextEdit DisplayFormatString="&#163;##,##0"/></dx:GridViewDataTextColumn>
            
        </Columns>
    </dx:ASPxGridView>


    </div>
    <%-- End of Grid 5--%>    

       <asp:SqlDataSource ID="dsGrid1" runat="server" SelectCommand="sp_BonusBandingsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
             <asp:SessionParameter Name="FinYear" SessionField="ShortFinYear" Type="String" Size="5" />
             <asp:SessionParameter Name="dealerband" SessionField="dealerband" Type="Int16" Size="1" />
        </SelectParameters>
     </asp:SqlDataSource>

   </div>
</asp:Content>