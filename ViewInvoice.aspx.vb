﻿Imports System.Data

Partial Class ViewInvoice

    Inherits System.Web.UI.Page

    Dim sDealerCode As String, sInvoiceNumber As String
    Dim da As New DatabaseAccess, ds As DataSet, dsC As DataSet, sErr, sSQL As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Invoice Details"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        sDealerCode = Left(Request.QueryString(0), 4)
        sInvoiceNumber = Mid(Request.QueryString(0), 5)
        Session("DealerCode") = sDealerCode
        Session("InvoiceNumber") = sInvoiceNumber
        dsInvoiceParts.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        Call FetchCompanyDetails()
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "View Invoice", "Viewing Invoice Number : " & lblInvoiceNo.Text)

    End Sub

    Private Sub ViewInvoice_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete


        '  Hide all Master Page stuff EXCEPT back button, so only way out is by pressing back button ( or closing browser window which does the same thing)
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = False

        Dim master_btnBack As DevExpress.Web.ASPxButton
        master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
        master_btnBack.Visible = False

        Dim master_menu As DevExpress.Web.ASPxMenu
        master_menu = CType(Master.FindControl("ASPxMenu1"), DevExpress.Web.ASPxMenu)
        master_menu.Visible = False

        Dim master_btnMastCustSeach As DevExpress.Web.ASPxButton
        master_btnMastCustSeach = CType(Master.FindControl("btnMastCustSeach"), DevExpress.Web.ASPxButton)
        master_btnMastCustSeach.Visible = False

        Dim master_ddlAccessPoint As DevExpress.Web.ASPxComboBox
        master_ddlAccessPoint = CType(Master.FindControl("ddlAccessPoint"), DevExpress.Web.ASPxComboBox)
        master_ddlAccessPoint.Visible = False


        If Session("BackClicked") = True Then
            Session("BackClicked") = False
            If Session("ReturnProgram") Is Nothing Then
                Response.Redirect("Dashboard.aspx")  ' Catch all if something has gone wrong
            Else
                Response.Write("<script language='javascript'> { window.close();}</script>")
            End If
        End If

        '  End Handle Master Page Button Clicks


    End Sub


    Protected Sub dsInvoiceParts_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsInvoiceParts.Init
        Session("InvoiceNumber") = sInvoiceNumber
        dsInvoiceParts.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsInvoiceTotals_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsInvoiceTotals.Init
        Session("InvoiceNumber") = sInvoiceNumber
        dsInvoiceTotals.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Sub FetchCompanyDetails()

        sSQL = "SELECT TOP 1 * FROM v_AllPartTransaction WHERE DealerCode = '" & sDealerCode & "' AND InvoiceNumber = '" & sInvoiceNumber & "'"
        ds = da.ExecuteSQL(sErr, sSQL)

        If ds.Tables(0).Rows.Count = 1 Then

            With ds.Tables(0).Rows(0)
                lblDealerCode.Text = .Item("DealerCode")
                lblDealerName.Text = .Item("CentreName")
                lblInvoiceNo.Text = .Item("InvoiceNumber")
                lblInvoiceDate.Text = Format(.Item("InvoiceDate"), "dd-MMM-yyyy")
                lblCompany.Text = Trim(.Item("CustomerName")) & vbCrLf & FormatAddress(.Item("CustomerAddress1"), .Item("CustomerAddress2"), .Item("CustomerAddress3"), .Item("CustomerAddress4"), .Item("CustomerAddress5"), .Item("CustomerPostCode"), True)
                lblAccount.Text = Trim(.Item("DMSRef")) & vbCrLf & Trim(.Item("AccountName")) & vbCrLf & FormatAddress(.Item("AccountAddress1"), .Item("AccountAddress2"), .Item("AccountAddress3"), .Item("AccountAddress4"), .Item("AccountAddress5"), .Item("AccountPostCode"), True)
            End With

        End If

    End Sub

    Protected Sub gridsParts_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridParts.HtmlDataCellPrepared
        If e.KeyValue = 1 Then
            e.Cell.Font.Italic = True
            e.Cell.ForeColor = Drawing.Color.Red
        End If
        If e.GetValue("Department") <> "P" Or e.GetValue("SaleType") <> "T" Then
            e.Cell.BackColor = Drawing.Color.LightGray
            e.Cell.Font.Italic = True
            txtNotes.Visible = True
            txtNotes.Text = "* - Invalid Department Code or Sales Type" & vbCrLf & "This invoice is not included in the website database."
            txtNotes.Font.Bold = True
            txtNotes.ForeColor = Drawing.Color.Red
            gridTotals.Visible = False
        End If
    End Sub

    Protected Sub gridsTotals_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridTotals.HtmlDataCellPrepared
        If InStr(e.KeyValue, "Excluded") > 0 Then
            e.Cell.Font.Italic = True
            e.Cell.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
        If e.Column.FieldName = "Department" Then
            If e.Value <> "P" Then
                e.DisplayText = "* " & e.Value
            End If
        End If
        If e.Column.FieldName = "SaleType" Then
            If e.Value <> "T" Then
                e.DisplayText = "* " & e.Value
            End If
        End If
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
