﻿Imports System.Data
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting

Partial Class MechCompSalesPerf

    Inherits System.Web.UI.Page

    Dim dsTargetResults As DataSet
    Dim dsSalesResults1 As DataSet
    Dim dsSalesResults2 As DataSet
    Dim dsKPIResults1 As DataSet
    Dim dsKPIResults2 As DataSet
    Dim dsCustomerSummaryResults As DataSet
    Dim dsBusinessSummaryResults As DataSet
    Dim dsBD4 As DataSet
    Dim dsTradeRewards As DataSet
    Dim dsStandards As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Try
            If Not IsPostBack Then
                Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
                Session("TradeRewardYear") = 2014
            End If
        Catch ex As Exception
            Session("CallingModule") = "MechCompSalesPerf - Page_Load()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim sDate As String
        Dim sWholeMonth As String

        Dim sSelectionLevel As String = Session("SelectionLevel")
        Dim sSelectionId As String = Session("SelectionId")
        Dim nMonthFrom As Integer = Session("MonthFrom")
        Dim nTradeRewardYear As Integer = Session("TradeRewardYear")

        Try

            Me.Title = "Nissan Trade Site - Mechanical Competitive Sales Performance"
            Call LoadTradeTargetData()
            sDate = GetMaxDate(sSelectionLevel, sSelectionId)
            sWholeMonth = GetDataString("SELECT dbo.NicePeriodName(PeriodName) FROM DataPeriods WHERE Id = (SELECT DataEndPeriodWholeMonth FROM System)")
            lblPageTitle.Text = "Mechanical Competitive Sales Performance : Sales up to and including : " & sDate

        Catch ex As Exception
            Session("CallingModule") = "MechCompSalesPerf - Page_LoadComplete()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

    End Sub

    Function GetPercChange(ByVal nThisYear As Decimal, ByVal nLastYear As Decimal) As Decimal
        Dim nResult As Decimal
        nResult = 0
        If nThisYear <> 0 And nLastYear <> 0 Then
            nResult = (nThisYear - nLastYear) / nLastYear
        End If
        GetPercChange = nResult
    End Function

    Function GetMargin(ByVal nSale As Decimal, ByVal nCost As Decimal) As Decimal
        Dim nResult As Decimal
        nResult = 0
        If nSale <> 0 And nCost <> 0 Then
            nResult = (nSale - nCost) / nSale
        End If
        GetMargin = nResult
    End Function

    Protected Sub gridTargets_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "TargetValue" Then
            Select Case e.GetFieldValue("VarType")
                Case "M"
                    e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "£#,##0")
                Case "P"
                    e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
                Case Else
            End Select
        End If

    End Sub

    Protected Sub gridTargets_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        Dim nThisValue As Decimal = 0

        If e.DataColumn.FieldName = "TargetValue" Then

            If e.GetValue("VarType") = "P" Then
                If IsDBNull(e.CellValue) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.CellValue)
                End If
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = System.Drawing.Color.White
                If nThisValue = 0 Then
                    e.Cell.Font.Bold = False
                    e.Cell.BackColor = System.Drawing.Color.White
                    e.Cell.ForeColor = System.Drawing.Color.Black
                ElseIf nThisValue > 100 Then
                    e.Cell.BackColor = System.Drawing.Color.Green
                Else
                    e.Cell.BackColor = System.Drawing.Color.Red
                End If
            End If

        End If

    End Sub

    Protected Sub gridTargetSummary2_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "MCApplicSales" Or e.Column.FieldName = "MCApplicAverage" Then
            If e.GetFieldValue("MCApplicSales") > 0 Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "£#,##0")
            Else
                e.DisplayText = "-"
            End If
        End If

        If e.Column.FieldName = "MCApplicAchieved" Then
            If e.GetFieldValue("MCApplicSales") > 0 Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            Else
                e.DisplayText = "-"
            End If
        End If

        If e.Column.FieldName = "MCTotalSales" Or e.Column.FieldName = "MCTotalAverage" Then
            If e.GetFieldValue("MCTotalSales") > 0 Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "£#,##0")
            Else
                e.DisplayText = "-"
            End If
        End If

    End Sub

    Protected Sub gridTargetSummary3_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.GetFieldValue("Row") = 0 Then

            If e.Column.FieldName = "TargetValue1" Or e.Column.FieldName = "TargetValue2" Then
                If IsDBNull(e.Value) Then
                    e.DisplayText = "-"
                Else
                    e.DisplayText = Format(IIf(IsDBNull(e.Value), "-", Val(e.Value)), "£#,##0")
                End If
            Else
                If IsDBNull(e.Value) Then
                    e.DisplayText = "-"
                Else
                    If IsNumeric(e.Value) Then
                        e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
                    End If
                End If
            End If

        Else

            If InStr(e.Column.FieldName, "TargetValue") > 0 Then
                If e.GetFieldValue("Row") < 3 Then
                    If IsDBNull(e.Value) Then
                        e.DisplayText = "-"
                    Else
                        e.DisplayText = Format(IIf(IsDBNull(e.Value), "-", Val(e.Value)), "£#,##0")
                    End If
                Else
                    If IsDBNull(e.Value) Then
                        e.DisplayText = "-"
                    Else
                        If IsNumeric(e.Value) Then
                            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
                        End If
                    End If
                End If
            End If

        End If

    End Sub

    Protected Sub gridTargetSummary3_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)
        If e.GetValue("Row") = 0 Then
            e.Cell.BackColor = System.Drawing.Color.LightGray
            'e.Cell.Font.Italic = True
        End If
        If e.DataColumn.FieldName = "TargetValue1" Or e.DataColumn.FieldName = "TargetValue2" Then
            If e.GetValue("Row") = 3 Then
                e.Cell.ForeColor = System.Drawing.Color.White
                If e.CellValue < 100 Then
                    e.Cell.BackColor = System.Drawing.Color.Red
                Else
                    e.Cell.BackColor = System.Drawing.Color.Green
                End If
            End If
        End If
        If e.DataColumn.FieldName = "TargetValue3" Then
            If e.GetValue("Row") = 0 Then
                e.Cell.ForeColor = System.Drawing.Color.White
                If e.CellValue < 100 Then
                    e.Cell.BackColor = System.Drawing.Color.Red
                Else
                    e.Cell.BackColor = System.Drawing.Color.Green
                End If
            End If
        End If
    End Sub

    Sub LoadTradeTargetData()

        Dim htIn1 As New Hashtable
        Dim htIn2 As New Hashtable

        Dim sSelectionLevel As String = Session("SelectionLevel")
        Dim sSelectionId As String = Session("SelectionId")

        Dim da As New DatabaseAccess
        Dim sErr As String = ""

        Try

            htIn1.Add("@sDealerCode", sSelectionId)
            dsTargetResults = da.Read(sErr, "p_2014_MC_Targets_Summary", htIn1)
            If sErr <> "" Then
                Session("CallingModule") = "MechCompSalesPerf - LoadTradeTargetData() - p_2014_MC_Targets_Summary"
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

            gridTargetSummary2.DataSource = dsTargetResults.Tables(0)
            gridTargetSummary2.DataBind()

            gridTargetSummary3.DataSource = dsTargetResults.Tables(2)
            gridTargetSummary3.DataBind()

            gridApplicPerf.DataSource = dsTargetResults.Tables(3)
            gridApplicPerf.DataBind()

            htIn2.Add("@sDealerCode", sSelectionId)
            dsBD4 = da.Read(sErr, "p_2014_BD4", htIn2)
            If sErr <> "" Then
                Session("CallingModule") = "MechCompSalesPerf - LoadTradeTargetData() - p_2014_BD4"
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

        Catch ex As Exception
            Session("CallingModule") = "MechCompSalesPerf - LoadTradeTargetData()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try

    End Sub

    Protected Sub gridApplicPerf_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        Dim nThisValue As Decimal = 0

        If e.DataColumn.FieldName = "Variance" Then

            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.Font.Bold = True
            e.Cell.ForeColor = System.Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.ForeColor = System.Drawing.Color.Black
            ElseIf nThisValue > 1 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            Else
                e.Cell.BackColor = System.Drawing.Color.Red
            End If

        End If

    End Sub

End Class
