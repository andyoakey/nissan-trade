﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VisitActionSummaryReport.aspx.vb" Inherits="VisitActionSummaryReport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px; padding-bottom:10px;">
            <table width="968px">
                <tr>
                    <td style="width:60%" align="left" valign="middle" > 
                        <asp:Label ID="lblPageTitle" runat="server" Text="Visit Action Summary Report" Style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridView ID="gridVAR" ClientInstanceName="gridVAR" runat="server" width="967px" 
            AutoGenerateColumns="False" KeyFieldName="Id"  DataSourceID="dsVAR" style="position:relative; left:5px;">
       
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center" />
                <Cell Font-Size="Small" HorizontalAlign="Center"/>
                <LoadingPanel ImageSpacing="10px" />
            </Styles>
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False"
                ShowGroupFooter="VisibleIfExpanded" 
                UseFixedTableLayout="True" VerticalScrollableHeight="300" 
                ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False" 
                ShowFilterRowMenu="True" ShowHeaderFilterBlankItems="false"  />
                
            <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False"  />
            
            <Images>
                <CollapsedButton Height="12px" Width="11px" />
                <ExpandedButton Height="12px" Width="11px" />
                <DetailCollapsedButton Height="12px" Width="11px" />
                <DetailExpandedButton Height="12px" Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="Id" VisibleIndex="0" Visible="false" >
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="1" Width="10%" ExportWidth="120" />

                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="2" ExportWidth="300" />

                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone" VisibleIndex="3" Width="15%" ExportWidth="120" />

                <dxwgv:GridViewDataTextColumn Caption="Visits YTD" FieldName="VisitsYTD" VisibleIndex="4" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />

                <dxe:GridViewBandColumn Caption="Actions">

                        <Columns>

                            <dxwgv:GridViewDataTextColumn Caption="Actions YTD" FieldName="ActivitiesYTD" VisibleIndex="5" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />
                            <dxwgv:GridViewDataTextColumn Caption="Outstanding" FieldName="Outstanding" Name="Outstanding" VisibleIndex="6" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />
                            <dxwgv:GridViewDataTextColumn Caption="Awaiting TPSM Approval" FieldName="AwaitingApproval" Name="AwaitingApproval" VisibleIndex="7" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />
                            <dxwgv:GridViewDataTextColumn Caption="Completed On Time" FieldName="CompletedOnTime" Name="CompletedOnTime" VisibleIndex="8" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />
                            <dxwgv:GridViewDataTextColumn Caption="Completed Late" FieldName="CompletedLate" Name="CompletedLate" VisibleIndex="9" Width="8%" PropertiesTextEdit-DisplayFormatString="#,##0" ExportWidth="120" />

                        </Columns>

                </dxe:GridViewBandColumn>

            </Columns>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

        </dxwgv:ASPxGridView>
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsVAR" runat="server" SelectCommand="p_Web_VAR_Summary2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nThisYear" SessionField="VARYear" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridVAR" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>











