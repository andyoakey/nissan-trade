﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReminderAck.aspx.vb" Inherits="ReminderAck" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:275px;">

        <dxe:ASPxButton Font-Size="14px" Font-Names="Calibri,Verdana" ID="btnRemove" runat="server" Text="Acknowledge" 
            Style="height:20px; width:100px; margin: -20px -50px; position:relative; top:50%; left:50%;">
        </dxe:ASPxButton>

    </div>
    </form>
</body>
</html>
