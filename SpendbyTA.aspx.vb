﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Partial Class SpendbyTA

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Sales Trends"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales Comparisons Page", "Viewing Sales Comparison Page")
        End If
    End Sub

    Protected Sub dsSpendByTA_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSpendsByTA.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        dsSpendsByTA.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ddlTA_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged

        Dim i As Integer
        Dim nOrder As Integer

        nOrder = 1
        For i = 1 To 32
            gridSpendByTA.Columns(i).Visible = False
        Next

        WebChart1.Visible = False
        WebChart2.Visible = False
        WebChart3.Visible = False

        If ddlTA.SelectedIndex = 0 Then
            For i = 1 To 7
                gridSpendByTA.Columns(i).Visible = True
                gridSpendByTA.Columns(i).VisibleIndex = nOrder
                nOrder = nOrder + 1
            Next
            WebChart1.Visible = True
        End If

        If ddlTA.SelectedIndex = 1 Then
            For i = 19 To 32
                gridSpendByTA.Columns(i).Visible = True
                gridSpendByTA.Columns(i).VisibleIndex = nOrder
                nOrder = nOrder + 1
            Next
            WebChart2.Visible = True
        End If

        If ddlTA.SelectedIndex = 2 Then
            For i = 8 To 18
                gridSpendByTA.Columns(i).Visible = True
                gridSpendByTA.Columns(i).VisibleIndex = nOrder
                nOrder = nOrder + 1
            Next
            WebChart3.Visible = True
        End If

    End Sub

    Protected Sub gridSpendByTA_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index = 0 Then
            e.DisplayText = NicePeriodName(e.Value)
        End If
    End Sub



    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("TradeClubType") = ddlType.SelectedIndex
    End Sub

End Class
