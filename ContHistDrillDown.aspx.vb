﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ContHistDrillDown

    Inherits System.Web.UI.Page

    Dim sDealerCode As String
    Dim sMonth As String
    Dim nContactType As Integer
    Dim nTCMembers As Integer
    Dim dsContactTracker As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim htin As New Hashtable
        Dim nMonth As Integer

        sDealerCode = Request.QueryString(0)
        sMonth = Request.QueryString(1)
        nContactType = Val(Request.QueryString(2))
        nTCMembers = Val(Request.QueryString(3))

        If sMonth = "Six_Weeks" Then
            nMonth = 13
        Else
            nMonth = Mid(sMonth, 7)
        End If

        htin.Add("@sDealerCode", sDealerCode)
        htin.Add("@nContactType", nContactType)
        htin.Add("@nTCMembers", nTCMembers)
        htin.Add("@nMonth", nMonth)
        htin.Add("@nThisYear", Session("ContactHistYear"))
        dsContactTracker = da.Read(sErrorMessage, "p_ContactSummary_DrillDown2", htin)
        gridContHistDrillDown.DataSource = dsContactTracker.Tables(0)
        gridContHistDrillDown.DataBind()

    End Sub

    Protected Sub gridContHistDrillDown_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridContHistDrillDown.CustomColumnDisplayText
        If e.Column.FieldName = "ContactType" Then
            Select Case e.Value
                Case 0
                    e.DisplayText = "Phone"
                Case 1
                    e.DisplayText = "Email"
                Case 2
                    e.DisplayText = "Visit"
                Case 3
                    e.DisplayText = "Mail"
                Case Else
                    e.DisplayText = ""
            End Select
        End If
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call RenderBrick(e)
    End Sub

    Private Sub RenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Call GlobalRenderBrick(e)

        If e.Column.Name = "ContactType" Then
            Select Case e.Value
                Case 0
                    e.TextValue = "Phone"
                Case 1
                    e.TextValue = "Email"
                Case 2
                    e.TextValue = "Visit"
                Case Else
                    e.TextValue = ""
            End Select
        End If

        If e.Column.Name = "CustomerId" Then
            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        End If

    End Sub

End Class
