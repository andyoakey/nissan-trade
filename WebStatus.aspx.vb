﻿Partial Class WebStatus

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Web Status"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Web Status", "Web Status")
        End If
        ' Hide Excel Button (on masterpage) 
        Dim myExcelButton As DevExpress.Web.ASPxButton
        myExcelButton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myExcelButton Is Nothing Then
            myExcelButton.Visible = False
        End If
    End Sub

    Protected Sub dsStatus_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsStatus.Init
        dsStatus.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

End Class
