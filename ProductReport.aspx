<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ProductReport.aspx.vb" Inherits="ProductReport" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width:976px; margin: 0 auto; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Product Report" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <table id="trSelectionStuff" width="976px">
            <tr id="Tr2" runat="server" style="height:30px;">
                <td style="font-size: small;" align="left" >
                    By
                </td>
                <td style="width:20%; font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlTA" runat="server" AutoPostBack="True" 
                        Width="180px"   >
                        <asp:ListItem>Product Category</asp:ListItem>
                        <asp:ListItem>Product Group</asp:ListItem>
                        <asp:ListItem>Mech Comp Breakdown</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="left" >
                    From
                </td>
                <td style="width:20%; font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlFrom" runat="server" AutoPostBack="True" Width="180px"   >
                    </asp:DropDownList>
                </td>
                <td style="font-size: small;" align="left" >
                    To
                </td>
                <td style="width:20%; font-size: small;" align="left" >
                    <asp:DropDownList ID="ddlTo" runat="server" AutoPostBack="True" Width="180px"   >
                    </asp:DropDownList>
                </td>
                <td align="right" >
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" Width="250px" 
                          >
                        <asp:ListItem>All Customers</asp:ListItem>
                        <asp:ListItem>Trade Club Members only</asp:ListItem>
                        <asp:ListItem>Not Trade Club Members</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />

        <table id="tabCharts" width="976px">
            <tr>
                <td style="width:60%">
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" Height="500px" 
                        Width="580px" PaletteName="Office" BackColor="White">

                    </dxchartsui:WebChartControl>
                </td>
                <td valign="top" align="right">
                    <dxwgv:ASPxGridView ID="gridData" runat="server" AutoGenerateColumns="False"  Width="380px" >
                        <Styles>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <Footer HorizontalAlign="Center">
                            </Footer>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                        <SettingsPager Visible="False" Mode="ShowAllRecords">
                        </SettingsPager>
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0"  FieldName="SaleValue" ShowInColumn="Sales" ShowInGroupFooterColumn="Sales" SummaryType="Sum"/>
                        </TotalSummary>
                                                <Columns>
                            <dxwgv:GridViewDataTextColumn Caption="Product" FieldName="Product" Width="250px" 
                                VisibleIndex="0">
                                <HeaderStyle HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                                <FooterCellStyle HorizontalAlign="Center">
                                </FooterCellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn Caption="Sales" FieldName="SaleValue" VisibleIndex="1" Width="125px">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <Settings ShowFooter="True" />
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>
                    </dxwgv:ASPxGridView>
                    <br />
                </td>
            </tr>
        </table>
        
        <br />
        
    </div>
    
</asp:Content>

