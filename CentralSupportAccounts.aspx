﻿<%@ Page AutoEventWireup="false" CodeFile="CentralSupportAccounts.aspx.vb" Inherits="CentralSupportAccounts"   Language="VB" MasterPageFile="~/MasterPage.master" Title="Central Support Accounts" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"     Namespace="DevExpress.Web" TagPrefix="dx" %> 


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >


<table width="100%">
    <tr>
        <td width="100%">
            <ul class="inline_list">
                <li>
                    <dx:ASPxLabel 
                         ID="lblMonthFrom" 
                         runat="server" 
                         Text="First Month of Report:">
                      </dx:ASPxLabel>
                    <dx:ASPxComboBox 
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="true"
                        DataSourceID="sqlPopMonth" 
                        TextField="periodname" 
                        ValueField="periodname" 
                        width="144px">
                      </dx:ASPxComboBox>
                </li>
                <li>
                    <dx:ASPxLabel  
                         ID="lblMonthTo" 
                         runat="server" 
                         Text="Last Month of Report:">
                      </dx:ASPxLabel>

                    <dx:ASPxComboBox 
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="true"
                        DataSourceID="sqlPopMonth" 
                        TextField="periodname" 
                        ValueField="periodname" 
                        width="144px">
                      </dx:ASPxComboBox>
                </li>
            </ul>
        </td>
    </tr>
</table>

<br />

<dx:ASPxLabel 
        ID="lblSubHead" 
        runat="server" 
        CssClass="textnotes"
        Text="Note: Accounts shown are not included in overall dealer trade sales performance and are only shown within this specific report" />							

<br />
  

  <dx:ASPxButton 
       ID="btnUpdateReport" 
       AutoPostBack="true"
       Text="Update Report"
       runat="server" 
       visible ="false"
       Style="left: 300px; position: absolute; top: 157px; z-index: 109;">
  </dx:ASPxButton>

  <dx:ASPxGridView 
        ID="gridMain" 
        OnLoad="GridStyles" style="position:relative; margin-top: 5px;"
        runat="server" 
        SettingsDetail-ExportMode="Expanded"         
        AutoGenerateColumns="False"  
        Styles-AlternatingRow-BackColor = "GhostWhite"
        SettingsBehavior-ProcessSelectionChangedOnServer="true"
        DataSourceID="dsMain" 
        KeyFieldName="customerdealerid" 
        Styles-HeaderPanel-Wrap="True"
        visible="true" 
        width="100%">

        <SettingsPager 
            Mode="ShowPager"
            PageSize="16" 
            ShowDefaultImages="True" 
            AllButton-Visible="true"/>
		
        <Settings 
            ShowFilterRowMenu="False" 
            ShowHeaderFilterBlankItems="false"
            ShowFooter="True" 
            ShowGroupedColumns="False"
            ShowGroupFooter="Hidden" 
            ShowGroupPanel="False" 
            ShowHeaderFilterButton="False"
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False" />
    
        <Styles>
            <Footer HorizontalAlign="Center"></Footer>
        </Styles>

        <SettingsBehavior 
            AllowSort="True" 
            AutoFilterRowInputDelay="12000" 
            ColumnResizeMode="Control" />

        <Columns>

              	<dx:GridViewDataTextColumn 
					FieldName="zone" 
                    Caption="Zone"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
					HeaderStyle-HorizontalAlign ="Center"
					CellStyle-HorizontalAlign ="Center"
					VisibleIndex="0"
					Visible ="true" 
					ExportWidth = "100"
					Width="6%">
				</dx:GridViewDataTextColumn>

              	<dx:GridViewDataTextColumn 
					FieldName="dealercode" 
                    Caption="Dealer"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
					HeaderStyle-HorizontalAlign ="Center"
					CellStyle-HorizontalAlign ="Center"
					VisibleIndex="1"
					Visible ="true" 
					ExportWidth = "100"
					Width="10%">
				</dx:GridViewDataTextColumn>

    			<dx:GridViewDataTextColumn 
					Caption="Dealer Name" 
					ExportWidth = "150"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
		    		HeaderStyle-HorizontalAlign ="Left"
	    			CellStyle-HorizontalAlign ="Left"
					FieldName="dealername" 
					VisibleIndex="2"
					Visible ="true" 
					Width="15%">
	    		</dx:GridViewDataTextColumn>
	
    			<dx:GridViewDataTextColumn 
					Caption="A/C Code" 
					ExportWidth = "150"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="accountcode" 
					VisibleIndex="3"
					Visible ="true" 
					Width="10%">
	    		</dx:GridViewDataTextColumn>

            	<dx:GridViewDataTextColumn 
					Caption="Customer Name" 
					ExportWidth = "200"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
		    		HeaderStyle-HorizontalAlign ="Left"
	    			CellStyle-HorizontalAlign ="Left"
					FieldName="businessname" 
					VisibleIndex="4"
					Visible ="true" 
					Width="20%">
	    		</dx:GridViewDataTextColumn>
	
            	<dx:GridViewDataTextColumn 
					Caption="Business Type" 
					ExportWidth = "200"
					Settings-AllowHeaderFilter="True"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="businesstype" 
					VisibleIndex="5"
					Visible ="true" 
					Width="15%">
	    		</dx:GridViewDataTextColumn>

            	<dx:GridViewDataTextColumn 
					Caption="Invoiced Value" 
					ExportWidth = "150"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="nettsales" 
					VisibleIndex="6"
					Visible ="true" 
					Width="9%">
					<PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
	    		</dx:GridViewDataTextColumn>

            	<dx:GridViewDataTextColumn 
					Caption="Cost To Dealer" 
					ExportWidth = "150"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="costsales" 
					VisibleIndex="6"
					Visible ="true" 
					Width="9%">
					<PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Profit/Loss" 
					ExportWidth = "150"
                    Tooltip="Calculated as the difference between the invoiced value and the cost to the dealer"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="loss" 
					VisibleIndex="6"
					Visible ="true" 
					Width="9%">
					<PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
	    		</dx:GridViewDataTextColumn>

       </Columns>
    
        <TotalSummary>
            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="nettsales" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costsales" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="loss" SummaryType="Sum" />
       </TotalSummary>
    
        <SettingsDetail 
            AllowOnlyOneMasterRowExpanded="True" 
            ShowDetailRow="True" />

         <Templates>
             <DetailRow>
                <dx:ASPxGridView 
                        ID="gridTransactions" 
                        runat="server" 
                        OnLoad="GridStyles2"
                        SettingsDetail-IsDetailGrid="true"
                        ClientInstanceName="gridTransactions" 
                        AutoGenerateColumns="False"
                        OnBeforePerformDataSelect="gridTransactions_BeforePerformDataSelect"
                        DataSourceID="dsTransactions" 
                        Font-Bold="False" 
                        Width="100%"
                        Font-Strikeout="False"> 
         
                        <SettingsBehavior 
                            AllowSort="False" 
                            ColumnResizeMode="Control"  />

                        <Settings 
                            ShowTitlePanel="False"
                            ShowFilterRow="False"
                            ShowFilterRowMenu="False"
                            ShowFooter="True"
                            ShowHeaderFilterButton="False"/>
          
                        <SettingsPager 
                            PageSize= "16"
                            AllButton-Visible="true" 
                            AlwaysShowPager="true" 
                            ShowDefaultImages="true">
                        </SettingsPager>

                        <Styles>
                           <Footer HorizontalAlign="Center"></Footer>
                      </Styles>
         
                        <Columns>

                          <dx:GridViewDataTextColumn 
                                    Caption="Year" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
		                            CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="xyear"
                                    VisibleIndex="0" 
                                    Width="9%">
                            </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn 
                                    Caption="Month" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="xmonth"
                                    VisibleIndex="1" 
                                    exportwidth ="150"
                                    Width="9%">
                          </dx:GridViewDataTextColumn>

                          <dx:GridViewDataDateColumn
                                    Caption="Invoice Date" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="invoicedate"
                                    VisibleIndex="2" 
                                    exportwidth ="150"
                                    Width="9%">
                          </dx:GridViewDataDateColumn> 

                          <dx:GridViewDataTextColumn 
                                    Caption="Invoice" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="invoicenumber"
                                    VisibleIndex="3" 
                                    exportwidth ="150"
                                    Width="9%">
                            </dx:GridViewDataTextColumn>
             
                          <dx:GridViewDataTextColumn 
                                    Caption="Product Group" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="level2_code"
                                    VisibleIndex="4" 
                                    exportwidth ="150"
                                    Width="9%">
                            </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn 
                                    Caption="Part Number" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="partnumber"
                                    exportwidth ="150"
                                    VisibleIndex="4" 
                                    Width="9%">
                            </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn 
                                    Caption="Description" 
                                  	Settings-AllowHeaderFilter="True"
			                		Settings-HeaderFilterMode="CheckedList"
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="partdescription"
                                    exportwidth ="150"
                                    VisibleIndex="4" 
                                    Width="9%">
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                                    Caption="Units Sold" 
                                    CellStyle-HorizontalAlign="Center"
                                    HeaderStyle-HorizontalAlign="Center"
                                    FieldName="units"
                                    exportwidth ="150"
                                    VisibleIndex="5" 
                                    Width="9%">
                                    <PropertiesTextEdit DisplayFormatString="##,##0"/>
                            </dx:GridViewDataTextColumn>

                          <dx:GridViewDataTextColumn 
				    	Caption="Invoiced Value" 
						Settings-AllowHeaderFilter="False"
	    	    		HeaderStyle-HorizontalAlign ="Center"
	        			CellStyle-HorizontalAlign ="Center"
			    		FieldName="nettsales" 
                        exportwidth ="150"
				    	VisibleIndex="6"
					    Visible ="true" 
    					Width="9%">
	    				<PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
	    		</dx:GridViewDataTextColumn>

               	          <dx:GridViewDataTextColumn 
		        		Caption="Cost To Dealer" 
        			    Settings-AllowHeaderFilter="False"
		            	HeaderStyle-HorizontalAlign ="Center"
        	    		CellStyle-HorizontalAlign ="Center"
                        exportwidth ="150"
				        FieldName="costsales" 
		        		VisibleIndex="7"
        				Visible ="true" 
			        	Width="9%">
		    			<PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
        		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
		    			Caption="Profit/Loss" 
                    	exportwidth ="150"
                        Tooltip="Calculated as the difference between the invoiced value and the cost to the dealer"
					    Settings-AllowHeaderFilter="False"
    		    		HeaderStyle-HorizontalAlign ="Center"
	        			CellStyle-HorizontalAlign ="Center"
		    			FieldName="loss" 
                        VisibleIndex="9"
				    	Visible ="true" 
					    Width="9%">
					    <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
	    		</dx:GridViewDataTextColumn>

                        </Columns>
         
                        <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="units" SummaryType="Sum"/>
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="nettsales" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costsales" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="loss" SummaryType="Sum" />
                       </TotalSummary>

                  </dx:ASPxGridView>
             </DetailRow>
        </Templates>


   </dx:ASPxGridView>


</div>

  <asp:SqlDataSource 
        ID="dsMain" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_CentralSupportAccounts_Summary" 
        SelectCommandType="StoredProcedure">
       <SelectParameters>
             <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
             <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nfrom" SessionField="FromMonth" Type="Int32" />
            <asp:SessionParameter Name="nto" SessionField="ToMonth" Type="Int32" />
         </SelectParameters>
    </asp:SqlDataSource>
    
  <asp:SqlDataSource 
        ID="dsTransactions" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_CentralSupportAccounts_Detail" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
             <asp:SessionParameter DefaultValue="" Name="ncustomerdealerid" SessionField="thiscustomerdealerid" Type="String"  />
             <asp:SessionParameter Name="nfrom" SessionField="FromMonth" Type="Int32" />
             <asp:SessionParameter Name="nto"  SessionField   ="ToMonth" Type="Int32" />
       </SelectParameters>
    </asp:SqlDataSource>
 
  <dx:ASPxGridViewExporter 
         ID="ASPxGridViewExporter1" 
          runat="server" 
          GridViewID="gridMain" >
    </dx:ASPxGridViewExporter>

  <asp:SqlDataSource 
      ID="sqlPopMonth" 
      runat="server" 
      ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
      SelectCommand="p_Get_Months" 
      SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

  
</asp:Content>


