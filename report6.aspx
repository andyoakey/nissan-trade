﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report6.aspx.vb" Inherits="report6" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"   Namespace="DevExpress.Web" TagPrefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        
    <dxp:ASPxPopupControl ID="PopupZoomMap" Font-Size="13px" Font-Names="Calibri,Verdana"
        runat="server" ShowOnPageLoad="False" ClientInstanceName="PopupZoomMap" 
        Modal="True" CloseAction="CloseButton"  AllowDragging="True" Width="910px" Height="670px"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ShowHeader="True" HeaderText="Mapping">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol4" runat="server">
                <asp:Image ID="imgZoom" runat="server" ImageUrl="" Width="890px" Height="610px"  BorderStyle="Solid"  BorderWidth="1px"
                    Style="left: 10px; top: 40px; position: absolute" />
            </dxp:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dxp:ASPxPopupControl>

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width:976px; margin: 0 auto; text-align: left;" >
            
        <dxe:ASPxLabel Font-Size="16px" Font-Names="Calibri,Verdana" ID="lblPageTitle" Text="Mapping"
            Style="left: 5px; position: absolute; top: 5px; font-weight: 700; font-size: medium; z-index: 1;" runat="server">
        </dxe:ASPxLabel>
            
        <dxe:ASPxLabel Font-Size="18px" Font-Names="Calibri,Verdana" ID="lblNoDataMessage" Text="There is no mapping data for this Centre." Font-Bold="True"
            Style="left: 0px; position: absolute; top: 5px; z-index: 1;" runat="server" Visible="false">
        </dxe:ASPxLabel>

        <asp:DropDownList ID="ddlParcChoice" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" Visible="false"
            AutoPostBack="True" Width="150px" Style="left: 820px; position: absolute; top: 5px; z-index: 5">
            <asp:ListItem Text="Total" Value="1" Selected="true" />
            <asp:ListItem Text="8 Year" Value="0"  />
        </asp:DropDownList>

        <asp:DropDownList ID="ddlSalesChoice" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" Visible="false"
            AutoPostBack="True" Width="150px" Style="left: 630px; position: absolute; top: 5px; z-index: 5">
            <asp:ListItem Text="Total Sales" Value="0"  Selected="true" />
            <asp:ListItem Text="Mechanical" Value="1" />
        </asp:DropDownList>

        <asp:DropDownList ID="ddlBusinessTypeChoice" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" Visible="false"
            AutoPostBack="True" Width="180px" Style="left: 790px; position: absolute; top: 5px; z-index: 5">
            <asp:ListItem Text="All" Value="100"  Selected="true" />
            <asp:ListItem Text="Applicable" Value="101" />
            <asp:ListItem Text="Bodyshop" Value="1" />
            <asp:ListItem Text="Breakdown Recovery Services" Value="2" />
            <asp:ListItem Text="Car Dealer New/Used" Value="3" />
            <asp:ListItem Text="Independent Motor Trader" Value="4" />
            <asp:ListItem Text="Mechanical Specialist" Value="5" />
            <asp:ListItem Text="Motor Factor" Value="6" />
            <asp:ListItem Text="Parts Supplier" Value="7" />
            <asp:ListItem Text="Taxi & Private Hire" Value="8" />
            <asp:ListItem Text="Windscreens" Value="9" />
            <asp:ListItem Text="Other" Value="99" />
        </asp:DropDownList>

        <asp:DropDownList ID="ddlCustomerChoice" Font-Size="13px" Font-Names="Calibri,Verdana" runat="server" Visible="false"
            AutoPostBack="True" Width="180px" Style="left: 790px; position: absolute; top: 5px; z-index: 5">
            <asp:ListItem Text="Active" Value="2"  Selected="true" />
            <asp:ListItem Text="In-Active" Value="3" />
            <asp:ListItem Text="Known" Value="1" />
            <asp:ListItem Text="Lapsed" Value="4" />
            <asp:ListItem Text="Prospects" Value="5" />
            <asp:ListItem Text="New Prospects" Value="6" />
            <asp:ListItem Text="Summary" Value="7" />
        </asp:DropDownList>

        <%-- Icon Buttons --%>
        <dxe:ASPxButton ID="btn1" runat="server" Style="left: 0px; position: absolute; top: 40px; z-index: 2;" EnableDefaultAppearance="false" 
            Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/TerritoryCarParc.png"
            Image-UrlHottracked="~/Images2014/TerritoryCarParc_Hover.png">
        </dxe:ASPxButton>
        <dxe:ASPxButton ID="btn2" runat="server" Style="left: 0px; position: absolute; top: 160px;
            z-index: 2;"  EnableDefaultAppearance="false"  Text="" CssClass ="special" HorizontalPosition="True"
            Border-BorderStyle="None" Image-Url="~/Images2014/TradeSalesByPostcodeDistrict.png" Image-UrlHottracked="~/Images2014/TradeSalesByPostcodeDistrict-Hover.png">
        </dxe:ASPxButton>
       <dxe:ASPxButton ID="btn3" runat="server" Style="left: 0px; position: absolute; top: 280px;
            z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
            Border-BorderStyle="None" Image-Url="~/Images2014/DatabaseCustomerMapping1.png" Image-UrlHottracked="~/Images2014/DatabaseCustomerMapping1_Hover.png">
        </dxe:ASPxButton>
       
        <dxwgv:ASPxGridView Font-Size="12px" Font-Names="Calibri,Verdana" ID="gridTerritory" 
            runat="server" Settings-ShowHeaderFilterBlankItems="false" 
            Style="left: 135px; position: absolute; top: 40px" 
            Width="275px" AutoGenerateColumns="False" 
            EnableTheming="True">

            <Settings ShowFilterRowMenu="False" ShowGroupedColumns="False" 
                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="False" ShowFooter="True" UseFixedTableLayout="true" VerticalScrollableHeight="375" />

            <SettingsPager PageSize="16" Mode="ShowAllRecords" AllButton-Visible="true"  >
                <AllButton Visible="True"></AllButton>
            </SettingsPager>

            <Columns>
        
                <dxwgv:GridViewDataTextColumn Caption="Postcode District" FieldName="PostCode" VisibleIndex="0" ExportWidth="100">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False"
                        HeaderFilterMode="CheckedList" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                    <DataItemTemplate>
                        <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="PostCode_Init" Font-Size="X-Small"  Font-Underline="true">
                        </dxe:ASPxHyperLink>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>
            
                <dxwgv:GridViewDataTextColumn Caption="Total Parc" FieldName="ParcTotal" VisibleIndex="1"
                    ExportWidth="200" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False"
                        HeaderFilterMode="CheckedList" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="8Yr Parc" FieldName="Parc8Year" VisibleIndex="2"
                    ExportWidth="200" Visible="false">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False"
                        HeaderFilterMode="CheckedList" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                  
                <dxwgv:GridViewDataTextColumn Caption="Total Trade Sales" FieldName="TradeSalesTotal" VisibleIndex="3" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Applicable Trade Sales" FieldName="TradeSalesApplic" VisibleIndex="4" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales1" VisibleIndex="5" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales2" VisibleIndex="6" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales3" VisibleIndex="7" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales4" VisibleIndex="8" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales5" VisibleIndex="9" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales6" VisibleIndex="10" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales7" VisibleIndex="11" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales8" VisibleIndex="12" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales9" VisibleIndex="13" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Trade Sales" FieldName="TradeSales99" VisibleIndex="14" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Total M/C Sales" FieldName="MCSalesTotal" VisibleIndex="15" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Applicable M/C Sales" FieldName="MCSalesApplic" VisibleIndex="16" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales1" VisibleIndex="17" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales2" VisibleIndex="18" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales3" VisibleIndex="19" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales4" VisibleIndex="20" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales5" VisibleIndex="21" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales6" VisibleIndex="22" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales7" VisibleIndex="23" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales8" VisibleIndex="24" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales9" VisibleIndex="25" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="M/C Sales" FieldName="MCSales99" VisibleIndex="26" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Known Customers" FieldName="CustomersKnown" VisibleIndex="27" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Active Customers" FieldName="CustomersActive" VisibleIndex="28" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="In-Active Customers" FieldName="CustomersInActive" VisibleIndex="29" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Lapsed Customers" FieldName="CustomersLapsed" VisibleIndex="30" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Prospects" FieldName="CustomersProspect" VisibleIndex="31" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="New Prospects" FieldName="CustomersNewProspects" VisibleIndex="32" ExportWidth="200"  Visible="false">
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <%-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------%>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSalesTotalPerc" VisibleIndex="33" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSalesApplicPerc" VisibleIndex="34" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales1Perc" VisibleIndex="35" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales2Perc" VisibleIndex="36" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales3Perc" VisibleIndex="37" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales4Perc" VisibleIndex="38" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales5Perc" VisibleIndex="39" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales6Perc" VisibleIndex="40" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales7Perc" VisibleIndex="41" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales8Perc" VisibleIndex="42" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales9Perc" VisibleIndex="43" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="TradeSales99Perc" VisibleIndex="44" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSalesTotalPerc" VisibleIndex="45" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSalesApplicPerc" VisibleIndex="46" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales1Perc" VisibleIndex="47" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales2Perc" VisibleIndex="48" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales3Perc" VisibleIndex="49" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales4Perc" VisibleIndex="50" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales5Perc" VisibleIndex="51" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales6Perc" VisibleIndex="52" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales7Perc" VisibleIndex="53" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales8Perc" VisibleIndex="54" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales9Perc" VisibleIndex="55" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="% Sales" FieldName="MCSales99Perc" VisibleIndex="56" ExportWidth="200" Visible="false">
                    <PropertiesTextEdit DisplayFormatString="0.0%">
                    </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <FooterCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowInFilterControl="False" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
            </Columns>
        
            <TotalSummary>

                <dxwgv:ASPxSummaryItem FieldName="ParcTotal" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="ParcTotal" />
                <dxwgv:ASPxSummaryItem FieldName="Parc8Year" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="Parc8Year" />
    
                <dxwgv:ASPxSummaryItem FieldName="TradeSalesTotal" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSalesTotal" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSalesApplic" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSalesApplic" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales1" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales1" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales2" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales2" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales3" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales3" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales4" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales4" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales5" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales5" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales6" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales6" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales7" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales7" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales8" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales8" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales9" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales9" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales99" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="TradeSales99" />

                <dxwgv:ASPxSummaryItem FieldName="MCSalesTotal" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSalesTotal" />
                <dxwgv:ASPxSummaryItem FieldName="MCSalesApplic" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSalesApplic" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales1" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales1" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales2" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales2" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales3" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales3" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales4" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales4" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales5" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales5" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales6" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales6" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales7" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales7" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales8" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales8" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales9" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales9" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales99" DisplayFormat="&#163;#,##0" SummaryType="Sum" ShowInColumn="MCSales99" />

                <dxwgv:ASPxSummaryItem FieldName="CustomersKnown" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersKnown" />
                <dxwgv:ASPxSummaryItem FieldName="CustomersActive" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersActive" />
                <dxwgv:ASPxSummaryItem FieldName="CustomersInActive" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersInActive" />
                <dxwgv:ASPxSummaryItem FieldName="CustomersLapsed" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersLapsed" />
                <dxwgv:ASPxSummaryItem FieldName="CustomersProspect" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersProspect" />
                <dxwgv:ASPxSummaryItem FieldName="CustomersNewProspects" DisplayFormat="#,##0" SummaryType="Sum" ShowInColumn="CustomersNewProspects" />
 
                <dxwgv:ASPxSummaryItem FieldName="TradeSalesTotalPerc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSalesTotalPerc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSalesApplicPerc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSalesApplicPerc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales1Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales1Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales2Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales2Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales3Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales3Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales4Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales4Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales5Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales5Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales6Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales6Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales7Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales7Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales8Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales8Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales9Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales9Perc" />
                <dxwgv:ASPxSummaryItem FieldName="TradeSales99Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="TradeSales99Perc" />

                <dxwgv:ASPxSummaryItem FieldName="MCSalesTotalPerc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSalesTotalPerc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSalesApplicPerc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSalesApplicPerc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales1Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales1Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales2Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales2Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales3Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales3Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales4Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales4Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales5Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales5Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales6Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales6Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales7Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales7Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales8Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales8Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales9Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales9Perc" />
                <dxwgv:ASPxSummaryItem FieldName="MCSales99Perc" DisplayFormat="#,##0.0%" SummaryType="Sum" ShowInColumn="MCSales99Perc" />

            </TotalSummary>

        </dxwgv:ASPxGridView>

        <asp:ImageButton ID="imgMap" runat="server"  ImageUrl="" Width="690px" Height="410px"  BorderStyle="Solid"  BorderWidth="1px"
           Style="left: 280px; top: 42px; position: absolute"  ToolTip="Click image to enlarge"/>

        <dxe:ASPxLabel Font-Size="18px" Font-Bold="True" Font-Names="Calibri,Verdana" ID="lblFileNotFound" Text="This map file cannot be found."
            Style="left: 280px; top: 42px; position: absolute" runat="server" Visible="false">
        </dxe:ASPxLabel>

        <dxe:ASPxLabel Font-Size="11px" Font-Names="Calibri,Verdana" ID="lblImageEnlarge" Text="Click map to zoom in" 
            Style="left: 280px; position: absolute; top: 462px; z-index: 1;" runat="server">
        </dxe:ASPxLabel>
        
        <dxe:ASPxButton ID="btnSaveImage" runat="server" Font-Size="12px" Font-Names="Calibri,Verdana"
            Text="Download Map" Width="100px" Style="left: 871px; position: absolute; top: 462px" CssClass="indbutton">
        </dxe:ASPxButton>

        <dxchartsui:WebChartControl ID="chartCustomerSummary" runat="server" Style="left: 478px; position: absolute; top: 42px; z-index: 4" 
            CrosshairEnabled="True" Height="400px" PaletteName="Toyota" Width="490px">
            <palettewrappers>
                <dxchartsui:PaletteWrapper Name="Toyota" ScaleMode="Repeat">
                    <palette>
                        <dxcharts:PaletteEntry Color="72, 127, 20" Color2="72, 127, 20" />
                        <dxcharts:PaletteEntry Color="155, 31, 90" Color2="155, 31, 90" />
                        <dxcharts:PaletteEntry Color="217, 165, 160" Color2="217, 165, 160" />
                        <dxcharts:PaletteEntry Color="166, 40, 28" Color2="166, 40, 28" />
                        <dxcharts:PaletteEntry Color="143, 166, 28" Color2="143, 166, 28" />
                    </palette>
                </dxchartsui:PaletteWrapper>
            </palettewrappers>
        </dxchartsui:WebChartControl>

    </div>

</asp:Content>

