﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Download
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Me.Title = "Toyota PARTS website"

        If Request.QueryString.Count > 0 Then
            If Request.QueryString("t") <> Nothing Then
                Select Case Request.QueryString("t")
                    Case "mtm"
                        TradeMeetingPdf()
                    Case Else
                        Response.Redirect("HomePage.aspx")
                End Select
            Else
                Response.Redirect("HomePage.aspx")
            End If
        Else
            Response.Redirect("HomePage.aspx")
        End If

    End Sub

    Private Sub TradeMeetingPdf()
        If Request.QueryString("cid") <> Nothing And Request.QueryString("mo") <> Nothing Then
            ' Variables are set, don't know if they are valid, but what the hell...

            Dim cid As String = Request.QueryString("cid")
            Dim mo As Integer

            If (Integer.TryParse(cid, vbNull) And Integer.TryParse(Request.QueryString("mo"), vbNull)) Then
                ' Valid
                mo = Integer.Parse(Request.QueryString("mo"))

                'Try

                Using conn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString)
                    Dim comm As SqlCommand = New SqlCommand("p_dimtpbr_dlf", conn)
                    Dim d As Date = New DateTime(Date.Now.Year, mo, 1)

                    comm.CommandType = CommandType.StoredProcedure
                    comm.Parameters.AddWithValue("@sSelectionLevel", Session("SelectionLevel"))
                    comm.Parameters.AddWithValue("@sSelectionId", Session("SelectionId"))
                    comm.Parameters.AddWithValue("@sDealerCode", cid)
                    comm.Parameters.AddWithValue("@nMonth", mo)

                    Response.Buffer = False
                    Response.Clear()
                    Response.ClearHeaders()
                    Response.ContentType = "application/pdf"
                    'Response.AddHeader("content-disposition", String.Format("attachment; filename=TradeMeeting_{0}_{1}_{2}.pdf", cid, d.Year, d.ToString("MMMM")))

                    ' Open reader
                    conn.Open()

                    Using sdr As SqlDataReader = comm.ExecuteReader()
                        While sdr.Read()
                            sdr.GetStream(0).CopyTo(Response.OutputStream)
                        End While
                    End Using

                    Response.Flush()

                End Using

                'Catch
                ' Response.Redirect("HomePage.aspx")
                'End Try

                ' Close the page
                Response.End()

            Else
                Response.Redirect("HomePage.aspx")
            End If
        End If
    End Sub

End Class