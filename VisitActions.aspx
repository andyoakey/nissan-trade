﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VisitActions.aspx.vb" Inherits="VisitActions" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
        function VisitActionActivityNew(contentUrl) {
            PopupVisitLine.SetContentUrl(contentUrl);
            PopupVisitLine.SetHeaderText('Visit Action Activity');
            PopupVisitLine.SetSize(712, 455);
            PopupVisitLine.Show();
        }
        function ActivityWindow(contentUrl) {
            PopupVisitLine.SetContentUrl(contentUrl);
            PopupVisitLine.SetHeaderText('Click the Remove button to continue');
            PopupVisitLine.SetSize(230, 90);
            PopupVisitLine.Show();
        }
        function HideVisitActionActivityWindow() {
            PopupVisitLine.Hide();
            gridVAR.Refresh();
        }
        function VisitActionDate(contentUrl) {
            PopupDateEntry.SetContentUrl(contentUrl);
            PopupDateEntry.SetHeaderText('Date');
            PopupDateEntry.SetSize(300, 335);
            PopupDateEntry.Show();
        }
        function HideVisitActionDateWindow() {
            PopupDateEntry.Hide();
            gridVAR.Refresh();
        }
    </script>

    <dxp:ASPxPopupControl ID="PopupVisitLine" ClientInstanceName="PopupVisitLine" Width="275px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="New Visit Action - Select Centre and Date"
        Modal="True" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
        <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">
            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <dxp:ASPxPopupControl ID="PopupDateEntry" ClientInstanceName="PopupDateEntry" Width="300px"
        runat="server" ShowOnPageLoad="False" ShowHeader="True" HeaderText="New Visit Action - Select Centre and Date"
        Modal="False" CloseAction="CloseButton" PopupHorizontalAlign="WindowCenter"  PopupVerticalAlign="WindowCenter">
            <HeaderStyle Font-Size="14px" Font-Bold="True"/>
        <ContentCollection>
            <dxp:PopupControlContentControl ID="Popupcontrolcontentcontrol3" runat="server">
            </dxp:PopupControlContentControl>
        </ContentCollection>
    </dxp:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px; padding-bottom:10px;">
            <table width="100%">
                <tr>
                    <td align="left" style="width:85%">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Visit Actions" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" >
                        <dxe:ASPxButton ID="btnNewVisit" ClientInstanceName="btnNewVisit" runat="server" Text="New Visit" Width="100px" Style="padding-top:5px; margin-left:10px">
                        </dxe:ASPxButton>
                    </td>
                    <td align="right" >
                        <dxe:ASPxButton ID="btnMeetings" ClientInstanceName="btnMeetings" runat="server" Text="Meetings..." Width="100px" Style="padding-top:5px; margin-left:10px">
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>

        <dxwgv:ASPxGridView ID="gridVAR" ClientInstanceName="gridVAR" runat="server" width="100%" AutoGenerateColumns="False" KeyFieldName="Id"  DataSourceID="dsVAR" style="position:relative; left:5px;">
       
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center" />
                <Cell Font-Size="Small" HorizontalAlign="Center"/>
                <LoadingPanel ImageSpacing="10px" />
            </Styles>
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded" 
                UseFixedTableLayout="True" VerticalScrollableHeight="300" 
                ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False" 
                ShowFilterRowMenu="True" ShowHeaderFilterBlankItems="false"  />
                
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False"  />
            
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            
            <Images>
                <CollapsedButton Height="12px" Width="11px" />
                <ExpandedButton Height="12px" Width="11px" />
                <DetailCollapsedButton Height="12px" Width="11px" />
                <DetailExpandedButton Height="12px" Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Id" FieldName="Id" VisibleIndex="0" Visible="false" ExportWidth="0">
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="1" Width="10%" ExportWidth="100">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True" ShowFilterRowMenu="False" HeaderFilterMode="CheckedList" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="2" ExportWidth="200">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True" ShowFilterRowMenu="False" HeaderFilterMode="CheckedList" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Zone" FieldName="Zone" VisibleIndex="3" Width="15%" ExportWidth="100">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True" ShowFilterRowMenu="False" HeaderFilterMode="CheckedList" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataDateColumn Caption="Visit Date" FieldName="VisitDate" VisibleIndex="4" Width="15%" ExportWidth="100">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True" ShowFilterRowMenu="False" />
                </dxwgv:GridViewDataDateColumn>
    
                <dxwgv:GridViewDataTextColumn Caption="Status" FieldName="Status" VisibleIndex="5" Width="15%" ExportWidth="150">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True" ShowFilterRowMenu="False" HeaderFilterMode="List" FilterMode="DisplayText" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataHyperLinkColumn Caption="Add Activity" FieldName="Id" VisibleIndex="6" Width="10%" ExportWidth="0">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" ShowFilterRowMenu="False" />
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="VisitActionActivityUpd.aspx?Type=0&Id={0}" Target="_self"  Text="" ImageUrl = "~/Images2014/vcard_edit.png">
                        <Style Font-Size="X-Small" HorizontalAlign="Center">
                        </Style>
                    </PropertiesHyperLinkEdit>
                </dxwgv:GridViewDataHyperLinkColumn>

                <dxwgv:GridViewDataTextColumn Caption="Remove" FieldName="Id" Width="10%"
                    ExportWidth="0" EditFormSettings-Visible="False" VisibleIndex="7" UnboundType="String">
                    <DataItemTemplate>
                        <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="RemoveVAR_Init">
                        </dxe:ASPxHyperLink>
                    </DataItemTemplate>
                </dxwgv:GridViewDataTextColumn>        

            </Columns>

            <Templates>
            
                <DetailRow>

                      <dxwgv:ASPxGridView ID="gridVARDetail" runat="server" 
                        OnBeforePerformDataSelect="gridVARDetail_BeforePerformDataSelect"
                        OnCustomColumnDisplayText="gridVARDetail_CustomColumnDisplayText"
                        OnHtmlDataCellPrepared="gridVARDetail_HtmlDataCellPrepared"
                        AutoGenerateColumns="False" DataSourceID="dsVARDetail" Width="921px">

                        <Styles>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center" />
                            <Cell Font-Size="X-Small" HorizontalAlign="Center" BackColor="ghostwhite" Wrap="True" />
                            <LoadingPanel ImageSpacing="10px" />
                        </Styles>
       
                        <SettingsPager PageSize="120" Mode="ShowAllRecords">
                        </SettingsPager>
            
                        <Settings
                            ShowFooter="False" ShowHeaderFilterButton="False"
                            ShowStatusBar="Hidden" ShowTitlePanel="True"
                            ShowGroupFooter="VisibleIfExpanded" 
                            UseFixedTableLayout="True" VerticalScrollableHeight="300" 
                            ShowVerticalScrollBar="False" ShowGroupPanel="false" ShowFilterBar="Hidden" ShowFilterRow="False" ShowFilterRowMenu="False" />
                
                        <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                            ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
                        <Images>
                            <CollapsedButton Height="12px" Width="11px" />
                            <ExpandedButton Height="12px" Width="11px" />
                            <DetailCollapsedButton Height="12px" Width="11px" />
                            <DetailExpandedButton Height="12px" Width="11px" />
                            <FilterRowButton Height="13px" Width="13px" />
                        </Images>

                        <Columns>
                
                            <dxwgv:GridViewDataHyperLinkColumn Caption="Update" FieldName="Id" VisibleIndex="0" Width="7%">
                                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" ShowFilterRowMenu="False" />
                                <PropertiesHyperLinkEdit NavigateUrlFormatString="VisitActionActivityUpd.aspx?Type=1&Id={0}" Target="_self"  Text="" ImageUrl = "~/Images2014/vcard_edit.png">
                                    <Style Font-Size="X-Small" HorizontalAlign="Center">
                                    </Style>
                                </PropertiesHyperLinkEdit>
                            </dxwgv:GridViewDataHyperLinkColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Activity" FieldName="Activity" VisibleIndex="1" ExportWidth="150">
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Comments" FieldName="Comments" VisibleIndex="2" Width="35%" ExportWidth="250">
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Responsibility" FieldName="Responsibility" VisibleIndex="3" ExportWidth="100">
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataDateColumn Caption="Target Date" FieldName="TargetDate" VisibleIndex="4" ExportWidth="100">
                            </dxwgv:GridViewDataDateColumn>
    
                            <dxwgv:GridViewDataTextColumn Caption="Completion Date" FieldName="CompletionDate" ExportWidth="100" EditFormSettings-Visible="False" VisibleIndex="5" UnboundType="String">
                                <DataItemTemplate>
                                    <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="CompletionDate_Init" Font-Size="X-Small"  Font-Underline="true">
                                    </dxe:ASPxHyperLink>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>        

                            <dxwgv:GridViewDataTextColumn Caption="Confirmed Date" FieldName="ConfirmedDate" ExportWidth="100" EditFormSettings-Visible="False" VisibleIndex="6" UnboundType="String">
                                <DataItemTemplate>
                                    <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ConfirmedDate_Init" Font-Size="X-Small" Font-Underline="true" >
                                    </dxe:ASPxHyperLink>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>        

                            <dxwgv:GridViewDataTextColumn Caption="Status" FieldName="DetailStatus" Name="DetailStatus" VisibleIndex="7" ExportWidth="150">
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Remove" FieldName="Id" ExportWidth="0" EditFormSettings-Visible="False" VisibleIndex="8" Width="7%" UnboundType="String">
                                <DataItemTemplate>
                                    <dxe:ASPxHyperLink ID="hyperLink" runat="server" OnInit="RemoveActivity_Init">
                                    </dxe:ASPxHyperLink>
                                </DataItemTemplate>
                            </dxwgv:GridViewDataTextColumn>        

                        </Columns>

                        <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />
                        
                    </dxwgv:ASPxGridView>
                
                </DetailRow>
                
            </Templates>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailRow="True" ShowDetailButtons="True" ExportMode="Expanded" AllowOnlyOneMasterRowExpanded="False"  />

        </dxwgv:ASPxGridView>
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsVAR" runat="server" SelectCommand="p_VAR_Headers2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="VARYear" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsVarDetail" runat="server" SelectCommand="p_VAR_Detail_New" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nVARId" SessionField="VARId" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridVAR" PreserveGroupRowStates="True">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>










