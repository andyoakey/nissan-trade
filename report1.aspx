﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report1.aspx.vb" Inherits="report1" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
        <table style="width: 100%;">
            <tr>
                <td width="350px">
                    <dx:ASPxLabel 
                        ID="lblPageTitle" 
                        Font-Size="Large"
                        Text="Customers"
                        runat="server">
                    </dx:ASPxLabel>
                </td>
                <td style="width: calc(100% - 350px);">
                </td>


            </tr>
            <tr>
                <td width="350px">
                    <%-- Icon Buttons --%>
                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn1" runat="server" Style="left: 0px; position: absolute; top: 30px;
                        z-index: 2;"  EnableDefaultAppearance="false"  Text="" CssClass ="special" HorizontalPosition="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/CustomerRelationshipManager.png" Image-UrlHottracked="~/Images2014/CustomerRelationshipManager_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn2" runat="server" Style="left: 125px; position: absolute;
                        top: 30px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/ExcludedAccounts.png"
                        Image-UrlHottracked="~/Images2014/ExcludedAccounts-Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn3" runat="server" Style="left: 0px; position: absolute; top: 150px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/CustomerSalesAnalysis.png"
                        Image-UrlHottracked="~/Images2014/CustomerSalesAnalysis_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn4" runat="server" Style="left: 125px; position: absolute;
                        top: 150px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/HighestValueCustomers.png"
                        Image-UrlHottracked="~/Images2014/HighestValueCustomers-Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn5" runat="server" Style="left: 0px; position: absolute;
                        top: 270px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/NewCustomers.png"
                        Image-UrlHottracked="~/Images2014/NewCustomers_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn6" runat="server" Style="left: 125px; position: absolute; top: 270px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/YOYCustomerPerformance.png" Image-UrlHottracked="~/Images2014/YOYCustomerPerformance_Hover.png">
                    </dx:ASPxButton>

                    <%--<dx:ASPxButton 
                        BackColor="White"
                        ID="btn7" runat="server" Style="left: 0px; position: absolute;
                        top: 400px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/ContactHistoryTracker.png"
                        Image-UrlHottracked="~/Images2014/ContactHistoryTracker_Hover.png">
                    </dx:ASPxButton>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn8" runat="server" Style="left: 125px; position: absolute; top: 400px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/CustomerLoyalty.png" Image-UrlHottracked="~/Images2014/CustomerLoyalty_Hover.png">
                    </dx:ASPxButton>--%>

                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn9" runat="server" Style="left: 0px; position: absolute; top: 390px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/EmailSummary.png" Image-UrlHottracked="~/Images2014/EmailSummary_Hover.png">
                    </dx:ASPxButton>
                    
                    <dx:ASPxButton 
                        BackColor="White"
                        ID="btn10" runat="server" Style="left: 125px; position: absolute; top: 390px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True"
                        Border-BorderStyle="None" Image-Url="~/Images2014/CentrallySupportedSales.png" Image-UrlHottracked="~/Images2014/CentrallySupportedSales_Hover.png">
                    </dx:ASPxButton>
                </td>
        
                <td style="width: calc(100% - 260px);">
                
                    <table width="100%">
                        <tr>
                            <td width="48%">
                                <dxchartsui:WebChartControl ID="chartCustomerSummary1" runat="server" 
                                    CrosshairEnabled="True" Height="450px" PaletteName="Toyota" Width="450px">
                                    <diagramserializable>
                                        <dxcharts:XYDiagram>
                                            <axisx visibleinpanesserializable="-1">
                                            </axisx>
                                            <axisy visibleinpanesserializable="-1">
                                            </axisy>
                                        </dxcharts:XYDiagram>
                                    </diagramserializable>
                                    <seriesserializable>
                                        <dxcharts:Series Name="Series 1">
                                        </dxcharts:Series>
                                        <dxcharts:Series Name="Series 2">
                                        </dxcharts:Series>
                                    </seriesserializable>
                                    <palettewrappers>
                                        <dxchartsui:PaletteWrapper Name="Toyota" ScaleMode="Repeat">
                                            <palette>
                                                <dxcharts:PaletteEntry Color="green" Color2="green" />
                                                <dxcharts:PaletteEntry Color="gold" Color2="gold" />
                                                <dxcharts:PaletteEntry Color="Red" Color2="Red" />
                                                <dxcharts:PaletteEntry Color="DarkRed" Color2="DarkRed" />
                                                <dxcharts:PaletteEntry Color="DarkGray" Color2="DarkGray" />
                                            </palette>
                                        </dxchartsui:PaletteWrapper>
                                    </palettewrappers>
                                </dxchartsui:WebChartControl>
                            </td>
                            
                            <td width="48%">
                                <dxchartsui:WebChartControl ID="chartCustomerSummary2" runat="server" 
                                    CrosshairEnabled="True" Height="450px" PaletteName="Toyota" Width="450px">
                                    <palettewrappers>
                                        <dxchartsui:PaletteWrapper Name="Toyota" ScaleMode="Repeat">
                                            <palette>
                                                <dxcharts:PaletteEntry Color="green" Color2="green" />
                                                <dxcharts:PaletteEntry Color="gold" Color2="gold" />
                                                <dxcharts:PaletteEntry Color="Red" Color2="Red" />
                                                <dxcharts:PaletteEntry Color="DarkRed" Color2="DarkRed" />
                                                <dxcharts:PaletteEntry Color="DarkGray" Color2="DarkGray" />
                                               </palette>
                                        </dxchartsui:PaletteWrapper>
                                    </palettewrappers>
                                </dxchartsui:WebChartControl>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
