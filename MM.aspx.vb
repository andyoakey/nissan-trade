﻿Imports System.Data
Imports DevExpress.Web
Imports System.Collections.Generic
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class MM

    Inherits System.Web.UI.Page
    Dim da As New DatabaseAccess
    Dim dsPC As DataSet
    Dim nBand1 As Integer
    Dim nBand2 As Integer

    Protected Sub dsPostCodeList_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsPostCodeList.Init
        dsPostCodeList.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCustomers_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCustomers.Init
        dsCustomers.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsLists_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLists.Init
        dsLists.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsDownload1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDownload1.Init
        dsDownload1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsDownload2_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDownload2.Init
        dsDownload2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Marketing Manager"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Marketing Manager", "Marketing Manager")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Not Page.IsPostBack Then
            CreateTree()
            ddlDownloadOptions.SelectedIndex = 0
        End If

        Call GetPostCodes()
        nBand1 = GetDataLong("SELECT MileageBand1 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
        nBand2 = GetDataLong("SELECT MileageBand2 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")


    End Sub

    Protected Sub btnMM_Click(sender As Object, e As EventArgs) Handles btnMM.Click
        Response.Redirect("ViewCompanies.aspx")
    End Sub

    Private Sub CreateTree()

        tvProducts.Nodes.Clear()
        Call AddProductNodes("Brake")
        Call AddProductNodes("Bumpers And Fixings")
        Call AddProductNodes("Clutch/ Drivetrain")
        Call AddProductNodes("Consumables")
        Call AddProductNodes("Electrical")
        Call AddProductNodes("Exhaust")
        Call AddProductNodes("Grilles And Mouldings")
        Call AddProductNodes("Heating/ Cooling")
        Call AddProductNodes("Lighting")
        Call AddProductNodes("Main Panels")
        Call AddProductNodes("Mirrors And Glass")
        Call AddProductNodes("Other")
        Call AddProductNodes("Service")
        Call AddProductNodes("Steering/ Suspension")

    End Sub

    Private Sub AddProductNodes(sGridName As String)

        Dim dsProducts As DataSet
        Dim sErrorMessage As String = ""
        Dim i As Integer
        Dim htIn As New Hashtable

        Dim mainNode = tvProducts.Nodes.Add(sGridName)

        htIn.Add("@sGridName", sGridName)
        dsProducts = da.Read(sErrorMessage, "p_GetGridLinesNissan", htIn)
        For i = 0 To dsProducts.Tables(0).Rows.Count - 1
            mainNode.Nodes.Add(dsProducts.Tables(0).Rows(i).Item(0))
        Next

    End Sub

    Private Sub GetPostCodes()

        Dim sErrorMessage As String = ""
        Dim htIn As New Hashtable

        htIn.Add("@sDealerCode", Session("SelectionId"))
        htIn.Add("@nOutOfTerritory_Flag", 0)
        dsPC = da.Read(sErrorMessage, "sp_Get_NissanPostCodes", htIn)
        ddlPostCodes.DataSource = dsPC.Tables(0)
        ddlPostCodes.DataBind()


    End Sub

    Protected Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        PopFilter.ShowOnPageLoad = True
    End Sub

    Protected Sub chkSpendFilters_CheckedChanged(sender As Object, e As EventArgs) Handles chkSpendFilters.CheckedChanged
        ddlHaveHaveNot.Visible = chkSpendFilters.Checked
        tvProducts.Visible = chkSpendFilters.Checked
        lblIncCustomers.Visible = chkSpendFilters.Checked
    End Sub

    Private Function CollateListBoxValuesDevX(ByVal lst As DevExpress.Web.ASPxListBox) As String
        Dim i As Integer
        Dim sStr As String
        Dim sItem As String
        sStr = "'"
        For i = 0 To lst.SelectedItems.Count - 1
            sItem = Trim(lst.SelectedItems(i).Text)
            sItem = Left(sItem, InStr(sItem, "-") - 2)
            sStr = sStr & sItem & "','"
        Next

        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If
        Return sStr
    End Function

    Private Function CollateCheckBoxValuesDevX(ByVal lst As DevExpress.Web.ASPxCheckBoxList) As String
        Dim i As Integer
        Dim sStr As String
        Dim sItem As String = " "

        For i = 0 To lst.SelectedItems.Count - 1
            sItem = Trim(lst.SelectedItems(i).Value)
            sStr = sStr & sItem & ","
        Next
        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 1)
        Else
            sStr = " "
        End If
        Return sStr
    End Function

    Private Function CollateCheckBoxValues(ByVal chklst As CheckBoxList) As String
        Dim i As Integer
        Dim sStr As String
        Dim sItem As String
        sStr = "'"
        For i = 0 To chklst.Items.Count - 1
            If chklst.Items(i).Selected Then
                sItem = Trim(chklst.Items(i).Value)
                sStr = sStr & sItem & "','"
            End If
        Next
        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If
        CollateCheckBoxValues = sStr
    End Function

    Private Function CollateTreeValuesDevX(ByVal tv As DevExpress.Web.ASPxTreeView) As String

        Dim mainNode As TreeViewNode
        Dim subNode As TreeViewNode
        Dim sStr As String = ""
        Dim sItem As String

        For Each mainNode In tv.Nodes
            For Each subNode In mainNode.Nodes
                If subNode.Checked Then
                    sItem = GetGridPFCs(subNode.Text)
                    If sItem <> "''" Then
                        sStr = sStr & sItem & ","
                    End If
                End If
            Next
        Next
        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 1)
        Else
            sStr = "''"
        End If
        CollateTreeValuesDevX = sStr

    End Function

    Private Function GetGridPFCs(sGridline As String) As String

        Dim sErrorMessage As String = ""
        Dim htIn As New Hashtable
        Dim dsPFCs As DataSet
        Dim i As Integer
        Dim sStr As String = "'"
        Dim sItem As String = ""

        htIn.Add("@sGridLine", sGridline)
        dsPFCs = da.Read(sErrorMessage, "p_GetGridPFCsNissan", htIn)
        For i = 0 To dsPFCs.Tables(0).Rows.Count - 1
            sItem = dsPFCs.Tables(0).Rows(i).Item(0)
            sStr = sStr & sItem & "','"
        Next
        If Len(sStr) > 1 Then
            sStr = Left(sStr, Len(sStr) - 2)
        Else
            sStr = "''"
        End If
        GetGridPFCs = sStr

    End Function

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Dim sPostCodes As String = CollateListBoxValuesDevX(ddlPostCodes)
        Dim nIncludeNTS As Integer = IIf(chkOutOfTerr.Checked, 1, 0)
        Session("nIncludeNTS") = nIncludeNTS
        Dim sBusTypes As String = CollateCheckBoxValuesDevX(chkBusType)
        Dim nActive As Integer = IIf(chkFilterCustType.Items(0).Selected, 1, 0)
        Dim nInActive As Integer = IIf(chkFilterCustType.Items(1).Selected, 1, 0)
        Dim nLapsed As Integer = IIf(chkFilterCustType.Items(2).Selected, 1, 0)
        Dim nProspect As Integer = IIf(chkFilterCustType.Items(3).Selected, 1, 0)
        Dim nNewProspect As Integer = IIf(chkFilterCustType.Items(4).Selected, 1, 0)
        Dim nDriveTime1 As Double = txtDriveTime1.Text
        Dim nDriveTime2 As Double = txtDriveTime2.Text
        Dim nDistance1 As Double = txtDistance1.Text
        Dim nDistance2 As Double = txtDistance2.Text
        Dim nSpendFilters As Double = IIf(chkSpendFilters.Checked, 1, 0)
        Dim nHaveHaveNot As Integer = ddlHaveHaveNot.Value
        Dim sPFCList As String = CollateTreeValuesDevX(tvProducts)

        If sPostCodes = "''" Then sPostCodes = ""
        If sBusTypes = "''" Then sBusTypes = ""

        If sPFCList = "''" Then
            sPFCList = ""
            nSpendFilters = 0
        End If


        Dim htIn1 As New Hashtable
        Dim dsResults As DataSet
        Dim sErr As String = ""

        '/ -------------------------------------------------------------------------------------------
        '/ Create the list and save it in the database
        '/ -------------------------------------------------------------------------------------------
        htIn1.Clear()
        htIn1.Add("@sDealerCode", Session("SelectionId"))
        htIn1.Add("@sPostCodeList", sPostCodes)
        htIn1.Add("@nIncludeNTS", nIncludeNTS)
        htIn1.Add("@sBusinessTypeString", sBusTypes)
        htIn1.Add("@nActive", nActive)
        htIn1.Add("@nInActive", nInActive)
        htIn1.Add("@nLapsed", nLapsed)
        htIn1.Add("@nProspect", nProspect)
        htIn1.Add("@nNewProspect", nNewProspect)
        htIn1.Add("@nDriveTime1", txtDriveTime1.Value)
        htIn1.Add("@nDriveTime2", txtDriveTime2.Value)
        htIn1.Add("@nDistance1", txtDistance1.Value)
        htIn1.Add("@nDistance2", txtDistance2.Value)
        htIn1.Add("@nSpendFilters", nSpendFilters)
        htIn1.Add("@nHaveHaveNot", nHaveHaveNot)
        htIn1.Add("@sPFCList", sPFCList)

        dsResults = da.Read(sErr, "p_MM_CreateList", htIn1)
        Session("ListNumber") = dsResults.Tables(0).Rows(0).Item(0)
        gridCompanies.DataBind()
        btnRemove.Enabled = gridCompanies.VisibleRowCount > 0
        btnDownload1.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadOptions.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadFileType.Enabled = gridCompanies.VisibleRowCount > 0
        btnExport.Enabled = gridCompanies.VisibleRowCount > 0
        btnSave.Enabled = gridCompanies.VisibleRowCount > 0

        If Session("ListNumber") = 0 Then
            PopErrorMessage.ShowOnPageLoad = True
        Else
            PopFilter.ShowOnPageLoad = False
        End If

    End Sub

    Protected Sub gridCompanies_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridCompanies.CustomColumnDisplayText

        Dim nDriveTime As Integer

        If e.Column.FieldName = "Distance" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                nDriveTime = IIf(IsDBNull(e.Value), 0, e.Value)
                If nDriveTime < 1 Then
                    e.DisplayText = "<1 mile"
                Else
                    e.DisplayText = Str(CInt(nDriveTime)) & " miles"
                End If
            End If
        End If

        If e.Column.FieldName = "DriveTime" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = FormatTime(e.Value)
            End If
        End If

        If e.Column.FieldName = "BusinessTypeDescription" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = ShortBusinessType(e.Value)
            End If
        End If

        If e.Column.FieldName = "CustomerStatus" Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = ShortCustomerType(e.Value)
            End If
        End If

    End Sub

    Private Function FormatTime(ByVal nMins As Long) As String

        Dim sTime As String = ""
        Dim nHours As Integer = 0

        If nMins > 60 Then
            nHours = Convert.ToInt32(Math.Floor(nMins / 60))
            nMins = nMins - (nHours * 60)
            sTime = Str(nHours) & "h " & Str(nMins) & "m"
        Else
            sTime = Str(CInt(nMins)) & "m"
        End If
        FormatTime = sTime

    End Function

    Private Function ShortBusinessType(ByVal sBT As String) As String
        Dim sShort As String
        Select Case Trim(sBT)
            Case "Bodyshop"
                sShort = "Bodyshop"
            Case "Breakdown Recovery Services"
                sShort = "Breakdown"
            Case "Car Dealer New/Used"
                sShort = "Dealer "
            Case "Independent Motor Trader"
                sShort = "IMT"
            Case "Mechanical Specialist"
                sShort = "Mech Spec"
            Case "Motor Factor"
                sShort = "Factor"
            Case "Parts Supplier"
                sShort = "Part Supp"
            Case "Taxi & Private Hire"
                sShort = "Taxi P/H"
            Case "Windscreens"
                sShort = "Windscreen"
            Case Else
                sShort = "Other"
        End Select
        ShortBusinessType = sShort
    End Function

    Private Function ShortCustomerType(ByVal sCT As String) As String

        Dim sShort As String

        sShort = sCT
        'Select Case Trim(sCT)


        '    Case "Active"
        '        sShort = "ACTV"
        '    Case "In-active"
        '        sShort = "INAC"
        '    Case "Lapsed"
        '        sShort = "LAPS"
        '    Case "Prospect"
        '        sShort = "PROS"
        '    Case ("Competitor")
        '        sShort = "COMPET"
        '    Case Else
        '        sShort = "NEW"
        'End Select

        ShortCustomerType = sShort

    End Function

    Protected Sub gridCompanies_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompanies.HtmlDataCellPrepared

        Dim nDistance As VariantType
        Dim bNull As Boolean = False

        If e.DataColumn.FieldName = "DriveTime" Or e.DataColumn.FieldName = "Distance" Then

            If IsDBNull(e.GetValue("Distance")) Then
                nDistance = 0
                bNull = True
            Else
                nDistance = Convert.ToInt32(Math.Floor(e.GetValue("Distance")))
            End If

            If Not bNull Then
                If nDistance < nBand1 Then
                    e.Cell.BackColor = System.Drawing.Color.YellowGreen
                ElseIf nDistance < nBand2 Then
                    e.Cell.BackColor = System.Drawing.Color.Gold
                Else
                    e.Cell.BackColor = System.Drawing.Color.Tomato
                    e.Cell.ForeColor = System.Drawing.Color.White
                End If
            End If

        End If

    End Sub

    Protected Sub btnSaveList_Click(sender As Object, e As EventArgs) Handles btnSaveList.Click

        Dim sSQL As String = ""
        If txtName.Text <> "" Then
            sSQL = "EXEC p_MM_SaveWorkList " & Session("ListNumber") & ",'" & txtName.Text & "'"
            Call RunNonQueryCommand(sSQL)
            ddlLists.DataBind()
            PopSaveList.ShowOnPageLoad = False
        Else
            PopNameError.ShowOnPageLoad = True
        End If

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        txtName.Text = GetDataString("SELECT ListName FROM Web_MM_Header WHERE Id = " & Session("ListNumber"))
        PopSaveList.ShowOnPageLoad = True
    End Sub

    Protected Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim selectedValues As List(Of Object)
        Dim i As Integer
        Dim fieldNames As List(Of String) = New List(Of String)()
        Dim sSQL As String

        fieldNames.Add("Id")
        selectedValues = gridCompanies.GetSelectedFieldValues(fieldNames.ToArray())
        For i = 0 To selectedValues.Count - 1
            sSQL = "DELETE FROM Web_MM_WorkList WHERE Id = " & selectedValues.Item(i).ToString
            Call RunNonQueryCommand(sSQL)
        Next
        gridCompanies.DataBind()

    End Sub

    Protected Sub btnLoadList_Click(sender As Object, e As EventArgs) Handles btnLoadList.Click

        Dim sSQL As String
        Try
            Session("ListNumber") = ddlLists.SelectedItem.Value
        Catch ex As Exception
            Session("ListNumber") = 0
        End Try

        sSQL = "EXEC p_MM_MoveWorkList " & Session("ListNumber")
        Call RunNonQueryCommand(sSQL)
        gridCompanies.DataBind()

        btnRemove.Enabled = gridCompanies.VisibleRowCount > 0
        btnDownload1.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadOptions.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadFileType.Enabled = gridCompanies.VisibleRowCount > 0
        btnExport.Enabled = gridCompanies.VisibleRowCount > 0
        btnSave.Enabled = gridCompanies.VisibleRowCount > 0

        PopLoadList.ShowOnPageLoad = False

    End Sub

    Protected Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        PopLoadList.ShowOnPageLoad = True
    End Sub

    Protected Sub btnDownload1_Click(sender As Object, e As EventArgs) Handles btnDownload1.Click

        Dim sFileName As String

        Select Case ddlDownloadOptions.SelectedItem.Value
            Case "0"
                ASPxGridViewExporter.GridViewID = "gridCompanies"
            Case "1"
                gridDownload1.DataBind()
                ASPxGridViewExporter.GridViewID = "gridDownload1"
            Case "2"
                gridDownload2.DataBind()
                ASPxGridViewExporter.GridViewID = "gridDownload2"
        End Select

        sFileName = "MarketingManagerList_" & Format(Now, "ddMMM")
        ASPxGridViewExporter.FileName = sFileName

        nBand1 = GetDataLong("SELECT MileageBand1 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")
        nBand2 = GetDataLong("SELECT MileageBand2 FROM CentreMaster WHERE DealerCode = '" & Session("SelectionId") & "'")

        If ddlDownloadFileType.SelectedIndex = 0 Then
            ASPxGridViewExporter.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        Else
            ASPxGridViewExporter.WriteCsvToResponse(fileName:=sFileName)
        End If

    End Sub

    Protected Sub ASPxGridViewExporter_RenderBrick(sender As Object, e As ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter.RenderBrick

        Dim nThisValue As Decimal = 0
        Dim nDistance As VariantType
        Dim bNull As Boolean = False

        Call GlobalRenderBrick(e)

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            e.Url = ""
            e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)
            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Name = "DriveTime" Or e.Column.Name = "Distance" Then

                If IsDBNull(e.GetValue("Distance")) Then
                    nDistance = 0
                    bNull = True
                Else
                    nDistance = Convert.ToInt32(Math.Floor(e.GetValue("Distance")))
                End If

                If Not bNull Then
                    If nDistance < nBand1 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.YellowGreen
                    ElseIf nDistance < nBand2 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.Gold
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                        e.BrickStyle.ForeColor = System.Drawing.Color.White
                    End If
                End If
            End If

        End If

    End Sub

    Protected Sub btnRemoveList_Click(sender As Object, e As EventArgs) Handles btnRemoveList.Click
        PopupRemoveListConfirm.ShowOnPageLoad = True
    End Sub

    Protected Sub btnRemoveConfirm_Click(sender As Object, e As EventArgs) Handles btnRemoveConfirm.Click

        Dim sSQL As String
        Try
            Session("ListNumber") = ddlLists.SelectedItem.Value
        Catch ex As Exception
            Session("ListNumber") = 0
        End Try

        sSQL = "EXEC p_MM_DeleteList " & Session("ListNumber")
        Call RunNonQueryCommand(sSQL)
        Session("ListNumber") = 0
        gridCompanies.DataBind()

        btnRemove.Enabled = gridCompanies.VisibleRowCount > 0
        btnDownload1.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadOptions.Enabled = gridCompanies.VisibleRowCount > 0
        ddlDownloadFileType.Enabled = gridCompanies.VisibleRowCount > 0
        btnSave.Enabled = gridCompanies.VisibleRowCount > 0

        ddlLists.DataBind()
        PopupRemoveListConfirm.ShowOnPageLoad = False
        PopLoadList.ShowOnPageLoad = False

    End Sub

    Protected Sub btnRemoveCancel_Click(sender As Object, e As EventArgs) Handles btnRemoveCancel.Click
        PopupRemoveListConfirm.ShowOnPageLoad = False
    End Sub

    Protected Sub btnContHistGo_Click(sender As Object, e As EventArgs) Handles btnContHistGo.Click

        lblContHistError.Text = ""

        If txtContHistComment.Text = "" Then
            lblContHistError.Text = "A comment must be entered."
        Else

            Dim htin As New Hashtable
            Dim nRes As Long
            Dim sErr As String

            htin.Add("@sDealerCode", Session("SelectionId"))
            htin.Add("@nListId", Session("ListNumber"))
            htin.Add("@nType", ddlContHistType.SelectedItem.Value)
            htin.Add("@sComment", txtContHistComment.Text)
            htin.Add("@nUserId", Session("UserId"))
            nRes = da.Update(sErr, "p_MM_UpdateContHist", htin)

            PopupContHist.ShowOnPageLoad = False

        End If

    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        PopupContHist.ShowOnPageLoad = True
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
