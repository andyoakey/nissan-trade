﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraPrinting
Imports DevExpress.Web
Imports DevExpress.Web.Data

Partial Class Dashboard

    Inherits System.Web.UI.Page

    Dim dsTargetResults As DataSet
    Dim dsTargetResults2015 As DataSet
    Dim dsSalesResults1 As DataSet
    Dim dsSalesResults2 As DataSet
    Dim dsKPIResults1 As DataSet
    Dim dsKPIResults2 As DataSet
    Dim dsCustomerSummaryResults As DataSet
    Dim dsBusinessSummaryResults As DataSet
    Dim dsBD4 As DataSet
    Dim dsTradeRewards As DataSet
    Dim dsStandards As DataSet
    Dim dsServiceTypes As DataSet

    Dim nForecastApplic As Double
    Dim nGrowthColumn As Integer

    Dim nperformance_last As Decimal = 0
    Dim nperformance_this As Decimal = 0

    Dim nEX_actual_2016 As Decimal = 0
    Dim nEX_forecast_2017 As Decimal = 0

    Dim nALL_actual_2016 As Decimal = 0
    Dim nALL_forecast_2017 As Decimal = 0

    Dim nDailyRunRate As Integer = 0
    Dim sBand As String = ""
    Dim bShowInfo As Boolean = False

    ' Used For Highlighting 
    Dim nQPayBand As Integer = 0
    Dim nDealerAverage As Integer = 0
    Dim nQForecast As Decimal = 0
    Dim nQRow As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim i As Integer

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        Try
            If Not IsPostBack Then
                Session("MonthFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O'")
                Session("TradeRewardYear") = 2017
                Session("KPIYear") = Session("CurrentYear")
                'Session("KPIYear") = 2017
                Session("BandCaption") = "Sales Forecast"
                For i = Session("CurrentYear") To 2014 Step -1
                    ddlKPIYear.Items.Add(Trim(Str(i)))
                    ddlKPIYear.SelectedIndex = 0
                Next
                Session("HighlightPeriod") = "Q2"

            End If
        Catch ex As Exception
            Session("CallingModule") = "Dashboard.ASPX - Page_Load()"
            Session("ErrorToDisplay") = ex.Message
            Response.Redirect("dbError.aspx")
        End Try



    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim sDate As String
        Dim sWholeMonth As String

        Dim sSelectionLevel As String = Session("SelectionLevel")
        Dim sSelectionId As String = Session("SelectionId")
        Dim nMonthFrom As Integer = Session("MonthFrom")
        Dim nKPIYear As Integer = Session("KPIYear")
        Dim nTradeRewardYear As Integer = Session("TradeRewardYear")

        If Page.IsPostBack = False Then
            If Session("ServiceType") = " " Or Session("ServiceType") Is Nothing Then
                Session("ServiceType") = "DAM"
            End If
        End If


        '  Handle Master Page Button Clicks
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim master_btnBack As DevExpress.Web.ASPxButton
        master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
        master_btnBack.Visible = False

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
        '  End Handle Master Page Button Clicks


        Call LoadTab1DashboardData()
        Call LoadMainDashboardData()

        ' Try

        Me.Title = "Nissan Trade Site - Dashboard (Sales Performance)"

            Select Case sSelectionLevel
                Case "N", "G"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = False
                    gridKPI1.Columns(4).Visible = False
                    gridKPI2.Visible = False
                Case "R"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = False
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "T"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = False
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "Z"
                    gridKPI1.Columns(1).Visible = False
                    gridKPI1.Columns(2).Visible = True
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = False
                Case "C"
                    If Not Page.IsPostBack Then
                        ASPxPageControl1.ActiveTabIndex = 0
                    End If
                    gridKPI1.Columns(1).Visible = True
                    gridKPI1.Columns(2).Visible = True
                    gridKPI1.Columns(3).Visible = True
                    gridKPI1.Columns(4).Visible = True
                    gridKPI2.Visible = True
            End Select

            sDate = GetMaxDate(sSelectionLevel, sSelectionId)
            sWholeMonth = GetDataString("SELECT dbo.NicePeriodName(PeriodName) FROM DataPeriods WHERE Id = (SELECT DataEndPeriodWholeMonth FROM System)")

            lblPageTitle.Text = "Dashboard : Sales up to and including : " & sDate
            gridCustomerSummary.Columns(0).Caption = " * Customer Summary as at " & sDate
            If nKPIYear = CurrentYear() Then
                gridKPI1.Columns(0).Caption = "KPI Summary as at " & sWholeMonth
                gridKPI2.Columns(0).Caption = "National position as at " & sWholeMonth
                gridKPI2.Columns(2).Visible = True
            Else
                gridKPI1.Columns(0).Caption = "KPI Summary as at 31st Dec " & nKPIYear
                gridKPI2.Columns(0).Caption = "National position as at 31st Dec " & nKPIYear
                gridKPI2.Columns(2).Visible = False
            End If


        '        If sSelectionLevel <> "C" Then And sSelectionLevel <> "G" Then   ' Only show this for Dealers and Groups
    '    If sSelectionLevel <> "C" Then '' Only show this for Dealers 
            ASPxPageControl1.TabPages(3).Visible = False
'        Else
 '           ASPxPageControl1.TabPages(3).Visible = True
  '          Call LoadBonusExtraData()
   '     End If


        'Catch ex As Exception
        'Session("CallingModule") = "Dashboard.ASPX - Page_LoadComplete()"
        'Session("ErrorToDisplay") = ex.Message
        'Response.Redirect("dbError.aspx")
        'End Try

    End Sub

    Protected Sub ds_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDashboardLeft.Init, dsDashboardRight.Init
        dsDashboardLeft.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsDashboardRight.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsCustomerSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBusinessTypeSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPI1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPI2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusPayments.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusBanding.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusByQuarter.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsKPIRanking.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Sub LoadTab1DashboardData()

        Dim da As New DatabaseAccess
        Dim sSection As String = ""
        Dim sErr As String = ""

        If Session("SelectionLevel") <> "N" And
                Session("SelectionLevel") <> "R" And
                Session("SelectionLevel") <> "T" And
                Session("SelectionLevel") <> "Z" And
                Session("SelectionLevel") <> "G" And
                Session("SelectionLevel") <> "C" Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If

        If Session("SelectionLevel") = "D" Or Session("SelectionLevel") = "C" Then
            gridtotalsales.Columns("valid_pc").Visible = True
        Else
            gridtotalsales.Columns("valid_pc").Visible = False
        End If

        '  Product Group Drop Down
        Try

            Dim htIn3 As New Hashtable

            dsServiceTypes = da.Read(sErr, "sp_ProductGroupList ", htIn3)

            ddlServiceTypes.DataSource = dsServiceTypes.Tables(0)
            ddlServiceTypes.DataBind()

        Catch ex As Exception

            If sErr <> "" Then
                Session("CallingModule") = "Dashboard.ASPX - LoadMainDashboardData() - " & sSection
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If

        End Try

    End Sub

    Protected Sub ddlKPIYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlKPIYear.SelectedIndexChanged
        Session("KPIYear") = ddlKPIYear.SelectedItem.Value

        If Session("KPIYear") = 2017 Then
            gridKPI2.DataSourceID = "dsKPIRanking"
        Else
            gridKPI2.DataSourceID = "dsKPI2"
        End If

    End Sub

    Function GetPercChange(ByVal nThisYear As Decimal, ByVal nLastYear As Decimal) As Decimal

        Dim nResult As Decimal
        nResult = 0
        If nThisYear <> 0 And nLastYear <> 0 Then
            nResult = (nThisYear - nLastYear) / nLastYear
        End If
        GetPercChange = nResult

    End Function

    Function GetMargin(ByVal nSale As Decimal, ByVal nCost As Decimal) As Decimal
        Dim nResult As Decimal
        nResult = 0
        If nSale <> 0 And nCost <> 0 Then
            nResult = (nSale - nCost) / nSale
        End If
        GetMargin = nResult
    End Function

    Protected Sub gridSalesSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName <> "RowTitle" Then
            If e.GetFieldValue("RowType") = "P" Then
                If e.Column.FieldName <> "Col6" Then
                    e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
                Else
                    e.DisplayText = "-"
                End If
            ElseIf e.GetFieldValue("RowType") = "I" Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "#,##0")
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "£#,##0")
            End If
        End If

    End Sub

    Protected Sub gridKPI2_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        Dim nNumber As Integer
        If e.Column.FieldName = "ColCentre" Then
            nNumber = IIf(IsDBNull(e.Value), 0, e.Value)
            e.DisplayText = FormatFancyNumber(Str(nNumber))
        End If
    End Sub

    Protected Sub gridCustomerSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ExcludedPerc" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub gridKPI2_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridKPI2.HtmlDataCellPrepared

        If e.DataColumn.Name = "Movement" Then
            If Not IsDBNull(e.CellValue) Then
                If e.CellValue > 0 Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Green
                    e.Cell.Text = "+" & e.CellValue
                End If

                If e.CellValue < 0 Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Red
                End If

            End If
        End If

    End Sub

    Protected Sub gridBD4_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If InStr(e.Column.FieldName, "_1") > 0 Or InStr(e.Column.FieldName, "_2") > 0 Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = Format(Val(e.Value), "£#,##0")
            End If
        End If

        If InStr(e.Column.FieldName, "_3") Then
            If IsDBNull(e.Value) Then
                e.DisplayText = ""
            Else
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, Val(e.Value)), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If

    End Sub

    Protected Sub gridBD4_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        Dim nThisValue As Decimal = 0

        If InStr(e.DataColumn.FieldName, "_3") Then
            If Not IsDBNull(e.CellValue) = True Then
                nThisValue = CDec(e.CellValue)
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = GlobalVars.g_Color_White
                If nThisValue = 0 Then
                    e.Cell.Font.Bold = False
                    e.Cell.BackColor = GlobalVars.g_Color_White
                    e.Cell.ForeColor = GlobalVars.g_Color_Black
                ElseIf nThisValue >= 100 Then
                    e.Cell.BackColor = GlobalVars.g_Color_YellowGreen
                Else
                    e.Cell.BackColor = GlobalVars.g_Color_Tomato
                End If
            End If
        End If

    End Sub

    Protected Sub gridBusinessType_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridBusinessTypeSummary.HtmlDataCellPrepared
        If e.GetValue("RowOrder") = 2 Or e.GetValue("RowOrder") = 4 Or e.GetValue("RowOrder") = 5 Then
            e.Cell.Font.Bold = True
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#a6a6a6")
        End If
    End Sub

    Sub LoadMainDashboardData()

        Dim da As New DatabaseAccess

        Dim sSection As String = ""
        Dim sErr As String = ""

        Dim htIn1 As New Hashtable
        Dim htIn2 As New Hashtable
        Dim htIn3 As New Hashtable
        Dim htIn4 As New Hashtable
        Dim htIn5 As New Hashtable
        Dim htIn6 As New Hashtable
        Dim htIn7 As New Hashtable
        Dim htIn8 As New Hashtable
        Dim htIn9 As New Hashtable

        Dim sSelectionLevel As String = Session("SelectionLevel")
        Dim sSelectionId As String = Session("SelectionId")
        Dim nMonthFrom As Integer = Session("MonthFrom")
        Dim nKPIYear As Integer = Session("KPIYear")
        Dim nTradeRewardYear As Integer = Session("TradeRewardYear")
        '     Dim nApplic As Integer = ddlApplic.SelectedIndex

        If sSelectionLevel Is Nothing Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If

        If Session("SelectionLevel") <> "N" And
           Session("SelectionLevel") <> "R" And
           Session("SelectionLevel") <> "T" And
           Session("SelectionLevel") <> "Z" And
           Session("SelectionLevel") <> "G" And
            Session("SelectionLevel") <> "C" Then
            Session.Abandon()
            Response.Redirect("Login.aspx")
        End If

    End Sub

    Protected Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridtotalsales.HtmlDataCellPrepared, gridselectedsales.HtmlDataCellPrepared
        Dim nThisValue As Decimal = 0

        If e.DataColumn.Caption = "" Then
            e.Cell.Width = 2
        End If

        If e.DataColumn.Caption = "% Uplift" Then
            e.Cell.ForeColor = Color.White
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            If nThisValue = 0 Then
                e.Cell.BackColor = Color.White
                e.Cell.ForeColor = Color.Black
            ElseIf nThisValue >= 1 Then
                e.Cell.BackColor = Color.Green
            ElseIf nThisValue >= 0.9 Then
                e.Cell.BackColor = Color.Orange
                e.Cell.ForeColor = Color.Black
            Else
                e.Cell.BackColor = Color.Red
            End If
        End If

        If e.DataColumn.Caption = "% Valid Sales" Then
            e.Cell.ForeColor = Color.White
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If

            If nThisValue >= 0.8 Then
                e.Cell.BackColor = Color.Green
            Else
                e.Cell.BackColor = Color.Red
            End If

            If nThisValue = 0 Then
                e.Cell.ForeColor = Color.White
                e.Cell.BackColor = Color.White
            End If

        End If

    End Sub

    Private Sub ddlServiceTypes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServiceTypes.SelectedIndexChanged
        Session("ServiceType") = ddlServiceTypes.Value
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles expTotalSales.RenderBrick, expSelectedSales.RenderBrick, expCustomerSummary.RenderBrick, expBusinessTypeSummary.RenderBrick, expKPI1.RenderBrick, expKPI2.RenderBrick, expBonus.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Sub btnExcel_Click()

        Dim link1 As New DevExpress.Web.Export.GridViewLink(expTotalSales)
        Dim link2 As New DevExpress.Web.Export.GridViewLink(expSelectedSales)
        Dim link3 As New DevExpress.Web.Export.GridViewLink(expCustomerSummary)
        Dim link4 As New DevExpress.Web.Export.GridViewLink(expBusinessTypeSummary)
        Dim link5 As New DevExpress.Web.Export.GridViewLink(expKPI1)
        Dim link6 As New DevExpress.Web.Export.GridViewLink(expKPI2)


        gridtotalsales.Settings.ShowTitlePanel = True
        gridtotalsales.SettingsText.Title = "Total Sales"

        gridselectedsales.Settings.ShowTitlePanel = True
        gridselectedsales.SettingsText.Title = ddlServiceTypes.Text

        gridCustomerSummary.Settings.ShowTitlePanel = True
        gridCustomerSummary.SettingsText.Title = "Customer Summary"

        gridBusinessTypeSummary.Settings.ShowTitlePanel = True
        gridBusinessTypeSummary.SettingsText.Title = "Business Type Summary"

        gridKPI1.Settings.ShowTitlePanel = True
        gridKPI1.SettingsText.Title = "KPI Summary 1"

        gridKPI2.Settings.ShowTitlePanel = True
        gridKPI2.SettingsText.Title = "KPI Summary 2"


        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())

        Dim headercell As New Link()
        Dim blankcell As New Link()
        AddHandler headercell.CreateDetailArea, AddressOf createheadercell
        AddHandler blankcell.CreateDetailArea, AddressOf createblankcell

        'If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then   ' Only show this for Dealers and Groups
      '  If Session("SelectionLevel") = "C" Then ' Only show this for Dealers 
       '     Dim link7 As New DevExpress.Web.Export.GridViewLink(expBonus)
        '    gridBonusByQuarter.Settings.ShowTitlePanel = True
         '   gridBonusByQuarter.SettingsText.Title = "Trade Sales Reward"
          '  composite.Links.AddRange(New Object() {headercell, blankcell, link1, blankcell, link2, blankcell, link3, blankcell, link4, blankcell, link5, blankcell, link6, blankcell, link7})
        'Else
            composite.Links.AddRange(New Object() {headercell, blankcell, link1, blankcell, link2, blankcell, link3, blankcell, link4, blankcell, link5, blankcell, link6})
        'End If

        composite.CreateDocument()

        Dim stream As New System.IO.MemoryStream()

        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, "Dashboard", True, "xlsx", stream)

        gridtotalsales.Settings.ShowTitlePanel = vbFalse
        gridtotalsales.SettingsText.Title = ""

        gridselectedsales.Settings.ShowTitlePanel = vbFalse
        gridselectedsales.SettingsText.Title = ""

        gridCustomerSummary.Settings.ShowTitlePanel = vbFalse
        gridCustomerSummary.SettingsText.Title = ""

        gridBusinessTypeSummary.Settings.ShowTitlePanel = vbFalse
        gridBusinessTypeSummary.SettingsText.Title = ""

        gridKPI1.Settings.ShowTitlePanel = vbFalse
        gridKPI1.SettingsText.Title = ""

        gridKPI2.Settings.ShowTitlePanel = vbFalse
        gridKPI2.SettingsText.Title = ""

        If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then
            gridBonusByQuarter.Settings.ShowTitlePanel = vbFalse
            gridBonusByQuarter.SettingsText.Title = ""
        End If

    End Sub

    Public Sub createblankcell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.White)
        brick.Text = " "
        brick.Rect = New RectangleF(0, 0, 2000, 20)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub createheadercell(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        Dim brick As New TextBrick(BorderSide.None, 0, Color.White, Color.White, Color.Black)
        brick.Font = New System.Drawing.Font("Verdana", 16, FontStyle.Underline)
        brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        brick.Text = "Trade Parts Overview "
        brick.Rect = New RectangleF(0, 0, 2000, 40)
        e.Graph.DrawBrick(brick)
    End Sub

    Private Sub gridBonusByQuarter_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridBonusByQuarter.HtmlDataCellPrepared
        If Right(Trim(e.DataColumn.Name), 6) = "growth" Then
            e.Cell.ForeColor = Color.White

            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If

            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            End If

            If e.CellValue = 0 Then
                e.Cell.BackColor = Color.White
            End If
        End If


        If Left(e.GetValue("quartername"), 2) = Session("HighlightPeriod") Then
            If e.DataColumn.FieldName = "quartername" Or e.DataColumn.FieldName = "sales_forecast" Or e.DataColumn.FieldName = "rewardband" Then
                e.Cell.BackColor = Color.LightBlue
            End If
        End If

        If e.GetValue("rewardband") = 0 Then
            bShowInfo = False
        Else
            bShowInfo = True
        End If

    End Sub



    'Private Sub gridBonusByQuarter_HtmlCommandCellPrepared(sender As Object, e As ASPxGridViewTableCommandCellEventArgs) Handles gridBonusByQuarter.HtmlCommandCellPrepared
    '    If bShowInfo = False Then
    '        e.Cell.Controls.Clear()
    '    End If

    'End Sub

    Sub LoadBonusExtraData()
        Dim htInBonus As New Hashtable
        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim dsBonus As DataSet

        htInBonus.Add("@sSelectionLevel", Session("SelectionLevel"))
        htInBonus.Add("@sSelectionID", Session("SelectionId"))

        dsBonus = da.Read(sErr, "sp_BonusReport_1718", htInBonus)

        With dsBonus.Tables(0)
            nDailyRunRate = .Rows(0).Item("runrate")
        End With


        ' These Are used for Blue Highlighting
        Select Case Session("HighlightPeriod")
            Case "Q2"
                nQRow = 0
            Case "Q3"
                nQRow = 1
            Case "Q4"
                nQRow = 2
            Case "Q1"
                nQRow = 3
        End Select

        nQPayBand = dsBonus.Tables(0).Rows(nQRow).Item("rewardband")
        nQForecast = dsBonus.Tables(0).Rows(nQRow).Item("forecast_sales")





        If Session("SelectionLevel") = "C" Then
            sBand = GetDataString("select banding from Bonus_DealerUIO where dealercode = '" & Session("SelectionId") & "'")
            lblRunRate.Text = "Dealer Daily Run Rate (inc. NWCR sales where applicable) - £" & Format(nDailyRunRate, "#,###.")
            lblPerformanceBand.Text = "Dealer Performance Banding: " & sBand
        End If

        If Session("SelectionLevel") = "G" Then
            sBand = GetDataString("select banding from Bonus_DealerGroupUIO Where dealergroup_id = " & Session("SelectionId"))
            lblRunRate.Text = "Group Daily Run Rate (inc. NWCR sales where applicable) - £" & Format(nDailyRunRate, "#,###.")
            lblPerformanceBand.Text = "Group Performance Banding: " & sBand
        End If

        gridPaymentScale.DataBind()

    End Sub

    Protected Sub CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridBonusByQuarter.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nperformance_last = 0
            nperformance_this = 0

            nEX_actual_2016 = 0
            nEX_forecast_2017 = 0

            nALL_actual_2016 = 0
            nALL_forecast_2017 = 0

        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nperformance_last = nperformance_last + e.GetValue("performance_last")
            nperformance_this = nperformance_this + e.GetValue("performance_this")

            nEX_actual_2016 = nEX_actual_2016 + e.GetValue("ex_actual_2016")
            nEX_forecast_2017 = nEX_forecast_2017 + e.GetValue("ex_forecast_2017")

            nALL_actual_2016 = nALL_actual_2016 + e.GetValue("all_actual_2016")
            nALL_forecast_2017 = nALL_forecast_2017 + e.GetValue("all_forecast_2017")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "performance_growth" Then
                If nperformance_last > 0 And nperformance_this > 0 Then
                    e.TotalValue = ((nperformance_this / nperformance_last) - 1)
                End If
            End If

            If objSummaryItem.FieldName = "ex_forecast_growth" Then
                If nEX_actual_2016 > 0 And nEX_forecast_2017 > 0 Then
                    e.TotalValue = ((nEX_forecast_2017 / nEX_actual_2016) - 1)
                End If
            End If

            If objSummaryItem.FieldName = "all_forecast_growth" Then
                If nALL_actual_2016 > 0 And nALL_forecast_2017 > 0 Then
                    e.TotalValue = ((nALL_forecast_2017 / nALL_actual_2016) - 1)
                End If
            End If

        End If

    End Sub

    Protected Sub grid_CustomButtonCallback(ByVal sender As Object, ByVal e As ASPxGridViewCustomButtonCallbackEventArgs) Handles gridBonusByQuarter.CustomButtonCallback
        If e.ButtonID <> "btninfo" Then
            Return
        End If
        Session("ShowInformationColours") = vbTrue
    End Sub

    Private Sub rblHighlight_ValueChanged(sender As Object, e As EventArgs) Handles rblHighlight.ValueChanged
        Session("HighlightPeriod") = rblHighlight.Value
    End Sub

    Private Sub gridBonusByQuarter_SelectionChanged(sender As Object, e As EventArgs) Handles gridBonusByQuarter.SelectionChanged
        Session("HighlightPeriod") = Left(sender.GetSelectedFieldValues("quartername")(0), 2)
    End Sub

    Private Sub gridBonusBandings_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridBonusBandings.HtmlDataCellPrepared

        Select Case Left(LTrim(sBand), 1)
            Case "1"  ' Below Average
                If e.DataColumn.Name = "band0_text" Then
                    If nQPayBand = 0 Then
                        If e.GetValue("id") = 1 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band1_text" Then
                    If nQPayBand = 1 Then
                        If e.GetValue("id") = 1 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band2_text" Then
                    If nQPayBand = 2 Then
                        If e.GetValue("id") = 1 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band3_text" Then
                    If nQPayBand = 3 Then
                        If e.GetValue("id") = 1 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

            Case "2"  ' Average

                If e.DataColumn.Name = "band0_text" Then
                    If nQPayBand = 0 Then
                        If e.GetValue("id") = 2 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If


                If e.DataColumn.Name = "band1_text" Then
                    If nQPayBand = 1 Then
                        If e.GetValue("id") = 2 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band2_text" Then
                    If nQPayBand = 2 Then
                        If e.GetValue("id") = 2 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band3_text" Then
                    If nQPayBand = 3 Then
                        If e.GetValue("id") = 2 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If
            Case "3"  ' AboveAverage

                If e.DataColumn.Name = "band0_text" Then
                    If nQPayBand = 0 Then
                        If e.GetValue("id") = 3 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band1_text" Then
                    If nQPayBand = 1 Then
                        If e.GetValue("id") = 3 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band2_text" Then
                    If nQPayBand = 2 Then
                        If e.GetValue("id") = 3 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If

                If e.DataColumn.Name = "band3_text" Then
                    If nQPayBand = 3 Then
                        If e.GetValue("id") = 3 Then
                            e.Cell.BackColor = Color.LightBlue
                        End If
                    End If
                End If
        End Select


    End Sub


    Private Sub gridPaymentScale_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridPaymentScale.HtmlDataCellPrepared

        Dim nDisplayValueLower As Integer = 0
        Dim nDisplayValueUpper As Integer = 0

        Select Case e.GetValue("id")
            Case 1
                nDisplayValueLower = 0
                nDisplayValueUpper = 31249
            Case 2
                nDisplayValueLower = 31250
                nDisplayValueUpper = 62499
            Case 3
                nDisplayValueLower = 62500
                nDisplayValueUpper = 124999
            Case 4
                nDisplayValueLower = 125000
                nDisplayValueUpper = 187499
            Case 5
                nDisplayValueLower = 187500
                nDisplayValueUpper = 274999
            Case 6
                nDisplayValueLower = 275000
                nDisplayValueUpper = 374999
            Case 7
                nDisplayValueLower = 375000
                nDisplayValueUpper = 499999
            Case 8
                nDisplayValueLower = 500000
                nDisplayValueUpper = 50000000
        End Select

        If e.DataColumn.Name = "payband_0" Then
            If nQPayBand = 0 Then
                If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
                    e.Cell.BackColor = Color.LightBlue
                Else
                    e.Cell.ForeColor = Color.Black
                    e.Cell.BackColor = Color.White
                End If
            End If
        End If


        If e.DataColumn.Name = "payband_1" Then
            If nQPayBand = 1 Then
                If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
                    e.Cell.BackColor = Color.LightBlue
                Else
                    e.Cell.ForeColor = Color.Black
                    e.Cell.BackColor = Color.White
                End If
            End If
        End If

        If e.DataColumn.Name = "payband_2" Then
            If nQPayBand = 2 Then
                If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
                    e.Cell.BackColor = Color.LightBlue
                Else
                    e.Cell.ForeColor = Color.Black
                    e.Cell.BackColor = Color.White
                End If
            End If
        End If

        If e.DataColumn.Name = "payband_3" Then
            If nQPayBand = 3 Then
                If (nDisplayValueLower <= nQForecast) And (nDisplayValueUpper >= nQForecast) Then
                    e.Cell.BackColor = Color.LightBlue
                Else
                    e.Cell.ForeColor = Color.Black
                    e.Cell.BackColor = Color.White
                End If
            End If
        End If


    End Sub



End Class


