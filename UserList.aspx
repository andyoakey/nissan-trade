﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserList.aspx.vb" Inherits="UserList" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
	
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
       <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="User Details" />
        </div>

 
        <br />
        
        <dx:ASPxGridView 
            ID="gridUsers" 
            runat="server" 
            DataSourceID="dsUsers" 
            AutoGenerateColumns="False" 
            KeyFieldName="UserId" 
            Width="100%"
            OnLoad="GridStyles"
            OnCustomColumnDisplayText="gridUsers_OnCustomColumnDisplayText" >
            
            <SettingsPager PageSize="16" Visible="True">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </SettingsPager>

            <SettingsPopup 
                EditForm-VerticalAlign ="WindowCenter"
                EditForm-HorizontalAlign="WindowCenter"
                EditForm-Modal="true"
                EditForm-ShowHeader="true"
                />

            <SettingsEditing 
                Mode="PopupEditForm" 
                EditFormColumnCount="2" 
                PopupEditFormShowHeader="false"  
                PopupEditFormWidth="450px"  />
          
            <Columns>
            
                <dx:GridViewCommandColumn  
                    Width = "6%"
                    Caption=" " 
                    VisibleIndex="0" 
                    NewButton-Visible="false" 
                    DeleteButton-Visible="false" 
                    ButtonType="Link" 
                    CellStyle-Font-Size ="10px">
                    <EditButton Visible="True" Text="">
                    </EditButton>
                    <ClearFilterButton Visible="True">
                    </ClearFilterButton>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dx:GridViewCommandColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="FirstName"
                    Name="Name" 
                    Caption="Name"
                    VisibleIndex="2" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "2" EditFormSettings-ColumnSpan = "2" Width="15%">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" VisibleIndex="2" ColumnSpan="2"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="EmailAddress"
                    Name="Email Address" 
                    Caption="Email Address"
                    VisibleIndex="3" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "3" EditFormSettings-ColumnSpan = "2">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" VisibleIndex="3" ColumnSpan="2"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Access" 
                    Caption="Access" 
                    VisibleIndex="4" 
                    ReadOnly="true" 
                    PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"  
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "4" EditFormSettings-ColumnSpan = "2" Width="25%">
                    <PropertiesTextEdit>
                        <ReadOnlyStyle BackColor="LightGray"></ReadOnlyStyle>
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="True" ColumnSpan="2" VisibleIndex="4"></EditFormSettings>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    FieldName="UserId" 
                    Caption="Id" 
                    VisibleIndex="5" 
                    Visible="False"
                    EditFormSettings-Visible="False" 
                    EditFormSettings-VisibleIndex = "5">
                    <EditFormSettings Visible="False" VisibleIndex="5"></EditFormSettings>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    FieldName="Password" 
                    Caption="Password" 
                    VisibleIndex="6" 
                    Visible="False"
                    EditFormSettings-Visible="False" 
                    EditFormSettings-VisibleIndex = "6">
                    <EditFormSettings Visible="False" VisibleIndex="5"></EditFormSettings>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataDateColumn Caption="Last Login" FieldName="LastLogin" 
                    Name="LastLogin" VisibleIndex="5">
                    <PropertiesDateEdit DisplayFormatString="">
                    </PropertiesDateEdit>
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="True" />
                    <EditFormSettings Visible="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataDateColumn>

                <dx:GridViewDataCheckColumn Caption="Deleted" FieldName="Deleted" ToolTip=""
                    VisibleIndex="8" Width="75px" Visible ="False" 
                    EditFormSettings-Visible="True" 
                    EditFormSettings-VisibleIndex = "7">
                    <EditFormSettings Visible="True" VisibleIndex="6"></EditFormSettings>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="False" ShowFilterRowMenu="False" />
                </dx:GridViewDataCheckColumn>

                <dx:GridViewDataHyperLinkColumn Caption="#" FieldName="UserId" VisibleIndex="9">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" 
                        ShowFilterRowMenu="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" VerticalAlign="Middle" />
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="UserConfEmail.aspx?{0}" 
                        Target="_blank"  Text="Password Reminder">
                        <Style HorizontalAlign="Center">
                        </Style>
                    </PropertiesHyperLinkEdit>
                    <EditFormSettings Visible="False" />
                    <CellStyle HorizontalAlign="Center" />
                </dx:GridViewDataHyperLinkColumn>

            </Columns>
            
            <Settings 
                ShowHeaderFilterButton="True" 
                UseFixedTableLayout="True" 
                ShowFilterRow="False" 
                ShowFooter ="False" 
                ShowVerticalScrollBar="False" />
            
            <TotalSummary>
                <dx:ASPxSummaryItem FieldName="UserId" DisplayFormat="Users: {0}" SummaryType="Count" ShowInColumn="Surname" />
            </TotalSummary>

        </dx:ASPxGridView>

        <br />        

    </div>

    <asp:SqlDataSource ID="dsUsers" runat="server" SelectCommand="p_WebUsers" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nUserId" SessionField="UserId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter"
        runat="server" 
        GridViewID="gridUsers">
    </dx:ASPxGridViewExporter>

</asp:Content>

