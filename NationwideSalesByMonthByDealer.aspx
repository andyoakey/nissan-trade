﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NationwideSalesByMonthByDealer.aspx.vb" Inherits="NationwideSalesByMonthByDealer" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Nationwide Sales Summary By Dealer" />
        </div>

         <br />

        <div id="divFY" style="position:relative;">
            <dx:ASPxComboBox
                runat="server"
                AutoPostBack="true"
                id="cboFY">
                <Items>
                    <dx:ListEditItem Text ="Fin Year 2018-19" Value ="148" />
                    <dx:ListEditItem Text ="Fin Year 2017-18" Value ="136" Selected ="true" />
                    <dx:ListEditItem Text ="Fin Year 2016-17" Value ="124" />
                </Items>
            </dx:ASPxComboBox>
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsSales" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Styles-Footer-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="5%" 
                exportwidth="100"
                Caption="Region" 
                FieldName="region">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="4%" 
                exportwidth="100"
                Caption="Zone" 
                FieldName="zone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="4%" 
                exportwidth="100"
                Caption="Code" 
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="250"
                Caption="Dealer Name" 
                FieldName="dealername">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="month1" 
                Name="month1"
                FieldName="month1">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="month2" 
                Name="month2"
                FieldName="month2">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="month3" 
                Name="month3"
                FieldName="month3">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Name="month4"
                Caption="month4" 
                FieldName="month4">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="5" 
                Settings-AllowSort="True"
                Width="6%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="150"
                Name="month5"
                Caption="month5" 
                FieldName="month5">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="6" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="6%" 
                exportwidth="150"
                Name="month6"
                Caption="month6" 
                FieldName="month6">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="7" 
                Settings-AllowSort="True"
                Width="6%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="150"
                Name="month7"
                Caption="month7" 
                FieldName="month7">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="8" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                Name="month8"
                Caption="month8" 
                FieldName="month8">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="9" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="month9" 
                Name="month9"
                FieldName="month9">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="10" 
                Settings-AllowSort="True"
                Width="6%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="150"
                Name="month10"
                Caption="month10" 
                FieldName="month10">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="11" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="6%" 
                exportwidth="150"
                Name="month11"
                Caption="month11" 
                FieldName="month11">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="12" 
                Settings-AllowSort="True"
                Width="6%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="month12" 
                Name="month12"
                FieldName="month12">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="13" 
                Settings-AllowSort="True"
                Width="7%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="FY Total" 
                FieldName="fytotal">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>
         </Columns>
   
            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total: ({0} Dealers)" FieldName="dealername" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month1" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month2" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month3" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month4" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month5" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month6" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month7" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month8" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month9" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month10" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month11" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="month12" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0 " FieldName="fytotal" SummaryType="Sum" />
            </TotalSummary>
                 
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsSales" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_NationwideSalesByMonthByDealer" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="nFrom" SessionField="FYFrom" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
   

    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

