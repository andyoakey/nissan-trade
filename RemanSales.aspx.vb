﻿Imports DevExpress.XtraPrinting

Partial Class RemanSales
    Inherits System.Web.UI.Page
    Dim nTotalThisUnits As Integer = 0
    Dim nTotalThisSales As Decimal = 0
    Dim nTotalThisCustomers As Integer = 0
    Dim nTotalPrevUnits As Integer = 0
    Dim nTotalPrevSales As Decimal = 0
    Dim nTotalPrevCustomers As Integer = 0

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim master_lblPageTitle As DevExpress.Web.ASPxLabel
        master_lblPageTitle = CType(Master.FindControl("lblPageTitle"), DevExpress.Web.ASPxLabel)
        master_lblPageTitle.Text = "Reman Part Sales"

        Me.Page.Title = master_lblPageTitle.Text
        Session("nDataPeriod") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'") + 1

        If Page.IsPostBack = False Then
            Session("RefCode") = ""
            Session("ShowDataType") = 0
            If IsPostBack = False Then
                Session("FromMonth") = GetDataDecimal("select min(id) from DataPeriods  where financialyear = (Select financialyear from DataPeriods where id = " & Session("nDataPeriod") & ")")
                Session("ToMonth") = Session("nDataPeriod")
                Me.ddlFrom.Text = GetDataString("Select periodname from DataPeriods where id = " & Session("FromMonth"))
                Me.ddlTo.Text = GetDataString("Select periodname from DataPeriods where id = " & Session("ToMonth"))
            End If
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        With gridMain
            .Columns("accompressor").Visible = False
            .Columns("alternator").Visible = False
            .Columns("cylinderhead").Visible = False
            .Columns("driveshaft").Visible = False
            .Columns("engine").Visible = False
            .Columns("injectionpump").Visible = False
            .Columns("injectors").Visible = False
            .Columns("radiator").Visible = False
            .Columns("startermotor").Visible = False
            .Columns("steeringgear").Visible = False
            .Columns("transmission").Visible = False
            .Columns("turbocharger").Visible = False
            .Columns("remantotal").Visible = False
            .Columns("accompressor_q").Visible = False
            .Columns("alternator_q").Visible = False
            .Columns("cylinderhead_q").Visible = False
            .Columns("driveshaft_q").Visible = False
            .Columns("engine_q").Visible = False
            .Columns("injectionpump_q").Visible = False
            .Columns("injectors_q").Visible = False
            .Columns("radiator_q").Visible = False
            .Columns("startermotor_q").Visible = False
            .Columns("steeringgear_q").Visible = False
            .Columns("transmission_q").Visible = False
            .Columns("turbocharger_q").Visible = False
            .Columns("remantotal_q").Visible = False

            If Session("ShowDataType") = 0 Then
                .Columns("accompressor").Visible = True
                .Columns("alternator").Visible = True
                .Columns("cylinderhead").Visible = True
                .Columns("driveshaft").Visible = True
                .Columns("engine").Visible = True
                .Columns("injectionpump").Visible = True
                .Columns("injectors").Visible = True
                .Columns("radiator").Visible = True
                .Columns("startermotor").Visible = True
                .Columns("steeringgear").Visible = True
                .Columns("transmission").Visible = True
                .Columns("turbocharger").Visible = True
                .Columns("remantotal").Visible = True
            End If

            If Session("ShowDataType") = 1 Then
                .Columns("accompressor_q").Visible = True
                .Columns("alternator_q").Visible = True
                .Columns("cylinderhead_q").Visible = True
                .Columns("driveshaft_q").Visible = True
                .Columns("engine_q").Visible = True
                .Columns("injectionpump_q").Visible = True
                .Columns("injectors_q").Visible = True
                .Columns("radiator_q").Visible = True
                .Columns("startermotor_q").Visible = True
                .Columns("steeringgear_q").Visible = True
                .Columns("transmission_q").Visible = True
                .Columns("turbocharger_q").Visible = True
                .Columns("remantotal_q").Visible = True
            End If

        End With

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "RemanSales"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub GridStyles2(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub gridTransactions_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("thiscustomerdealerid") = Trim((TryCast(sender, DevExpress.Web.ASPxGridView)).GetMasterRowKeyValue())
    End Sub

    Private Sub ddlFrom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("FromMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlFrom.Value, 8)) & "'")
    End Sub

    Private Sub ddlTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("ToMonth") = GetDataDecimal("select id from dataperiods where periodname = '" & Trim(Left(ddlTo.Value, 8)) & "'")
    End Sub

    Private Sub ddlShowType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlShowType.SelectedIndexChanged
        Session("ShowDataType") = ddlShowType.Value
    End Sub

End Class



