﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class HOBonusReport_1819

    Inherits System.Web.UI.Page

    Dim nperformance_last As Decimal = 0
    Dim nperformance_this As Decimal = 0

    Dim nEX_actual_2017 As Decimal = 0
    Dim nEX_forecast_2018 As Decimal = 0

    Dim nALL_actual_2017 As Decimal = 0
    Dim nALL_forecast_2018 As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Dealer Details"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Dealer List", "Viewing Dealer List")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Dim dToday As DateTime
        dToday = Now()

        If Page.IsPostBack = False Then
            'Session("CurrentQuarter") = GetDataDecimal("select financialquarter from dataperiods where getdate() between startdate and dateadd(d,1,enddate)")
            Session("CurrentQuarter") = 1
            Session("ShowBanding") = False
        End If


        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@nquarter_id", Session("CurrentQuarter"))
        ds = da.Read(sErrorMessage, "sp_QuarterDays_1718", htin)
        txtWorkingDays.Text = ds.Tables(0).Rows(0).Item("workingdaysinquarter").ToString
        txtDaysToDate.Text = ds.Tables(0).Rows(0).Item("workingdayscompleted").ToString

        With gridDealerBonus

            ' Header 
            With .Styles.Header
                .Wrap = DevExpress.Utils.DefaultBoolean.True
                .HorizontalAlign = HorizontalAlign.Center
            End With

            ' Cell
            With .Styles.Cell
                .Wrap = False
                .HorizontalAlign = HorizontalAlign.Center
            End With

            ' Pager
            With .SettingsPager
                .AllButton.Visible = True
                .AlwaysShowPager = False
                .FirstPageButton.Visible = False
                .LastPageButton.Visible = False
            End With

            ' Footer
            With .Styles.Footer
                .HorizontalAlign = HorizontalAlign.Center
            End With

            ' Settings 
            With .Settings
                .ShowHeaderFilterButton = False
                .ShowHeaderFilterBlankItems = False
                .ShowFooter = True
            End With

            With .SettingsBehavior
                .AllowSort = True
            End With

            Select Case Session("CurrentQuarter")
                Case 1
                    .Columns("sales_all_2017").Caption = "Actual Sales Q2 2017"
                    .Columns("sales_all_2018").Caption = "Actual Sales Q2 2018"
                    .Columns("forecast_sales").Caption = "Forecast Sales Q2 2018"
                    .Columns("rewardpayment").Caption = "Forecast Reward Q2 2018"
                    .Columns("sales_ex_2017").Caption = "Non NWCR Sales Q2 2017"
                    .Columns("sales_ex_2018").Caption = "Non NWCR Sales Q2 2018"
                    .Columns("sales_nw_2018").Caption = "NWCR Sales Q2 2018"
                    .Columns("forecast_growth").Caption = "Non NWCR Growth Q2 2018"

                Case 2
                    .Columns("sales_all_2017").Caption = "Actual Sales Q3 2017"
                    .Columns("sales_all_2018").Caption = "Actual Sales Q3 2018"
                    .Columns("forecast_sales").Caption = "Forecast Sales Q3 2018"
                    .Columns("rewardpayment").Caption = "Forecast Reward Q3 2018"
                    .Columns("sales_ex_2017").Caption = "Non NWCR Sales Q3 2017"
                    .Columns("sales_ex_2018").Caption = "Non NWCR Sales Q3 2018"
                    .Columns("sales_nw_2018").Caption = "NWCR Sales Q3 2018"
                    .Columns("forecast_growth").Caption = "Non NWCR Growth Q3 2018"
                Case 3
                    .Columns("sales_all_2017").Caption = "Actual Sales Q4 2017"
                    .Columns("sales_all_2018").Caption = "Actual Sales Q4 2018"
                    .Columns("forecast_sales").Caption = "Forecast Sales Q4 2018"
                    .Columns("rewardpayment").Caption = "Forecast Reward Q4 2018"
                    .Columns("sales_ex_2017").Caption = "Non NWCR Sales Q4 2017"
                    .Columns("sales_ex_2018").Caption = "Non NWCR Sales Q4 2018"
                    .Columns("sales_nw_2018").Caption = "NWCR Sales Q4 2018"
                    .Columns("forecast_growth").Caption = "Non NWCR Growth Q4 2018"
                Case 4
                    .Columns("sales_all_2017").Caption = "Actual Sales Q1 2018"
                    .Columns("sales_all_2018").Caption = "Actual Sales Q1 2019"
                    .Columns("forecast_sales").Caption = "Forecast Sales Q1 2019"
                    .Columns("rewardpayment").Caption = "Forecast Reward Q1 2018"
                    .Columns("sales_ex_2017").Caption = "Non NWCR Sales Q1 2018"
                    .Columns("sales_ex_2018").Caption = "Non NWCR Sales Q1 2019"
                    .Columns("sales_ex_2017").Caption = "Actual Sales Q1 2019"
                    .Columns("sales_ex_2018").Caption = "Actual Sales Q1 2019"
                    .Columns("sales_nw_2018").Caption = "NWCR Sales Q1 2019"
                    .Columns("forecast_growth").Caption = "Non NWCR Growth Q1 2019"
            End Select






        End With

        If Session("ShowBanding") = False Then
            btnViewHide.Text = "Show Payment Bands"
            gridDealerBonus.SettingsPager.PageSize = 18
            divPayments.Visible = False
        Else
            btnViewHide.Text = "Hide Payment Bands"
            gridDealerBonus.SettingsPager.PageSize = 8
            divPayments.Visible = True
        End If

        gridDealerBonus.DataBind()
        gridBonusBandings.DataBind()
        gridPaymentScale.DataBind()


    End Sub

    Protected Sub ExpGridDealer_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)

        If e.Value Is Nothing Then
        Else
            If Right(RTrim(e.Column.Name), 6) = "growth" Then

                If e.Value > 0 Then
                    e.BrickStyle.BackColor = Color.Green
                    e.BrickStyle.ForeColor = Color.White
                End If
                If e.Value = 0 Then
                    e.BrickStyle.BackColor = Color.White
                    e.BrickStyle.ForeColor = Color.White
                End If
                If e.Value < 0 Then
                    e.BrickStyle.BackColor = Color.Red
                    e.BrickStyle.ForeColor = Color.White
                End If
            End If
        End If
    End Sub


    Protected Sub btnExcel_Click()
        Dim sFileName As String = "BonusReport18_19"

        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})


    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub GridStylesNo(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub
    Protected Sub dsDealerBonus_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDealerBonus.Init  ' And all the others but dont need to name them explicitly
        dsDealerBonus.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsQuarterList.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusBanding.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusPayments.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsQuarters.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Private Sub cboQuarters_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboQuarters.SelectedIndexChanged
        Session("CurrentQuarter") = cboQuarters.Value
    End Sub

    Protected Sub CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridDealerBonus.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nperformance_last = 0
            nperformance_this = 0

            nEX_actual_2017 = 0
            nEX_forecast_2018 = 0

            nALL_actual_2017 = 0

        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nperformance_last = nperformance_last + e.GetValue("sales_ex_2017")
            nperformance_this = nperformance_this + e.GetValue("sales_ex_2018")

            nALL_actual_2017 = nALL_actual_2017 + e.GetValue("forecast_sales")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "growth_ex" Then
                If nperformance_last > 0 And nperformance_this > 0 Then
                    e.TotalValue = ((nperformance_this / nperformance_last) - 1)
                End If
            End If

            If objSummaryItem.FieldName = "forecast_growth" Then
                If nEX_actual_2017 > 0 And nEX_forecast_2018 > 0 Then
                    e.TotalValue = ((nEX_forecast_2018 / nEX_actual_2017) - 1)
                End If
            End If

        End If

    End Sub

    Private Sub gridDealerBonus_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridDealerBonus.HtmlDataCellPrepared
        If Right(RTrim(e.DataColumn.Name), 6) = "growth" Then

            e.Cell.ForeColor = Color.White

            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If

            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            End If

            If e.CellValue = 0 Then
                e.Cell.BackColor = Color.White
            End If
        End If
    End Sub

    Private Sub btnViewHide_Click(sender As Object, e As EventArgs) Handles btnViewHide.Click
        If Session("ShowBanding") = False Then
            Session("ShowBanding") = True
            Exit Sub
        End If
        If Session("ShowBanding") = True Then
            Session("ShowBanding") = False
        End If
    End Sub
End Class
