﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NationwideAccountSearch.aspx.vb" Inherits="NationwideAccountSearch" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">





     <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div style="float: left; width: 25%">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Nationwide Account Search" />
        </div>

       <div style="float: right; width: 75%">
                       <dx:ASPxCheckBox
                id="chkSpend"
                Font-Size="Small"
                runat="server"
                AutoPostBack="true"
                Text="Only Show Accounts With Sales ?"
                Checked="true"/>
        </div>


 
 
 
 <br />
        <br />


        <dx:ASPxGridView  
        ID="gridPossibles" 
        CssClass="grid_styles"
        OnLoad="GridStyles" 
        style="position:relative;"
        runat="server" 
        keyfield="customerdealerid"
        AutoGenerateColumns="False"
        DataSourceID="dsPossibles" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Styles-Footer-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="4%" 
                exportwidth="100"
                Caption="Code" 
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                CellStyle-Wrap="False"
                Width="9%" 
                exportwidth="250"
                Caption="Dealer Name" 
                FieldName="dealername">
            </dx:GridViewDataTextColumn>
                
            <dx:GridViewDataTextColumn 
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Width="5%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="A/C Code" 
                FieldName="link">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Width="15%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-Wrap="False"
                fieldname="businessname"
                Caption="A/C Name" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="5" 
                Visible="false"
                Settings-AllowSort="True"
                CellStyle-Wrap="False"
                Width="20%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="left"
                HeaderStyle-HorizontalAlign="left"
                Caption="Address" 
                FieldName="fulladdress">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="6" 
                Settings-AllowSort="True"
                Width="5%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Postcode" 
                FieldName="postcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="7" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                HeaderStyle-Wrap="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Sales Last 12mth" 
                FieldName="spendlast12">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0 "/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataDateColumn 
                VisibleIndex="8" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="First Spend" 
                FieldName="firstspend">
            </dx:GridViewDataDateColumn>

            <dx:GridViewDataDateColumn 
                VisibleIndex="9" 
                Settings-AllowSort="True"
                Width="6%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Latest Spend" 
                FieldName="latestspend">
            </dx:GridViewDataDateColumn>

            <dx:GridViewBandColumn Caption ="Assigned Nationwide Site" HeaderStyle-HorizontalAlign="Center" VisibleIndex="10">
                <Columns>

                   <dx:GridViewDataDateColumn 
                VisibleIndex="11" 
                Settings-AllowSort="True"
                Width="7%" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Caption="Site ID" 
                FieldName="siteid">
            </dx:GridViewDataDateColumn>

                    <dx:GridViewDataDateColumn 
                VisibleIndex="12" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="12%" 
                Caption="Site Name" 
                FieldName="sitename">
            </dx:GridViewDataDateColumn>

              <dx:GridViewDataDateColumn 
                 VisibleIndex="13" 
                 Settings-AllowSort="True"
                 Width="5%" 
                 exportwidth="150"
                 CellStyle-HorizontalAlign="Center"
                 HeaderStyle-HorizontalAlign="Center"
                 Caption="Postcode" 
                 FieldName="sitepostcode">
               </dx:GridViewDataDateColumn>

               <dx:GridViewDataHyperLinkColumn 
                    FieldName="customerdealerid" 
                    Name="customerdealerid"
                    ReadOnly="True" 
                    Width="4%"
                    CellStyle-HorizontalAlign  ="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ToolTip="Assign this Account to A Nationwide Site, or remove from search screen permanently"
                    Caption="Assign"
                    VisibleIndex="14">
                    <PropertiesHyperLinkEdit  NavigateUrlFormatString="Nationwideassign.aspx?{0}" Target="_blank" ImageUrl="Images2014/search.png"  ImageHeight="15px"  ImageWidth="20px"/>
                </dx:GridViewDataHyperLinkColumn>

                </Columns>
            </dx:GridViewBandColumn>

         </Columns>
                    
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsPossibles" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_NationwidePossibleLink" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="nSpendOnly_Flag" SessionField="SpendOnly_Flag" Type = "Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
   
    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="gridPossibles"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

