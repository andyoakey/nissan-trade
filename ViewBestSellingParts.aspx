﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewBestSellingParts.aspx.vb" Inherits="ViewBestSellingParts" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
               <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Best Selling Parts" />
        </div>

        <table id="trSelectionStuff" style="position: relative; margin-bottom: 10px;">
    
            <tr runat="server" >
                <td style="width:17%;" align="left" >
                        <dx:ASPxLabel 
        id="lblProductGroup"
        runat="server" 
        Text="Product Group">
    </dx:ASPxLabel>
                </td>
         
                <td style="width:17%;" align="left" >
                        <dx:ASPxLabel 
        id="lblType"
        runat="server" 
        Text="Type of Parts">
    </dx:ASPxLabel>
                </td>


                <td style="width:17%;" align="left" >
                        <dx:ASPxLabel 
        id="ASPxLabel1"
        runat="server" 
        Text="Period">
    </dx:ASPxLabel>
                </td>
                
                <td style="width:17%;" align="left" >
                        <dx:ASPxLabel 
        id="ASPxLabel2"
        runat="server" 
        Text="Number of Items">
    </dx:ASPxLabel>
                 </td>
           
                <td style="width:17%;" align="left" >
                        <dx:ASPxLabel 
        id="ASPxLabel3"
        runat="server" 
        Text="Basis">
    </dx:ASPxLabel>
                </td>
                
                <td style="width:15%;" align="left" >
                        <dx:ASPxLabel 
        id="ASPxLabel4"
        runat="server" 
        Text="Customers">
    </dx:ASPxLabel>
               </td>
            </tr>

            <tr id="Tr6" runat="server" style="height:30px;">
                <td style="width:17%" align="left" >
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlProductGroup"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All" Value="ALL" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="ACC"/>
                            <dx:ListEditItem Text="Damage" Value="DAM"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="EXM"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="MEC"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="ROM"/>
                            <dx:ListEditItem Text="Additional Product" Value="OTH"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:17%" align="left" >
                    <dx:ASPxComboBox
                        ID="ddlTA"
                        ValueField="TradeAnalysis"
                        TextField="TradeAnalysis"
                        runat="server" 
                        DataSourceID="dsTradeAnalysis"
                        SelectedIndex="0"
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                </td>


                <td style="width:17%" align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlPeriod"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Month to Date" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Year to Date" Value="1"/>
                            <dx:ListEditItem Text="Last 12 Months" Value="2"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:17%" align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlTopN"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Top 50" Value="0" />
                            <dx:ListEditItem Text="Top 100" Value="1" Selected="true"/>
                            <dx:ListEditItem Text="Top 500" Value="2"/>
                            <dx:ListEditItem Text="Top 1000" Value="3"/>
                            <dx:ListEditItem Text="Top 2500" Value="4"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:17%" align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlBasis"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Units Sold" Value="0" />
                            <dx:ListEditItem Text="Sales Value" Value="1" Selected="true"/>
                            <dx:ListEditItem Text="Cost Value" Value="2"/>
                            <dx:ListEditItem Text="Profit (£)" Value="3"/>
                         </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:15%" align="left">
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Focus only" Value="1" />
                            <dx:ListEditItem Text="Excluding Focus" Value="2"/>
                         </Items>
                    </dx:ASPxComboBox>
 
                </td>
            </tr>

        </table> 
            
        <dx:ASPxGridView 
            ID="gridBestParts" 
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText"
            OnLoad="GridStyles"
            runat="server"                                                                                                                                                                                                                                             
            AutoGenerateColumns="False"
            DataSourceID="dsBestParts" 
            Visible="True" 
            Width="100%">
            
            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="20" />

            <Settings 
                UseFixedTableLayout="true"
                ShowFilterRowMenu="false" 
                ShowFooter="false" 
                ShowGroupedColumns="false"
                ShowHeaderFilterButton="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False"  />
                
            <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" /> 

            <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Rank" 
                    CellStyle-HorizontalAlign= "Right"
                    HeaderStyle-HorizontalAlign="Right"
                    readonly ="true" 
                    FieldName="Ranking" 
                    VisibleIndex="0"
                    Width="5%" ExportWidth="75">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="Trade Analysis" 
                    readonly ="true" 
                    FieldName="TradeAnalysis" 
                    VisibleIndex="1"
                    Width="12%" ExportWidth="150">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Left" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Product Group" 
                    readonly ="true" 
                    FieldName="productgroup" 
                    VisibleIndex="2"
                    Width="12%" ExportWidth="150">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Left" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Part Number" 
                    readonly ="true" 
                    FieldName="PartNumber" 
                    VisibleIndex="3"
                    Width="10%" ExportWidth="100">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center" >
                    </CellStyle>
                   </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                     Caption="Part Description" 
                     readonly ="true" 
                     FieldName="PartDescription" 
                     VisibleIndex="4" 
                     Width="15%" ExportWidth="200">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Left" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                     Caption="Desc.Code" 
                     FieldName="PFC" 
                     VisibleIndex="5" 
                     ToolTip="Description Code "
                     Width="10%" ExportWidth="75">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Center" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                     Caption="Desc.Code Description" 
                     readonly ="true" 
                     FieldName="PFCDescription" 
                     VisibleIndex="6" 
                     ToolTip="Description Code Text"
                     Width="15%" ExportWidth="150">
                    <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                    <CellStyle HorizontalAlign="Left" >
                    </CellStyle>
                </dx:GridViewDataTextColumn>  

                <dx:GridViewDataTextColumn 
                     Caption="Units Sold" 
                     CellStyle-HorizontalAlign= "Right"
                     HeaderStyle-HorizontalAlign="Right"
                     readonly ="true" 
                     FieldName="Quantity" 
                     Width="6%" 
                     VisibleIndex="7" ExportWidth="100">
                     <PropertiesTextEdit DisplayFormatString="##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Sales Value" 
                     CellStyle-HorizontalAlign= "Right"
                     HeaderStyle-HorizontalAlign="Right"
                     readonly ="true" 
                     Width="8%" 
                     FieldName="SaleValue" 
                     VisibleIndex="8" ExportWidth="100">
                     <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Cost Value" 
                     CellStyle-HorizontalAlign= "Right"
                     HeaderStyle-HorizontalAlign="Right"
                     readonly ="true" 
                     Width="8%" 
                     FieldName="CostValue" 
                     VisibleIndex="9" ExportWidth="100">
                     <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Margin (%)" 
                     CellStyle-HorizontalAlign= "Right"
                     HeaderStyle-HorizontalAlign="Right"
                     readonly ="true" 
                     Width="8%" 
                     FieldName="Margin" 
                     VisibleIndex="10" ExportWidth="100">
                     <PropertiesTextEdit DisplayFormatString="0.00">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

            </Columns>

        </dx:ASPxGridView>
 
    </div>

    <asp:SqlDataSource ID="dsBestParts" runat="server" SelectCommand="p_BestSellingPartsNissan" SelectCommandType="StoredProcedure" ConnectionString="databaseconnectionstring">
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sLevel4_Code" SessionField="Level4_Code" Type="String" Size="3"  />
            <asp:SessionParameter Name="sTradeAnalysis" SessionField="TA" Type="String" Size="50"  />
            <asp:SessionParameter Name="nPeriod" SessionField="Period" Type="Int32" />
            <asp:SessionParameter Name="nItems" SessionField="Items" Type="Int32" />
            <asp:SessionParameter Name="nBasis" SessionField="Basis" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFocus" SessionField="Focus" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="dsTradeAnalysis" runat="server" SelectCommand="sp_TradeAnalysisProducts" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>


    <dx:ASPxGridViewExporter 
        ID="ExpBestParts"
        runat="server" 
        GridViewID="gridBestParts">
    </dx:ASPxGridViewExporter>

</asp:Content>

