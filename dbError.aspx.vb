﻿Imports System.Diagnostics
Imports System.Runtime.CompilerServices
Imports System.Net.Mail
Imports System.Data

Partial Class dbError

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sIPAddress As String = ""
        Dim sStackTrace As String = "" & Session("StackTrace")
        sIPAddress = GetIPAddress()

        lblErrorMessage.Text = "Error Message = [" & Trim(Session("ErrorToDisplay")) & "]"

        If Not Session("CallingModule") Is Nothing Then
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "Source Program= [" & Session("CallingModule")
        End If

        Dim sErrorPosition As String = Mid(sStackTrace, sStackTrace.LastIndexOf(":") + 2, 50)
        lblErrorMessage.Text = lblErrorMessage.Text & " - " & sErrorPosition & "]"

        If Not Session("SelectionLevel") Is Nothing Then
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "SelectionLevel= [" & Session("SelectionLevel") & "]"
        Else
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "SelectionLevel is nothing"
        End If

        If Not Session("SelectionId") Is Nothing Then
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "SelectionId = [" & Trim(Session("SelectionId")) & "]"
        Else
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "SelectionId is nothing"
        End If

        If sIPAddress <> "" Then
            lblErrorMessage.Text = lblErrorMessage.Text & vbCrLf & "IP Address = [" & sIPAddress & "]"
        End If


        If Microsoft.VisualBasic.Left(lblErrorMessage.Text, 5) <> "Thread" Then
            Call SendDevelopersAnEmail(lblErrorMessage.Text)
        End If

    End Sub

    Sub SendDevelopersAnEmail(sErrorMessage As String)

        Dim MailObj As New SmtpClient
        Dim mm As New MailMessage
        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")
        basicAuthenticationInfo.UserName = "support@toyotavaluechain.net"
        basicAuthenticationInfo.Password = "4AhK56DmpGvn"
        MailObj.Host = "mail.toyotavaluechain.net"
        MailObj.UseDefaultCredentials = False
        MailObj.Credentials = basicAuthenticationInfo

        'mm.To.Add("developers@qubedata.net")
        mm.To.Add("dick@qubedata.net")
        mm.From = New MailAddress("nissantradehascrashed@qubedata.net")
        mm.Subject = "Qube - Nissan Trade Site - Crash!"
        If Not Session("UserId") Is Nothing Then
            mm.Body &= sErrorMessage & vbCrLf & vbCrLf & "User Id : [" & Session("UserId") & "]"
        Else
            mm.Body &= lblErrorMessage.Text & vbCrLf
        End If
        MailObj.Send(mm)

    End Sub

    Private Function GetIPAddress() As String
        Dim strIpAddress As String
        strIpAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If strIpAddress = "" Then
            strIpAddress = Request.ServerVariables("REMOTE_ADDR")
        End If
        Return strIpAddress
    End Function

End Class