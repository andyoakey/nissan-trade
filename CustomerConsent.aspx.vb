﻿Imports System.Data
Imports DevExpress.Export
Imports DevExpress.Web.Data
Imports DevExpress.XtraPrinting
Imports DevExpress.Web.ASPxGridView

Partial Class CustomerConsent

    Inherits System.Web.UI.Page

    Private Const GridCustomPageSizeName As String = "gridCustomPageSize"

    Dim nBand1 As Integer
    Dim nBand2 As Integer

    Dim dsCompanies As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Dim i As Integer
        Dim sSelectedPostCode As String = ""
        Dim nSelectedCustomerType As Integer = 0

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim master_btnBack As DevExpress.Web.ASPxButton
        master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
        master_btnBack.Visible = True

        Me.Title = "Nissan Trade Website - View all Customers"
        Session("CustomerCalledFrom") = "ViewCompanies"

        Session("CallingModule") = "ViewCompanies.aspx - LoadComplete"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        gridCompanies.SettingsPager.AllButton.Visible = (Session("SelectionLevel") <> "N")

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer List", "Viewing Customer List")
            Call ResetFilters()
        End If

        If Session("SelectionLevel") <> "C" And isQubeUser(Session("UserEmailAddress")) = False Then
            lblCentreError.Visible = vbTrue
            divmain.Visible = vbFalse
            master_btnExcel.Visible = vbFalse
        Else
            lblCentreError.Visible = vbFalse
            divmain.Visible = vbTrue
            master_btnExcel.Visible = vbTrue
        End If

        If Session("SelectionLevel") = "C" Then
            gridCompanies.Columns("bndDealer").Visible = False
        Else
            gridCompanies.Columns("bndDealer").Visible = True
        End If

        If IsPostBack = False Then
            ' _______________________________________________________________
            '  Move the contents of the filters set up in ViewCompanies.aspx.vb to this page
            Dim sBT As String = ""
            txtFiltBusinessName.Text = Session("C_BusinessName")
            txtFiltPostCode.Text = Session("C_Postcode")
            sBT = Session("C_BusinessTypeString")
            chkFilterCustType.Items(0).Selected = Session("C_Active")
            chkFilterCustType.Items(1).Selected = Session("C_InActive")
            chkFilterCustType.Items(2).Selected = Session("C_Lapsed")
            chkFilterCustType.Items(3).Selected = Session("C_Prospect")
            chkFilterCustType.Items(4).Selected = Session("C_NewProspect")
            If sBT Like ("'1'") Then
                chkFilterBusType.Items(0).Selected = True
            End If
            If sBT Like ("'2'") Then
                chkFilterBusType.Items(1).Selected = True
            End If
            If sBT Like ("'3'") Then
                chkFilterBusType.Items(2).Selected = True
            End If
            If sBT Like ("'4'") Then
                chkFilterBusType.Items(3).Selected = True
            End If
            If sBT Like ("'5'") Then
                chkFilterBusType.Items(4).Selected = True
            End If
            If sBT Like ("'6'") Then
                chkFilterBusType.Items(5).Selected = True
            End If
            If sBT Like ("'7'") Then
                chkFilterBusType.Items(6).Selected = True
            End If
            If sBT Like ("'8'") Then
                chkFilterBusType.Items(7).Selected = True
            End If
            If sBT Like ("'9'") Then
                chkFilterBusType.Items(8).Selected = True
            End If
            If sBT Like ("'99'") Then
                chkFilterBusType.Items(9).Selected = True
            End If
            '  End
            ' _______________________________________________________________
        End If

        If Request.QueryString.Count > 0 Then
            sSelectedPostCode = Request.QueryString(0)
            txtFiltPostCode.Text = sSelectedPostCode
        End If

        If Request.QueryString.Count = 2 Then
            For i = 0 To chkFilterCustType.Items.Count - 1
                chkFilterCustType.Items(i).Selected = False
            Next
            For i = 0 To chkFilterBusType.Items.Count - 1
                chkFilterBusType.Items(i).Selected = True
            Next
            chkFilterBusType.Items(5).Selected = False
            chkFilterBusType.Items(6).Selected = False
            Select Case Request.QueryString(1)
                Case 1  '/ Known
                    chkFilterCustType.Items(0).Selected = True
                    chkFilterCustType.Items(1).Selected = True
                    chkFilterCustType.Items(2).Selected = True
                    chkFilterCustType.Items(3).Selected = True
                Case 2  '/ Active
                    chkFilterCustType.Items(0).Selected = True
                Case 3  '/ Inactive
                    chkFilterCustType.Items(1).Selected = True
                Case 4  '/ Lapsed
                    chkFilterCustType.Items(2).Selected = True
                Case 5 '/ Prospect
                    chkFilterCustType.Items(3).Selected = True
                Case 6 '/ New Prospect
                    chkFilterCustType.Items(4).Selected = True
                Case Else
            End Select

        End If

        ' Add in the Tooltips
        With gridCompanies
            .Columns("lastupdatedby").ToolTip = "Indicates who made the most recent change to this Information.  Where this is shown as 'Qube' this indicates this was how the account was initially set up."
            .Columns("activefrom").ToolTip = "The date of the latest change made to this Information"
            .Columns("nocontact_flag").ToolTip = "If ticked, this customer may NOT be contacted for marketing purposes in ANY way. "
            .Columns("postallowed_flag").ToolTip = "If ticked this customer MAY be contacted by Post. To enable this option you must ensure the No Contact box is unticked first."
            .Columns("emailallowed_flag").ToolTip = "If ticked this customer MAY be contacted by Email. To enable this option you must ensure the No Contact box is unticked first."
            .Columns("phoneallowed_flag").ToolTip = "If ticked this customer MAY be contacted by Telephone. To enable this option you must ensure the No Contact box is unticked first."
            .Columns("smsallowed_flag").ToolTip = "If ticked this customer MAY be contacted by SMS/Text. To enable this option you must ensure the No Contact box is unticked first."
        End With


        Call BindGrid()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Session("BackClicked") = True Then
            Session("BackClicked") = False
            Response.Redirect("ViewCompanies.aspx")
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisValue As Decimal = 0
        Dim nDistance As VariantType
        Dim bNull As Boolean = False

        Call GlobalRenderBrick(e)

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            e.Url = ""
            e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)

            Try
                If Not IsDBNull(e.GetValue("BusinessURN")) Then
                    If Trim(e.GetValue("BusinessURN")) <> "" Then
                        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Bold)
                    End If
                End If
            Catch ex As Exception
            End Try

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Name = "DriveTime" Or e.Column.Name = "Distance" Then

                If IsDBNull(e.GetValue("Distance")) Then
                    nDistance = 0
                    bNull = True
                Else
                    nDistance = Convert.ToInt32(Math.Floor(e.GetValue("Distance")))
                End If

                If Not bNull Then
                    If nDistance < nBand1 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.YellowGreen
                    ElseIf nDistance < nBand2 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.Gold
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Tomato
                        e.BrickStyle.ForeColor = System.Drawing.Color.White
                    End If
                End If
            End If

        End If

    End Sub

    Private Function HiddenCount() As Integer

        Dim da As New DatabaseAccess
        Dim ds As DataSet
        Dim sErr As String = ""
        Dim htIn As New Hashtable

        htIn.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn.Add("@sSelectionId", Session("SelectionId"))
        ds = da.Read(sErr, "p_CustomerHiddenCount", htIn)

        HiddenCount = 0
        If sErr.Length = 0 Then
            HiddenCount = ds.Tables(0).Rows(0).Item("HiddenCount")
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Function

    Protected Sub btnExcel_Click()


        Dim nCols As Integer
        Dim nRows As Integer
        Dim sFileName As String = ""

        gridCompanies.DataBind()

        nCols = gridCompanies.Columns.Count - 1
        nRows = gridCompanies.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid Is too large To export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            gridCompanies.Columns("bndAddress").Visible = True
            Dim exportOptions = New XlsxExportOptionsEx()
            AddHandler exportOptions.CustomizeCell, AddressOf exportOptions_CustomizeCell

            ASPxGridViewExporter1.WriteXlsxToResponse(exportOptions)
            'ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.DataAware})
            gridCompanies.Columns("bndAddress").Visible = False

        End If
    End Sub

    Protected Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        PopFilter.ShowOnPageLoad = True
    End Sub

    Private Sub ResetFilters()

        Dim i As Integer

        Session("CFBusinessName") = ""
        Session("CFPostCode") = ""
        Session("CFBusinessType") = ""
        Session("CFActive") = 1
        Session("CFInActive") = 0
        Session("CFLapsed") = 0
        Session("CFProspect") = 0
        Session("CFNewProspect") = 0
        Session("CFCustomerType") = 0
        Session("CFShowAddress") = 0
        Session("CFShowHidden") = 0
        Session("FocusType") = 0

        txtFiltBusinessName.Text = ""
        txtFiltPostCode.Text = ""
        chkFilterCustType.Items(0).Selected = True
        chkFilterCustType.Items(1).Selected = False
        chkFilterCustType.Items(2).Selected = False
        chkFilterCustType.Items(3).Selected = False
        chkFilterCustType.Items(4).Selected = False

        For i = 0 To 9
            chkFilterBusType.Items(i).Selected = False
        Next

        ddlType.SelectedIndex = 0
        chkShowHidden.Checked = False

        gridCompanies.FilterEnabled = False
        gridCompanies.FilterExpression = ""

    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Response.Redirect("CustomerConsent.aspx")
    End Sub

    Private Sub BindGrid()

        Dim i As Integer

        Dim sBT As String = ""

        If IsPostBack = True Then
            sBT = ""
            For i = 0 To 9
                If chkFilterBusType.Items(i).Selected Then
                    sBT += "'" & chkFilterBusType.Items(i).Value & "',"
                End If
            Next
            sBT = Trim(sBT)
            If Right(sBT, 1) = "," Then
                sBT = Left(sBT, Len(sBT) - 1)
            End If
        Else
            sBT = Session("C_BusinessTypeString")
        End If

        Dim htin As New Hashtable
        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@sBusinessName", Trim(txtFiltBusinessName.Text))
        htin.Add("@sPostCode", Trim(txtFiltPostCode.Text))
        htin.Add("@sBusinessTypeString", sBT)
        htin.Add("@nActive", IIf(chkFilterCustType.Items(0).Selected, 1, 0))
        htin.Add("@nInActive", IIf(chkFilterCustType.Items(1).Selected, 1, 0))
        htin.Add("@nLapsed", IIf(chkFilterCustType.Items(2).Selected, 1, 0))
        htin.Add("@nProspect", IIf(chkFilterCustType.Items(3).Selected, 1, 0))
        htin.Add("@nNewProspect", IIf(chkFilterCustType.Items(4).Selected, 1, 0))
        htin.Add("@nFocus", Session("FocusType"))
        dsCompanies = da.Read(sErrorMessage, "p_CustomerListConsent", htin)

        Session("C_BusinessName") = Trim(txtFiltBusinessName.Text)
        Session("C_Postcode") = Trim(txtFiltPostCode.Text)
        Session("C_BusinessTypeString") = sBT
        Session("C_Active") = IIf(chkFilterCustType.Items(0).Selected, 1, 0)
        Session("C_InActive") = IIf(chkFilterCustType.Items(1).Selected, 1, 0)
        Session("C_Lapsed") = IIf(chkFilterCustType.Items(2).Selected, 1, 0)
        Session("C_Prospect") = IIf(chkFilterCustType.Items(3).Selected, 1, 0)
        Session("C_NewProspect") = IIf(chkFilterCustType.Items(4).Selected, 1, 0)

        gridCompanies.DataSource = dsCompanies.Tables(0)
        gridCompanies.DataBind()

        If Not Session("CompanyReturnKeyValue") Is Nothing Then
            gridCompanies.MakeRowVisible(Session("CompanyReturnKeyValue"))
            Session("CompanyReturnKeyValue") = Nothing
        End If

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        '/ The Page_Load_Complete will refresh the grid
        PopFilter.ShowOnPageLoad = False
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Private Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        Session("FocusType") = ddlType.Value
    End Sub

    Private Sub gridCompanies_BatchUpdate(sender As Object, e As ASPxDataBatchUpdateEventArgs) Handles gridCompanies.BatchUpdate

        Dim nLoopCount As Integer = e.UpdateValues.Count()
        Dim nLoop As Integer = 1
        Dim nThisKey As Integer = 0
        Dim nThisNoContact_Flag As Integer = 0
        Dim nThisPostAllowed_Flag As Integer = 0
        Dim nThisEmailAllowed_Flag As Integer = 0
        Dim nThisPhoneAllowed_Flag As Integer = 0
        Dim nThisSMSAllowed_Flag As Integer = 0
        Dim sSql As String = " "

        ' Loop Through every updated record 

        For nLoop = 0 To nLoopCount - 1
            nThisKey = e.UpdateValues(nLoop).Keys(0)
            nThisNoContact_Flag = e.UpdateValues(nLoop).NewValues("nocontact_flag")
            nThisPostAllowed_Flag = e.UpdateValues(nLoop).NewValues("postallowed_flag")
            nThisEmailAllowed_Flag = e.UpdateValues(nLoop).NewValues("emailallowed_flag")
            nThisPhoneAllowed_Flag = e.UpdateValues(nLoop).NewValues("phoneallowed_flag")
            nThisSMSAllowed_Flag = e.UpdateValues(nLoop).NewValues("smsallowed_flag")

            ' If no boxes are ticked show no_contact as True  ( this covers situation where nothing is ticked)
            If nThisPostAllowed_Flag + nThisEmailAllowed_Flag + nThisPhoneAllowed_Flag + nThisSMSAllowed_Flag = 0 Then
                nThisNoContact_Flag = 1
            End If

            ' If no_contact is ticked then all other boxes must be unticked
            If nThisNoContact_Flag = 1 Then
                nThisPostAllowed_Flag = 0
                nThisEmailAllowed_Flag = 0
                nThisPhoneAllowed_Flag = 0
                nThisSMSAllowed_Flag = 0
            End If

            sSql = "Exec sp_UpdateCustomerDealerConsent " & nThisKey & "," & nThisNoContact_Flag & "," & nThisPostAllowed_Flag & "," & nThisEmailAllowed_Flag & "," & nThisPhoneAllowed_Flag & "," & nThisSMSAllowed_Flag & "," & Session("UserID")

            Call RunNonQueryCommand(sSql)
        Next

        e.Handled = True
        gridCompanies.CancelEdit()

    End Sub

    Private Sub ASPxGridViewExporter1_Disposed(sender As Object, e As EventArgs) Handles ASPxGridViewExporter1.Disposed
    End Sub


    Private Sub exportOptions_CustomizeCell(ByVal e As DevExpress.Export.CustomizeCellEventArgs)

        If Right(Trim(e.ColumnFieldName), 5) = "_flag" Then
            If e.Value = 1 Then
                e.Value = "Yes"
            Else
                e.Value = " - "
            End If
        End If

        e.Handled = True

    End Sub

End Class
