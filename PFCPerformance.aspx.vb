﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class PFCPerformance2012

    Inherits System.Web.UI.Page

    Dim nSale1 As Double
    Dim nCost1 As Double
    Dim nSale2 As Double
    Dim nCost2 As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Product Type Performance Report"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "PFC Performance Report", "PFC Performance Report")
            Session("PFCPerfYear") = GetDataDecimal("SELECT YEAR(MAX(invoicedate))-1 FROM web_qube")   ' Get last complete year  - this is the default
            ddlYear.Items.Add(Session("PFCPerfYear") + 1)
            ddlYear.Items.Add(Session("PFCPerfYear"))
            ddlYear.Items.Add(Session("PFCPerfYear") - 1)
            ddlYear.Items.Add(Session("PFCPerfYear") - 2)
            ddlYear.Items.Add(Session("PFCPerfYear") - 3)
            ddlYear.SelectedIndex = 0
        End If

    End Sub

    Protected Sub dsPFCPerf_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsPFCPerf.Init
        dsPFCPerf.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridPFCPerf_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridPFCPerf.HtmlDataCellPrepared

        Dim nThisValue As Decimal

        If e.DataColumn.Name = "Units_Diff" Or e.DataColumn.Name = "Value_Diff" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            If nThisValue > 0 Then
                e.Cell.ForeColor = System.Drawing.Color.White
                e.Cell.BackColor = System.Drawing.Color.Green
            ElseIf nThisValue < 0 Then
                e.Cell.ForeColor = System.Drawing.Color.White
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisValue As Decimal = 0

        Call GlobalRenderBrick(e)

        If Left(e.Column.Name, 5) = ("Units") Then
            e.TextValueFormatString = "#,##0"
        End If

        If Left(e.Column.Name, 5) = ("Value") Then
            e.TextValueFormatString = "£#,##0"
        End If

        If e.Column.Name = "Units_Diff" Or e.Column.Name = "Value_Diff" Then
            If IsDBNull(e.Value) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.Value)
            End If
            If nThisValue > 0 Then
                e.BrickStyle.BackColor = System.Drawing.Color.Green
            ElseIf nThisValue < 0 Then
                e.BrickStyle.BackColor = System.Drawing.Color.Red
            End If
            e.TextValueFormatString = "#,##0"
        End If

    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String
        sFileName = "DescCodePerformance_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.FileName = sFileName

        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub


    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("PFCPerfYear") = ddlYear.SelectedItem.Text
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
        Dim nYear As Integer = Val(ddlYear.SelectedItem.Text)
        gridPFCPerf.Columns("Units_1").Caption = "Units (" & Trim(Str(nYear - 1)) & ")"
        gridPFCPerf.Columns("Value_1").Caption = "Sales (" & Trim(Str(nYear - 1)) & ")"
        gridPFCPerf.Columns("Units_2").Caption = "Units (" & Trim(Str(nYear)) & ")"
        gridPFCPerf.Columns("Value_2").Caption = "Sales (" & Trim(Str(nYear)) & ")"
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilter.Click
        gridPFCPerf.FilterEnabled = False
        gridPFCPerf.FilterExpression = ""
        gridPFCPerf.SearchPanelFilter = " "
    End Sub

End Class
