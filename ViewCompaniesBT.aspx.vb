﻿Imports System.Data
Imports System.Net.Mail

Partial Class ViewCompaniesBT

    Inherits System.Web.UI.Page

    Dim nCustomerId As Long
    Dim nCurrentBT As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nCustomerId = Request.QueryString(0)

        If Not Page.IsPostBack Then
            nCurrentBT = Request.QueryString(1)

            If nCurrentBT = 0 Then nCurrentBT = 99

            Session("NewBT") = nCurrentBT
            ddlBT.Items.Add("BodyShop", 1)
            ddlBT.Items.Add("Breakdown Recovery Services", 2)
            ddlBT.Items.Add("Car Dealer New/Used", 3)
            ddlBT.Items.Add("Independent Motor Trader", 4)
            ddlBT.Items.Add("Mechanical Specialist", 5)
            ddlBT.Items.Add("Motor Factor", 6)
            ddlBT.Items.Add("Parts Supplier", 7)
            ddlBT.Items.Add("Taxi & Private Hire", 8)
            ddlBT.Items.Add("Windscreens", 9)
            ddlBT.Items.Add("Other", 99)
            ddlBT.SelectedIndex = nCurrentBT - 1
        End If

    End Sub

    Protected Sub btnBTSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBTSave.Click
        Dim sSQL As String = "Update Customer SET BusinessTypeId = " & Session("NewBT") & " WHERE Id = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sSQL)

        Dim sSQL2 As String = "Update CustomerDealer SET BusinessTypeId = " & Session("NewBT") & " WHERE customerid = " & Trim(Str(nCustomerId))
        Call RunNonQueryCommand(sSQL2)

        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "BusinessTypeId Updated", "Customer Id : " & nCustomerId & ", New BT : " & Session("NewBT"))
        Dim startUpScript As String = String.Format("window.parent.HideBTEdit();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

    Protected Sub ddlBT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBT.SelectedIndexChanged
        Session("NewBT") = ddlBT.SelectedIndex + 1
    End Sub

End Class
