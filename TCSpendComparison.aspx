﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TCSpendComparison.aspx.vb" Inherits="TCSpendComparison" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
    
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
    
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans';
        left: 0px; width: 976px; text-align: left;">
        
        <div id="divTitle" style="position: relative; left: 5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Trade Club Spend Comparison By Month" Style="font-weight: 700;
                font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <table id="trSelectionStuff" width="976px">
            <tr id="Tr3" runat="server" style="height: 30px;">
                <td style="font-size: small; width:5%" align="left">
                    Year
                </td>
                <td style="font-size: small; width:5%" align="left">
                    <asp:DropDownList ID="ddlYear" runat="server"   AutoPostBack="true"  
                        Width="180px">
                    </asp:DropDownList>
                </td>
                <td style="font-size: small; width:15%" align="left">
                    Product Category
                </td>
                <td style="font-size: small; width:5%" align="left">
                    <asp:DropDownList ID="ddlTA" runat="server" AutoPostBack="True" 
                        Width="180px"   >
                        <asp:ListItem>All</asp:ListItem>
                        <asp:ListItem>Accessories</asp:ListItem>
                        <asp:ListItem>Body</asp:ListItem>
                        <asp:ListItem>Lighting</asp:ListItem>
                        <asp:ListItem>Mech Comp</asp:ListItem>
                        <asp:ListItem>Mechanical</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="right">
                </td>
            </tr>
        </table>

        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" AppearanceName="Chameleon" 
            ClientInstanceName="chart" Height="400px" PaletteName="Module" Width="976px">
        </dxchartsui:WebChartControl>
        <br />
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsSpendsByTA" runat="server" SelectCommand="p_TC_Spend_Comparison"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="TCYear" Type="Int32" Size="0" />
            <asp:SessionParameter Name="sTypeOfParts" SessionField="TradeAnalysis" Type="String" Size="50"  />
        </SelectParameters>
    </asp:SqlDataSource>
    
</asp:Content>

