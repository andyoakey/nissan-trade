﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="FocusMailing.aspx.vb" Inherits="FocusMailing" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
        </div>

        <div id="div" style="position:relative;">
            <table>
                <tr>
                    <td width ="50%">
                       <dx:ASPxLabel 
                        Font-Size="Larger"
                        ID="lblPageTitle" runat="server" 
                        Text="Create Focus Mailing" />
                   </td>

                    <td width ="50%" align="right">
                        <dx:ASPxLabel 
                            ID="lblNotes" 
                            runat="server" 
                            Font-Italic="true"
                            Text="The full set of fields needed for the mailing ( as requested by TBWA) will be in the Excel Extract." />
                   </td>
                </tr>
            </table>
            
        </div> 

         <br />


        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsFocus" 
        Styles-Header-HorizontalAlign="Left"
        Styles-CellStyle-HorizontalAlign="Left"
        Styles-Footer-HorizontalAlign="Left"
        Settings-HorizontalScrollBarMode="Auto"
        Styles-Cell-Wrap="False"
        Width="100%">

        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />

        <Styles Footer-HorizontalAlign ="Center"/>
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true"/>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                Visible="false"
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="50px" 
                exportwidth="50"
                Caption="ID" 
                FieldName="id"
                Name="id">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="200"
                Caption="Mailing URN" 
                FieldName="mailingurn"
                Name="mailingurn">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="200"
                Caption="Dealer Group" 
                FieldName="dealergroup"
                Name="dealergroup">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="true"
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="10%" 
                exportwidth="200"
                Caption="Dealer Code" 
                FieldName="dealercode"
                Name="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible="false"
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                exportwidth="200"
                Caption="Dealer Flyer Name" 
                FieldName="dealerflyername"
                Name="dealerflyername">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="5" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyeraddress1" 
                FieldName="dealerflyeraddress1"
                Name="dealerflyeraddress1">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="6" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyeraddress2" 
                FieldName="dealerflyeraddress2"
                Name="dealerflyeraddress2">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="7" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyeraddress3" 
                FieldName="dealerflyeraddress3"
                Name="dealerflyeraddress3">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="7" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyerposttown" 
                FieldName="dealerflyerposttown"
                Name="dealerflyerposttown">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="8" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyercounty" 
                FieldName="dealerflyercounty"
                Name="dealerflyercounty">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="8" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyerpostcode" 
                FieldName="dealerflyerpostcode"
                Name="dealerflyerpostcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="9" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyeremailaddress" 
                FieldName="dealerflyeremailaddress"
                Name="dealerflyeremailaddress">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="10" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyerphone" 
                FieldName="dealerflyerphone"
                Name="dealerflyerphone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="11" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyerfax" 
                FieldName="dealerflyerfax"
                Name="dealerflyerfax">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="12" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="dealerflyercontactname" 
                FieldName="dealerflyercontactname"
                Name="dealerflyercontactname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="13" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="contact_salutation" 
                FieldName="contact_salutation"
                Name="contact_salutation">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="true"
                VisibleIndex="14" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="Contact Name" 
                FieldName="contact_name"
                Name="contact_name">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="true"
                VisibleIndex="15" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="Contact Job Title" 
                FieldName="contact_jobtitle"
                Name="contact_jobtitle">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="16" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="contact_email" 
                FieldName="contact_email"
                Name="contact_email">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="true"
                VisibleIndex="16" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="20%" 
                exportwidth="200"
                Caption="Business Name" 
                FieldName="businessname"
                Name="businessname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="17" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="address1" 
                FieldName="address1"
                Name="address1">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="18" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="address2" 
                FieldName="address2"
                Name="address2">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="19" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="address3" 
                FieldName="address3"
                Name="address3">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="true"
                VisibleIndex="20" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="Post Town" 
                FieldName="posttown"
                Name="posttown">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="21" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="10%" 
                exportwidth="200"
                Caption="County" 
                FieldName="county"
                Name="county">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="true"
                VisibleIndex="22" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="10%" 
                exportwidth="200"
                Caption="Post Code" 
                FieldName="postcode"
                Name="postcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="23" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="refcode" 
                FieldName="refcode"
                Name="refcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="24" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="prospect_flag" 
                FieldName="prospect_flag"
                Name="prospect_flag">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="True"
                VisibleIndex="25" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="15%" 
                exportwidth="200"
                Caption="Business Type" 
                FieldName="businesstype"
                Name="businesstype">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="26" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="flyeropeninghours" 
                FieldName="flyeropeninghours"
                Name="flyeropeninghours">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="27" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="spendthisyear" 
                FieldName="spendthisyear"
                Name="spendthisyear">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="28" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="mailingstatus" 
                FieldName="mailingstatus"
                Name="mailingstatus">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                Visible ="false"
                VisibleIndex="29" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="200"
                Caption="mailingterritory" 
                FieldName="mailingterritory"
                Name="mailingterritory">
            </dx:GridViewDataTextColumn>

         </Columns>
   
                 
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsFocus" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_MakeMailingList_Focus" 
            SelectCommandType="StoredProcedure">
           </asp:SqlDataSource>
    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

