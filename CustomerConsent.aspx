<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerConsent.aspx.vb" Inherits="CustomerConsent" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <script type="text/javascript" language="javascript">

        function ViewCompaniesBT(contentUrl) {
            PopupBT.SetContentUrl(contentUrl);
            PopupBT.SetHeaderText('Select a Business Type');
            PopupBT.SetSize(400, 250);
            PopupBT.Show();
        }

        function ViewCompaniesFocusType(contentUrl) {
            PopupFT.SetContentUrl(contentUrl);
            PopupFT.SetHeaderText('Select a Focus Type');
            PopupFT.SetSize(400, 150);
            PopupFT.Show();
        }
     
        function ViewCompaniesEmail(contentUrl) {
            PopupEmail.SetContentUrl(contentUrl);
            PopupEmail.SetHeaderText('Email Address');
            PopupEmail.SetSize(400, 150);
            PopupEmail.Show();
        }

        function HideBTEdit() {
            PopupBT.Hide();
            gridCompanies.Refresh();
        }

        function HideFTEdit() {
            PopupFT.Hide();
            gridCompanies.Refresh();
        }

        function HideEmailEdit() {
            PopupEmail.Hide();
            gridCompanies.Refresh();
        }


        function ReAllocWindow(contentUrl) {
            PopupReAlloc.SetContentUrl(contentUrl);
            PopupReAlloc.SetHeaderText('Select a new customer record');
            PopupReAlloc.SetSize(850, 400);
            PopupReAlloc.Show();
        }
        function HideReAlloc() {
            PopupReAlloc.Hide();
            gridCompanies.Refresh();
        }
        function HideReAllocAndConf() {
            PopupReAlloc.Hide();
            gridCompanies.Refresh();
        }

        function OnBatchEditStartEditing(s, e) {
            ToggleButtons(true);
        }
        function OnBatchEditEndEditing(s, e) {
            window.setTimeout(function () {
                if (!s.batchEditApi.HasChanges())
                    ToggleButtons(false);
            }, 0);
        }
        function ToggleButtons(enabled) {
            btnUpdate.SetEnabled(enabled);
            btnCancel.SetEnabled(enabled);
        }
        function OnUpdateClick(s, e) {
            gridCompanies.UpdateEdit();
            ToggleButtons(false);
        }
        function OnCancelClick(s, e) {
            gridCompanies.CancelEdit();
            ToggleButtons(false);
        }
        function OnCustomButtonClick(s, e) {
            if (e.buttonID == "deleteButton") {
                s.DeleteRow(e.visibleIndex);
                ToggleButtons(true);
            }
        }
        function OnEndCallback(s, e) {
            window.setTimeout(function () {
                if (!s.batchEditApi.HasChanges())
                    ToggleButtons(false);
            }, 0);
        }
    </script>

    
 <dx:ASPxLabel 
    ForeColor="#cb2e55"
    ID="lblCentreError" 
    Visible="false"
    runat="server" 
    Font-Size="18px"
    Font-Names="Calibri,Verdana"
    Style="left: 550px; position: absolute; top: 300px"
    Text="The Customer Consent Screen is only available when a single Dealer is selected." />							

    
    
    <dx:ASPxPopupControl 
        ID="PopFilter" 
        ClientInstanceName="PopFilter" 
        Width="610px"
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText="Select filters and press 'Go'"
        Modal="False" 
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">

                <table id="trFilter" width="100%">
    
                    <tr>
                        <td align="left" valign="middle" style="width:55%" >
                            <dx:ASPxLabel 
                                ID="lblFilter1" 
                                runat ="server" 
                                Text="Business Name" />
                        </td>

                         <td align="left" valign="middle" >
                                <dx:ASPxLabel 
                                ID="lblFilter2" 
                                runat ="server" 
                                Text="Postcode" />
                          </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxTextBox 
                                ID="txtFiltBusinessName" 
                                runat="server" />
                        </td>

                         <td align="left" valign="middle" >
                            <dx:ASPxTextBox 
                                ID="txtFiltPostCode" 
                                runat="server" />
                        </td>
                       </tr>

                    <tr>
                        <td align="left" valign="middle">
                              <dx:ASPxLabel 
                                ID="lblFilter3" 
                                runat ="server" 
                                Text="Customer Type" />
                        </td>
                         <td align="left" valign="middle" >
                              <dx:ASPxLabel 
                                ID="lblFilter4" 
                                runat ="server" 
                                Text="Business Type" />
                        </td>
                    </tr>

                    <tr>
                    <td>

                    </td>
                    </tr>

                    <tr>
                        <td align="left" valign="top">
                            <dx:ASPxCheckBoxList
                                id ="chkFilterCustType" 
                                runat="server">
                               <Items>
                                   <dx:ListEditItem Text="Active" Value="0"/>
                                   <dx:ListEditItem Text="In-Active" Value="1"/>
                                   <dx:ListEditItem Text="Lapsed" Value="2"/>
                                   <dx:ListEditItem Text="Prospect" Value="3"/>
                                   <dx:ListEditItem Text="New Prospect" Value="4"/>
                               </Items>
                            </dx:ASPxCheckBoxList>
                        </td>
                         <td align="left" valign="top" >
                            <dx:ASPxCheckBoxList
                                id ="chkFilterBusType" 
                                runat="server">
                               <Items>
                                    <dx:ListEditItem Text="Bodyshop" Value="1"/>
                                    <dx:ListEditItem Text="Breakdown Recovery Services" Value="2"/>
                                    <dx:ListEditItem Text="Car Dealer New/Used" Value="3" />
                                    <dx:ListEditItem Text="Independent Motor Trader" Value="4" />
                                    <dx:ListEditItem Text="Mechanical Specialist" Value="5" />
                                    <dx:ListEditItem Text="Motor Factor" Value="6" />
                                    <dx:ListEditItem Text="Parts Supplier" Value="7" />
                                    <dx:ListEditItem Text="Taxi & Private Hire" Value="8" />
                                    <dx:ListEditItem Text="Windscreens" Value="9" />
                                    <dx:ListEditItem Text="Other" Value="99" />
                               </Items>
                            </dx:ASPxCheckBoxList>
                        </td>
                    </tr>

                    <tr>
                    <td>

                    </td>
                    </tr>


                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxLabel 
                                ID="lblFilter5" 
                                runat ="server" 
                                Text="Customer Type" />
                        </td>
                         <td align="left" valign="middle" style="margin-left:10px">
                               <dx:ASPxLabel 
                                    ID="lblFilter6" 
                                    runat ="server" 
                                    Visible="false"
                                    Text="Options" />
                        </td>
                    </tr>

                    <tr>
                        <td align="left" valign="middle">
                            <dx:ASPxComboBox
                                id ="ddlType" 
                               AutoPostBack="true"
                               runat="server">
                               <Items>
                                    <dx:ListEditItem Text="All Customers"  Value="0" Selected="true"/>
                                    <dx:ListEditItem Text ="All Focus" Value ="1"/>
                                    <dx:ListEditItem Text ="Dealer Focus Only" Value ="2"/>
                                    <dx:ListEditItem Text ="BGC Focus Only"  Value ="3"/>
                                    <dx:ListEditItem Text ="Not Focus" Value ="4"/>
                                </Items>
                            </dx:ASPxComboBox>
                        </td>

                         <td align="left" valign="middle" style="margin-left:10px">
                             <dx:ASPxCheckBox 
                                 ID="chkShowHidden" 
                                 runat="server" 
                                 Text="Include hidden" 
                                 Visible="false"
                                 TextAlign="right"/>
                        </td>
                    </tr>

                    <tr>
                       <td align="left">
                        </td>
                        
                        <td align="right" valign="middle" style="margin-left:10px">
                        
                            <dx:ASPxButton ID="btnReset" runat="server" Text="Reset"  />

                            <dx:ASPxButton ID="btnSearch" runat="server" Text="Go" />
                        
                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;"  id ="divmain" runat="server">

        <div id="divTitle" style="position:relative;">
            <table id="trCustomerDetails" width="100%" >
                <tr>
                    <td align="left" width="88%" >                  
                        <dx:ASPxLabel 
                            Font-Size="Larger"
                             ID="lblPageTitle" 
                            runat ="server" 
                            Text="Customer Consent Manager" />
                         </td>

                        <td width="4%" align="right" > 
                          <dx:ASPxButton ID="btnUpdate" runat="server" Text="Update" ClientInstanceName="btnUpdate" AutoPostBack="false" ClientEnabled="false">
                                <ClientSideEvents Click="OnUpdateClick" />
                            </dx:ASPxButton>
                        </td>

                        <td width="4%" align="right" > 
                                <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel" ClientInstanceName="btnCancel" AutoPostBack="false" ClientEnabled="false">
                                    <ClientSideEvents Click="OnCancelClick" />
                                </dx:ASPxButton>
                        </td>
    
                        <td width="4%" align="right" > 
                            <dx:ASPxButton  
                                ID="btnMenu" 
                                runat="server" 
                                ClientInstanceName="btnMenu" 
                                ToolTip="Search & Filtering Options" 
                                Text="..." 
                                width="30px"
                                Visible="True">
                            </dx:ASPxButton>
                   </td>

                </tr>
            </table>
  
        </div>
   
        <table Width="100%" style="position:relative; top:10px">
            <tr>
            <td Width="100%" >

          
                <dx:ASPxGridView 
            ID="gridCompanies" 
            runat="server" 
            EnableCallBacks ="false"
            ClientInstanceName="gridCompanies" 
            AutoGenerateColumns="False" 
            KeyFieldName="customerdealerid" 
            Width="100%"
            OnLoad="GridStyles" 
            cssclass="grid_styles">
                        
            <ClientSideEvents BatchEditEndEditing="OnBatchEditEndEditing" BatchEditStartEditing="OnBatchEditStartEditing" CustomButtonClick="OnCustomButtonClick" EndCallback="OnEndCallback"/>

           <SettingsEditing Mode="Batch" BatchEditSettings-ShowConfirmOnLosingChanges="true"    />

            <Settings 
                ShowStatusBar="Hidden" 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowHeaderFilterButton="False" />
            
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16" />
            
            <SettingsDetail 
                ShowDetailRow="False" 
                ExportMode="None" />
            
            <SettingsBehavior AllowSelectByRowClick="True" />
            
            <Styles>
                <BatchEditModifiedCell BackColor="#ff6600"></BatchEditModifiedCell>

                <Header Wrap="True" 
                    HorizontalAlign="Center" 
                    VerticalAlign="Middle" />
            </Styles>

            <Columns>

                <dx:GridViewBandColumn Caption ="Dealer Details " Name="bndDealer" HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                <dx:GridViewDataTextColumn 
                    FieldName="dealercode" 
                    Caption="Dealer Code" 
                    VisibleIndex="0" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="True"
                    Settings-HeaderFilterMode="CheckedList"
                    ExportCellStyle-HorizontalAlign="Center"
                    Width="5%" 
                    visible="true"  
                    ExportWidth="150" />

                <dx:GridViewDataTextColumn 
                    FieldName="dealername" 
                    Caption="Dealer Name" 
                    ExportCellStyle-HorizontalAlign="Left"
                    VisibleIndex="1" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    visible="true"  
                    Width="15%" 
                    CellStyle-Wrap="false"
                    ExportWidth="250" />
                        </Columns>
                </dx:GridViewBandColumn>    

                <dx:GridViewBandColumn Caption ="Customer Details" Name="bndCustomer" HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                <dx:GridViewDataTextColumn 
                    FieldName="Id" 
                    Caption="ID" 
                    VisibleIndex="2" 
                    visible="false"  
                    ExportWidth="150" />
      
                <dx:GridViewDataTextColumn 
                    FieldName="businessname" 
                    ExportCellStyle-HorizontalAlign="Left"
                    EditFormSettings-Visible="False"
                    Caption="A/C Name" 
                    VisibleIndex="3" 
                    ReadOnly="true"
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-Wrap="False"
                    Width="10%" 
                    ExportWidth="250" />
                
                <dx:GridViewDataTextColumn 
                    FieldName="link" 
                    EditFormSettings-Visible="False"
                    ExportCellStyle-HorizontalAlign="Center"
                    Caption="A/C Code" 
                    VisibleIndex="4" 
                    ReadOnly="true"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    CellStyle-Wrap="False"
                    Width="6%" 
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    FieldName="postcode" 
                    ExportCellStyle-HorizontalAlign="Center"
                    EditFormSettings-Visible="False"
                    Caption="Postcode" 
                    ReadOnly="true"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    Visible="true"
                    Width="5%" 
                    VisibleIndex="5"  
                    ExportWidth="150" />
                
                <dx:GridViewDataTextColumn 
                    Caption="Business Type" 
                    ExportCellStyle-HorizontalAlign="Center"
                    EditFormSettings-Visible="False"
                    FieldName="BusinessTypeDescription" 
                    Name="BusinessTypeDescriptionEdit" 
                    ReadOnly="true"
                    VisibleIndex="6" 
                    Visible="true"
                    Width="7%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="150" />
      
                <dx:GridViewDataTextColumn 
                    FieldName="customertype" 
                    ExportCellStyle-HorizontalAlign="Center"
                    EditFormSettings-Visible="False"
                    Caption="Customer Type" 
                    ReadOnly="true"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    VisibleIndex="7" 
                    Visible="true"
                    Width="7%" 
                    ExportWidth="150" />
                   
                <dx:GridViewDataTextColumn 
                    Caption="Focus" 
                    ExportCellStyle-HorizontalAlign="Center"
                    EditFormSettings-Visible="False"
                    FieldName="focustype" 
                    ReadOnly="true"
                    Name="focustypeEdit" 
                    VisibleIndex="8" 
                    Visible="true"
                    Width="5%" 
                    CellStyle-Wrap="False"
                    UnboundType="String" 
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center"
                    ExportWidth="150"/>
                        </Columns>
                </dx:GridViewBandColumn>    
    
                <dx:GridViewBandColumn Caption ="Update Details" HeaderStyle-HorizontalAlign="Center">
                    <Columns>

                         <dx:GridViewDataTextColumn 
                            Caption="Last Updated On" 
                            ExportCellStyle-HorizontalAlign="Center"
                            EditFormSettings-Visible="False"
                            FieldName="activefrom" 
                            Name="activefrom" 
                            ReadOnly="true"
                            VisibleIndex="9" 
                            Visible="true"
                            Width="10%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                         <dx:GridViewDataTextColumn 
                            ExportCellStyle-HorizontalAlign="Center"
                            Caption="Last Updated By" 
                            EditFormSettings-Visible="False"
                            FieldName="lastupdatedby" 
                            Name="lastupdatedby" 
                            ReadOnly="true"
                            VisibleIndex="10" 
                            Visible="true"
                            Width="10%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>
                
                        </columns>
                </dx:GridViewBandColumn>    

                <dx:GridViewBandColumn Name="bndConsentScreen" Caption ="Consent Details - Check to Opt in" HeaderStyle-HorizontalAlign="Center">
                        <Columns>

                                            
                        <dx:GridViewDataCheckColumn
                            Caption="No Contact " 
                            FieldName="nocontact_flag" 
                            ExportCellStyle-HorizontalAlign="Center"
                            Name="nocontact_flag" 
                            VisibleIndex="11" 
                            Visible="true"
                            Width="7%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                        <dx:GridViewDataCheckColumn
                            Caption="Post" 
                            FieldName="postallowed_flag" 
                            ExportCellStyle-HorizontalAlign="Center"
                            Name="postallowed_flag" 
                            VisibleIndex="12" 
                            Visible="true"
                            Width="7%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                        <dx:GridViewDataCheckColumn
                            Caption="Email" 
                            FieldName="emailallowed_flag" 
                            ExportCellStyle-HorizontalAlign="Center"
                            Name="emailallowed_flag" 
                            VisibleIndex="13" 
                            Visible="true"
                            Width="7%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                        <dx:GridViewDataCheckColumn
                            Caption="Phone" 
                            FieldName="phoneallowed_flag" 
                            Name="phoneallowed_flag" 
                            ExportCellStyle-HorizontalAlign="Center"
                            VisibleIndex="14" 
                            Visible="true"
                            Width="7%" 
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                        <dx:GridViewDataCheckColumn
                            Caption="SMS" 
                            FieldName="smsallowed_flag" 
                            Name="smsallowed_flag" 
                            VisibleIndex="15" 
                            Visible="true"
                            Width="7%" 
                            ExportCellStyle-HorizontalAlign="Center"
                            CellStyle-Wrap="False"
                            UnboundType="String" 
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center"
                            ExportWidth="150"/>

                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption ="Address Details" Name="bndAddress" HeaderStyle-HorizontalAlign="Center" Visible="false">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Qube ID" VisibleIndex="16" FieldName="customerdealerid" width="50px" ExportWidth="150" ExportCellStyle-HorizontalAlign="Center"/>
                            <dx:GridViewDataTextColumn Caption="Email" VisibleIndex="17" FieldName="email" width="300px" ExportWidth="300" ExportCellStyle-HorizontalAlign="Left"/>
                            <dx:GridViewDataTextColumn Caption="Full Address" VisibleIndex="18" FieldName="fulladdress" width="300px"  ExportWidth="600" ExportCellStyle-HorizontalAlign="Left"  ExportCellStyle-Wrap="True"/>
                            <dx:GridViewDataTextColumn Caption="Telephone" VisibleIndex="19" FieldName="telephonenumber"  width="100px" ExportWidth="100" ExportCellStyle-HorizontalAlign="Center" />
                    </Columns>
                </dx:GridViewBandColumn>
                

             
            </Columns>


         </dx:ASPxGridView>
           </td>
            </tr>
         </table>    
             
    
   </div>
                
    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        FileName="CustomerConsentInformation"
        GridViewID="gridCompanies">
    </dx:ASPxGridViewExporter>

   
</asp:Content>
