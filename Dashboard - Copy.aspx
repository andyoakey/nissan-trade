﻿<%@ page title="" language="VB" masterpagefile="~/MasterPage.master" autoeventwireup="false" CodeFile="Dashboard.aspx.vb" inherits="Dashboard" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ MasterType VirtualPath="~/MasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <dx:ASPxPopupControl 
        ID="popSupportingInfo" 
        runat="server"  
        Width ="800px"  
        Height="500px" 
        HeaderText="Payment Bands and Supporting Information"
        ShowOnPageLoad="False" 
        ClientInstanceName="popSupportingInfo" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" 
        CloseOnEscape="True">
        
        <ContentCollection>
            <dx:PopupControlContentControl ID="popSupportingInfoContent" runat="server" CssClass="invoice">

                <dx:ASPxGridView 
                                ID="gridBonusBandings" 
                                DataSourceID="dsBonusBanding"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     
                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                        
                                        <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Dealer/Group Performance Banding">
                                            <Columns>

                                                <dx:GridViewDataTextColumn 
                                              Fieldname="id" Visible="false"/>

                                                <dx:GridViewDataTextColumn 
                                              Caption="Band Name" 
                                              FieldName="banding"
                                              Name="banding"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              CellStyle-Wrap ="False"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                                            
                                                <dx:GridViewDataTextColumn 
                                              Caption="Band Description" 
                                              FieldName="comment"
                                              Name="comment"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="True"
                                              ExportWidth="200"
                                              Width= "32%"
                                              VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                                            
                                            </Columns>
                                       </dx:GridViewBandColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="No Band"  
                                              FieldName="band0_text"
                                              Name="band0_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap ="False"
                                              ToolTip="Must Grow more than this for ANY payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 1 Range" 
                                              FieldName="band1_text"
                                              Name="band1_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="False"
                                              ToolTip="% Growth Range Required to Earn Band 1 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 2 Range" 
                                              FieldName="band2_text"
                                              Name="band2_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-Wrap="False"
                                              CellStyle-HorizontalAlign="Center"
                                              ToolTip="% Growth Range Required to Earn Band 2 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 3 Range" 
                                              FieldName="band3_text"
                                              Name="band3_text"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              CellStyle-Wrap="False"
                                              ToolTip="% Growth Range Required to Earn Band 3 payment"
                                              ExportWidth="200"
                                              Width= "12%"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
                                </dx:ASPxGridView>   

                <br />

                 <dx:ASPxGridView 
                                ID="gridPaymentScale" 
                                DataSourceID="dsBonusPayments"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     

                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>

                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Sales Performance Reward">
                                                <Columns>

                                                    <dx:GridViewDataTextColumn 
                                              Caption="Sales By Quarter" 
                                              FieldName="qsales_bandname"
                                              Name="qsales_bandname"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              ToolTip="Total Sales By Quarter"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                        
                                            <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 1" 
                                              FieldName="payband_1"
                                              Name="payband_1"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                                            
                                            <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 2" 
                                              FieldName="payband_2"
                                              Name="payband_2"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                           <dx:GridViewDataTextColumn 
                                              Caption="Growth Band 3" 
                                              FieldName="payband_3"
                                              Name="payband_3"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>

                                                 </Columns>

                                             </dx:GridViewBandColumn>


                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Operating Standards">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn 
                                              Caption="Payment Scale" 
                                              FieldName="standardsreward"
                                              Name="standardsreward"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap="True"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              Tooltip="Subject To Audit"
                                              PropertiesTextEdit-DisplayFormatString="c0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                                </Columns>
                                             </dx:GridViewBandColumn>

                               </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
                                </dx:ASPxGridView>   
   
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                runat="server" 
                Text="Dashboard" 
                Font-Size="Larger" />
        </div>
        
        <br />

        <dx:ASPxPageControl
            ID="ASPxPageControl1"
            runat="server"
            Width="100%" 
            ActiveTabIndex="0" 
            CssClass="page_tabs">

            <TabPages>

                <dx:TabPage Text="Overall Sales Performance*" name = "tabOverall" >

                    <ContentCollection>

                        <dx:ContentControl runat="server">

                            <table width ="100%">
                                <tr>
                                    <td width ="48%">
                                            <dx:ASPxLabel
                                                ID="ASPxLabel1"           
                                                runat="server"           
                                                Text="Total Sales"/>         
                                    </td>
                                    
                                    <td width ="4%">
                                    </td>

                                    <td width ="48%">
                                          <dx:ASPxComboBox 
        ID="ddlServiceTypes" 
        SelectedIndex="0"
        runat="server" 
        AutoPostBack="True" 
        TextField="description" 
        ValueField="productgroup" 
        Width="300px">
    </dx:ASPxComboBox> 
                                        <br />
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td width ="48%">
                                         <dx:ASPxGridView 
                                ID="gridtotalsales" 
                                DataSourceID="dsDashboardLeft"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true"
                                onload="GridStyles"
                                CssClass="grid_styles">
     
        <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

        <SettingsPager PageSize="15" ShowDefaultImages="False">
            <AllButton Visible="True" />
        </SettingsPager>

        <Columns>

            <dx:GridViewDataTextColumn  FieldName="id" Visible="false" />
                       
            <dx:GridViewDataTextColumn 
                Caption="Period Name" 
                FieldName="periodname"
                ExportWidth="250"
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-HorizontalAlign="Left"
                Width= "33%"
                VisibleIndex="0" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="This Period" 
                FieldName="thisyear" 
                ExportWidth="200"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                Width= "19%"
                VisibleIndex="1">
                <PropertiesTextEdit DisplayFormatString="c0">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
  
            <dx:GridViewDataTextColumn 
                Caption="12mths ago" 
                FieldName="lastyear" 
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                Width= "16%"
                ExportWidth="200"
                VisibleIndex="2">
                <PropertiesTextEdit DisplayFormatString="c0">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
  
            <dx:GridViewDataTextColumn 
                Caption="% Uplift" 
                FieldName="yony"
                ExportWidth="200"
                Width= "16%"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                ReadOnly="True" 
                VisibleIndex="3"  >
                <PropertiesTextEdit DisplayFormatString="0.00%">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="% Valid Sales" 
                FieldName="valid_pc"
                Name="valid_pc"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                ToolTip="As part of core standards, at least 85% of all dealer transactions should be valid"
                ExportWidth="200"
                Width= "17%"
                ReadOnly="True" 
                VisibleIndex="4"  >
                <PropertiesTextEdit DisplayFormatString="0.00%">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
 </Columns>
              
        <SettingsBehavior 
            AllowSort="False" 
            AllowDragDrop="False" 
            ColumnResizeMode="Control" />
      
<Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
</dx:ASPxGridView>   
                                    </td>
                                    
                                    <td width ="4%">
                                    </td>

                                    <td width ="48%">
                                        <dx:ASPxGridView
                                            ID="gridselectedsales"
                                            DataSourceID="dsDashboardRight"
                                            OnLoad="GridStyles"
                                            CssClass="grid_styles"
                                            Styles-Header-HorizontalAlign="Center"
                                            Styles-CellStyle-HorizontalAlign="Center"
                                            runat="server"
                                            AutoGenerateColumns="False"
                                            Settings-UseFixedTableLayout="true">
     
        <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

        <SettingsPager PageSize="15" ShowDefaultImages="False" AllButton-Visible="true">

<AllButton Visible="True"></AllButton>
                                            </SettingsPager>

        <Columns>
           
            <dx:GridViewDataTextColumn 
                Caption="Period Name" 
                FieldName="periodname"
                ExportWidth="250"
                HeaderStyle-HorizontalAlign="Left"
                CellStyle-HorizontalAlign="Left"
                Width= "40%"
                VisibleIndex="0" >
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="This Period" 
                FieldName="thisyear" 
                ExportWidth="200"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                Width= "20%"
                VisibleIndex="1">
                <PropertiesTextEdit DisplayFormatString="c0">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
  
            <dx:GridViewDataTextColumn 
                Caption="12mths ago" 
                FieldName="lastyear" 
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                Width= "20%"
                ExportWidth="200"
                VisibleIndex="2">
                <PropertiesTextEdit DisplayFormatString="c0">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>
  
            <dx:GridViewDataTextColumn 
                Caption="% Uplift" 
                FieldName="yony"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                ExportWidth="200"
                Width= "20%"
                ReadOnly="True" 
                VisibleIndex="3"  >
                <PropertiesTextEdit DisplayFormatString="0.00%">
                </PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
            </dx:GridViewDataTextColumn>

        </Columns>
              
        <SettingsBehavior 
            AllowSort="False" 
            AllowDragDrop="False" 
            ColumnResizeMode="Control" />
      
<Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
</dx:ASPxGridView>  
                                 </td>
                                </tr>
                         </table>
    
             <br />

                  <dx:ASPxLabel 
                        ID="lblDateTitle" 
                        runat="server"
                        Text="* Sales performance is now shown on a ‘like for like’ basis using the same sales period for month to date and YOY versus last year">
                  </dx:ASPxLabel>

            <br />

                   <dx:ASPxLabel 
                       ID="lblNationwide" 
                       Font-Italic="true"
                       runat="server" 
                       width = "800px"
                       text="** The % Uplift shown above will be used to determine individual Dealer growth bands for the Trade Bonus Reward.">
                    </dx:ASPxLabel>
      
                </dx:ContentControl>

            </ContentCollection>

                </dx:TabPage>

                <dx:TabPage Text="Customer Summary" Name="tabCustomerSummary">

                    <ContentCollection>

                        <dx:ContentControl runat="server">
                            <dx:ASPxGridView ID="gridCustomerSummary" 
                                runat="server" 
                                DataSourceID = "dsCustomerSummary"
                                AutoGenerateColumns="False" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                CssClass="grid_styles"
                                Onload="GridStyles"
                                OnCustomColumnDisplayText="gridCustomerSummary_OnCustomColumnDisplayText" 
                                Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn 
                                        Caption="Customer Summary" 
                                        FieldName="RowTitle" 
                                        HeaderStyle-HorizontalAlign="Left"
                                        CellStyle-HorizontalAlign="Left"
                                        HeaderStyle-Wrap="True"
                                        ReadOnly="True" 
                                        VisibleIndex="0" 
                                        exportwidth="200"
                                        Width="20%">
<HeaderStyle HorizontalAlign="Left" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Active (last 90 days)" 
                                        FieldName="ActCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        exportwidth="150"
                                        VisibleIndex="1">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Inactive (91-180 days)" 
                                        FieldName="InActCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        VisibleIndex="2">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Lapsed (181-270 days)" 
                                        FieldName="LapsedCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        VisibleIndex="3">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Prospects (over 270 days)" 
                                        FieldName="ProspectCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        VisibleIndex="4">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="New Prospects (never spent)" 
                                        FieldName="NewProspectCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        VisibleIndex="5">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Focus Customers" 
                                        FieldName="TCCust" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="0"
                                        VisibleIndex="6">
<PropertiesTextEdit DisplayFormatString="0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Top 10% Customer Sales YTD" 
                                        FieldName="Top10" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        exportwidth="150"
                                        HeaderStyle-Wrap="True"
                                        PropertiesTextEdit-DisplayFormatString="£##,##0"
                                        VisibleIndex="7">
<PropertiesTextEdit DisplayFormatString="c0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Excluded Sales YTD" 
                                        FieldName="Excluded" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        exportwidth="150"
                                        CellStyle-HorizontalAlign="Center"
                                        HeaderStyle-Wrap="True"
                                        PropertiesTextEdit-DisplayFormatString="£##,##0"
                                        VisibleIndex="8">
<PropertiesTextEdit DisplayFormatString="c0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Excluded Sales (%)" 
                                        FieldName="ExcludedPerc" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        exportwidth="150"
                                        HeaderStyle-Wrap="True"
                                        PropertiesTextEdit-DisplayFormatString="0.00"
                                        VisibleIndex="9">
<PropertiesTextEdit DisplayFormatString="0.00"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>

                                <SettingsPager Visible="False">
                                </SettingsPager>
                                
                                <Settings UseFixedTableLayout="True"  ShowFooter="false"/>
                
<Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
                
                              </dx:ASPxGridView>

                            <br/>
                            
                            <dx:ASPxLabel
                                ID="lblNotes" 
                                runat="server" 
                                CssClass="textnotes"
                                Text=" *Competitors (Motor Factors and Parts Suppliers) are not included in these counts.">
                            </dx:ASPxLabel>
                            
                            <br/>
                            <br/>
                            <br/>
                            
                            <dx:ASPxGridView 
                                ID="gridBusinessTypeSummary"         
                                runat="server" 
                                DataSourceID = "dsBusinessTypeSummary"
                                CssClass="grid_styles"
                                Onload="GridStyles"
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                ShowFooter="false"
                                AutoGenerateColumns="False" 
                                Width="100%">
                                <Columns>
                                    <dx:GridViewDataTextColumn 
                                        Caption="Business Type" 
                                        FieldName="BusinessTypeDescription" 
                                        HeaderStyle-HorizontalAlign="Left"
                                        CellStyle-HorizontalAlign="Left"
                                        exportwidth="300"
                                        VisibleIndex="0" 
                                        Width="50%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Mechanical Spend YTD" 
                                        FieldName="MechCompSpend" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        CellStyle-HorizontalAlign="Center"
                                        exportwidth="150"
                                        PropertiesTextEdit-DisplayFormatString="£##,##0"
                                        Width="25%"
                                        VisibleIndex="1">
<PropertiesTextEdit DisplayFormatString="c0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Total Spend YTD" 
                                        HeaderStyle-HorizontalAlign="Center"
                                        exportwidth="150"
                                        CellStyle-HorizontalAlign="Center"
                                        PropertiesTextEdit-DisplayFormatString="£##,##0"
                                        FieldName="TotalSpend" 
                                        Width="25%"
                                        VisibleIndex="2">
<PropertiesTextEdit DisplayFormatString="c0"></PropertiesTextEdit>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<CellStyle HorizontalAlign="Center"></CellStyle>
                                    </dx:GridViewDataTextColumn>
                                
                                </Columns>
                                
                                <SettingsPager Visible="False" PageSize="15"/>
                                <Settings ShowFooter="False"/>

<Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
                            </dx:ASPxGridView>

                            <br />
                        </dx:ContentControl>

                    </ContentCollection>

                </dx:TabPage>

                <dx:TabPage Text="KPIs" Name="tabKPIs">

                    <ContentCollection>

                        <dx:ContentControl runat="server">
                            <table style="width:100%">
                                <tr>
                                    <td align="left" >

                                        <dx:ASPxComboBox
                                            ID="ddlKPIYear" 
                                            runat="server" 
                                            width="75px"
                                            AutoPostBack="True">
                                        </dx:ASPxComboBox>


                                     </td>
                                </tr>
                            </table>
                            
                            <br />
       
                            <dx:ASPxGridView 
                                ID="gridKPI1" 
                                datasourceid="dsKPI1"
                                runat="server" 
                                CssClass="grid_styles"
                                Onload="GridStyles"
                                AutoGenerateColumns="False" 
                                KeyFieldName="RowTitle" 
                                OnCustomColumnDisplayText="gridSalesSummary_OnCustomColumnDisplayText" 
                                Width="100%">

                                <Columns>
                                
                                    <dx:GridViewDataTextColumn 
                                        Caption="KPI Summary" 
                                        FieldName="RowTitle" 
                                        ReadOnly="True" 
                                        exportwidth="150"
                                        VisibleIndex="0" 
                                        Width="25%">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Dealer Average" 
                                        FieldName="ColCentre" 
                                        exportwidth="150"
                                        VisibleIndex="1">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Zone Average" 
                                        FieldName="ColZone" 
                                        exportwidth="150"
                                        VisibleIndex="2">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="TPM Average" 
                                        FieldName="ColTPSM" 
                                        exportwidth="150"
                                        VisibleIndex="3">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Region Average" 
                                        FieldName="ColRegion" 
                                        exportwidth="150"
                                        VisibleIndex="4">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="National Average" 
                                        exportwidth="150"
                                        FieldName="ColNational" 
                                        VisibleIndex="5">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Top 10 Average" 
                                        FieldName="ColTop10" 
                                        exportwidth="150"
                                        VisibleIndex="6">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>
                                </Columns>

                                <SettingsPager Visible="False">
                                </SettingsPager>

                                <Settings UseFixedTableLayout="True" />

                            </dx:ASPxGridView>

                            <br />

                            <br />

                            <dx:ASPxGridView 
                                ID="gridKPI2" 
                                datasourceid="dsKPIRanking"
                                runat="server" 
                                CssClass="grid_styles"
                                AutoGenerateColumns="False" 
                                OnCustomColumnDisplayText="gridKPI2_OnCustomColumnDisplayText">
                                <Columns>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Level" 
                                        FieldName="RowTitle" 
                                        exportwidth="150"
                                        VisibleIndex="0" 
                                        Width="175px">
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </dx:GridViewDataTextColumn>
                                  
                                      <dx:GridViewDataTextColumn 
                                          Caption="Ranking" 
                                          FieldName="ColCentre" 
                                        exportwidth="150"
                                          VisibleIndex="1" 
                                          Width="100px">
                                        <PropertiesTextEdit DisplayFormatString="0">
                                        </PropertiesTextEdit>
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center"/>
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Performance vs last month" 
                                        FieldName="Movement" 
                                        Name="Movement" 
                                        exportwidth="150"
                                        VisibleIndex="2" 
                                        Width="100px">
                                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                </Columns>

                                <SettingsPager Visible="False">
                                </SettingsPager>

                            </dx:ASPxGridView>

                            <br />

                            <br />

                        </dx:ContentControl>

                    </ContentCollection>

                </dx:TabPage>

                <dx:TabPage Text="Trade Sales Reward" Name="tabSalesBonus" >

                    <ContentCollection>

                        <dx:ContentControl runat="server">
      
                            <table style="width:100%">
                             <tr>
                                    <td style="width:10%" align="left">
                                           <dx:ASPxComboBox id ="cboFY" Width="100px" runat="server" Font-Names="Calibri" Font-Size="Medium" >
                                               <Items>
                                                   <dx:ListEditItem Text="FY 2019/20" />
                                               </Items>
                                           </dx:ASPxComboBox>
                                    </td>

                                    <td style="width:40%" align="left">
                                            <dx:ASPxLabel ID="lblPerformanceBand" runat="server" Font-Names="Calibri" Font-Size="Medium" Width="100%"/>
                                    </td>

                                    <td style="width:30%" align="left">
                                            <dx:ASPxLabel ID="lblRunRate" runat="server" Font-Names="Calibri" Font-Size="Medium"  Width="100%"/>
                                    </td>

                                    <td style="width:20%" align="right">
                                            <dx:ASPxButton id ="btnInfo" runat ="server" Font-Names="Calibri" Font-Size="Medium" AutoPostBack="true" Text="Supporting Information"/>
                                    </td>


                                 </tr>
                            </table>
                            
                            <br />

                            <table>
                                <tr>
                                    <td style="width:100%" valign="top">
                                        <dx:ASPxLabel ID="lblBonus1" runat="server" Font-Names="Calibri" Font-Size="Medium" Text="Sales Excluding NWCR (used to calculate growth level)"  />
                                        <dx:ASPxGridView 
                                ID="gridBonus1" 
                                DataSourceID="dsBonusByQuarter"
                                KeyFieldName="quartername"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Header-Wrap="True"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     
     
                                <Settings
                                    ShowGroupButtons="False" 
                                    ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="id" Name="id" Visible="false"/>
                                     <dx:GridViewDataTextColumn FieldName="quarter_id" Name="quarter_id" Visible="false"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="quartername" Name="quartername"  Width= "16%" />
                                     <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="workingdaysinquarter" Name="workingdaysinquarter" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="workingdayscompleted" Name="workingdayscompleted" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="runrate_ex" Name="runrate_ex" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="sales_ex_Last" Name="sales_ex_Last" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="6" FieldName="sales_ex_Lastfq" Name="sales_ex_Lastfq" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="7" FieldName="sales_ex_This" Name="sales_ex_This" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="8" FieldName="growth_ex" Name="growth_ex" Width= "12%" PropertiesTextEdit-DisplayFormatString="0.0%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="9" FieldName="forecast_exsales" Name="forecast_exsales" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                </Columns>

                                <SettingsBehavior AllowSort="False" AllowSelectSingleRowOnly="true" AllowDragDrop="False" ColumnResizeMode="Control" />
      
                                <Styles>
                                      <Header HorizontalAlign="Center"></Header>
                                </Styles>

                                <Settings ShowFooter="true" />

                                <Styles Footer-HorizontalAlign ="Center" />
                
                                 <TotalSummary>
                                        <dx:ASPxSummaryItem   FieldName="quartername" SummaryType="Sum" DisplayFormat="Projection FY19/20: {0}" />
                                        <dx:ASPxSummaryItem   FieldName="workingdaysinquarter" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="workingdayscompleted" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_ex_Lastfq" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_ex_Last" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_ex_This" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_sales" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="rewardpayment" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_exsales" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="growth_ex" SummaryType="Custom" DisplayFormat="0.0%"/>
                                 </TotalSummary>

                                </dx:ASPxGridView>   
                                    </td>
                                </tr>    
                            </table>

                            <br />

                            <table>
                                <tr>
                                    <td style="width:100%" valign="top">
                                        <dx:ASPxLabel ID="lblBonus2" runat="server" Font-Names="Calibri" Font-Size="Medium" Text="Sales Including NWCR (used to calculate reward value)"  />
                                        <dx:ASPxGridView 
                                ID="gridBonus2" 
                                DataSourceID="dsBonusByQuarter"
                                KeyFieldName="quartername"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Header-Wrap="True"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     
     
                                <Settings
                                    ShowGroupButtons="False" 
                                    ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="id" Name="id" Visible="false"/>
                                     <dx:GridViewDataTextColumn FieldName="quarter_id" Name="quarter_id" Visible="false"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="quartername" Name="quartername"  Width= "16%" />
                                     <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="workingdaysinquarter" Name="workingdaysinquarter" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="workingdayscompleted" Name="workingdayscompleted" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="runrate_all" Name="runrate_all" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="sales_all_Last" Name="sales_all_Last" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="6" FieldName="sales_all_Lastfq" Name="sales_all_Lastfq" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="7" FieldName="sales_all_This" Name="sales_all_This" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="8" FieldName="growth_all" Name="growth_all" Width= "12%" PropertiesTextEdit-DisplayFormatString="0.0%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="9" FieldName="forecast_sales" Name="forecast_sales" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                </Columns>

                                <SettingsBehavior AllowSort="False" AllowSelectSingleRowOnly="true" AllowDragDrop="False" ColumnResizeMode="Control" />
      
                                <Styles>
                                      <Header HorizontalAlign="Center"></Header>
                                </Styles>

                                <Settings ShowFooter="true" />

                                <Styles Footer-HorizontalAlign ="Center" />
                
                                 <TotalSummary>
                                        <dx:ASPxSummaryItem   FieldName="quartername" SummaryType="Sum" DisplayFormat="Projection FY19/20: {0}" />
                                        <dx:ASPxSummaryItem   FieldName="workingdaysinquarter" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="workingdayscompleted" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_all_Lastfq" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_all_Last" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_all_This" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_sales" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="rewardpayment" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_allsales" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="growth_all" SummaryType="Custom" DisplayFormat="0.0%"/>
                                 </TotalSummary>

                                </dx:ASPxGridView>   
                                    </td>
                                </tr>    
                            </table>

                            <br />

                            <table style="width:100%" >
                                <tr>
                                    <td style="width:100%" align="left">
                                        <dx:ASPxLabel ID="lblBonus3" runat="server" Font-Names="Calibri" Font-Size="Medium" Text="Sales Reward Based on Projected Sales (including NWCR)"/>
                                    </td>
                                </tr>    
                
                                <tr>
                                    <td style="width:100%" valign="bottom" align="left">
                                        <dx:ASPxGridView 
                                ID="gridBonus3" 
                                DataSourceID="dsBonusByQuarter"
                                KeyFieldName="quartername"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Header-Wrap="True"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true">
     

                                <Settings
                                    ShowGroupButtons="False" 
                                    ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                     <dx:GridViewDataTextColumn FieldName="id" Name="id" Visible="false"/>
                                     <dx:GridViewDataTextColumn FieldName="quarter_id" Name="quarter_id" Visible="false"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="quartername" Name="quartername"  Width= "16%" />
                                     <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="workingdaysinquarter" Name="workingdaysinquarter" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="workingdayscompleted" Name="workingdayscompleted" Width= "12%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="runrate_all" Name="runrate_all" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="forecast_growth" Name="forecast_growth" Width= "12%" PropertiesTextEdit-DisplayFormatString="0.0%"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="6" FieldName="rewardband" Name="rewardband" Width= "12%" />
                                     <dx:GridViewDataTextColumn VisibleIndex="7" FieldName="forecast_sales" Name="forecast_sales" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="8" FieldName="rewardpayment" Name="rewardpayment" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                     <dx:GridViewDataTextColumn VisibleIndex="9" FieldName="StandardsReward" Name="StandardsReward" Width= "12%" PropertiesTextEdit-DisplayFormatString="c0"/>
                                </Columns>

                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowSelectSingleRowOnly="true"
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
                                      <Header HorizontalAlign="Center"></Header>
                                </Styles>

                                <Settings ShowFooter="true" />

                                <Styles Footer-HorizontalAlign ="Center" />
                
                                 <TotalSummary>
                                        <dx:ASPxSummaryItem   FieldName="quartername" SummaryType="Sum" DisplayFormat="Projection FY19/20: {0}" />
                                        <dx:ASPxSummaryItem   FieldName="workingdaysinquarter" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="workingdayscompleted" SummaryType="Sum" DisplayFormat="0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_sales" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="rewardpayment" SummaryType="Sum" DisplayFormat="c0"/>
                                        <dx:ASPxSummaryItem   FieldName="StandardsReward" SummaryType="Sum" DisplayFormat="c0"/>
                                 </TotalSummary>

                                </dx:ASPxGridView>   
                                        <div  style="text-align:right">
                                        <dx:ASPxLabel ID="ASPxLabel5"  runat="server" Font-Names="Calibri"  Font-Italic="true" Text="*Subject to Nissan TPM Audit"/>
                                        </div>
                                    </td>
                                </tr>    



                          
                            </table>
  
       
                        </dx:ContentControl>
                    </ContentCollection>

                </dx:TabPage>

            </TabPages>

        </dx:ASPxPageControl>
        
        <table width="100%">
            <tr>
                <td align="right">
                    System provided and maintained by QUBE
                </td>
            </tr>
        </table>

        <dx:ASPxButton 
            ID="btnExportToExcel" 
            runat="server" 
            Text="Print" 
            Width="100px" 
            Height="22px" 
            Visible="false" >
        </dx:ASPxButton>

        <br />
            
    </div>

<%-- Exporters --%>
<div>
    <dx:ASPxGridViewExporter 
        ID="expTotalSales"
        runat="server" 
        GridViewID="gridtotalsales" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expSelectedSales"
        runat="server" 
        GridViewID="gridselectedsales" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expCustomerSummary"
        runat="server" 
        GridViewID="gridcustomersummary" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expBusinessTypeSummary"
        runat="server" 
        GridViewID="gridbusinesstypesummary" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expKPI1"
        runat="server" 
        GridViewID="gridKPI1" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expKPI2"
        runat="server" 
        GridViewID="gridKPI2" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expBonus"
        runat="server" 
        GridViewID="gridBonusByQuarter" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expGridBonus1"
        runat="server" 
        GridViewID="gridBonus1" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expGridBonus2"
        runat="server" 
        GridViewID="gridBonus2" >
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter 
        ID="expGridBonus3"
        runat="server" 
        GridViewID="gridBonus3" >
    </dx:ASPxGridViewExporter>

</div>

<%-- Data Sets --%>
<div>
     <asp:SqlDataSource ID="dsDashboardLeft" runat="server" SelectCommand="sp_Dashboard_Tab1" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="nThisPeriod" SessionField="MonthFrom" Type = "Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sServiceType" DefaultValue="ALL"  Type="String" Size="3" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsDashboardRight" runat="server" SelectCommand="sp_Dashboard_Tab1" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="nThisPeriod" SessionField="MonthFrom" Type = "Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sServiceType" SessionField = "ServiceType"  Type="String" Size="3" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsCustomerSummary" runat="server" SelectCommand="p_Dashboard_Customer_Summary2" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsBusinessTypeSummary" runat="server" SelectCommand="p_Dashboard_BusinessTypeSummary" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsKPI1" runat="server" SelectCommand="p_KPI_Grid1" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nYear" SessionField="KPIYear" Type = "Int32" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsKPI2" runat="server" SelectCommand="p_KPI_Grid2" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nYear" SessionField="KPIYear" Type = "Int32" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsKPIRanking" runat="server" SelectCommand="sp_ShowKPIRankings" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
     </asp:SqlDataSource>


     <asp:SqlDataSource ID="dsBonusByQuarter" runat="server" SelectCommand="sp_BonusReport" SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sFinYear" SessionField="FinYear" Type="String" Size="7" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsBonusPayments" runat="server" SelectCommand="sp_BonusPaymentsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
             <asp:SessionParameter Name="FinYear" SessionField="ShortFinYear" Type="String" Size="5" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsBonusBanding" runat="server" SelectCommand="sp_BonusBandingsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
             <asp:SessionParameter Name="FinYear" SessionField="ShortFinYear" Type="String" Size="5" />
             <asp:SessionParameter Name="dealerband" SessionField="dealerband" Type="Int16" Size="1" />
        </SelectParameters>
     </asp:SqlDataSource>
</div>
</asp:Content>

