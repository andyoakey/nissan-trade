﻿Imports System.Data
Imports System.Net
Imports System.Net.Mail
Imports System.Collections

Partial Class DocLibrary

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If
    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Document Library"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

    End Sub

    Protected Sub dsDocLib_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDocLib.Init
        dsDocLib.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

End Class
