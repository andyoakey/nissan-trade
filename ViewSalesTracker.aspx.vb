﻿Imports System.Data
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraPrinting

Partial Class ViewSalesTracker

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Sales Tracker"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call GetMonthsThisYear(ddlMonth)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales Tracker", "Viewing Sales Tracker")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("SelectionLevel") <> "C" Then
            lblCentreError.Visible = True
            gridSalesTracker.Visible = False
            gridSalesTrackerSummary.Visible = False
        Else
            lblCentreError.Visible = False
            gridSalesTracker.Visible = True
            gridSalesTrackerSummary.Visible = True
        End If

    End Sub

    Protected Sub dsSalesTracker_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSalesTracker.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsSalesTracker.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsSalesTrackerSummary_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSalesTrackerSummary.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsSalesTrackerSummary.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsTrackerRunRate_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsTrackerRunRate.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        dsTrackerRunRate.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridSalesTracker_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "PeriodName" Then
            e.DisplayText = NicePeriodName(e.Value)
        End If
        If e.Column.FieldName = "TotalSalesDiff" Or e.Column.FieldName = "MechCompDiff" Then
            If IIf(IsDBNull(e.Value), 0, e.Value) <> 0 Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If
    End Sub

    Protected Sub gridSalesTracker_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSalesTracker.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0

        If Not IsDBNull(e.GetValue("TotalSales")) And (e.GetValue("WorkingDaysThisMonth") = e.GetValue("WorkingDaysAdjusted")) Then
            e.Cell.Font.Bold = True
            e.Cell.Font.Italic = False
        Else
            e.Cell.Font.Bold = False
            If e.DataColumn.Index = 6 Or e.DataColumn.Index = 11 Then
                e.Cell.Font.Italic = True
            Else
                e.Cell.Font.Italic = False
            End If
        End If

    End Sub


    Protected Sub gridSalesTrackerSummary_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.GetFieldValue("RowOrder") = 3 Then
            If Left(e.Column.FieldName, 8) = "RowValue" Then
                e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
            End If
        End If
    End Sub

    Protected Sub gridSalesTrackerSummary_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridSalesTrackerSummary.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0

        If e.KeyValue = "3" Then
            If e.DataColumn.Index = 1 Or e.DataColumn.Index = 3 Or e.DataColumn.Index = 5 Or e.DataColumn.Index = 7 Then
                If IsDBNull(e.CellValue) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.CellValue)
                End If
                e.Cell.Font.Bold = True
                e.Cell.ForeColor = System.Drawing.Color.White
                If nThisValue = 0 Then
                    e.Cell.Font.Bold = False
                    e.Cell.BackColor = System.Drawing.Color.White
                    e.Cell.ForeColor = System.Drawing.Color.Black
                ElseIf nThisValue >= 100 Then
                    e.Cell.BackColor = System.Drawing.Color.Green
                Else
                    e.Cell.BackColor = System.Drawing.Color.Red
                End If
            End If
        End If

        If e.DataColumn.Index = 0 Or e.DataColumn.Index = 2 Or e.DataColumn.Index = 4 Or e.DataColumn.Index = 6 Then
            e.Cell.Font.Bold = False
            e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")
            e.Cell.ForeColor = System.Drawing.Color.Black
        End If

    End Sub

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Session("MonthFrom") = ddlMonth.SelectedValue
    End Sub

End Class
