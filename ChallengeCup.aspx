﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChallengeCup.aspx.vb" Inherits="ChallengeCup" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Campaign Challenge Cup" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridCampaigns" runat="server" width="976px" 
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            DataSourceID="dsCampaigns"  AutoGenerateColumns="False" KeyFieldName="DealerCode"
            >
       
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded" ShowVerticalScrollBar="True" 
                UseFixedTableLayout="True" VerticalScrollableHeight="300"  />
            
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <ImagesFilterControl>
                <LoadingPanel>
                </LoadingPanel>
            </ImagesFilterControl>
            
            <Images>
                <LoadingPanelOnStatusBar >
                </LoadingPanelOnStatusBar>
                <LoadingPanel>
                </LoadingPanel>
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="National Position" FieldName="Ranking"
                    VisibleIndex="0" Width="8%">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="False" ShowInFilterControl="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="CentreName"
                    VisibleIndex="1" Width="25%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataCheckColumn Caption="Service Kits" VisibleIndex="2" FieldName="ServiceKits">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>

                <dxwgv:GridViewDataCheckColumn Caption="5W30-Oil" VisibleIndex="3" FieldName="FreeOil">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>
                
                <dxwgv:GridViewDataCheckColumn Caption="Power Packs" VisibleIndex="4" FieldName="PowerPacks">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>
                
                <dxwgv:GridViewDataCheckColumn Caption="M & S Vouchers" VisibleIndex="5" FieldName="Vouchers">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>
                
                <dxwgv:GridViewDataCheckColumn Caption="Pads and Discs" VisibleIndex="6" FieldName="PadsAndDiscs">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>
                
                <dxwgv:GridViewDataCheckColumn Caption="Antifreeze" VisibleIndex="7" FieldName="AntiFreeze">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dxwgv:GridViewDataCheckColumn>
                
                <dxwgv:GridViewDataTextColumn Caption="Offer Types Sold" FieldName="Offers"
                    VisibleIndex="8" Width="5%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Total Points" FieldName="Points"
                    VisibleIndex="9" Width="5%">
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="True" ShowDetailRow="True"/>

            <Templates>
            
                <DetailRow>
                    <br />
                    <dxwgv:ASPxGridView ID="gridDetails" runat="server" width="476px"  
                        DataSourceID="dsCampaignDetails"  AutoGenerateColumns="False" 
                        KeyFieldName="DealerCode"
                        OnHtmlDataCellPrepared ="gridDetail_HtmlDataCellPrepared"
                        OnBeforePerformDataSelect="gridDetail_BeforePerformDataSelect" 
                        >
                   
                        <Styles>
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                   
                        <SettingsPager PageSize="120" Mode="ShowAllRecords">
                        </SettingsPager>
                        
                        <Settings ShowColumnHeaders="false" 
                            ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                            ShowGroupFooter="VisibleIfExpanded"  />
                        
                        <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                            ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
                        
                        <ImagesFilterControl>
                            <LoadingPanel >
                            </LoadingPanel>
                        </ImagesFilterControl>
                        
                        <Images>
                            <LoadingPanelOnStatusBar >
                            </LoadingPanelOnStatusBar>
                            <LoadingPanel>
                            </LoadingPanel>
                        </Images>
                        
                        <Columns>
                            
                            <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="Description"
                                VisibleIndex="0" Width="75%">
                                <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataTextColumn Caption="Points" FieldName="Points" VisibleIndex="1" Width="8%">
                                <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                                    ShowFilterRowMenu="False" ShowInFilterControl="False" />
                                <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <PropertiesTextEdit DisplayFormatString="#,##0">
                                </PropertiesTextEdit>
                                <CellStyle HorizontalAlign="Center">
                                </CellStyle>
                            </dxwgv:GridViewDataTextColumn>


                        </Columns>
                        
                        <TotalSummary>
                            <dxwgv:ASPxSummaryItem DisplayFormat="#,##0" FieldName="Points" SummaryType="Sum" ShowInColumn="Points"  />
                        </TotalSummary>
                        
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>

                        <SettingsDetail IsDetailGrid="true" />

                    </dxwgv:ASPxGridView>
                    <br />
                    <br />

                </DetailRow>
            
            </Templates>
            
        </dxwgv:ASPxGridView>
        <br />
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsCampaigns" runat="server" SelectCommand="p_CampaignChallengeCup2011Q4" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCampaignDetails" runat="server" SelectCommand="p_CampaignChallengeCup2011Q4_Detail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="DealerCode" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>



