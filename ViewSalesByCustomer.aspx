<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewSalesByCustomer.aspx.vb" Inherits="ViewSalesByCustomer" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel 
                ID="lblPageTitle" 
                Font-Size="Larger"
                runat="server" 
                Text="Customer Analysis" />
        </div>

        <br />

        <table id="trSelectionStuff" Width="100%">
            <tr>
                <td width="10%" >
                    <dx:ASPxLabel 
                        ID="ASPxLabel5" 
                        runat="server" 
                        Text="From" />
                </td>
                
                <td width="10%" >
                    <dx:ASPxLabel 
                        ID="ASPxLabel1" 
                        runat="server" 
                        Text="To" />
                </td>

                <td width="80%"  align="Left">
                    <dx:ASPxLabel 
                        ID="ASPxLabel2" 
                        runat="server" 
                        Text="Type of Customer" />
                </td>
           </tr>

            <tr runat="server"  >
                <td width="10%" >
                    <dx:ASPxComboBox 
                        id ="ddlFrom"
                        AutoPostBack="True"
                        runat="server"
                        width="100%">
                    </dx:ASPxComboBox>
                </td>
                
                <td width="10%" >
                    <dx:ASPxComboBox 
                        id ="ddlTo"
                        AutoPostBack="True"
                        runat="server"
                        width="100%">
                    </dx:ASPxComboBox>
                </td>

                <td width="80%"  align="Left">
                      <dx:ASPxComboBox 
                           ID="ddlFocusType" 
                           runat="server" 
                           AutoPostBack="True">
                           <Items>
                                <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/> 
                                <dx:ListEditItem Text="Focus A/Cs only" Value="1"/> 
                                <dx:ListEditItem Text="Not Focus A/Cs" Value="2" /> 
                           </Items>
                      </dx:ASPxComboBox>
                   </td>
               </tr>
        </table>

        <br/>
        
        <dx:ASPxGridView 
            ID="gridSales" 
            onload="gridstyles"
            cssclass="grid_styles"
            runat="server" 
            DataSourceID="dsViewSalesLevel0"  
            AutoGenerateColumns="False"  
            KeyFieldName="customerid"
            Styles-Header-HorizontalAlign="Center" 
            Styles-Header-Wrap="True" 
            Styles-Cell-HorizontalAlign="Center" 
            Style="position: relative;" Width="100%" >
       
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16">
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign="Center" />

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False" 
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" 
                ShowFooter="True" />

            <SettingsBehavior 
                AllowSort="True" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Columns>
                
                <dx:GridViewDataTextColumn 
                    Caption="" 
                    FieldName="Id" 
                    ReadOnly="True" 
                    VisibleIndex="1" 
                    Visible="false" />
                
                <dx:GridViewDataTextColumn 
                    Caption="" 
                    FieldName="CustomerId" 
                    ReadOnly="True" 
                    VisibleIndex="2" 
                    Visible="false" />

                <dx:GridViewDataTextColumn 
                    Caption="Customer Name" 
                    FieldName="customername" 
                    ReadOnly="True" 
                    VisibleIndex="3" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    Width="17%"  
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    Caption="Postcode" 
                    FieldName="postcode" 
                    Visible="True" 
                    VisibleIndex="4" 
                    Width="9%" 
                    ExportWidth="120" />

                <dx:GridViewBandColumn 
                    Caption="Product Category" 
                    Visible="true" 
                    VisibleIndex="5"
                    HeaderStyle-HorizontalAlign="Center" >

                    <Columns>

                        <dx:GridViewDataTextColumn 
                            Caption="Accessories" 
                            FieldName="acc_sales" 
                            VisibleIndex="6" 
                            Width="11%" 
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Damage" 
                            FieldName="dam_sales" 
                            VisibleIndex="7"
                            Width="11%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Mechanical Repair" 
                            FieldName="mec_sales" 
                            VisibleIndex="8"
                            Width="11%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Routine Maintenance" 
                            FieldName="rom_sales" 
                            VisibleIndex="9"  
                            Width="11%" 
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Extended Maintenance" 
                            FieldName="exm_sales" 
                            VisibleIndex="10"
                            Width="11%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Additional Product" 
                            FieldName="oth_sales" 
                            VisibleIndex="11" 
                            Width="11%"  
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Total" 
                            FieldName="tot_sales" 
                            VisibleIndex="12" 
                            Width="11%"  
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total Customers: {0}" FieldName="Customer Name" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="acc_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="dam_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="mec_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="rom_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="exm_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="oth_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="tot_sales" SummaryType="Sum" />
            </TotalSummary>

            <Styles Footer-HorizontalAlign="Center" />

            <Templates>
                <DetailRow>
                       <dx:ASPxGridView 
            ID="gridSalesLevel1" 
            onBeforePerformDataSelect ="gridSalesLevel1_BeforePerformDataSelect"
            onload="gridstylesFalse"
            cssclass="grid_styles"
            runat="server" 
            DataSourceID="dsViewSalesLevel1"  
            AutoGenerateColumns="False"  
            KeyFieldName="customerdealerid"
            Styles-Header-HorizontalAlign="Center" 
            Styles-Header-Wrap="True" 
            Styles-Cell-HorizontalAlign="Center" 
            Style="position: relative; left: 5px" Width="100%" >
       
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16">
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign="Center" />

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False" 
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" 
                ShowFooter="True" />

            <SettingsBehavior 
                AllowSort="True" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Columns>
                
                <dx:GridViewDataTextColumn 
                    Caption="" 
                    FieldName="Id" 
                    ReadOnly="True" 
                    VisibleIndex="1" 
                    Visible="false" />
                
                <dx:GridViewDataTextColumn 
                    Caption="" 
                    FieldName="CustomerId" 
                    ReadOnly="True" 
                    VisibleIndex="2" 
                    Visible="false" />

                <dx:GridViewDataTextColumn 
                    Caption="Dealer" 
                    FieldName="dealercode" 
                    ReadOnly="True" 
                    VisibleIndex="3" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    Width="5%"  
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    Caption="Account" 
                    FieldName="account" 
                    ReadOnly="True" 
                    VisibleIndex="4" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    Width="7%"  
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    Caption="Customer Name" 
                    FieldName="customername" 
                    ReadOnly="True" 
                    VisibleIndex="4" 
                    CellStyle-HorizontalAlign="Left"
                    HeaderStyle-HorizontalAlign="Left"
                    CellStyle-Wrap="False"
                    Width="12%"  
                    ExportWidth="200" />

                <dx:GridViewDataTextColumn 
                    Caption="Postcode" 
                    FieldName="postcode" 
                    Visible="True" 
                    VisibleIndex="4" 
                    Width="9%" 
                    ExportWidth="120" />

                <dx:GridViewBandColumn 
                    Caption="Product Category" 
                    Visible="true" 
                    VisibleIndex="5"
                    HeaderStyle-HorizontalAlign="Center" >

                    <Columns>

                        <dx:GridViewDataTextColumn 
                            Caption="Accessories" 
                            FieldName="acc_sales" 
                            VisibleIndex="6" 
                            Width="10%" 
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Damage" 
                            FieldName="dam_sales" 
                            VisibleIndex="7"
                            Width="10%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Mechanical Repair" 
                            FieldName="mec_sales" 
                            VisibleIndex="8"
                            Width="10%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Routine Maintenance" 
                            FieldName="rom_sales" 
                            VisibleIndex="9"  
                            Width="10%" 
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Extended Maintenance" 
                            FieldName="exm_sales" 
                            VisibleIndex="10"
                            Width="11%"   
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Additional Product" 
                            FieldName="oth_sales" 
                            VisibleIndex="11" 
                            Width="10%"  
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                            Caption="Total" 
                            FieldName="tot_sales" 
                            VisibleIndex="12" 
                            Width="9%"  
                            ExportWidth="150">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total Customers: {0}" FieldName="Customer Name" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="acc_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="dam_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="mec_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="rom_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="exm_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="oth_sales" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="tot_sales" SummaryType="Sum" />
            </TotalSummary>

            <Templates>
                 <DetailRow>
                     <dx:ASPxGridView 
            ID="gridSalesLevel2" 
            onBeforePerformDataSelect ="gridSalesLevel2_BeforePerformDataSelect"
            onload="gridstylesFalse"
            cssclass="grid_styles"
            runat="server" 
            DataSourceID="dsViewSalesLevel2"  
            AutoGenerateColumns="False"  
            Styles-Header-HorizontalAlign="Center" 
            Styles-Cell-HorizontalAlign="Center" 
            Style="position: relative; left: 5px" Width="100%" >
       
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="16">
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign="Center" />

            <Settings 
                ShowFilterRow="False" 
                ShowFilterRowMenu="False" 
                ShowGroupedColumns="False" 
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False" 
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" 
                ShowFooter="True" />

            <SettingsBehavior 
                AllowSort="True" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <Columns>
        
                    <dx:GridViewDataDateColumn 
                        Caption="Invoice Date" 
                        FieldName="invoicedate" 
                        ReadOnly="True" 
                        VisibleIndex="0" 
                        Width="16%"  
                        ExportWidth="200">
                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                            </PropertiesDateEdit>
                      </dx:GridViewDataDateColumn>

                 <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                



                     <dx:GridViewDataTextColumn 
                        Caption="Quantity" 
                        FieldName="quantity" 
                        ReadOnly="True" 
                        VisibleIndex="2" 
                        Width="16%"  
                        ExportWidth="200">
                            <PropertiesTextEdit DisplayFormatString="#,##0">
                            </PropertiesTextEdit>
                     </dx:GridViewDataTextColumn>
 
                      <dx:GridViewDataTextColumn 
                        Caption="Sale Value" 
                        FieldName="salevalue" 
                        ReadOnly="True" 
                        VisibleIndex="3" 
                        Width="16%"  
                        ExportWidth="200">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>


                      <dx:GridViewDataTextColumn 
                        Caption="Cost Value" 
                        FieldName="costvalue" 
                        ReadOnly="True" 
                        VisibleIndex="4" 
                        Width="16%"  
                        ExportWidth="200">
                            <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                      <dx:GridViewDataTextColumn 
                        Caption="Margin" 
                        FieldName="margin" 
                        ReadOnly="True" 
                        VisibleIndex="5" 
                        Width="16%" >
                            <PropertiesTextEdit DisplayFormatString="0.0%">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>

                        
                </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total Invoices: {0}" FieldName="invoicenumber" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="salevalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costvalue" SummaryType="Sum" />
            </TotalSummary>

               </dx:ASPxGridView>
                 </DetailRow>
            </Templates>

            <SettingsDetail ShowDetailRow="True" ExportMode="Expanded"  AllowOnlyOneMasterRowExpanded="true" />

        </dx:ASPxGridView>
                 </DetailRow>
            </Templates>

            <SettingsDetail ShowDetailRow="True" ExportMode="Expanded"  AllowOnlyOneMasterRowExpanded="true" />

        </dx:ASPxGridView>
       
    </div>
    
    <asp:SqlDataSource ID="dsViewSalesLevel0" runat="server" SelectCommand="p_CustomerSummaryNissan_Level0" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsViewSalesLevel1" runat="server" SelectCommand="p_CustomerSummaryNissan_Level1" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nCustomerId" SessionField="CustomerId" Type="Int32" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsViewSalesLevel2" runat="server" SelectCommand="p_CustomerSummaryNissan_Level2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nCustomerDealerId" SessionField="CustomerDealerId" Type="Int32" />
            <asp:SessionParameter Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="MonthTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridSales" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>



