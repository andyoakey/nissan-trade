﻿Imports System.Data

Partial Class ViewVCInvoice

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSQL As String
    Dim nRetailValue As Double
    Dim nDiscountValue As Double

    Protected Sub dsInvoiceDetailParts_Init(sender As Object, e As EventArgs) Handles dsInvoiceDetailParts.Init
        dsInvoiceDetailParts.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("SearchEventId") = Request.QueryString(0)
        If Not IsPostBack Then
            Call FetchInvoiceDetails()
        End If
    End Sub

    Sub FetchInvoiceDetails()

        sSQL = "SELECT TOP 1 * FROM VALUE_CHAIN_LIVE..v_VC_EventWhole WHERE EventId = " & Trim(Str(Session("SearchEventId")))
        ds = da.ExecuteSQL(sErr, sSQL)

        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)

                lblDealerCode.Text = ("" & .Item("DealerCode"))
                lblCentreName.Text = ("" & .Item("CentreName"))
                lblInvoiceDate.Text = ("" & .Item("InvoiceDate"))
                lblInvoiceNumber.Text = ("" & .Item("InvoiceNumber"))
                lblDepartment.Text = ("" & .Item("Department"))
                lblSaleType.Text = ("" & .Item("SaleType"))

                lblAccountName.Text = FormatName(("" & .Item("AccountName")), "", "", "", "")

                lblAccountAddress.Text = FormatAddress(("" & .Item("AccountAddress1")), _
                                                                            ("" & .Item("AccountAddress2")), _
                                                                            ("" & .Item("AccountAddress3")), _
                                                                            ("" & .Item("AccountAddress4")), _
                                                                            ("" & .Item("AccountAddress5")), _
                                                                            ("" & .Item("AccountPostCode")), True)

                lblAccountCode.Text = ("" & .Item("AccountCode"))

                lblCompanyName.Text = FormatName(("" & .Item("CompanyName")), "", "", "", "")

                lblCompanyAddress.Text = FormatAddress(("" & .Item("CompanyAddress1")), _
                                                                            ("" & .Item("CompanyAddress2")), _
                                                                            ("" & .Item("CompanyAddress3")), _
                                                                            ("" & .Item("CompanyAddress4")), _
                                                                            ("" & .Item("CompanyAddress5")), _
                                                                            ("" & .Item("CompanyPostCode")), True)

                lblCompanyMagicNumber.Text = ("" & .Item("CompanyMagicNumber"))

                lblTotal.Text = Format(Val("" & .Item("TotalInvoiceValue")), "£#,##0.00")

            End With
        End If

    End Sub

    Protected Sub gridInvoiceDetailParts_CustomSummaryCalculate(sender As Object, e As DevExpress.Data.CustomSummaryEventArgs) Handles gridInvoiceDetailParts.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nRetailValue = 0
            nDiscountValue = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nRetailValue = nRetailValue + e.GetValue("RetailValue")
            nDiscountValue = nDiscountValue + e.GetValue("DiscountValue")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then

            Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

            If objSummaryItem.FieldName = "DiscountPerc" Then
                If nRetailValue <> 0 And nDiscountValue <> 0 Then
                    e.TotalValue = (nDiscountValue / nRetailValue)
                Else
                    e.TotalValue = 0
                End If
            End If

        End If

    End Sub
End Class
