<%@ Page Language="VB" AutoEventWireup="false"    MasterPageFile="~/MasterPage.master" CodeFile="TradePriceSupport.aspx.vb" Inherits="TradePriceSupport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

 <asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

<div style="position: relative; font-family: Calibri; text-align: left;" >
    
    <div id="divTitle" style="position:relative; left:0px">
           <table width ="100%">
            <tr>
                <td width ="78%" align="left">
                     <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Trade Price Support" />
                </td>

       
                <td width ="8%"  align="right" valign ="top" >
                    <dx:ASPxLabel 
                    ID="lblSalesPeriod" 
                    text=" Sales Period"
                    runat ="server" 
                    />
                </td>
    
                <td width ="6%" align="right">
                     <dx:ASPxComboBox
    ID="cboMonthFrom"
    AutoPostBack="true"
    SelectedIndex="0"
    DataSourceID="dsMonths"
    TextField="periodname" 
    ValueField="id"  
    runat="server" >
 </dx:ASPxComboBox>
                </td>

    <br />

                <td width ="2%"  align="center" valign ="top">
                    <dx:ASPxLabel 
                    ID="lblTo" 
                    text=" To "
                    runat ="server" 
                    />
                </td>
                
                <td width ="6%" align="left">
                     <dx:ASPxComboBox
    ID="cboMonthTo"
    AutoPostBack="true"
    SelectedIndex="0"
    DataSourceID="dsMonths"
    TextField="periodname" 
    ValueField="id" 
     runat="server" >
 </dx:ASPxComboBox>
                </td>
            </tr>
        </table>




        <table>
                <tr>
                <td align="right" style="width: 100%" >
                    <dx:ASPxLabel 
                    ID="lblSubTitle" 
                    Font-Italic="true"
                    Font-Size="XX-Small"
                    runat ="server" 
                        width ="200px"
                         Wrap="False"
                    />
                </td>
            </tr>

    </table>

    </div>
     
<dx:ASPxPageControl  
            ID="ASPxPageControl1" 
            runat="server"
            TabStyle-Wrap="False"
            EnableTabScrolling="true"
            width="100%"
            TabAlign="Justify"
    CssClass="page_tabs">

                <TabPages>

                    <%-- View By Dealer Grid --%>  
                    <dx:TabPage Text="View By Dealer And Account">
                        <ContentCollection>
                            <dx:ContentControl>
 
                                  <dx:ASPxGridView 
        ID="gridSummary_Dealer" 
        OnLoad="GridStyles"
        KeyFieldName="dealercode" 
        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
        runat="server" 
        DataSourceID="dsSummary_Dealer" 
        AutoGenerateColumns="False"  
        settings-header-wrap ="False"
        Width="100%">
     
        <Settings 
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False"  
            ShowFooter="true"/>
     
        <Styles>
            <Footer HorizontalAlign="Center"/>
        </Styles>

        <SettingsPager 
            PageSize="15" 
            AllButton-Visible="true" >
        </SettingsPager>

        <SettingsDetail 
                AllowOnlyOneMasterRowExpanded="True" 
                ExportMode="Expanded"
                ShowDetailRow="True" />
       
        <Columns>
                 <dx:GridViewDataTextColumn 
                FieldName ="dealercode"
                Visible="false" >
        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                CellStyle-HorizontalAlign="Left"
                CellStyle-wrap="False"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="dealer"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="250"
                Width= "24%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="1" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="2" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Payable" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebate_payable"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non-Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="5" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="7" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_rebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="8" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="12 Mth Uplift* " Name="bandUplift">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>
                </Columns>
             </dx:GridViewBandColumn>
          </Columns>

        <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="rebated_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebate_payable" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="missed_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="uplift_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="uplift_invoice" SummaryType="Sum" />
        </TotalSummary>
        
        <Templates>

                <DetailRow>

                    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="968px" TabStyle-Height="30px">

                        <TabPages >

                            <dx:TabPage Text="Rebated Sales">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl1" runat="server">
                                        <br />
                                            <dx:ASPxGridView 
                            ID="gridInvoicesDealerGood" 
                            runat="server" 
                            Styles-Header-Wrap="True"
                            OnLoad="GridStyles2"
                            SettingsBehavior-ProcessSelectionChangedOnServer="true"
                            AutoGenerateColumns="False"  
                            DataSourceID="dsInvoicesDealer_Good" 
                            KeyFieldName="keyfield" 
                            OnBeforePerformDataSelect="gridInvoices_BeforePerformDataSelect"
                            OnHtmlDataCellPrepared="gridInvoices_HtmlDataCellPrepared"            
                            OnUnload="gridInvoicesGood_Unload"           
                            ClientInstanceName="gridInvoices" 
                            Width="980px">

                            <Settings  
                                UseFixedTableLayout="True" 
                                ShowFilterRowMenu="False" 
                                ShowFilterRow="False"
                                ShowFooter="True"  
                                ShowHeaderFilterButton="False"
                                ShowStatusBar="Hidden" 
                                ShowTitlePanel="False" />
   
                            <SettingsPager 
                                AlwaysShowPager="False" 
                                AllButton-Visible="true"
                                PageSize="12" 
                                ShowDefaultImages="True">
                             </SettingsPager>
    
                            <SettingsDetail 
                                AllowOnlyOneMasterRowExpanded="True" 
                                ShowDetailRow="True" />

                            <Styles>
                                  <Footer HorizontalAlign="Center"/>
                            </Styles>

                            <Columns>
                         
                                <dx:GridViewDataTextColumn                                     
                                    FieldName="keyfield" 
                                    Visible="false">
                                </dx:GridViewDataTextColumn>
                                              
                                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Account Details">
                <Columns>
                                <dx:GridViewDataTextColumn 
                                    FieldName="accountcode" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="A/C Code"
                                    VisibleIndex="0" 
                                    width ="6%"
                                    exportwidth ="100"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                                    FieldName="businessname" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Name"
                                    CellStyle-Wrap="false"
                                    VisibleIndex="1" 
                                    exportwidth ="200"
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                                    FieldName="posttown" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    CellStyle-Wrap="false"
                                    exportwidth ="200"
                                    Caption="Town"
                                    VisibleIndex="2" 
                                    width ="10%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>
                    
                            </Columns>
             </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Product Details">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Partnumber" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="partnumber"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "8%"
                VisibleIndex="4" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Part Description" 
                CellStyle-HorizontalAlign="Center"
                CellStyle-Wrap="False"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="partdescription"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "10%"
                VisibleIndex="5" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Product Group" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="productcode"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>
                               
                               <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sale Details">
                <Columns>
           
                    <dx:GridViewDataTextColumn                                     
                                    FieldName="transactiondate" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Date"
                                    VisibleIndex="7"
                                    exportwidth ="100"
                                    width ="8%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                                    FieldName="invoicenumber" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Invoice No."
                                    VisibleIndex="8"
                                    exportwidth ="100" 
                                    width ="7%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>
                     
               <dx:GridViewDataTextColumn 
                Caption="Qty" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "4%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Total" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="totalnettvalue"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Each" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="nettprice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="11" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Campaign Price" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="campaignprice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="12" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Earned" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="applicablerebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="13" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    
                                </Columns>
             </dx:GridViewBandColumn>
                   
                     </Columns>
      
                            <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="units" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="totalnettvalue" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="applicablerebate" SummaryType="Sum" />




                        </TotalSummary>

                            <Templates>
                                <DetailRow>

                            <dx:ASPxGridView 
                            ID="gridTransactions" 
                            runat="server" 
                            Styles-Header-Wrap="True"
                            OnHtmlDataCellPrepared="gridTransactions_HtmlDataCellPrepared"
                            OnLoad="GridStyles2"
                            SettingsBehavior-ProcessSelectionChangedOnServer="true"
                            AutoGenerateColumns="False"  
                            DataSourceID="dsTransactions" 
                            OnBeforePerformDataSelect="gridTransactions_BeforePerformDataSelect"
                            ClientInstanceName="gridTransactions" 
                            Width="920px">

                            <Settings  
                                UseFixedTableLayout="True" 
                                ShowFilterRowMenu="False" 
                                ShowFilterRow="False"
                                ShowFooter="True"  
                                ShowHeaderFilterButton="False"
                                ShowStatusBar="Hidden" 
                                ShowTitlePanel="False" />
   
                            <SettingsPager 
                                AlwaysShowPager="False" 
                                AllButton-Visible="true"
                                PageSize="12" 
                                ShowDefaultImages="True">
                             </SettingsPager>
    
                            <Styles>
                                  <Footer HorizontalAlign="Center"/>
                            </Styles>

                            <Columns>

                                 <dx:GridViewDataTextColumn                                     
                                    FieldName="colour_flag" 
                                    Visible="false" />
                                

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="partnumber" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Part Number"
                                    VisibleIndex="0" 
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="partsdescription" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Parts Description"
                                    VisibleIndex="1" 
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="level2_code" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Product Code"
                                    VisibleIndex="2" 
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="quantity" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Qty"
                                    VisibleIndex="3" 
                                    width ="10%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="totalnettvalue" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Total Sales"
                                    VisibleIndex="4" 
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="unitprice" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Unit Sale Price"
                                    VisibleIndex="5" 
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="campaignprice" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Campaign Price"
                                    VisibleIndex="5" 
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="rebate_value" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Rebate Value"
                                    VisibleIndex="5" 
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>


                             </Columns>
                        
                            <TotalSummary>
                                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="quantity" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="totalnettvalue" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="rebate_value" SummaryType="Sum" />
                            </TotalSummary>
                            
                            </dx:ASPxGridView>

                                </DetailRow>
                            </Templates>


                        </dx:ASPxGridView>
                                        <br />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                            <dx:TabPage Text="Non-Rebated Sales">
                                <ContentCollection>
                                    <dx:ContentControl ID="ContentControl2" runat="server">
                                        <br />
                                            <dx:ASPxGridView 
                            ID="gridInvoicesDealer_Bad" 
                            runat="server" 
                            Styles-Header-Wrap="True"
                            OnLoad="GridStyles2"
                            SettingsBehavior-ProcessSelectionChangedOnServer="true"
                            AutoGenerateColumns="False"  
                            DataSourceID="dsInvoicesDealer_Bad" 
                            KeyFieldName="keyfield" 
                            OnHtmlDataCellPrepared="gridInvoices_HtmlDataCellPrepared"                       
                            OnBeforePerformDataSelect="gridInvoices_BeforePerformDataSelect"
                            ClientInstanceName="gridInvoices" 
                            Width="980px">

                            <Settings  
                                UseFixedTableLayout="True" 
                                ShowFilterRowMenu="False" 
                                ShowFilterRow="False"
                                ShowFooter="True"  
                                ShowHeaderFilterButton="False"
                                ShowStatusBar="Hidden" 
                                ShowTitlePanel="False" />
   
                            <SettingsPager 
                                AlwaysShowPager="False" 
                                AllButton-Visible="true"
                                PageSize="12" 
                                ShowDefaultImages="True">
                             </SettingsPager>
    
                            <SettingsDetail 
                                AllowOnlyOneMasterRowExpanded="True" 
                                ShowDetailRow="True" />

                            <Styles>
                                  <Footer HorizontalAlign="Center"/>
                            </Styles>

                            <Columns>
                         
                                <dx:GridViewDataTextColumn                                     
                                    FieldName="keyfield" 
                                    Visible="false">
                                </dx:GridViewDataTextColumn>
                                              
                                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Account Details">
                <Columns>
                                <dx:GridViewDataTextColumn 
                                    FieldName="accountcode" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="A/C Code"
                                    VisibleIndex="0" 
                                    width ="6%"
                                    exportwidth ="100"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                                    FieldName="businessname" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Name"
                                    CellStyle-Wrap="false"
                                    VisibleIndex="1" 
                                    exportwidth ="200"
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn 
                                    FieldName="posttown" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    CellStyle-Wrap="false"
                                    Caption="Town"
                                    VisibleIndex="2" 
                                    width ="10%"
                                    exportwidth ="200"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>
                    
                            </Columns>
             </dx:GridViewBandColumn>

                                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Product Details">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Partnumber" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="partnumber"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "8%"
                VisibleIndex="4" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Part Description" 
                CellStyle-HorizontalAlign="Center"
                CellStyle-Wrap="False"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="partdescription"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "10%"
                VisibleIndex="5" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Product Group" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="productcode"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>
                               
                               <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non Rebated Sale Details">
                <Columns>
           
                    <dx:GridViewDataTextColumn                                     
                                    FieldName="transactiondate" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Date"
                                    VisibleIndex="7"
                                    width ="8%"
                                    exportwidth ="100"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                                    FieldName="invoicenumber" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Invoice No."
                                    VisibleIndex="8" 
                                    exportwidth ="100"
                                    width ="7%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>
                     
                    <dx:GridViewDataTextColumn 
                Caption="Qty" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "4%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Total" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="totalnettvalue"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Each" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="nettprice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="11" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Campaign Price" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="campaignprice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="12" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="applicablerebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="100"
                Width= "7%"
                VisibleIndex="13" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    
                                </Columns>
             </dx:GridViewBandColumn>
                   
                     </Columns>
      
                            <TotalSummary>
                            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="units" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="totalnettvalue" SummaryType="Sum" />
                            <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="applicablerebate" SummaryType="Sum" />
                        </TotalSummary>

                            <Templates>
                                <DetailRow>
                                        <dx:ASPxGridView 
                            ID="gridTransactions" 
                            runat="server" 
                            Styles-Header-Wrap="True"
                            OnHtmlDataCellPrepared="gridTransactions_HtmlDataCellPrepared"
                            OnLoad="GridStyles2"
                            SettingsBehavior-ProcessSelectionChangedOnServer="true"
                            AutoGenerateColumns="False"  
                            DataSourceID="dsTransactions" 
                            OnBeforePerformDataSelect="gridTransactions_BeforePerformDataSelect"
                            ClientInstanceName="gridTransactions" 
                            Width="920px">

                            <Settings  
                                UseFixedTableLayout="True" 
                                ShowFilterRowMenu="False" 
                                ShowFilterRow="False"
                                ShowFooter="True"  
                                ShowHeaderFilterButton="False"
                                ShowStatusBar="Hidden" 
                                ShowTitlePanel="False" />
   
                            <SettingsPager 
                                AlwaysShowPager="False" 
                                AllButton-Visible="true"
                                PageSize="12" 
                                ShowDefaultImages="True">
                             </SettingsPager>
    
                            <Styles>
                                  <Footer HorizontalAlign="Center"/>
                            </Styles>

                            <Columns>

                                 <dx:GridViewDataTextColumn                                     
                                    FieldName="colour_flag" 
                                    Visible="false" />
                                

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="partnumber" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Part Number"
                                    VisibleIndex="0" 
                                    ExportWidth="100"
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="partsdescription" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Parts Description"
                                    VisibleIndex="1" 
                                    ExportWidth="100"
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="level2_code" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Product Code"
                                    VisibleIndex="2" 
                                    ExportWidth="100"
                                    width ="15%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="quantity" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Units"
                                    VisibleIndex="3" 
                                    ExportWidth="100"
                                    width ="10%"
                                    Visible="true">
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="totalnettvalue" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    ExportWidth="100"
                                    Caption="Total Sales"
                                    VisibleIndex="4" 
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="unitprice" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Unit Sale Price"
                                    VisibleIndex="5" 
                                    ExportWidth="100"
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="campaignprice" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Campaign Price"
                                    VisibleIndex="5" 
                                    ExportWidth="100"
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn                                     
                                    FieldName="rebate_value" 
                                    HeaderStyle-HorizontalAlign="Center"
                                    CellStyle-HorizontalAlign="Center"
                                    Caption="Rebate Value"
                                    VisibleIndex="5" 
                                    ExportWidth="100"
                                    width ="10%"
                                    Visible="true">
                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
                                </dx:GridViewDataTextColumn>


                             </Columns>
                        
                            <TotalSummary>
                                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="quantity" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="totalnettvalue" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00"  FieldName="rebate_value" SummaryType="Sum" />
                            </TotalSummary>
                            
                            </dx:ASPxGridView>
                                </DetailRow>
                            </Templates>


                        </dx:ASPxGridView>
                                        <br />
                                    </dx:ContentControl>
                                </ContentCollection>
                            </dx:TabPage>

                        </TabPages>
                        </dx:ASPxPageControl>

                </DetailRow>

            </Templates>

 </dx:ASPxGridView> 
                    
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%-- End of View By Dealer Grid --%>  
       
                    <%-- View By Product Group Grid --%>  
                    <dx:TabPage Text="View By Product Group">
                        <ContentCollection>
                            <dx:ContentControl>
      
                                 <dx:ASPxGridView 
        ID="gridSummary_PG" 
        OnLoad="GridStyles"
        KeyFieldName="level2_code" 
        runat="server" 
        DataSourceID="dsSummary_PG" 
        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
        AutoGenerateColumns="False"  
        settings-header-wrap ="False"
        Width="100%">
     
        <Settings 
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False"  
            ShowFooter="true"/>
     
        <Styles>
            <Footer HorizontalAlign="Center"/>
        </Styles>

        <SettingsPager 
            PageSize="15" 
            AllButton-Visible="true" >
        </SettingsPager>

        <SettingsDetail 
                AllowOnlyOneMasterRowExpanded="True" 
                ExportMode  ="Expanded"
                ShowDetailRow="True" />
       
        <Columns>
               <dx:GridViewDataTextColumn 
                     FieldName ="dealercode"
                   Visible="false" >
              </dx:GridViewDataTextColumn>

             <dx:GridViewDataTextColumn 
                Caption="Product Group" 
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="productgroup"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="350"
                Width= "24%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="1" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="2" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Payable" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebate_payable"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non-Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="5" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="7" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_rebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="8" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="12 Mth Uplift* " Name="bandUplift">
               <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>
                </Columns>
             </dx:GridViewBandColumn>
          </Columns>

        <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="rebated_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebate_payable" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="missed_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="uplift_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="uplift_invoice" SummaryType="Sum" />



        </TotalSummary>
        
        <Templates>
                <DetailRow>
                        <%-- View One PG By Dealer Grid --%>  
                        <dx:ASPxGridView 
        ID="gridSummary_PG_Dealer" 
        OnLoad="GridStyles"
        KeyFieldName="dealercode" 
        runat="server" 
        DataSourceID="dsSummary_PG_Dealer" 
        OnBeforePerformDataSelect="gridSummary_PG_Dealer_BeforePerformDataSelect"
        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
        AutoGenerateColumns="False"  
        settings-header-wrap ="False"
        Width="100%">
     
        <Settings 
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False"  
            ShowFooter="true"/>
     
        <Styles>
            <Footer HorizontalAlign="Center"/>
        </Styles>

        <SettingsPager 
            PageSize="15" 
            AllButton-Visible="true" >
        </SettingsPager>

        <Columns>
                 <dx:GridViewDataTextColumn 
                FieldName ="dealercode"
                Visible="false" >
        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                CellStyle-HorizontalAlign="Left" 
                CellStyle-Wrap="False"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="dealer"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="250"
                Width= "24%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="1" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="2" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Payable" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebate_payable"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non-Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="5" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="7" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_rebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="8" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

               <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="12 Mth Uplift* " Name="bandUplift">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>
                </Columns>
             </dx:GridViewBandColumn>
          </Columns>

        <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="rebated_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebate_payable" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="missed_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="uplift_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="uplift_invoice" SummaryType="Sum" />
        </TotalSummary>
        
 </dx:ASPxGridView> 
                        <%-- End of View One PG By Dealer Grid --%>  
                </DetailRow>
         </Templates>

 </dx:ASPxGridView> 
                                
                              </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%-- End of View By Product Group Grid --%>  
       
                    <%-- View By PartNumber Grid --%>  
                    <dx:TabPage Text ="View By Part Number">
                        <ContentCollection>
                            <dx:ContentControl>

                             <dx:ASPxGridView 
        ID="gridSummary_Part" 
        OnLoad="GridStyles"
        KeyFieldName="partnumber" 
        runat="server" 
        DataSourceID="dsSummary_Part" 
        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
        AutoGenerateColumns="False"  
        settings-header-wrap ="False"
        Width="100%">
     
        <Settings 
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False"  
            ShowFooter="true"/>
     
        <Styles>
            <Footer HorizontalAlign="Center"/>
        </Styles>

        <SettingsPager 
            PageSize="15" 
            AllButton-Visible="true" >
        </SettingsPager>

        <SettingsDetail 
                AllowOnlyOneMasterRowExpanded="True" 
                ExportMode="Expanded"
                ShowDetailRow="True" />
       
        <Columns>
               <dx:GridViewDataTextColumn 
                     FieldName ="dealercode"
                   Visible="false" >
              </dx:GridViewDataTextColumn>

             <dx:GridViewDataTextColumn 
                Caption="Part Number" 
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="partnumber"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="150"
                Width= "10%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>

             <dx:GridViewDataTextColumn 
                Caption="Description" 
                CellStyle-HorizontalAlign="Left"
                CellStyle-Wrap="False"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="partdesc"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="200"
                Width= "10%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>


                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="1" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="2" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Payable" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebate_payable"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non-Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="5" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="7" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_rebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="8" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>
                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="12 Mth Uplift* " Name="bandUplift">
              <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>
                </Columns>
             </dx:GridViewBandColumn>
          </Columns>

        <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="rebated_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebate_payable" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="missed_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="uplift_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="uplift_invoice" SummaryType="Sum" />



        </TotalSummary>
        
        <Templates>

                     <%-- View One Part By Dealer Grid --%>  
                    <DetailRow>
                         <dx:ASPxGridView 
        ID="gridSummary_Part_Dealer" 
        OnLoad="GridStyles"
        KeyFieldName="dealercode" 
        runat="server" 
        DataSourceID="dsSummary_Part_Dealer" 
        OnBeforePerformDataSelect="gridSummary_Part_Dealer_BeforePerformDataSelect"
        OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
        AutoGenerateColumns="False"  
        settings-header-wrap ="False"
        Width="100%">
     
        <Settings 
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False"  
            ShowFooter="true"/>
     
        <Styles>
            <Footer HorizontalAlign="Center"/>
        </Styles>

        <SettingsPager 
            PageSize="15" 
            AllButton-Visible="true" >
        </SettingsPager>

        <Columns>
                 <dx:GridViewDataTextColumn 
                FieldName ="dealercode"
                Visible="false" >
        </dx:GridViewDataTextColumn>

                 <dx:GridViewDataTextColumn 
                Caption="Dealer" 
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left"
                FieldName ="dealer"
                CellStyle-Wrap="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                ExportWidth="250"
                Width= "24%"
                VisibleIndex="0" >
            </dx:GridViewDataTextColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="1" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="2" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebated_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Payable" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="rebate_payable"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="3" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                 <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="Non-Rebated Sales">
                <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="5" >
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="6" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Cost To Dealer" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_cost"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="7" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                Caption="Rebate Missed" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="missed_rebate"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="8" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

                </Columns>
             </dx:GridViewBandColumn>

                    <dx:GridViewBandColumn HeaderStyle-HorizontalAlign ="Center" Caption="12 Mth Uplift* " Name="bandUplift">        <Columns>
                   <dx:GridViewDataTextColumn 
                Caption="Units" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_units"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "7%"
                VisibleIndex="9" >
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                Caption="Sale Value" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                FieldName ="uplift_invoice"
                Settings-AllowHeaderFilter="False"
                ExportWidth="150"
                Width= "8%"
                VisibleIndex="10" >
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>
                </Columns>
             </dx:GridViewBandColumn>
          </Columns>

        <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="rebated_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebated_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="rebate_payable" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="missed_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_invoice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_cost" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="missed_rebate" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="uplift_units" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="uplift_invoice" SummaryType="Sum" />
        </TotalSummary>
        
 </dx:ASPxGridView> 
                    </DetailRow>
                     <%-- End of View One Part By Dealer Grid --%>  
   
            </Templates>

 </dx:ASPxGridView> 
 
                            </dx:ContentControl>
                        </ContentCollection>
                    </dx:TabPage>
                    <%-- End of View By PartNumber Grid --%>  

                 </TabPages>

     

</dx:ASPxPageControl>

</div>



 <%-- DataSources And Exporters --%>
<div>
 <asp:SqlDataSource 
        ID="dsSummary_Dealer" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Dealer_Summary"
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="nUserID" SessionField="UserID" Type="Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
            <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
            <asp:SessionParameter Name="slevel2_code"  DefaultValue="ALL" Type="String"/>
            <asp:SessionParameter Name="spartnumber"  DefaultValue=" " Type="String"/>
        </SelectParameters>
 </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsSummary_PG_Dealer" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Dealer_Summary"
        SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="nUserID" SessionField="UserID" Type="Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
            <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="String" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
            <asp:SessionParameter Name="slevel2_code"  SessionField="TPSLevel2_Code"  Type="String"/>
            <asp:SessionParameter Name="spartnumber"  DefaultValue=" " Type="String"/>
        </SelectParameters>
 </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsSummary_Part_Dealer" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Dealer_Summary"
        SelectCommandType="StoredProcedure" >
        <SelectParameters>
            <asp:SessionParameter Name="nUserID" SessionField="UserID" Type="Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
            <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="Int32" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
            <asp:SessionParameter Name="slevel2_code"  DefaultValue=" "  Type="String"/>
            <asp:SessionParameter Name="spartnumber"  SessionField="TPPartNumber" Type="String"/>
        </SelectParameters>
 </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsSummary_PG" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_PG_Summary"
        SelectCommandType="StoredProcedure">
    <SelectParameters>
            <asp:SessionParameter Name="nUserID" SessionField="UserID" Type="Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
            <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="Int32" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
        </SelectParameters>
 </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsSummary_Part" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Part_Summary"
        SelectCommandType="StoredProcedure"> 
        <SelectParameters>
             <asp:SessionParameter Name="nUserID" SessionField="UserID" Type="Int32" />
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" />
            <asp:SessionParameter Name="sSelectionID" SessionField="SelectionID" Type="Int32" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
     </SelectParameters>
 </asp:SqlDataSource> 
 
 <asp:SqlDataSource 
        ID="dsInvoicesDealer_Good" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Invoice" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="TPSDealerCode" Type="String" />
            <asp:Parameter Name ="nQualifying_flag" DefaultValue ="1" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
      </SelectParameters>
    </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsInvoicesDealer_Bad" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Invoice" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sDealerCode" SessionField="TPSDealerCode" Type="String" />
            <asp:Parameter Name ="nQualifying_flag" DefaultValue ="0" />
            <asp:SessionParameter Name="nFrom" SessionField="TPSFrom" Type="Int32" />
            <asp:SessionParameter Name="nTo" SessionField="TPSTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

 <asp:SqlDataSource 
        ID="dsTransactions" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_TradeSupport_Transaction" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sRefCode"   SessionField  ="TPSRefCode" Type="String" />
            <asp:SessionParameter Name="sInvoiceNumber" SessionField="TPSInvoiceNumber" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsMonths" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="p_Get_Months" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

          
 <dx:ASPxGridViewExporter 
    ID="ASPxGridViewExporter1" 
    GridViewID="gridSummary_Dealer"
    runat="server">
 </dx:ASPxGridViewExporter>

 <dx:ASPxGridViewExporter 
    ID="ASPxGridViewExporter2" 
    GridViewID="gridSummary_PG"
    runat="server">
 </dx:ASPxGridViewExporter>

 <dx:ASPxGridViewExporter 
    ID="ASPxGridViewExporter3" 
    GridViewID="gridSummary_Part"
    runat="server">
 </dx:ASPxGridViewExporter>
</div>

</asp:Content>