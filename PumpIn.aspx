﻿<%@ Page AutoEventWireup="false" CodeFile="PumpIn.aspx.vb" Inherits="PumpIn"   Language="VB" MasterPageFile="~/MasterPage.master" Title="Postcode Pump-In" %>

<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxtl" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <div style="position: relative; font-family: Calibri; text-align: left;" >

  <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Postcode Pump In Analysis" />
  </div>

  <table width ="100%">
      <tr>
<%--           <td width ="20%">
                 <dx:ASPxComboBox
                      ID="cboSalesType" 
                      AutoPostBack="true"
                      runat="server" >
                    <Items>
                        <dx:ListEditItem Text ="Total Sales"  Value = "1" Selected= "true" /> 
                        <dx:ListEditItem Text ="Mechanical Repair Sales" Value = "2" /> 
                        <dx:ListEditItem Text ="Total Sales Excluding Damage" Value = "3" /> 
                    </Items>
                </dx:ASPxComboBox>
            </td>--%>

            <td width ="20%">
                <div style="margin-left: 3px; margin-top: 10px;">
                     <dx:ASPxButton  
                           ID="btnRunQuery" 
                           runat="server" 
                           AutoPostBack="true"
                           text = "Run Query"
                           tooltip="Run the Query to show results in a Grid"/>
                </div>
            </td>

            <td width ="60%" align="right">
                  <dx:ASPxLabel 
                        ID="lbl12mths" 
                        runat="server" 
                        Text="Shows total sales for last 12 complete months" 
                        Font-Italic="true"
                        Font-Size="Smaller"/>
           </td>
      </tr>
  </table>
  
      <dx:ASPxPageControl 
            ID="tabPageControl" 
            runat="server" 
            ActiveTabIndex="0" 
            Height="500px" 
            Width ="100%" 
            SaveStateToCookies="True"
            CssClass="page_tabs">
            <TabPages>
            <dx:TabPage Name="Select PostCodes" Text="Select PostCodes">
                <ContentCollection>
                            <dx:ContentControl ID="ContentControl1" runat="server"> 
                                <table>
                                <tr>
                                        <td valign="top" width = "40%">
                                             <dx:ASPxLabel 
                                                 CssClass="textnormal"
                                                 runat="server" 
                                                 Text="Available">
                                            </dx:ASPxLabel>
                                        </td>   
                                        
                                        <td valign="top" width = "8%" >
                                              &nbsp;
                                        </td>   

                                        <td valign="top" width = "55%">
                                             <dx:ASPxLabel 
                                                 CssClass="textnormal"
                                                 runat="server" 
                                                 Text="Selected">
                                            </dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                         <td valign="top" width = "40%">

        <dxtl:ASPxTreeList onload ="TreeListStyles"   Font-Size="12px" 
            ID="tlPostCodes"
            CssClass="textgrid"
            runat="server" 
            visible = "true"
            Settings-ShowColumnHeaders="false"
            EnableCallBacks="true"
            ClientInstanceName="tlpostcodes"
            ProcessSelectionChangedOnServer = "True"
            AutoGenerateColumns="False"  
            Styles-AlternatingRow-BackColor = "GhostWhite"
            Styles-AlternatingNode-Enabled="True"           
            DataSourceID="dsPostCodes" 
            Settings-GridLines="Both"
            KeyFieldName="id" 
            Width = "300px"
            ParentFieldName="parent_id" >
    
        <Settings SuppressOuterGridLines="True" />
        <SettingsBehavior AllowDragDrop="False" AllowSort="False"  />
        <SettingsSelection Enabled ="true" Recursive="true"/>
        <ClientSideEvents SelectionChanged="function(s, e) {callback.PerformCallback()}" />
        <Columns>
            <dxtl:TreeListDataColumn 
                FieldName="id" 
                Visible="False" 
                VisibleIndex="0" >
            </dxtl:TreeListDataColumn>

            <dxtl:TreeListDataColumn 
                FieldName="parent_id" 
                Visible="False" 
                VisibleIndex="1" >
            </dxtl:TreeListDataColumn>

            <dxtl:TreeListDataColumn 
                FieldName="displaytext" 
                Caption="Postcodes Available"
                Visible="True" 
                VisibleIndex="2" >
            </dxtl:TreeListDataColumn>
        </Columns>

<Styles>
<AlternatingNode Enabled="True"></AlternatingNode>
</Styles>
    </dxtl:ASPxTreeList>
                                        </td>   
                                        
                                        <td valign="top" width = "8%" >
                                              &nbsp;</td>   

                                        <td valign="top" width = "55%">
                                                <dx:ASPxMemo 
                                                        ID = "memPostCodeList"
                                                        CssClass="textnormal"
                                                        Height="400"
                                                        Width="100%"
                                                        ReadOnly="true"
                                                        runat="server" ClientInstanceName="memPostCodeList">
                                                </dx:ASPxMemo>
                                        </td>
                                    </tr>
                            </table>
                        </dx:ContentControl>
                </ContentCollection>
             </dx:TabPage>

            <dx:TabPage Name="Results" Text="Results">
                <ContentCollection>
                     <dx:ContentControl ID="ContentControl3" runat="server"> 
                             <dxp:ASPxPivotGrid 
                ID="gridPumpIn" 
                DataSourceID="dsPumpIn" 
                onload="PivotGridStyles"
                Font-Size="XX-Small"
                visible ="false"
                EnableTheming="True" 
               	OptionsCustomization-AllowDrag="false"
                OptionsCustomization-AllowFilter="false"
                OptionsCustomization-AllowSort="false"
                OptionsView-ShowFilterHeaders="False"
                OptionsBehavior-SortBySummaryDefaultOrder="Ascending"
                OptionsLayout-AddNewGroups="False"
                OptionsPager-Position="Bottom"
                AutoGenerateColumns="False" 
                Width="100%"
                runat="server"
                >
                
                <OptionsBehavior SortBySummaryDefaultOrder="Ascending"></OptionsBehavior>

                <OptionsPager Position="Bottom" RowsPerPage="15"></OptionsPager>

                <Styles>
                    <FieldValueStyle Wrap="False" />
                </Styles>
              
                <Fields>
                        <dxp:PivotGridField 
                            Area="ColumnArea" 
                            Width="100"
                            AreaIndex="0" 
                            Caption="Postcode" 
                            FieldName="postcode" 
                            GrandTotalText="Total All Postcodes"
                            ID="xpostcode"  
                            HeaderStyle-HorizontalAlign="Left">
                        </dxp:PivotGridField>

                        <dxp:PivotGridField 
                            Area="RowArea" 
                            Width="100"
                            AreaIndex="1" 
                            Caption="Dealer" 
                            GrandTotalText="Total All Dealers"
                            FieldName="dealer" 
                            ID="xdealer"  
                            HeaderStyle-HorizontalAlign="Left" >
                        </dxp:PivotGridField>

                        <dxp:PivotGridField 
                            Area="DataArea" 
                            Width="100"
                            AreaIndex="2" 
                            Caption="-"   
                            HeaderStyle-BackColor="Transparent"
                            HeaderStyle-Border-BorderStyle="None"
                            HeaderStyle-ForeColor="Transparent"
                            FieldName="salesvalue" 
                            HeaderStyle-BackgroundImage-ImageUrl=" "
                            ID="xsalesvalue"   
                            CellStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center">
                            <HeaderStyle Wrap="True" />
                        </dxp:PivotGridField>
              </Fields>

              <OptionsCustomization  AllowSort="False" AllowFilter="False" AllowDrag="False"></OptionsCustomization>
              <OptionsView ShowFilterHeaders="False"></OptionsView>
        </dxp:ASPxPivotGrid>

                     </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
      </dx:ASPxPageControl>
  
</div>

  <asp:SqlDataSource 
        ID="dsPumpIn" 
        runat="server" 
        SelectCommand="sp_PumpIn" 
        SelectCommandType="StoredProcedure"
           ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>">
       <SelectParameters>
                <asp:SessionParameter Name="nType" SessionField="SalesType" Type="Int32" />
                <asp:SessionParameter Name="sPostCodeList" SessionField="PostCodeList" Type="String" />
                <asp:SessionParameter Name="nTo" SessionField="nDataPeriod" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
   
  <asp:SqlDataSource 
        ID="dsPostCodes" 
        runat="server" 
        SelectCommand="sp_postcodehierarchy " 
        SelectCommandType="StoredProcedure"
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>">
    </asp:SqlDataSource>
    
  <dxp:ASPxPivotGridExporter 
        ID="ASPxPivotGridExporter1" runat="server" ASPxPivotGridID="gridPumpIn" > 
    </dxp:ASPxPivotGridExporter>
  
  <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="callback">
        <ClientSideEvents CallbackComplete="function(s, e) {memPostCodeList.SetText(s.cppostcodelist)}" />
    </dx:ASPxCallback>

</asp:Content>


