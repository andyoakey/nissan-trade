﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NewUser.aspx.vb" Inherits="NewUser" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative; left:5px">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Add A New User" />
        </div>
 
   
        <br />

        <table style="height: 100px;" id="trOrgDetails" width="100%">
        
            <tr runat="server" id="trEmailAddress">

                <td width="20%">
                   <dx:ASPxLabel 
                        ID="lblEmailAddress" 
                        runat ="server" 
                        Text="Email Address" Font-Size="13px" />
                </td>

                <td width="50%">
                    <dx:ASPxTextBox 
                        ID="txtEmailAddress" 
                        runat="server" 
                        Width="70%" 
                        TabIndex="3" />
                </td>

                <td width="30%">
                        <dx:ASPxLabel 
                            ID="lblErr" 
                            Text="" 
                            Visible="false"
                            Font-Size="X-Small"
                            Style="color: Red"
                            runat="server" />
                </td>
            </tr>
            
            <tr runat="server" id="trConfEmailAddress">
            
                <td width="20%">
                       <dx:ASPxLabel
                            ID="lblConfEmailAddress" 
                            runat ="server" 
                            Text="Confirm Email Address" Font-Size="13px" />
                </td>

                <td width="50%">
                    <dx:ASPxTextBox 
                        ID="txtConfEmailAddress" 
                        runat="server" 
                        Width="70%" 
                        TabIndex="3" />
                </td>
                
                <td width="30%">
                    <dx:ASPxLabel 
                            ID="lblErr2" 
                            Text="" 
                            Visible="false"
                            Font-Size="X-Small"
                            Style="color: Red"
                            runat="server" />
                </td>

            </tr>
            
            <tr runat="server" id="trName">
                 <td width="20%">
                       <dx:ASPxLabel
                            ID="lblName" 
                            runat ="server" 
                            Text="User Name" Font-Size="13px" />
                </td>
                
                 <td width="50%">
                        <dx:ASPxTextBox 
                            ID="txtName" 
                            runat="server" 
                            Width="70%" 
                            TabIndex="4" />
                </td>
                
                <td width="20%">
                  <dx:ASPxLabel 
                            ID="lblErr3" 
                            Text="" 
                            Visible="false"
                            Font-Size="X-Small"
                            Style="color: Red"
                            runat="server" />
                </td>
            </tr>
            
            <tr runat="server" id="trCentre">
                     <td width="20%">
                           <dx:ASPxLabel
                               ID="lblDealer" 
                               runat ="server" 
                               Text="Dealer" Font-Size="13px"/>
                     </td>
                
                     <td width="50%">
                         <dx:ASPxComboBox
                             ID="cboDealers" 
                             runat="server" 
                             SelectedIndex="-1"
                             AutoPostBack="True" 
                             Width="70%"   
                             DataSourceID="dsDealers" 
                             TextField="DealerName" 
                             ValueField="DealerCode" />
                    </td>
        
                    <td width="20%">
                          <dx:ASPxLabel 
                            ID="lblErr4" 
                            Text="" 
                            Visible="false"
                            Font-Size="X-Small"
                            Style="color: Red"
                            runat="server" />
                    </td>
            </tr>
            
         </table>
         
         <br />

         <dx:ASPxLabel  
            ID="lblRegDone" 
            Text="" 
            Font-Bold="true"   
            Visible="false"
            runat="server" />

         <br />
         <br />

         <table style="height: 20px;" id="tabButtons" width="750px">

            <tr runat="server" id="trUserRow9">

                <td width="10%">
                    <dx:ASPxButton 
                        ID="btnSaveReg" 
                        runat="server" 
                        Text="Send Request" 
                        Width="100%" Font-Size="13px"/>
                </td>

                <td width="10%">
                    <dx:ASPxButton 
                        ID="btnClearReg" 
                        runat="server" 
                        Text="Clear" 
                        Width="100%" Font-Size="13px" /> 
                </td>

                <td width="80%">
                </td>

            </tr>
        </table>

        <br />
    
    </div>

    <asp:SqlDataSource 
        ID="dsDealers" 
        runat="server" 
        SelectCommand="sp_DealerList" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

</asp:Content>


