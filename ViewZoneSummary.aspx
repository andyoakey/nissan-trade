﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewZoneSummary.aspx.vb" Inherits="ViewZoneSummary" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

       <div style="position: relative; font-family: Calibri; text-align: left;" >
   
            <div id="divTitle" style="position:relative; left:5px">
                <asp:Label ID="lblPageTitle" runat="server" Text="Zone Summary" />
            </div>
        
             <table id="trSelectionStuff" width="100%" style="margin-bottom:10px">
                <tr id="Tr1" runat="server" style="height:30px;">

                <td style="width:2%" align="left" >
                    <dx:ASPxLabel
                        ID="lblTo"           
                        runat="server"           
                        Text="To"/>         
                </td>

                <td style="width:10%" align="left" >
                    <dx:ASPxComboBox
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="True" 
                        Width="120px"   >
                    </dx:ASPxComboBox>
                </td>

                <td style="width:10%;" align="right" >
                    <dx:ASPxLabel
                        ID="lblReportBy"           
                        runat="server"           
                        Text="Report By"/>         
                </td>

                <td style="width:10%;" align="left" >
                    <dx:ASPxComboBox
                        ID="ddlType" 
                        runat="server" 
                        AutoPostBack="True" 
                        SelectedIndex="0"
                        Width="120px"   >
                        <Items>
                            <dx:ListEditItem Text="TPM"  Value="TPM"/>
                            <dx:ListEditItem Text="Zone"  Value="Zone" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:10%;" align="right" >
                    <dx:ASPxLabel
                        ID="lblReportOn"           
                        runat="server"           
                        Text="Report On"/>         
                </td>

                <td style="width:10%;" align="left" >
                     <dx:ASPxComboBox
                        ID="ddlReport" 
                        runat="server" 
                        AutoPostBack="True" 
                        SelectedIndex="0"
                        Width="160px">
                        <Items>
                            <dx:ListEditItem Text="Mechanical & Total"  Value="Mechanical & Total"/>
                            <dx:ListEditItem Text="Product Category"  Value="Product Category" />
                            <dx:ListEditItem Text="Reporting Category"  Value="Reporting Category" />
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:50%;" align="right" >
                    <dx:ASPxCheckBox
                        ID="chkApplic" 
                        runat="server" 
                        AutoPostBack="true"  
                        Text="Applicable business types only" 
                        TextAlign="right"/>
                </td>
            </tr>
        </table> 

             <dx:ASPxGridView 
            ID="gridZoneSummary" 
            runat="server" 
            width="100%" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode"
            Onload="GridStyles"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" Visible="False">
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False"
                ShowGroupFooter="VisibleIfExpanded" 
                HorizontalScrollBarMode="Visible" 
                UseFixedTableLayout="True" 
                VerticalScrollableHeight="350" 
                ShowVerticalScrollBar="True" 
                VerticalScrollBarStyle="Virtual" 
                ShowFilterRowMenu="False" 
                ShowFilterRow="False" />

            <SettingsBehavior 
                AllowSort="False" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" 
                AllowDragDrop="False" 
                AllowGroup="False" />
            
            <Columns>

                <dx:GridViewDataTextColumn Caption="Region" FieldName="Region" Name="Region" VisibleIndex="0" Width="57px" Visible="False" ExportWidth="100" FixedStyle="Left" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="TPM" FieldName="Area" MinWidth="81" Name="Area" ShowInCustomizationForm="False" VisibleIndex="1" Width="81px" ExportWidth="100" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Zone" FieldName="Zone" Name="Zone" VisibleIndex="2" Width="57px" ExportWidth="100" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Dealer" FieldName="DealerCode" Name="DealerCode" VisibleIndex="3" Width="57px" HeaderStyle-HorizontalAlign="Left" CellStyle-HorizontalAlign="Left" ExportWidth="100" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="4" Width="157px" ExportWidth="200" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mech Comp" FieldName="SalesMechComp" VisibleIndex="5" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mech Bal" FieldName="SalesMech" VisibleIndex="6" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Lighting" FieldName="SalesLighting" VisibleIndex="7" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Body" FieldName="SalesBody" VisibleIndex="8" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Accessories" FieldName="SalesAcc" VisibleIndex="9" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Other" FieldName="SalesOther" VisibleIndex="10" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Sales" FieldName="SalesTotal" VisibleIndex="11" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mechanical Month YOY" FieldName="SalesMechanicalMonthYonY" VisibleIndex="12" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mechanical Month% +/-" FieldName="CompPercChangeMonth" VisibleIndex="13" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mechanical Sales YTD" FieldName="SalesMechanicalYTDThisYear" VisibleIndex="14" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Mechanical YTD YOY" FieldName="SalesMechanicalYTDYonY" VisibleIndex="15" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Mechanical YTD% +/-" FieldName="CompPercChange" VisibleIndex="16" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Sales Month YOY" FieldName="SalesMonthYonY" VisibleIndex="17" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Total Sales Month% +/-" FieldName="SalesPercChangeMonth" VisibleIndex="18" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Sales YTD" FieldName="SalesYTDThisYear" VisibleIndex="19" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Sales YTD YOY" FieldName="SalesYTDYonY" VisibleIndex="20" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Total Sales % +/-" FieldName="SalesPercChange" VisibleIndex="21" Width="87px" ExportWidth="100">
                    <PropertiesTextEdit DisplayFormatString="0">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>
                
            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dx:ASPxGridView>

             <dx:ASPxGridView 
            ID="gridZS4_Category" 
            runat="server" 
            width="976px" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode"
            Onload="GridStyles"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText"  
            Visible="False">
       
            
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False"
                ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" 
                UseFixedTableLayout="True" VerticalScrollableHeight="350" 
                ShowVerticalScrollBar="True" VerticalScrollBarStyle="Virtual" 
                ShowFilterRowMenu="False" ShowFilterRow="False" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

            <Columns>

                <dx:GridViewDataTextColumn Caption="Region" FieldName="Region" Name="Region"  VisibleIndex="0" Width="57px" Visible="false" FixedStyle="Left" >
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="TPM" FieldName="Area" Name="TPSM" VisibleIndex="1" Width="127px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Zone" FieldName="Zone" Name="Zone" VisibleIndex="2" Width="127px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" Name="DealerCode" VisibleIndex="3" Width="57px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="4" Width="157px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewBandColumn Caption="Mechanical Competitive" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="5">
                    
                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="MC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="MCYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="MCC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Mechanical Balance"  HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="6">
                    
                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="MB" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="MBYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="MBC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Body" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="7">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="B" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="BYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="BC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Lighting" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="8">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="L" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="LYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="LC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Accessories" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="9">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="Acc" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="AccYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="AccC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Other" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="10">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="O" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="OYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="OC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Totals" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="11">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="Total" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="TotalYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="TotalC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px" />
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dx:ASPxGridView>

             <dx:ASPxGridView 
            ID="gridZS4_RC" 
            runat="server" 
            width="976px" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode"
            Onload="GridStyles"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
            Visible="False">
   
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="False" ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" ShowTitlePanel="False"
                ShowGroupFooter="VisibleIfExpanded" HorizontalScrollBarMode="Visible" 
                UseFixedTableLayout="True" VerticalScrollableHeight="350" 
                ShowVerticalScrollBar="True" VerticalScrollBarStyle="Virtual" 
                ShowFilterRowMenu="False" ShowFilterRow="False" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

            <Columns>

                <dx:GridViewDataTextColumn Caption="Region" FieldName="Region" Name="Region" VisibleIndex="0" Width="57px" Visible="false" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="TPM" FieldName="Area" Name="TPSM" VisibleIndex="1" Width="127px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Zone" FieldName="Zone" Name="Zone" VisibleIndex="2" Width="57px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" Name="DealerCode" VisibleIndex="3" Width="57px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Name" FieldName="CentreName" VisibleIndex="4" Width="157px" FixedStyle="Left">
                </dx:GridViewDataTextColumn>

                <dx:GridViewBandColumn Caption="Maintenance" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="5">
                    
                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="M" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="MYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="MC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Extended Maintenance" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="6">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="EM" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="EMYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="EMC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Oil" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="7">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="Oil" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="OilYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="OilC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Repair" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="8">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="R" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="RYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="RC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Accessories" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="9">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="A" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="AYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="AC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Damage (Excl. Wheels)" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="10">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="D" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="DYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="DC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>
                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Other" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="11">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="O" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="OYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="OC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Wheels" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="12">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="W" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="WYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="WC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Totals" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True" VisibleIndex="13">

                    <Columns>

                        <dx:GridViewDataTextColumn Caption="Sales YTD" FieldName="Total" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YoY" FieldName="TotalYOY" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="£##,##0" />
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn Caption="Sales YTD% +/-" FieldName="TotalC" Width="87px">
                            <PropertiesTextEdit DisplayFormatString="0" />
                        </dx:GridViewDataTextColumn>

                    </Columns>

                </dx:GridViewBandColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px" />
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dx:ASPxGridView>

    </div>
    
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridZoneSummary" PreserveGroupRowStates="true" />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="gridZS4_Category" PreserveGroupRowStates="true" />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server" GridViewID="gridZS4_RC" PreserveGroupRowStates="true" />

</asp:Content>




