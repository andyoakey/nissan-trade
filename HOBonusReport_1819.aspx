﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="HOBonusReport_1819.aspx.vb" Inherits="HOBonusReport_1819" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
    
    <div id="divTitle" style="position:relative;">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                runat="server" 
                Text="Head Office Bonus Summary FY 2018/19" 
                Font-Size="Larger" />
    </div>

    <div id="divQSelect" style="position:relative;">
        <table>
            <tr>
                <td width ="15%">
                    <dx:ASPxLabel
                        runat="server"
                        id ="lblQuarter"
                        Text="Quarter" />
                </td>

                <td width ="1%">
                </td>
                
                <td width ="15%">
                    <dx:ASPxLabel
                        runat="server"
                        id ="lblWorkingDays"
                        Text="Working Days In this Quarter" />
                </td>

                <td width ="1%">
                </td>

                <td width ="50%">
                    <dx:ASPxLabel
                        runat="server"
                        id ="lblDaysToDate"
                        Text="Working Days Accrued this Quarter" />
                </td>


            </tr>

            <tr>
                <td width ="15%">
                    <dx:ASPxComboBox 
                         runat="server"
                         AutoPostBack="true"
                         id="cboQuarters"
                         DataSourceID="dsQuarterList"
                         ValueField="id"
                         SelectedIndex="0"
                         TextField="quartername"
                         Width="100%">
                       </dx:ASPxComboBox>
                </td>

                <td width ="1%">
                </td>

                <td width ="15%">
                    <dx:ASPxTextBox
                        runat="server"
                        id ="txtWorkingDays" />
                </td>

                <td width ="1%">
                </td>

                <td width ="15%">
                    <dx:ASPxTextBox
                        runat="server"
                        id ="txtDaysToDate" />
                </td>

                <td width ="53%" align="right">
                    <dx:ASPxButton 
                        runat ="server"
                        AutoPostBack="true"
                        id="btnViewHide" 
                        Text="Show Payment/Banding Information"
                        />
                </td>


            </tr>
        </table>
    </div>

    <br />

    <dx:ASPxGridView 
                                ID="gridDealerBonus" 
                                DataSourceID="dsDealerBonus"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Header-Wrap="True"
                                Styles-Cell-Wrap="False"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true"
                                onload="GridStylesNo"
                                CssClass="grid_styles">
     

                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="true" AllButton-Visible="true"/>

                                <Columns>

                                                    <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Dealer Information" Name="colBandDealer">
                                                                <Columns>
                                                                        <dx:GridViewDataTextColumn 
                                                                          Caption="Dealer Name" 
                                                      FieldName="dealername"
                                                      Name="dealername"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Left"
                                                      CellStyle-Wrap="False"
                                                      ExportWidth="250"
                                                      Width= "10%"
                                                      VisibleIndex="0">
                                                        <Settings AllowHeaderFilter ="True" HeaderFilterMode="CheckedList" />
                                                    </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewDataTextColumn 
                                                                          Caption="Code" 
                                                      FieldName="dealercode"
                                                      Name="dealercode"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "4%"
                                                      VisibleIndex="1">
                                                          <Settings AllowHeaderFilter ="True" HeaderFilterMode="CheckedList" />
                                                    </dx:GridViewDataTextColumn>
                                                        
                                                                        <dx:GridViewDataTextColumn 
                                                                          Caption="Zone" 
                                                      FieldName="zone"
                                                      Name="zone"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "4%"
                                                      VisibleIndex="2">
                                                           <Settings AllowHeaderFilter ="True" HeaderFilterMode="CheckedList" /> 
                                                    </dx:GridViewDataTextColumn>
                                                        
                                                                        <dx:GridViewDataTextColumn 
                                                                          Caption="Group" 
                                                      FieldName="dealergroup"
                                                      Name="dealergroup"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                       CellStyle-Wrap="False"
                                                      ExportWidth="250"
                                                                            visible ="False"
                                                      Width= "8%"
                                                      VisibleIndex="3">
                                                            <Settings AllowHeaderFilter ="True" HeaderFilterMode="CheckedList" />
                                                    </dx:GridViewDataTextColumn>

                                                                        <dx:GridViewDataTextColumn 
                                                                          Caption="Bonus Band" 
                                                      FieldName="bonusband"
                                                      Name="bonusband"
                                                       CellStyle-Wrap="False"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ExportWidth="200"
                                                      Width= "7%"
                                                      VisibleIndex="4">
                                                            <Settings AllowHeaderFilter ="True" HeaderFilterMode="CheckedList" />
                                                    </dx:GridViewDataTextColumn>

                                                                </Columns>
                                                    </dx:GridViewBandColumn>

                                                    <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Actual Sales Performance (incl. NWCR)" Name="colBand1">
                                                        <Columns>
                                                                <dx:GridViewDataTextColumn 
                                                      Caption="17/18 Sales to Date" 
                                                      FieldName="sales_all_2017"
                                                      Name="sales_all_2017"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      ToolTip="Total Invoiced Sales for same period in Financial Year 2018/19"
                                                      VisibleIndex="5">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>            

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="18/19 Sales to Date" 
                                                      FieldName="sales_all_2018"
                                                      Name="sales_all_2018"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ToolTip="Total Invoiced Sales in this Quarter (to date if current Quarter)"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      VisibleIndex="6">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="Growth % incl. NWCR" 
                                                      FieldName="growth_all"
                                                      Name="growth_all"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ToolTip="Comparison of Sales excl. NWCR in this Quarter versus same period 12 months ago.  If Quarter is incomplete then comparison is with the same dates 12 months ago."
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "5%"
                                                      VisibleIndex="7">
                                                      <PropertiesTextEdit DisplayFormatString="0.0%"/>
                                                    </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="Daily Run Rate" 
                                                      FieldName="runrate"
                                                      Name="runrate"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "5%"
                                                      Tooltip="Average Sales Per Working Day this Quarter.  This value is used for all forecasts on this grid"
                                                      VisibleIndex="8">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>
                                                         </Columns>
                                                    </dx:GridViewBandColumn>

                                                    <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Actual Sales Performance (excl. NWCR)" Name="colBand2">
                                                        <Columns>
                                                                <dx:GridViewDataTextColumn 
                                                      Caption="NWCR Sales " 
                                                      FieldName="sales_nw_2018"
                                                      Name="sales_nw_2018"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      ToolTip="NWCR Sales for this period"
                                                      VisibleIndex="9">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>
            
                                                                <dx:GridViewDataTextColumn 
                                                      Caption="17/18 Sales to Date" 
                                                      FieldName="sales_ex_2017"
                                                      Name="sales_ex_2017"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      ToolTip="Invoiced Sales Excluding NWCR in this Quarter"
                                                      VisibleIndex="10">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>            

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="18/19 Sales to Date" 
                                                      FieldName="sales_ex_2018"
                                                      Name="sales_exl_2018"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ToolTip="Invoiced Sales Excluding NWCR in this Quarter (to date if current Quarter)"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      VisibleIndex="11">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="Growth % excl NWCR" 
                                                      FieldName="growth_ex"
                                                      Name="growth_ex"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ToolTip="Comparison of Sales in this Quarter versus same period 12 months ago.  If Quarter is incomplete then comparison is with the same dates 12 months ago."
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "6%"
                                                      VisibleIndex="12">
                                                      <PropertiesTextEdit DisplayFormatString="0.0%"/>
                                                    </dx:GridViewDataTextColumn>

                                                                <dx:GridViewDataTextColumn 
                                                      Caption="Reward Band" 
                                                      FieldName="rewardband"
                                                      Name="rewardband"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ToolTip="The reward band that will be reached if the Forecast Sales are achieved"
                                                      ExportWidth="100"
                                                      Width= "4%"
                                                      VisibleIndex="13"/>
                                                         </Columns>
                                                    </dx:GridViewBandColumn>
                                                                                    
                                                    <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Forecast Total Sales inc NWCR (Sales Reward)" Name="colBand3">
                                                        <Columns>
      
                                                        <dx:GridViewDataTextColumn 
                                                      Caption="Forecast" 
                                                      FieldName="forecast_growth"
                                                      Name="forecast_growth"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ToolTip="Forecast Growth for Sales Excluding NWCR"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      VisibleIndex="14">
                                                      <PropertiesTextEdit DisplayFormatString="0.0%"/>
                                                    </dx:GridViewDataTextColumn>

                                                            
                                                      <dx:GridViewDataTextColumn 
                                                      Caption="Forecast" 
                                                      FieldName="forecast_sales"
                                                      Name="forecast_sales"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      CellStyle-HorizontalAlign="Center"
                                                      ToolTip="Actual Sales for this Quarter if quarter is complete. If Quarter is incomplete Forecast Sales are based on Daily Run Rate * Working Days accrued"
                                                      ExportWidth="100"
                                                      Width= "7%"
                                                      VisibleIndex="15">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>

                                                      <dx:GridViewDataTextColumn 
                                                      Caption="Forecast Reward £" 
                                                      FieldName="rewardpayment"
                                                      Name="rewardpayment"
                                                      HeaderStyle-HorizontalAlign="Center"
                                                      ToolTip="The reward that will be paid if the Forecast Sales are achieved, based on Dealer Performance Banding (see calculation tables below) "
                                                      CellStyle-HorizontalAlign="Center"
                                                      ExportWidth="100"
                                                      Width= "6%"
                                                      VisibleIndex="16">
                                                      <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                                                    </dx:GridViewDataTextColumn>
                                                         </Columns>
                                                    </dx:GridViewBandColumn>

                                             </Columns>

                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>

                                <Settings ShowFooter="true" />

                                <Styles Footer-HorizontalAlign ="Center" />
                
                                 <TotalSummary>
                                        <dx:ASPxSummaryItem   FieldName="quartername" SummaryType="Sum" DisplayFormat="Projection For FY 18/19: {0}" />
                                        <dx:ASPxSummaryItem   FieldName="sales_all_2017" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_all_2018" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_ex_2017" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_ex_2018" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="sales_nw_2018" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="forecast_sales" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                        <dx:ASPxSummaryItem   FieldName="rewardpayment" SummaryType="Sum" DisplayFormat="&#163;##,##0"/>
                                </TotalSummary>

                                </dx:ASPxGridView>   
  
  </div>

    <br />

     <div runat="server" id="divPayments" >

        <table>
                                <tr>
                                    <td style="width:50%" valign="top">
                                        <dx:ASPxGridView 
                                ID="gridPaymentScale" 
                                DataSourceID="dsBonusPayments"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true"
                                onload="GridStylesNo"
                                CssClass="grid_styles">
     

                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>

                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Payment Scale Sales-Out Bonus">
                                                <Columns>

                                                    <dx:GridViewDataTextColumn 
                                              Caption="Total Sales By Qtr" 
                                              FieldName="qsales_bandname"
                                              Name="qsales_bandname"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                              Caption="Payment Band 1" 
                                              FieldName="payband_1"
                                              Name="payband_1"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "19%"
                                              PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                              VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                                            
                                                    <dx:GridViewDataTextColumn 
                                              Caption="Payment Band 2" 
                                              FieldName="payband_2"
                                              Name="payband_2"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "19%"
                                              PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                              Caption="Payment Band 3" 
                                              FieldName="payband_3"
                                              Name="payband_3"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "19%"
                                              PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                                 </Columns>

                                             </dx:GridViewBandColumn>


                                            <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Operating Standards">
                                                <Columns>
                                                    <dx:GridViewDataTextColumn 
                                              Caption="Reward Payment" 
                                              FieldName="standardsreward"
                                              Name="standardsreward"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "23%"
                                              PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                                </Columns>
                                             </dx:GridViewBandColumn>

                               </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
<Header HorizontalAlign="Center"></Header>
</Styles>
      
                                </dx:ASPxGridView>   
                                    </td>

                                    <td style="width:50%" valign="top">
                                        <dx:ASPxGridView 
                                ID="gridBonusBandings" 
                                DataSourceID="dsBonusBanding"
                                runat="server" 
                                Styles-Header-HorizontalAlign="Center"
                                Styles-CellStyle-HorizontalAlign="Center"
                                AutoGenerateColumns="False"  
                                Settings-UseFixedTableLayout="true"
                                onload="GridStylesNo"
                                CssClass="grid_styles">
     
                                <Settings
            ShowGroupButtons="False" 
            ShowStatusBar="Hidden" />

                                <SettingsPager  Visible="false"/>

                                <Columns>
                                        
                                        <dx:GridViewBandColumn  HeaderStyle-HorizontalAlign="Center"  Caption="Dealer Performance Banding">
                                            <Columns>

                                            <dx:GridViewDataTextColumn 
                                              Caption="Band Name" 
                                              FieldName="banding"
                                              Name="banding"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "20%"
                                              VisibleIndex="0">
                                            </dx:GridViewDataTextColumn>
                                            
                                                <dx:GridViewDataTextColumn 
                                              Caption="Band Description" 
                                              FieldName="comment"
                                              Name="comment"
                                              HeaderStyle-HorizontalAlign="Center"
                                              CellStyle-HorizontalAlign="Center"
                                              ExportWidth="200"
                                              Width= "50%"
                                              VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                                            
                                            </Columns>
                                       </dx:GridViewBandColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 1 %" 
                                              FieldName="band1_pc"
                                              Name="band1_pc"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              ToolTip="% Growth Required to Earn Band 1 payment"
                                              ExportWidth="200"
                                              Width= "10%"
                                              PropertiesTextEdit-DisplayFormatString="##,##0.0"
                                              VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 2 %" 
                                              FieldName="band2_pc"
                                              Name="band2_pc"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              ToolTip="% Growth Required to Earn Band 2 payment"
                                              CellStyle-HorizontalAlign="Center"
                                              Width= "10%"
                                              PropertiesTextEdit-DisplayFormatString="##,##0.0"
                                              VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                              Caption="Band 3 %" 
                                              FieldName="band3_pc"
                                              Name="band3_pc"
                                              HeaderStyle-HorizontalAlign="Center"
                                              HeaderStyle-Wrap ="True"
                                              CellStyle-HorizontalAlign="Center"
                                              ToolTip="% Growth Required to Earn Band 3 payment"
                                              ExportWidth="200"
                                              Width= "10%"
                                              PropertiesTextEdit-DisplayFormatString="##,##0.0"
                                              VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>

                                </Columns>
              
                                <SettingsBehavior 
                                AllowSort="False" 
                                AllowDragDrop="False" 
                                ColumnResizeMode="Control" />
      
                                <Styles>
                                    <Header HorizontalAlign="Center"></Header>
                                </Styles>
      
                                </dx:ASPxGridView>   
                                    </td>
                                </tr>
                            </table>
   
  </div>

    
  <div>


      <div>

    <asp:SqlDataSource 
        ID="dsDealerBonus" 
        runat="server" 
        SelectCommand="sp_HO_Bonus_1819" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nquarter" SessionField="CurrentQuarter" Type="Int16" Size="1" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsQuarters" 
        runat="server" 
        SelectCommand="sp_QuarterDays_1819" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nquarter_id" SessionField="CurrentQuarter" Type="Int16" Size="1" />
        </SelectParameters>
    </asp:SqlDataSource>
   
    <asp:SqlDataSource 
        ID="dsQuarterList" 
        runat="server" 
        SelectCommand="sp_QuarterList_1819" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1"
        runat="server" 
        GridViewID="gridDealerBonus">
    </dx:ASPxGridViewExporter>

     <asp:SqlDataSource ID="dsBonusPayments" runat="server" SelectCommand="sp_BonusPaymentsGrid" SelectCommandType="StoredProcedure" >
        <SelectParameters>
                <asp:Parameter Name="finyear" DefaultValue="18/19"  Type="String" Size="5" />
        </SelectParameters>
     </asp:SqlDataSource>

     <asp:SqlDataSource ID="dsBonusBanding" runat="server" SelectCommand="sp_BonusBandings" SelectCommandType="StoredProcedure" >
        <SelectParameters>
                <asp:Parameter Name="finyear" DefaultValue="18/19"  Type="String" Size="5" />
        </SelectParameters>
     </asp:SqlDataSource>

          </div>


    </div>

</asp:Content>

