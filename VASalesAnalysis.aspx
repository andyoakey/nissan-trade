﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VASalesAnalysis.aspx.vb" Inherits="VASalesAnalysis" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="VA Part Sales Analysis" />
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsVASales" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true">
        </SettingsPager>
 
        <Styles Footer-HorizontalAlign ="Center" />
                
        <Settings 
             
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewBandColumn Caption="Dealer And Account Information"  HeaderStyle-HorizontalAlign="Center">
                   <Columns>
      <%--      <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="5%" 
                exportwidth="200"
                Caption="Region" 
                FieldName="region">
            </dx:GridViewDataTextColumn>--%>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="4%" 
                exportwidth="200"
                Caption="Zone" 
                FieldName="zone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="5%" 
                exportwidth="200"
                Caption="Dealer Code" 
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="3" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="10%" 
                exportwidth="200"
                Caption="Dealer Name" 
                FieldName="dealername">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="4" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="6%" 
                exportwidth="200"
                Caption="A/C Code" 
                FieldName="link">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="5" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="10%" 
                exportwidth="200"
                Caption="A/C Name" 
                FieldName="accountname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="6" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="6%" 
                exportwidth="200"
                Caption="Business Type" 
                FieldName="businesstype">
            </dx:GridViewDataTextColumn>
            </columns>
            </dx:GridViewBandColumn>


            <dx:GridViewBandColumn Caption="Product Information" HeaderStyle-HorizontalAlign="Center">
                   <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="7" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="5%" 
                exportwidth="200"
                Caption="Service Type" 
                FieldName="level4_code">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="8" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                Width="8%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="Trade Analysis" 
                FieldName="tradeanalysis">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="9" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                HeaderStyle-Wrap="True"
                CellStyle-Wrap="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="5%" 
                exportwidth="200"
                Caption="Service Desc." 
                FieldName="level2_code">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="10" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-Wrap="False"
                Width="7%" 
                exportwidth="200"
                Caption="Part Number" 
                FieldName="partnumber">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="11" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                CellStyle-Wrap="False"
                Width="8%" 
                exportwidth="200"
                Caption="Part Desc." 
                FieldName="partdescription">
            </dx:GridViewDataTextColumn>
              </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn Caption="Date of Invoice" HeaderStyle-HorizontalAlign="Center">
                   <Columns>

            <dx:GridViewDataTextColumn
                VisibleIndex="12" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Width="4%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="Year" 
                FieldName="year">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="12" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Width="4%" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="200"
                Caption="Month" 
                FieldName="month">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataDateColumn
                VisibleIndex="12" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Settings-HeaderFilterMode="CheckedList"
                Width="6%" 
                exportwidth="200"
                Caption="Date" 
                FieldName="invoicedate">
            </dx:GridViewDataDateColumn>
                  </Columns>
            </dx:GridViewBandColumn>

            <dx:GridViewBandColumn Caption="Sales">
                   <Columns>
                        <dx:GridViewDataTextColumn
                VisibleIndex="13" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="6%" 
                PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"
                exportwidth="200"
                Caption="Value" 
                FieldName="salevalue">
            </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn
                VisibleIndex="14" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="False"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="4%" 
                exportwidth="200"
                Caption="Units" 
                FieldName="quantity">
            </dx:GridViewDataTextColumn>
                  </Columns>
            </dx:GridViewBandColumn>

         </Columns>
        
        <TotalSummary>
            <dx:ASPxSummaryItem FieldName ="salevalue" DisplayFormat="&#163;##,##0.00" SummaryType="Sum" />
            <dx:ASPxSummaryItem FieldName ="quantity" DisplayFormat="##,##0" SummaryType="Sum" />
        </TotalSummary>

    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsVASales" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_VA_Report" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
                <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            </SelectParameters>
        </asp:SqlDataSource>
   

    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

