﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Partial Class ViewVehicles

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - View Vehicle List"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

    End Sub

    Protected Sub dsVehicleList_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVehicleList.Init
        dsVehicleList.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsVehicleListOwners_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVehicleListOwners.Init
        dsVehicleListOwners.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsVehicleListWork_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVehicleListWork.Init
        dsVehicleListWork.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsVehicleListInvoices_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVehicleListInvoices.Init
        dsVehicleListInvoices.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridWork_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("RegNumber") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Protected Sub gridInvoices_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("RegNumber") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Protected Sub gridOwners_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("RegNumber") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub



End Class
