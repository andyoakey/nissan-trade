﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LatestData.aspx.vb" Inherits="LatestData" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Latest Data Transmissions" />
        </div>

      <div id="divNote" style="position:relative;">
           <dx:ASPxLabel 
                Font-Italic="true"
                ID="lblNote" runat="server" 
                Text="This report shows the latest data we have for any dealer regardless of whether the rest of the website has been updated. Remember the date here is the most recent date we have. This does NOT mean we have ALL their transactions from the days before this date" />
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsLatest" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="25" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="False"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="6%" 
                exportwidth="250"
                Caption="Zone" 
                FieldName="zone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Dealer Group" 
                FieldName="dealergroup">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="7%" 
                exportwidth="250"
                Caption="Dealer Code" 
                FieldName="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="3"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="15%" 
                exportwidth="250"
                Caption="Dealer Name" 
                FieldName="dealername">
            </dx:GridViewDataTextColumn>
                
            <dx:GridViewDataTextColumn
                VisibleIndex="4"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="DMS Type" 
                FieldName="dmstype">
            </dx:GridViewDataTextColumn>
  
           <dx:GridViewDataTextColumn
                VisibleIndex="5"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="15%" 
                exportwidth="250"
                Caption="FTP Mailbox Folder" 
                FieldName="ftplocation">
            </dx:GridViewDataTextColumn>
                
            <dx:GridViewDataTextColumn
                VisibleIndex="6"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Delivery Method" 
                FieldName="method">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="7"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Expected Frequency" 
                FieldName="frequency">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="8"
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="10%" 
                exportwidth="250"
                Caption="Days Overdue" 
                Name="daysoverdue"
                FieldName="daysoverdue">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataDateColumn
                VisibleIndex="9" 
                Settings-AllowSort="True"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                CellStyle-HorizontalAlign="Center"
                Width="12%" 
                exportwidth="250"
                Caption="Most Recent Transaction" 
                FieldName="latestdate">
            </dx:GridViewDataDateColumn>

         </Columns>
        
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsLatest" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_LatestDataCheck" 
            SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>

        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

