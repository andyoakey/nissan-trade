﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraCharts
Imports DevExpress.Web

Partial Class report6

    Inherits System.Web.UI.Page

    Dim sErrorMessage As String = ""
    Dim nMapType As Integer

    Dim da As New DatabaseAccess
    Dim htIn1 As New Hashtable
    Dim sErr As String
    Dim sSQL As String
    Dim dsResultsTerritory As DataSet
    Dim dsCustomerSummary As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Mapping"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Mapping", "Mapping")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        nMapType = Request.QueryString(0)

    End Sub

    Private Sub ShowMap(sImageURL As String)

        If Not (System.IO.File.Exists(MapPath(sImageURL))) Then
            lblFileNotFound.Visible = True
            lblImageEnlarge.Visible = False
            imgMap.Visible = False
            btnSaveImage.Visible = False
        End If

    End Sub

    Private Sub LoadData()

        Dim sDealerCode As String = Trim(Session("SelectionId"))
        Dim nIncludeNTV As Integer = 1

        If nMapType <= 1 Then nIncludeNTV = 0

        '/ ------------------------------------------------------------------------------------------------------
        '/ Data by Centre grid
        '/ ------------------------------------------------------------------------------------------------------
        htIn1.Clear()
        htIn1.Add("@sDealerCode", sDealerCode)
        htIn1.Add("@nIncludeNTV", nIncludeNTV)
        dsResultsTerritory = da.Read(sErr, "p_Get_MapInfo", htIn1)
        gridTerritory.DataSource = dsResultsTerritory.Tables(0)
        gridTerritory.DataBind()

        htIn1.Clear()
        htIn1.Add("@sDealerCode", sDealerCode)
        dsCustomerSummary = da.Read(sErr, "p_Get_MapInfo_CustomerSummary", htIn1)
        Call PopulatePieChart(chartCustomerSummary, "Trade Database", _
                                    dsCustomerSummary.Tables(0).Rows(0).Item("CustomersActive"), _
                                    dsCustomerSummary.Tables(0).Rows(0).Item("CustomersInActive"), _
                                    dsCustomerSummary.Tables(0).Rows(0).Item("CustomersLapsed"), _
                                    dsCustomerSummary.Tables(0).Rows(0).Item("CustomersProspect"), _
                                    dsCustomerSummary.Tables(0).Rows(0).Item("CustomersNewProspects"))

    End Sub

    Protected Sub IconButtons_Click(sender As Object, e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click
        Select Case sender.id
            Case "btn1"
                Response.Redirect("~/report6.aspx?map=1")
            Case "btn2"
                Response.Redirect("~/report6.aspx?map=3")
            Case "btn3"
                Response.Redirect("~/report6.aspx?map=2")
        End Select
    End Sub

    Protected Sub btnSaveImage_Click(sender As Object, e As EventArgs) Handles btnSaveImage.Click

        Dim sStr As String = "attachment; filename=" & imgMap.ImageUrl
        Response.ContentType = "GIF Image/gif"
        Response.AppendHeader("Content-Disposition", sStr)
        Response.TransmitFile(Server.MapPath(imgMap.ImageUrl))
        Response.End()

    End Sub

    Protected Sub gridTerritory_CustomColumnDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs) Handles gridTerritory.CustomColumnDisplayText
        If e.Column.FieldName = "PostCode" Then
            If e.GetFieldValue("PostCode") = "NTV" Then
                e.DisplayText = "NTS"
            End If
        End If
    End Sub

    Protected Sub gridTerritory_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridTerritory.HtmlDataCellPrepared

        If nMapType = 1 Then
            If e.DataColumn.FieldName = "ParcTotal" Then
                If ddlParcChoice.SelectedItem.Value = "1" Then
                    e.Cell.BackColor = Drawing.Color.GhostWhite
                Else
                    e.Cell.BackColor = Drawing.Color.White
                End If
            End If
            If e.DataColumn.FieldName = "Parc8Year" Then
                If ddlParcChoice.SelectedItem.Value = "0" Then
                    e.Cell.BackColor = Drawing.Color.GhostWhite
                Else
                    e.Cell.BackColor = Drawing.Color.White
                End If
            End If
        End If

    End Sub

    Private Sub PopulatePieChart(objChart As DevExpress.XtraCharts.Web.WebChartControl, sTitle As String, nActive As Integer, nInActive As Integer, nLapsed As Integer, nProspect As Integer, nNewProspects As Integer)

        ' Create a pie series.
        Dim series1 As New Series(sTitle, ViewType.Pie3D)

        ' Populate the series with points.
        series1.Points.Add(New SeriesPoint("Active", nActive))
        series1.Points.Add(New SeriesPoint("In-Active", nInActive))
        series1.Points.Add(New SeriesPoint("Lapsed", nLapsed))
        series1.Points.Add(New SeriesPoint("Prospects", nProspect))
        series1.Points.Add(New SeriesPoint("New Prospects", nNewProspects))

        ' Add the series to the chart.
        objChart.Series.Clear()
        objChart.Series.Add(series1)

        ' Adjust the point options of the series.
        series1.Label.TextPattern = "{V:n0}"
        series1.Label.TextAlignment = StringAlignment.Center
        series1.LegendTextPattern = "{A}"

        ' Detect overlapping of series labels.
        CType(series1.Label, PieSeriesLabel).ResolveOverlappingMode = ResolveOverlappingMode.Default
        CType(series1.Label, PieSeriesLabel).Position = PieSeriesLabelPosition.Inside

        ' Access the view-type-specific options of the series.
        Dim myView As Pie3DSeriesView = CType(series1.View, Pie3DSeriesView)

        ' Specify a data filter to explode points.
        myView.ExplodeMode = PieExplodeMode.All
        myView.ExplodedDistancePercentage = 10
        myView.Titles.Add(New SeriesTitle())
        myView.Titles(0).Text = series1.Name

        With objChart
            .Legend.Visible = True
            .Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
            .Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside
            .Legend.MaxHorizontalPercentage = 100
            .Legend.Direction = LegendDirection.LeftToRight
            .AppearanceNameSerializable = "Toyota"
            .BackColor = Color.Transparent
            .ToolTipEnabled = DevExpress.Utils.DefaultBoolean.False
        End With

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete

        Dim sDealerCode As String = Trim(Session("SelectionId"))
        Dim nCount As Integer = GetDataLong("SELECT COUNT(*) FROM Web_Mapping WHERE DealerCode = '" & sDealerCode & "'")

        If Session("SelectionLevel") <> "C" Then

            lblPageTitle.Text = "Mapping"
            lblNoDataMessage.Visible = False
            btn1.Visible = False
            btn2.Visible = False
            btn3.Visible = False
            gridTerritory.Visible = False
            imgMap.Visible = False
            lblImageEnlarge.Visible = False
            btnSaveImage.Visible = False
            chartCustomerSummary.Visible = False

            ' Hide Excel Button ( on masterpage) 
            Dim myPopupMapMessage As DevExpress.Web.ASPxPopupControl
            myPopupMapMessage = CType(Master.FindControl("PopMapMessage"), DevExpress.Web.ASPxPopupControl)
            If Not myPopupMapMessage Is Nothing Then
                myPopupMapMessage.ShowOnPageLoad = True
            End If

        Else

            If nCount = 0 Then

                lblNoDataMessage.Visible = True
                btn1.Visible = False
                btn2.Visible = False
                btn3.Visible = False
                gridTerritory.Visible = False
                imgMap.Visible = False
                lblImageEnlarge.Visible = False
                btnSaveImage.Visible = False
                chartCustomerSummary.Visible = False

            Else

                lblNoDataMessage.Visible = False

                For i = 1 To 56
                    gridTerritory.Columns(i).Visible = False
                Next

                ddlParcChoice.Visible = False
                ddlBusinessTypeChoice.Visible = False
                ddlCustomerChoice.Visible = False
                gridTerritory.Visible = False
                gridTerritory.Settings.ShowFooter = True
                imgMap.Visible = True
                chartCustomerSummary.Visible = False
                lblFileNotFound.Visible = False

                btn1.Visible = False
                btn2.Visible = False
                btn3.Visible = False
                gridTerritory.Style.Value = "left: 10px; position: absolute; top: 42px; z-index: 5"
                gridTerritory.Width = 255
                gridTerritory.Settings.VerticalScrollableHeight = "365"

                Call LoadData()
                If dsResultsTerritory.Tables(0).Rows.Count < 18 Then
                    gridTerritory.Settings.VerticalScrollBarMode = DevExpress.Web.ScrollBarMode.Hidden
                Else
                    gridTerritory.Settings.VerticalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible
                End If

                Select Case nMapType

                    Case 0 ' Territory
                        gridTerritory.Visible = True
                        lblPageTitle.Text = "Mapping - Territory"
                        imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/Territory.gif"
                        Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/Territory.gif"))
                        btn1.Visible = True
                        btn2.Visible = True
                        btn3.Visible = True
                        gridTerritory.Style.Value = "left: 125px; position: absolute; top: 42px; z-index: 5"
                        gridTerritory.Width = 125
                        gridTerritory.Settings.ShowFooter = False

                    Case 1 '  Parc                        
                        gridTerritory.Visible = True
                        ddlParcChoice.Visible = True
                        gridTerritory.Columns(1).Visible = True
                        gridTerritory.Columns(2).Visible = True
                        If ddlParcChoice.SelectedItem.Value = "1" Then
                            lblPageTitle.Text = "Territory & Car Parc - Car Parc Data (Total)"
                            imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/ParcTotal.gif"
                            Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/ParcTotal.gif"))
                        Else
                            lblPageTitle.Text = "Territory & Car Parc - Car Parc Data (8 Year)"
                            imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/Parc8Year.gif"
                            Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/Parc8Year.gif"))
                        End If

                    Case 2 '/ Customers                        
                        gridTerritory.Visible = True
                        ddlCustomerChoice.Visible = True
                        Select Case ddlCustomerChoice.SelectedItem.Value
                            Case "1"
                                gridTerritory.Columns(27).Visible = True
                                lblPageTitle.Text = "Database & Customers - Known Customers"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersKnown.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersKnown.gif"))
                            Case "2"
                                gridTerritory.Columns(28).Visible = True
                                lblPageTitle.Text = "Database & Customers - Active Customers"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersActive.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersActive.gif"))
                            Case "3"
                                gridTerritory.Columns(29).Visible = True
                                lblPageTitle.Text = "Database & Customers - In-Active Customers"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersInActive.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersInActive.gif"))
                            Case "4"
                                gridTerritory.Columns(30).Visible = True
                                lblPageTitle.Text = "Database & Customers - Lapsed Customers"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersLapsed.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersLapsed.gif"))
                            Case "5"
                                gridTerritory.Columns(31).Visible = True
                                lblPageTitle.Text = "Database & Customers - Prospect Customers"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersProspect.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersProspect.gif"))
                            Case "6"
                                gridTerritory.Columns(32).Visible = True
                                lblPageTitle.Text = "Database & Customers - New Prospects"
                                imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/CustomersNewProspects.gif"
                                Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/CustomersNewProspects.gif"))
                            Case "7"
                                For i = 27 To 32
                                    gridTerritory.Columns(i).Visible = True
                                Next
                                gridTerritory.Width = 455
                                lblPageTitle.Text = "Database & Customers - Trade Database"
                                imgMap.Visible = False
                                lblImageEnlarge.Visible = False
                                btnSaveImage.Visible = False
                                chartCustomerSummary.Visible = True
                        End Select

                    Case 3 '/ Sales
                        gridTerritory.Visible = True
                        ddlSalesChoice.Visible = True
                        ddlBusinessTypeChoice.Visible = True
                        Select Case ddlBusinessTypeChoice.SelectedItem.Value
                            Case "100"
                                If ddlSalesChoice.SelectedItem.Value = 0 Then
                                    Call ShowSalesColumns(sDealerCode, 3, "Total Trade Sales", "TotalSales")
                                Else
                                    Call ShowSalesColumns(sDealerCode, 15, "Total Mechanical Sales", "MCSales")
                                End If
                            Case "101"
                                If ddlSalesChoice.SelectedItem.Value = 0 Then
                                    Call ShowSalesColumns(sDealerCode, 4, "Total Applicable Sales", "Applic")
                                Else
                                    Call ShowSalesColumns(sDealerCode, 16, "Applicable Mechanical Sales", "MCSalesApplic")
                                End If
                            Case Else
                                If ddlSalesChoice.SelectedItem.Value = 0 Then
                                    Call ShowSalesColumns(sDealerCode, 3 + Val(ddlBusinessTypeChoice.SelectedIndex), "Total Sales (" & GetBusinessTypeName(ddlBusinessTypeChoice.SelectedItem.Value) & ")", "Sales" & Trim(ddlBusinessTypeChoice.SelectedItem.Value))
                                Else
                                    Call ShowSalesColumns(sDealerCode, 15 + Val(ddlBusinessTypeChoice.SelectedIndex), "Mechanical Sales (" & GetBusinessTypeName(ddlBusinessTypeChoice.SelectedItem.Value) & ")", "MCSales" & Trim(ddlBusinessTypeChoice.SelectedItem.Value))
                                End If
                        End Select

                End Select

            End If

        End If


    End Sub

    Private Sub ShowSalesColumns(sDealerCode As String, nCol1 As Integer, sTitle As String, sMapName As String)
        gridTerritory.Columns(nCol1).Visible = True
        gridTerritory.Columns(nCol1 + 30).Visible = True
        lblPageTitle.Text = "Trade Sales by Postcode District - " & sTitle
        imgMap.ImageUrl = "~/Maps/" & sDealerCode & "/" & sMapName & ".gif"
        Call ShowMap(imgMap.ResolveUrl("~/Maps/" & sDealerCode & "/" & sMapName & ".gif"))
    End Sub

    Private Function GetBusinessTypeName(sId As String) As String
        Dim sSQL As String = "SELECT Description FROM Toyota_BusinessTypes_Internal WHERE Id = " & sId
        GetBusinessTypeName = GetDataString(sSQL)
    End Function

    Protected Sub imgMap_Click(sender As Object, e As ImageClickEventArgs) Handles imgMap.Click
        'lblZoomTitle.Text = lblPageTitle.Text
        imgZoom.ImageUrl = imgMap.ImageUrl
        PopupZoomMap.HeaderText = lblPageTitle.Text
        PopupZoomMap.ShowOnPageLoad = True
    End Sub

    Protected Sub PostCode_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sPostCode As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "PostCode").ToString()

        link.Text = sPostCode
        If nMapType = 2 Then
            If sPostCode <> "NTV" And ddlCustomerChoice.SelectedItem.Value <> 7 Then
                link.NavigateUrl = String.Format("ViewCompanies.aspx?PostCode={0}&CustomerType={1}", sPostCode, ddlCustomerChoice.SelectedItem.Value)
                link.ToolTip = "Click here to see the Customers in this postcode."
            Else
                link.Font.Underline = False
            End If
        Else
            link.Font.Underline = False
        End If

    End Sub


End Class