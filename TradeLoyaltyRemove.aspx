﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TradeLoyaltyRemove.aspx.vb" Inherits="TradeLoyaltyRemove" %>

<!DOCTYPE html>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server" style="height:40px">
        <asp:Label ID="lblRegistration" runat="server" Text="Click Remove to permanently remove : " Font-Names="Calibri,Verdana"
            Style="left: 10px; position: absolute; top: 10px">
        </asp:Label>
        <br />
        <dxe:ASPxButton Font-Size="12px" Font-Names="Calibri,Verdana" ID="btnRemove" runat="server"
            Text="Remove" Width="100px" Style="left: 10px; position: absolute; top: 45px" CssClass="indbutton"  >
        </dxe:ASPxButton>
    </form>
</body>
</html>
