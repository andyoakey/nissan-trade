﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Drawing
Imports DevExpress.XtraCharts

Partial Class report1

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess
    Dim htIn1 As New Hashtable
    Dim sErr As String
    Dim sSQL As String
    Dim dsCustomerSummary As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Customers"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Workshop and Part Sales", "Workshop and Part Sales")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

        'If Session("UserEmailAddress").ToString().Contains("@qubedata.") Or _
        '    Session("UserId") = 13 Or _
        '    Session("UserId") = 14 Or _
        '    Session("UserId") = 15 Or _
        '    Session("UserId") = 16 Or _
        '    Session("UserId") = 17 Or _
        '    Session("UserId") = 18 Or _
        '    Session("UserId") = 601 Or _
        '    Session("UserId") = 680 Or _
        '    Session("UserId") = 681 Or _
        '    Session("UserId") = 762 Or _
        '    Session("UserId") = 642 Or _
        '    Session("UserId") = 591 Then
        '    btn8.Visible = True
        'Else
        '    btn8.Visible = False
        'End If


    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        Dim nButtonHeight As Integer = Session("ButtonHeight")
        Dim nButtonWidth As Integer = 100


        'btn1.Image.Height = nButtonHeight
        btn1.Image.Width = nButtonWidth
        'btn2.Image.Height = nButtonHeight
        btn2.Image.Width = nButtonWidth
        'btn3.Image.Height = nButtonHeight
        btn3.Image.Width = nButtonWidth
        'btn4.Image.Height = nButtonHeight
        btn4.Image.Width = nButtonWidth
        'btn5.Image.Height = nButtonHeight
        btn5.Image.Width = nButtonWidth
        'btn6.Image.Height = nButtonHeight
        btn6.Image.Width = nButtonWidth
        'btn7.Image.Height = nButtonHeight
        'btn7.Image.Width = nButtonWidth
        'btn8.Image.Height = nButtonHeight
        'btn8.Image.Width = nButtonWidth
        'btn9.Image.Height = nButtonHeight
        btn9.Image.Width = nButtonWidth
        btn10.Image.Width = nButtonWidth

        ' Who Can See Central Support Accounts icon  (default is invisible)
        btn10.Visible = False

        If Session("SelectionLevel") = "N" Or Session("SelectionLevel") = "R" Or Session("SelectionLevel") = "T" Then
            btn10.Visible = True
        End If

        If Session("SelectionLevel") = "Z" Then
            ' Check if any dealers in this zone have a CSA account
            If ZoneAccessToCSA(Session("SelectionID")) = True Then
                btn10.Visible = True
            End If
        End If

        If Session("SelectionLevel") = "G" Then
            ' Check if any dealers in this group have a CSA account
            If GroupAccessToCSA(Session("SelectionID")) = True Then
                btn10.Visible = True
            End If
        End If

        If Session("SelectionLevel") = "C" Then
            ' Check if this dealer has a CSA account
            If DealerAccessToCSA(Session("SelectionID")) = True Then
                btn10.Visible = True
            End If
        End If

        Call LoadPieCharts()
    End Sub

    Protected Sub IconButtons_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click, btn6.Click, btn9.Click, btn10.Click
        'btn7.Click, btn8.Click,
        Select Case sender.id
            Case "btn1"
		        Session("C_BusinessName") = "9999999"
                Response.Redirect("ViewCompanies.aspx")
            Case "btn2"
                Response.Redirect("ViewExcludedAccounts.aspx")
            Case "btn3"
                Response.Redirect("ViewSalesByCustomer.aspx")
            Case "btn4"
                Response.Redirect("ViewBestSellingCustomers.aspx")
            Case "btn5"
                Response.Redirect("NewCustomers.aspx")
            Case "btn6"
                Response.Redirect("CustomerYOYReport.aspx")
                ' Case "btn7"
                '    Response.Redirect("ContHistSummary.aspx")
                'Case "btn8"
                '   Response.Redirect("TradeLoyaltyNew.aspx")
            Case "btn9"
                Response.Redirect("EmailSummary.aspx")
            Case "btn10"
                Response.Redirect("CentralSupportAccounts.aspx")


        End Select
    End Sub

    Private Sub LoadPieCharts()

        htIn1.Clear()
        htIn1.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn1.Add("@sSelectionId", Session("SelectionId"))
        dsCustomerSummary = da.Read(sErr, "p_Dashboard_Customer_Summary2", htIn1)

        Call PopulatePieChart(chartCustomerSummary1, "Mechanical Customers",
                                    dsCustomerSummary.Tables(0).Rows(0).Item("ActCust"),
                                    dsCustomerSummary.Tables(0).Rows(0).Item("InActCust"),
                                    dsCustomerSummary.Tables(0).Rows(0).Item("LapsedCust"),
                                    dsCustomerSummary.Tables(0).Rows(0).Item("ProspectCust"),
                                    dsCustomerSummary.Tables(0).Rows(0).Item("NewProspectCust"))

        Call PopulatePieChart(chartCustomerSummary2, "Bodyshop Customers",
                                    dsCustomerSummary.Tables(0).Rows(1).Item("ActCust"),
                                    dsCustomerSummary.Tables(0).Rows(1).Item("InActCust"),
                                    dsCustomerSummary.Tables(0).Rows(1).Item("LapsedCust"),
                                    dsCustomerSummary.Tables(0).Rows(1).Item("ProspectCust"),
                                    dsCustomerSummary.Tables(0).Rows(1).Item("NewProspectCust"))

    End Sub

    Private Sub PopulatePieChart(objChart As DevExpress.XtraCharts.Web.WebChartControl, sTitle As String, nActive As Integer, nInActive As Integer, nLapsed As Integer, nProspect As Integer, nNewProspects As Integer)

        ' Create a pie series.
        Dim series1 As New Series(sTitle, ViewType.Pie3D)

        ' Populate the series with points.
        series1.Points.Add(New SeriesPoint("Active (last 90 days)", nActive))
        series1.Points.Add(New SeriesPoint("In-Active (91-180 days)", nInActive))
        series1.Points.Add(New SeriesPoint("Lapsed (181-270 days)", nLapsed))
        series1.Points.Add(New SeriesPoint("Prospects (over 270 days)", nProspect))
        series1.Points.Add(New SeriesPoint("New Prospects (never spent)", nNewProspects))

        ' Add the series to the chart.
        objChart.Series.Clear()
        objChart.Series.Add(series1)

        ' Adjust the point options of the series.
        series1.Label.ResolveOverlappingMinIndent = 20
        series1.Label.TextPattern = "{V:n0}"
        series1.Label.TextAlignment = StringAlignment.Center
        series1.LegendTextPattern = "{A}"

        ' Detect overlapping of series labels.
        CType(series1.Label, PieSeriesLabel).ResolveOverlappingMode = ResolveOverlappingMode.Default
        CType(series1.Label, PieSeriesLabel).Position = PieSeriesLabelPosition.Outside

        ' Access the view-type-specific options of the series.
        Dim myView As Pie3DSeriesView = CType(series1.View, Pie3DSeriesView)

        ' Specify a data filter to explode points.
        myView.ExplodeMode = PieExplodeMode.All
        myView.ExplodedDistancePercentage = 20
        myView.Titles.Add(New SeriesTitle())
        myView.Titles(0).Text = series1.Name


        With objChart
            .Legend.Visible = True
            .Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Center
            .Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside
            .Legend.MaxHorizontalPercentage = 100
            .Legend.Direction = LegendDirection.TopToBottom
            .AppearanceNameSerializable = "Toyota"
            .BackColor = Color.Transparent
            .ToolTipEnabled = DevExpress.Utils.DefaultBoolean.False
        End With

    End Sub

    Function DealerAccessToCSA(sDealerCode As String) As Boolean
        Return DataExists("Select * from v_CentralSupportAccounts where dealercode = '" & sDealerCode & "'")
    End Function
    Function GroupAccessToCSA(nGroupID As Integer) As Boolean
        Return DataExists("Select * From [dbo].[CentreGroupMembership] Where centregroupid = " & nGroupID & " And dealercode In (select dealercode from v_CentralSupportAccounts)")
    End Function
    Function ZoneAccessToCSA(sZone As String) As Boolean
        Return DataExists("Select * from v_CentralSupportAccounts where dealercode in (select DealerCode from centremaster where zone = '" & sZone & "')")
    End Function

End Class
