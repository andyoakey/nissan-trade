﻿Imports System.Data
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewZoneSummary

    Inherits System.Web.UI.Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim htin As New Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Zone Summary Report"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call GetMonthsDevX(ddlTo)
            Session("MonthTo") = Session("CurrentPeriod")
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Zone Summary", "Viewing Zone Summary")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
        Call LoadZoneSummary()
    End Sub

    Sub LoadZoneSummary()

        If Session("MonthTo") Is Nothing Then
            Session("MonthTo") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        If Session("ZoneApplic") Is Nothing Then
            If chkApplic.Checked Then
                Session("ZoneApplic") = 1
            Else
                Session("ZoneApplic") = 0
            End If
        Else
            If Session("ZoneApplic") = 1 Then
                chkApplic.Checked = True
            Else
                chkApplic.Checked = False
            End If
        End If

        htin.Add("@nTo", Session("MonthTo"))
        htin.Add("@nApplic", Session("ZoneApplic"))

        If ddlReport.SelectedIndex = 0 Then
            gridZoneSummary.Visible = True
            gridZS4_Category.Visible = False
            gridZS4_RC.Visible = False
            gridZoneSummary.Columns(1).Visible = If(ddlType.SelectedIndex = 0, True, False)
            gridZoneSummary.Columns(2).Visible = If(ddlType.SelectedIndex = 1, True, False)
            dsResults = da.Read(sErrorMessage, If(ddlType.SelectedIndex = 0, "p_ZoneSummary5_TPSM", "p_ZoneSummary5_Zone"), htin)
            gridZoneSummary.DataSource = dsResults.Tables(0)
            gridZoneSummary.DataBind()
        ElseIf ddlReport.SelectedIndex = 1 Then
            gridZoneSummary.Visible = False
            gridZS4_Category.Visible = True
            gridZS4_RC.Visible = False
            gridZS4_Category.Columns(1).Visible = If(ddlType.SelectedIndex = 1, True, False)
            gridZS4_Category.Columns(2).Visible = If(ddlType.SelectedIndex = 0, True, False)
            dsResults = da.Read(sErrorMessage, If(ddlType.SelectedIndex = 0, "p_ZoneSummary4_TPSM_Category_New", "p_ZoneSummary4_Zone_Category_New"), htin)
            gridZS4_Category.DataSource = dsResults.Tables(0)
            gridZS4_Category.DataBind()
        ElseIf ddlReport.SelectedIndex = 2 Then
            gridZoneSummary.Visible = False
            gridZS4_Category.Visible = False
            gridZS4_RC.Visible = True
            gridZS4_RC.Columns(1).Visible = If(ddlType.SelectedIndex = 1, True, False)
            gridZS4_RC.Columns(2).Visible = If(ddlType.SelectedIndex = 0, True, False)
            dsResults = da.Read(sErrorMessage, If(ddlType.SelectedIndex = 0, "p_ZoneSummary4_TPSM_RC", "p_ZoneSummary4_Zone_RC"), htin)
            gridZS4_RC.DataSource = dsResults.Tables(0)
            gridZS4_RC.DataBind()
        End If

    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        'If e.Column.FieldName = "Area" And e.GetFieldValue("Area") = "ZZ" Then
        '    e.DisplayText = ""
        'End If

        If e.Column.FieldName = "Zone" And e.GetFieldValue("DealerCode") = "99999" Then
            e.DisplayText = ""
        End If

        If e.Column.FieldName = "Region" And e.GetFieldValue("Region") = "ZZ" Then
            e.DisplayText = ""
        End If

        If e.Column.FieldName = "DealerCode" Then
            If e.GetFieldValue("DealerCode") = "99999" Then
                If e.GetFieldValue("Zone") = "ZZ" Then
                    If e.GetFieldValue("Region") = "ZZ" Then
                        e.DisplayText = "National"
                    Else
                        e.DisplayText = "Totals " & e.GetFieldValue("Region")
                    End If
                Else
                    e.DisplayText = "Totals " & e.GetFieldValue("Zone")
                End If
            End If
        End If

        If e.Column.FieldName = "CompPercChange" Or e.Column.FieldName = "CompPercChangeMonth" Or e.Column.FieldName = "SalesPercChange" Or e.Column.FieldName = "SalesPercChangeMonth" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If

        If e.Column.Caption = "Sales YTD% +/-" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If

    End Sub

    Protected Sub gridZoneSummary_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridZoneSummary.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0

        If e.KeyValue = "99999" Then
            If e.GetValue("Zone") = "ZZ" Then
                If e.GetValue("Region") = "ZZ" Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = System.Drawing.Color.White
                    e.Cell.BackColor = System.Drawing.Color.DarkGray
                Else
                    e.Cell.Font.Bold = True
                    e.Cell.BackColor = System.Drawing.Color.LightGray
                End If
            Else
                e.Cell.Font.Bold = True
                e.Cell.BackColor = System.Drawing.Color.LightGray
            End If
        Else
            If e.DataColumn.Index >= 12 And e.DataColumn.Index <= 16 Then
                e.Cell.BackColor = GlobalVars.g_Color_GridBack1
            End If
            If e.DataColumn.Index >= 17 And e.DataColumn.Index <= 21 Then
                e.Cell.BackColor = GlobalVars.g_Color_GridBack2
            End If
        End If

    End Sub

    Protected Sub gridZS_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridZS4_Category.HtmlDataCellPrepared, gridZS4_RC.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0

        If e.KeyValue = "99999" Then
            If e.GetValue("Zone") = "ZZ" Then
                If e.GetValue("Region") = "ZZ" Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = System.Drawing.Color.White
                    e.Cell.BackColor = System.Drawing.Color.DarkGray
                Else
                    e.Cell.Font.Bold = True
                    e.Cell.BackColor = System.Drawing.Color.LightGray
                End If
            Else
                e.Cell.Font.Bold = True
                e.Cell.BackColor = System.Drawing.Color.LightGray
            End If
        End If

    End Sub

    Protected Sub aspxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter2.RenderBrick, ASPxGridViewExporter3.RenderBrick

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        Else

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            Select Case e.Column.Name
                Case "SaleValue", "CostValue"
                    e.TextValueFormatString = "c0"
                Case "Quantity"
                    e.TextValueFormatString = "#,##0"
                Case "Margin"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                Case "Variance"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                    If IsDBNull(e.Value) = True Then
                        nThisValue = 0
                    Else
                        nThisValue = CDec(e.Value)
                    End If
                    If nThisValue = 0 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.White
                        e.BrickStyle.ForeColor = System.Drawing.Color.Black
                    ElseIf nThisValue > 0 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.Green
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Red
                    End If
            End Select

            If e.KeyValue = "99999" Then
                If e.GetValue("Zone") = "ZZ" Then
                    If e.GetValue("Region") = "ZZ" Then
                        e.BrickStyle.ForeColor = System.Drawing.Color.White
                        e.BrickStyle.BackColor = System.Drawing.Color.DarkGray
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                    End If
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                End If
            End If

        End If

        If e.Column.Name = "Area" And e.GetValue("Area") = "ZZ" Then
            e.TextValue = ""
        End If
        If e.Column.Name = "Zone" And e.GetValue("DealerCode") = "99999" Then
            e.TextValue = ""
        End If
        If e.Column.Name = "DealerCode" Then
            If e.GetValue("DealerCode") = "99999" Then
                If e.GetValue("Zone") = "ZZ" Then
                    If e.GetValue("Region") = "ZZ" Then
                        e.TextValue = "National"
                    Else
                        e.TextValue = "Totals " & e.GetValue("Region")
                    End If
                Else
                    e.TextValue = "Totals " & e.GetValue("Zone")
                End If
            End If
        End If

    End Sub

    Protected Sub aspxGridViewExporter2_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, System.Drawing.FontStyle.Regular)

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        Else

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            Select Case e.Column.Name
                Case "SaleValue", "CostValue"
                    e.TextValueFormatString = "c0"
                Case "Quantity"
                    e.TextValueFormatString = "#,##0"
                Case "Margin"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                Case "Variance"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                    If IsDBNull(e.Value) = True Then
                        nThisValue = 0
                    Else
                        nThisValue = CDec(e.Value)
                    End If
                    If nThisValue = 0 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.White
                        e.BrickStyle.ForeColor = System.Drawing.Color.Black
                    ElseIf nThisValue > 0 Then
                        e.BrickStyle.BackColor = System.Drawing.Color.Green
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Red
                    End If
            End Select

            If e.KeyValue = "99999" Then
                If e.GetValue("Zone") = "ZZ" Then
                    If e.GetValue("Region") = "ZZ" Then
                        e.BrickStyle.ForeColor = System.Drawing.Color.White
                        e.BrickStyle.BackColor = System.Drawing.Color.DarkGray
                    Else
                        e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                    End If
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                End If
            Else
                If e.Column.Index >= 12 And e.Column.Index <= 16 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.LightBlue
                End If
                If e.Column.Index >= 17 And e.Column.Index <= 21 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.LightGreen
                End If
            End If

        End If

        If e.Column.Name = "Area" And e.GetValue("Area") = "ZZ" Then
            e.TextValue = ""
        End If
        If e.Column.Name = "Zone" And e.GetValue("DealerCode") = "99999" Then
            e.TextValue = ""
        End If
        If e.Column.Name = "DealerCode" Then
            If e.GetValue("DealerCode") = "99999" Then
                If e.GetValue("Zone") = "ZZ" Then
                    If e.GetValue("Region") = "ZZ" Then
                        e.TextValue = "National"
                    Else
                        e.TextValue = "Totals " & e.GetValue("Region")
                    End If
                Else
                    e.TextValue = "Totals " & e.GetValue("Zone")
                End If
            End If
        End If

    End Sub

    Protected Sub chkApplic_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkApplic.CheckedChanged
        If chkApplic.Checked Then
            Session("ZoneApplic") = 1
        Else
            Session("ZoneApplic") = 0
        End If
    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = String.Empty

        Call LoadZoneSummary()

        If ddlReport.SelectedIndex = 0 Then
            ASPxGridViewExporter1.FileName = Trim("ZoneSummaryMC_" & Format(Now, "ddMMMyyyy") & ".xls")
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        ElseIf ddlReport.SelectedIndex = 1 Then
            ASPxGridViewExporter2.FileName = Trim("ZoneSummaryPC_" & Format(Now, "ddMMMyyyy") & ".xls")
            ASPxGridViewExporter2.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        ElseIf ddlReport.SelectedIndex = 2 Then
            ASPxGridViewExporter3.FileName = Trim("ZoneSummaryRC_" & Format(Now, "ddMMMyyyy") & ".xls")
            ASPxGridViewExporter3.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
        End If

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub


End Class
