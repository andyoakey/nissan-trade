﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Partial Class ViewPFCsByCustomer

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Customer Analysis by PFC"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "PFCs by Customer", "Viewing PFCs by Customer")
        End If
    End Sub

    Protected Sub dsViewSalesLevel1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel1.Init
        Session("Grouping") = Request.QueryString(0)
        dsViewSalesLevel1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ASPXPivotResults_CustomCellDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxPivotGrid.PivotCellDisplayTextEventArgs) Handles ASPxPivotGrid1.CustomCellDisplayText
        Select Case e.DataField.FieldName
            Case "Sale Value"
                e.DisplayText = Format(e.Value, "£#,##0")
            Case "Quantity"
                e.DisplayText = Format(e.Value, "#,##0")
            Case Else
        End Select
    End Sub



End Class
