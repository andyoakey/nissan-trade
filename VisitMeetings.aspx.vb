﻿Imports DevExpress.Export
Imports DevExpress.Web
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting

Partial Class VisitMeetings

    Inherits System.Web.UI.Page

    Dim host As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Toyota PARTS website - Meetings"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        '' Hide Excel Button ( on masterpage) 
        'Dim myexcelbutton As DevExpress.Web.ASPxButton
        'myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        'If Not myexcelbutton Is Nothing Then
        '    myexcelbutton.Visible = False
        'End If

        '' Hide PDFl Button ( on masterpage) 
        'Dim mypdfbutton As DevExpress.Web.ASPxButton
        'mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        'If Not mypdfbutton Is Nothing Then
        '    mypdfbutton.Visible = False
        'End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Action Meetings", "Viewing Visit Action Meetings")
            Session("VARYear") = 2016
        End If

        host = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host
        If (HttpContext.Current.Request.Url.Port And HttpContext.Current.Request.Url.Port <> 80) Then
            host += ":" + HttpContext.Current.Request.Url.Port.ToString()
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsMeeting_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsMeeting.Init
        dsMeeting.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub deletePDF_Click(ByVal sender As Object, ByVal e As EventArgs) Handles deletePDF.Click

        ' PDF needs removing...
        If viewingPDF.Value.Length > 0 Then
            Dim vars As String() = viewingPDF.Value.Split(",")

            If vars.Length = 2 Then
                ' Remove from db
                Using sqlcn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString)
                    Using sqlcm As New SqlCommand("DELETE FROM Web_Meetings WHERE Year = @dYear AND DealerCode = @sDealerCode AND Month = @nMonth", sqlcn)
                        sqlcn.Open()
                        sqlcm.Parameters.AddWithValue("@dYear", DateTime.Now.Year)
                        sqlcm.Parameters.AddWithValue("@sDealerCode", vars(0))
                        sqlcm.Parameters.AddWithValue("@nMonth", vars(1))
                        sqlcm.ExecuteNonQuery()
                    End Using
                End Using
                PopupPDF.ShowOnPageLoad = False
                ' Re-bind to get latest updates
                gridMeetings.DataBind()
            End If
        End If

    End Sub

    Protected Sub gridMeetings_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridMeetings.HtmlDataCellPrepared
        If (e.DataColumn.FieldName.StartsWith("D_")) Then
            Dim dc As String = Nothing

            Try
                dc = gridMeetings.GetRowValues(e.VisibleIndex, e.DataColumn.FieldName.Replace("D_", "M_")).ToString()
            Catch
                Return
            End Try

            Dim mo As Integer = DateTime.Parse("01-" & e.DataColumn.FieldName.Substring(2, (e.DataColumn.FieldName.Length - 2)) & "-1900").Month
            Dim cid As String = gridMeetings.GetRowValues(e.VisibleIndex, "DealerCode").ToString()
            Dim pdfv As String = cid & "," & mo.ToString()

            If dc = "-1" Then
                e.Cell.BackColor = System.Drawing.Color.LightGray
                e.Cell.ForeColor = System.Drawing.Color.LightGray
            ElseIf dc = "0" Then
                e.Cell.BackColor = System.Drawing.Color.Red
                e.Cell.ForeColor = System.Drawing.Color.Red
                e.Cell.Attributes.Add("onclick", String.Format("gridMeetings.PerformCallback({0} + '|' + {1}); PopupUpload.Show(); return false;", e.VisibleIndex, mo.ToString()))
            Else
                e.Cell.BackColor = System.Drawing.Color.Green
                e.Cell.ForeColor = System.Drawing.Color.Green
                e.Cell.ToolTip = dc
                e.Cell.Attributes.Add("onclick", "document.getElementById('iframe_pdf').src = 'Download.aspx?t=mtm&cid=" + cid + "&mo=" + mo.ToString() + "'; document.getElementById('viewingPDF').value = '" & pdfv & "'; PopupPDF.Show();")
            End If

            If dc <> "-1" Then
                e.Cell.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
                e.Cell.Text = "<div style='text-align: center; vertical-align: middle; background: url(img/" + If(dc = "0", "up", "down") + "load.png) no-repeat; background-size: 16px; width: 16px;'>&nbsp;</div>"
            End If
        End If
    End Sub

    Protected Sub gridMeetings_CustomCallback(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewCustomCallbackEventArgs) Handles gridMeetings.CustomCallback
        Dim params As String() = e.Parameters.Split("|")
        Session("dimtpbr") = params
        PopupUpload.ShowOnPageLoad = True
    End Sub

    Protected Sub ASPxGridViewExporter2_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter2.RenderBrick

        Call GlobalRenderBrick(e)

        Dim dc As GridViewDataColumn = TryCast(e.Column, GridViewDataColumn)

        If dc.FieldName.StartsWith("D_") Then
            If e.RowType <> GridViewRowType.Header Then
                Dim v As String = "-1"
                Try
                    v = gridMeetings.GetRowValues(e.VisibleIndex, dc.FieldName.Replace("D_", "M_")).ToString()
                Catch
                    Return
                End Try
                If v = "0" Then
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                    e.TextValue = "N"
                ElseIf v = "-1" Then
                    e.BrickStyle.BackColor = System.Drawing.Color.LightGray
                    e.TextValue = "-"
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.Green
                    e.TextValue = "Y"
                End If
            End If
        End If

    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "MeetingMinutes_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter2.FileName = sFileName
        ASPxGridViewExporter2.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub fileUploadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles fileUploadButton.Click

        Dim sre As String = "Please select a PDF file (500kb or less)"

        If fileUpload.HasFile Then

            If Not fileUpload.PostedFile.FileName.EndsWith(".pdf") Or fileUpload.PostedFile.ContentType <> "application/pdf" Or fileUpload.PostedFile.ContentLength > 500000 Then
                PopupUploadControl.ShowOnPageLoad = True
            Else
                Using conn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString)
                    Dim comm As SqlCommand = New SqlCommand("p_dimtpbr_add", conn)
                    comm.CommandType = System.Data.CommandType.StoredProcedure
                    comm.Parameters.AddWithValue("@sDealerCode", gridMeetings.GetRowValues(Session("dimtpbr")(0), "DealerCode").ToString())
                    comm.Parameters.AddWithValue("@nUserId", Session("UserID"))
                    comm.Parameters.AddWithValue("@nMonth", Integer.Parse(Session("dimtpbr")(1)))
                    comm.Parameters.AddWithValue("@bFileData", fileUpload.FileBytes)
                    conn.Open()
                    comm.ExecuteNonQuery()
                End Using
                PopupUpload.ShowOnPageLoad = False
                ' Re-bind to get latest updates
                gridMeetings.DataBind()
            End If

        Else
            PopupUploadControl.ShowOnPageLoad = True
        End If

    End Sub

    Protected Sub btnVisits_Click(sender As Object, e As EventArgs) Handles btnVisits.Click
        Response.Redirect("VisitActions.aspx")
    End Sub

End Class

