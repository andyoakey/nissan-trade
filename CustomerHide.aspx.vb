﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web


Partial Class CustomerHide

    Inherits System.Web.UI.Page

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Hide Customers"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Hide Customers", "Hiding Customer records")
        End If

    End Sub

    Protected Sub dsCompanies_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCompanies.Init
        dsCompanies.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ViewCompanies.aspx")
    End Sub

    Protected Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHide.Click

        Dim sStr As String = ""
        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        lblErr1.Text = ""
        htIn.Add("@sDealerCode", Session("SelectionId"))
        htIn.Add("@sHideIDs", Session("DeDupeIDs"))
        nRes = da.Update(sErr, "p_CustomerHide", htIn)
        If sErr.Length <> 0 Then
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customers hidden!", "")
        Response.Redirect("ViewCompanies.aspx")

    End Sub

    Protected Sub gridCompanies_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompanies.HtmlDataCellPrepared
        If e.GetValue("BusinessURN") <> "" Then
            e.Cell.Font.Bold = True
        Else
            e.Cell.Font.Bold = False
        End If
    End Sub

End Class
