﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VisitActionActivityUpd.aspx.vb" Inherits="VisitActionActivityUpd" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <dx:ASPxSpellChecker ID="ASPxSpellChecker2" runat="server" ClientInstanceName="spellChecker" Culture="English (United States)">
        <Dictionaries>
            <dx:ASPxSpellCheckerISpellDictionary AlphabetPath="~/App_Data/Dictionaries/EnglishAlphabet.txt"
                GrammarPath="~/App_Data/Dictionaries/english.aff" DictionaryPath="~/App_Data/Dictionaries/american.xlg"
                CacheKey="ispellDic" Culture="English (United States)" EncodingName="Western European (Windows)">
            </dx:ASPxSpellCheckerISpellDictionary>
        </Dictionaries>
        <ClientSideEvents BeforeCheck="function(s, e) { checkButton.SetEnabled(false);}"
            AfterCheck="function(s, e) { checkButton.SetEnabled(true);}" 
            CheckCompleteFormShowing="function(s, e) {e.cancel=true;} " />
    </dx:ASPxSpellChecker>

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px; padding-bottom:10px;">
            <table width="968px">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Visit Action Activity" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right" >
                    </td>
                </tr>
            </table>
        </div>

        <table id="ActivityTable" style="width:675px">

            <tr>
                <td style="padding-top:10px; vertical-align:top">
                    <asp:Label ID="Label2" runat="server" Text="Activity : " Font-Names="Calibri,Verdana"></asp:Label>
                </td>
                <td style="padding-left:3px; padding-top:10px; vertical-align:top">
                    <asp:TextBox ID ="txtActivity" runat="server" Width="550px" Font-Names="Calibri,Verdana" TabIndex="1"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td style="vertical-align:top; padding-top:10px; vertical-align:top">
                    <asp:Label ID="Label5" runat="server" Text="Comments : " Font-Names="Calibri,Verdana"></asp:Label>
                </td>
                <td style="padding-left:3px; padding-top:10px; vertical-align:top">
                    <asp:TextBox ID="txtComments" Width="548px" Font-Names="Calibri,Verdana" Height="268px" runat="server" TextMode="MultiLine" TabIndex="2"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td style="padding-top:10px; vertical-align:top">
                    <asp:Label ID="Label6" runat="server" Text="Responsibility : " Font-Names="Calibri,Verdana"></asp:Label>
                </td>
                <td style="padding-left:3px;padding-top:10px; vertical-align:top">
                    <asp:TextBox ID ="txtResponsibility" runat="server" Width="120px" Font-Names="Calibri,Verdana" TabIndex="3">
                    </asp:TextBox>
                </td>
            </tr>

            <tr>
                <td style="padding-top:15px; vertical-align:top">
                    <asp:Label ID="Label4" runat="server" Text="Target Date : " Font-Names="Calibri,Verdana"></asp:Label>
                </td>
                <td style="padding-top:10px">
                    <table>
                        <tr>
                            <td style="width: 22%" valign="top">
                                <dxe:ASPxDateEdit ID="txtTargetDate" runat="server" Width="120px" Font-Names="Calibri,Verdana" TabIndex="4" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter">
                                </dxe:ASPxDateEdit>
                            </td>
                            <td valign="top" align="right" >
                                <dxe:ASPxButton ID="checkButton" runat="server" ClientInstanceName="checkButton" ClientEnabled="True" Text="Check Spelling ..." AutoPostBack="False" Width="90px" >
                                    <ClientSideEvents Click="function(s, e) { spellChecker.CheckElementsInContainer(document.getElementById('ActivityTable')) }" />
                                </dxe:ASPxButton>                            
                                <dxe:ASPxButton ID="btnNewVisitLine" runat="server" ClientInstanceName="btnNewVisitLine" Text="Save" Width="90px" TabIndex="5" Style="margin-left:5px;">
                                </dxe:ASPxButton>
                                <dxe:ASPxButton ID="btnCancel" runat="server" ClientInstanceName="btnCancel" Text="Cancel" Width="90px" TabIndex="6" Style="margin-left:5px;">
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>

    </div>

</asp:Content>











