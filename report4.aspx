﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="report4.aspx.vb" Inherits="report4" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
        
        <table width="100%">
            <tr>
                <td width="260px">
                    <dxe:ASPxLabel 
                        ID="lblPageTitle" 
                        Text="Tools and Help"
                        Font-Size="Larger" runat="server">
                    </dxe:ASPxLabel>
                </td>
                <td style="width: calc(100% - 260px);">
                    <dxe:ASPxLabel 
                        ID="lblSubTitle" 
                        Text="Excluded Sales"
                        Font-Size="Larger" runat="server">
                    </dxe:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td width="260px">
                     <%-- Icon Buttons --%>
                    <dxe:ASPxButton ID="btn1" runat="server" Style="left: 0px; position: absolute; top: 40px;
                        z-index: 2;"  EnableDefaultAppearance="false"  Text="" CssClass ="special" HorizontalPosition="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/ExcludedSales.png" Image-UrlHottracked="~/Images2014/ExcludedSales_Hover.png">
                    </dxe:ASPxButton>
        
                    <dxe:ASPxButton ID="btn2" runat="server" Style="left: 125px; position: absolute; 
                        top: 40px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/DataChecker.png" BackColor="Transparent"
                        Image-UrlHottracked="~/Images2014/DataChecker_Hover.png">
                    </dxe:ASPxButton>
       
                    <dxe:ASPxButton ID="btn3" runat="server" Style="left: 0px; position: absolute; top: 160px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/PartChecker.png" Image-UrlHottracked="~/Images2014/PartChecker_Hover.png">
                    </dxe:ASPxButton>
        
                    <dxe:ASPxButton ID="btn4" runat="server" Style="left: 125px; position: absolute;
                        top: 160px; z-index: 2;" EnableDefaultAppearance="false" 
                        Text="" CssClass ="special" Visible="True" Border-BorderStyle="None" Image-Url="~/Images2014/QuickInvoiceSearch1.png" BackColor="Transparent"
                        Image-UrlHottracked="~/Images2014/QuickInvoiceSearch1_Hover.png">
                    </dxe:ASPxButton>
        
                    <dxe:ASPxButton ID="btn5" runat="server" Style="left: 0px; position: absolute; top: 280px; 
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/DocumentLibrary.png" Image-UrlHottracked="~/Images2014/DocumentLibrary_Hover.png">
                    </dxe:ASPxButton>
        
                    <dxe:ASPxButton ID="btn6" runat="server" Style="left: 125px; position: absolute; top: 280px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/ContactUs.png" Image-UrlHottracked="~/Images2014/ContactUs_Hover.png">
                    </dxe:ASPxButton>
        
                    <dxe:ASPxButton ID="btn7" runat="server" Style="left: 0px; position: absolute; top: 400px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/ChangePassword.png" Image-UrlHottracked="~/Images2014/ChangePassword_Hover.png">
                    </dxe:ASPxButton>

                    <dxe:ASPxButton ID="btn8" runat="server" Style="left: 125px; position: absolute; top: 400px;
                        z-index: 2;" EnableDefaultAppearance="false"  Text="" CssClass ="special" Visible="True" BackColor="Transparent"
                        Border-BorderStyle="None" Image-Url="~/Images2014/DealerSettings.png" Image-UrlHottracked="~/Images2014/DealerSettings_Hover.png">
                    </dxe:ASPxButton>


                </td>
                <td style="width: calc(100% - 260px);">
                    <dxe:ASPxGridView ID="gridSummary" 
                        runat="server" 
                        DataSourceID="dsExcludedSalesSummary"  
                        AutoGenerateColumns="False"       
                        KeyFieldName="CustomerDealerId" 
                        OnCustomColumnDisplaytext="gridExcludedInvoices_OnCustomColumnDisplayText"
                        Width="100%"
                        Style="margin-top: 12px;">
                   
                        <SettingsPager Visible="False">
                        </SettingsPager>

                        <Settings ShowFilterBar="Hidden" ShowFilterRowMenu="false" />

                        <Styles>
                            <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" ImageSpacing="5px" SortingImageSpacing="5px" />
                            <Cell HorizontalAlign="Center" />
                            <LoadingPanel ImageSpacing="10px" />
                        </Styles>                   

                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>

                        <Columns>
                        
                            <dxe:GridViewDataTextColumn Caption="Excluded Sales (YTD)"  FieldName="ExcludedSales" VisibleIndex="0" Width="25%">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dxe:GridViewDataTextColumn>

                            <dxe:GridViewDataTextColumn Caption="Included Sales (YTD)" FieldName="IncludedSales" VisibleIndex="1" Width="25%">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dxe:GridViewDataTextColumn>

                            <dxe:GridViewDataTextColumn Caption="Total Sales (YTD)" FieldName="TotalSales" VisibleIndex="2" Width="25%">
                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                </PropertiesTextEdit>
                            </dxe:GridViewDataTextColumn>

                            <dxe:GridViewDataTextColumn Caption="Excluded (%)" FieldName="ExcludedPerc" VisibleIndex="3" Width="25%">
                                <PropertiesTextEdit DisplayFormatString="0.00">
                                </PropertiesTextEdit>
                            </dxe:GridViewDataTextColumn>

                        </Columns>
                        
                    </dxe:ASPxGridView>     
                    
                    <dxchartsui:WebChartControl ID="chartExcludedSales" runat="server" 
                        CrosshairEnabled="True" Height="350px" PaletteName="Toyota" Width="1300px" Style="margin-top: 20px;" CssClass="graph_respond">
                        <palettewrappers>
                            <dxchartsui:PaletteWrapper Name="Toyota" ScaleMode="Repeat">
                                <palette>
                                    <dxcharts:PaletteEntry Color="72, 127, 20" Color2="72, 127, 20" />
                                    <dxcharts:PaletteEntry Color="155, 31, 90" Color2="155, 31, 90" />
                                    <dxcharts:PaletteEntry Color="217, 165, 160" Color2="217, 165, 160" />
                                    <dxcharts:PaletteEntry Color="166, 40, 28" Color2="166, 40, 28" />
                                    <dxcharts:PaletteEntry Color="143, 166, 28" Color2="143, 166, 28" />
                               </palette>
                            </dxchartsui:PaletteWrapper>
                        </palettewrappers>
                    </dxchartsui:WebChartControl>   
                </td>
            </tr>
        </table>
    </div>
    
    
    <asp:SqlDataSource ID="dsExcludedSalesSummary" runat="server" SelectCommand="p_ExcludedSales_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>                    
