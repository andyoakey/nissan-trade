﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="WebStatus.aspx.vb" Inherits="WebStatus" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register assembly="DevExpress.Web.v14.2" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width: 976px; margin: 0 auto; text-align: left;">
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Web Status..." 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <dxwgv:ASPxGridView ID="gridStatus" runat="server" DataSourceID="dsStatus" AutoGenerateColumns="False" KeyFieldName="DealerCode" Width="976px" >
            
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            
            <Settings ShowHeaderFilterButton="False" UseFixedTableLayout="True" 
                VerticalScrollableHeight="300" ShowFilterRow="False" 
                ShowVerticalScrollBar="True" />

            <SettingsBehavior AllowSort="true" />
            
            <SettingsPager PageSize="0" Mode="ShowAllRecords" Visible="False">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </SettingsPager>

            <Columns>
            
                <dxwgv:GridViewDataTextColumn 
                    FieldName="CreatedDate" 
                    Caption="Date/Time" 
                    VisibleIndex="0" Width="20%"> 
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="ProcessId" 
                    Caption="Process ID" 
                    VisibleIndex="1" Width="10%">  
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="ProcessName" 
                    Caption="Process Name" 
                    VisibleIndex="2" Width="15%"> 
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="Description" 
                    Caption="Description" 
                    VisibleIndex="3"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            
        </dxwgv:ASPxGridView>
        <br />
        
    </div>

    <asp:SqlDataSource ID="dsStatus" runat="server" SelectCommand="p_Sel_WebStatus" SelectCommandType="StoredProcedure">
        <SelectParameters>
        </SelectParameters>
    </asp:SqlDataSource>
    
</asp:Content>


