﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="QuickInvoiceSearch.aspx.vb" Inherits="QuickInvoiceSearch" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <dx:ASPxLabel
                ID="lblPageTitle" 
                Font-Size="Larger"
                runat="server" 
                Text="Quick Invoice Search" />
        </div>

        <table id="trSelectionStuff"style="position: relative; left: 2px">
    
            <tr id="Tr1" runat="server" style="height:30px;">
                <td style="width:20%; font-size: small;" align="left" >
                    <dx:ASPxLabel
                        ID="lblInvoice" 
                        runat="server" 
                        Text="Invoice Number" />
                </td>
       
                <td style="width:20%; font-size: small;" align="left" >
                    <dx:ASPxLabel
                        ID="lblPartNumber" 
                        runat="server" 
                        Text="Part Number" />
                </td>
                
                <td style="width:20%; font-size: small;" align="left" >
                    <dx:ASPxLabel
                        ID="lblMonth" 
                        runat="server" 
                        Text="Month" />
                </td>
                
                <td style="width:40%; font-size: small;" align="left" >
                </td>
            </tr>

            <tr id="Tr6" runat="server" style="height:30px;">

                <td style="width:20%;" align="left" >
                    <dx:ASPxTextBox ID="txtInvoiceNumber" runat="server" Width="170px" >
                    </dx:ASPxTextBox>
                </td>

                <td style="width:20%;" align="left" >
                    <dx:ASPxTextBox ID="txtPartNumber" runat="server" Width="170px" >
                    </dx:ASPxTextBox>
                </td>

                <td style="width:20%;" align="left" >
                    <dx:ASPxComboBox
                        ID="ddlFrom" 
                        runat="server" 
                        Width="180px"   />
                </td>

                <td style="width:20%;" align="left" >
                    <dx:ASPxButton 
                        ID="btnSearch" 
                        ClientInstanceName="btnGo" 
                        AutoPostBack="true"
                        runat="server" 
                        Text="Search" 
                        Width="100px">
                    </dx:ASPxButton>
                </td>

                <td style="width:20%;" align="left" >
                    <dx:ASPxButton 
                        ID="btnClear" 
                        ClientInstanceName="btnClear" 
                        AutoPostBack="true"
                        runat="server" 
                        Text="Clear" 
                        Width="100px">
                    </dx:ASPxButton>
                </td>

            </tr>
            <tr>
                <td colspan="4">
                    <dx:ASPxLabel ID="lblError" runat="server" Visible = "false" ForeColor="Red" Font-Italic="true" 
                        Text="Enter something in the Invoice Number or Part Number boxes">
                    </dx:ASPxLabel>
                </td>
            </tr>
        </table> 
        
        <br />

        <dx:ASPxGridView 
            ID="gridInvoices" 
            runat="server" 
            AutoGenerateColumns="False" 
            OnLoad="GridStyles"
            OnCustomColumnDisplayText="gridInvoices_OnCustomColumnDisplayText" 
            style="position: relative; left: 5px" Width="100%" >

            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
                <Header Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <Cell HorizontalAlign="Center" />
            </Styles>

            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true">
            </SettingsPager>

            <Settings ShowFooter="False" ShowGroupedColumns="False" ShowGroupFooter="Hidden" ShowGroupPanel="False"
                ShowHeaderFilterButton="True"  ShowFilterRow="false" ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True"/>

            <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"/>

<%--                                <Settings AllowAutoFilter="True" AllowHeaderFilter="True" />--%>

            <Columns>

                <dx:GridViewDataDateColumn Caption="Date" FieldName="invoicedate" VisibleIndex="0" Width="10%" ExportWidth="150" />

                <dx:GridViewDataTextColumn Caption="Dealer" FieldName="dealercode" VisibleIndex="1" Width="5%" ExportWidth="100" />

                <dx:GridViewDataTextColumn Caption="Customer" FieldName="customername" VisibleIndex="2" Width="20%" ExportWidth="300">
                    <CellStyle Font-Size="X-Small"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="DMS Ref." FieldName="dmsref" VisibleIndex="3" Width="10%" ExportWidth="150"/>

                <dx:GridViewDataTextColumn Caption="Account Name" FieldName="accountname" VisibleIndex="4" Width="20%" ExportWidth="300">
                    <CellStyle Font-Size="X-Small" />
                </dx:GridViewDataTextColumn>

               <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                    <DataItemTemplate>
                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                

                <dx:GridViewDataTextColumn Caption="Sales Value" CellStyle-HorizontalAlign="Right"
                    FieldName="salevalue" ToolTip="The total value of all parts sold in this PFC/Month/Year"
                    VisibleIndex="6" ExportWidth="150">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Cost Value" CellStyle-HorizontalAlign="Right"
                    FieldName="costvalue" ToolTip="The total cost of all parts purchased in this PFC/Month/Year"
                    VisibleIndex="7" ExportWidth="150">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Margin (%)" CellStyle-HorizontalAlign="Right"
                    FieldName="margin" ToolTip="The total percentage margin made on sales of all parts in this PFC/Month/Year"
                    VisibleIndex="8" ExportWidth="150">
                    <PropertiesTextEdit DisplayFormatString="0.00">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                </dx:GridViewDataTextColumn>

            </Columns>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ExportMode="Expanded" />

        </dx:ASPxGridView>
        
        <br />
 
    </div>
    
    <asp:SqlDataSource ID="dsInvoices" runat="server" SelectCommand="sp_QuickInvoiceSearch" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sInvoiceNumber" SessionField="InvoiceNumber" Type="String" Size="50" />
            <asp:SessionParameter Name="sPartNumber" SessionField="PartNumber" Type="String" Size="50"  />
            <asp:SessionParameter Name="nMonth" SessionField="MonthFrom" Type="Int32" />        
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridInvoices">
    </dx:ASPxGridViewExporter>

</asp:Content>

