﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewBestSellingCustomers.aspx.vb" Inherits="ViewBestSellingCustomers" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:0px">
               <dx:ASPxLabel 
        Font-Size="Larger"
        ID="ASPxLabel5" 
        runat ="server" 
        Text="Highest Value Customers" />
        </div>

        <table id="trSelectionStuff"  Width="100%" style="margin: 10px 0;">
    
            <tr runat="server" >
                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel6"
        runat="server" 
        Text="Product Group">
    </dx:ASPxLabel>
                </td>
         
                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel7"
        runat="server" 
        Text="Trade Analysis">
    </dx:ASPxLabel>
                </td>

                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel8"
        runat="server" 
        Text="Period">
    </dx:ASPxLabel>
                </td>
                
                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel9"
        runat="server" 
        Text="Number of Items">
    </dx:ASPxLabel>
                 </td>
           
                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel10"
        runat="server" 
        Text="Basis">
    </dx:ASPxLabel>
                </td>
                
                <td width="12%" >
                        <dx:ASPxLabel 
        id="ASPxLabel11"
        runat="server" 
        Text="Customers">
    </dx:ASPxLabel>
               </td>

               <td width="28%" >
                        <dx:ASPxLabel 
        id="ASPxLabel1"
        runat="server" 
        Text="Business Type">
    </dx:ASPxLabel>
               </td>

            </tr>

              <tr id="Tr6" runat="server" style="height:30px;">
                <td align="left" >
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlProductGroup"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All" Value="ALL" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="ACC"/>
                            <dx:ListEditItem Text="Damage" Value="DAM"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="EXM"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="MEC"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="ROM"/>
                            <dx:ListEditItem Text="Additional Product" Value="OTH"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td align="left" >
                    <dx:ASPxComboBox
                        ID="ddlTA"
                        ValueField="TradeAnalysis"
                        TextField="TradeAnalysis"
                        runat="server" 
                        DataSourceID="dsTradeAnalysis"
                        SelectedIndex="0"
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                </td>

                <td align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlPeriod"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Month to Date" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Year to Date" Value="1"/>
                            <dx:ListEditItem Text="Last 12 Months" Value="2"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlTopN"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Top 50" Value="0" />
                            <dx:ListEditItem Text="Top 100" Value="1" Selected="true"/>
                            <dx:ListEditItem Text="Top 500" Value="2"/>
                            <dx:ListEditItem Text="Top 1000" Value="3"/>
                            <dx:ListEditItem Text="Top 2500" Value="4"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>

                <td align="left" >
                     <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlBasis"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Units Sold" Value="0" />
                            <dx:ListEditItem Text="Sales Value" Value="1" Selected="true"/>
                            <dx:ListEditItem Text="Cost Value" Value="2"/>
                            <dx:ListEditItem Text="Profit (£)" Value="3"/>
                         </Items>
                    </dx:ASPxComboBox>
                </td>

                <td align="left">
                    <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Focus only" Value="1" />
                            <dx:ListEditItem Text="Excluding Focus" Value="2"/>
                         </Items>
                    </dx:ASPxComboBox>
                </td>

                 <td align="left">
                    <dx:ASPxComboBox 
                        ID="ddlBusinessType" 
                        AutoPostBack="True"
                        runat="server">
                    </dx:ASPxComboBox>
                 </td>       

            </tr>
        </table> 
            
        <dx:ASPxGridView 
            ID="gridBestCustomers" 
            runat="server"                                                                                                                                                                                                                                             
            CssClass="grid_styles"
            OnLoad="GridStyles"
            AutoGenerateColumns="False"
            OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText"
            DataSourceID="dsBestCustomers" 
            Visible="True" 
            Width="100%">
            
            <SettingsPager 
                AllButton-Visible="true" 
                AlwaysShowPager="False" 
                FirstPageButton-Visible="true" 
                LastPageButton-Visible="true" 
                PageSize="20">
            </SettingsPager>

            <Settings 
                UseFixedTableLayout="true"
                ShowFilterRowMenu="false" 
                ShowFooter="false" 
                ShowGroupedColumns="false"
                ShowHeaderFilterButton="false"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="false"/>
                
            <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" /> 

            <Columns>

                <dx:GridViewDataTextColumn 
                    Caption="Rank" 
                    CellStyle-HorizontalAlign= "Right"
                    HeaderStyle-HorizontalAlign="Right"
                    readonly ="true" 
                    FieldName="ranking" 
                    VisibleIndex="0"
                    Width="5%" ExportWidth="75">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    Caption="DMS Ref" 
                    readonly ="true" 
                    FieldName="refcode" 
                    VisibleIndex="1"
                    Width="10%" ExportWidth="100">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                 </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Customer" 
                    readonly ="true" 
                    FieldName="customername" 
                    VisibleIndex="2"
                    Width="30%" ExportWidth="300">
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

 <%--               <dx:GridViewDataTextColumn 
                    Caption="Postcode" 
                    readonly ="true" 
                    FieldName="customerpostcode" 
                    VisibleIndex="3"
                    Width="10%" ExportWidth="300">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Email" 
                    readonly ="true" 
                    FieldName="emailaddress" 
                    VisibleIndex="4"
                    Width="25%" ExportWidth="300">
                    <HeaderStyle HorizontalAlign="Left" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Telephone" 
                    readonly ="true" 
                    FieldName="customertelephonenumber" 
                    VisibleIndex="5"
                    Width="10%" ExportWidth="300">
                    <HeaderStyle HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>--%>


                <dx:GridViewDataTextColumn 
                     Caption="Units Sold" 
                     readonly ="true" 
                     FieldName="quantity" 
                     Width="10%" 
                     VisibleIndex="6" ExportWidth="150">
                     <PropertiesTextEdit DisplayFormatString="##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Sales Value" 
                     readonly ="true" 
                     FieldName="salevalue" 
                     Width="10%" 
                     VisibleIndex="7" ExportWidth="150">
                     <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Cost Value" 
                     readonly ="true" 
                     Width="10%" 
                     FieldName="costvalue" 
                     VisibleIndex="8" ExportWidth="150">
                     <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn> 

                <dx:GridViewDataTextColumn 
                     Caption="Margin (%)" 
                     readonly ="true" 
                     Width="10%" 
                     FieldName="margin" 
                     Name="margin"
                     VisibleIndex="9" ExportWidth="150">
                     <PropertiesTextEdit DisplayFormatString="0.00">
                     </PropertiesTextEdit>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                </dx:GridViewDataTextColumn>
                 
            </Columns>
    
        </dx:ASPxGridView>

   </div>

    <asp:SqlDataSource ID="dsBestCustomers" runat="server" SelectCommand="p_BestSellingCustomersNissan" SelectCommandType="StoredProcedure" ConnectionString="databaseconnectionstring">
        <SelectParameters>
            <asp:SessionParameter Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="sLevel4_Code" SessionField="Level4_Code" Type="String" Size="3"  />
            <asp:SessionParameter Name="sTradeAnalysis" SessionField="TA" Type="String" Size="50"  />
            <asp:SessionParameter Name="nPeriod" SessionField="Period" Type="Int32" />
            <asp:SessionParameter Name="nItems" SessionField="Items" Type="Int32" />
            <asp:SessionParameter Name="nBasis" SessionField="Basis" Type="Int32" />
            <asp:SessionParameter Name="nFocus" SessionField="Focus" Type="Int32" Size="0" />
            <asp:SessionParameter Name="nBusinessType" SessionField="BusinessType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource 
        ID="dsTradeAnalysis" 
        runat="server" 
        SelectCommand="sp_TradeAnalysisProducts" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    
    <dx:ASPxGridViewExporter 
        ID="ExpBestCustomers"
        runat="server" 
        GridViewID="gridBestCustomers">
    </dx:ASPxGridViewExporter>

</asp:Content>



