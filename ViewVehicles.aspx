﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewVehicles.aspx.vb" Inherits="ViewVehicles" title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>

<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="border-bottom: medium solid #000000; position: relative; top: 5px; font-family: 'Lucida Sans'; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Vehicle Details" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridVehicleList" runat="server" 
            DataSourceID="dsVehicleList"  AutoGenerateColumns="False" KeyFieldName="RegNumber"
             Width="100%">
       
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="16">
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Settings
                ShowFooter="False" ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded" ShowFilterRow="True" 
                UseFixedTableLayout="True" />
            <SettingsBehavior AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" />
            
            <ImagesFilterControl>
                <LoadingPanel >
                </LoadingPanel>
            </ImagesFilterControl>
            
            <Images >
                <CollapsedButton Height="12px" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="Model" FieldName="DistributorModelCode"
                    VisibleIndex="1" Width="10%">
                    <Settings AllowGroup="True" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Reg. No" FieldName="RegNumber"
                    VisibleIndex="2" Width="8%">
                    <Settings AllowGroup="True" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Chassis" FieldName="ChassisNumber"
                    VisibleIndex="3" Width="15%">
                    <Settings AllowAutoFilter="True" AllowGroup="True" AllowHeaderFilter="True" 
                        ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="Description"
                    VisibleIndex="4" Width="15%">
                    <Settings AllowAutoFilter="True" AllowGroup="True" AllowHeaderFilter="True" 
                        ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="CC" FieldName="CC"
                    VisibleIndex="5" Width="8%">
                    <Settings AllowGroup="True" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="False" ShowFilterRowMenu="True" ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Registered" FieldName="RegDate"
                    VisibleIndex="6" Width="75px">
                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="False" ShowFilterRowMenu="True" ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>   
                
                <dxwgv:GridViewDataTextColumn Caption="Mileage" FieldName="Mileage"
                    VisibleIndex="7" Width="75px">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="False" ShowFilterRowMenu="True" ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Next Service Date" FieldName="NextServiceDate"
                    VisibleIndex="8" Width="75px">
                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="False" ShowFilterRowMenu="True" ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>  

                <dxwgv:GridViewDataTextColumn Caption="Date Last Seen" FieldName="DateLastSeen"
                    VisibleIndex="9" Width="75px">
                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                    </PropertiesTextEdit>
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="False" ShowFilterRowMenu="True" ShowInFilterControl="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn> 
                                                             
            </Columns>
            
            <Templates>
            
                <DetailRow>
                    <br />                        
                    <dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" Width="996px"
                        >
                        <ContentStyle>
                            <Border BorderColor="#7C7C94" BorderStyle="Solid" BorderWidth="1px" />
                        </ContentStyle>
                        
                        <TabPages>
                            <%------------------------------------------------------------------------------------------
                            ' TAB 1
                            ------------------------------------------------------------------------------------------%>
                            <dxtc:TabPage Text="Owners">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl1" runat="server">
                                        <br />
                                        <dxwgv:ASPxGridView ID="gridOwners" runat="server" KeyFieldName="RegNumber" DataSourceID="dsVehicleListOwners"
                                            AutoGenerateColumns="False" OnBeforePerformDataSelect="gridOwners_BeforePerformDataSelect" Width="926px"
                                            >
                                            <Styles >
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <SettingsPager PageSize="16">
                                                <AllButton Visible="True">
                                                </AllButton>
                                                <FirstPageButton Visible="True">
                                                </FirstPageButton>
                                                <LastPageButton Visible="True">
                                                </LastPageButton>
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" ShowGroupedColumns="True"
                                                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                                                ShowStatusBar="Hidden" ShowTitlePanel="True" />
                                            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                            <ImagesFilterControl>
                                                <LoadingPanel >
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Images >
                                                <LoadingPanelOnStatusBar >
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel>
                                                </LoadingPanel>
                                            </Images>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="Owned from" FieldName="SaleDate" ReadOnly="True"
                                                    VisibleIndex="0" Width="100px">
                                                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                                                    </PropertiesTextEdit>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Mileage" FieldName="MileageAtSale" VisibleIndex="1"
                                                    Width="100px">
                                                    <PropertiesTextEdit DisplayFormatString="#,##0">
                                                    </PropertiesTextEdit>
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Name" FieldName="CustomerName" VisibleIndex="2"
                                                    Width="150px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Address" FieldName="Address" VisibleIndex="3"
                                                    Width="350px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Telephone" FieldName="Tele1" VisibleIndex="4"
                                                    Width="100px">
                                                </dxwgv:GridViewDataTextColumn>
                                            </Columns>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                            <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />
                                        </dxwgv:ASPxGridView>
                                        <br />
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <%------------------------------------------------------------------------------------------
                            ' TAB 2
                            ------------------------------------------------------------------------------------------%>
                            <dxtc:TabPage Text="Service History">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl2" runat="server">
                                        <br />
                                        <dxwgv:ASPxGridView ID="gridWork" runat="server" KeyFieldName="RegNumber" DataSourceID="dsVehicleListWork"
                                            AutoGenerateColumns="False" OnBeforePerformDataSelect="gridWork_BeforePerformDataSelect" Width="926px"
                                            >
                                            
                                            <Styles >
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>

                                            <SettingsPager PageSize="16">
                                                <AllButton Visible="True">
                                                </AllButton>
                                                <FirstPageButton Visible="True">
                                                </FirstPageButton>
                                                <LastPageButton Visible="True">
                                                </LastPageButton>
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" ShowGroupedColumns="True"
                                                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                                                ShowStatusBar="Hidden" ShowTitlePanel="True" />
                                            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                            <ImagesFilterControl>
                                                <LoadingPanel >
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Images >
                                                <LoadingPanelOnStatusBar >
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel >
                                                </LoadingPanel>
                                            </Images>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="Invoice Date" FieldName="InvoiceDate"
                                                    VisibleIndex="0" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                                <dxwgv:GridViewDataTextColumn Caption="Invoice No." FieldName="InvoiceNumber"
                                                    VisibleIndex="1" Width="75px">
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                                <dxwgv:GridViewDataTextColumn Caption="RTS Code" FieldName="RTSCode" VisibleIndex="2"
                                                    Width="150px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="LabourDescription" VisibleIndex="3"
                                                    Width="350px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Cost" FieldName="SaleValue"
                                                    VisibleIndex="4" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##00">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                            </Columns>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                            <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />
                                        </dxwgv:ASPxGridView>
                                        <br />
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <%------------------------------------------------------------------------------------------
                            ' TAB 3
                            ------------------------------------------------------------------------------------------%>
                            <%--<dxtc:TabPage Text="Invoices">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl5" runat="server">
                                        <br />
                                        <dxwgv:ASPxGridView ID="gridInvoices" runat="server" KeyFieldName="RegNumber" DataSourceID="dsVehicleListInvoices"
                                            AutoGenerateColumns="False" OnBeforePerformDataSelect="gridInvoices_BeforePerformDataSelect" Width="926px"
                                            CssFilePath="~/App_Themes/Office2003Silver/{0}/styles.css" CssPostfix="Office2003Silver">
                                            <Styles CssFilePath="~/App_Themes/Office2003Silver/{0}/styles.css" CssPostfix="Office2003Silver">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>
                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <SettingsPager PageSize="16">
                                                <AllButton Visible="True">
                                                </AllButton>
                                                <FirstPageButton Visible="True">
                                                </FirstPageButton>
                                                <LastPageButton Visible="True">
                                                </LastPageButton>
                                            </SettingsPager>
                                            <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowFooter="True" ShowGroupedColumns="True"
                                                ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="True"
                                                ShowStatusBar="Hidden" ShowTitlePanel="True" />
                                            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Silver/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Silver/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Silver/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003Silver/GridView/Loading.gif">
                                                </LoadingPanel>
                                            </Images>
                                            <Columns>
                                                <dxwgv:GridViewDataTextColumn Caption="Invoice Date" FieldName="InvoiceDate"
                                                    VisibleIndex="0" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="dd-MMM-yyyy">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                                <dxwgv:GridViewDataHyperLinkColumn Caption="Invoice" FieldName="InvoiceNumber" 
                                                    PropertiesHyperLinkEdit-Text="InvoiceNumber" PropertiesHyperLinkEdit-Target="_blank"
                                                    PropertiesHyperLinkEdit-TextField="InvoiceNumber" PropertiesHyperLinkEdit-TextFormatString="{0}"
                                                    PropertiesHyperLinkEdit-NavigateUrlFormatString="ViewInvoice.aspx?{0}" VisibleIndex="1"
                                                    Width="150px" ToolTip="The Invoice Number">
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                    <PropertiesHyperLinkEdit Text="InvoiceNumber" Target="_blank"
                                                        NavigateUrlFormatString="ViewInvoice.aspx?{0}" TextField="InvoiceNumber">
                                                    </PropertiesHyperLinkEdit>
                                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" ShowFilterRowMenu="False" />
                                                </dxwgv:GridViewDataHyperLinkColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Centre" FieldName="DealerCode" VisibleIndex="2"
                                                    Width="75px">
                                                </dxwgv:GridViewDataTextColumn>
                                                <dxwgv:GridViewDataTextColumn Caption="Parts" FieldName="SaleValue"
                                                    VisibleIndex="4" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##00">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                                <dxwgv:GridViewDataTextColumn Caption="Labour" FieldName="LabourSaleValue"
                                                    VisibleIndex="5" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##00">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                                <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="TotalSaleValue"
                                                    VisibleIndex="6" Width="75px">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##00">
                                                    </PropertiesTextEdit>
                                                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="False" 
                                                        AllowSort="False" ShowFilterRowMenu="True" />
                                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                                    <CellStyle HorizontalAlign="Center">
                                                    </CellStyle>
                                                </dxwgv:GridViewDataTextColumn> 
                                            </Columns>
                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>
                                            <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />
                                        </dxwgv:ASPxGridView>
                                        <br />
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>--%>
                        </TabPages>
                        <LoadingPanelStyle ImageSpacing="6px">
                        </LoadingPanelStyle>
                    </dxtc:ASPxPageControl>
                    <br />
                </DetailRow>
            
            </Templates>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            <SettingsDetail ShowDetailRow="True" ExportMode="Expanded" />

        </dxwgv:ASPxGridView>
        <br />
        
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsVehicleList" runat="server" SelectCommand="p_VehicleList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsVehicleListOwners" runat="server" SelectCommand="p_VehicleListOwners" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sRegNumber" SessionField="RegNumber" Type="String" Size="10" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsVehicleListWork" runat="server" SelectCommand="p_VehicleListWork" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sRegNumber" SessionField="RegNumber" Type="String" Size="10" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsVehicleListInvoices" runat="server" SelectCommand="p_VehicleListInvoices" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sRegNumber" SessionField="RegNumber" Type="String" Size="10" />
        </SelectParameters>
    </asp:SqlDataSource>


    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridVehicleList" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>





