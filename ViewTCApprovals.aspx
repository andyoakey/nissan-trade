﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewTCApprovals.aspx.vb" Inherits="ViewTCApprovals" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div style=" position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >

        <div id="divTitle" style="position:relative; left:5px">
        
            <table id="trCustomerDetails" Width="100%" >
                <tr>
                    <td align="left" >
                        <asp:Label ID="lblPageTitle" runat="server" Text="Trade Club Approvals" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right" >
                    </td>
                </tr>
            </table>
            
        </div>
        <br />
        
        <dxwgv:ASPxGridView ID="gridCompanies" runat="server" DataSourceID="dsCompanies" AutoGenerateColumns="False" Width="100%" >
            
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
                <Header Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" VerticalAlign="Middle" />
                <Cell HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" Font-Size="X-Small" />
            </Styles>
            
            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="16" />

            <Settings ShowFilterRow="True" ShowFilterRowMenu="True" ShowHeaderFilterButton="True" />

            <Columns>

                <dxwgv:GridViewDataHyperLinkColumn Caption="#" FieldName="Id" VisibleIndex="0" Width="30px">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" ShowFilterRowMenu="False" />
                    <PropertiesHyperLinkEdit NavigateUrlFormatString="ViewTCApprovalDetails.aspx?0{0}" Target="_self"  Text="View">
                    </PropertiesHyperLinkEdit>
                </dxwgv:GridViewDataHyperLinkColumn>

                <dxwgv:GridViewDataTextColumn FieldName="DealerCode" Caption="Centre"  VisibleIndex="0" Width="5%">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="CentreName" Caption="Centre Name" VisibleIndex="1" Width="15%">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="TradeClubNumber" Caption="T/C No."  VisibleIndex="2" Width="5%">
                    <Settings ShowFilterRowMenu="False" AllowAutoFilter="False" AllowHeaderFilter="True" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="BusinessName" Caption="Name"  VisibleIndex="3" Width="15%">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address1" Caption="Address 1" VisibleIndex="4" Visible="True" Width="20%">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address2" Caption="Address 2"  VisibleIndex="5" Visible = "False">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address3" Caption="Address 3"  VisibleIndex="6" Visible = "False">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address4" Caption="Address 4"  VisibleIndex="7" Visible = "False">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address5" Caption="Address 5"  VisibleIndex="8" Visible = "False">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="Address6" Caption="Address 6"  VisibleIndex="9" Visible = "False">
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="PostCode" Caption="Postcode" VisibleIndex="10" Width="75px">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="TelephoneNumber" Caption="Tele." VisibleIndex="11" Width="10%">
                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="CreatedBy" Caption="Requested By" VisibleIndex="12" Width="75px">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="CreatedDate" Caption="Date" VisibleIndex="13" Width="75px">
                    <Settings AllowHeaderFilter="False" />
                    <EditFormSettings Visible="False" />
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            
        </dxwgv:ASPxGridView>
        <br />

    </div>
                
    <asp:SqlDataSource ID="dsCompanies" runat="server" SelectCommand="p_TCApprovalList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCompanies">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>

