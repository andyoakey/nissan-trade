﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class HOBonusReport

    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "HO Bonus Report", "Viewing HO Bonus Report")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete


        If Not IsPostBack Then
            Session("finyear") = Trim(GetDataString("Select FinancialYear From DataPeriods where id =" & Session("CurrentPeriod")))
            Session("shortfinyear") = Mid(Session("finyear"), 3, 5)
        End If

        Me.Title = "Nissan Trade Site - HO Bonus Report " & Session("finyear")

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "BonusReportFY" & Replace(Session("shortfinyear"), "/", "-") ' Cant have / in file names

        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})


    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub GridStylesNo(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub
    Protected Sub dsDealerBonus_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDealerBonus.Init  ' And all the others but dont need to name them explicitly
        dsDealerBonus.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusPayments.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBonusBanding.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString

    End Sub


    Private Sub gridDealerBonus_HtmlDataCellPrepared(sender As Object, e As ASPxGridViewTableDataCellEventArgs) Handles gridDealerBonus.HtmlDataCellPrepared
        If Right(RTrim(e.DataColumn.Name), 6) = "growth" Then

            e.Cell.ForeColor = Color.White

            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If

            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            End If

            If e.CellValue = 0 Then
                e.Cell.BackColor = Color.White
            End If
        End If
    End Sub

    Private Sub btnInfo_Click(sender As Object, e As EventArgs) Handles btnInfo.Click
        popSupportingInfo.ShowOnPageLoad = True
    End Sub

End Class
