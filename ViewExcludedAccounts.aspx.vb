﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class ViewExcludedAccounts

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - View all Companies"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Excluded Accounts", "Viewing Excluded Accounts")
        End If

    End Sub

    Protected Sub dsLinks_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsLinks.Init
        Session("CustomerId") = 0
        dsLinks.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridAccounts_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("CustomerId") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        gridAccounts.SettingsText.Title = Session("Title")
    End Sub



    Protected Sub gridAccounts_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gridAccounts.RowUpdating

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim txtBox As TextBox
        Dim lbl As Label
        Dim pageControl As ASPxPageControl

        Dim gridView As ASPxGridView = CType(sender, ASPxGridView)

        pageControl = TryCast(gridView.FindEditFormTemplateControl("LinkPageControl"), ASPxPageControl)

        lbl = TryCast(pageControl.FindControl("lblCustomerDealerId"), Label)
        htIn.Add("@nCustomerDealerId", lbl.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountName"), TextBox)
        htIn.Add("@sBusinessName", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountAddress1"), TextBox)
        htIn.Add("@sAddress1", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountAddress2"), TextBox)
        htIn.Add("@sAddress2", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountAddress3"), TextBox)
        htIn.Add("@sAddress3", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountAddress4"), TextBox)
        htIn.Add("@sAddress4", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountAddress5"), TextBox)
        htIn.Add("@sAddress5", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountPostCode"), TextBox)
        htIn.Add("@sPostCode", txtBox.Text)

        txtBox = TryCast(pageControl.FindControl("txtAccountTelephoneNumber"), TextBox)
        htIn.Add("@sTelephoneNumber", txtBox.Text)

        htIn.Add("@nUserId", Session("UserId"))

        nRes = da.Update(sErr, "p_CustomerDealer_Upd", htIn)

        e.Cancel = True
        If sErr.Length = 0 Then
            gridView.CancelEdit()
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub btnExcel_Click()
        ASPxGridViewExporter1.FileName = "ExcludedAccountList"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

End Class
