﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PostCodeAllocation.aspx.vb" Inherits="PostCodeAllocation" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >

     <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblTitle" 
                runat ="server" 
                Text="Postcode Allocation" />
    </div>

    <br />

    <dx:ASPxGridView 
        ID="gridPostCodes" 
        OnLoad ="GridStyles"
        runat="server"                                                                                                                                                                                                                                             
        KeyFieldName="id"
        AutoGenerateColumns="False"
        DataSourceID="dsPostCodes" 
        Width="100%">
   
        <SettingsText CommandClearFilter="Clear" Title="Active Filters:" />
   
        <SettingsPager 
            PageSize="16" 
            ShowDefaultImages="True" 
            AllButton-Visible="true"/>
        
        <Settings 
            UseFixedTableLayout="true"
            ShowFilterRowMenu="True" 
            ShowFooter="True" 
            ShowGroupedColumns="True"
            ShowGroupFooter="VisibleIfExpanded" 
            ShowHeaderFilterButton="True"
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False" />
        
        <SettingsBehavior 
            AllowSort="False" 
            ColumnResizeMode="Control" /> 
   
        <Columns>
   
            <dx:GridViewDataTextColumn 
                Caption="id" 
                FieldName="id" 
                visible = "false"
                
                VisibleIndex="0">
            </dx:GridViewDataTextColumn>
        
            <dx:GridViewDataTextColumn 
                ToolTip="The Postal area (usually a city or large town) in which this district is located. For example, the B60 Bromsgrove postal district is in the B - Birmingham Postal Area.  There are 124 Postal Areas in the UK."
                Caption="Postal Area" 
                readonly ="true" 
                FieldName="postalarea" 
                VisibleIndex="1"
                exportwidth="200"
                Width="15%">
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn 
                ToolTip="The code local district / small town / large village assigned to this Post District Code by the Post Office. There are nearly 3,000 Postal Districts in the UK"
                Caption="District" 
                readonly ="true" 
                FieldName="postaldistrict" 
                VisibleIndex="2"
                exportwidth="100"
                Width="15%">
                <HeaderStyle Wrap="True" />
                <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                 ToolTip="The name of the local district / small town / large village to which the postal district code was assigned by the Post Office."
                 Caption="District Name" 
                readonly ="true" 
                 FieldName="districtname" 
                 VisibleIndex="3" 
                exportwidth="250"
                 Width="15%">
                 <Settings AllowAutoFilter ="False"  AllowHeaderFilter="False" />
                <HeaderStyle Wrap="True" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataComboBoxColumn
                 ToolTip="This is the only column you can change. Filter on this column to see postcodes allocated to a specific dealer - eg all the postcodes allocated to Dealer  1012"
                 Caption="Assigned Dealer" 
                 FieldName="assigneddealercode" 
                 PropertiesComboBox-DataSourceID="dsDealers"
                 PropertiesComboBox-TextField="assigneddealercode" 
                 PropertiesComboBox-ValueField="assigneddealercode" 
                 PropertiesComboBox-Style-BackColor="Yellow"
                 VisibleIndex="4" 
                exportwidth="130"
                 Width="15%">
                <HeaderStyle Wrap="True" />
            </dx:GridViewDataComboBoxColumn>
            
            <dx:GridViewDataTextColumn 
                 ToolTip="The name of the assigned dealer"
                 Caption="Assigned Dealer Name" 
                 readonly ="true" 
                 FieldName="assigneddealer" 
                 VisibleIndex="5" 
                exportwidth="200"
                 Width="30%">
                <Settings AllowAutoFilter ="False"  AllowHeaderFilter="False" />
                <HeaderStyle Wrap="True" />
            </dx:GridViewDataTextColumn>  

            <dx:GridViewCommandColumn 
                ToolTip="Click here to amend the Assigned Dealer for your selected Postal District"
                VisibleIndex="7" 
                Width="10%">
                <EditButton Visible="true" />
            </dx:GridViewCommandColumn>
               
         </Columns>
            
      <SettingsEditing  Mode="Inline" />

  </dx:ASPxGridView>

</div>
 
  <asp:SqlDataSource ID="dsPostCodes" 
       runat ="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_PostCodeDistricts_Select" 
        SelectCommandType ="StoredProcedure"
        UpdateCommand="sp_PostCodeDistricts_Update" 
        UpdateCommandType ="StoredProcedure">
        <UpdateParameters>
            <asp:SessionParameter Name="id" Type="Int32" />
            <asp:SessionParameter Name="assigneddealercode" Type="String" />
            <asp:SessionParameter Name="user_id" SessionField="UserID" Type="Int32" />
        </UpdateParameters>
  </asp:SqlDataSource>
 
  <asp:SqlDataSource 
        ID="dsDealers" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_PostCodeDealers_Select" 
        SelectCommandType="StoredProcedure">
  </asp:SqlDataSource>

  <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1"
        runat="server" 
        GridViewID="gridPostCodes">
    </dx:ASPxGridViewExporter>



</asp:Content>

