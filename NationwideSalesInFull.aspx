﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NationwideSalesInFull.aspx.vb" Inherits="NationwideSalesInFull" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Nationwide Sales in Full" />
        </div>

         <br />

        <div id="div" style="position:relative;">
            <table>
                <tr>
                    <td width ="7%">
                         <dx:ASPxComboBox
                            ID="ddlFrom"
                            Width="80px"
                            runat="server" 
                            ValueField="ID"
                            TextField="PeriodName"
                            SelectedIndex="0"
                            DataSourceID="dsMonths"
                            AutoPostBack="True"/>
                   </td>

                    <td width ="2%" align="center">
                       <dx:ASPxLabel 
                            ID="ASPxLabel1" 
                            runat="server" 
                            Text="to" />
                     </td>

                    <td width ="91%" align="left">
                         <dx:ASPxComboBox
                            ID="ddlTo"
                            Width="80px"
                            runat="server" 
                            ValueField="ID"
                            TextField="PeriodName"
                            SelectedIndex="0"
                            DataSourceID="dsMonths"
                            AutoPostBack="True"/>
                   </td>


                </tr>
            </table>
            
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsSales" 
        Styles-Header-HorizontalAlign="Left"
        Styles-CellStyle-HorizontalAlign="Left"
        Styles-Footer-HorizontalAlign="Left"
        Settings-HorizontalScrollBarMode="Auto"
        Styles-Cell-Wrap="False"
        Font-Size="XX-Small"
        Width="100%">

        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />

        <Styles Footer-HorizontalAlign ="Center"/>
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true"/>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="True"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="50px" 
                exportwidth="100"
                Caption="Site ID" 
                FieldName="siteid"
                Name="siteid">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="1" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="200px" 
                exportwidth="100"
                Caption="Site Name" 
                FieldName="sitename"
                Name="sitename">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="2" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="250px" 
                exportwidth="250"
                Caption="Dealer Name" 
                FieldName="dealername"
                Name="dealername">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn
                VisibleIndex="3" 
                Settings-AllowSort="True"
                Settings-AllowGroup="False"
                CellStyle-HorizontalAlign="Left"
                HeaderStyle-HorizontalAlign="Left" 
                FooterCellStyle-HorizontalAlign="Left"
                Width="100px" 
                exportwidth="100"
                Caption="Dealer Code" 
                FieldName="dealercode"
                Name="dealercode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="4" 
                Settings-AllowSort="True"
                Width="100px" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Account Code" 
                FieldName="accountcode"
                Name="accountcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="5" 
                Settings-AllowSort="True"
                Width="60px" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Region" 
                Name="nissan_region"
                FieldName="nissan_region">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="6" 
                Settings-AllowSort="True"
                Width="60px" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Zone" 
                Name="nissan_zone"
                FieldName="nissan_zone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="7" 
                Settings-AllowSort="True"
                Width="80px" 
                exportwidth="150"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Caption="Qube URN" 
                Name="qube_urn"
                FieldName="qube_urn">
           </dx:GridViewDataTextColumn>

          <dx:GridViewDataTextColumn 
                VisibleIndex="8"
                Settings-AllowSort="True"
                Width="150px" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="400"
                Caption="Business Name" 
                Name="businessname"
                FieldName="businessname">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="9" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="150px" 
                exportwidth="150"
                Caption="Address 1" 
                Name="address1"
                FieldName="address1">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="10" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="150px" 
                exportwidth="150"
                Caption="Address 2" 
                Name="address2"
                FieldName="address2">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="11" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="150px" 
                exportwidth="150"
                Caption="Address 3" 
                Name="address3"
                FieldName="address3">
            </dx:GridViewDataTextColumn>
        
            <dx:GridViewDataTextColumn 
                VisibleIndex="12" 
                Settings-AllowSort="True"
                Width="150px" 
                exportwidth="150"
                Caption="Post town" 
                Name="posttown"
                FieldName="posttown">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="13" 
                Settings-AllowSort="True"
                Width="100px" 
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                exportwidth="150"
                Caption="Postcode" 
                Name="postcode"
                FieldName="postcode">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="14" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Phone" 
                FieldName="telephone"
                Name="telephone">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="15" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Invoice No." 
                FieldName="invoicenumber"
                Name="invoicenumber">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataDateColumn 
                VisibleIndex="16" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Invoice Date" 
                FieldName="invoicedate"
                Name="invoicedate">
            </dx:GridViewDataDateColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="17" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Cost Price" 
                FieldName="costprice"
                Name="costprice">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="18" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Retail Price" 
                FieldName="retailprice"
                Name="retailprice">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="19" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Sale Price" 
                FieldName="saleprice"
                Name="saleprice">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="20" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Sale Value" 
                FieldName="salevalue"
                Name="salevalue">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="21" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Partnumber" 
                FieldName="partnumber"
                Name="partnumber">
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="22" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Part Description" 
                FieldName="partdescription"
                Name="partdescription">
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="23" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="100px" 
                exportwidth="150"
                Caption="Product Group" 
                FieldName="productgroup"
                Name="productgroup">
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="24" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Units Sold" 
                FieldName="quantity"
                Name="quantity">
                <PropertiesTextEdit DisplayFormatString="##,##0"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="25" 
                Settings-AllowSort="True"
                CellStyle-HorizontalAlign="Center"
                HeaderStyle-HorizontalAlign="Center"
                Width="80px" 
                exportwidth="150"
                Caption="Discount" 
                FieldName="discountapplied"
                Name="discountapplied">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

         </Columns>
   
            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="costprice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="retailprice" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="salevalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="quantity" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0.00" FieldName="discountapplied" SummaryType="Sum" />
            </TotalSummary>
                 
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsSales" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_NationwideSales" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="nFrom" SessionField="periodfrom" Type="Int32" />
                <asp:SessionParameter Name="nTo" SessionField="periodto" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
   
    <asp:SqlDataSource 
        ID="dsMonths" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="p_Get_Months" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

