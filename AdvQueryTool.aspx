<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AdvQueryTool.aspx.vb" Inherits="AdvQueryTool" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dxpg" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >

       <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" 
                runat ="server" 
                Text="Advanced Reporting Tool" />
        </div>

        <br />

        <table id="tabLoadQuery" runat="server" visible="true" style="width:100%">
            <tr>
                <td style="width:30%">
                    <dx:ASPxComboBox 
                        id ="ddlQueries"
                        runat="server"
                        width="100%">
                    </dx:ASPxComboBox>
                </td>
                <td style="width:75px" valign="middle" align="left" >
                    <dx:ASPxButton ID="btnLoad" runat="server" Text="Load" Width="75px" >
                    </dx:ASPxButton>
                </td>
                <td style="width:75px" valign="middle" align="left">
                    <dx:ASPxButton ID="btnClear" runat="server" Text="Clear" Width="75px">
                    </dx:ASPxButton>
                </td>
                <td style="width:75px" valign="middle" align="left">
                    <dx:ASPxButton ID="btnDelete" runat="server" Text="Delete" Width="75px">
                    </dx:ASPxButton>
                </td>
                <td  valign="middle" align="right">
                    <dx:ASPxButton ID="btnDelOK" runat="server" Text="Confirm Delete Ok" Width="150px" 
                         Visible="false" >
                    </dx:ASPxButton>
                </td>
                <td style="width:75px" valign="middle" align="left">
                    <dx:ASPxButton ID="btnDelCancel" runat="server" Text="Cancel" Width="75px" 
                         Visible="false" >
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <br />

        <dx:ASPxPageControl 
            ID="ASPxPageControl1" 
            runat="server" 
            AutoPostBack="True" 
            Width="100%"
            CssClass="page_tabs" ActiveTabIndex="2">

            <TabPages>

                <%------------------------------------------------------------------------------------------
                ' TAB 1 - Select fields
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Select Report Columns & Date Range">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl4" runat="server">

                            <table id="tabSelection" width="100%">
                                <tr runat="server" id="trSel1">

                                    <td style="width:70%">
                                        <dx:ASPxLabel
                                            id="lbl1"
                                            runat="server"
                                            Font-Bold="true"
                                            text="Select the data columns to include in your results:">
                                        </dx:ASPxLabel>
                                    </td>

                                    <td style="width:30%" align="left">
                                        <dx:ASPxLabel
                                            id="lbl2"
                                            runat="server"
                                            Font-Bold="true"
                                            text="Select the values and dates to include in your results:">
                                        </dx:ASPxLabel>
                                    </td>
                                        
                                </tr>
                            </table>

                            <table width="100%">
                                <tr runat="server" id="tr1">
                                    <td style="width:75%">
                                        <dx:ASPxLabel 
                                            ID="lblErr1" 
                                            runat="server" 
                                            Text="" 
                                            Visible="true" 
                                            ForeColor="#FF3300">
                                        </dx:ASPxLabel>
                                    </td>

                                    <td style="width:25%">
                                        <dx:ASPxLabel 
                                            ID="lblErr2" 
                                            runat="server" 
                                            Text="" 
                                            Visible="true" 
                                            ForeColor="#FF3300">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                            </table>

                            <table>
                                <tr runat="server" id="tr4"   >
                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="Dealer"  Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="Customer" Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="Geographical Location" Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="Product" Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="Invoice" Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:13%">
                                        <dx:ASPxLabel runat="server" Text="User Defined" Font-Bold="true" visible="false"/>
                                    </td>

                                    <td valign="bottom" style="width:18%">
                                        <dx:ASPxLabel runat="server" Text="Values" Font-Bold="true"/>
                                    </td>

                                    <td valign="bottom" style="width:4%">
                                        <dx:ASPxLabel runat="server" Text="Dates" Font-Bold="true"/>
                                    </td>

                                </tr>

                                <tr runat="server" id="tr7"   >
                                    <td valign="top" style="width:13%" align="left" >
                                        <dx:ASPxCheckBoxList
                                            styles="align:left"
                                            ID="chkDealer" 
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Region" Value="0" />    
                                                <dx:ListEditItem Text="Zone" Value="1"/>
                                                <dx:ListEditItem Text="Dealer Code" Value="2" />
                                                <dx:ListEditItem Text="Dealer Name" Value="3" />
                                            </Items>

<Border BorderStyle="None"></Border>

                                           </dx:ASPxCheckBoxList>                                     
                                    </td>

                                    <td valign="top" style="width:13%"  align="left">
                                        <dx:ASPxCheckBoxList
                                            ID="chkCustomer" 
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Customer ID" Value="0" />
                                                <dx:ListEditItem Text="Account" Value="1" />
                                                <dx:ListEditItem Text="Customer Name" Value="2"/>
                                                <dx:ListEditItem Text="Address" Value="3" />
                                                <dx:ListEditItem Text="Postcode" Value="4" />
                                                <dx:ListEditItem Text="Telephone Number" Value="5" />
                                                <dx:ListEditItem Text="Email Address" Value="6"/>
                                                <dx:ListEditItem Text="Focus Account" Value="7" />
                                                <dx:ListEditItem Text="Business Type" Value="8" />
                                            </Items>

<Border BorderStyle="None"></Border>
                                           </dx:ASPxCheckBoxList>                                     
                                     </td>

                                    <td valign="top" style="width:13%"  align="left">
                                        <dx:ASPxCheckBoxList
                                            ID="chkLocation" 
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">  
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Drive Time" Value="0"/>
                                                <dx:ListEditItem Text="Distance" Value="1"/>
                                                <dx:ListEditItem Text="Sales Rep" Value="2"/>
                                                <dx:ListEditItem Text="Van Route" Value="3"/>
                                            </Items>

<Border BorderStyle="None"></Border>
                                           </dx:ASPxCheckBoxList>                                     
            
                                    </td>
                                    
                                    <td valign="top" style="width:13%"  align="left">
                                        <dx:ASPxCheckBoxList
                                            ID="chkProduct" 
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Service Description" Value="0"/> 
                                                <dx:ListEditItem Text="Trade Anaysis " Value="1" />
                                                <dx:ListEditItem Text="Description Code" Value="2" />
                                                <dx:ListEditItem Text="Part Number" Value="3"/>
                                
                                            </Items>

<Border BorderStyle="None"></Border>
                                           </dx:ASPxCheckBoxList>                                     
                                    
                                    </td>

                                    <td valign="top" style="width:13%"  align="left">
                                        <dx:ASPxCheckBoxList
                                            ID="chkInvoice" 
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Invoice Month" Value="0"/> 
                                                <dx:ListEditItem Text="Invoice Number" Value="1" />
                                                <dx:ListEditItem Text="Invoice Date" Value="2" />
                                            </Items>

<Border BorderStyle="None"></Border>
                                           </dx:ASPxCheckBoxList>                                     

                                    </td>
                                
                                    <td valign="top" style="width:13%"  align="left">
                                        <dx:ASPxCheckBoxList
                                            ID="chkUserDefined" 
                                            visible="false"
                                            runat="server" 
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            Width="100%">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="User Field 1" Value="23"/> 
                                                <dx:ListEditItem Text="User Field 2" Value="24" />
                                                <dx:ListEditItem Text="User Field 3" Value="25"/>
                                                <dx:ListEditItem Text="User Field 4" Value="26"/>
                                                <dx:ListEditItem Text="Notes" Value="27"/>
                                            </Items>

<Border BorderStyle="None"></Border>
                                           </dx:ASPxCheckBoxList>                                     
                                      </td>
               
                                    <td valign="top" style="width:12%"  align="left">
                                        <dx:ASPxCheckBoxList 
                                            ID="chkValues"                   
                                            RepeatLayout="Table" 
                                            RepeatColumns="1" 
                                            RepeatDirection="Vertical" 
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Quantity" Value="1" />
                                                <dx:ListEditItem Text="Sales Value" Value="2" />
                                                <dx:ListEditItem Text="Cost Value" Value="3" />
                                                <dx:ListEditItem Text="Margin" Value="4" />
                                                <dx:ListEditItem Text="Retail Value" Value="5" />
                                            </Items>

<Border BorderStyle="None"></Border>
                                       </dx:ASPxCheckBoxList>
                                     </td>   
                                       
                                    <td valign="top" style="width:10%"  align="left">
                                          
                                                    <dx:ASPxLabel
                                                        ID="lbl5a" 
                                                        runat="server"
                                                        text="From: ">
                                                    </dx:ASPxLabel>

                                                    <dx:ASPxComboBox 
                                                        ID="ddlFrom" 
                                                        runat="server" >
                                                    </dx:ASPxComboBox>
                                                    
                                                    <br />

                                                    <dx:ASPxLabel
                                                            id="lbl5"
                                                            runat="server"
                                                            text="To: ">
                                                    </dx:ASPxLabel>
                                                    
                                                    <br />
                                        
                                                    <dx:ASPxComboBox 
                                                        ID="ddlTo" 
                                                        runat="server" >
                                                    </dx:ASPxComboBox>

                                                    <br />
                                    
                                                   <dx:ASPxLabel 
                                                        ID="lblErrDates" 
                                                        runat="server" 
                                                        Text="" 
                                                        Visible="true" 
                                                        ForeColor="#FF3300">
                                                    </dx:ASPxLabel>
            
                                                    <br />

                                                    <dx:ASPxCheckBox
                                                        id="chkDateComparison"    
                                                        runat="server"
                                                        Text="Compare to Previous Year"
                                                        ToolTip="Show the same data but for 12 months earlier"
                                                        Visible="true" />


                                     </td>
                                 </tr>
                             </table>

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 2 - Product Categories and Groups
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Filter by Product or Customer Type">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server">
                          
                            <table id="tabProdFilters1" width="100%">
                                
                                <tr runat="server" id="tr6">
                                    <td valign="top" style="width: 25%">
                                        <dx:ASPxLabel 
                                            ID="lblErr3" 
                                            runat="server" 
                                            Text="" 
                                            Visible="true" 
                                            ForeColor="Red">
                                        </dx:ASPxLabel>
                                    </td>

                                    <td valign="top" style="width: 25%">
                                    </td>

                                    <td valign="top" style="width: 50%">
                                        <dx:ASPxLabel 
                                            ID="lblErr4" 
                                            runat="server" 
                                            Text="" Visible="true" 
                                            ForeColor="Red">
                                        </dx:ASPxLabel>
                                    </td>

                                    <td valign="top">
                                    </td>
                                </tr>
                                
                                <tr runat="server" id="tr2">

                                    <td valign="top" style="width: 25%">
                                                <dx:ASPxLabel
                                                    id="ASPxLabel2"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Service Description: ">
                                                </dx:ASPxLabel>
                                    </td>

                                    <td valign="top" style="width: 25%">
                                                <dx:ASPxLabel
                                                    id="ASPxLabel8"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Trade Analysis Group: ">
                                                </dx:ASPxLabel>
                                    </td>

                                    <td valign="top" style="width: 25%">
                                                <dx:ASPxLabel
                                                    id="ASPxLabel3"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Business Type: ">
                                                </dx:ASPxLabel>
                                    </td>

                                    <td valign="top" style="width: 25%">
                                                <dx:ASPxLabel
                                                    id="ASPxLabel4"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Customer Type: ">
                                                </dx:ASPxLabel>
                                    </td>

                                </tr>

                                <tr runat="server" id="tr3">

                                    <td valign="top" style="width: 25%">
                                        <dx:ASPxCheckBoxList 
                                            ID="chkProductGroups" 
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Paddings Padding="0px" />
                                       </dx:ASPxCheckBoxList>
                                    </td>

                                    <td valign="top" style="width: 33%">
                                        <dx:ASPxCheckBoxList
                                            ID="chkTradeAnalysis" 
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Paddings Padding="0px" />
                                            <Items>
                                                <dx:ListEditItem Text="Service" Value="Service" />
                                                <dx:ListEditItem Text="Steering/Suspension" Value="Steering/Suspension"/>
                                                <dx:ListEditItem Text="Brake" Value="Brake"/>
                                                <dx:ListEditItem Text="Clutch/Drivetrain" Value="Clutch/Drivetrain"/>
                                                <dx:ListEditItem Text="Exhaust" Value="Exhaust"/>
                                                <dx:ListEditItem Text="Electrical" Value="Electrical"/>
                                                <dx:ListEditItem Text="Heating/Cooling" Value="Heating/Cooling"/>
                                                <dx:ListEditItem Text="Consumables" Value="Consumables"/>
                                                <dx:ListEditItem Text="Main Panels" Value="Main Panels"/>
                                                <dx:ListEditItem Text="Mirrors and Glass" Value="Mirrors and Glass"/>
                                                <dx:ListEditItem Text="Bumpers and Fixings" Value="Bumpers and Fixings"/>
                                                <dx:ListEditItem Text="Lighting" Value="Lighting"/>
                                                <dx:ListEditItem Text="Grilles and Mouldings" Value="Grilles and Mouldings"/>
                                                <dx:ListEditItem Text="Other" Value="Other" />
                                            </Items>
                                       </dx:ASPxCheckBoxList>
                                    </td>

                                    <td valign="top" style="width: 33%">
                                        <dx:ASPxCheckBoxList
                                            ID="chkBusinessTypes" 
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Paddings Padding="0px" />
                                       </dx:ASPxCheckBoxList>
                                    </td>

                                    <td valign="top" style="width: 33%">
                                        <dx:ASPxComboBox ID="ddlType" runat="server" AutoPostBack="True">
                                            <Items>
                                                <dx:ListEditItem  Text="All Customers" Value="0"/> 
                                                <dx:ListEditItem Text="Dealer Focus A/Cs" Value="1"/> 
                                                <dx:ListEditItem Text="BGC Focus A/Cs" Value="2" /> 
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            
                            </table>
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 3 - Trade Analysis
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text ="Filter By Trade Analysis" Visible="false">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl6" runat="server">
                        
                            <table width ="100%">
                                <tr valign="top" >
                                        <td width ="86%" align="left" valign="top">
                                                <dx:ASPxLabel
                                                    id="ASPxLabel1"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Select The Trade Analysis Items To Report On: ">
                                                </dx:ASPxLabel>
                                        </td>

                                        <td width ="14%" align="left" valign="top" >
                                                <dx:ASPxLabel
                                                    id="ASPxLabel7"
                                                    runat="server"
                                                    Font-Bold="true"
                                                    text="Selected Items">
                                                </dx:ASPxLabel>
                                        </td>
                                </tr>
                             </table>

                             <table width ="100%">
                                 <tr valign="top">

                                     <td width ="86%">
                                        <table width ="100%">

                                         <tr valign="top" >
                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkService" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Service">
                                        </dx:ASPxCheckBox>
                                    </td>

                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkSteering" 
                                            runat="server" 
                                            AutoPostBack="true"
                                            Font-Bold="true"
                                            Text="Steering/Suspension">
                                        </dx:ASPxCheckBox>
                                    </td>

                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkBrake" 
                                            runat="server" 
                                            AutoPostBack="true"
                                            Font-Bold="true"
                                            Text="Brake">
                                        </dx:ASPxCheckBox>
                                    </td>

                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkClutch" 
                                            runat="server" 
                                            AutoPostBack="true"
                                            Font-Bold="true"
                                            Text="Clutch/DriveTrain">
                                        </dx:ASPxCheckBox>
                                    </td>
                                        
                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkExhaust" 
                                            runat="server" 
                                            AutoPostBack="true"
                                            Font-Bold="true"
                                            Text="Exhaust">
                                        </dx:ASPxCheckBox>
                                    </td>
                                                    
                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkElectrical" 
                                            runat="server" 
                                            AutoPostBack="true"
                                            Font-Bold="true"
                                            Text="Electrical">
                                        </dx:ASPxCheckBox>
                                    </td>
                                                    
                                            <td align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkHeating" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Heating/Cooling">
                                        </dx:ASPxCheckBox>
                                    </td>
                                          </tr>
                                                
                                          <tr valign="top" >

                                               <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkServiceList" 
                                            Border-BorderStyle="None"
                                            AutoPostBack="true"
                                            RepeatColumns="1"
                                            RepeatDirection="Horizontal"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Air Filters" Value ="0" />
                                                <dx:ListEditItem Text="Engine Oil"  Value ="1"/>
                                                <dx:ListEditItem Text="Fuel Filters" Value ="2"/>
                                                <dx:ListEditItem Text="Genuine Coolant" Value ="3"/>
                                                <dx:ListEditItem Text="Oil Filters" Value ="4"/>
                                                <dx:ListEditItem Text="Pollen Filters" Value ="5"/>
                                                <dx:ListEditItem Text="Spark Plugs" Value ="6"/>
                                                <dx:ListEditItem Text="Wiper Blades" Value ="7"/>
                                            </Items>
                                             <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                               <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkSteeringList" 
                                            RepeatColumns="1"
                                            AutoPostBack="true"
                                            RepeatDirection="Horizontal"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Ball Joints" Value ="8"/>
                                                <dx:ListEditItem Text="Coil & Leaf Springs" Value ="9"/>
                                                <dx:ListEditItem Text="Driveshafts & CV Joints" Value ="10"/>
                                                <dx:ListEditItem Text="Shock Absorbers" Value ="11"/>
                                                <dx:ListEditItem Text="Steering Racks" Value ="12"/>
                                                <dx:ListEditItem Text="Suspension Arms" Value ="13"/>
                                                <dx:ListEditItem Text="Wheel Bearings" Value ="14"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                               <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkBrakeList" 
                                            RepeatColumns="1"
                                            AutoPostBack="true"
                                            RepeatDirection="Horizontal"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Brake Calipers" Value ="15"/>
                                                <dx:ListEditItem Text="Brake Discs" Value ="16" />
                                                <dx:ListEditItem Text="Brake Others" Value ="17" />
                                                <dx:ListEditItem Text="Brake Pads" Value ="18" />
                                                <dx:ListEditItem Text="Brake Shoes" Value ="19" />
                                                <dx:ListEditItem Text="Wheel Cylinders"  Value ="20"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                               <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkClutchList" 
                                            RepeatColumns="1"
                                            AutoPostBack="true"
                                            RepeatDirection="Horizontal"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Clutch Covers"  Value ="21" />
                                                <dx:ListEditItem Text="Clutch Discs"   Value ="22"/>
                                                <dx:ListEditItem Text="Clutch Kits"  Value ="23" />
                                                <dx:ListEditItem Text="Clutch Release Bearings"   Value ="24"/>
                                                <dx:ListEditItem Text="Flywheels"   Value ="25"/>
                                                <dx:ListEditItem Text="TimingBelts"   Value ="26"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                               <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            id="chkExhaustList" 
                                            AutoPostBack="true"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Catalysts" Value ="27" />
                                                <dx:ListEditItem Text="Centre Pipes" Value ="28"/>
                                                <dx:ListEditItem Text="Front Pipes" Value ="29"/>
                                                <dx:ListEditItem Text="Rear Silencers" Value ="30" />
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>
                                        
                                               <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            id="chkElectricalList" 
                                            AutoPostBack="true"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Alternators" Value ="31"/>
                                                <dx:ListEditItem Text="Batteries" Value ="32"/>
                                                <dx:ListEditItem Text="Oxygen Sensors" Value ="33"/>
                                                <dx:ListEditItem Text="Starter Motors" Value ="34"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>
                                                    
                                               <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            id="chkHeatingList" 
                                            AutoPostBack="true"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Air Con Parts" Value ="35"/>
                                                <dx:ListEditItem Text="Radiators" Value ="36"/>
                                                <dx:ListEditItem Text="Water Pumps" Value ="37"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                     </td>
                                         </tr>
                                        
                                         <tr valign="top" >
                                               <td>


                                               </td>  
                                         </tr>

                                         <tr valign="top" >

                                            <td align="left" 
                                                valign="top">
                                            </td>

                                            <td align="left" valign="top">
                                                <dx:ASPxCheckBox 
                                            ID="chkLighting" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Lighting">
                                        </dx:ASPxCheckBox>
                                           </td>
                                            
                                            <td align="left" valign="top">
                                               <dx:ASPxCheckBox 
                                            ID="chkMirrors" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Mirrors and Glass">
                                        </dx:ASPxCheckBox>
                                            </td>
                                            
                                            <td align="left" valign="top">
                                                <dx:ASPxCheckBox 
                                                    ID="chkBumpers" 
                                                    runat="server" 
                                                    Font-Bold="true"
                                                    AutoPostBack="true"
                                                    Text="Bumpers & Fixings">
                                               </dx:ASPxCheckBox>
                                            </td>

                                            <td align="left" valign="top">
                                                <dx:ASPxCheckBox 
                                            ID="chkMainPanels" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Main Panels">
                                        </dx:ASPxCheckBox>
                                            </td>

                                            <td align="left" valign="top">
                                                <dx:ASPxCheckBox 
                                            ID="chkGrilles" 
                                            runat="server"
                                            Font-Bold="true" 
                                            AutoPostBack="true"
                                            Text="Grills/Mouldings">
                                        </dx:ASPxCheckBox>
                                            </td>

                                            <td align="left" valign="top">
                                                <dx:ASPxCheckBox 
                                            ID="chkOther" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Other">
                                        </dx:ASPxCheckBox>
                                          </td>

                                       </tr>
     
                                       <tr valign="top" >
                                             <td  align="left" valign="top">
                                        <dx:ASPxCheckBox 
                                            ID="chkConsumables" 
                                            runat="server" 
                                            Font-Bold="true"
                                            AutoPostBack="true"
                                            Text="Consumables">
                                        </dx:ASPxCheckBox>
                                    </td>

                                             <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkLightingList" 
                                            AutoPostBack="true"
                                            RepeatColumns="1"
                                            RepeatDirection="Horizontal"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Foglamps" Value ="38" />
                                                <dx:ListEditItem Text="Headlamps"  Value ="39"/>
                                                <dx:ListEditItem Text="Minor Lamps" Value ="40"/>
                                                <dx:ListEditItem Text="Rear Lamps" Value ="41"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                             <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkMirrorsList" 
                                            AutoPostBack="true"
                                            RepeatColumns="1"
                                            Border-BorderStyle="None"
                                            RepeatDirection="Horizontal"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Glass" Value ="42"/>
                                                <dx:ListEditItem Text="Mirrors" Value ="43" />
                                                <dx:ListEditItem Text="Windscreens" Value ="44" />
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                             <td align="left" valign="top">
                                        <dx:ASPxCheckBoxList
                                            id="chkBumpersList" 
                                            AutoPostBack="true"
                                            RepeatColumns="1"
                                            Border-BorderStyle="None"
                                            RepeatDirection="Horizontal"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Bumper Parts"  Value ="45" />
                                                <dx:ListEditItem Text="Front Bumpers"   Value ="46"/>
                                                <dx:ListEditItem Text="Rear Bumpers"  Value ="47" />
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>

                                             <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            id="chkMainPanelsList" 
                                            AutoPostBack="true"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Bonnets" Value ="48"/>
                                                <dx:ListEditItem Text="Cross Members" Value ="49"/>
                                                <dx:ListEditItem Text="Door Skins" Value ="50"/>
                                                <dx:ListEditItem Text="Doors" Value ="51"/>
                                                <dx:ListEditItem Text="Quarter Panels" Value ="52"/>
                                                <dx:ListEditItem Text="Radiator Support Panels" Value ="53"/>
                                                <dx:ListEditItem Text="Tailgates & Trunk Lids" Value ="54"/>
                                                <dx:ListEditItem Text="Wings" Value ="55"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>
                                        
                                             <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            AutoPostBack="true"
                                            id="chkGrillesList" 
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Mouldings & Badges" Value ="56" />
                                                <dx:ListEditItem Text="Radiator Grilles" Value ="57"/>
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                    </td>
                                        
                                             <td align="left" valign="top">
                                            <dx:ASPxCheckBoxList
                                            id="chkOtherList" 
                                            AutoPostBack="true"
                                            Border-BorderStyle="None"
                                            runat="server">
                                            <Items>
                                                <dx:ListEditItem Text="Accessories" Value ="58" />
                                                <dx:ListEditItem Text="Air Bag Parts" Value ="59" />
                                                <dx:ListEditItem Text="Interior Parts" Value ="60" />
                                                <dx:ListEditItem Text="Misc Body Parts" Value ="61" />
                                                <dx:ListEditItem Text="Misc Fixings" Value ="62" />
                                                <dx:ListEditItem Text="Misc Panels" Value ="63" />
                                                <dx:ListEditItem Text="Wheels" Value ="64" />
                                            </Items>
                                            <Paddings Padding="0px" />
                                        </dx:ASPxCheckBoxList>
                                     </td>
                                        </tr>

                                        </table>
                                    </td>   

                                    <td width ="14%">
                                         <dx:ASPxMemo 
                                                        ID = "memTAItems"
                                                        Height="348"
                                                        Width="100%"
                                                        ReadOnly="true"
                                                        runat="server" ClientInstanceName="memTAItems">
                                         </dx:ASPxMemo>
                                    </td>   
                                </tr>
                             </table>
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 4 - Product Groups
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Filter By Description Code">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl3" runat="server">
                        
                            <table width="100%">
                                <tr style="height:25px">
                                    <td style="width:45%">
                                        <table width="100%">
                                            <tr>
                                                <td valign="top" align="left" style="width:50%">
                                              
                                                    <dx:ASPxTextBox 
                                                        ID="txtPFC" 
                                                        runat="server" 
                                                        MaxLength="30" 
                                                        Width="100%" 
                                                        Height="18px">
                                                    </dx:ASPxTextBox>
                                                  
                                                    <br />
                                                    
                                                    <dx:ASPxLabel 
                                                        ID="lblPFCErr" 
                                                        runat="server" 
                                                        Text="" 
                                                        Visible="true" >
                                                    </dx:ASPxLabel>
                                                
                                                </td>
                                                
                                                
                                                <td valign="top" align="left" style="width:35%"> 

                                                    <dx:ASPxButton 
                                                        ID="btnAddPFC" 
                                                        runat="server" 
                                                        Text="Add Description Code" 
                                                        Width="100%">
                                                    </dx:ASPxButton>

                                                </td>

                                                <td valign="top" align="left" style="width:15%">
                                                    
                                                    <dx:ASPxButton 
                                                        ID="btnSrchPFC" 
                                                        runat="server" 
                                                        Text="..."  
                                                        Width="100%">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>     
                                    </td>

                                    <td width="5%"></td>

                                    <td valign="top" style="width:50%">
                                               <dx:ASPxLabel
                                                    id="ASPxLabel5"
                                                    runat="server"
                                                    text="These Description Codes will be included in your report:">
                                                </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr style="height:25px">
                                    <td>
                                        <asp:ListBox 
                                            ID="lstSrchPFCs" 
                                            Font-Names="Calibri"
                                            runat="server" 
                                            Width="100%" 
                                            Height="250px" 
                                            SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td></td>
                                    <td>
                                        <asp:ListBox 
                                            ID="lstPFCs" 
                                            Font-Names="Calibri"
                                            runat="server" 
                                            Width="100%" 
                                            Height="250px" 
                                            SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                                <tr style="height:25px">
                                    <td valign="top">
                                        <dx:ASPxButton ID="btnAddPFCMulti" runat="server" Text="Add selected Description Codes" Width="200px">
                                        </dx:ASPxButton>
                                    </td>
                                    <td></td>
                                    <td valign="top">
                                        <dx:ASPxButton ID="btnRemovePFC" runat="server" Text="Remove selected Description Codes" Width="200px">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>  

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 5 - Parts
                ------------------------------------------------------------------------------------------%>                
                <dx:TabPage Text="Filter By Part Number">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl5" runat="server">

                            <table width="100%">
                                <tr style="height:25px">
                                    <td style="width:45%">
                                        <table width="100%">
                                            <tr>
                                                <td valign="top" align="left" style="width:50%">
                                                    <dx:ASPxTextBox ID="txtPart" runat="server" Width="100%" MaxLength="30" Height="18px">
                                                    </dx:ASPxTextBox>
                                                    <br />
                                                    <dx:ASPxLabel ID="lblPartErr" runat="server" Text="" Visible="true" Font-Size="Medium" ForeColor="#FF3300">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td valign="top" align="left" style="width:35%"> 
                                                    <dx:ASPxButton ID="btnAddPart" runat="server" Text="Add Part" Width="100%" 
                                                         style="text-align: right">
                                                    </dx:ASPxButton>
                                                </td>
                                                <td valign="top" align="left" style="width:15%">
                                                    <dx:ASPxButton ID="btnSrchPart" runat="server" Text="..." Width="100%" >
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>     
                                    </td>
                                    <td style="width:5%">
                                    </td>
                                    <td valign="top" align="left" style="width:50%">
                                             <dx:ASPxLabel
                                                    id="ASPxLabel6"
                                                    runat="server"
                                                    text="These Parts will be included in your report (Max 50):">
                                                </dx:ASPxLabel>
                                        
                                                <dx:ASPxLabel ID="lblPartCount" runat="server" Text ="Count:0">
                                                </dx:ASPxLabel>
                                        <br />
                                        
                                        <dx:ASPxLabel ID="lblTooManyParts" runat="server" Font-Size="Medium" 
                                            ForeColor="#FF3300">
                                        </dx:ASPxLabel>
                                    </td>
                                </tr>
                                <tr style="height:25px">
                                    <td>
                                        <asp:ListBox ID="lstSrchParts" runat="server" Width="100%" Height="250px" 
                                            SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:ListBox ID="lstParts" runat="server" Width="100%" Height="250px" 
                                            SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                </tr>
                                <tr style="height:25px">
                                    <td valign="top">
                                        <dx:ASPxButton ID="btnAddPartMulti" runat="server" Text="Add selected Parts" Width="200px" >                                       </dx:ASPxButton>
                                    </td>
                                    <td>
                                    </td>
                                    <td valign="top">
                                        <dx:ASPxButton ID="btnRemovePart" runat="server" Text="Remove Selected Parts" Width="200px">
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>


                <%------------------------------------------------------------------------------------------
                ' TAB 6 - Results
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Results">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl2" runat="server">
                            <table id="tabSaveDef" runat="server" visible="true" style="width:100%">
                                <tr>
                                    <td style="width:15%" valign="middle">
                                        <dx:ASPxButton ID="btnSaveDef" runat="server" Text="Save Definition" Width="200px" 
                                            >
                                        </dx:ASPxButton>
                                    </td>
                                    <td style="width:2%" valign="middle">
                                        <dx:ASPxLabel ID="lblQName" runat="server" Text="Name" >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td valign="middle">
                                        <dx:ASPxTextBox ID="txtDefName" runat="server" Width="350px">
                                        </dx:ASPxTextBox>
                                    </td>
                                    <td  style="width:15%" valign="middle">
                                        <dx:ASPxComboBox
                                            ID="ddlDefType" 
                                            runat="server" 
                                            Visible="false"  
                                            Width="180px">
                                            <Items>
                                                <dx:ListEditItem Text ="Private" Value ="Private" />
                                            </Items>
                                        </dx:ASPxComboBox>

<%--                                        <asp:DropDownList ID="ddlDefType" runat="server" Visible="false"  
                                            Width="180px"   >
                                            <asp:ListItem>Private</asp:ListItem>
                                        </asp:DropDownList>--%>
                                    </td>

                                    <td style="width:10%" align="right" valign="middle">
                                        <dx:ASPxButton 
                                            ID="btnQSave" 
                                            runat="server" 
                                            Text="Save" 
                                            Width="100px" >
                                        </dx:ASPxButton>
                                    </td>

                                    <td style="width:10%" align="left" valign="middle">
                                        <dx:ASPxButton 
                                            ID="btnQCancel" 
                                            runat="server" 
                                            Text="Cancel" 
                                            Width="100px" >
                                        </dx:ASPxButton>
                                    </td>

                                </tr>
                            </table>
                            <dx:ASPxLabel ID="lblQSave" runat="server" Text=""
                                Font-Size="Medium" ForeColor="#FF3300">
                            </dx:ASPxLabel>
                    
                            <dxpg:ASPxPivotGrid ID="ASPXPivotResults" 
                                OnCustomCellDisplayText="ASPXPivotResults_CustomCellDisplayText" 
                                runat="server" 
                                Visible="false" 
                                onload="PivotGridStyles"
                                DataSourceID="dsResults"
                                FieldValueCollapsedImage-Height="12px" 
                                FieldValueCollapsedImage-Width="11px" 
                                FieldValueExpandedImage-Height="12px" 
                                FieldValueExpandedImage-Width="11px" 
                                HeaderSortDownImage-Height="8px" 
                                HeaderSortDownImage-Width="7px" 
                                HeaderSortUpImage-Height="8px" 
                                HeaderSortUpImage-Width="7px"
                                Theme="Office2010Silver">
                                <OptionsPager AllButton-Visible="true" RowsPerPage="25" >
                                    <AllButton Visible="True"></AllButton>
                                </OptionsPager>
                                <OptionsView 
                                    ShowColumnGrandTotalHeader="False" 
                                    ShowColumnGrandTotals="False" 
                                    ShowColumnTotals="False" ShowRowGrandTotalHeader="False" 
                                    ShowRowGrandTotals="False" ShowRowTotals="False" />
                                <Images>
                                    <FieldValueCollapsed Height="12px" 
                                        Width="11px" />
                                    <FieldValueExpanded Height="12px" 
                                        Width="11px" />
                                    <HeaderSortDown Height="8px" 
                                        Width="7px" />
                                    <HeaderSortUp Height="8px" 
                                        Width="7px" />
                                    <SortByColumn Height="7px" 
                                        Width="11px" />
                                </Images>
                                <Styles>
                                    <CustomizationFieldsHeaderStyle>
                                        <Paddings PaddingLeft="12px" PaddingRight="6px" />
                                    </CustomizationFieldsHeaderStyle>
                                </Styles>

     <%--                           <StylesPrint 
                                    Cell-BackColor2="" 
                                    Cell-GradientMode="Horizontal" 
                                    FieldHeader-BackColor2="" 
                                    FieldHeader-GradientMode="Horizontal" 
                                    TotalCell-BackColor2="" 
                                    TotalCell-GradientMode="Horizontal" 
                                    GrandTotalCell-BackColor2="" 
                                    GrandTotalCell-GradientMode="Horizontal" 
                                    CustomTotalCell-BackColor2="" 
                                    CustomTotalCell-GradientMode="Horizontal" 
                                    FieldValue-BackColor2="" 
                                    FieldValue-GradientMode="Horizontal" 
                                    FieldValueTotal-BackColor2="" 
                                    FieldValueTotal-GradientMode="Horizontal" 
                                    FieldValueGrandTotal-BackColor2="" 
                                    FieldValueGrandTotal-GradientMode="Horizontal" 
                                    Lines-BackColor2="" 
                                    Lines-GradientMode="Horizontal">
                                </StylesPrint>--%>

                            </dxpg:ASPxPivotGrid>
                           
                            <br />

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

            </TabPages>

        </dx:ASPxPageControl>
   
        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsResults" runat="server" SelectCommand="p_AdvQuery_2017" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="sFieldList" SessionField="SelectFieldList" Type="String" Size="30" />
            <asp:SessionParameter DefaultValue="" Name="sProductCategories" SessionField="TradeAnalysis" Type="String" Size="5000" />
            <asp:SessionParameter DefaultValue="" Name="sProductGroups" SessionField="ProductGroupList" Type="String" Size="2000" />
            <asp:SessionParameter DefaultValue="" Name="sBusinessTypes" SessionField="BusinessTypeList" Type="String" Size="500" />
            <asp:SessionParameter DefaultValue="" Name="nBlueGrass" SessionField="BlueGrassID" Type="Int32" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="sPFCs" SessionField="PFCList" Type="String" Size="2000" />
            <asp:SessionParameter DefaultValue="" Name="sParts" SessionField="PartList" Type="String" Size="2000" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nDateCompare" SessionField="DateCompare" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource 
        ID="dsTradeAnalysis" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_NissanTradeAnalysisTree" 
        SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

    <dxpg:ASPxPivotGridExporter ID="ASPxPivotGridExporter1"  ASPxPivotGridID="ASPXPivotResults" runat="server" 
        OptionsPrint-MergeColumnFieldValues="False" OptionsPrint-MergeRowFieldValues="False">
            <OptionsPrint MergeColumnFieldValues="False" MergeRowFieldValues="False" 
            PrintColumnHeaders="False" PrintDataHeaders="False" PrintFilterHeaders="False" 
            PrintHorzLines="False" PrintRowHeaders="True" PrintUnusedFilterFields="False" 
            PrintVertLines="False" VerticalContentSplitting="Exact"></OptionsPrint>
    </dxpg:ASPxPivotGridExporter>

  <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="callback">
        <ClientSideEvents CallbackComplete="function(s, e) {memPostCodeList.SetText(s.cppostcodelist)}" />
    </dx:ASPxCallback>

    
</asp:Content>