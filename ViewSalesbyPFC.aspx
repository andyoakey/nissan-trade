<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewSalesbyPFC.aspx.vb" Inherits="ViewSalesbyPFC" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>





<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

  <div style="position: relative; font-family: Calibri; text-align: left;" >
        
       <div id="divTitle" style="position:relative; top:0px">
            <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Product Analysis" />
       </div>

       <br />

        <table id="trSelectionStuff" Width="100%" style="position:relative; margin-bottom: 10px;">
            <tr id="Tr2" runat="server" >
                <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblBy"
                            runat="server" 
                            Text="Summarise Data By">
                    </dx:ASPxLabel>
                </td>
    
                <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblFrom"
                            runat="server" 
                            Text="Month From">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="14%" >
                     <dx:ASPxLabel 
                            id="lblTo"
                            runat="server" 
                            Text="Month To">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="58%" >
                     <dx:ASPxLabel 
                            id="lblCustomerType"
                            runat="server" 
                            Text="Customer Type">
                    </dx:ASPxLabel>
                </td>

                </tr>

                <tr id="Tr1" runat="server" >

                    <td align="left" width="14%" valign="top">
                          <dx:ASPxComboBox
                        ID="ddlTA" 
                        runat="server" 
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text ="Service Type" Value ="1"  Selected="true"/>
                            <dx:ListEditItem Text ="Trade Analysis" Value ="2" />
                        </Items>
                    </dx:ASPxComboBox>
                    </td>

                    <td align="left" width="14%" valign="top" >
                         <dx:ASPxComboBox
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                     </td>

                    <td align="left" width="14%" valign="top">
                         <dx:ASPxComboBox
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                    </td>

                    <td align="left" width="58%" valign="top">
                         <dx:ASPxComboBox
                            runat="server" 
                            ID="ddlType"
                            AutoPostBack="True">
                            <Items>
                                <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/>
                                <dx:ListEditItem Text="Focus only" Value="1" />
                                <dx:ListEditItem Text="Excluding Focus" Value="2"/>
                             </Items>
                        </dx:ASPxComboBox>         
                    </td>
             </tr>
        </table>
        
        <dx:ASPxGridView 
            ID="gridSales" 
            runat="server" 
            onload="gridStyles"
            cssclass="grid_styles"
            AutoGenerateColumns="False" 
            DataSourceID="dsViewSales" 
            KeyFieldName="code" 
            Width="100%"> 

            <SettingsDetail  ShowDetailRow="true"  ShowDetailButtons="true" />
            
            <Settings 
                ShowHeaderFilterBlankItems="false"
                ShowFilterRow="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior 
                AllowSort="true" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <SettingsPager  Mode="ShowPager" PageSize="16" >
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign ="Center" />

            <Columns>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="0"
                    FieldName="code"
                    caption="Code"
                    Name="code"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="13%" >
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn 
                    VisibleIndex="1"
                    Caption="Description" 
                    FieldName="description" 
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    Settings-AllowAutoFilter="False"
                    Width="15%" 
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="2" 
                    Caption="Units Sold" 
                    FieldName="quantity" 
                    Name="quantity"
                    ToolTip="The total number of units sold by this Dealer for this month"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="#,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn 
                    VisibleIndex="3" 
                    FieldName="retailvalue" 
                    Name="retailvalue"
                    Caption="Retail Value" 
                    ToolTip="The total retail value of all the sales in this month by this Dealer of this item"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn 
                    VisibleIndex="4" 
                    FieldName="salevalue" 
                    Name="salevalue"
                    Caption="Sales Value" 
                    ToolTip="The total invoice value of all the sales in this month by this Dealer of this item"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="5" 
                    FieldName="costvalue" 
                    Name="costvalue"
                    Caption="Cost Value" 
                    ToolTip="The cost of purchases in this month by this Dealer of this item"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="6" 
                    FieldName="profit" 
                    Name="profit"
                    Caption="Profit" 
                    ToolTip="The Profit (or loss) made by this Dealer for this item in this period, calculated by subtracting Cost Value from Sales Value"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="7" 
                    FieldName="margin" 
                    Name="margin"
                    Caption="Margin" 
                    ToolTip="The total percentage margin made on sales of this item in this period"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="0.00%"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum"  />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="retailvalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="salevalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costvalue" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="profit" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="margin" ShowInColumn="margin" SummaryType="Custom" />
            </TotalSummary>
            
              <Templates>
            
                <DetailRow>

                      <br />


                      <dx:ASPxGridView 
                          ID="gridDrillDown" 
                          runat="server"
                          onload="gridStyles"
                          cssclass="grid_styles"
                          DataSourceID="dsViewSalesLevel1" 
                          KeyFieldName="code"                        	    
                          OnBeforePerformDataSelect="gridDrillDown_BeforePerformDataSelect"
                          OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
                          OnCustomSummaryCalculate="gridSales_CustomSummaryCalculate"
                          Width="100%" 
                          AutoGenerateColumns="False">

                          <Columns>
                              <dx:GridViewDataTextColumn 
                                  FieldName="code" 
                                  Name="code" 
                                  Caption="Code"
                                  VisibleIndex="0" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  Width="15%" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>
                              
                              <dx:GridViewDataTextColumn 
                                  FieldName="description" 
                                  Name="description" 
                                  Caption="Description"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="1" 
                                  Width="30%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>
                              
                              <dx:GridViewDataTextColumn 
                                  FieldName="quantity" 
                                  Name="quantity" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Units Sold"
                                  PropertiesTextEdit-DisplayFormatString="##,##0"
                                  VisibleIndex="2" 
                                  ExportWidth="150">
                                 </dx:GridViewDataTextColumn>
                              
                              <dx:GridViewDataTextColumn 
                                  FieldName="retailvalue" 
                                  Name="retailvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Retail Value"
                                  VisibleIndex="3" 
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn 
                                  FieldName="salevalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Name="salevalue" 
                                  Caption="Sale Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  VisibleIndex="4" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>
               
                              <dx:GridViewDataTextColumn 
                                  FieldName="costvalue" 
                                  Name="costvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Cost Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  VisibleIndex="5" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn 
                    VisibleIndex="6" 
                    FieldName="profit" 
                    Name="profit"
                    Caption="Profit" 
                    ToolTip="The Profit (or loss) made by this Dealer for this item in this period, calculated by subtracting Cost Value from Sales Value"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn 
                    VisibleIndex="7" 
                    FieldName="margin" 
                    Name="margin"
                    Caption="Margin" 
                    ToolTip="The total percentage margin made on sales of this item in this period"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="0.00%"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>


                    
                          </Columns>

                          <Settings 
                            ShowFooter="true"
                            ShowFilterRow="False" 
                            ShowFilterRowMenu="False" 
                            ShowGroupedColumns="False"
                            ShowGroupFooter="VisibleIfExpanded" 
                            ShowGroupPanel="False" 
                            ShowHeaderFilterButton="True"
                            ShowStatusBar="Hidden" 
                            ShowTitlePanel="False" />
                        
                          <SettingsBehavior 
                              AllowSort="true" 
                              AutoFilterRowInputDelay="12000" 
                              ColumnResizeMode="Control" />
            
                          <SettingsPager  Mode="ShowPager" PageSize="12" >
                                <AllButton Visible="True">
                                </AllButton>
                          </SettingsPager>
  
                          <SettingsDetail 
                            ShowDetailRow="True" 
                            ExportMode="Expanded" 
                            IsDetailGrid="True" />

                          <Styles Footer-HorizontalAlign ="Center" />

                           <TotalSummary>
                                <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum"  />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="retailvalue" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="salevalue" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costvalue" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="profit" SummaryType="Sum" />
                                <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="margin" ShowInColumn="margin" SummaryType="Custom" />
                            </TotalSummary>
        


                <Templates>
                        
                            <DetailRow>
                            
                                  <br />

                                  <dx:ASPxGridView 
                                      ID="gridDrillDown2" 
                                      runat="server" 
                                      AutoGenerateColumns="False"
                                      DataSourceID="dsViewSalesLevel2" 
                                      KeyFieldName="partnumber"
                                      OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
                                      OnBeforePerformDataSelect="gridDrillDown2_BeforePerformDataSelect" 
                                      OnCustomSummaryCalculate="gridSales_CustomSummaryCalculate"
                                      Width="100%">
        
                                      <Settings 
                            ShowFooter="true"
                            ShowFilterRow="False" 
                            ShowFilterRowMenu="False" 
                            ShowGroupedColumns="False"
                            ShowGroupFooter="VisibleIfExpanded" 
                            ShowGroupPanel="False" 
                            ShowHeaderFilterButton="True"
                            ShowStatusBar="Hidden" 
                            ShowTitlePanel="False" />
                        
                                      <SettingsBehavior 
                              AllowSort="true" 
                              AutoFilterRowInputDelay="12000" 
                              ColumnResizeMode="Control" />
            
                                      <SettingsPager  Mode="ShowPager" PageSize="12" >
                                <AllButton Visible="True">
                                </AllButton>
                          </SettingsPager>
  
                                      <SettingsDetail 
                            ShowDetailRow="True" 
                            ExportMode="Expanded" 
                            IsDetailGrid="True" />
                                      
                                                            
                                    <Columns>
                                    
                                        <dx:GridViewDataTextColumn 
                                            Caption="Part Number" 
                                            FieldName="partnumber" 
                                            ToolTip=""
                                            CellStyle-HorizontalAlign="Center"
                                            HeaderStyle-HorizontalAlign="Center"
                                            Settings-HeaderFilterMode="CheckedList"
                                            Settings-AllowHeaderFilter="True"
                                            VisibleIndex="1" 
                                            Width="15%" 
                                            ExportWidth="150">
                                        </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                  FieldName="description" 
                                  Name="description" 
                                  Caption="Description"
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-HeaderFilterMode="CheckedList"
                                  Settings-AllowHeaderFilter="True"
                                  VisibleIndex="1" 
                                  Width="30%" 
                                  ExportWidth="300">
                              </dx:GridViewDataTextColumn>
                              
                                        <dx:GridViewDataTextColumn 
                                  FieldName="quantity" 
                                  Name="quantity" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Units Sold"
                                  PropertiesTextEdit-DisplayFormatString="##,##0"
                                  VisibleIndex="2" 
                                  ExportWidth="150">
                                 </dx:GridViewDataTextColumn>
                            
                                        <dx:GridViewDataTextColumn 
                                  FieldName="retailvalue" 
                                  Name="retailvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Retail Value"
                                  VisibleIndex="3" 
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                                  FieldName="salevalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Name="salevalue" 
                                  Caption="Sale Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  VisibleIndex="4" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>
               
                                        <dx:GridViewDataTextColumn 
                                  FieldName="costvalue" 
                                  Name="costvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Cost Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                                  VisibleIndex="5" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                    VisibleIndex="6" 
                    FieldName="profit" 
                    Name="profit"
                    Caption="Profit" 
                    ToolTip="The Profit (or loss) made by this Dealer for this item in this period, calculated by subtracting Cost Value from Sales Value"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="&#163;##,##0"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                                        <dx:GridViewDataTextColumn 
                    VisibleIndex="7" 
                    FieldName="margin" 
                    Name="margin"
                    Caption="Margin" 
                    ToolTip="The total percentage margin made on sales of this item in this period"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    PropertiesTextEdit-DisplayFormatString="0.00%"
                    Settings-AllowHeaderFilter="False"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowAutoFilter="False"
                    Width="13%"
                    ExportWidth="150">
                </dx:GridViewDataTextColumn>

                                    </Columns>
                                    
                                    <TotalSummary>
                                        <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum"  />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="retailvalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="salevalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costvalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="profit" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="margin" ShowInColumn="margin" SummaryType="Custom" />
                                    </TotalSummary>
        
                                     <Styles Footer-HorizontalAlign ="Center" />
                                        

                                    <Templates>
                                    
                                        <DetailRow>

                                              <br />
                                            
                                              <dx:ASPxGridView 
                                                  ID="gridDrillDown3" 
                                                  runat="server" 
                                                  AutoGenerateColumns="False"
                                                  DataSourceID="dsViewSalesLevel3"
                                                  OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" 
                                                  OnBeforePerformDataSelect="gridDrillDown3_BeforePerformDataSelect" 
                                                  OnCustomSummaryCalculate="gridSales_CustomSummaryCalculate"
                                                  Width  ="100%">

                                                  <Styles Footer-HorizontalAlign ="Center" />
                
                                                  <Settings 
                            ShowFooter="true"
                            ShowFilterRow="False" 
                            ShowFilterRowMenu="False" 
                            ShowGroupedColumns="False"
                            ShowGroupFooter="VisibleIfExpanded" 
                            ShowGroupPanel="False" 
                            ShowHeaderFilterButton="True"
                            ShowStatusBar="Hidden" 
                            ShowTitlePanel="False" />
                        
                                                  <SettingsBehavior 
                              AllowSort="true" 
                              AutoFilterRowInputDelay="12000" 
                              ColumnResizeMode="Control" />
            
                                                  <SettingsPager  Mode="ShowPager" PageSize="12" >
                                <AllButton Visible="True">
                                </AllButton>
                          </SettingsPager>
  
                                                  <Columns>
                                                
                                                    <dx:GridViewDataTextColumn 
                                                        Caption="Dealer Code" 
                                                        FieldName="dealercode" 
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowHeaderFilter="True"
                                                        ToolTip=""
                                                        VisibleIndex="1" 
                                                        Width="7%" 
                                                        ExportWidth="200">
                                                    </dx:GridViewDataTextColumn>
                                                    
                                                    <dx:GridViewDataTextColumn 
                                                        Caption="Dealer Name" 
                                                        FieldName="dealername" 
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowHeaderFilter="True"
                                                        ToolTip=""
                                                        VisibleIndex="2" 
                                                        Width="13%" 
                                                        ExportWidth="200">
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                                        Caption="Reference" 
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowHeaderFilter="True"
                                                        FieldName="link" 
                                                        ToolTip=""
                                                        VisibleIndex="3" 
                                                        Width="10%" 
                                                        ExportWidth="200">
                                                    </dx:GridViewDataTextColumn>
                                                    
                                                    <dx:GridViewDataTextColumn
                                                        Caption="Date" 
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowHeaderFilter="True"
                                                        FieldName="invoicedate" 
                                                        ToolTip=""
                                                        VisibleIndex="4" 
                                                        Width="10%" 
                                                        ExportWidth="200">
                                                    </dx:GridViewDataTextColumn>

                                                   <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="5" ExportWidth="150" width="10%">
                                                        <DataItemTemplate>
                                                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                  FieldName="quantity" 
                                  Name="quantity" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Units Sold"
                                  PropertiesTextEdit-DisplayFormatString="##,##0"
                                                        Width="10%" 
                                  VisibleIndex="4" 
                                  ExportWidth="150">
                                 </dx:GridViewDataTextColumn>
                            
                                                    <dx:GridViewDataTextColumn 
                                  FieldName="retailvalue" 
                                  Name="retailvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Retail Value"
                                                        Width="10%" 
                                  VisibleIndex="5" 
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>
                                                        
                                                    <dx:GridViewDataTextColumn 
                                  FieldName="salevalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Name="salevalue" 
                                  Caption="Sale Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"
                                  VisibleIndex="6" 
                                                        Width="10%" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>
               
                                                    <dx:GridViewDataTextColumn 
                                  FieldName="costvalue" 
                                  Name="costvalue" 
                                  CellStyle-HorizontalAlign="Center"
                                  HeaderStyle-HorizontalAlign="Center"
                                  Settings-AllowHeaderFilter="False"
                                  Caption="Cost Value"
                                  PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"
                                  VisibleIndex="7" 
                                                        Width="10%" 
                                  ExportWidth="150">
                              </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                                        VisibleIndex="8" 
                                                        FieldName="profit" 
                                                        Name="profit"
                                                        Caption="Profit" 
                                                        ToolTip="The Profit (or loss) made by this Dealer for this item in this period, calculated by subtracting Cost Value from Sales Value"
                                                        CellStyle-Wrap="False"
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Wrap="False"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"
                                                        Settings-AllowHeaderFilter="False"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowAutoFilter="False"
                                                        Width="10%" 
                                                        ExportWidth="150">
                                                    </dx:GridViewDataTextColumn>

                                                    <dx:GridViewDataTextColumn 
                                                        VisibleIndex="9" 
                                                        FieldName="margin" 
                                                        Name="margin"
                                                        Caption="Margin" 
                                                        ToolTip="The total percentage margin made on sales of this item in this period"
                                                        CellStyle-Wrap="False"
                                                        CellStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Wrap="False"
                                                        HeaderStyle-HorizontalAlign="Center"
                                                        PropertiesTextEdit-DisplayFormatString="0.00%"
                                                        Settings-AllowHeaderFilter="False"
                                                        Settings-HeaderFilterMode="CheckedList"
                                                        Settings-AllowAutoFilter="False"
                                                        Width="10%" 
                                                        ExportWidth="150">
                                                   </dx:GridViewDataTextColumn>

                                                </Columns>

                                                  <TotalSummary>
                                        <dx:ASPxSummaryItem DisplayFormat="#,##0" FieldName="quantity" SummaryType="Sum"  />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="retailvalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="salevalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="costvalue" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="profit" SummaryType="Sum" />
                                        <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="margin" ShowInColumn="margin" SummaryType="Custom" />
                                    </TotalSummary>
        

                                                
                                            </dx:ASPxGridView>
                                            <br />

                                        </DetailRow>
                                        
            
                                        
                                        </Templates>
            
                     
            
                                    <SettingsDetail ShowDetailRow="True" ExportMode="Expanded" IsDetailGrid="True" />
                                    
                                </dx:ASPxGridView>
    
                                   <br />
                                
                            </DetailRow>
                            
                        </Templates>

                    </dx:ASPxGridView>
                    <br />
                    
                </DetailRow>
                
            </Templates>

        </dx:ASPxGridView>
         
    </div>
    
    <asp:SqlDataSource ID="dsViewSales" runat="server" SelectCommand="p_ProductSummary_Level0" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nAnalysisType" SessionField="AnalysisType" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="dsViewSalesLevel1" runat="server" SelectCommand="p_ProductSummary_Level1" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nAnalysisType" SessionField="AnalysisType" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
            <asp:SessionParameter DefaultValue="" Name="sReportIdentifier" SessionField="ReportIdentifier" Type="String" Size="50" /> 
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsViewSalesLevel2" runat="server" SelectCommand="p_ProductSummary_Level2" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="sPFC" SessionField="PFC" Type="String" Size="3" />
            <asp:SessionParameter DefaultValue="" Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsViewSalesLevel3" runat="server" SelectCommand="p_ProductSummary_Level3" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="sPartNumber" SessionField="PartNumber" Type="String" Size="15" />
            <asp:SessionParameter DefaultValue="" Name="nFocusType" SessionField="FocusType" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridSales" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>
