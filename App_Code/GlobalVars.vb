Imports System.Data.SqlClient

Public Class GlobalVars

    Public Shared g_SystemName As String = ""
    Public Shared g_Location As String = "l"
    Public Shared g_StatusText As String = ""
    Public Shared g_User As String = ""
    Public Shared g_Drive As String = ""
    Public Shared g_AppLocation As String = ""
    Public Shared g_Quote As String = "'"
    Public Shared g_VBQuote As String = """"
    Public Shared g_Comma As String = ","

    Public Shared g_Color_White As System.Drawing.Color = System.Drawing.Color.White
    Public Shared g_Color_LightBlue As System.Drawing.Color = System.Drawing.Color.LightBlue
    Public Shared g_Color_YellowGreen As System.Drawing.Color = System.Drawing.Color.YellowGreen
    Public Shared g_Color_Green As System.Drawing.Color = System.Drawing.Color.Green
    Public Shared g_Color_Red As System.Drawing.Color = System.Drawing.Color.Red
    Public Shared g_Color_Black As System.Drawing.Color = System.Drawing.Color.Black
    Public Shared g_Color_Tomato As System.Drawing.Color = System.Drawing.Color.Tomato
    Public Shared g_Color_DevXGrey As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")
    Public Shared g_Color_DevXBackColor As System.Drawing.Color = System.Drawing.Color.FromArgb(148, 182, 232)
    Public Shared g_Color_DarkGray As System.Drawing.Color = System.Drawing.Color.DarkGray
    Public Shared g_Color_LightGray As System.Drawing.Color = System.Drawing.Color.LightGray
    Public Shared g_Color_LightYellow As System.Drawing.Color = System.Drawing.Color.LightYellow
    Public Shared g_Color_Yellow As System.Drawing.Color = System.Drawing.Color.Yellow

    Public Shared g_Color_GridBack1 As System.Drawing.Color = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")
    Public Shared g_Color_GridBack2 As System.Drawing.Color = System.Drawing.Color.FromArgb(148, 182, 232)



End Class

