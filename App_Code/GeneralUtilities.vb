Imports DevExpress.Web
Imports System.Data
Imports System.Linq
Imports System.Net.Mail
Imports QubeUtils
Imports Encoder = Microsoft.Security.Application.Encoder

Public Module GeneralUtilities

    Function CurrentPeriodId() As Integer
        Return GetDataLong("SELECT Id FROM DataPeriods WHERE Id = (SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus = 'O')")
    End Function

    Function CurrentMonth() As Integer
        Return GetDataLong("SELECT CalendarMonth FROM DataPeriods WHERE Id = (SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C')")
    End Function

    Function CurrentYear() As Integer
        Return GetDataLong("SELECT CalendarYear FROM DataPeriods WHERE Id = (SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C')")
    End Function

    Function pCase(ByVal strIn As String) As String

        '/----------------------------------------------------------------------------------
        '/ Turns "HELLO WORLD" or "hello world" or "HEllo wORLD" into "Hello World"
        '/----------------------------------------------------------------------------------

        Dim strOut As String = ""
        Dim bCapital As Boolean = True
        Dim lngCounter As Long
        Dim chrTmp As String

        strIn = Trim(strIn)

        For lngCounter = 1 To Len(strIn)
            ' Get the current character
            chrTmp = Mid$(strIn, lngCounter, 1)
            If bCapital = True Then
                strOut = strOut & UCase(chrTmp)
            Else
                strOut = strOut & LCase(chrTmp)
            End If
            If chrTmp = " " Or chrTmp = "." Or chrTmp = "(" Or chrTmp = ")" Or chrTmp = "," Or chrTmp = "_" Or chrTmp = "-" Then
                bCapital = True
            Else
                bCapital = False
            End If
        Next
        ' Return the cleaned string
        Return strOut

    End Function

    Function isEmpty(ByVal sTextString As String) As Boolean
        If Len(Trim(sTextString)) = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function FileExists(ByVal spathname As String, ByVal sfilename As String) As Boolean
        Dim breturnvalue As Boolean = False
        Dim sPath As String = Trim(spathname) & Trim(sfilename)
        If System.IO.File.Exists(sPath) = True Then
            breturnvalue = True
        Else
            breturnvalue = False
        End If
        Return breturnvalue
    End Function

    Public Sub MakeDirIfNeeded(ByVal DirName As String)
        '/ Checks to see if a directory exists and makes it if it doesnt
        If Trim(Dir(DirName, vbDirectory)) = "" Then
            MkDir(DirName)
        End If
    End Sub

    Public Function InCSVString(ByVal sCSVList As String, ByVal sSearchString As String) As Boolean
        ' Checks if a CSV String Contains a Given Value
        Dim bReturnValue As Boolean = False
        Dim nCSVLen As Integer = Len(Trim(sCSVList))
        Dim sWorkingChar As String = ""
        Dim sCheckString As String = ""
        Dim nLoop As Integer = 0
        For nLoop = 1 To nCSVLen
            sWorkingChar = Mid(sCSVList, nLoop, 1)
            If sWorkingChar = "," Then
                If sCheckString = sSearchString Then
                    bReturnValue = True
                    Return bReturnValue
                End If
                sCheckString = ""
            Else
                sCheckString &= sWorkingChar
            End If

        Next
        Return bReturnValue
    End Function

    Public Sub RenameFile(ByVal sFullPath As String, ByVal sOldName As String, ByVal sNewName As String)
        Dim sFullOldName As String
        sFullOldName = Trim(sFullPath) & Trim(sOldName)
        My.Computer.FileSystem.RenameFile(sFullOldName, sNewName)
    End Sub

    Public Sub KillFile(ByVal sFullPath As String, ByVal sFileToKill As String)
        My.Computer.FileSystem.DeleteFile(sFullPath & sFileToKill)
    End Sub

    Function KillComma(ByVal sText As String) As String

        Dim nLen As Integer
        Dim sReturnString As String
        Dim sWorkString As String
        Dim i As Integer

        sReturnString = ""
        sWorkString = ""

        sText = Trim(sText)
        nLen = Len(sText)
        For i = 1 To nLen
            sWorkString = Mid(sText, i, 1)
            If sWorkString <> "," Then
                sReturnString = sReturnString & sWorkString
            Else
                sReturnString = sReturnString & " "
            End If
        Next
        KillComma = Trim(sReturnString)
    End Function

    Function KillSpaces(ByVal sText As String) As String

        Dim nLen As Integer
        Dim sReturnString As String
        Dim sWorkString As String
        Dim i As Integer

        sReturnString = ""
        sWorkString = ""

        sText = Trim(sText)
        nLen = Len(sText)
        For i = 1 To nLen
            sWorkString = Mid(sText, i, 1)
            If sWorkString <> " " Then
                sReturnString = sReturnString & sWorkString
            Else
                sReturnString = sReturnString & ""
            End If
        Next
        KillSpaces = Trim(sReturnString)
    End Function


    Function Between(ByVal nValueToCompare As Decimal, ByVal nLower As Decimal, ByVal nUpper As Decimal) As Boolean
        If (nValueToCompare >= nLower) And (nValueToCompare <= nUpper) Then
            Return True
        Else
            Return False
        End If
    End Function

    Function GetLastDayOfLastMonth(ByVal dIndate As Date) As Date
        Dim dDays As Integer = DatePart(DateInterval.Day, dIndate)
        Return DateAdd(DateInterval.Day, -(dDays), dIndate)
    End Function

    Public Function FirstDayOfMonth(ByVal nMonth As Integer, ByVal nYear As Integer) As Date
        ' Returns the first day of a givenmonth
        Dim sMonth As String
        If nMonth < 10 Then
            sMonth = " " & Str(nMonth)
        Else
            sMonth = Str(nMonth)
        End If
        Return CDate("01/" & sMonth & "/" & Str(nYear))
    End Function

    Private Function StripSpaces(ByVal sInString As String) As String
        'Input: String containing Spaces
        'Returns: Same String With All Spaces Removed
        Dim sReturnString As String = ""
        Dim sWorkString As String = ""
        Dim nLoop As Integer = 1
        For nLoop = 1 To Len(Trim(sInString))
            sWorkString = Mid(sInString, nLoop, 1)
            sReturnString += Trim(sWorkString)
        Next
        Return Trim(sReturnString)
    End Function

    Private Function StripJunk(ByVal sInString As String) As String
        'Input: String containing "-","," etc
        'Returns: Same String With All Junk Removed
        Dim sReturnString As String = ""
        Dim sWorkString As String = ""
        Dim nLoop As Integer = 1
        For nLoop = 1 To Len(Trim(sInString))
            sWorkString = Mid(sInString, nLoop, 1)
            If sWorkString <> "-" And sWorkString <> "_" And sWorkString <> "," Then
                sReturnString += Trim(sWorkString)
            End If
        Next
        Return Trim(sReturnString)
    End Function

    Function FindAndReplace(ByVal sString As String, ByVal sSearch As String, ByVal sReplace As String) As String
        Dim sReturnValue As String = ""
        sString = Trim(sString)
        Dim nLen As Integer = Len(sString)
        Dim sWorkChar As String = ""
        Dim nLoop As Integer
        For nLoop = 1 To nLen
            sWorkChar = Mid(sString, nLoop, 1)
            If sWorkChar = sSearch Then
                sWorkChar = sReplace
            End If
            sReturnValue &= sWorkChar
        Next
        Return sReturnValue
    End Function

    Public Sub AspMessageAlert(ByRef aspxPage As System.Web.UI.Page, ByVal strMessage As String, ByVal strKey As String)
    End Sub

    Public Function LinkName(ByVal sLink As String) As String
        Select Case sLink
            Case "A"
                LinkName = "Account"
            Case "M", "T"
                LinkName = "Target"
            Case "C"
                LinkName = "Company"
            Case Else
                LinkName = "Unknown link!"
        End Select
    End Function

    Public Function CanViewDealer(ByVal sSelectionLevel As String, ByVal sSelectionId As String, ByVal sDealerCode As String) As Boolean

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        CanViewDealer = False

        htin.Add("@sSelectionLevel", sSelectionLevel)
        htin.Add("@sSelectionId", sSelectionId)
        htin.Add("@sDealerCode", sDealerCode)

        ds = da.Read(sErrorMessage, "p_DealerSelectionCount", htin)
        If ds.Tables(0).Rows.Count = 1 Then
            If ds.Tables(0).Rows(0).Item("DealerCount") = 1 Then
                CanViewDealer = True
            End If
        End If

    End Function

    Public Function GetMaxDate(ByVal sSelectionLevel As String, ByVal sSelectionId As String) As String

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable
        Dim dDate As Date

        GetMaxDate = ""

        htin.Add("@sSelectionLevel", sSelectionLevel)
        htin.Add("@sSelectionId", sSelectionId)

        ds = da.Read(sErrorMessage, "p_MaxDataDate", htin)
        If ds.Tables(0).Rows.Count = 1 Then
            dDate = ds.Tables(0).Rows(0).Item("MaxDate")
            GetMaxDate = Format(dDate, "dd-MMM-yyyy")
        End If

    End Function

    Public Sub GetQueryDefn(ByVal ddl As DropDownList, ByVal nUserId As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@nUserId", nUserId)

        ds = da.Read(sErrorMessage, "p_AdvQuery_List", htin)
        ddl.DataSource = ds
        ddl.DataValueField = "Id"
        ddl.DataTextField = "QueryName"
        ddl.DataBind()

    End Sub

    Public Sub GetQueryDefnDevEx(ByVal ddl As DevExpress.Web.ASPxComboBox, ByVal nUserId As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@nUserId", nUserId)

        ds = da.Read(sErrorMessage, "p_AdvQuery_List", htin)
        ddl.DataSource = ds
        ddl.ValueField = "Id"
        ddl.TextField = "QueryName"
        ddl.DataBind()

    End Sub



    Public Sub GetCampaignRewards(ByVal ddl As DropDownList, ByVal nCampaignYear As Integer, ByVal nCampaignQuarter As Integer, ByVal nCampaignId As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@nYear", nCampaignYear)
        htin.Add("@nQtr", nCampaignQuarter)
        htin.Add("@nId", nCampaignId)

        ds = da.Read(sErrorMessage, "p_Get_Campaign_Rewards", htin)
        ddl.DataSource = ds
        ddl.DataValueField = "RewardId"
        ddl.DataTextField = "Description"
        ddl.DataBind()

    End Sub

    Public Sub GetCentreSpecificInfo(ByVal nCustomerID As Long, ByVal sDealerCode As String, ByRef sVanRoute As String, ByRef sDriveTime As String, ByRef sDistance As String, ByRef sSalesRep As String, ByRef sContactName As String, ByRef sTelephoneNumber As String, ByRef sUserField1 As String, ByRef sUserField2 As String, ByRef sUserField3 As String, ByRef sUserField4 As String, ByRef sNotes As String)
        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sSQL As String
        Dim sErrorMessage As String = ""

        sSQL = "SELECT TOP 1 * FROM CustomerDealerSpecific WHERE CustomerId = " & Str(nCustomerID) & " AND DealerCode = '" & Trim(sDealerCode) & "'"
        ds = da.ExecuteSQL(sErrorMessage, sSQL)
        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                sVanRoute = "" & .Item("VanRoute").ToString
                sDriveTime = "" & .Item("DriveTime").ToString
                sDistance = "" & .Item("Distance").ToString
                sSalesRep = "" & .Item("SalesRep").ToString
                sContactName = "" & .Item("ContactName").ToString
                sTelephoneNumber = "" & .Item("TelephoneNumber").ToString
                sUserField1 = "" & .Item("Userfield1").ToString
                sUserField2 = "" & .Item("Userfield2").ToString
                sUserField3 = "" & .Item("Userfield3").ToString
                sUserField4 = "" & .Item("Userfield4").ToString
                sNotes = "" & .Item("Notes").ToString
            End With
        End If

    End Sub

    Public Sub GetTradeClubStatus(ByVal nCustomerID As Long, ByVal sDealerCode As String, ByRef bOnTradeClub As Boolean, ByRef nApprovalStatus As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sSQL As String
        Dim sErrorMessage As String = ""

        bOnTradeClub = False

        sSQL = "SELECT * FROM CustomerTradeClubMembership WHERE CustomerId = " & Str(nCustomerID) & " AND DealerCode = '" & Trim(sDealerCode) & "' AND DeletedFlag = 'N'"
        ds = da.ExecuteSQL(sErrorMessage, sSQL)
        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                bOnTradeClub = ("" & .Item("DeletedFlag").ToString) = "N"
                nApprovalStatus = Val("" & .Item("ApprovalStatus"))
            End With
        End If

    End Sub


    Public Function IsInCDA(ByVal sSelectionLevel As String, ByVal sSelectionId As String, ByVal sTerritory As String) As Boolean

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        IsInCDA = False

        htin.Add("@sSelectionLevel", sSelectionLevel)
        htin.Add("@sSelectionId", sSelectionId)
        htin.Add("@sTerritory", sTerritory)
        ds = da.Read(sErrorMessage, "p_SelectionCDACount", htin)
        If ds.Tables(0).Rows.Count = 1 Then
            If ds.Tables(0).Rows(0).Item("CDACount").ToString > 0 Then
                IsInCDA = True
            End If
        End If

    End Function

    Public Sub GetMonths(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "Id"
        ddlMths.DataTextField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub
    Public Sub GetMonthsDevX(ByVal ddlMths As DevExpress.Web.ASPxComboBox)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_All", htin)
        ddlMths.DataSource = ds
        ddlMths.TextField = "PeriodName"
        ddlMths.ValueField = "Id"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetMonthsThisYear(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_ThisYear", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "Id"
        ddlMths.DataTextField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetMonthsWhole(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_Whole", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "Id"
        ddlMths.DataTextField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetMonthsWholeDevEx(ByVal ddlMths As DevExpress.Web.ASPxComboBox)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_Whole", htin)
        ddlMths.DataSource = ds
        ddlMths.TextField = "Id"
        ddlMths.ValueField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub
    Public Function GetNewTCNumber(ByVal sBusinessType As String, ByVal nCustomerId As Long) As String

        Dim sTCNumber As String
        Dim sSQL As String

        sSQL = "SELECT TradeClubNumber FROM Customer WHERE Id = " & Trim(Str(nCustomerId))
        sTCNumber = GetDataString(sSQL)
        If Trim(sTCNumber) = "" Then
            sTCNumber = "T" & Right("00" & Trim(sBusinessType), 2) & Format(nCustomerId, "0000000")
        End If
        GetNewTCNumber = sTCNumber

    End Function

    Public Sub GetMonthsComplete(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_Complete", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "Id"
        ddlMths.DataTextField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetMonthsAll(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Months_All", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "Id"
        ddlMths.DataTextField = "PeriodName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetUsers(ByVal ddlMths As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Get_Users", htin)
        ddlMths.DataSource = ds
        ddlMths.DataValueField = "UserId"
        ddlMths.DataTextField = "FirstName"
        ddlMths.DataBind()
        ddlMths.SelectedIndex = 0

    End Sub

    Public Sub GetDefinitions(ByVal ddlDef As DropDownList, ByVal nType As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        htin.Add("@nType", nType)

        ds = da.Read(sErrorMessage, "p_Get_PFC_Categories", htin)
        ddlDef.DataSource = ds
        ddlDef.DataValueField = "Category"
        ddlDef.DataTextField = "Category"
        ddlDef.DataBind()
        ddlDef.SelectedIndex = 0

    End Sub

    Public Sub GetSegments(ByVal chkBox As CheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Segments", htin)
        chkBox.DataSource = ds
        chkBox.DataValueField = "Segment"
        chkBox.DataTextField = "Segment"
        chkBox.DataBind()

    End Sub

    Public Sub GetSegmentsDevX(ByVal chkBox As DevExpress.Web.ASPxCheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_Segments", htin)
        chkBox.DataSource = ds
        chkBox.TextField = "Segment"
        chkBox.ValueField = "Segment"
        chkBox.DataBind()

    End Sub
    Public Sub GetProductGroups(ByVal chkBox As CheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_ProductGroups", htin)
        chkBox.DataSource = ds
        chkBox.DataValueField = "ProductGroup"
        chkBox.DataTextField = "ProductGroup"
        chkBox.DataBind()

    End Sub

    Public Sub GetProductGroupsDevX(ByVal chkBox As DevExpress.Web.ASPxCheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "sp_Level4Code", htin)
        chkBox.DataSource = ds
        chkBox.ValueField = "productgroup"
        chkBox.TextField = "description"
        chkBox.DataBind()

    End Sub
    Public Sub GetBusinessTypesChkBoxes(ByVal chkBox As CheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_BusinessTypes", htin)
        chkBox.DataSource = ds
        chkBox.DataValueField = "BusinessType"
        chkBox.DataTextField = "BusinessType"
        chkBox.DataBind()

    End Sub
    Public Sub GetBusinessTypesChkBoxesDevX(ByVal chkBox As DevExpress.Web.ASPxCheckBoxList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_BusinessTypes", htin)
        chkBox.DataSource = ds
        chkBox.ValueField = "BusinessType"
        chkBox.TextField = "BusinessType"
        chkBox.DataBind()

    End Sub

    Public Sub GetBusinessTypes(ByVal ddlBT As DropDownList)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_BusinessTypes", htin)
        ddlBT.DataSource = ds
        ddlBT.DataValueField = "Id"
        ddlBT.DataTextField = "BusinessType"
        ddlBT.DataBind()
        ddlBT.SelectedIndex = 0

    End Sub

    Public Sub GetBusinessTypes2(ByVal ddlBT As DropDownList, ByVal nId As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        If nId = 1 Or nId = 6 Or nId = 7 Then
            ds = da.Read(sErrorMessage, "p_BusinessTypes_Piranha", htin)
            ddlBT.DataSource = ds
            ddlBT.DataValueField = "Id"
            ddlBT.DataTextField = "BusinessType"
            ddlBT.DataBind()
            ddlBT.SelectedIndex = 0
        Else
            ds = da.Read(sErrorMessage, "p_BusinessTypes_Goldfish", htin)
            ddlBT.DataSource = ds
            ddlBT.DataValueField = "Id"
            ddlBT.DataTextField = "BusinessType"
            ddlBT.DataBind()
            ddlBT.SelectedIndex = 0
        End If


    End Sub

    Public Function GetPartDescription(ByVal sPart As String) As String
        Dim sSQL As String
        sSQL = "SELECT Description FROM Part WHERE PartNumber = '" & sPart & "'"
        GetPartDescription = GetDataString(sSQL)
    End Function

    Public Function GetPFCDescription(ByVal sPFC As String) As String
        Dim sSQL As String
        sSQL = "SELECT Description FROM PartPFC WHERE PFC = '" & sPFC & "'"
        GetPFCDescription = GetDataString(sSQL)
    End Function

    Public Function GetPeriodName(ByVal nId As Integer) As String
        Dim sSQL As String
        sSQL = "SELECT PeriodName FROM DataPeriods WHERE Id = " & Trim(Str(nId))
        GetPeriodName = GetDataString(sSQL)
    End Function

    Public Sub AddToActivityLog(ByVal nUserId As Integer, ByVal sSelectionLevel As String, ByVal sSelectionId As String, ByVal sScreen As String, ByVal sNotes As String)

        Dim sSQL As String

        sSQL = " p_ActivityLog_Ins " & Trim(Str(nUserId)) & ","
        sSQL = sSQL & "'" & sSelectionLevel & "',"
        sSQL = sSQL & "'" & sSelectionId & "',"
        sSQL = sSQL & "'" & sScreen & "',"
        sSQL = sSQL & "'" & Replace(sNotes, "'", "") & "'"
        Call RunNonQueryCommand(sSQL)

    End Sub

    Function GetDaysInMonth(ByVal nDataPeriod As Integer) As Integer
        Dim nReturnValue As Integer = GetDataDecimal("SELECT DATEDIFF(d,StartDate,EndDate)+1 FROM DataPeriods WHERE Id = " & nDataPeriod)
        Return nReturnValue
    End Function

     Public Function GetDayCaption(ByVal nPeriodID As Integer, ByVal nDayOfMonth As Integer) As String

        Dim sReturnValue As String = ""
        Dim nFirstDayOfMonth As Integer = GetDataDecimal("SELECT DATEPART(dw, Startdate) FROM DataPeriods WHERE Id = " & nPeriodID)
        Dim sDay1Text As String = ""
        Dim sDay2Text As String = ""
        Dim sDay3Text As String = ""
        Dim sDay4Text As String = ""
        Dim sDay5Text As String = ""
        Dim sDay6Text As String = ""
        Dim sDay7Text As String = ""
        Dim sDay8Text As String = ""
        Dim sDay9Text As String = ""
        Dim sDay10Text As String = ""
        Dim sDay11Text As String = ""
        Dim sDay12Text As String = ""
        Dim sDay13Text As String = ""
        Dim sDay14Text As String = ""
        Dim sDay15Text As String = ""
        Dim sDay16Text As String = ""
        Dim sDay17Text As String = ""
        Dim sDay18Text As String = ""
        Dim sDay19Text As String = ""
        Dim sDay20Text As String = ""
        Dim sDay21Text As String = ""
        Dim sDay22Text As String = ""
        Dim sDay23Text As String = ""
        Dim sDay24Text As String = ""
        Dim sDay25Text As String = ""
        Dim sDay26Text As String = ""
        Dim sDay27Text As String = ""
        Dim sDay28Text As String = ""
        Dim sDay29Text As String = ""
        Dim sDay30Text As String = ""
        Dim sDay31Text As String = ""

        Select Case nFirstDayOfMonth
            Case 1
                sDay7Text = "Su "
                sDay1Text = "Mo "
                sDay2Text = "Tu "
                sDay3Text = "We "
                sDay4Text = "Th "
                sDay5Text = "Fr "
                sDay6Text = "Sa "
            Case 2
                sDay6Text = "Su "
                sDay7Text = "Mo "
                sDay1Text = "Tu "
                sDay2Text = "We "
                sDay3Text = "Th "
                sDay4Text = "Fr "
                sDay5Text = "Sa "
            Case 3
                sDay5Text = "Su "
                sDay6Text = "Mo "
                sDay7Text = "Tu "
                sDay1Text = "We "
                sDay2Text = "Th "
                sDay3Text = "Fr "
                sDay4Text = "Sa "
            Case 4
                sDay4Text = "Su "
                sDay5Text = "Mo "
                sDay6Text = "Tu "
                sDay7Text = "We "
                sDay1Text = "Th "
                sDay2Text = "Fr "
                sDay3Text = "Sa "
            Case 5
                sDay3Text = "Su "
                sDay4Text = "Mo "
                sDay5Text = "Tu "
                sDay6Text = "We "
                sDay7Text = "Th "
                sDay1Text = "Fr "
                sDay2Text = "Sa "
            Case 6
                sDay2Text = "Su "
                sDay3Text = "Mo "
                sDay4Text = "Tu "
                sDay5Text = "We "
                sDay6Text = "Th "
                sDay7Text = "Fr "
                sDay1Text = "Sa "
            Case 7
                sDay1Text = "Su "
                sDay2Text = "Mo "
                sDay3Text = "Tu "
                sDay4Text = "We "
                sDay5Text = "Th "
                sDay6Text = "Fr "
                sDay7Text = "Sa "
        End Select
        sDay8Text = sDay1Text
        sDay9Text = sDay2Text
        sDay10Text = sDay3Text
        sDay11Text = sDay4Text
        sDay12Text = sDay5Text
        sDay13Text = sDay6Text
        sDay14Text = sDay7Text
        sDay15Text = sDay1Text
        sDay16Text = sDay2Text
        sDay17Text = sDay3Text
        sDay18Text = sDay4Text
        sDay19Text = sDay5Text
        sDay20Text = sDay6Text
        sDay21Text = sDay7Text
        sDay22Text = sDay1Text
        sDay23Text = sDay2Text
        sDay24Text = sDay3Text
        sDay25Text = sDay4Text
        sDay26Text = sDay5Text
        sDay27Text = sDay6Text
        sDay28Text = sDay7Text
        sDay29Text = sDay1Text
        sDay30Text = sDay2Text
        sDay31Text = sDay3Text

        Select Case nDayOfMonth
            Case 1
                sReturnValue = sDay1Text & nDayOfMonth
            Case 2
                sReturnValue = sDay2Text & nDayOfMonth
            Case 3
                sReturnValue = sDay3Text & nDayOfMonth
            Case 4
                sReturnValue = sDay4Text & nDayOfMonth
            Case 5
                sReturnValue = sDay5Text & nDayOfMonth
            Case 6
                sReturnValue = sDay6Text & nDayOfMonth
            Case 7
                sReturnValue = sDay7Text & nDayOfMonth
            Case 8
                sReturnValue = sDay1Text & nDayOfMonth
            Case 9
                sReturnValue = sDay2Text & nDayOfMonth
            Case 10
                sReturnValue = sDay3Text & nDayOfMonth
            Case 11
                sReturnValue = sDay4Text & nDayOfMonth
            Case 12
                sReturnValue = sDay5Text & nDayOfMonth
            Case 13
                sReturnValue = sDay6Text & nDayOfMonth
            Case 14
                sReturnValue = sDay7Text & nDayOfMonth
            Case 15
                sReturnValue = sDay1Text & nDayOfMonth
            Case 16
                sReturnValue = sDay2Text & nDayOfMonth
            Case 17
                sReturnValue = sDay3Text & nDayOfMonth
            Case 18
                sReturnValue = sDay4Text & nDayOfMonth
            Case 19
                sReturnValue = sDay5Text & nDayOfMonth
            Case 20
                sReturnValue = sDay6Text & nDayOfMonth
            Case 21
                sReturnValue = sDay7Text & nDayOfMonth
            Case 22
                sReturnValue = sDay1Text & nDayOfMonth
            Case 23
                sReturnValue = sDay2Text & nDayOfMonth
            Case 24
                sReturnValue = sDay3Text & nDayOfMonth
            Case 25
                sReturnValue = sDay4Text & nDayOfMonth
            Case 26
                sReturnValue = sDay5Text & nDayOfMonth
            Case 27
                sReturnValue = sDay6Text & nDayOfMonth
            Case 28
                sReturnValue = sDay7Text & nDayOfMonth
            Case 29
                sReturnValue = sDay1Text & nDayOfMonth
            Case 30
                sReturnValue = sDay2Text & nDayOfMonth
            Case 31
                sReturnValue = sDay3Text & nDayOfMonth
        End Select

        Return sReturnValue
    End Function

    Function NicePeriodName(ByVal sPeriodName As String) As String

        Dim sStr As String

        sPeriodName = Trim(sPeriodName)

        Select Case Right(sPeriodName, 2)
            Case "01"
                sStr = "Jan " + Left(sPeriodName, 4)
            Case "02"
                sStr = "Feb " + Left(sPeriodName, 4)
            Case "03"
                sStr = "Mar " + Left(sPeriodName, 4)
            Case "04"
                sStr = "Apr " + Left(sPeriodName, 4)
            Case "05"
                sStr = "May " + Left(sPeriodName, 4)
            Case "06"
                sStr = "Jun " + Left(sPeriodName, 4)
            Case "07"
                sStr = "Jul " + Left(sPeriodName, 4)
            Case "08"
                sStr = "Aug " + Left(sPeriodName, 4)
            Case "09"
                sStr = "Sep " + Left(sPeriodName, 4)
            Case "10"
                sStr = "Oct " + Left(sPeriodName, 4)
            Case "11"
                sStr = "Nov " + Left(sPeriodName, 4)
            Case "12"
                sStr = "Dec " + Left(sPeriodName, 4)
            Case Else
                sStr = "??? " + Left(sPeriodName, 4)
        End Select

        NicePeriodName = sStr

    End Function

    Public Function FormatFancyNumber(ByVal sNumber As String) As String
        Dim iTemp As Integer
        iTemp = Int(sNumber)
        If 4 < iTemp And iTemp < 20 Then
            FormatFancyNumber = sNumber & "th"
        Else
            Select Case iTemp Mod 10
                Case 1
                    FormatFancyNumber = sNumber & "st"
                Case 2
                    FormatFancyNumber = sNumber & "nd"
                Case 3
                    FormatFancyNumber = sNumber & "rd"
                Case Else
                    FormatFancyNumber = sNumber & "th"
            End Select
        End If
    End Function

    Public Sub OldGlobalRenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Dim nThisValue As Decimal = 0

        e.Url = ""
        e.BrickStyle.Font = New System.Drawing.Font("Arial", 9, Drawing.FontStyle.Regular)
        e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then

            e.BrickStyle.ForeColor = Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#a4a2bd")

        Else

            e.BrickStyle.ForeColor = Drawing.Color.Black
            e.BrickStyle.BackColor = Drawing.Color.White

            Select Case e.Column.Name
                Case "SaleValue", "CostValue"
                    e.TextValueFormatString = "c0"
                Case "Quantity"
                    e.TextValueFormatString = "#,##0"
                Case "Margin"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                Case "Variance"
                    e.TextValue = Format(e.Value, "#,##0.00") & "%"
                    If IsDBNull(e.Value) = True Then
                        nThisValue = 0
                    Else
                        nThisValue = CDec(e.Value)
                    End If
                    If nThisValue = 0 Then
                        e.BrickStyle.BackColor = Drawing.Color.White
                        e.BrickStyle.ForeColor = Drawing.Color.Black
                    ElseIf nThisValue > 0 Then
                        e.BrickStyle.BackColor = Drawing.Color.Green
                    Else
                        e.BrickStyle.BackColor = Drawing.Color.Red
                    End If
            End Select

        End If

    End Sub

    Public Sub GlobalRenderBrick(ByRef e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs)

        Dim nThisValue As Decimal = 0

        e.BrickStyle.ForeColor = Drawing.Color.Black
        e.BrickStyle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter

        Select Case e.RowType

            Case GridViewRowType.Title
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 9, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.LightGray

            Case GridViewRowType.Header
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 9, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.LightGray

            Case GridViewRowType.Footer
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.LightGray

            Case GridViewRowType.Data
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.White

            Case Else
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.White

        End Select

    End Sub

    Public Sub GlobalRenderBrickTreeList(ByRef e As ASPxTreeList.ASPxTreeListExportRenderBrickEventArgs)

        Dim nThisValue As Decimal = 0

        e.BrickStyle.ForeColor = Drawing.Color.Black

        Select Case e.RowKind

            Case ASPxTreeList.TreeListRowKind.Header
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 9, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.LightGray

            Case ASPxTreeList.TreeListRowKind.Footer
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.LightGray

            Case ASPxTreeList.TreeListRowKind.Data
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.White

            Case Else
                e.BrickStyle.Font = New System.Drawing.Font("Verdana", 8, Drawing.FontStyle.Regular)
                e.BrickStyle.BackColor = Drawing.Color.White

        End Select

    End Sub

    Public Function GetShortMonthName(ByVal nMonth As Integer) As String

        Select Case nMonth
            Case 1
                GetShortMonthName = "Jan"
            Case 2
                GetShortMonthName = "Feb"
            Case 3
                GetShortMonthName = "Mar"
            Case 4
                GetShortMonthName = "Apr"
            Case 5
                GetShortMonthName = "May"
            Case 6
                GetShortMonthName = "Jun"
            Case 7
                GetShortMonthName = "Jul"
            Case 8
                GetShortMonthName = "Aug"
            Case 9
                GetShortMonthName = "Sep"
            Case 10
                GetShortMonthName = "Oct"
            Case 11
                GetShortMonthName = "Nov"
            Case 12
                GetShortMonthName = "Dec"
            Case Else
                GetShortMonthName = "xxx"
        End Select

    End Function

    Public Function GetMonthNumber(ByVal sMonth As String) As Integer

        Select Case sMonth
            Case "Jan"
                GetMonthNumber = 1
            Case "Feb"
                GetMonthNumber = 2
            Case "Mar"
                GetMonthNumber = 3
            Case "Apr"
                GetMonthNumber = 4
            Case "May"
                GetMonthNumber = 5
            Case "Jun"
                GetMonthNumber = 6
            Case "Jul"
                GetMonthNumber = 7
            Case "Aug"
                GetMonthNumber = 8
            Case "Sep"
                GetMonthNumber = 9
            Case "Oct"
                GetMonthNumber = 10
            Case "Nov"
                GetMonthNumber = 11
            Case "Dec"
                GetMonthNumber = 12
            Case Else
                GetMonthNumber = 0
        End Select

    End Function


    Sub Grid_Styles(ByRef inGrid As DevExpress.Web.ASPxGridView, bShowSearchPanel As Boolean)

        With inGrid
            ' .Font.Size = 8

            With .SettingsSearchPanel
                .Visible = bShowSearchPanel
                .ShowApplyButton = False
                .ShowClearButton = False
            End With
        End With

        With inGrid.Styles
            .SearchPanel.BackColor = Drawing.Color.WhiteSmoke
            .Footer.Font.Bold = True
            .AlternatingRow.BackColor = Drawing.Color.WhiteSmoke
        End With

    End Sub
    Sub TreeList_Styles(ByRef inTreeList As DevExpress.Web.ASPxTreeList.ASPxTreeList)

        With inTreeList

            '.Font.Size = 8

            With .Styles.Header
                '        .BackgroundImage.ImageUrl = "~\images\BlueBackGround.png"
                .CssClass = "textsmallcaps"
                '.BackgroundImage.ImageUrl = "~\images2014\MetropolisBlueBackGround.png"
                ' .BackgroundImage.ImageUrl = "~\images2014\MulberryBackGround.png"
                .ForeColor = Drawing.Color.White
            End With

            With .Styles.AlternatingNode
                .BackColor = Drawing.Color.WhiteSmoke
            End With

        End With
    End Sub

    Sub PivotGrid_Styles(ByRef inGrid As DevExpress.Web.ASPxPivotGrid.ASPxPivotGrid)

        'With inGrid
        '.Font.Size = 8
        'End With

        '        With inGrid.Styles
        '       .HeaderStyle.BackgroundImage.ImageUrl = "~\images2014\MetropolisBlueBackGround.png"
        '      .HeaderStyle.ForeColor = Drawing.Color.White
        '     End With

    End Sub
    Public Sub WriteToResponse(ByVal page As Page, ByVal fileName As String, ByVal saveAsFile As Boolean, ByVal fileFormat As String, ByVal stream As System.IO.MemoryStream)

        If page Is Nothing OrElse page.Response Is Nothing Then
            Return
        End If
        Dim disposition As String
        If saveAsFile Then
            disposition = "attachment"
        Else
            disposition = "inline"
        End If

        page.Response.Clear()
        page.Response.Buffer = False
        page.Response.AppendHeader("Content-Type", String.Format("application/{0}", fileFormat))
        page.Response.AppendHeader("Content-Transfer-Encoding", "binary")
        page.Response.AppendHeader("Content-Disposition", String.Format("{0}; filename={1}.{2}", disposition, fileName, fileFormat))
        page.Response.BinaryWrite(stream.ToArray())
        page.Response.End()

    End Sub

    Function IsValidEmail(ByVal emailAddress As String) As Boolean
        'Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim pattern As String = "([\w-+]+(?:\.[\w-+]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7})"
        Dim emailAddressMatch As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            IsValidEmail = True
        Else
            IsValidEmail = False
        End If
    End Function

Public Sub GetBusinessTypes2dx(ByVal ddlBT As DevExpress.Web.ASPxComboBox, ByVal nId As Integer)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_BusinessTypesALL", htin)
        ddlBT.DataSource = ds
        ddlBT.ValueField = "Id"
        ddlBT.TextField = "BusinessType"
        ddlBT.DataBind()
        ddlBT.SelectedIndex = 0

    End Sub

    Public Sub GetBusinessTypesdx(ByVal ddlBT As DevExpress.Web.ASPxComboBox)

        Dim ds As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim htin As New Hashtable

        ds = da.Read(sErrorMessage, "p_BusinessTypesALL", htin)
        ddlBT.DataSource = ds
        ddlBT.ValueField = "Id"
        ddlBT.TextField = "BusinessType"
        ddlBT.DataBind()
        ddlBT.SelectedIndex = 0

    End Sub

    Public Function isQubeUser(sEmail As String) As Boolean
        Return DataExists("select * from web_user where emailaddress = '" & Trim(sEmail) & "' and superuser = 'Y'")
    End Function

    Public Function HTMLEscape(ByVal sText As String) As String
        sText = Encoder.HtmlEncode(sText)
        sText = sText.Replace("�", "&raquo;")
        sText = sText.Replace("�", "&laquo;")
        sText = sText.Replace("&#13;", "")
        Return sText
    End Function

    public Function URLEncode(sText As String) as String
        sText = Encoder.UrlEncode(sText)
        return sText
    End Function


    Sub SendSupportEmail(ByVal sUserEmail As String, ByVal sBody As String, ByVal sSubject As String, Optional ByVal bOmitBCC as boolean = false, Optional ByVal bIsHtml As Boolean = true)

        Dim MailObj As New SmtpClient
        Dim mm As New MailMessage

        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")

        basicAuthenticationInfo.UserName = Settings.GetString("SMTPUsername")
        basicAuthenticationInfo.Password = Settings.GetString("SMTPPassword")

        MailObj.Host = Settings.GetString("SMTPServer")
        MailObj.UseDefaultCredentials = False
        MailObj.Credentials = basicAuthenticationInfo

        mm.To.Add(sUserEmail)
        
        If bOmitBCC = false Then
            mm.Bcc.Add("nissansupport@qubedata.net")
        End If

        mm.IsBodyHtml = bIsHtml
        mm.From = New MailAddress(Settings.GetString("SupportEmail"))
        mm.Subject = sSubject
        mm.Body = sBody & "<br/><br/>"
        mm.Body += "If you require further assistance, you can contact the " + Settings.GetString("WebAppName") + " support by emailing "
        mm.Body += "<a href='mailto:" & Settings.GetString("SupportEmail") & "'>" & Settings.GetString("SupportEmail") & "</a>.<br/><br/>"
        mm.Body += "Kind Regards<br/><br/>"
        mm.Body += "The " + Settings.GetString("WebAppName") + " website support team."

        MailObj.Send(mm)

    End Sub


    Function IsValidEmailStrict(Byval email as string) as boolean
        Static emailExpression As New Regex("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")

        return emailExpression.IsMatch(email)
    End Function

    Public Function FoundInList(inlistfull As String, itemtofind As String) As Boolean

        ' Searches a list of items ( inlistfull - concatenated by commas) for one specific item (itemtofind)

        ' Stops false discovery -  if the list is 'Frederick,Kevin',  then searching for 'Fred' will return false
        inlistfull = Trim(inlistfull) & ","
        itemtofind = Trim(itemtofind) & ","


        If InStr(UCase(inlistfull), UCase(itemtofind)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


End Module



