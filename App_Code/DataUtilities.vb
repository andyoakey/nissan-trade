Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Net.Mail
Imports QubeUtils

'/******************************************************************************************************************/'
'/  Put All Data Related Code In Here - try to keep non-project specific so can be raised to class level eventually /'
'/******************************************************************************************************************/'

Public Module DataUtilities

    Public Sub RunNonQueryCommand(ByVal sSql As String)

        Try

            Dim sCommand As New SqlCommand
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sCommand.ExecuteNonQuery()
            sCommand.Dispose()
            mConnection.Close()

        Catch ex As Exception

            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "RunNonQueryCommand " + sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try


    End Sub

    Public Function GetDataString(ByVal sSql As String) As String

        Dim sReturnValue As String = ""

        Try
            ' Returns A Single Character String For A Given sql String
            ' Sql string must return one column, one row
            Dim sCommand As New SqlCommand
            Dim sReader As SqlDataReader
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sCommand.CommandTimeout = 3000000
            sReader = sCommand.ExecuteReader
            While sReader.Read()
                sReturnValue = sReader(0).ToString
            End While
            mConnection.Close()

        Catch ex As Exception
            sReturnValue = ""
            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "GetDataString " & sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try

        Return sReturnValue

    End Function

    Public Function GetDataDecimal(ByVal sSql As String) As Decimal

        Dim nReturnValue As Decimal = 0

        Try

            ' Returns A Single Numeric Value For A Given sql String
            ' Sql string must return one column, one row
            Dim sCommand As New SqlCommand
            Dim sReader As SqlDataReader
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sCommand.CommandTimeout = 3000000
            sReader = sCommand.ExecuteReader()
            While sReader.Read()
                If Microsoft.VisualBasic.IsDBNull(sReader(0)) = False Then
                    nReturnValue = sReader(0)
                Else
                    nReturnValue = 0
                End If

            End While
            mConnection.Close()

        Catch ex As Exception

            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "GetDataDecimal " & sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try
        Return nReturnValue

    End Function

    Public Function GetDataLong(ByVal sSql As String) As Long

        Dim nReturnValue As Long = 0

        Try

            ' Returns A Single Numeric Value For A Given sql String
            ' Sql string must return one column, one row
            Dim sCommand As New SqlCommand
            sCommand.CommandTimeout = 3000000
            Dim sReader As SqlDataReader
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sReader = sCommand.ExecuteReader()
            While sReader.Read()
                If Microsoft.VisualBasic.IsDBNull(sReader(0)) = False Then
                    nReturnValue = sReader(0)
                Else
                    nReturnValue = 0
                End If
            End While
            mConnection.Close()

        Catch ex As Exception

            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "GetDataLong " & sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try

        Return nReturnValue

    End Function

    Public Function GetDataDate(ByVal sSql As String) As Date

        Dim nReturnValue As Date

        Try

            ' Returns A Single Numeric Value For A Given sql String
            ' Sql string must return one column, one row
            Dim sCommand As New SqlCommand
            Dim sReader As SqlDataReader
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sReader = sCommand.ExecuteReader()
            While sReader.Read()
                If Microsoft.VisualBasic.IsDBNull(sReader(0)) = False Then
                    nReturnValue = sReader(0)
                Else
                    nReturnValue = Now
                End If
            End While
            mConnection.Close()

        Catch ex As Exception

            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "GetDataDate " & sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try

        Return nReturnValue

    End Function

    Public Function DataExists(ByVal sSql As String) As Boolean

        Dim bReturnValue As Boolean = False

        Try

            ' Returns A Boolean Value For A Given sql String - false if string returns 0 records, true if 1 or more
            Dim sCommand As New SqlCommand
            Dim sReader As SqlDataReader
            Dim mConnection As New SqlConnection
            mConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
            mConnection.Open()
            sCommand.CommandTimeout = 3000000
            sCommand.Connection = mConnection
            sCommand.CommandText = sSql
            sReader = sCommand.ExecuteReader()
            While sReader.Read()
                bReturnValue = True
            End While
            mConnection.Close()

        Catch ex As Exception

            SendEmail(ex.Message)
            HttpContext.Current.Session("ErrorToDisplay") = ex.Message
            HttpContext.Current.Session("CallingModule") = "DataExists " & sSql
            HttpContext.Current.Response.Redirect("dbError.aspx")

        End Try

        Return bReturnValue

    End Function

    Public Function FormatAddress(ByVal sAddress1 As String, ByVal sAddress2 As String, ByVal sAddress3 As String, ByVal sAddress4 As String, ByVal sAddress5 As String, ByVal sPostCode As String, ByVal bUsePostCode As Boolean) As String
        Dim sReturnString As String = ""
        If sAddress1 <> "" Then
            sReturnString = Trim("" & pCase(sAddress1))
        End If
        If sAddress2 <> "" Then
            sReturnString += ", " & Trim("" & pCase(sAddress2))
        End If
        If sAddress3 <> "" Then
            sReturnString += ", " & Trim("" & pCase(sAddress3))
        End If
        If sAddress4 <> "" Then
            sReturnString += ", " & Trim("" & pCase(sAddress4))
        End If
        If sAddress5 <> "" Then
            sReturnString += ", " & Trim("" & pCase(sAddress5))
        End If
        If bUsePostCode = True Then
            sReturnString += " " & Trim("" & UCase(sPostCode))
        End If
        Return sReturnString
    End Function

    Private Sub SendEmail(ByVal sError As String, Optional ByVal bv_sStoredProcedure As String = "")
		If Settings.GetBool("IsLive") Then
			If InStr(sError, "Timeout") > -1 Then Exit Sub
			Try

				Dim MailObj As New SmtpClient
				Dim mm As New MailMessage
				Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")
				basicAuthenticationInfo.UserName = "support@toyotavaluechain.net"
				basicAuthenticationInfo.Password = "4AhK56DmpGvn"
				MailObj.Host = "mail.toyotavaluechain.net"
				MailObj.UseDefaultCredentials = False
				MailObj.Credentials = basicAuthenticationInfo

				mm.From = New MailAddress("developers@qubedata.net")
				mm.To.Add("errors@qubedata.net")
				mm.Subject = "dev.nissantrade.net - Crash!"
				mm.Body = "Dear Developers," & vbCrLf & vbCrLf
				mm.Body += "SendingPage " & HttpContext.Current.Session("SendingPage") & " has created a database error in the " & HttpContext.Current.Session("CrashFunc") & " databaseaccess code :" & vbCrLf & vbCrLf
				mm.Body += sError & vbCrLf & vbCrLf
				mm.Body += "Connectionstring is : " & System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString & vbCrLf & vbCrLf
				mm.Body += "CurrentFunction is : " & HttpContext.Current.Session("CurrentFunction") & vbCrLf & vbCrLf
				mm.Body += "The web page in question was " & HttpContext.Current.Session("SendingPage") & vbCrLf & vbCrLf
				mm.Body += "This is an automated email from TOYOTA/SQLDataBaseAccess, generated when SQL was accessed by User : "
				mm.Body += HttpContext.Current.Session("UserId") & " on " & Now()
				MailObj.Send(mm)
			Catch ex As Exception
				HttpContext.Current.Session("ErrorToDisplay") = ex.Message
				HttpContext.Current.Session("CallingModule") = "SendMail " & sError
				HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
			End Try
		End If
    End Sub

    Public Function FormatName(ByVal sName As String, ByVal sTitle As String, ByVal sInitials As String, ByVal sFirstname As String, ByVal sSurname As String) As String

        Dim sReturnString As String = ""

        If Trim(sName) <> "" Then
            sReturnString = sName
        Else

            If sTitle <> "" Then
                sReturnString = Trim("" & pCase(sTitle))
            End If

            If sInitials <> "" Then
                sReturnString += ", " & Trim("" & (sInitials))
            End If

            If sFirstname <> "" Then
                sReturnString += ", " & Trim("" & pCase(sFirstname))
            End If

            If sSurname <> "" Then
                sReturnString += ", " & Trim("" & pCase(sSurname))
            End If

        End If

        Return sReturnString

    End Function

    Function TextDate(ByVal dInDate As Date) As String
        Return Format(dInDate, "dd MMM yyyy")
    End Function

    Function TextDateMDY(ByVal dInDate As Date) As String
        Return Format(dInDate, "MMM dd yyyy")
    End Function
    Function TextDateSlash(ByVal dInDate As Date) As String
        Return Format(dInDate, "dd/MM/yyyy")
    End Function
    Function TextDateMY(ByVal dInDate As Date) As String
        Return Format(dInDate, "MMM-yy")
    End Function
    Function TextDateFull(ByVal dInDate As Date) As String
        ' Returns eg Wednesday 03 Dec 2008
        Return Format(dInDate, "dddd dd MMMM yyyy")
    End Function
    Function TextDateMonth(ByVal dInDate As Date) As String
        Return Left(MonthName(Month(dInDate)), 3)
    End Function
    Function TextDateFileName(ByVal dInDate As Date) As String
        'Returns a date in 2006-06-15 format for use in filenames.
        Dim sMonthName As String
        Dim sDayName As String
        Dim nSaveDay As Integer = 0

        If Month(dInDate) < 10 Then
            sMonthName = "0" & Month(dInDate)
        Else
            sMonthName = Month(dInDate)
        End If

        nSaveDay = Microsoft.VisualBasic.DateAndTime.Day(dInDate)
        If nSaveDay < 10 Then
            sDayName = "0" & Trim(Str(nSaveDay))
        Else
            sDayName = Trim(Str(nSaveDay))
        End If
        Return Trim(Str(Year(dInDate))) & sMonthName & sDayName
    End Function
    Function TextDateFileNameHMS(ByVal dInDate As Date) As String
        'Returns a date in 2006-06-15 format for use in filenames.
        Dim sMonthName As String
        Dim sDayName As String
        Dim nSaveDay As Integer = 0

        If Month(dInDate) < 10 Then
            sMonthName = "0" & Month(dInDate)
        Else
            sMonthName = Month(dInDate)
        End If

        nSaveDay = Microsoft.VisualBasic.DateAndTime.Day(dInDate)
        If nSaveDay < 10 Then
            sDayName = "0" & Trim(Str(nSaveDay))
        Else
            sDayName = Trim(Str(nSaveDay))
        End If

        Dim xMonth As String
        Dim xDay As String

        If DatePart(DateInterval.Month, dInDate) < 10 Then
            xMonth = "0" & DatePart(DateInterval.Month, dInDate)
        Else
            xMonth = DatePart(DateInterval.Month, dInDate)
        End If

        If DatePart(DateInterval.Day, dInDate) < 10 Then
            xDay = "0" & DatePart(DateInterval.Day, dInDate)
        Else
            xDay = DatePart(DateInterval.Day, dInDate)
        End If

        Return DatePart(DateInterval.Year, dInDate) & "_" & xMonth & "_" & xDay & "_" & DatePart(DateInterval.Hour, dInDate) & DatePart(DateInterval.Minute, dInDate) & DatePart(DateInterval.Second, dInDate)
    End Function

    Function TextTime(ByVal dInDate As Date) As String
        Return Format(dInDate, "hh:mm:ss")
    End Function


End Module

