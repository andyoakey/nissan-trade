﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Web
Imports QubeUtils

Public Class DatabaseAccess

    Public Function ConnectionString() As String
        ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Function

    Public Function Read(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedureName As String) As DataSet

        ' simple version of a database read, no parameters, so a null set is created and passed to the main read function
        HttpContext.Current.Session("CrashFunc") = "Read1"
        Dim oInputParams As Hashtable = Nothing
        Dim oOutputParams As Hashtable = Nothing
        Try
            br_sErrorMessage = ""
            ' try and make the call
            Return Read(br_sErrorMessage, bv_sStoredProcedureName, oInputParams, oOutputParams)
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = FormatErrorMessage(e, oInputParams, oOutputParams)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try

    End Function

    Public Function Read(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedureName As String, ByVal bv_oInputParams As Hashtable) As DataSet
        'database read, input parameters only passed to the main read function
        HttpContext.Current.Session("CrashFunc") = "Read2"
        Dim oOutputParams As Hashtable = Nothing
        Try
            br_sErrorMessage = ""
            ' try and make the call
            Return Read(br_sErrorMessage, bv_sStoredProcedureName, bv_oInputParams, oOutputParams)
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = FormatErrorMessage(e, bv_oInputParams, oOutputParams)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Read(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedureName As String, ByVal bv_oInputParams As Hashtable, ByVal bv_oOutputParams As Hashtable) As DataSet

        ' This is the main read function that converts hashtable parameters to sql params and sorts out output parameters

        HttpContext.Current.Session("CrashFunc") = "Read3"
        Dim oSQLConnection As New SqlConnection(Me.COnnectionString)

        Try
            br_sErrorMessage = ""
            Dim oSQLCommand As New SqlCommand(bv_sStoredProcedureName, oSQLConnection)
            Dim oSQLDataAdapter As New SqlDataAdapter
            Dim oDataSet As New DataSet

            oSQLCommand.CommandType = CommandType.StoredProcedure
            oSQLCommand.CommandTimeout = 0

            Call PopulateCommand(bv_oInputParams, bv_oOutputParams, oSQLCommand)

            oSQLDataAdapter.SelectCommand = oSQLCommand
            oSQLConnection.Open()
            'Dim lRows = oSQLCommand.ExecuteNonQuery()
            oSQLDataAdapter.Fill(oDataSet)
            ProcessOutputParams(bv_oOutputParams, oSQLCommand)
            oSQLConnection.Close()
            Return oDataSet

        Catch e As SqlException
            br_sErrorMessage = FormatErrorMessage(e, bv_oInputParams, bv_oOutputParams)
            oSQLConnection.Close()
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing

        Catch e As Exception
            oSQLConnection.Close()
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try
    End Function

    Public Function Update(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedure As String) As Long
        ' performs updates at the database, no result set no input or output params
        HttpContext.Current.Session("CrashFunc") = "Update1"
        Dim oInputParams As Hashtable = Nothing
        Dim oOutputParams As Hashtable = Nothing
        Try
            br_sErrorMessage = ""
            Return Update(br_sErrorMessage, bv_sStoredProcedure, oInputParams, oOutputParams)
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = FormatErrorMessage(e, oInputParams, oOutputParams)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try
    End Function

    Public Function Update(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedure As String, ByVal bv_oInputParams As Hashtable) As Long
        ' performs updates at the database, no result set, input but no output paramss
        HttpContext.Current.Session("CrashFunc") = "Update2"
        Dim oOutputParams As Hashtable = Nothing
        Try
            Return Update(br_sErrorMessage, bv_sStoredProcedure, bv_oInputParams, oOutputParams)
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = FormatErrorMessage(e, bv_oInputParams, oOutputParams)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try
    End Function

    Public Function Update(ByRef br_sErrorMessage As String, ByVal bv_sStoredProcedure As String, ByVal bv_oInputParams As Hashtable, ByVal bv_oOutputParams As Hashtable) As Long
        ' performs updates at the database, result set, input and output params
        HttpContext.Current.Session("CrashFunc") = "Update3"
        Dim oSQLConnection As New SqlConnection(Me.COnnectionString)
        Try
            br_sErrorMessage = ""
            ' new command object
            Dim oSQLCommand As New SqlCommand
            ' open the connection
            oSQLConnection.Open()
            'associate the connection with the command
            oSQLCommand.Connection = oSQLConnection
            oSQLCommand.CommandText = bv_sStoredProcedure
            oSQLCommand.CommandType = CommandType.StoredProcedure
            'process the input params
            PopulateCommand(bv_oInputParams, bv_oOutputParams, oSQLCommand)
            ' execute and get number of rows affected
            Dim lRows As Long = oSQLCommand.ExecuteNonQuery
            ProcessOutputParams(bv_oOutputParams, oSQLCommand)
            'close off the connection
            oSQLConnection.Close()
            Return lRows
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = FormatErrorMessage(e, bv_oInputParams, bv_oOutputParams)
            oSQLConnection.Close()
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            oSQLConnection.Close()
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try
    End Function

    Private Sub PopulateCommand(ByVal bv_oInputParams As Hashtable, ByVal bv_oOutputParams As Hashtable, ByVal bv_oSQLCommand As SqlCommand)
        ' append params to SQLCommand object
        Try
            Dim oParam As DictionaryEntry
            If Not (bv_oInputParams Is Nothing) Then
                ' do the input params
                For Each oParam In bv_oInputParams
                    Dim oSQLParameter As New SqlParameter
                    oSQLParameter.ParameterName = oParam.Key
                    oSQLParameter.Value = oParam.Value
                    oSQLParameter.Direction = ParameterDirection.Input
                    bv_oSQLCommand.Parameters.Add(oSQLParameter)
                Next oParam
            End If
            If Not (bv_oOutputParams Is Nothing) Then
                ' and the output params
                For Each oParam In bv_oOutputParams
                    Dim oSQLParameter As New SqlParameter
                    oSQLParameter.ParameterName = oParam.Key
                    oSQLParameter.Value = oParam.Value
                    oSQLParameter.Direction = ParameterDirection.Output
                    ' added in to test return params
                    ' TODO: check each return param and create appropriate DB type
                    oSQLParameter.SqlDbType = SqlDbType.VarChar
                    oSQLParameter.Size = 255
                    bv_oSQLCommand.Parameters.Add(oSQLParameter)
                Next oParam
            End If
        Catch e As Exception
            ' something went wrong
            Throw New ApplicationException(e.Message, e.InnerException)
        End Try
    End Sub

    Private Sub ProcessOutputParams(ByVal bv_oOutputParams As Hashtable, ByVal bv_oSQLCommand As SqlCommand)
        'process the output params after a call and repopulate the output params hashtable
        Try
            If Not (bv_oOutputParams Is Nothing) Then
                Dim oSQLParam As SqlParameter
                ' find all of the output parameters
                For Each oSQLParam In bv_oSQLCommand.Parameters
                    If oSQLParam.Direction = ParameterDirection.Output Then
                        'update the hashtable
                        bv_oOutputParams(oSQLParam.ParameterName) = oSQLParam.Value
                    End If
                Next
            End If
        Catch e As Exception
            ' something went wrong
            Throw New ApplicationException(e.Message, e.InnerException)
        End Try
    End Sub

    Private Function FormatErrorMessage(ByVal e As Exception) As String
        ' return a nicely formatted exception
        Return e.Message & ":" + e.Source '& ":" & e.StackTrace.Trim()
    End Function

    Private Function FormatErrorMessage(ByVal e As SqlException, ByVal bv_oInputParams As Hashtable, ByVal bv_oOutputParams As Hashtable) As String
        ' return a nicely formatted exception
        Dim sInput As String = " There were no input parameters."
        Dim sOutput As String = " There were no output parameters."
        Dim oParam As DictionaryEntry
        ' format the input params
        If Not (bv_oInputParams Is Nothing) Then
            If bv_oInputParams.Count > 0 Then
                sInput = "; Input parameters were...; "
                For Each oParam In bv_oInputParams
                    sInput = sInput & oParam.Key & " = " & oParam.Value & "; "
                Next
            End If
        End If
        ' and now format the output params
        If Not (bv_oOutputParams Is Nothing) Then
            If bv_oOutputParams.Count > 0 Then
                sOutput = "; Output parameters...; "
                For Each oParam In bv_oOutputParams
                    sOutput = sOutput & oParam.Key & " = " & oParam.Value & "; "
                Next
            End If
        End If
        Dim eSQLError As String = "SQL Error " & e.Number & " occurred in " & e.Source & " in SP '" & e.Procedure & "'.; " & e.Message & sInput & sOutput & e.Source
        Return eSQLError
    End Function

    Private Sub SendEmail(ByVal sError As String, Optional ByVal bv_sStoredProcedure As String = "")
        If InStr(sError, "Timeout") > -1 Then Exit Sub
		
		HttpContext.Current.Response.Redirect("~/dbError.aspx", False)

    End Sub

    Public Function ExecuteSQL(ByRef br_sErrorMessage As String, ByVal bv_sSQL As String) As DataSet
        Try
            br_sErrorMessage = ""
            ' Executes a piece of SQL and returns the result sets in a dataset of datatables
            Dim oDS As New DataSet
            ' connect to the database and get results
            Dim oDA As New SqlDataAdapter(bv_sSQL, Me.ConnectionString)
            ' fill dataset with results
            oDA.Fill(oDS)
            ' return it
            Return oDS
        Catch e As SqlException
            ' we've caught a SQL exception
            br_sErrorMessage = "SQL was - " + bv_sSQL + vbCrLf + "Error was - " + FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        Catch e As Exception
            ' general application error
            br_sErrorMessage = FormatErrorMessage(e)
            SendEmail(br_sErrorMessage)
            HttpContext.Current.Session("ErrorToDisplay") = br_sErrorMessage
            HttpContext.Current.Response.Redirect("~/dbError.aspx", False)
            Return Nothing
        End Try
    End Function

End Class