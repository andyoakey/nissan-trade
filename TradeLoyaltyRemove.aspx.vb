﻿Imports System.Data
Imports System.Net.Mail

Partial Class TradeLoyaltyRemove

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, dsC As DataSet, sErr, sSQL As String
    Dim sDealerCode As String
    Dim sAccountName As String
    Dim sAccountPostCode As String
    Dim nTradeLoyaltyMemberId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call FetchUserDetails()
    End Sub

    Sub FetchUserDetails()

        sSQL = "SELECT TOP 1 * FROM v_TradeLoyaltyMembers WHERE TradeLoyaltyMemberId = " & Trim(Str(Request.QueryString(0)))
        ds = da.ExecuteSQL(sErr, sSQL)

        btnRemove.Enabled = False

        If ds.Tables(0).Rows.Count = 1 Then
            With ds.Tables(0).Rows(0)
                lblRegistration.Text = "Click Remove to remove registration : " & ds.Tables(0).Rows(0).Item("AccountName") & ", " & ds.Tables(0).Rows(0).Item("AccountPostCode")
                sDealerCode = ds.Tables(0).Rows(0).Item("DealerCode")
                sAccountName = ds.Tables(0).Rows(0).Item("AccountName")
                sAccountPostCode = ds.Tables(0).Rows(0).Item("AccountPostCode")
                btnRemove.Enabled = True
            End With
        End If

    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        Dim sSQL = "EXEC p_TradeLoyalty_Del " & Trim(Str(Request.QueryString(0)))
        Call RunNonQueryCommand(sSQL)
        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Trade Registration removed", "Reg: " & Trim(sDealerCode) & " / " & sAccountName & "/" & sAccountPostCode & " removed")

        Dim startUpScript As String = String.Format("window.parent.HideRegDelWindow();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)

    End Sub

End Class
