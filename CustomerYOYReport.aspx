<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CustomerYOYReport.aspx.vb" Inherits="CustomerYOYReport" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
               <dx:ASPxLabel 
                    ID="lblPageTitle" 
                    Font-Size="Larger"
                    runat ="server" 
                    Text="Customer Performance Year on Year"  />
        </div>

        <br />

        <table style="position: relative; margin-bottom: 10px;" Width="100%">
          
              <tr id="Tr6" runat="server">
                <td style="width:15%" align="left" >
                       <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlTA"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="Total Spend" Value="0" Selected="true" />
                            <dx:ListEditItem Text="Accessories" Value="1"/>
                            <dx:ListEditItem Text="Additional Product" Value="2"/>
                            <dx:ListEditItem Text="Damage" Value="3"/>
                            <dx:ListEditItem Text="Extended Maintenance" Value="4"/>
                            <dx:ListEditItem Text="Mechanical Repair" Value="5"/>
                            <dx:ListEditItem Text="Routine Maintenance" Value="6"/>
                        </Items>
                    </dx:ASPxComboBox>
                </td>
            
                <td style="width:15%" align="left" >
                       <dx:ASPxComboBox
                        runat="server" 
                        ID="ddlType"
                        AutoPostBack="True">
                        <Items>
                            <dx:ListEditItem Text="All Customers" Value="0" Selected="true"/>
                            <dx:ListEditItem Text="Focus only" Value="1" />
                            <dx:ListEditItem Text="Excluding Focus" Value="2"/>
                         </Items>
                    </dx:ASPxComboBox>
                </td>

                <td style="width:60%" align="left" >

                </td>
      
                <td style="width:10%" align="right" >
                        <dx:ASPxButton 
            ID="btnClearFilter" 
            runat="server" 
            visible="false"
            Text="Clear Filters" 
            AutoPostBack="true">
        </dx:ASPxButton>
                </td>

            </tr>

        </table> 

        <dx:ASPxGridView 
            ID="gridCustomers" 
            runat="server" 
            OnLoad="GridStyles" 
            style="position:relative;"
            Width="100%" 
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode"
            OnCustomColumnDisplayText="gridCustomers_OnCustomColumnDisplayText" >
       
            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="16"  />
            
            <Settings
                ShowFooter="True"  
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False"
                ShowGroupFooter="Hidden"
                ShowFilterRow="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" 
                AllowDragDrop="False" 
                AllowGroup="False" />
            
            <Columns>
                
                <dx:GridViewDataTextColumn Caption="Name" FieldName="BusinessName" VisibleIndex="1" Width="20%" ExportWidth="300">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Postcode" FieldName="PostCode" VisibleIndex="2" Width="10%" ExportWidth="100">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle  HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="In Territory" FieldName="InCDA" VisibleIndex="3" Width="10%" ExportWidth="100">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle  HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Focus" FieldName="focustext" VisibleIndex="3" Width="10%" ExportWidth="100">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle  HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn Caption="Business Type" FieldName="BusinessTypeDescription" VisibleIndex="4" Width="20%" ExportWidth="300">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle  HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Spend This Year"  FieldName="SpendThisYear" VisibleIndex="5" ExportWidth="200">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center" >
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Total Spend Last Year" FieldName="SpendLastYear" VisibleIndex="6" ExportWidth="200">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" 
                        ShowFilterRowMenu="False" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                    <CellStyle HorizontalAlign="Center" >
                    </CellStyle>
                    <FooterCellStyle HorizontalAlign="Center">
                    </FooterCellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Status" FieldName="AccountStatus" VisibleIndex="7" Width="10%" ExportWidth="150">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 1" FieldName="Address1"
                    VisibleIndex="8" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 2" FieldName="Address2"
                    VisibleIndex="9" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 3" FieldName="Address3"
                    VisibleIndex="10" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 4" FieldName="Address4"
                    VisibleIndex="11" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 5" FieldName="Address5"
                    VisibleIndex="12" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Address 6" FieldName="Address6"
                    VisibleIndex="13" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="PostCode" FieldName="PostCode"
                    VisibleIndex="14" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Telephone Number" FieldName="TelephoneNumber"
                    VisibleIndex="15" Visible="false" ExportWidth="200">
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Safe To Mail" FieldName="SafeToMail"
                    VisibleIndex="16" Visible="false" ExportWidth="100">
                </dx:GridViewDataTextColumn>
                
                <dx:GridViewDataTextColumn Caption="Safe To Phone" FieldName="SafeToPhone"
                    VisibleIndex="17" Visible="false" ExportWidth="100">
                </dx:GridViewDataTextColumn>
                
            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="Total Customers: {0}" FieldName="BusinessName" SummaryType="Count" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="SpendThisYear" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="SpendLastYear" SummaryType="Sum" />
            </TotalSummary>

        </dx:ASPxGridView>
        

        <br />
     
        <br />
        <br />

        <table>
            <tr>
                <td width ="25%">


                </td>

                <td width ="50%" align="center">
                         <dx:ASPxLabel  Width="100%" ForeColor="Red"
        ID="lblCentreError" 
        Visible="false"
        runat="server" 
        Font-Size="Larger"
        Text="This Report is only available when a single Dealer is selected." />							
                </td>

                <td width ="25%">


                </td>
            </tr>


        </table>
        

    </div>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCustomers" PreserveGroupRowStates="False">
    </dx:ASPxGridViewExporter>

</asp:Content>