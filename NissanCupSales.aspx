﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NissanCupSales.aspx.vb" Inherits="NissanCupSales" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div style="position: relative; font-family: Calibri; text-align: left;" >

      <div id="divTitle" style="position:relative;">
           <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="Nissan Cup Sales" />
        </div>

         <br />

        <dx:ASPxGridView  
        ID="grid" 
        CssClass="grid_styles"
        OnLoad="GridStyles" style="position:relative;"
        runat="server" 
        AutoGenerateColumns="False"
        DataSourceID="dsCupSales" 
        Styles-Header-HorizontalAlign="Center"
        Styles-CellStyle-HorizontalAlign="Center"
        Width=" 100%">
        <SettingsBehavior AllowSort="False" ColumnResizeMode="Control"  />
            
        <SettingsPager AlwaysShowPager="false" PageSize="18" AllButton-Visible="true">
        </SettingsPager>
 
        <Settings 
            ShowFilterRow="False" 
            ShowFilterRowMenu="False" 
            ShowGroupPanel="False" 
            ShowFooter="False"
            ShowTitlePanel="False" />
                      
        <Columns>
            <dx:GridViewDataTextColumn
                VisibleIndex="0" 
                Settings-AllowSort="False"
                Settings-AllowGroup="False"
                Settings-AllowHeaderFilter="True"
                Settings-HeaderFilterMode="CheckedList"
                Width="22%" 
                exportwidth="250"
                Caption="Dealer" 
                FieldName="dealer">
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="1" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Oct 2015" 
                FieldName="oct15">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="2" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Oct 2016" 
                FieldName="oct16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="3" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Nov 2015" 
                FieldName="nov15">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="4" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Nov 2016" 
                FieldName="nov16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="5" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Dec 2015" 
                FieldName="dec15">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="6" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Dec 2016" 
                FieldName="dec16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="7" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Jan 2016" 
                FieldName="jan16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="8" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Jan 2017" 
                FieldName="jan17">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

           <dx:GridViewDataTextColumn 
                VisibleIndex="9" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Feb 2016" 
                FieldName="feb16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="10" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Feb 2017" 
                FieldName="feb17">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="11" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Mar 2016" 
                FieldName="mar16">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn 
                VisibleIndex="12" 
                Settings-AllowSort="False"
                Width="6%" 
                exportwidth="70"
                Caption="Mar 2017" 
                FieldName="mar17">
                <PropertiesTextEdit DisplayFormatString="&#163;##,##0.00"/>
            </dx:GridViewDataTextColumn>

         </Columns>
        
    </dx:ASPxGridView>

        <asp:SqlDataSource 
            ID="dsCupSales" 
            runat="server" 
            ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
            SelectCommand="sp_CupSales" 
            SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
   

    
        <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        GridViewID="grid"
        runat="server">
    </dx:ASPxGridViewExporter>
    </div>

</asp:Content>

