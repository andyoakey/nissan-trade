﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="VisitActionAddNew.aspx.vb" Inherits="VisitActionAddNew" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px; padding-bottom:10px;">
            <table width="968px">
                <tr>
                    <td align="left">
                        <asp:Label ID="lblPageTitle" runat="server" Text="Visit Actions" 
                            style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td align="right" >
                    </td>
                </tr>
            </table>
        </div>

        <table width="540px" style="position:relative; left:5px;">
                    
            <tr>
                <td >
                    <asp:Label ID="Label1" runat="server" Text="Select Centre : "></asp:Label>
                </td>
                <td style="padding-left:3px;">

                    <dxe:ASPxComboBox ID="ddlVisitCentre" runat="server" Width="310px" DropDownWidth="550"
                        DropDownStyle="DropDown" DataSourceID="dsCentres" ValueField="DealerCode"
                        ValueType="System.String" TextFormatString="{0} - {1}" EnableCallbackMode="true" IncrementalFilteringMode="StartsWith"
                        CallbackPageSize="30">
                        <Columns>
                            <dxe:ListBoxColumn FieldName="DealerCode" Width="130px" />
                            <dxe:ListBoxColumn FieldName="CentreName" Width="100%" />
                        </Columns>
                    </dxe:ASPxComboBox>

                </td>
            </tr>

            <tr>
                <td style="width:30%">
                    <asp:Label ID="Label3" runat="server" Text="Select Date : "></asp:Label>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <dxe:ASPxDateEdit Id="txtVisitDate" runat="server" Width="120px">
                                </dxe:ASPxDateEdit>
                            </td>
                            <td align="right">
                                <dxe:ASPxButton ID="btnNewVisitSave" ClientInstanceName="btnNewVisit" runat="server" Text="Save" Width="90px" ImagePosition="Top">
                                </dxe:ASPxButton>
                            </td>
                            <td align="right">
                                <dxe:ASPxButton ID="btnCancel" ClientInstanceName="btnCancel" runat="server" Text="Cancel" Width="90px" ImagePosition="Top">
                                </dxe:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>

    </div>

    <asp:SqlDataSource ID="dsCentres" runat="server" SelectCommand="p_CentreList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>











