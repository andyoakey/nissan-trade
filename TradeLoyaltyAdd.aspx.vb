﻿Imports System.Data
Imports System.Net.Mail
Imports DevExpress.Web

Partial Class TradeLoyaltyAdd

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess
    Dim sErr, sSQL As String
    Dim nTradeLoyaltyMemberId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        nTradeLoyaltyMemberId = Request.QueryString(0)
        If Not IsPostBack Then
            Call GetMonthsComplete(ddlStartPeriod)
            Call FetchTradeLoyaltyMemberDetails()
        End If
    End Sub

    Sub FetchTradeLoyaltyMemberDetails()

        Dim sLinkType As String = ""
        Dim sLink As String = ""
        Dim dsMembers As DataSet
        Dim ds As DataSet
        Dim i As Integer

        If nTradeLoyaltyMemberId <> 0 Then

            sSQL = "SELECT TOP 1 * FROM v_TradeLoyaltyMembers WHERE TradeLoyaltyMemberId = " & nTradeLoyaltyMemberId
            ds = da.ExecuteSQL(sErr, sSQL)

            If ds.Tables(0).Rows.Count = 1 Then
                With ds.Tables(0).Rows(0)
                    txtDealerCode.Text = .Item("DealerCode")
                    txtDealerName.Text = .Item("CentreName")
                    txtOwnerDealerCode.Text = .Item("OwnerDealerCode")
                    txtOwnerDealerName.Text = GetDataString("SELECT CentreName FROM VALUE_CHAIN_LIVE..CentreMaster WHERE DealerCode = '" & txtOwnerDealerCode.Text & "'")
                    txtAccountName.Text = .Item("AccountName")
                    txtAccountPostCode.Text = .Item("AccountPostCode")
                    txtRebate.Text = .Item("Rebate") * 100
                    txtQualifier.Text = .Item("RebateDiscountQualifier") * 100
                    ddlStartPeriod.SelectedValue = .Item("StartPeriod")
                End With
                txtDealerCode.Enabled = False
                txtDealerName.Enabled = False
                txtOwnerDealerCode.Enabled = False
                txtOwnerDealerName.Enabled = False

                sSQL = "SELECT * FROM TradeLoyaltyMemberLinks WHERE TradeLoyaltyMemberId = " & nTradeLoyaltyMemberId
                dsMembers = da.ExecuteSQL(sErr, sSQL)
                For i = 0 To dsMembers.Tables(0).Rows.Count - 1

                    sLinkType = dsMembers.Tables(0).Rows(i).Item(1)
                    sLink = dsMembers.Tables(0).Rows(i).Item(2)

                    If i = 0 Then
                        ddlLink1.SelectedValue = sLinkType
                        txtLink1.Text = sLink
                        lblLink1.Text = GetElementName(ddlLink1, txtLink1.Text)
                    End If
                    If i = 1 Then
                        ddlLink2.SelectedValue = sLinkType
                        txtLink2.Text = sLink
                        lblLink2.Text = GetElementName(ddlLink2, txtLink2.Text)
                    End If
                    If i = 2 Then
                        ddlLink3.SelectedValue = sLinkType
                        txtLink3.Text = sLink
                        lblLink3.Text = GetElementName(ddlLink3, txtLink3.Text)
                    End If
                    If i = 3 Then
                        ddlLink4.SelectedValue = sLinkType
                        txtLink4.Text = sLink
                        lblLink4.Text = GetElementName(ddlLink4, txtLink4.Text)
                    End If
                    If i = 4 Then
                        ddlLink5.SelectedValue = sLinkType
                        txtLink5.Text = sLink
                        lblLink5.Text = GetElementName(ddlLink5, txtLink5.Text)
                    End If
                Next

            End If

        End If

    End Sub

    Protected Sub btnSaveReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReg.Click

        Dim bErr As Boolean = False
        Dim htIn As New Hashtable
        Dim sMessage As String = ""

        lblErr.Visible = False
        lblErr.Text = ""

        If Not bErr And (Trim(txtAccountName.Text)) = "" Then
            lblErr.Text = "An account name must be entered."
            bErr = True
        End If

        If Not bErr And (Trim(txtAccountPostCode.Text)) = "" Then
            lblErr.Text = "An account postcode must be entered."
            bErr = True
        End If

        If Not bErr And (Trim(txtLink1.Text)) = "" _
                       And (Trim(txtLink2.Text)) = "" _
                       And (Trim(txtLink3.Text)) = "" _
                       And (Trim(txtLink4.Text)) = "" _
                       And (Trim(txtLink5.Text)) = "" Then
            lblErr.Text = "An account code, or company magic number or target magic number is required."
            bErr = True
        End If

        If Not bErr And (Trim(txtRebate.Text) = "" Or Val(txtRebate.Text) <= 0) Then
            lblErr.Text = "A valid rebate must be entered."
            bErr = True
        End If

        If Not bErr And (Trim(txtQualifier.Text) = "" Or Val(txtQualifier.Text) <= 0) Then
            lblErr.Text = "A valid qualifier must be entered."
            bErr = True
        End If

        If Not bErr Then
            If nTradeLoyaltyMemberId = 0 Then
                If AccountExists() Then
                    lblErr.Text = "This registration already exists."
                    bErr = True
                End If
            End If
        End If

        If Not bErr Then
            If txtOwnerDealerCode.Text = "" Then
                lblErr.Text = "Pick an Owner Dealer Code."
                bErr = True
            End If
        End If

        If Not bErr Then
            If txtOwnerDealerCode.Text <> "" And txtOwnerDealerName.Text = "" Then
                lblErr.Text = "Pick a Trade Centre who owns this record."
                bErr = True
            End If
        End If

        If Not bErr Then

            htIn.Add("@nTradeLoyaltyMemberId", nTradeLoyaltyMemberId)
            htIn.Add("@sDealerCode", Trim(txtDealerCode.Text))
            htIn.Add("@sAccountName", txtAccountName.Text)
            htIn.Add("@sAccountPostCode", txtAccountPostCode.Text)
            htIn.Add("@sLinkType1", ddlLink1.SelectedValue)
            htIn.Add("@sLink1", txtLink1.Text)
            htIn.Add("@sLinkType2", ddlLink2.SelectedValue)
            htIn.Add("@sLink2", txtLink2.Text)
            htIn.Add("@sLinkType3", ddlLink3.SelectedValue)
            htIn.Add("@sLink3", txtLink3.Text)
            htIn.Add("@sLinkType4", ddlLink4.SelectedValue)
            htIn.Add("@sLink4", txtLink4.Text)
            htIn.Add("@sLinkType5", ddlLink5.SelectedValue)
            htIn.Add("@sLink5", txtLink5.Text)
            htIn.Add("@nRebate", Val(txtRebate.Text))
            htIn.Add("@nRebateDiscountQualifier", Val(txtQualifier.Text))
            htIn.Add("@nStartPeriod", ddlStartPeriod.SelectedItem.Value)
            htIn.Add("@sOwnerDealerCode", txtOwnerDealerCode.Text)
            da.Update(sErr, "p_TradeLoyalty_Member_Ins", htIn)

            If sErr.Length > 0 Then
                HttpContext.Current.Session("sErr") = sErr : HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            Else
                If nTradeLoyaltyMemberId = 0 Then
                    Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Trade Registration added", "New Reg: " & Trim(txtDealerCode.Text) & " / " & txtAccountName.Text & " / " & txtAccountPostCode.Text & " added")
                    Dim startUpScript As String = String.Format("window.parent.HideRegAddWindow();")
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
                Else
                    Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Trade Registration edited", "Reg: " & Trim(txtDealerCode.Text) & " / " & txtAccountName.Text & " / " & txtAccountPostCode.Text & " amended")
                    Dim startUpScript As String = String.Format("window.parent.HideRegDelWindow();")
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
                End If
            End If

        Else

            lblErr.Visible = True

        End If

    End Sub

    Protected Sub txtDealerCode_Validation(sender As Object, e As ValidationEventArgs) Handles txtDealerCode.Validation
        txtDealerName.Text = GetDataString("SELECT ISNULL(CentreName,'') FROM VALUE_CHAIN_LIVE..CentreMaster WHERE DealerCode = '" & txtDealerCode.Text & "'")
        txtOwnerDealerCode.Text = txtDealerCode.Text
        txtOwnerDealerName.Text = txtDealerName.Text
    End Sub

    Private Function AccountExists() As Boolean
        AccountExists = GetDataLong("SELECT COUNT(*) FROM v_TradeLoyaltyMembers WHERE DealerCode = '" & txtDealerCode.Text & "' AND AccountName = '" & txtAccountName.Text & "' AND AccountPostCode = '" & txtAccountPostCode.Text & "'") > 0
    End Function

    Protected Sub txtOwnerDealerCode_Validation(sender As Object, e As ValidationEventArgs) Handles txtOwnerDealerCode.Validation
        txtOwnerDealerName.Text = GetDataString("SELECT ISNULL(CentreName,'') FROM VALUE_CHAIN_LIVE..CentreMaster WHERE DealerCode = '" & txtOwnerDealerCode.Text & "'")
    End Sub

    Private Sub GetCustomerDealerLink(nCustomerId As Integer, sDealerCode As String)

        Dim dsMembers As DataSet
        Dim da As New DatabaseAccess
        Dim sErrorMessage As String = ""
        Dim sMembers As String = ""
        Dim i As Integer
        Dim htin As New Hashtable
        Dim iA As Integer = 0
        Dim iC As Integer = 0
        Dim iM As Integer = 0

        Dim sLinkType As String = ""
        Dim sLink As String = ""
        Dim sBusinessName As String = ""
        Dim bAllocated As Boolean = False

        htin.Add("@sDealerCode", sDealerCode)
        htin.Add("@nCustomerId", nCustomerId)
        dsMembers = da.Read(sErrorMessage, "p_TradeLoyalty_CustomerLinks", htin)

        For i = 0 To dsMembers.Tables(0).Rows.Count - 1

            sLinkType = dsMembers.Tables(0).Rows(i).Item(0)
            sLink = dsMembers.Tables(0).Rows(i).Item(1)
            sBusinessName = dsMembers.Tables(0).Rows(i).Item(2)

            bAllocated = AlreadyAllocated(sLinkType, sLink)

            If Not bAllocated And ddlLink1.SelectedIndex = 0 Then
                ddlLink1.SelectedValue = sLinkType
                txtLink1.Text = sLink
                lblLink1.Text = sBusinessName
                bAllocated = True
            End If
            If Not bAllocated And ddlLink2.SelectedIndex = 0 Then
                ddlLink2.SelectedValue = sLinkType
                txtLink2.Text = sLink
                lblLink2.Text = sBusinessName
                bAllocated = True
            End If
            If Not bAllocated And ddlLink3.SelectedIndex = 0 Then
                ddlLink3.SelectedValue = sLinkType
                txtLink3.Text = sLink
                lblLink3.Text = sBusinessName
                bAllocated = True
            End If
            If Not bAllocated And ddlLink4.SelectedIndex = 0 Then
                ddlLink4.SelectedValue = sLinkType
                txtLink4.Text = sLink
                lblLink4.Text = sBusinessName
                bAllocated = True
            End If
            If Not bAllocated And ddlLink5.SelectedIndex = 0 Then
                ddlLink5.SelectedValue = sLinkType
                txtLink5.Text = sLink
                lblLink5.Text = sBusinessName
            End If

        Next

    End Sub

    Protected Sub btnLookup_Click(sender As Object, e As EventArgs) Handles btnLookup.Click

        Dim nCustomerId As Long = 0
        Dim sStr As String = ""

        If IsNumeric(txtCustomerId.Text) Then
            nCustomerId = Val(txtCustomerId.Text)
            If nCustomerId <> 0 Then
                If Trim(txtAccountName.Text) = "" Then
                    txtAccountName.Text = GetDataString("SELECT BusinessName FROM Customer WHERE Id = " & Trim(Str(nCustomerId)))
                    txtAccountPostCode.Text = GetDataString("SELECT ISNULL(PostCode,'') FROM Customer WHERE Id = " & Trim(Str(nCustomerId)))
                End If
                Call GetCustomerDealerLink(nCustomerId, txtDealerCode.Text)
            End If
        End If

    End Sub

    Protected Sub btnResetLinks_Click(sender As Object, e As EventArgs) Handles btnResetLinks.Click
        ddlLink1.SelectedIndex = 0
        txtLink1.Text = ""
        lblLink1.Text = ""
        ddlLink2.SelectedIndex = 0
        txtLink2.Text = ""
        lblLink2.Text = ""
        ddlLink3.SelectedIndex = 0
        txtLink3.Text = ""
        lblLink3.Text = ""
        ddlLink4.SelectedIndex = 0
        txtLink4.Text = ""
        lblLink4.Text = ""
        ddlLink5.SelectedIndex = 0
        txtLink5.Text = ""
        lblLink5.Text = ""
    End Sub

    Protected Sub txtLink1_Validation(sender As Object, e As ValidationEventArgs) Handles txtLink1.Validation
        lblLink1.Text = GetElementName(ddlLink1, txtLink1.Text)
    End Sub

    Protected Sub txtLink2Validation(sender As Object, e As ValidationEventArgs) Handles txtLink2.Validation
        lblLink2.Text = GetElementName(ddlLink2, txtLink2.Text)
    End Sub

    Protected Sub txtLink3_Validation(sender As Object, e As ValidationEventArgs) Handles txtLink3.Validation
        lblLink3.Text = GetElementName(ddlLink3, txtLink3.Text)
    End Sub

    Protected Sub txtLink4_Validation(sender As Object, e As ValidationEventArgs) Handles txtLink4.Validation
        lblLink4.Text = GetElementName(ddlLink4, txtLink4.Text)
    End Sub

    Protected Sub txtLink5_Validation(sender As Object, e As ValidationEventArgs) Handles txtLink5.Validation
        lblLink5.Text = GetElementName(ddlLink5, txtLink5.Text)
    End Sub

    Private Function GetElementName(ddlLink As DropDownList, sValue As String) As String

        Dim sField1 As String = ""
        Dim sField2 As String = ""
        Dim sSQL As String = ""
        Dim sResult As String = ""

        If sValue <> "" Then
            sResult = "* Not found *"
        End If

        Select Case ddlLink.SelectedValue
            Case "A"
                sField1 = "AccountName"
                sField2 = "AccountCode"
            Case "C"
                sField1 = "CompanyName"
                sField2 = "CompanyMagicNumber"
            Case "M"
                sField1 = "CustomerName"
                sField2 = "CustomerMagicNumber"
            Case Else
        End Select

        If sField1 <> "" And sField2 <> "" And sValue <> "" Then
            sSQL = " SELECT TOP 1 ISNULL(" & sField1 & ",'')" & _
                       " FROM Web_TradeLoyalty_Accounts " & _
                       " WHERE " & sField1 & " <> '' AND " & _
                       " DealerCode = '" & txtDealerCode.Text & "' AND " & _
                       sField2 & " = '" & sValue & "'"
            sResult = GetDataString(sSQL)
        End If

        GetElementName = sResult

    End Function

    Protected Sub ddlLink1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLink1.SelectedIndexChanged
        txtLink1.Text = ""
        lblLink1.Text = ""
    End Sub

    Protected Sub ddlLink2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLink2.SelectedIndexChanged
        txtLink2.Text = ""
        lblLink2.Text = ""
    End Sub

    Protected Sub ddlLink3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLink3.SelectedIndexChanged
        txtLink3.Text = ""
        lblLink3.Text = ""
    End Sub

    Protected Sub ddlLink4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLink4.SelectedIndexChanged
        txtLink4.Text = ""
        lblLink4.Text = ""
    End Sub

    Protected Sub ddlLink5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLink5.SelectedIndexChanged
        txtLink5.Text = ""
        lblLink5.Text = ""
    End Sub

    Private Function AlreadyAllocated(sLinkType As String, sLink As String) As Boolean

        Dim bResult As Boolean = False

        If ddlLink1.SelectedValue = sLinkType And txtLink1.Text = sLink Then
            bResult = True
        End If

        If ddlLink2.SelectedValue = sLinkType And txtLink2.Text = sLink Then
            bResult = True
        End If

        If ddlLink3.SelectedValue = sLinkType And txtLink3.Text = sLink Then
            bResult = True
        End If

        If ddlLink4.SelectedValue = sLinkType And txtLink4.Text = sLink Then
            bResult = True
        End If

        If ddlLink5.SelectedValue = sLinkType And txtLink5.Text = sLink Then
            bResult = True
        End If

        AlreadyAllocated = bResult

    End Function

End Class
