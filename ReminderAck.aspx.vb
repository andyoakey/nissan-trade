﻿Imports System.Data
Imports System.Net.Mail

Partial Class ReminderAck

    Inherits System.Web.UI.Page

    Dim nId As Integer
    Dim nType As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        nId = Request.QueryString(0)
        nType = Request.QueryString(1)
    End Sub

    Protected Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        Dim sSQL As String

        '/ Contact History line...
        If nType = 1 Then
            sSQL = "UPDATE CustomerDealerContact SET Reminder = 0 WHERE Id = " & Trim(Str(nId))
            Call RunNonQueryCommand(sSQL)
        End If

        Dim startUpScript As String = String.Format("window.parent.HideReminderAck();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)

    End Sub

End Class

