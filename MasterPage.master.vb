Imports System.Data
Imports DevExpress.Web
Imports QubeSecurity

Partial Class MasterPage

    Inherits System.Web.UI.MasterPage

    Protected Sub dsSwitch_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsSwitch.Init
        dsSwitch.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        DevExpress.Web.ASPxWebControl.SetIECompatibilityMode(8)

        If HttpContext.Current.User.Identity.IsAuthenticated Then
            If Session("SelectionLevel") Is Nothing OrElse Session("SelectionId") Is Nothing Then
                Session.Clear()
                Session.Abandon()
                Response.Redirect("~/Login.aspx")
            End If
        Else
            Session.Clear()
            Session.Abandon()
            Response.Redirect("~/Login.aspx")
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not HttpContext.Current.User.Identity.IsAuthenticated Then
            Response.Redirect("~/Login.aspx")
        End If
		
        If Authentication.CheckPasswordExpiry(Session("UserEmailAddress")) AND Not HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("chgpwd") Then
            Response.Redirect("~/ChgPwd.aspx?gdpr=1", True)
        End If

        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        Dim sAccessLevel As String

        If Not IsPostBack Then
            '     btnMasterBack.Attributes.Add("onClick", "javascript:history.back(); return false;")
            Session("MastCustSrchString") = ""
        End If

        If Session("UserId") Is Nothing Or Session("UserId") = 0 Then
            ASPxMenu1.Visible = False
            Call MenuOnOff("mnuLogout", False)
            ddlAccessPoint.Visible = False
        Else

            If Not Session("UserEmailAddress") Is Nothing Then
                If Session("UserEmailAddress").ToString().Contains("@qubedata.") Then
                    Image1.ToolTip = System.Net.Dns.GetHostName() & " : " & DBName() & vbCrLf & HttpContext.Current.Request.ServerVariables("HTTP_HOST")
                End If
            End If

            If Session("SelectionLevel") = "" Or Session("SelectionLevel") Is Nothing Then
                sAccessLevel = GetDataString("SELECT AccessLevel FROM Web_User WHERE UserID = " & Session("UserId"))
                If sAccessLevel <> "" Then
                    Session("SelectionLevel") = Left(sAccessLevel, 1)
                    Session("SelectionId") = Mid(sAccessLevel, 2)
                End If
            End If

            ASPxMenu1.Visible = True
            ddlAccessPoint.Visible = True
            Call MenuOnOff("mnuLogout", True)
            Call ShowLoyalty()
            If Session("UserLevel") = "C" Or Session("UserLevel") = "G" Then
                Call MenuOnOff("mnuHeadOffice", False)
            End If
            If Session("SelectionLevel") Is Nothing Then
                If Session("UserLevel") <> "C" Then
                    Call MenuOnOff("mnuYOYCustomers", False)
                End If
            End If

        End If

        ' Only Qube users can see Qube menu and add new users
        btnQube.Visible = False
        
        If isQubeUser(Session("UserEmailAddress")) = True Then
            btnQube.Visible = True
        End If



    End Sub

    Protected Sub ASPxMenu1_ItemClick(ByVal source As Object, ByVal e As DevExpress.Web.MenuItemEventArgs) Handles ASPxMenu1.ItemClick
        If e.Item.Name = "mnuLogout" Then
            Session("UserId") = Nothing
            Response.Redirect("~/Login.aspx")
        End If

        If e.Item.Name = "mnuViewAllCustomers" Then
            Response.Redirect("ViewCompanies.aspx")
        End If

        If e.Item.Name = "mnuHeadOffice" Then
            PopupHOMenu.ShowOnPageLoad = True
        End If


        If e.Item.Name = "mnuMapping" Then
            If Session("SelectionLevel") = "C" Then
                Response.Redirect("~/Report6.aspx?map=0")
            End If
        End If
    End Sub

    Protected Sub ddlAccessPoint_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccessPoint.DataBound
        If Session("CurrentSelection") <> Nothing Then
            ddlAccessPoint.SelectedIndex = Session("CurrentSelection")
            Session("SelectionLevel") = Left(ddlAccessPoint.SelectedItem.Value, 1)
            Session("SelectionId") = Mid(ddlAccessPoint.SelectedItem.Value, 2)
        End If
    End Sub

    Protected Sub ddlAccessPoint_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccessPoint.SelectedIndexChanged

        Session("SelectionLevel") = Left(ddlAccessPoint.SelectedItem.Value, 1)
        Session("SelectionId") = Trim(Mid(ddlAccessPoint.SelectedItem.Value, 2))

        Session("CurrentSelection") = ddlAccessPoint.SelectedIndex
        If Session("SelectionLevel") = "N" Then
            Session("DealerCode") = Session("SelectionId")
        End If

    End Sub

    Private Sub ShowLoyalty()

        'Call MenuOnOff("mnuCustomerLoyalty", False)

        'If Session("SelectionLevel") = "N" Or Session("SelectionLevel") = "T" Then
        '    Call MenuOnOff("mnuCustomerLoyalty", True)
        'End If

    End Sub

    Private Sub MenuOnOff(ByVal sName As String, ByVal bValue As Boolean)

        Dim i As Integer
        Dim j As Integer

        For i = 0 To ASPxMenu1.Items.Count - 1
            If ASPxMenu1.Items(i).Name = sName Then
                ASPxMenu1.Items(i).Visible = bValue
            End If
            If ASPxMenu1.Items(i).Items.Count > 0 Then
                For j = 0 To ASPxMenu1.Items(i).Items.Count - 1
                    If ASPxMenu1.Items(i).Items(j).Name = sName Then
                        ASPxMenu1.Items(i).Items(j).Visible = bValue
                    End If
                Next
            End If
        Next

    End Sub

    Public ReadOnly Property SelectionLevel() As String
        Get
            SelectionLevel = Left(Me.ddlAccessPoint.SelectedItem.Text, 1)
        End Get
    End Property

    Public ReadOnly Property SelectionId() As String
        Get
            SelectionId = Mid(Me.ddlAccessPoint.SelectedItem.Text, 2)
        End Get
    End Property

    Public Sub btnExcelClick() Handles btnExcel.Click
        Session("ExcelClicked") = True
    End Sub

    Public Sub btnMasterBackClick() Handles btnMasterBack.Click
        Session("BackClicked") = True
    End Sub

    Public Function DBName() As String

        Dim sConn As String = UCase(System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString)
        Dim i1 As Integer
        Dim i2 As Integer
        Dim i3 As Integer
        Dim i4 As Integer

        DBName = ""
        i1 = InStr(sConn, "=")
        i2 = InStr(sConn, ";")
        i3 = InStr(i2 + 1, sConn, "=", )
        i4 = InStr(i2 + 1, sConn, ";")
        If i1 > 0 And i2 > 0 Then
            DBName = Mid(sConn, i3 + 1, (i4 - (i3 + 1)))
        End If

    End Function

    Protected Sub btnMastCustSeach_Click(sender As Object, e As EventArgs) Handles btnMastCustSeach.Click
        If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then
            lblSearch1.Text = "Enter Customer Id, Name or Account Code"
        Else
            lblSearch1.Text = "Enter Customer Id or Name"
        End If
        PopMastCustSrch.ShowOnPageLoad = True
    End Sub

    Protected Sub btnMastCustGoTo_Click(sender As Object, e As EventArgs) Handles btnMastCustGoTo.Click
        Session("CustomerCalledFrom") = "MasterPage"
        If Session("SrchCustomerId") <> 0 Then
            Dim sPage As String = "ViewCustomerDetails.aspx?0" & Trim(Session("SrchCustomerId"))
            Response.Redirect(sPage)
        End If
    End Sub

    Protected Sub btnMastCustSearchBind_Click(sender As Object, e As EventArgs) Handles btnMastCustSearchBind.Click
        If txtMastCustSearch.Text <> "" Then
            Call MasterCustomerSearch()
        Else
            PopMastCustSrch.ShowOnPageLoad = False
        End If
    End Sub

    Private Sub MasterCustomerSearch()

        Dim htIn1 As New Hashtable
        Dim da As New DatabaseAccess
        Dim dsMastCustSrch As DataSet
        Dim sErr As String = ""
        Dim i As Integer

        Session("SrchCustomerId") = 0
        ddlMastCustSrch.Items.Clear()

        htIn1.Add("@sSelectionLevel", Session("SelectionLevel"))
        htIn1.Add("@sSelectionId", Session("SelectionId"))
        htIn1.Add("@sSrchString", txtMastCustSearch.Text)
        dsMastCustSrch = da.Read(sErr, "p_CustIdNameSrch", htIn1)
        If sErr <> "" Then
            Session("CallingModule") = "Dashboard.ASPX - ReminderRows()"
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If
        For i = 0 To dsMastCustSrch.Tables(0).Rows.Count - 1
            ddlMastCustSrch.Items.Add(New ListEditItem(dsMastCustSrch.Tables(0).Rows(i).Item(2), dsMastCustSrch.Tables(0).Rows(i).Item(1)))
        Next
        If dsMastCustSrch.Tables(0).Rows.Count > 0 Then
            ddlMastCustSrch.SelectedIndex = 0
            Session("SrchCustomerId") = ddlMastCustSrch.SelectedItem.Value
        End If

    End Sub

    Protected Sub ddlMastCustSrch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMastCustSrch.SelectedIndexChanged
        Session("SrchCustomerId") = ddlMastCustSrch.SelectedItem.Value
    End Sub

    Protected Sub btnQube_Click(sender As Object, e As EventArgs)
        Me.PopupHOMenu.ShowOnPageLoad = False
        Me.PopupQubeMenu.ShowOnPageLoad = True
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs)
        Me.PopupHOMenu.ShowOnPageLoad = False
        Me.PopupExports.ShowOnPageLoad = True
    End Sub


End Class