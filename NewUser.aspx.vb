﻿Imports System.Net.Mail
Imports System.Data

Partial Class NewUser

    Inherits System.Web.UI.Page

    Public Enum Model As Integer
        micra = 1
        xtrail = 2
        note = 3
        patrol = 4
        qashqai = 5
        juke = 6
        leaf = 7
        pulsar = 8
        navara = 9
        gtr = 10
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - New User Request"

        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New User Request", "Requesting new user")
        End If

        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

    End Sub

    Protected Sub dsDealers_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDealers.Init
        dsDealers.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub btnClearReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearReg.Click

        txtEmailAddress.Text = ""
        txtConfEmailAddress.Text = ""
        txtName.Text = ""
        cboDealers.SelectedIndex = -1

        lblErr.Visible = False
        lblRegDone.Visible = False
        btnSaveReg.Enabled = True

        txtEmailAddress.Enabled = True
        txtConfEmailAddress.Enabled = True
        txtName.Enabled = True
        cboDealers.Enabled = True
        btnSaveReg.Enabled = True

        txtEmailAddress.Focus()

    End Sub

    Protected Sub btnSaveReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveReg.Click

        Dim bErr As Boolean = False
        Dim da As New DatabaseAccess
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim sErr As String = ""
        Dim sSQL As String = ""
        Dim htIn As New Hashtable
        Dim sAccessLevel As String
        Dim sNewPassword As String

        lblErr.Visible = False
        lblErr2.Visible = False
        lblErr3.Visible = False
        lblErr4.Visible = False

        If IsValidEmail(txtEmailAddress.Text) = False Then
            lblErr.Text = "Invalid Email Format"
            lblErr.Visible = True
            bErr = True
        End If

        If Trim(txtEmailAddress.Text) = "" Then
            lblErr.Text = "The Email Address is mandatory"
            lblErr.Visible = True
            bErr = True
        End If

        If Trim(txtConfEmailAddress.Text) = "" Then
            lblErr2.Text = "The new Email Address must be confirmed"
            lblErr2.Visible = True
            bErr = True
        End If


        If Trim(txtEmailAddress.Text) <> Trim(txtConfEmailAddress.Text) Then
            lblErr2.Text = "The Email address and confirmation do not match"
            lblErr2.Visible = True
            bErr = True
        End If

        If Trim(txtName.Text) = "" Then
            lblErr3.Text = "A Name is mandatory"
            lblErr3.Visible = True
            bErr = True
        End If

        If Trim(cboDealers.Text) = "" Then
            lblErr4.Text = "You must select a Dealer"
            lblErr4.Visible = True
            bErr = True
        End If

        If Not bErr Then
            sSQL = "SELECT * FROM Web_User WHERE EmailAddress = '" & Trim(txtEmailAddress.Text) & "'"
            ds = da.ExecuteSQL(sErr, sSQL)
            If Not ds Is Nothing Then
                If sErr = "" Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblErr.Text = "This Email Address has already been registered on the website"
                        lblErr.Visible = True
                        bErr = True
                    End If
                Else
                    HttpContext.Current.Session("sErr") = sErr
                    HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
                End If
            Else
                HttpContext.Current.Session("sErr") = sErr
                HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            End If
        End If

        If Not bErr Then

            sAccessLevel = "C" & cboDealers.Value
            sNewPassword = CreateWebPassword()

            htIn.Add("@sEmailAddress", Trim(txtEmailAddress.Text))
            htIn.Add("@sPassword", sNewPassword)
            htIn.Add("@sAccessLevel", sAccessLevel)
            htIn.Add("@sFirstName", Trim(txtName.Text))

            da.Update(sErr, "p_User_Ins", htIn)

            If sErr.Length > 0 Then
                HttpContext.Current.Session("sErr") = sErr : HttpContext.Current.Response.Redirect("~/dberror.aspx", True)
            Else
                Call SendConfirmationEmail(Trim(txtEmailAddress.Text), sNewPassword, Trim(txtName.Text), sAccessLevel)
                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New User Added", "New User : " & Trim(txtEmailAddress.Text) & " added and given access to Dealer: " & Mid(sAccessLevel, 2))
                lblRegDone.Text = "Your request is complete. Email Address " & Trim(txtEmailAddress.Text) & " has been added to the website and given access to Dealer " & Mid(sAccessLevel, 2)
                lblRegDone.Visible = True
                txtEmailAddress.Enabled = False
                txtConfEmailAddress.Enabled = False
                txtName.Enabled = False
                cboDealers.Enabled = False
                btnSaveReg.Enabled = False
            End If

        End If

    End Sub

    Private Function IsValidEmail(ByVal EmailAddress As String) As Boolean
        Return True
    End Function

    Private Function CreateWebPassword() As String

        'Create random car name part of password
        Dim nmodel As Model
        Dim nModelValue As Integer
        nModelValue = 0
        Randomize()
        While CStr(Model.GetName(nmodel.GetType, nModelValue)) = ""
            nModelValue = Int(Rnd() * 10)
            nmodel = nModelValue
        End While

        'Create random number to be appended to the car name
        Dim nRandomValue As Integer
        Randomize()
        nRandomValue = Int(Rnd() * 999)
        Dim sPassword As String

        sPassword = CStr(Model.GetName(nmodel.GetType, nModelValue)) + CStr(nRandomValue)

        Return sPassword

    End Function

    Sub SendConfirmationEmail(ByVal sUserEmail As String, ByVal sUserPassword As String, ByVal sName As String, ByVal sAccessLevel As String)

        Dim MailObj As New SmtpClient
        Dim mm As New MailMessage
        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")
        basicAuthenticationInfo.UserName = "support@toyotavaluechain.net"
        basicAuthenticationInfo.Password = "4AhK56DmpGvn"
        MailObj.Host = "mail.toyotavaluechain.net"
        MailObj.UseDefaultCredentials = False
        MailObj.Credentials = basicAuthenticationInfo

        mm.To.Add(sUserEmail)
        mm.From = New MailAddress("nissansupport@qubedata.co.uk")
        mm.Subject = "Nissan Trade Site - New User Registration"
        mm.Body += "To: " & sName & vbCrLf & vbCrLf
        mm.Body += "You have been registered for access to the Nissan Trade Site." & vbCrLf & vbCrLf
        mm.Body += "The URL is   nissan.qubetradeparts.net   Please DO NOT prefix the URL with www" & vbCrLf & vbCrLf
        mm.Body += "Your current password is '" + sUserPassword + "'" + vbCrLf & vbCrLf
        mm.Body += "You have been given access to the Following Dealer:  '" + Mid(sAccessLevel, 2) + "'" + vbCrLf & vbCrLf & vbCrLf & vbCrLf
        mm.Body += "Please do not reply, this is an automated response from an unmonitored email address." & vbCrLf & vbCrLf
        mm.Body += "If you want to contact us, please use the details in the contact us box." & vbCrLf & vbCrLf
        mm.Body += "Kind Regards" & vbCrLf & vbCrLf
        mm.Body += "The Nissan Trade Site support team."

        MailObj.Send(mm)

    End Sub

End Class
