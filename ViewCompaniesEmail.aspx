﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCompaniesEmail.aspx.vb" Inherits="ViewCompaniesEmail" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
    </title>
    <link rel="shortcut icon" href="assets/images/nissan/favicon.ico?v=2" />
    <link rel="stylesheet" href="styles/master.css?v=2" />

</head>
<body>
    <form id="form1" runat="server">

        <div style="float: left; width: 70%">
           <dx:ASPxTextBox 
               runat="server" 
               Width="100%" 
               ID="txtEmail" >
                <ValidationSettings 
                   ErrorDisplayMode="Text"
                   ErrorTextPosition="Bottom">
                   <RegularExpression 
                       ErrorText="Enter a Valid e-mail or click <x> to Cancel" 
                       ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
               </ValidationSettings>
                <InvalidStyle BackColor="LightPink"/>
               </dx:ASPxTextBox>
        </div>

        <div style="float: right; width: 20%">
            <dx:ASPxButton 
                ID="btnEmailSave" 
                ClientInstanceName="btnEmailSave" 
                runat="server" 
                Text="Save" 
                Width="100%"> 
            </dx:ASPxButton>
        </div>

    </form>
</body>
</html>

