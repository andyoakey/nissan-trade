﻿Imports DevExpress.XtraPrinting

Partial Class FocusMailing
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True



    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "FocusMailing"

        With grid
            .Columns("dealerflyername").Visible = True
            .Columns("dealerflyeraddress1").Visible = True
            .Columns("dealerflyeraddress2").Visible = True
            .Columns("dealerflyeraddress3").Visible = True
            .Columns("dealerflyerposttown").Visible = True
            .Columns("dealerflyercounty").Visible = True
            .Columns("dealerflyerpostcode").Visible = True
            .Columns("dealerflyeremailaddress").Visible = True
            .Columns("dealerflyerphone").Visible = True
            .Columns("dealerflyerfax").Visible = True
            .Columns("dealerflyercontactname").Visible = True
            .Columns("contact_salutation").Visible = True
            .Columns("address1").Visible = True
            .Columns("address2").Visible = True
            .Columns("address3").Visible = True
            .Columns("county").Visible = True
            .Columns("refcode").Visible = True
            .Columns("prospect_flag").Visible = True
            .Columns("flyeropeninghours").Visible = True
            .Columns("spendthisyear").Visible = True
            .Columns("mailingstatus").Visible = True
            .Columns("mailingterritory").Visible = True
        End With

        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)

        With grid
            .Columns("dealerflyername").Visible = False
            .Columns("dealerflyeraddress1").Visible = False
            .Columns("dealerflyeraddress2").Visible = False
            .Columns("dealerflyeraddress3").Visible = False
            .Columns("dealerflyerposttown").Visible = False
            .Columns("dealerflyercounty").Visible = False
            .Columns("dealerflyerpostcode").Visible = False
            .Columns("dealerflyeremailaddress").Visible = False
            .Columns("dealerflyerphone").Visible = False
            .Columns("dealerflyerfax").Visible = False
            .Columns("dealerflyercontactname").Visible = False
            .Columns("contact_salutation").Visible = False
            .Columns("address1").Visible = False
            .Columns("address2").Visible = False
            .Columns("address3").Visible = False
            .Columns("county").Visible = False
            .Columns("refcode").Visible = False
            .Columns("prospect_flag").Visible = False
            .Columns("flyeropeninghours").Visible = False
            .Columns("spendthisyear").Visible = False
            .Columns("mailingstatus").Visible = False
            .Columns("mailingterritory").Visible = False
        End With




    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

End Class
