﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCompaniesFocusType.aspx.vb" Inherits="ViewCompaniesFocusType" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/normalize.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/master.css" />
    <link rel="stylesheet" href="../css/custom.css" />
    <style>
        body {
            overflow: hidden !important;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:dropdownlist ID="ddlFT"  AutoPostBack="True" runat="server" CssClass="mb10"></asp:dropdownlist>
        <dx:ASPxButton ID="btnFTSave" ClientInstanceName="btnFTSave"  runat="server"  Text="Save"></dx:ASPxButton>
    </form>
</body>
</html>
