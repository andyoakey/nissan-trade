﻿Imports System.Data
Imports System.Data.SqlClient
Imports QubeUtils

Partial Class Invoice
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString.Count > 0 Then
            Session("DealerCode") = Request.QueryString("dc")
            Session("InvoiceNumber") = Request.QueryString("inv")

            LoadCompanyDetails()
            LoadInvoiceParts()
            LoadInvoiceTotals()

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "View Invoice", "Viewing Invoice Number : " & Session("InvoiceNumber"))
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        lblPageTitle.Text = "Invoice " & Session("InvoiceNumber").ToString()

    End Sub
    
    Protected Sub LoadCompanyDetails()
        Dim param = New SqlParameter(1){}
        param(0) = New SqlParameter("@DealerCode", SqlDbType.NVarChar, 6) With {.Value = Session("DealerCode").ToString() }
        param(1) = New SqlParameter("@InvoiceNumber", SqlDbType.NVarChar, 20) With {.Value = Session("InvoiceNumber").ToString() }

        Dim dtCompanyDetails = MSSQLHelper.GetDataTable("SELECT TOP 1 * FROM web_qube WHERE DealerCode=@DealerCode AND InvoiceNumber=@InvoiceNumber order by dataperiodid desc", param)

        If dtCompanyDetails.Rows.Count = 1 Then
            With dtCompanyDetails.Rows(0)
                lblDealerName.Text = .Item("CentreName") & " (" & .Item("DealerCode") & ")"

                lblInvoiceDate.Text = Format(.Item("InvoiceDate"), "dd-MMM-yyyy")
                lblCompany.Text = Trim(.Item("CustomerName")) & vbCrLf & FormatAddress(.Item("CustomerAddress1"), .Item("CustomerAddress2"), .Item("CustomerAddress3"), .Item("CustomerAddress4"), .Item("CustomerAddress5"), .Item("CustomerPostCode"), True)
                lblAccount.Text = Trim(.Item("DMSRef")) & vbCrLf & Trim(.Item("AccountName")) & vbCrLf & FormatAddress(.Item("AccountAddress1"), .Item("AccountAddress2"), .Item("AccountAddress3"), .Item("AccountAddress4"), .Item("AccountAddress5"), .Item("AccountPostCode"), True)
            End With
        End If

    End Sub
    Protected Sub LoadInvoiceParts()
        Dim param = New SqlParameter(1){}
        param(0) = New SqlParameter("@sDealerCode", SqlDbType.NVarChar, 6) With {.Value = Session("DealerCode").ToString() }
        param(1) = New SqlParameter("@sInvoiceNumber", SqlDbType.NVarChar, 20) With {.Value = Session("InvoiceNumber").ToString() }

        Dim dtInvoiceParts = MSSQLHelper.GetDataTable("p_InvoiceParts", param, "", true)

        If dtInvoiceParts.Rows.Count > 0 Then
            gridParts.DataSource = dtInvoiceParts
            gridParts.DataBind()
        End If
    End Sub
    Protected Sub LoadInvoiceTotals()
        Dim param = New SqlParameter(1){}
        param(0) = New SqlParameter("@sDealerCode", SqlDbType.NVarChar, 6) With {.Value = Session("DealerCode").ToString() }
        param(1) = New SqlParameter("@sInvoiceNumber", SqlDbType.NVarChar, 20) With {.Value = Session("InvoiceNumber").ToString() }

        Dim dtInvoiceTotals as DataTable = MSSQLHelper.GetDataTable("p_InvoiceTotals", param, "", true)

        If dtInvoiceTotals.Rows.Count > 0 Then
            gridTotals.DataSource = dtInvoiceTotals
            gridTotals.DataBind()
        End If
    End Sub

    Protected Sub gridsParts_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridParts.HtmlDataCellPrepared
        If e.KeyValue = 1 Then
            e.Cell.Font.Italic = True
            e.Cell.ForeColor = Drawing.Color.Red
        End If
        If e.GetValue("Department") <> "P" Or e.GetValue("SaleType") <> "T" Then
            e.Cell.BackColor = Drawing.Color.LightGray
            e.Cell.Font.Italic = True
            txtNotes.Visible = True
            txtNotes.Text = "* - Invalid Department Code or Sales Type" & vbCrLf & "This invoice is not included in the website database."
            txtNotes.Font.Bold = True
            txtNotes.ForeColor = Drawing.Color.Red
            gridTotals.Visible = False
        End If
    End Sub

    Protected Sub gridsTotals_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridTotals.HtmlDataCellPrepared
        If InStr(e.KeyValue, "Excluded") > 0 Then
            e.Cell.Font.Italic = True
            e.Cell.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
        If e.Column.FieldName = "Department" Then
            If e.Value <> "P" Then
                e.DisplayText = "* " & e.Value
            End If
        End If
        If e.Column.FieldName = "SaleType" Then
            If e.Value <> "T" Then
                e.DisplayText = "* " & e.Value
            End If
        End If
    End Sub

End Class
