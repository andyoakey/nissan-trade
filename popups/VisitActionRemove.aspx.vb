﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports QubeUtils

Partial Class popups_VisitActionActivityRemove

    Inherits System.Web.UI.Page

    Dim nType As Integer
    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        nType = Request.QueryString("type")
        nId = Request.QueryString("id")

        If nType = 0 Then
            MSSQLHelper.ExecuteSQL("DELETE FROM Web_VAR_Details WHERE VARId=@Id", new SqlParameter(0){new SqlParameter("@Id",SqlDbType.Int) With {.Value = nId}})
            MSSQLHelper.ExecuteSQL("DELETE FROM Web_VAR WHERE Id=@Id", new SqlParameter(0){new SqlParameter("@Id",SqlDbType.Int) With {.Value = nId}})
        Else
            MSSQLHelper.ExecuteSQL("DELETE FROM Web_VAR_Details WHERE Id=@Id", new SqlParameter(0){new SqlParameter("@Id",SqlDbType.Int) With {.Value = nId}})
        End If

        Response.Redirect("~/VisitActions.aspx")
    End Sub


End Class

