﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports QubeUtils

Partial Class ViewCompaniesBT

    Inherits System.Web.UI.Page

    Dim nCustomerId As Long
    Dim nCurrentBT As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nCustomerId = Request.QueryString(0)

        If Not Page.IsPostBack Then
            nCurrentBT = Request.QueryString(1)

            If nCurrentBT = 0 Then nCurrentBT = 99

            Session("NewBT") = nCurrentBT
            ddlBT.Items.Add(New ListItem("BodyShop", 1))
            ddlBT.Items.Add(New ListItem("Breakdown Recovery Services", 2))
            ddlBT.Items.Add(New ListItem("Car Dealer New/Used", 3))
            ddlBT.Items.Add(New ListItem("Independent Motor Trader", 4))
            ddlBT.Items.Add(New ListItem("Mechanical Specialist", 5))
            ddlBT.Items.Add(New ListItem("Motor Factor", 6))
            ddlBT.Items.Add(New ListItem("Parts Supplier", 7))
            ddlBT.Items.Add(New ListItem("Taxi & Private Hire", 8))
            ddlBT.Items.Add(New ListItem("Windscreens", 9))
            ddlBT.Items.Add(New ListItem("Other", 99))
            ddlBT.SelectedIndex = nCurrentBT - 1
        End If

    End Sub

    Protected Sub btnBTSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBTSave.Click
        Dim param = New SqlParameter(1){
           New SqlParameter("@CustomerId", SqlDbType.Int) With { .Value = nCustomerId},
           New SqlParameter("@BusinessTypeId", SqlDbType.Int) With { .Value = Session("NewBT")}
       }
        MSSQLHelper.ExecuteSQL("Update Customer SET BusinessTypeId=@BusinessTypeId WHERE Id=@CustomerId", param)

        Dim param2 = New SqlParameter(1){
            New SqlParameter("@CustomerId", SqlDbType.Int) With { .Value = nCustomerId},
            New SqlParameter("@BusinessTypeId", SqlDbType.Int) With { .Value = Session("NewBT")}
        }
        MSSQLHelper.ExecuteSQL("Update CustomerDealer SET BusinessTypeId=@BusinessTypeId WHERE customerid=@CustomerId", param2)

        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "BusinessTypeId Updated", "Customer Id : " & nCustomerId & ", New BT : " & Session("NewBT"))
        Dim startUpScript As String = String.Format("window.parent.HideBTEdit();")

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

    Protected Sub ddlBT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBT.SelectedIndexChanged
        Session("NewBT") = ddlBT.SelectedIndex + 1
    End Sub

End Class
