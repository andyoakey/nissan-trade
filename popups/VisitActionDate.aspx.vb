﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports QubeUtils

Partial Class VisitActionsDate

    Inherits System.Web.UI.Page

    Dim nType As String
    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nType = Request.QueryString("type")
        nId = Request.QueryString("id")

        If Not Page.IsPostBack Then
            Dim sDate As String
            Dim param = new SqlParameter(){new SqlParameter("@Id", SqlDbType.Int) With { .Value = nId}}

            sDate = MSSQLHelper.ExecuteScalar("SELECT " & IIF(nType = 1, "CompletionDate","ConfirmedDate") & " FROM Web_VAR_Details WHERE Id=@Id", param)

            If sDate <> "" Then
                calDate.SelectedDate = CDate(sDate)
            Else
                calDate.SelectedDate = CDate(Now)
            End If
        End If

    End Sub

    Protected Sub btnDateSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDateSave.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim sDate As String = Format(CDate(calDate.SelectedDate), "dd-MMM-yyyy")

        Dim param = New SqlParameter(3){
         New SqlParameter("@nId", SqlDbType.Int) With { .Value = nId},
         New SqlParameter("@nType", SqlDbType.Int) With { .Value = nType},
         New SqlParameter("@sDate", SqlDbType.VarChar,12) With { .Value = sDate},
         new SqlParameter("@nUserId", SqlDbType.Int) With { .Value = Session("UserId")}
        }

        Try
            MSSQLHelper.ExecuteSQL("p_VAR_Detail_UpdDate", param, "", true)
        Catch ex As Exception
            Session("ErrorToDisplay") = ex.Message.Tostring()
            Response.Redirect(Settings.GetString("ErrorPage"))
        End Try

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Activity date updated", "Id : " & nId)
            Dim startUpScript As String = String.Format("window.parent.HidePopups();")

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

End Class
