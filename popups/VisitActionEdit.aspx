﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="VisitActionEdit.aspx.vb" Inherits="popups_VisistActionEdit" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxsc" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/normalize.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/master.css" />
    <link rel="stylesheet" href="../css/custom.css" />

    <style>
        .dxeFocused_Kia {
            border: 1px solid #adadad !important;
        }
        body {
            overflow: hidden;
        }
    </style>

</head>
<body>

    <form id="form1" runat="server">
        <dxsc:ASPxSpellChecker ID="ASPxSpellChecker2" runat="server" ClientInstanceName="spellChecker" Culture="English (United States)">
            <Dictionaries>
                <dxsc:ASPxSpellCheckerISpellDictionary AlphabetPath="~/App_Data/Dictionaries/EnglishAlphabet.txt"
                    GrammarPath="~/App_Data/Dictionaries/english.aff" DictionaryPath="~/App_Data/Dictionaries/american.xlg"
                    CacheKey="ispellDic" Culture="English (United States)" EncodingName="Western European (Windows)"></dxsc:ASPxSpellCheckerISpellDictionary>
            </Dictionaries>
            <ClientSideEvents BeforeCheck="function(s, e) { checkButton.SetEnabled(false);}" AfterCheck="function(s, e) { checkButton.SetEnabled(true);}" CheckCompleteFormShowing="function(s, e) {e.cancel=true;} " />
        </dxsc:ASPxSpellChecker>
        
        <div class="row mt10">
            <div class="col-md-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Activity: </label>
                        <asp:TextBox ID="txtActivity" runat="server" TabIndex="1" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Comments: </label>
                        <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" TabIndex="2" CssClass="form-control" Style="height: 50px !important;" ClientIDMode="Static" />
                    </div>
                    <div class="form-group">
                        <label>Responsibility: </label>
                        <asp:TextBox ID="txtResponsibility" runat="server" TabIndex="3" CssClass="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Target Date: </label>
                        <dx:ASPxDateEdit ID="txtTargetDate" runat="server" TabIndex="4" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt10">
            <div class="col-md-12">
                <div class="f-left mr10">
                    <dx:ASPxButton ID="btnNewVisitLine" runat="server" ClientInstanceName="btnNewVisitLine" Text="Save" TabIndex="5" />
                </div>
                <div class="f-left mr10">
                    <dx:ASPxButton ID="btnCancel" runat="server" ClientInstanceName="btnCancel" Text="Cancel" TabIndex="6" CssClass="grey">
                        <ClientSideEvents Click="function(s, e) { parent.HidePopups(); }" />
                    </dx:ASPxButton>
                </div>
                <%--<div class="f-left mr10">
                <dx:ASPxButton ID="checkButton" runat="server" ClientInstanceName="checkButton" ClientEnabled="True" Text="Check Spelling ..." AutoPostBack="False" cssclass="grey" >
                    <ClientSideEvents Click="function(s, e) { spellChecker.CheckElementsInContainer(document.getElementById('txtComments')) }" />
                </dx:ASPxButton>
            </div>--%>
            </div>
        </div>
    </form>
</body>
</html>
