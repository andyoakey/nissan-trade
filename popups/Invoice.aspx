﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Invoice.aspx.vb" Inherits="Invoice" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head id="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <meta charset="utf-8" />
    <meta name="google-site-verification" content="" />
    <title>Nissan Trade Parts</title>
    <script type="text/javascript" src="/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>

    <link href="/css/normalize.min.css" rel="stylesheet" />
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/css/master.css?v=3" rel="stylesheet" />
    <link href="/css/modal.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/custom.css?v=1" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <style>
        body {
            overflow-x: hidden;
        }
        .fixed-top {
            position: relative !important;
        }
        h3 { margin: 0 0 5px;}
        h4 { margin-bottom: 0;}
        
    </style>
</head>
<body id="body" runat="server">
    <form id="form1" runat="server">
        <div class="container-fluid">
            <%--<section class="header fixed-top">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="/Dashboard.aspx">
                            <img src="/imgs/Kia_logo.png" style="max-height: 60px;" class="img-responsive" alt="Kia" /></a>
                    </div>
                    <%--<div class="col-sm-9 txt-right">
                        
                        
                    </div>
                </div>
            </section>--%>
            <main role="main">
                <div class="row mt20">
                    <div class="col-sm-6">
                        <h1><asp:Label ID="lblPageTitle" runat="server" CssClass="page-title" /></h1>
                    </div>
                    <div class="col-sm-6 txt-right">
                        <h3><asp:Label runat="server" id="lblDealerName"></asp:Label></h3>
                        <h4><asp:Label ID="lblInvoiceDate" runat="server" /></h4>
                        <%--<h4>Account Details</h4>
                        <dx:ASPxLabel ID="lblCompany" runat="server" /><br/>
                        <dx:ASPxLabel ID="lblAccount" runat="server"/>--%>
                    </div>
                </div>
                <div class="row mt20">
                    <div class="col-md-12">
                        <table width="100%" class="table-invoice">
                            <%--<tr runat="server" id="tr2">
                                <td>Dealer Number:</td>
                                <td><dx:ASPxLabel ID="lblDealerCode" runat="server" /></td>
                                <td>Dealer Name:</td>
                                <td></td>
                            </tr>
                            <tr runat="server" id="tr0">
                                <td>Invoice #</td>
                                <td><dx:ASPxLabel ID="lblInvoiceNo" runat="server" /></td>
                                <td>Date:</td>
                                <td></td>
                            </tr>--%>
                            <tr runat="server" id="tr1">
                                <td>Company Details:</td>
                                <td><dx:ASPxLabel ID="lblCompany" runat="server" /></td>
                                <td>Account Details:</td>
                                <td><dx:ASPxLabel ID="lblAccount" runat="server"/></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row mt20">
                    <div class="col-md-12">
                        <dx:ASPxGridView ID="gridParts" runat="server" AutoGenerateColumns="False" OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" KeyFieldName="Excluded" Width="100%">

                            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" Mode="ShowAllRecords"></SettingsPager>
                            <Settings ShowFooter="False" ShowGroupedColumns="False" ShowGroupFooter="Hidden" ShowGroupPanel="False" ShowHeaderFilterButton="False" ShowFilterRow="false" ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True"/>
                            <Styles>
                                <Header HorizontalAlign="left" Font-Size="11px"></Header>
                                <Cell HorizontalAlign="left" Font-Size="11px"></Cell>
                            </Styles>
                            <SettingsBehavior AllowSort="False" ColumnResizeMode="Control" />

                            <Columns>

                                <dx:GridViewDataTextColumn Caption="Dept" FieldName="Department" ToolTip="" VisibleIndex="0" Width="5%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Sale Type" FieldName="SaleType" ToolTip="" VisibleIndex="0" Width="5%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Part" FieldName="PartNumber" ToolTip="" VisibleIndex="1" Width="10%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Description" FieldName="PartDescription" ToolTip="" VisibleIndex="2" Width="20%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Code" FieldName="PFC" ToolTip="" VisibleIndex="3" Width="7%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Code Description" FieldName="PFCDescription" ToolTip="" VisibleIndex="4" Width="20%">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Units" CellStyle-HorizontalAlign="Right" FieldName="Quantity" ToolTip="The Quantity" VisibleIndex="5" Width="8%">
                                    <Settings AllowAutoFilter="True" AllowHeaderFilter="False" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Sale Value" CellStyle-HorizontalAlign="Right" FieldName="SaleValue" ToolTip="The Sales Value" Width="8%" VisibleIndex="6" PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Cost Value" CellStyle-HorizontalAlign="Right" FieldName="CostValue" ToolTip="The Cost Value" Width="8%" VisibleIndex="7" PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn Caption="Profit" CellStyle-HorizontalAlign="Right" FieldName="Profit" ToolTip="The Margin" Width="8%" VisibleIndex="8" PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00">
                                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                </dx:GridViewDataTextColumn>

                            </Columns>

                        </dx:ASPxGridView>
                    </div>
                </div>

                <div class="row mt10">
                    <div class="col-md-12">
                        <div class="f-right txt-right">
                            <dx:ASPxGridView ID="gridTotals" runat="server" AutoGenerateColumns="False" OnCustomColumnDisplayText="AllGrids_OnCustomColumnDisplayText" KeyFieldName="SubTotal" Width="50%">
                                <SettingsPager Mode="ShowAllRecords" Visible="False"></SettingsPager>
                                <Settings UseFixedTableLayout="True" />
                                <SettingsBehavior AllowSort="False" AllowFocusedRow="False" AllowDragDrop="False" />
                                <Styles>
                                    <Header HorizontalAlign="Center" Font-Size="11px"></Header>
                                    <Cell HorizontalAlign="Center" Font-Size="11px"></Cell>
                                </Styles>
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="" FieldName="SubTotal" ToolTip="" VisibleIndex="1" Width="14%" CellStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                    <dx:GridViewDataTextColumn Caption="Sales Value" FieldName="SaleValue" ToolTip="The Sales Value" VisibleIndex="6" Width="12%" PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"/>
                                    <dx:GridViewDataTextColumn Caption="Cost Value" FieldName="CostValue" ToolTip="The Cost Value" VisibleIndex="7" Width="12%" PropertiesTextEdit-DisplayFormatString="&#163;##,##0.00"/>
                                    <dx:GridViewDataTextColumn Caption="Margin" FieldName="Margin" ToolTip="The Margin" VisibleIndex="8" Width="12%" PropertiesTextEdit-DisplayFormatString="0.00"/>
                                </Columns>
                            </dx:ASPxGridView>
                        </div>
                    </div>
                </div>
                <div class="row mt10">
                    <div class="col-md-12">
                        <asp:TextBox ID="txtNotes" runat="server" Width="100%" TabIndex="29" TextMode="MultiLine" BorderStyle="Outset" ReadOnly="True" Rows="4" Visible="False" />
                    </div>
                </div>
            </main>

        </div>
    </form>
</body>
</html>
