﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.Web.DynamicData
Imports QubeUtils

Partial Class popups_VisistActionEdit

    Inherits System.Web.UI.Page

    Dim nPageType As Integer
    Dim nVarId As Integer
    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nPageType = Val(Request.QueryString("type"))

        If nPageType = 0 Then
            nVarId = Val(Request.QueryString("id"))
            nId = 0
        Else
            nId = Val(Request.QueryString("id"))
            nVarId = GetDataLong("SELECT VarId FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId)))
            If Not Page.IsPostBack Then
                Call FetchDetails()
            End If
        End If

    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        txtActivity.Focus()
    End Sub

    Private Sub FetchDetails()
        Dim param = new SqlParameter(0){new SqlParameter("@Id", SqlDbType.Int)With {.Value = nId}} 
        Dim dtResults = MSSQLHelper.GetDataTable("SELECT * FROM Web_VAR_Details WHERE Id=@Id", param)

        If dtResults.Rows.Count = 1 Then
            With dtResults.Rows(0)
                txtActivity.Text = "" & .Item("Activity")
                txtComments.Text = "" & .Item("Comments")
                txtResponsibility.Text = "" & .Item("Responsibility")
                txtTargetDate.Date = CDate("" & .Item("TargetDate"))
            End With
        End If
    End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("VisitActions.aspx")
    End Sub


    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewVisitLine.Click
        Dim sTargetDate As String = ""
        Dim bOk As Boolean = True

        Try
            sTargetDate = Format(CDate(txtTargetDate.Text), "dd-MMM-yyyy")
        Catch ex As Exception
            bOk = False
        End Try

        If txtActivity.Text <> "" And txtComments.Text <> "" And txtResponsibility.Text <> "" And sTargetDate <> "" And bOk Then

            Dim param = new SqlParameter(5){
               new SqlParameter(IF(nPageType=0, "@nVARId","@nId"), SqlDbType.Int) With {.Value = IF(nPageType=0, nVarId, nId)},
               New SqlParameter("@sActivity", SqlDbType.VarChar, 255) With {.Value = txtActivity.Text},
               New SqlParameter("@sComments", SqlDbType.VarChar, 4000) With {.Value = txtComments.Text},
               New SqlParameter("@sResponsibility", SqlDbType.VarChar, 255) With {.Value = txtResponsibility.Text},
               New SqlParameter("@sTargetDate", SqlDbType.VarChar, 12) With {.Value = sTargetDate},
               New SqlParameter("@nUserId", SqlDbType.Int) With {.Value = Session("UserId")}
           }

            Try
                MSSQLHelper.ExecuteSQL(If(nPageType=0,"p_VAR_Detail_Ins","p_VAR_Detail_Upd"), param,"",true)
            Catch ex As Exception
                Session("ErrorToDisplay") = ex.Message.ToString()
                Response.Redirect(Settings.GetString("ErrorPage"))
            End Try

                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Visit Activity added", "Id : " & nVarId)
        End If

        Dim startUpScript As String = String.Format("window.parent.HidePopups();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

End Class
