﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="VisitActionNew.aspx.vb" Inherits="popups_VisitActionNew" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/normalize.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/master.css" />
    <link rel="stylesheet" href="../css/custom.css" />
    <style>
        .dxeFocused_Kia {
            border: 1px solid #adadad !important;
        }
        body {
            overflow: hidden;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Dealer: </label>
                        <dx:ASPxComboBox ID="ddlVisitDealer" runat="server" DataSourceID="dsDealers" ValueField="dealercode" TextField="dealername" EnableCallbackMode="true" IncrementalFilteringMode="Contains"
                            CallbackPageSize="30" DropDownWidth="400px" Width="300px" />
                    </div>
                    <div class="form-group">
                        <label>Date: </label>
                        <dx:ASPxDateEdit ID="txtVisitDate" runat="server" Width="200px" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="f-left mr10">
                    <dx:ASPxButton ID="btnNewVisitSave" ClientInstanceName="btnNewVisit" runat="server" Text="Save" />
                </div>
                <div class="f-left mr10">
                    <dx:ASPxButton ID="btnCancel" ClientInstanceName="btnCancel" runat="server" Text="Cancel" CssClass="grey">
                        <ClientSideEvents Click="function(s, e) { parent.HidePopups(); }" />
                    </dx:ASPxButton>
                </div>
            </div>
        </div>

        <asp:SqlDataSource ID="dsDealers" runat="server" SelectCommand="sp_DealerListing" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
                <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="10" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>

</body>
</html>
