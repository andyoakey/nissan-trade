﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web
Imports QubeUtils

Partial Class popups_VisitActionNew
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = Settings.GetString("WebAppName") & " - Visit Actions"
        
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Add New Visit Action", "Add New Visit Action")
        End If
    End Sub

    Protected Sub dsCentres_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDealers.Init
        dsDealers.ConnectionString = ConfigurationManager.ConnectionStrings(Settings.GetString("ConnectionStringName")).ConnectionString
    End Sub

    Protected Sub btnNewVisitSave_Click(sender As Object, e As EventArgs) Handles btnNewVisitSave.Click
        Dim sDealerCode As String = ddlVisitDealer.SelectedItem.Value
        Dim sVisitDate As String = Format(CDate(txtVisitDate.Text), "dd-MMM-yyyy")

        If sDealerCode <> "" And sVisitDate <> "" Then
            Dim param = New SqlParameter(2){
               New SqlParameter("@sDealerCode",SqlDbType.VarChar, 6) with {.Value=sDealerCode},
               New SqlParameter("@sVisitDate",SqlDbType.VarChar, 12) with {.Value=sVisitDate},
               new SqlParameter("@nUserId",SqlDbType.Int) with {.Value=Session("UserId")}
           }

            Try
                MSSQLHelper.ExecuteSQL("p_VAR_Header_Ins", param, "",True)
            Catch ex As Exception
                Session("ErrorToDisplay") = ex.Message.ToString()
                Response.Redirect(Settings.GetString("ErrorPage"))
            End Try

                Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Visit Action added", "Dealer : " & sDealerCode & "/" & sVisitDate)
                Dim startUpScript As String = String.Format("window.parent.HidePopups();")

                Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
            End If
    End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("VisitActions.aspx")
    End Sub


End Class
