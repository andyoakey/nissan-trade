﻿Imports System.Collections.Generic

Partial Class ViewCompaniesFocusType

    Inherits System.Web.UI.Page

    Dim nCustomerId As Long = 0
    Dim nCustomerDealerId As Long = 0
    Dim sDealerCode As String = ""
    Dim sCurrentFT As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nCustomerId = Request.QueryString(0)
        sDealerCode = Request.QueryString(1)
        sCurrentFT = Request.QueryString(2)
        nCustomerDealerId = GetDataLong("select id from customerdealer where customerid = " & nCustomerId & " and dealercode = '" & sDealerCode & "'")

        If Not Page.IsPostBack Then
            ddlFT.Items.Add(New ListItem("No", 0))
            ddlFT.Items.Add(New ListItem("Target", 1))

            If sCurrentFT = "No" Then
                ddlFT.SelectedIndex = 0
            End If

            If sCurrentFT = "Target" Then
                ddlFT.SelectedIndex = 1
            End If
        End If
    End Sub

    Protected Sub FTnFTSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFTSave.Click

        Dim sFT_Type As Integer = 0
        sFT_Type = ddlFT.SelectedIndex

        Dim sSQL As String = "Update NissanExtraFields SET bluegrassfocus_flag = " & sFT_Type & " WHERE customerdealerId = " & Trim(Str(nCustomerDealerId))
        Call RunNonQueryCommand(sSQL)

        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Focus Type Updated", "Customer Dealer Id : " & nCustomerDealerId & ", New FT : " & Session("NewFT"))
        Dim startUpScript As String = String.Format("window.parent.HideFTEdit();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub


End Class
