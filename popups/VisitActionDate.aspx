﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VisitActionDate.aspx.vb" Inherits="VisitActionsDate" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/normalize.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/master.css" rel="stylesheet" />
    <link href="../css/custom.css" rel="stylesheet" />
    <style>
        .dxeFocused_Kia {
            border: 1px solid #adadad !important;
        }
        body {
            overflow: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-md-12">
                <dx:ASPxCalendar ID="calDate" runat="server"></dx:ASPxCalendar>
            </div>
        </div>
        <div class="row mt10">
            <div class="col-md-12">
                <dx:ASPxButton ID="btnDateSave" ClientInstanceName="btnDateSave" runat="server" Text="Save"/>
            </div>
        </div>
    </form>
</body>
</html>
