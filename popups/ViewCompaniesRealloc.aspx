﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCompaniesRealloc.aspx.vb" Inherits="ViewCompaniesRealloc" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/normalize.min.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/master.css" />
    <link rel="stylesheet" href="../css/custom.css" />
</head>
<body>

    <form id="form1" runat="server">

        <dx:ASPxGridView ID="gridNewCustomers" runat="server" AutoGenerateColumns="False" Width="99%" KeyFieldName="Id" CssClass="mb10">
            <SettingsPager Visible="false" />
            <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False" ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False" ShowStatusBar="Hidden" ShowTitlePanel="False" ShowFooter="False" HorizontalScrollBarMode="Hidden" VerticalScrollableHeight="250" VerticalScrollBarMode="Auto" />
            <SettingsText EmptyDataRow="No alternative customers found" />
            <SettingsBehavior AllowSort="True" AutoFilterRowInputDelay="12000" ColumnResizeMode="Disabled" AllowSelectByRowClick="true" AllowSelectSingleRowOnly="True" AllowDragDrop="False" AllowGroup="False" />
            <StylesEditors>
                <ProgressBar Height="25px"></ProgressBar>
            </StylesEditors>

            <Styles>
                <Header HorizontalAlign="Center" Wrap="True" Font-Size="12px" />
                <Cell HorizontalAlign="Center" Wrap="True" Font-Size="12px" />
                <Footer HorizontalAlign="Center" Wrap="True" Font-Size="12px" />
            </Styles>

            <Columns>
                <dx:GridViewCommandColumn Caption="Select" ShowSelectCheckbox="True" VisibleIndex="0" Width="65px">
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                </dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn Caption="Customer Id" FieldName="Id" VisibleIndex="1" Width="12%" />
                <dx:GridViewDataTextColumn Caption="Experian URN" FieldName="BusinessURN" VisibleIndex="2" Width="12%" />
                <dx:GridViewDataTextColumn Caption="Name" FieldName="BusinessName" VisibleIndex="3" CellStyle-Wrap="False" />
                <dx:GridViewDataTextColumn Caption="Address" FieldName="Address1" VisibleIndex="4" CellStyle-Wrap="False" />
                <dx:GridViewDataTextColumn Caption="Postcode" FieldName="PostCode" VisibleIndex="5" Width="10%" />
                <dx:GridViewDataTextColumn Caption="Telephone Number" FieldName="TelephoneNumber" VisibleIndex="6" Width="12%" />

            </Columns>
        </dx:ASPxGridView>

        <table style="width: 99%" runat="server">
            <tr id="SelChoice" runat="server">
                <td align="left">
                    <dx:ASPxButton ID="btnSelectCustomer" ClientInstanceName="btnSelectCustomer" runat="server" Text="Use Selected Customer" Width="150px" ImagePosition="Top"></dx:ASPxButton>
                </td>
                <td align="right">
                    <dx:ASPxButton ID="btnNewCustomer" ClientInstanceName="btnNewCustomer" runat="server" Text="Create New Customer" Width="150px" ImagePosition="Top">
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr id="ConfChoice" runat="server" visible="false">
                <td align="right"></td>
                <td align="right">
                    <asp:Label ID="lblYesNo" runat="server" Text="Are you sure (this will take some time)?"> </asp:Label>
                    <dx:ASPxButton ID="btnYes" ClientInstanceName="btnYes" runat="server" Text="Yes" Width="50px" ImagePosition="Top">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="btnNo" ClientInstanceName="btnNo" runat="server" Text="No" Width="50px" ImagePosition="Top">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>

    </form>
</body>
</html>
