﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports QubeUtils

Partial Class ViewCompaniesTargetStatus

    Inherits System.Web.UI.Page

    Dim nCustomerId As Long = 0
    Dim nCustomerDealerId As Long = 0
    Dim sDealerCode As String = ""
    Dim sCurrentTargetStatus As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        nCustomerId = Request.QueryString(0)
        sDealerCode = Request.QueryString(1)
        sCurrentTargetStatus = Request.QueryString(2)

        Dim param = New SqlParameter(1){
            New SqlParameter("@CustomerId",SqlDbType.Int) With {.Value=nCustomerId},
            New SqlParameter("@DealerCode",SqlDbType.NVarChar,10) With {.Value=sDealerCode}
        }

        nCustomerDealerId = Long.Parse(MSSQLHelper.ExecuteScalar("select id from customerdealer where customerid=@CustomerId and dealercode=@DealerCode", param).ToString())

        If Not Page.IsPostBack Then
            ddlTargetStatus.Items.Add(New ListItem("No", 0))
            ddlTargetStatus.Items.Add(New ListItem("Target", 1))
            ddlTargetStatus.SelectedIndex = sCurrentTargetStatus
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim sTargetStatus_Type As Integer = 0
        sTargetStatus_Type = ddlTargetStatus.SelectedIndex

        Dim param = New SqlParameter(2){
            New SqlParameter("@Target",SqlDbType.Int) With {.Value=sTargetStatus_Type},
            New SqlParameter("@DealerCode",SqlDbType.NVarChar,10) With {.Value=sDealerCode},
            New SqlParameter("@CustomerId",SqlDbType.Int) With {.Value=nCustomerId}
        }

        MSSQLHelper.ExecuteSQL("UPDATE CustomerDealerSpecific SET Target=@Target WHERE DealerCode=@DealerCode AND CustomerId=@CustomerId", param)

        Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Target Flag Updated", "Customer Dealer Id : " & nCustomerDealerId & ", New Target Status : " & Session("NewTargetStatus"))

        Dim startUpScript As String = String.Format("window.parent.HideTargetEdit();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

End Class
