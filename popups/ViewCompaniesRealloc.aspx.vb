﻿Imports System
Imports System.Data
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Threading
Imports QubeUtils

Partial Class ViewCompaniesRealloc

    Inherits System.Web.UI.Page

    Dim nId As Integer = 0
    Dim nCustomerId As Long = 0

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Request.QueryString.Count > 0 Then
            nId = Val(Request.QueryString(0))
            If nId > 0 Then
                Call FetchDetails()
            End If
        End If
       
    End Sub

    Private Sub FetchDetails()
        Dim param As SqlParameter() = New SqlParameter(0){}
        param(0) = New SqlParameter("@CustomerId",SqlDbType.int)
        param(0).Value = nId

        nCustomerId = MSSQLHelper.ExecuteScalar("SELECT CustomerId FROM CustomerDealer WHERE Id=@CustomerId", param)
        Dim dtCustomers as DataTable = MSSQLHelper.GetDataTable("SELECT * FROM dbo.MatchCustomerList(" & Trim(Str(nId)) & ")  ORDER BY Confidence DESC", Nothing)
        
        gridNewCustomers.DataSource = dtCustomers
        gridNewCustomers.DataBind()

    End Sub

    Protected Sub gridNewCustomers_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridNewCustomers.HtmlDataCellPrepared
        If e.GetValue("Id") = nCustomerId Then
            e.Cell.BackColor = GlobalVars.g_Color_LightBlue
        End If
    End Sub

    Protected Sub btnNewCustomer_Click(sender As Object, e As EventArgs) Handles btnNewCustomer.Click
        Session("CustAction") = 1
        SelChoice.Visible = False
        ConfChoice.Visible = True
    End Sub

    Protected Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        Dim startUpScript As String = String.Format("window.parent.HideReAlloc();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

    Protected Sub btnSelectCustomer_Click(sender As Object, e As EventArgs) Handles btnSelectCustomer.Click

        Dim selectedValues As List(Of Object)
        Dim fieldNames As List(Of String) = New List(Of String)()

        fieldNames.Add("Id")
        selectedValues = gridNewCustomers.GetSelectedFieldValues(fieldNames.ToArray())
        If selectedValues.Count = 1 Then
            Session("CustAction") = 0
            Session("NewCustId") = selectedValues.Item(0).ToString
            SelChoice.Visible = False
            ConfChoice.Visible = True
        End If

    End Sub

    Protected Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click

        If Session("CustAction") = 1 Then
            Dim param = New SqlParameter(1){
                New SqlParameter("@nCustomerDealerId",SqlDbType.Int) With { .Value = nId},
                new SqlParameter("@nUserId ",SqlDbType.Int) With { .Value = Session("UserId")}
            }
            MSSQLHelper.ExecuteSQL("p_CustomerDealer_Realloc_NewCust",param, "", True)
        Else
            Dim param = New SqlParameter(2){
               New SqlParameter("@nCustomerDealerId",SqlDbType.Int) With { .Value = nId},
               New SqlParameter("@nNewCustomerId ",SqlDbType.Int) With { .Value = Session("NewCustId")},
               new SqlParameter("@nUserId ",SqlDbType.Int) With { .Value = Session("UserId")}
           }
            MSSQLHelper.ExecuteSQL("p_CustomerDealer_Realloc", param, "", True)
        End If

        Dim startUpScript As String = String.Format("window.parent.HideReAlloc();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

End Class
