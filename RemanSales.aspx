﻿<%@ Page AutoEventWireup="false" CodeFile="RemanSales.aspx.vb" Inherits="RemanSales"   Language="VB" MasterPageFile="~/MasterPage.master" Title="Reman Sales" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"     Namespace="DevExpress.Web" TagPrefix="dx" %> 


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >


<table width="100%">
    <tr>
        <td width="100%">
            <ul class="inline_list">
                <li>
                    <dx:ASPxLabel 
                         ID="lblMonthFrom" 
                         runat="server" 
                         Text="First Month of Report:">
                      </dx:ASPxLabel>
                    <dx:ASPxComboBox 
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="true"
                        DataSourceID="sqlPopMonth" 
                        TextField="periodname" 
                        ValueField="periodname" 
                        width="144px">
                      </dx:ASPxComboBox>
                </li>

                <li>
                    <dx:ASPxLabel  
                         ID="lblMonthTo" 
                         runat="server" 
                         Text="Last Month of Report:">
                      </dx:ASPxLabel>

                    <dx:ASPxComboBox 
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="true"
                        DataSourceID="sqlPopMonth" 
                        TextField="periodname" 
                        ValueField="periodname" 
                        width="144px">
                      </dx:ASPxComboBox>
                </li>

                <li>
                    <dx:ASPxLabel 
                         ID="ASPxLabel1" 
                         runat="server" 
                         Text="Data To Show:">
                      </dx:ASPxLabel>

                    <dx:ASPxComboBox 
                        ID="ddlShowType" 
                        runat="server" 
                        AutoPostBack="true"
                        width="144px">
                            <Items>
                                <dx:ListEditItem Text="Sales Value £" Selected="true" value="0"/>
                                <dx:ListEditItem Text="Units Sold"  value="1"/>
                            </Items>
                      </dx:ASPxComboBox>
                </li>



            </ul>
        </td>
    </tr>
</table>

<br />

  <dx:ASPxGridView 
        ID="gridMain" 
        OnLoad="GridStyles" 
        DataSourceID="dsMain" 
        Styles-AlternatingRow-BackgroundImage-HorizontalPosition  ="position:relative; margin-top: 5px;"
        runat="server" 
        SettingsDetail-ExportMode="Expanded"         
        AutoGenerateColumns="False"  
        Styles-AlternatingRow-BackColor = "GhostWhite"
        SettingsBehavior-ProcessSelectionChangedOnServer="true"
        KeyFieldName="customerdealerid" 
        Styles-HeaderPanel-Wrap="True"
        visible="true" 
        width="100%">

        <SettingsPager 
            Mode="ShowPager"
            PageSize="16" 
            ShowDefaultImages="True" 
            AllButton-Visible="true"/>
		
        <Settings 
            ShowFilterRowMenu="False" 
            ShowHeaderFilterBlankItems="false"
            ShowFooter="True" 
            ShowGroupedColumns="False"
            ShowGroupFooter="Hidden" 
            ShowGroupPanel="False" 
            ShowHeaderFilterButton="False"
            ShowStatusBar="Hidden" 
            ShowTitlePanel="False" />
    
        <Styles>
            <Footer HorizontalAlign="Center"></Footer>
        </Styles>

        <SettingsBehavior 
            AllowSort="True" 
            AutoFilterRowInputDelay="12000" 
            ColumnResizeMode="Control" />

        <Columns>

              	<dx:GridViewDataTextColumn 
					FieldName="zone" 
                    Caption="Zone"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
					HeaderStyle-HorizontalAlign ="Center"
					CellStyle-HorizontalAlign ="Center"
					VisibleIndex="0"
					Visible ="true" 
					ExportWidth = "100"
					Width="5%">
				</dx:GridViewDataTextColumn>

    			<dx:GridViewDataTextColumn 
					Caption="Dealer" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
		    		HeaderStyle-HorizontalAlign ="Left"
	    			CellStyle-HorizontalAlign ="Left"
					FieldName="dealercode" 
					VisibleIndex="0"
                    CellStyle-Wrap="False"
					Visible ="true" 
					Width="5%">
	    		</dx:GridViewDataTextColumn>

    			<dx:GridViewDataTextColumn 
					Caption="Dealer Name" 
					ExportWidth = "300"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="True"
					Settings-HeaderFilterMode="CheckedList"
		    		HeaderStyle-HorizontalAlign ="Left"
	    			CellStyle-HorizontalAlign ="Left"
					FieldName="dealername" 
					VisibleIndex="0"
                    CellStyle-Wrap="False"
					Visible ="true" 
					Width="10%">
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="AC Compressor" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="accompressor" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="AC Compressor" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="accompressor_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Alternator" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="alternator" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Alternator" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="alternator_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>
	
                <dx:GridViewDataTextColumn 
					Caption="Cylinder Head" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="cylinderhead" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Cylinder Head" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="cylinderhead_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Drive Shaft" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="driveshaft" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Drive Shaft" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="driveshaft_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Engine" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="engine" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Engine" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="engine_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Injection Pump" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="injectionpump" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Injection Pump" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="injectionpump_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Injectors" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="injectors" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Injectors" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="injectors_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Radiator" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="radiator" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Radiator" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="radiator_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Starter Motor" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="startermotor" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Starter Motor" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="startermotor_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Steering Gear" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="steeringgear" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Steering Gear" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="steeringgear_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Trans- mission" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="transmission" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Trans- mission" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="transmission_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Turbo charger" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="turbocharger" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Turbo charger" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="turbocharger_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Total Reman" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="remantotal" 
                    PropertiesTextEdit-DisplayFormatString="£#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
					Caption="Total Reman" 
					ExportWidth = "150"  HeaderStyle-Wrap = "True"
					Settings-AllowHeaderFilter="False"
		    		HeaderStyle-HorizontalAlign ="Center"
	    			CellStyle-HorizontalAlign ="Center"
					FieldName="remantotal_q" 
                    PropertiesTextEdit-DisplayFormatString="#,##0" width = "6%"
					VisibleIndex="0"
					Visible ="true" >
	    		</dx:GridViewDataTextColumn>

       </Columns>
    
        <TotalSummary>
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="accompressor" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="accompressor_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="alternator" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="alternator_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="cylinderhead" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="cylinderhead_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="driveshaft" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="driveshaft_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="engine" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="engine_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="injectionpump" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="injectionpump_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="injectors" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="injectors_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="radiator" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="radiator_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="startermotor" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="startermotor_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="steeringgear" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="steeringgear_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="transmission" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="transmission_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="turbocharger" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="turbocharger_q" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="£#,##0" FieldName="remantotal" SummaryType="Sum" />
            <dx:ASPxSummaryItem DisplayFormat="##,##0" FieldName="remantotal_q" SummaryType="Sum" />

       </TotalSummary>
 

   </dx:ASPxGridView>


</div>

  <asp:SqlDataSource 
        ID="dsMain" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
        SelectCommand="sp_Reman_Sales" 
        SelectCommandType="StoredProcedure">
       <SelectParameters>
             <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
             <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter Name="nfrom" SessionField="FromMonth" Type="Int32" />
            <asp:SessionParameter Name="nto" SessionField="ToMonth" Type="Int32" />
         </SelectParameters>
    </asp:SqlDataSource>
 
  <dx:ASPxGridViewExporter 
         ID="ASPxGridViewExporter1" 
          runat="server" 
          GridViewID="gridMain" >
    </dx:ASPxGridViewExporter>

  <asp:SqlDataSource 
      ID="sqlPopMonth" 
      runat="server" 
      ConnectionString="<%$ ConnectionStrings:databaseconnectionstring %>"
      SelectCommand="p_Get_Months" 
      SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>

  
</asp:Content>


