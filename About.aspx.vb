﻿Partial Class About

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sConn As String = UCase(System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString)
        Dim i1 As Integer
        Dim i2 As Integer
        Dim i3 As Integer
        Dim i4 As Integer

        Me.Title = "NissanDATA PARTSy - About (System Information)"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        lblApplicationVersion.Text = GetDataString("SELECT SystemVersion FROM System")
        lblWebRefresh.Text = GetDataString("SELECT LastWebRefresh FROM System")

        i1 = InStr(sConn, "=")
        i2 = InStr(sConn, ";")
        If i1 > 0 And i2 > 0 Then
            lblDBIP.Text = Mid(sConn, i1 + 1, (i2 - (i1 + 1)))
        End If

        i3 = InStr(i2 + 1, sConn, "=", )
        i4 = InStr(i2 + 1, sConn, ";")
        If i1 > 0 And i2 > 0 Then
            lblDBName.Text = Mid(sConn, i3 + 1, (i4 - (i3 + 1)))
        End If

    End Sub

End Class
