﻿Imports System.Data
Imports System.Net.Mail

Partial Class VisitActionsDate

    Inherits System.Web.UI.Page

    Dim nType As String
    Dim nId As Integer

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nType = Left(Request.QueryString(0), 1)
        nId = Mid(Request.QueryString(0), 2)

        If Not Page.IsPostBack Then
            Dim sDate As String
            If nType = 1 Then
                sDate = GetDataString("SELECT CompletionDate FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId)))
            Else
                sDate = GetDataString("SELECT ConfirmedDate FROM Web_VAR_Details WHERE Id = " & Trim(Str(nId)))
            End If
            If sDate <> "" Then
                calDate.SelectedDate = CDate(sDate)
            Else
                calDate.SelectedDate = CDate(Now)
            End If
        End If

    End Sub

    Protected Sub btnDateSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDateSave.Click

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long
        Dim sDate As String = Format(CDate(calDate.SelectedDate), "dd-MMM-yyyy")

        htIn.Add("@nId", nId)
        htIn.Add("@nType", nType)
        htIn.Add("@sDate", sDate)
        htIn.Add("@nUserId", Session("UserId"))
        nRes = da.Update(sErr, "p_VAR_Detail_UpdDate", htIn)

        If sErr.Length = 0 Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Activity date updated", "Id : " & nId)
            Dim startUpScript As String = String.Format("window.parent.HideVisitActionDateWindow();")
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Sub

End Class
