﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports DevExpress.Web
Imports System.Data
Imports QubeUtils
Imports System.Data.SqlClient

Partial Class HOBasketComparison

    Inherits System.Web.UI.Page
    Dim nSales1 As Decimal = 0
    Dim nSales2 As Decimal = 0
    Dim nCost1 As Decimal = 0
    Dim nCost2 As Decimal = 0
    Dim da As New DatabaseAccess
    Dim ds As DataSet
    Dim sErr As String = ""
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Me.Title = "Nissan Trade Site - Sales Comparison Reporting"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Or Session("FiltersCleared") = True Then

            Session("FiltersCleared") = False

            '  Reset Everything
            cboReportLevel.SelectedIndex = 0
            ddlFrom.SelectedIndex = 0
            ddlType.SelectedIndex = 0
            ddlReportType.SelectedIndex = 0
            rblAll.SelectedIndex = 0
            txtProductList.Text = "All Basket Products"
            gridCompare.FilterEnabled = False


            ' Default to last complete month 
            Session("ToThis") = GetDataDecimal("select dbo.getMaxInvoiceDataPeriod()") - 1  ' This returns the max COMPLETED month for which we have data in web_qube  - Important on first day of month when no data for this month loaded yet.
            Session("FromThis") = Session("ToThis")

            ' Default to last complete month before that     ( so in Feb 2020 Session("ToThis")  would be 169 (Jan 2020) and Session("ToLast") would be 168 (Dec 2019)
            Session("ToLast") = Session("ToThis") - 1
            Session("FromThis") = Session("ToLast")

            Session("FromLast") = Session("ToLast")
            Session("IncludedProductString") = "ALL,"

            Session("ReportLevel") = 1  ' Default to Full Detail Report
            Session("ReportType") = 1  ' Default to Sale Value 

            Call RefreshGrid(True)

            gridCompare.Visible = True

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Comparison Report", "Viewing Comparison Report")
        End If

        Call BindGrid()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If


    End Sub

    'Protected Sub gridCompare_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridCompare.DataBound

    '    Dim sFromMonthThis As String = GetPeriodName(CInt(Session("FromThis")))
    '    Dim sToMonthThis As String = GetPeriodName(CInt(Session("ToThis")))
    '    Dim sFromMonthLast As String = GetPeriodName(CInt(Session("FromLast")))
    '    Dim sToMonthLast As String = GetPeriodName(CInt(Session("ToLast")))


    Protected Sub gridCompare_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCompare.HtmlDataCellPrepared

        Dim nThisValue As Decimal = 0
        Dim sThisDealerCode As String = ""


        If e.DataColumn.Caption = "" Then
            e.Cell.Width = 2
        End If

        If e.DataColumn.Caption = "Comparison %" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.Font.Bold = True
            e.Cell.ForeColor = System.Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.ForeColor = System.Drawing.Color.White
            ElseIf nThisValue >= 1 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            Else
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If

        If e.DataColumn.Caption = "Comparison" Then
            If IsDBNull(e.CellValue) = True Then
                nThisValue = 0
            Else
                nThisValue = CDec(e.CellValue)
            End If
            e.Cell.Font.Bold = True
            e.Cell.ForeColor = System.Drawing.Color.White
            If nThisValue = 0 Then
                e.Cell.Font.Bold = False
                e.Cell.BackColor = System.Drawing.Color.White
                e.Cell.ForeColor = System.Drawing.Color.White
            ElseIf nThisValue >= 0 Then
                e.Cell.BackColor = System.Drawing.Color.Green
            Else
                e.Cell.BackColor = System.Drawing.Color.Red
            End If
        End If



    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "Sales1" Or e.Column.FieldName = "Sales2" Then
            e.DisplayText = Format(e.Value, "£#,##0")
        End If
        If e.Column.FieldName = "Variance" Then
            e.DisplayText = Format(e.Value, "#,##0.00") & "%"
        End If

    End Sub

    Protected Sub dsComparison_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsBasketDetailThis.Init
        dsBasketDetailThis.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsBasketDetailLast.ConnectionString = dsBasketDetailThis.ConnectionString
        dsMonths.ConnectionString = dsBasketDetailThis.ConnectionString
        dsProductGroups.ConnectionString = dsBasketDetailThis.ConnectionString
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged, ddlType.SelectedIndexChanged
        Call RefreshGrid(False)
    End Sub

    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        Session("ReportType") = ddlReportType.Value
        Call RefreshGrid(False)
    End Sub


    Private Sub RefreshGrid(bStart As Boolean)

        Dim sTextColFromThis As String = ""
        Dim sTextColToThis As String = ""
        Dim sTextColFromLast As String = ""
        Dim sTextColToLast As String = ""
        Dim sThisText As String = ""
        Dim sLastText As String = ""



        If bStart = False Then
            Session("ToThis") = ddlFrom.SelectedItem.Value
        End If

        Select Case ddlType.SelectedItem.Value
            Case 0     '  "Monthly - Compare to last month"
                Session("FromThis") = Session("ToThis")
                Session("ToLast") = Session("ToThis") - 1
                Session("FromLast") = Session("FromThis") - 1
            Case 1    ' "Monthly - Compare to 12 months ago"
                Session("FromThis") = Session("ToThis")
                Session("ToLast") = Session("ToThis") - 12
                Session("FromLast") = Session("FromThis") - 12
            Case 2   ' "Quarterly - Compare to last quarter"
                Session("FromThis") = Session("ToThis") - 2
                Session("ToLast") = Session("ToThis") - 3
                Session("FromLast") = Session("FromThis") - 3
            Case 3   ' "Quarterly - Compare to same quarter 12 months ago"
                Session("FromThis") = Session("ToThis") - 2
                Session("ToLast") = Session("ToThis") - 12
                Session("FromLast") = Session("FromThis") - 12
            Case 4   ' "Yearly - last 12 months vs. preceding 12 months"
                Session("FromThis") = Session("ToThis") - 11
                Session("ToLast") = Session("ToThis") - 12
                Session("FromLast") = Session("FromThis") - 12
            Case 5   ' "Calendar Year To Date -  vs. equivalent last year"
                Session("FromThis") = GetDataLong("Select min(id) From DataPeriods where calendaryear = (select calendaryear from DataPeriods where id = " & Session("ToThis") & ")")
                Session("ToLast") = Session("ToThis") - 12
                Session("FromLast") = Session("FromThis") - 12
            Case 6   ' "Financial Year To Date -  vs. equivalent last year"
                Session("FromThis") = GetDataLong("Select min(id) From DataPeriods where financialyear = (select financialyear from DataPeriods where id = " & Session("ToThis") & ")")
                Session("ToLast") = Session("ToThis") - 12
                Session("FromLast") = Session("FromThis") - 12
        End Select

        sTextColFromThis = GetPeriodName(Session("FromThis"))
        sTextColToThis = GetPeriodName(Session("ToThis"))
        sTextColFromLast = GetPeriodName(Session("FromLast"))
        sTextColToLast = GetPeriodName(Session("ToLast"))

        If sTextColFromThis <> sTextColToThis Then
            sThisText = sTextColFromThis & " to " & sTextColToThis
        Else
            sThisText = sTextColFromThis
        End If

        If sTextColFromLast <> sTextColToLast Then
            sLastText = sTextColFromLast & " to " & sTextColToLast
        Else
            sLastText = sTextColFromLast
        End If

        gridCompare.Columns("bndCompareBasket").Caption = "Basket Products"
        gridCompare.Columns("bndCompareFull").Caption = "All Products"
        gridCompare.Columns("BasketThis").Caption = sThisText
        gridCompare.Columns("BasketLast").Caption = sLastText
        gridCompare.Columns("FullThis").Caption = sThisText
        gridCompare.Columns("FullLast").Caption = sLastText


        gridCompare.DataBind()
        gridCompare.Visible = True
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Call GlobalRenderBrick(e)

        Dim nThisValue As Decimal = 0

        If e.RowType = DevExpress.Web.GridViewRowType.Data Then

            If e.Column.Name = "Sales1" Or e.Column.Name = "Sales2" Then
                e.TextValueFormatString = "c0"
            End If

            If e.Column.Name = "Variance" Then
                If IsDBNull(e.Value) = True Then
                    nThisValue = 0
                Else
                    nThisValue = CDec(e.Value)
                End If
                e.TextValue = Format(e.Value, "#,##0.00") & "%"
                If nThisValue = 0 Then
                    e.BrickStyle.BackColor = System.Drawing.Color.White
                    e.BrickStyle.ForeColor = System.Drawing.Color.Black
                ElseIf nThisValue > 0 Then
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                    e.BrickStyle.BackColor = System.Drawing.Color.Green
                Else
                    e.BrickStyle.BackColor = System.Drawing.Color.Red
                    e.BrickStyle.ForeColor = System.Drawing.Color.White
                End If
            End If

        End If

    End Sub

    Protected Sub btnExcel_Click()

        ASPxGridViewExporter1.FileName = "BasketComparisonReport"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub
    Protected Sub GridStylesOff(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub
    Protected Sub dsMonths_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsMonths.Init
        dsMonths.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridBasketDetail_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("CustomerDealerID") = sKey
        Session("SubHeading") = "Basket Parts Sales To " & GetDataString("SELECT businessname FROM customerdealer WHERE id =" & Session("CustomerDealerID"))

    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim dealercode = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "DealerCode").ToString()

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealercode, invoiceId), invoiceId)

    End Sub


    Protected Sub pgcDetail_DataBound(sender As Object, e As EventArgs)
        sender.tabpages(0).text = gridCompare.Columns("BasketThis").Caption
        sender.tabpages(1).text = gridCompare.Columns("BasketLast").Caption
    End Sub

    Private Sub BindGrid()

        Dim csvItems As DataTable = New DataTable
        csvItems.Columns.Add("ItemName", GetType(System.String))

        Dim ItemString As String = Session("IncludedProductString")
        Dim ItemCount As Integer = Len(Trim(ItemString)) / 4  ' 3 digit code + comma
        Dim nLoop As Integer = 1
        Dim sThisItem As String = ""
        Dim dr As DataRow = csvItems.NewRow

        If isEmpty(ItemString) = True Then ItemString = "ALL,"

        While nLoop <= ItemCount
            sThisItem = Mid(ItemString, (ItemCount * 4) - 3, 3)
            csvItems.Rows.Add(sThisItem)
            nLoop = nLoop + 1
        End While

        If Session("ReportLevel") = 1 Then
            gridCompare.Columns("bndCustomer").Visible = True
            Dim param = New SqlParameter(7) {
            New SqlParameter("@SelectionLevel", SqlDbType.Char, 1) With {.Value = Session("SelectionLevel")},
            New SqlParameter("@SelectionId", SqlDbType.VarChar, 6) With {.Value = Session("SelectionId")},
            New SqlParameter("@FromThis", SqlDbType.Int) With {.Value = Session("FromThis")},
            New SqlParameter("@ToThis", SqlDbType.Int) With {.Value = Session("ToThis")},
            New SqlParameter("@FromLast", SqlDbType.Int) With {.Value = Session("FromLast")},
            New SqlParameter("@ToLast", SqlDbType.Int) With {.Value = Session("ToLast")},
            New SqlParameter("@ReportType", SqlDbType.Int) With {.Value = Session("ReportType")},
            New SqlParameter("@ProductList", SqlDbType.Structured) With {.Value = csvItems}
            }
            With gridCompare
                .DataSource = MSSQLHelper.GetDataTable("p_HOBasketSalesComparison", param, "", True)
                .SettingsDetail.ShowDetailRow = True
                .DataBind()
            End With
        Else
            gridCompare.Columns("bndCustomer").Visible = False
            Dim param = New SqlParameter(7) {
            New SqlParameter("@SelectionLevel", SqlDbType.Char, 1) With {.Value = Session("SelectionLevel")},
            New SqlParameter("@SelectionId", SqlDbType.VarChar, 6) With {.Value = Session("SelectionId")},
            New SqlParameter("@FromThis", SqlDbType.Int) With {.Value = Session("FromThis")},
            New SqlParameter("@ToThis", SqlDbType.Int) With {.Value = Session("ToThis")},
            New SqlParameter("@FromLast", SqlDbType.Int) With {.Value = Session("FromLast")},
            New SqlParameter("@ToLast", SqlDbType.Int) With {.Value = Session("ToLast")},
            New SqlParameter("@ReportType", SqlDbType.Int) With {.Value = Session("ReportType")},
            New SqlParameter("@ProductList", SqlDbType.Structured) With {.Value = csvItems}
            }
            With gridCompare
                .DataSource = MSSQLHelper.GetDataTable("p_HOBasketSalesComparisonSummary", param, "", True)
                .SettingsDetail.ShowDetailRow = False
                .DataBind()
            End With


        End If



    End Sub
    Protected Sub cboProduct_Init(sender As Object, e As EventArgs)

        Dim sSQL As String = "EXEC sp_PartsBasketProductGroups"
        Dim sThisValue As String = ""
        Dim sThisText As String = ""

        ds = da.ExecuteSQL(sErr, sSQL)

        Dim nRowCount As Integer = ds.Tables(0).Rows().Count - 1
        Dim nLoop As Integer = 1

        With ds.Tables(0)
            For nLoop = 0 To nRowCount
                sThisValue = ds.Tables(0).Rows(nLoop).Item(0)
                sender.items.add(sThisValue)
            Next

        End With

    End Sub

    Protected Sub cboProduct_ValueChanged(sender As Object, e As EventArgs)
        Dim nLoop As Integer = 0
        Dim sIncludeString As String = ""

        While nLoop < 19
            If sender.items(nLoop).Selected = True Then
                sIncludeString &= Left(sender.items(nLoop).text, 3) & ","
            End If
            nLoop = nLoop + 1
        End While

        If isEmpty(sIncludeString) = True Then
            sIncludeString = "ALL"
        End If

        Session("IncludedProductString") = sIncludeString

        If Session("IncludedProductString") <> "ALL" Then
            txtProductList.Text = Left(Session("IncludedProductString"), Len(Session("IncludedProductString")) - 1)
        Else
            txtProductList.Text = "All Basket Products"
        End If

    End Sub

    Protected Sub btnProducts_Click(sender As Object, e As EventArgs)
        popProducts.ShowOnPageLoad = True
    End Sub
    Protected Sub btnClose_Click(sender As Object, e As EventArgs)
        popProducts.ShowOnPageLoad = False
    End Sub
    Protected Sub btnClearFilters_Click(sender As Object, e As EventArgs)
        Session("FiltersCleared") = True
    End Sub
    Protected Sub rblAll_SelectedIndexChanged(sender As Object, e As EventArgs)
        If sender.value = 0 Then
            cblProducts.UnselectAll()
            cblProducts.Enabled = False
            Session("IncludedProductString") = "ALL"
            txtProductList.Text = "All Basket Products"
            popProducts.ShowOnPageLoad = False
        Else
            cblProducts.Enabled = True
        End If

    End Sub
    Protected Sub cboReportLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboReportLevel.SelectedIndexChanged
        Session("ReportLevel") = cboReportLevel.Value
    End Sub

End Class
