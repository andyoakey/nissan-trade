﻿Imports System.Data
Imports System.Drawing
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class RebateCampaign

    Inherits System.Web.UI.Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim nTotalValue(2) As Double
    Dim nTotalUnits(2) As Double

    Protected Sub dsDrillDown0_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDrillDown0.Init
        dsDrillDown0.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsDrillDown1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
        dsDrillDown2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    '    Protected Sub dsDrillDown1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsDrillDown1.Init
    '       dsDrillDown1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    '  End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dMaxDate As Date

        Me.Title = "Rebate Campaign"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Session("DecimalPlaces") = 2
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Rebate Campaign", "Rebate Campaign")
            dMaxDate = GetMaxDate("N", "O")
            Dim nCurrentPeriod As Integer = GetDataDecimal("select ID from dataperiods WHERE '" & TextDate(dMaxDate) & "' between startdate and enddate")
            Session("MonthFrom") = nCurrentPeriod - 1
            Session("MonthTo") = nCurrentPeriod
            Call GetMonthsDevX(ddlFrom)
            ddlFrom.SelectedIndex = 1
            Call GetMonthsDevX(ddlTo)
        End If
        Session("CallingModule") = "RebateCampaign.aspx - PageLoad"

        Session("ShowOtherParts") = 0

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        Call LoadCampaignData()
        Call SetDecimals()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Session("SelectionLevel") = "C" Then
            gridCampaigns.Settings.ShowFooter = False
        Else
            gridCampaigns.Settings.ShowFooter = True
        End If



        'select rebatename,id from RebateCampaignControl order by id desc



    End Sub

    Sub LoadCampaignData()

        Dim htIn As New Hashtable
        htIn.Clear()
        htIn.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htIn.Add("@nFrom", Session("MonthFrom"))
        htIn.Add("@nTo", Session("MonthTo"))

        dsResults = da.Read(sErrorMessage, "p_RebateCampaignSummary", htIn)
        gridCampaigns.DataSource = dsResults.Tables(0)
        gridCampaigns.DataBind()

    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "RebateCampaign"

        Call LoadCampaignData()
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsToResponse(New XlsExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

    End Sub
    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared

	 '''' Value Swapped for Cost JIRA NMGBM-216       
	 ''''If e.DataColumn.FieldName = "Value_Diff" Or e.DataColumn.FieldName = "Value_DiffPerc" Or e.DataColumn.FieldName = "Units_Diff" Or e.DataColumn.FieldName = "Units_DiffPerc" Then
         If e.DataColumn.FieldName = "Cost_Diff" Or e.DataColumn.FieldName = "Cost_DiffPerc" Or e.DataColumn.FieldName = "Units_Diff" Or e.DataColumn.FieldName = "Units_DiffPerc" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue < 0 Then
                e.Cell.BackColor = Color.Red
            Else
                e.Cell.BackColor = Color.Green
            End If
        End If

        If e.DataColumn.FieldName = "Rebate" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue > 0 Then
                e.Cell.BackColor = Color.Green
            End If
        End If

        If e.DataColumn.FieldName = "Missed_Rebate" Then
            e.Cell.ForeColor = Color.White
            If e.CellValue <> 0 Then
                e.Cell.BackColor = Color.Red
            End If
        End If

    End Sub

    Protected Sub gridDrillDown_OnHtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Rebated_Value" Then
            e.DataColumn.PropertiesEdit.DisplayFormatString = "&#163;#,##0"
        End If


        If e.DataColumn.FieldName = "ActualRebate" Then
            e.Cell.ForeColor = Color.White

            If sender.id = "gridDrillDown0" Then
                e.Cell.BackColor = Color.Green   'Rebate Earned - show in Green
            End If

            If sender.id = "gridDrillDown1" Then
                e.Cell.BackColor = Color.Red   'Rebate Earned - show in Red
            End If

        End If

    End Sub



    'Protected Sub gridCampaigns_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridCampaigns.CustomSummaryCalculate

    '    If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
    '        nTotalUnits(0) = nTotalUnits(0) + e.GetValue("Units_This")
    '        nTotalUnits(1) = nTotalUnits(1) + e.GetValue("Units_Last")
    '        nTotalValue(0) = nTotalValue(0) + e.GetValue("Value_This")
    '        nTotalValue(1) = nTotalValue(1) + e.GetValue("Value_Last")
    '    End If

    '    If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
    '        Dim objSummaryItem As DevExpress.Web.ASPxSummaryItem = CType(e.Item, DevExpress.Web.ASPxSummaryItem)

    '        If objSummaryItem.FieldName = "Value_DiffPerc" Then
    '            If nTotalValue(0) <> 0 And nTotalValue(1) <> 0 Then
    '                e.TotalValue = (nTotalValue(0) - nTotalValue(1)) / nTotalValue(1)
    '            Else
    '                e.TotalValue = 0
    '            End If
    '        End If

    '        If objSummaryItem.FieldName = "Units_DiffPerc" Then
    '            If nTotalUnits(0) <> 0 And nTotalUnits(1) <> 0 Then
    '                e.TotalValue = (nTotalUnits(0) - nTotalUnits(1)) / nTotalUnits(1)
    '            Else
    '                e.TotalValue = 0
    '            End If
    '        End If

    '    End If

    'End Sub
    Protected Sub gridDrillDown_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("RebateDealer") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

    Sub SetDecimals()

        ' Set the number of Decimal Places Based on User Input ,  The 3rd Parameter indicates if the Total Summary needs changing too
        Dim DecimalPlaces As Integer = Session("DecimalPlaces")
        
	Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebated_Value"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebated_Value").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebated_Cost"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebated_Cost").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Rebate"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Rebate").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Value"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Value").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Cost"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Cost").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Missed_Rebate"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Missed_Rebate").DisplayFormat = "C" & DecimalPlaces


        'Call SetDecimalPlacesLocal(gridCampaigns.Columns("Value_This"), DecimalPlaces, True)
        'gridCampaigns.TotalSummary("Value_This").DisplayFormat = "C" & DecimalPlaces

        'Call SetDecimalPlacesLocal(gridCampaigns.Columns("Value_Last"), DecimalPlaces, True)
        'gridCampaigns.TotalSummary("Value_Last").DisplayFormat = "C" & DecimalPlaces

        'Call SetDecimalPlacesLocal(gridCampaigns.Columns("Value_Diff"), DecimalPlaces, True)
        'gridCampaigns.TotalSummary("Value_Diff").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_This"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_This").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_Last"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_Last").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(gridCampaigns.Columns("Cost_Diff"), DecimalPlaces, True)
        gridCampaigns.TotalSummary("Cost_Diff").DisplayFormat = "C" & DecimalPlaces
    End Sub

    Protected Sub chkRound_CheckedChanged(sender As Object, e As EventArgs)
        If chkRound.Checked Then
            Session("DecimalPlaces") = 0
        Else
            Session("DecimalPlaces") = 2
        End If

    End Sub

    Protected Sub gridDrillDown_DataBinding(sender As Object, e As EventArgs)

        Dim DecimalPlaces As Integer = Session("DecimalPlaces")

        Call SetDecimalPlacesLocal(sender.Columns("TotalSaleValue"), DecimalPlaces, True)
        sender.TotalSummary("TotalSaleValue").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(sender.Columns("TotalCostValue"), DecimalPlaces, True)
        sender.TotalSummary("TotalCostValue").DisplayFormat = "C" & DecimalPlaces

        Call SetDecimalPlacesLocal(sender.Columns("UnitSaleValue"), DecimalPlaces, True)

        Call SetDecimalPlacesLocal(sender.Columns("RebatePrice"), DecimalPlaces, True)

        If sender.id <> "gridDrillDown2" Then
           Call SetDecimalPlacesLocal(sender.Columns("ActualRebate"), DecimalPlaces, True)
            sender.TotalSummary("ActualRebate").DisplayFormat = "C" & DecimalPlaces
        End If

        Call SetDecimalPlacesLocal(sender.Columns("nettprofit"), DecimalPlaces, True)
        sender.TotalSummary("nettprofit").DisplayFormat = "C" & DecimalPlaces


    End Sub

    Sub SetDecimalPlacesLocal(thiscolumn As GridViewDataTextColumn, DecimalPlaces As Integer, AlterSummary As Boolean)

        Dim localColumn As GridViewDataTextColumn = TryCast(thiscolumn, GridViewDataTextColumn)
        Dim totalSummary As ASPxSummaryItem = New ASPxSummaryItem()
        Dim sLocalFieldName As String = localColumn.FieldName

        localColumn.PropertiesTextEdit.DisplayFormatString = "C" & DecimalPlaces


        '  Cannot get this to work 
        'If AlterSummary Then
        '    totalSummary.FieldName = sLocalFieldName
        '    totalSummary.ShowInColumn = sLocalFieldName
        '    totalSummary.DisplayFormat = "C" & DecimalPlaces
        'End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(sender As Object, e As ASPxGridViewExportRenderingEventArgs)
        Call GlobalRenderBrick(e)
    End Sub
    Protected Sub pageControlDetail_Load(sender As Object, e As EventArgs)
        If Session("ShowOtherParts") = 1 Then
            sender.tabpages(2).visible = True
        Else
            sender.tabpages(2).visible = False
        End If
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim dealerCode = Left(templateContainer.Grid.GetRowValues(nRowVisibleIndex, "id").ToString(), 4)

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub


    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub


End Class
