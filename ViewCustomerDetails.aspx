﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ViewCustomerDetails.aspx.vb" Inherits="ViewCustomerDetails" %>


<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dxs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language="javascript">
        function ContactHistRemove(contentUrl) {
            PopupHistoryLine.SetContentUrl(contentUrl);
            PopupHistoryLine.SetHeaderText('Click the Remove button to continue');
            PopupHistoryLine.SetSize(320, 90);
            PopupHistoryLine.Show();
        }
        function HideContactHistoryWindow() {
            PopupHistoryLine.Hide();
            gridContactHistory.Refresh();
        }
    </script>

    <dxs:ASPxSpellChecker 
        ID="ASPxSpellChecker2" 
        runat="server" 
        ClientInstanceName="spellChecker" 
        Culture="English (United States)" 
        SettingsText-FinishSpellChecking ="Spell check complete" 
        SettingsText-SpellCheckFormCaption ="Spell Checker">
        <Dictionaries>
            <dxs:ASPxSpellCheckerISpellDictionary 
                AlphabetPath="~/App_Data/Dictionaries/EnglishAlphabet.txt"
                GrammarPath="~/App_Data/Dictionaries/english.aff" 
                DictionaryPath="~/App_Data/Dictionaries/american.xlg"
                CacheKey="ispellDic" 
                Culture="English (United States)" 
                EncodingName="Western European (Windows)">
            </dxs:ASPxSpellCheckerISpellDictionary>
        </Dictionaries>
       
         <ClientSideEvents 
            BeforeCheck="function(s, e) { checkButton.SetEnabled(false);}"
            AfterCheck="function(s, e) { checkButton.SetEnabled(true);}" 
            CheckCompleteFormShowing="function(s, e) {e.cancel=true;} " />
    </dxs:ASPxSpellChecker>

    <dx:ASPxPopupControl 
        ID="PopupNewHistory" 
        runat="server" 
        ShowOnPageLoad="False" 
        ClientInstanceName="PopupUserAction" 
        Modal="True" 
        CloseAction="CloseButton" 
        AllowDragging="True" 
        PopupHorizontalAlign="WindowCenter" 
        PopupVerticalAlign="WindowCenter" >
         <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol1" runat="server">

                <table id="HistTable" style="width: 660px">
                    <tr>
                        <td style="width: 20%" valign="top">
                            <dx:ASPxLabel ID="lblContactDate" runat="server" Text="Date"></dx:ASPxLabel>
                            <br />
                          
                            <dx:ASPxDateEdit ID="txtContactDate" runat="server" Width="120px">
                            </dx:ASPxDateEdit>
                           
                             <asp:CheckBox ID="chkReminder" runat="server" Text="Reminder?" Checked="True" Style="margin-left: -3px" />
                         
                               <br />
                            <dx:ASPxDateEdit ID="txtReminderDate" runat="server" Width="120px">
                            </dx:ASPxDateEdit>
                        
                        </td>
                        <td style="width: 80%" valign="top">
                       
                            <dx:ASPxLabel ID="lblDetails" runat="server" Text="Details"></dx:ASPxLabel>
                     
                             <br />
                  
                              <dx:ASPxMemo ID="txtContact" runat="server"  Width="98%" Height="100px"/> 
                    </tr>
                    <tr>
                        <td style="width: 20%" valign="top">
                        </td>
                        <td valign="top" align="right" >

                            <dx:ASPxButton 
                                ID="checkButton" 
                                runat="server" 
                                ClientInstanceName="checkButton" 
                                ClientEnabled="True" 
                                Text="Check Spelling ..." 
                                AutoPostBack="False" 
                                Width="125px" 
                                style="margin-right:10px">
                                <ClientSideEvents Click="function(s, e) { spellChecker.CheckElementsInContainer(document.getElementById('HistTable')) }" />
                            </dx:ASPxButton>
    
                            <dx:ASPxButton 
                                ID="btnNewContactSave" 
                                ClientInstanceName="btnNewContactSave" 
                                runat="server" 
                                Text="Save" 
                                Width="125px" 
                                style="margin-right:5px">
                            </dx:ASPxButton>

                        </td>
                    </tr>
                </table>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <HeaderStyle>
            <Paddings PaddingRight="6px" />
        </HeaderStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl 
        ID="PopupHistoryLine" 
        ClientInstanceName="PopupHistoryLine" 
        Width="275px"
        runat="server" 
        ShowOnPageLoad="False" 
        ShowHeader="True" 
        HeaderText=""
        Modal="True"
        CloseAction="CloseButton" 
        PopupHorizontalAlign="WindowCenter"  
        PopupVerticalAlign="WindowCenter">
        <ContentCollection>
            <dx:PopupControlContentControl ID="Popupcontrolcontentcontrol2" runat="server">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <div style="position: relative; font-family: Calibri; text-align: left;" >

        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="3" Width="100%" CssClass="page_tabs" Height="500px">

            <TabPages>

                <dx:TabPage Text="Customer Details">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server">
                     
                                        <br /><br />

                                        <div class="row100">
                                            <dx:ASPxLabel ID="lblBusinessName" runat="server" Text="Business Name"   />
                                        </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtBusinessName" runat="server" Width="300px" TabIndex="2" Enabled="false" MaxLength="100" />
                                        </div>                 

                                       <div class="row700">
                                            <dx:ASPxLabel ID="lblBusinessType" runat="server" Text="Business Type" CssClass="row700"/>
                                        </div>                 

                                       <div class="row850">
                                            <dx:ASPxComboBox ID="ddlBusinessType" runat="server" Width="305px" Enabled="false" CssClass="row850"/>
                                        </div>                 

                                      <br /><br />

                                       <div class="row100">
                                            <dx:ASPxLabel ID="lblAddress1" runat="server" Text="Address" />
                                        </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtAddress1" runat="server" Width="300px" TabIndex="3" Enabled="true" MaxLength="100" />
                                        </div>                 

                                        <div class="row700">
                                            <dx:ASPxLabel ID="lblExperianCategory" runat="server" Text="Experian Category" />
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxTextBox ID="txtYPCategory" runat="server" Width="300px" TabIndex="13"  Enabled="false"/>
                                        </div>                 
                                                                   
                                       <br /><br />
                            
                                   
                                       <div class="row100">
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtAddress2" runat="server" Width="300px" TabIndex="4" />
                                        </div>                 

                                        <div class="row700">
                                            <dx:ASPxLabel ID="lblExperianURN" runat="server" Text="Experian URN" />
                                        </div>                 

                                        <div class="row850">
                                             <dx:ASPxTextBox ID="txtYPURN" runat="server" Width="300px" TabIndex="13"  Enabled="false"/>
                                        </div>                 
            
                                        <br /><br />
    
                                       <div class="row100">
                                         </div>                 

                                       <div class="row250">
                                                <dx:ASPxTextBox ID="txtAddress3" runat="server" Width="300px" TabIndex="6" MaxLength="100" />
                                        </div>                 

                                        <div class="row700">
                                                    <dx:ASPxLabel ID="lblTelephoneNumber" runat="server" Text="Telephone Number" />
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxTextBox ID="txtTelephone" runat="server" Width="300px" TabIndex="14" MaxLength="100" />
                                        </div>                 
            
                                        <br /><br />
                                   
                                       <div class="row100">
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtAddress4" runat="server" Width="300px" TabIndex="7" MaxLength="100" />
                                        </div>                 

                                        <div class="row700">
                                            <dx:ASPxLabel ID="lblMobileNumber" runat="server" Text="Mobile Number" />
                                        </div>                 

                                        <div class="row850">
                                           <dx:ASPxTextBox ID="txtMobile" runat="server" Width="300px" TabIndex="15" MaxLength="100" />
                                        </div>                 

                                        <br /><br />
                                   
                                       <div class="row100">
                                            <dx:ASPxLabel ID="lblTown" runat="server" Text="Town" />
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtAddress5" runat="server" Width="300px" TabIndex="7" MaxLength="100" />
                                        </div>                 

                                        <div class="row700">
                                            <dx:ASPxLabel ID="lblFaxNumber" runat="server" Text="Fax Number" />
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxTextBox ID="txtFaxNumber" runat="server" Width="300px" TabIndex="16" MaxLength="100" />
                                        </div>                 
                            
                            
                                        <br /><br />
                             
                                       <div class="row100">
                                              <dx:ASPxLabel ID="lblCounty" runat="server" Text="County" />
                                       </div>                 

                                       <div class="row250">
                                                <dx:ASPxTextBox ID="txtAddress6" runat="server" Width="300px" TabIndex="8" />
                                        </div>                 

                                        <div class="row700">
                                                <dx:ASPxLabel ID="lblEmailAddress" runat="server" Text="Email Address" />
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxTextBox ID="txtEmailAddress" runat="server" Width="300px" TabIndex="17" MaxLength="100" />
                                        </div>                 
                            
                                        <br /><br />
                             
                                       <div class="row100">
                                            <dx:ASPxLabel ID="lblPostCode" runat="server" Text="Postcode" />                                        
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtPostCode" runat="server" Width="100px" TabIndex="9" />
                                        </div>                 

                                        <div class="row700">
                                             <dx:ASPxLabel ID="lblWebsite" runat="server" Text="Website" />
                                        </div>                 

                                        <div class="row850">
                                                <dx:ASPxTextBox ID="txtWebSite" runat="server" Width="300px" TabIndex="18" MaxLength="100" />
                                        </div>                 
                            
                                        <br /><br />
                             
                                       <div class="row100">
                                            <dx:ASPxLabel ID="lblKeyContactName" runat="server" Text="Key Contact Name" />
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxTextBox ID="txtKeyContactName" runat="server" Width="300px" TabIndex="11" />
                                        </div>                 

                                        <div class="row700">
                                            <dx:ASPxLabel ID="lblKeyContactJobTitle" runat="server" Text="Key Contact Job Title" />
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxTextBox ID="txtKeyContactJobTitle" runat="server" Width="300px" TabIndex="20" Rows="100" />
                                        </div>                 

                                        <br /><br />
                             
                                       <div class="row100">
                                        <dx:ASPxLabel ID="lblFocus" runat="server" Text="Focus Status" />
                                       </div>                 

                                       <div class="row250">
                                        <dx:ASPxComboBox ID="cboFocus" runat="server" Width="305px" TabIndex="21">
                                           <Items>
                                                <dx:ListEditItem Text ="No" Value="0" Selected="true" />
                                                <dx:ListEditItem Text ="Dealer" Value="1" />
                                                <dx:ListEditItem Text ="OEC" Value="2" />
                                            </Items>
                                       </dx:ASPxComboBox>
                                        </div>                 


                                       <br /><br />
                                       <br /><br />

                            
                            
                                         <div class="row700">
                                                <dx:ASPxLabel ID="lblCustomerConsent" runat="server" Text="Customer Consent Preferences: " Font-Bold="true" Visible="false"/>
                                        </div>                 

                                        <div class="row950">
                                                <dx:ASPxLabel ID="lblConsentUser" Font-Italic="true" runat="server" Visible="false"  />
                                        </div>                 
                                        
                                        <br /><br />


                                        <div class="row700">
                                                <dx:ASPxLabel ID="lblNoContact" runat="server" Text="No Contact"  Visible="false"/>
                                        </div>                 

                                        <div class="row800">
                                                <dx:ASPxImage ID ="boxContact_Ticked" ImageUrl="Images2014/boxticked.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxContact_Empty" ImageUrl="Images2014/boxempty.png" runat="server" Visible="false"/>
                                        </div>                 

                                        <div class="row850">
                                                <dx:ASPxLabel ID="lblPost" runat="server" Text="Post"  Visible="false"/>
                                        </div>                 

                                        <div class="row900">
                                                <dx:ASPxImage ID ="boxPost_GreyedOut" ImageUrl="Images2014/boxgreyedout.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxPost_Ticked" ImageUrl="Images2014/boxticked.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxPost_Empty" ImageUrl="Images2014/boxempty.png" runat="server" Visible="false"/>
                                        </div>                 

                                         <div class="row950">
                                                <dx:ASPxLabel ID="lblEmail" runat="server" Text="Email"  Visible="false"/>
                                        </div>                 

                                         <div class="row1000">
                                                <dx:ASPxImage ID ="boxEmail_GreyedOut" ImageUrl="Images2014/boxgreyedout.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxEmail_Ticked" ImageUrl="Images2014/boxticked.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxEmail_Empty" ImageUrl="Images2014/boxempty.png" runat="server" Visible="false"/>
                                        </div>                 

                                         <div class="row1050">
                                                <dx:ASPxLabel ID="lblPhone" runat="server" Text="Phone"  Visible="false"/>
                                        </div>                 

                                         <div class="row1100">
                                                <dx:ASPxImage ID ="boxPhone_GreyedOut" ImageUrl="Images2014/boxgreyedout.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxPhone_Ticked" ImageUrl="Images2014/boxticked.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxPhone_Empty" ImageUrl="Images2014/boxempty.png" runat="server" Visible="false"/>
                                         </div>                 

                                         <div class="row1150">
                                                <dx:ASPxLabel ID="lblSMS" runat="server" Text="SMS"  Visible="false"/>
                                         </div>                 

                                         <div class="row1200">
                                                <dx:ASPxImage ID ="boxSMS_GreyedOut" ImageUrl="Images2014/boxgreyedout.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxSMS_Ticked" ImageUrl="Images2014/boxticked.png" runat="server" Visible="false"/>
                                                <dx:ASPxImage ID ="boxSMS_Empty" ImageUrl="Images2014/boxempty.png" runat="server" Visible="false"/>
                                         </div>                 


                                        <br /><br />
                             
                                       <div class="row50">
                                            <dx:ASPxButton ID="btnOther" runat="server" Text="" />
                                       </div>                 

                                       <div class="row150">
                                            <dx:ASPxButton ID="btnCustBack" runat="server" Text="Back"  Style="text-align: left" Visible="False"/>
                                       </div>                 

                                       <div class="row200">
                                            <dx:ASPxButton ID="btnSave" runat="server" Text="Save"  Style="text-align: left" Visible="false"/>
                                       </div>                 

                                       <div class="row250">
                                            <dx:ASPxButton ID="btnEdit" runat="server" Text="Edit" Style="text-align: left"/>
                                            <dx:ASPxButton ID="btnCancel" runat="server" Text="Cancel"  Style="text-align: left" Visible="false"/>
                                       </div>                 

                                        <div class="row350">
                                            <dx:ASPxLabel ID="lblCreated" runat="server" Text="" Visible="false"/>
                                        </div>                 

                                        <div class="row850">
                                            <dx:ASPxLabel ID="lblUpdated" runat="server" Text="" Visible="false"/>
                                        </div>                 

                                  
                                                <%-- None of these are used for Nissan -  quicker to hide them than delete them --%>
                                                <dx:ASPxLabel ID="lblProprietorName" runat="server" Text="Proprietor's Name"  Visible="false"/>
                                                <dx:ASPxTextBox ID="txtProprietorName" runat="server" Width="300px" TabIndex="18" MaxLength="100" Visible="false"/>
                                                <dx:ASPxLabel ID="lblProprietorsJobTitle" runat="server" Text="Prop. Job Title" Visible="false"/>
                                                <dx:ASPxTextBox ID="txtProprietorJobTitle" runat="server" Width="300px" TabIndex="19" Rows="100" Visible="false"/>
                                                <dx:ASPxLabel ID="lblStatusMessage" runat="server" Text="Status Message" style="font-weight: 700; color: #5255D3; text-align: center;" Visible="false"/>            
                            

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <dx:TabPage Text="Centre Specific Info" Visible="false">

                    <ContentCollection>

                        <dx:ContentControl ID="ContentControl2" runat="server">
                        
                            <table id="tabCentreSpec1" width="956px" style="font-size: x-small">
                                <%----------------------------------------------------------------------------------%>                            
                                
                                <tr id="Tr1" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblVanRoute" runat="server" Text="Van Route" Width="180px"
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td style="width: 150px" align="left">
                                        <dx:ASPxTextBox ID="txtVanRoute" runat="server" Width="100px" TabIndex="19" 
                                            MaxLength="10" />
                                    </td>
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblUser1" runat="server" Text="User Field 1" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtUserField1" runat="server" Width="350px" TabIndex="23" 
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr2" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblDriveTime" runat="server" Text="Drive Time (mins)" Width="180px"
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <dx:ASPxTextBox ID="txtDriveTime" runat="server" Width="100px" TabIndex="20" 
                                            MaxLength="10" />
                                    </td>
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblUser2" runat="server" Text="User Field 2" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtUserField2" runat="server" Width="350px" TabIndex="24" 
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr3" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblDistance" runat="server" Text="Distance (miles)" Width="180px"
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <dx:ASPxTextBox ID="txtDistance" runat="server" Width="100px" TabIndex="21" 
                                            MaxLength="10" />
                                    </td>
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblUser3" runat="server" Text="User Field 3" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtUserField3" runat="server" Width="350px" TabIndex="25" 
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                                
                                <tr id="Tr4" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblSalesRep" runat="server" Text="Sales Rep" Width="180px"
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td style="width: 150px;" align="left">
                                        <dx:ASPxTextBox ID="txtSalesRep" runat="server" Width="100px" TabIndex="22"  
                                            MaxLength="10"/>
                                    </td>
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="lblUser4" runat="server" Text="User Field 4" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtUserField4" runat="server" Width="350px" TabIndex="26" 
                                            MaxLength="100"  />
                                    </td>
                                </tr>
                            </table>
                            
                            <table id="tabCentreSpec2" width="956px" style="font-size: x-small">
                            
                                <tr id="Tr5" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="Label1" runat="server" Text="Alternative Contact Name" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtContactNameSpec" runat="server" Width="690px" TabIndex="27" 
                                            MaxLength="100"/>
                                    </td>
                                </tr>
                                
                                <tr id="Tr6" runat="server">
                                    <td style="width: 20%;  " align="right">
                                        <dx:ASPxLabel ID="Label3" runat="server" Text="Alternative Telephone" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtTelephoneSpec" runat="server" Width="690px" TabIndex="28" 
                                            MaxLength="100" />
                                    </td>
                                </tr>
                                
                                <tr id="Tr7" runat="server">
                                    <td style="width: 20%;  " align="right" valign="top" >
                                        <dx:ASPxLabel ID="lblNotes" runat="server" Text="Notes" 
                                             >
                                        </dx:ASPxLabel>
                                    </td>
                                    <td align="left">
                                        <dx:ASPxTextBox ID="txtNotes" runat="server" Width="690px" TabIndex="29" 
                                            Enabled="false" Height="250px" Rows="1" TextMode="MultiLine"  />
                                    </td>
                                </tr>
                                
                            </table>

                        </dx:ContentControl>

                    </ContentCollection>

                </dx:TabPage>

                <dx:TabPage Text="Services" Visible="false">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl4" runat="server">

                            <%----------------------------------------------------------------------------------%>                            
                            <table id="tabServices" width="100%">
                                <tr>
                                    
                                    <td style="width: 240px;  " align="left">
                                        <dx:ASPxCheckBoxList ID="cblServices" runat="server"  
                                            RepeatColumns="1" TextAlign="right"  TabIndex="30">
                                            <Items>
                                            <dx:ListEditItem Value="0" Text="Service/Repair" />
                                            <dx:ListEditItem Value="1" Text="MOT" />
                                            <dx:ListEditItem Value="2" Text="Tyres" />
                                            <dx:ListEditItem Value="3" Text="Diagnostics" />
                                            <dx:ListEditItem Value="4" Text="Air Conditioning" />
                                            <dx:ListEditItem Value="5" Text="Electrical Repairs" />
                                            </Items>
                                        </dx:ASPxCheckBoxList>
                                    </td>
                                    
                                    <td valign="top" >
                                        <table>
                                            <tr>
                                                <td style="width: 128px;  " align="right">
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px;  " align="right">
                                                    <dx:ASPxLabel ID="lblRamps" runat="server" Text="Ramps/Bays" 
                                                         >
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dx:ASPxSpinEdit ID="spinRamps" runat="server" AutoPostBack="true" 
                                                        Height="21px" Number="0" MinValue="0" Width="50px" TabIndex="31">
                                                    </dx:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px;  " align="right">
                                                    <dx:ASPxLabel ID="Label2" runat="server" Text="Technicians" 
                                                         >
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 50px" align="right">
                                                    <dx:ASPxSpinEdit ID="spinTechnicians" runat="server" AutoPostBack="true" 
                                                        Height="21px" Number="0" MinValue="0" Width="50px" TabIndex="32">
                                                    </dx:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px;  " align="right">
                                                    <dx:ASPxLabel ID="Label4" runat="server" Text="Vehicles Repaired/Wk" 
                                                         >
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dx:ASPxSpinEdit ID="spinVehicleRepairs" runat="server" AutoPostBack="true" 
                                                        Height="21px" Number="0" MinValue="0" Width="50px" TabIndex="33">
                                                    </dx:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 128px;   font-weight: 700;" align="right">
                                                    <dx:ASPxLabel ID="Label6" runat="server" Text="Vehicles Repaired/Wk" 
                                                        style="font-weight: normal; font-size: small">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style="width: 50px;" align="right">
                                                    <dx:ASPxSpinEdit ID="spinToyotaRepairs" runat="server" AutoPostBack="true" 
                                                        Height="21px" Number="0" MinValue="0" Width="50px" TabIndex="34">
                                                    </dx:ASPxSpinEdit>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                    <td valign="top" >
                                        <table id="tabCurrentMotorFactors" style="font-size: x-small">
                                            <tr>
                                                <td style="width: 60%;  " align="left">
                                                    <dx:ASPxLabel ID="lblMotorFactors" runat="server" Text="Motor Factors" 
                                                         >
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td style=" " align="left">
                                                    <dx:ASPxLabel ID="lblSpend" runat="server" Text="Spend" 
                                                         >
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width: 60%;  " align="left">
                                                    <dx:ASPxTextBox ID="txtMotorFactor1" runat="server" Width="300px" TabIndex="35" 
                                                        Enabled="false" />
                                                </td>
                                                <td style=" " align="left">
                                                    <dx:ASPxTextBox ID="txtSpend1" runat="server" Width="125px" TabIndex="36" 
                                                        Enabled="false" />
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width: 60%;  " align="left">
                                                    <dx:ASPxTextBox ID="txtMotorFactor2" runat="server" Width="300px" TabIndex="37" 
                                                        Enabled="false" />
                                                </td>
                                                <td style=" " align="left">
                                                    <dx:ASPxTextBox ID="txtSpend2" runat="server" Width="125px" TabIndex="40" 
                                                        Enabled="false" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 60%;  " align="left">
                                                    <dx:ASPxTextBox ID="txtMotorFactor3" runat="server" Width="300px" TabIndex="1" Enabled="false" />
                                                </td>
                                                <td style=" " align="left">
                                                    <dx:ASPxTextBox ID="txtSpend3" runat="server" Width="125px" TabIndex="42" 
                                                        Enabled="false" />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 60%;   height: 26px;" align="left">
                                                    <dx:ASPxTextBox ID="txtMotorFactor4" runat="server" Width="300px" TabIndex="43" 
                                                        Enabled="false" />
                                                </td>
                                                <td style="  height: 26px;" align="left">
                                                    <dx:ASPxTextBox ID="txtSpend4" runat="server" Width="125px" TabIndex="44" 
                                                        Enabled="false" />
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br />
                                        
                            <table>
                                <tr>
                                    <td valign="middle" >
                                        <dx:ASPxLabel ID="lblOther" runat="server" Text="Other Specialism" style="padding-left:5px; font-size: small">
                                        </dx:ASPxLabel>
                                    </td>
                                    <td valign="middle" >
                                        <dx:ASPxTextBox ID="txtOther" runat="server" Width="300px" TabIndex="45" 
                                            Enabled="false"  />
                                    </td>
                                </tr>
                            </table>
                            <br />

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <dx:TabPage Text="Spend Summary">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl3" runat="server">

                            <table width="100%">
                                <tr>
                                    <td>

                                        <dx:ASPxLabel ID="lblPageTitle" runat="server" Text="Overall Sales" style="float:left ">
                                        </dx:ASPxLabel>

                                        <dx:ASPxComboBox ID="ddlSpendYear" runat="server" AutoPostBack="True"   Width="75px" style="float:right">
                                        </dx:ASPxComboBox>

                                        <dx:ASPxComboBox ID="ddlTA" runat="server" AutoPostBack="True" Width="150px" SelectedIndex="0"  style="margin-right:10px; float:right">
                                            <Items>
                                                <dx:ListEditItem text="All" Value ="ALL"/>
                                                <dx:ListEditItem text="Accessories" Value ="ACC"/>
                                                <dx:ListEditItem text="Damage"  Value ="DAM"/>
                                                <dx:ListEditItem text="Mechanical Repair"  Value="MEC"/>
                                                <dx:ListEditItem text="Routine Maintenance"  Value ="ROM"/>
                                                <dx:ListEditItem text="Extended Maintenance" Value ="EXM"/>
                                                <dx:ListEditItem text="Additional Product" Value="OTH"/>
                                            </Items>
                                        </dx:ASPxComboBox>

                                    </td>
                                </tr>
                              
                                  <tr>
                                    <td width="100%">

                                        <dx:ASPxGridView 
                                            ID="gridSales" 
                                            runat="server" 
                                            width="100%"
                                            cssclass="grid_styles"
                                            onload="GridStyles"
                                            style="margin-top:10px" >

                                            <Settings 
                                                ShowFilterRow="False"
                                                ShowFilterRowMenu="False" 
                                                ShowGroupedColumns="False"
                                                ShowGroupFooter="VisibleIfExpanded" 
                                                ShowGroupPanel="False" 
                                                ShowHeaderFilterButton="False"
                                                ShowStatusBar="Hidden" 
                                                ShowTitlePanel="False" 
                                                UseFixedTableLayout="True" />

                                            <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
            
                                            <Styles>
                                                <Header HorizontalAlign="Center" Wrap="True"  />
                                                <Cell HorizontalAlign="Center" Wrap="True"    />
                                            </Styles>

                                            <SettingsPager AlwaysShowPager="false">
                                            </SettingsPager>
            
                                            <Columns>
                
                                                <dx:GridViewDataTextColumn Caption="Year" FieldName="RowDescription" ReadOnly="True" VisibleIndex="0" />
                                                <dx:GridViewDataTextColumn Caption="January" FieldName="Month_1" VisibleIndex="1" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="February" FieldName="Month_2" VisibleIndex="2" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="March" FieldName="Month_3" VisibleIndex="3" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="April" FieldName="Month_4" VisibleIndex="4" PropertiesTextEdit-DisplayFormatString="£#,##0"  >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="May" FieldName="Month_5" VisibleIndex="5" PropertiesTextEdit-DisplayFormatString="£#,##0"  >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="June" FieldName="Month_6" VisibleIndex="6" PropertiesTextEdit-DisplayFormatString="£#,##0"  >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="July" FieldName="Month_7" VisibleIndex="7" PropertiesTextEdit-DisplayFormatString="£#,##0"  >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="August" FieldName="Month_8" VisibleIndex="8" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="September" FieldName="Month_9" VisibleIndex="9" PropertiesTextEdit-DisplayFormatString="£#,##0"  >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="October" FieldName="Month_10" VisibleIndex="10" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="November" FieldName="Month_11" VisibleIndex="11" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="December" FieldName="Month_12" VisibleIndex="12" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="YTD" FieldName="YTD_SoFar" VisibleIndex="13" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Total" FieldName="YTD_Total" VisibleIndex="14" PropertiesTextEdit-DisplayFormatString="£#,##0" >
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Avg Month" FieldName="YTD_Avg" VisibleIndex="15"  PropertiesTextEdit-DisplayFormatString="£#,##0" Width="8%" >
                
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                
                                            </Columns>

                                        </dx:ASPxGridView>

                                    </td>
                                </tr>

                                 <tr>
                                    <td>
                                    </td>
                                </tr>



                       <tr>
                           <td>
                               <dx:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="0" Width="100%">
                                   <TabPages>
                                       <dx:TabPage Text="Mechanical Products" >
                                           <ContentCollection>
                                               <dx:ContentControl runat="server">
                                                   <table width="100%">
                                                     <tr>
                                                         <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_M1" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="GridName" 
                                                                            FieldName="GridLine"
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                         <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_M2" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                
                                                          <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_M3" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>
                                                     </tr>
          
                                                     <tr>
                                                         <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_M4" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                         <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_M5" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                         <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_M6" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>
                                                    </tr>
            
                                                     <tr>
                                                         <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_M7" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                         <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_M8" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>
                                                     </tr>
                               
                                                       
                                                </table>
                                              
                                                   
                                                   
                                               </dx:ContentControl>
                                           </ContentCollection>
                                       </dx:TabPage>
                  
                                      <dx:TabPage Text="Damage Products">
                                           <ContentCollection>
                                               <dx:ContentControl runat="server">
                                                   <table width="100%">
                                                     <tr>
                                                        <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_D4" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                         <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                  ID="gridSpends_D2" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                           <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_D3" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>
                                                     </tr>
                                                     
                                                      <tr>
                                                            <td width ="33%" align="center" valign="top">
                                                                <dx:ASPxGridView 
                                                                   ID="gridSpends_D1" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>

                                                           <td width ="33%" align="center" valign="top">
                                                             <dx:ASPxGridView 
                                                                   ID="gridSpends_D5" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                  OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText"
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                           </td>

                                                            <td width ="33%" align="center" valign="top">
                                                              <dx:ASPxGridView 
                                                                   ID="gridSpends_D6" 
                                                                   runat="server" 
                                                                   AutoGenerateColumns="False" 
                                                                   OnCustomColumnDisplayText="SpendGrids_CustomColumnDisplayText" 
                                                                   OnHtmlDataCellPrepared="SpendGrids_HtmlDataCellPrepared" 
                                                                   OnLoad="GridStyles" 
                                                                   Styles-Header-HorizontalAlign="Center"
                                                                   Styles-Cell-HorizontalAlign="Center"
                                                                   Width="99%">
                                                                   <Columns>
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Service" 
                                                                            FieldName="GridLine"   
                                                                           ShowInCustomizationForm="True" 
                                                                           VisibleIndex="0" 
                                                                           HeaderStyle-HorizontalAlign="Left"
                                                                           CellStyle-HorizontalAlign="Left"
                                                                           Width="40%">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<CellStyle HorizontalAlign="Left"></CellStyle>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Units" 
                                                                           FieldName="Quantity"
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0"
                                                                           VisibleIndex="1"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                 
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Sales" 
                                                                           FieldName="SaleValue" 
                                                                           ShowInCustomizationForm="True"
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="2"
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>

                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var £" 
                                                                           FieldName="VarianceQ" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="£#,##0" 
                                                                           VisibleIndex="3" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="&#163;#,##0"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                         
                                                                       <dx:GridViewDataTextColumn 
                                                                           Caption="Var %" 
                                                                           FieldName="VarianceS" 
                                                                           ShowInCustomizationForm="True" 
                                                                           PropertiesTextEdit-DisplayFormatString="#,##0.0%"
                                                                           VisibleIndex="4" 
                                                                           Width="15%">
<PropertiesTextEdit DisplayFormatString="#,##0.0%"></PropertiesTextEdit>
                                                                       </dx:GridViewDataTextColumn>
                                                                   </Columns>

<Styles>
<Header HorizontalAlign="Center"></Header>

<Cell HorizontalAlign="Center"></Cell>
</Styles>
                                                               </dx:ASPxGridView>
                                                         </td>
                                                     </tr>
            
                                                </table>
                                              
                                                   
                                                   
                                               </dx:ContentControl>
                                           </ContentCollection>
                                       </dx:TabPage>
                  
                                   </TabPages>
                                      </dx:ASPxPageControl>
                           </td>
                                </tr>

                            </table>

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <dx:TabPage Text="Contact History" Visible="false">

                    <ContentCollection>

                        <dx:ContentControl runat="server">

                            <dx:ASPxGridView ID="gridContactHistory" ClientInstanceName="gridContactHistory" runat="server" 
                                style="margin-left:-3px; margin-top:5px;" DataSourceID="dsHistory">

                                <Settings ShowFilterRow="False" ShowFilterRowMenu="False" ShowGroupedColumns="False"
                                    ShowGroupFooter="VisibleIfExpanded" ShowGroupPanel="False" ShowHeaderFilterButton="False"
                                    ShowStatusBar="Hidden" ShowTitlePanel="False" UseFixedTableLayout="True" />
                                <SettingsBehavior AutoFilterRowInputDelay="12000" ColumnResizeMode="Control" />
                                <Styles>
                                    <Header HorizontalAlign="Center" Wrap="True"  />
                                    <Cell HorizontalAlign="Center" Wrap="True"   />
                                </Styles>
                                <SettingsPager AlwaysShowPager="false">
                                </SettingsPager>
                                <Columns>
                                    
                                    <dx:GridViewDataTextColumn Caption="Id" FieldName="Id" VisibleIndex="0" Visible="False" />

                                    <dx:GridViewDataTextColumn Caption="Date" FieldName="ContactDate" VisibleIndex="0" Width="12%"/>

                                    <dx:GridViewDataTextColumn Caption="Contact" FieldName="ContactType" VisibleIndex="1" Width="65px" UnboundType="String">
                                        <DataItemTemplate>
                                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ContactType_Init">
                                            </dx:ASPxHyperLink>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>        

                                    <dx:GridViewDataTextColumn Caption="Details" FieldName="ContactMessage" VisibleIndex="2" >
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <CellStyle HorizontalAlign="Left" />
                                    </dx:GridViewDataTextColumn>        

                                    <dx:GridViewDataTextColumn Caption="Reminder?" FieldName="ReminderDate" VisibleIndex="3" Width="12%"/>

                                    <dx:GridViewDataTextColumn Caption="User" FieldName="WebUserName" VisibleIndex="4" Width="12%"/>

                                    <dx:GridViewDataTextColumn Caption="Remove" FieldName="Id" VisibleIndex="5" Width="65px" UnboundType="String">
                                        <DataItemTemplate>
                                            <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="RemoveLink_Init">
                                            </dx:ASPxHyperLink>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>        

                                </Columns>
                            </dx:ASPxGridView>

                            <table style="margin-left:-3px; margin-top:10px; margin-right:-3px; width:100%; " >
                                <tr >
                                    <td>
                                        <dx:ASPxButton ID="btn1" runat="server"   Text="" CssClass ="special" HorizontalPosition="True"
                                            Border-BorderStyle="None" Image-Url="~/Images2014/Phone.png" Image-UrlHottracked="~/Images2014/Phone_Hover.png"
                                            Image-Width="35px" ToolTip="Phone" Image-Height="30px" style="margin-top:15px">
<Image UrlHottracked="~/Images2014/Phone_Hover.png" Height="30px" Width="35px" Url="~/Images2014/Phone.png"></Image>

<Border BorderStyle="None"></Border>
                                        </dx:ASPxButton>
                         
                                        <dx:ASPxButton ID="btn2" runat="server"   Text="" CssClass ="special" HorizontalPosition="True"
                                            Border-BorderStyle="None" Image-Url="~/Images2014/Email.png" Image-UrlHottracked="~/Images2014/Email_Hover.png"
                                            Image-Width="35px" ToolTip="Email" Image-Height="30px" style="margin-top:15px">
<Image UrlHottracked="~/Images2014/Email_Hover.png" Height="30px" Width="35px" Url="~/Images2014/Email.png"></Image>

<Border BorderStyle="None"></Border>
                                        </dx:ASPxButton>
                                   
                                        <dx:ASPxButton ID="btn3" runat="server"   Text="" CssClass ="special" HorizontalPosition="True"
                                            Border-BorderStyle="None" Image-Url="~/Images2014/CustomerVisit.png" Image-UrlHottracked="~/Images2014/CustomerVisit_Hover.png"
                                            Image-Width="35px" ToolTip="Face to face meeting" Image-Height="30px" style="margin-top:15px">
<Image UrlHottracked="~/Images2014/CustomerVisit_Hover.png" Height="30px" Width="35px" Url="~/Images2014/CustomerVisit.png"></Image>

<Border BorderStyle="None"></Border>
                                        </dx:ASPxButton>
                                  
                                        <dx:ASPxButton ID="btn4" runat="server"   Text="" CssClass ="special" HorizontalPosition="True"
                                            Border-BorderStyle="None" Image-Url="~/Images2014/Mailing.png" Image-UrlHottracked="~/Images2014/Mailing_Hover.png"
                                            Image-Width="35px" ToolTip="Mail" Image-Height="30px" style="margin-top:15px">
<Image UrlHottracked="~/Images2014/Mailing_Hover.png" Height="30px" Width="35px" Url="~/Images2014/Mailing.png"></Image>

<Border BorderStyle="None"></Border>
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </dx:ContentControl>

                    </ContentCollection>

                </dx:TabPage>

            </TabPages>
      
       </dx:ASPxPageControl>

    </div>
    
    <asp:SqlDataSource ID="dsHistory" runat="server" SelectCommand="p_CustManager_ContactHistory" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nCustomerId" SessionField="CustomerId" Type="Int32" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
