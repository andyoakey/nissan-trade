﻿Imports System.Net.Mail
Imports System.Data
Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text
Imports System.Windows.Forms
Imports System.Drawing
Imports DevExpress.Web

Partial Class HomePage

    Inherits System.Web.UI.Page

    Protected Sub dsReminders_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsReminders.Init
        dsReminders.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCampaigns_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns.Init
        dsCampaigns.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsOutstandingActions_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsOutstandingActions.Init
        dsOutstandingActions.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblWelcome.Text = "You are logged in as  - " & Session("UserEmailAddress")
        Session("CustomerCalledFrom") = "HomePage"
        Session("PrevPage") = "HomePage.aspx"
        If Session("UserLevel") <> "C" Then
            LinkMarketingManager.Visible = False
            LinkMapping.Visible = False
            gridReminders.Columns(1).Visible = True
            gridCampaigns.Columns(0).Visible = True
            gridVARDetail.Columns(0).Visible = True
        Else
            LinkMarketingManager.Visible = True
            LinkMapping.Visible = True
            gridReminders.Columns(1).Visible = False
            gridCampaigns.Columns(0).Visible = False
            gridVARDetail.Columns(0).Visible = False
        End If
    End Sub

    Protected Sub btnEnterWebsite_Click(sender As Object, e As EventArgs) Handles btnEnterWebsite.Click
        Response.Redirect("Dashboard.aspx")
    End Sub

    Protected Sub Page_LoadComplete(sender As Object, e As EventArgs) Handles Me.LoadComplete
        If Not Session("SelectionLevel") Is Nothing And Not Session("SelectionId") Is Nothing Then
            Call TabCounts()
        End If
    End Sub

    Private Sub TabCounts()

        Dim htIn1 As New Hashtable
        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim ds As DataSet
        Dim n1 As Integer = 0
        Dim n2 As Integer = 0
        Dim n3 As Integer = 0

        Try
            htIn1.Add("@sSelectionLevel", Session("SelectionLevel"))
            htIn1.Add("@sSelectionId", Session("SelectionId"))
            ds = da.Read(sErr, "p_ReminderListCount", htIn1)
            If sErr <> "" Then
                Session("CallingModule") = "Dashboard.ASPX - ReminderRows()"
                Session("ErrorToDisplay") = sErr
                Response.Redirect("dbError.aspx")
            End If
            n1 = ds.Tables(0).Rows(0).Item(0)
            n2 = ds.Tables(0).Rows(0).Item(1)
            n3 = ds.Tables(0).Rows(0).Item(2)

            HomePageTabs.TabPages(0).Text = "Contact History Reminders (" & Trim(Str(n1)) & ")"
            HomePageTabs.TabPages(1).Text = "Outstanding Campaign Invoices (" & Trim(Str(n2)) & ")"
            HomePageTabs.TabPages(2).Text = "Outstanding Visit Actions (" & Trim(Str(n3)) & ")"
        Catch ex As Exception

        End Try


    End Sub

    Protected Sub ReminderLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sType As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ReminderTypeId").ToString()
        Dim sId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ReminderLinkId").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}&Type={2}", "ReminderAck.aspx", sId, sType)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/user_block.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ReminderAck('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to remove this Contact History entry."

    End Sub

    Protected Sub CampaignLink_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)
        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sType As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "RewardType").ToString()
        If sType = "Batteries" Then
            link.NavigateUrl = String.Format("Campaign2016BatteriesOrders.aspx?{0}", sId)
        End If
        link.Text = "View"

    End Sub

End Class