﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="CustomerLoyalty2015.aspx.vb" Inherits="CustomerLoyalty2015" Title="Untitled Page" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 5px; width: 976px; text-align: left; height: 575px;">

        <div id="divTitle" style="position: relative; left: -2px">
            <table width="976px">
                <tr>
                    <td style="width:60%" align="left" valign="middle" > 
                        <asp:Label ID="lblPageTitle" runat="server" Text="Customer Loyalty (2015)" Style="font-weight: 700; font-size: large">
                        </asp:Label>
                    </td>
                    <td style="width:40%" align="right" valign="middle" > 
                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True"   Width="75px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>        
        <br />

        <div style="float: right;">
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" AutoPostBack="True"
                RepeatDirection="Horizontal">
                <asp:ListItem Enabled="False">Summary</asp:ListItem>
                <asp:ListItem Enabled="False">Q1</asp:ListItem>
                <asp:ListItem Enabled="False">Q2</asp:ListItem>
                <asp:ListItem Enabled="False">Q3</asp:ListItem>
                <asp:ListItem Enabled="False">Q4</asp:ListItem>
            </asp:CheckBoxList>
        </div>
        <br />

        <dx:ASPxGridView ID="ASPxGridView1" runat="server"  Width="970px"
            AutoGenerateColumns="False" >

            <Settings ShowFooter="True" ShowHeaderFilterButton="False" ShowStatusBar="Hidden"
                ShowTitlePanel="True" ShowGroupFooter="VisibleIfExpanded" UseFixedTableLayout="False"
                VerticalScrollableHeight="300" ShowFilterRow="False" ShowVerticalScrollBar="False" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" ColumnResizeMode="Control"
                AllowDragDrop="False" AllowGroup="False" />

            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="12" />

            <Styles>
                <Header HorizontalAlign="Center" Wrap="True" />
                <Cell />
                <Footer />
            </Styles>

            <Columns>

                <dx:GridViewDataTextColumn Caption="Centre" FieldName="CentreId" Name="CentreId" Width="70px">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True"
                        ShowFilterRowMenu="True" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Centre Name" FieldName="CentreName" Name="CentreName" Width="155px">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True"
                        ShowFilterRowMenu="True" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Customer Id" FieldName="CustomerId" Name="CustomerId" Width="75px">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                        ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="CustomerName" Name="CustomerName" Width="225px">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                        ShowFilterRowMenu="False" />
                    <CellStyle Font-Size="X-Small" HorizontalAlign="Left" />
                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Left" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Customer Postcode" FieldName="CustomerPostcode" Name="CustomerPostcode" Width="75px">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                        ShowFilterRowMenu="False" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn Caption="Agreed Rebate" FieldName="Rebate" Name="Rebate" Width="75px">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" AllowSort="True"
                        ShowFilterRowMenu="True" />
                    <PropertiesTextEdit DisplayFormatString="P">
                    </PropertiesTextEdit>
                </dx:GridViewDataTextColumn>

                <dx:GridViewBandColumn Name="Summary" Caption="Trade Sales" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                    <Columns>
                        <dx:GridViewBandColumn Caption="2013" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Total Sales" FieldName="2013_Total" Name="2013_Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Avg Sales/Month" FieldName="2013_Avg" Name="2013_Avg">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>

                        <dx:GridViewBandColumn Caption="2014" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Total Sales" FieldName="2014_Total" Name="2014_Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Avg Sales/Month" FieldName="2014_Avg" Name="2014_Avg">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>

                        <dx:GridViewBandColumn Caption="2015" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Total Sales" FieldName="2015_Total" Name="2015_Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Avg Sales/Month" FieldName="2015_Avg" Name="2015_Avg">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Name="Q1" Caption="2015/Q1" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                    <Columns>
                        <dx:GridViewBandColumn Caption="January" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Retail" FieldName="01_Retail" Name="01_Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_Applicable" Name="01_Applicable"
                                    Caption="Applicable">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_Total" Name="01_Total" Caption="Total">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_YoYPct" Name="01_YoYPct" Caption="YoY">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_Discount" Name="01_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_DiscountPct" Name="01_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="01_Rebate" Name="01_Rebate" Caption="Rebate">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="February" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="02_Retail" Name="02_Retail" Caption="Retail">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="02_Applicable" Name="02_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="02_Total" Name="02_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="02_YoYPct" Name="02_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat" />
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount" FieldName="02_Discount" Name="02_Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount %" FieldName="02_DiscountPct" Name="02_DiscountPct">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="02_Rebate" Name="02_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="March" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="03_Retail" Name="03_Retail" Caption="Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_Applicable" Name="03_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_Total" Name="03_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_YoYPct" Name="03_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle  BackColor="LightBlue" />
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_Discount" Name="03_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_DiscountPct" Name="03_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="03_Rebate" Name="03_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Name="Q2" Caption="2015/Q2" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                    <Columns>
                        <dx:GridViewBandColumn Caption="April" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Retail" FieldName="04_Retail" Name="04_Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_Applicable" Name="04_Applicable"
                                    Caption="Applicable">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_Total" Name="04_Total" Caption="Total">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_YoYPct" Name="04_YoYPct" Caption="YoY">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_Discount" Name="04_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_DiscountPct" Name="04_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="04_Rebate" Name="04_Rebate" Caption="Rebate">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="May" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="05_Retail" Name="05_Retail" Caption="Retail">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="05_Applicable" Name="05_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="05_Total" Name="05_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="05_YoYPct" Name="05_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount" FieldName="05_Discount" Name="05_Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount %" FieldName="05_DiscountPct" Name="05_DiscountPct">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="05_Rebate" Name="05_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="June" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="06_Retail" Name="06_Retail" Caption="Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_Applicable" Name="06_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_Total" Name="06_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_YoYPct" Name="06_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_Discount" Name="06_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_DiscountPct" Name="06_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="06_Rebate" Name="06_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Name="Q3" Caption="2015/Q3" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                    <Columns>
                        <dx:GridViewBandColumn Caption="July" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Retail" FieldName="07_Retail" Name="07_Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_Applicable" Name="07_Applicable"
                                    Caption="Applicable">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_Total" Name="07_Total" Caption="Total">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_YoYPct" Name="07_YoYPct" Caption="YoY">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_Discount" Name="07_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_DiscountPct" Name="07_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="07_Rebate" Name="07_Rebate" Caption="Rebate">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="August" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="08_Retail" Name="08_Retail" Caption="Retail">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="08_Applicable" Name="08_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="08_Total" Name="08_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="08_YoYPct" Name="08_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount" FieldName="08_Discount" Name="08_Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount %" FieldName="08_DiscountPct" Name="08_DiscountPct">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="08_Rebate" Name="08_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="September" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="09_Retail" Name="09_Retail" Caption="Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_Applicable" Name="09_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_Total" Name="09_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_YoYPct" Name="09_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_Discount" Name="09_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_DiscountPct" Name="09_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="09_Rebate" Name="09_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Name="Q4" Caption="2015/Q4" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                    <Columns>
                        <dx:GridViewBandColumn Caption="October" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Retail" FieldName="10_Retail" Name="10_Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_Applicable" Name="10_Applicable"
                                    Caption="Applicable">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_Total" Name="10_Total" Caption="Total">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_YoYPct" Name="10_YoYPct" Caption="YoY">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_Discount" Name="10_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_DiscountPct" Name="10_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="10_Rebate" Name="10_Rebate" Caption="Rebate">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="November" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="11_Retail" Name="11_Retail" Caption="Retail">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="11_Applicable" Name="11_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="11_Total" Name="11_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="11_YoYPct" Name="11_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle  BackColor="LightBlue" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount" FieldName="11_Discount" Name="11_Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Discount %" FieldName="11_DiscountPct" Name="11_DiscountPct">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="11_Rebate" Name="11_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="LightBlue">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                        <dx:GridViewBandColumn Caption="December" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="True">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="12_Retail" Name="12_Retail" Caption="Retail">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" ShowFilterRowMenu="False" AllowHeaderFilter="False"
                                        AllowSort="True" AllowGroup="False"></Settings>
                                    <HeaderStyle HorizontalAlign="Center" Wrap="True"></HeaderStyle>
                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle HorizontalAlign="Center" Font-Size="X-Small">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_Applicable" Name="12_Applicable" Caption="Applicable">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_Total" Name="12_Total" Caption="Total">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_YoYPct" Name="12_YoYPct" Caption="YoY">
                                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat" />

                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_Discount" Name="12_Discount" Caption="Discount">
                                    <PropertiesTextEdit DisplayFormatString="c">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_DiscountPct" Name="12_DiscountPct" Caption="Discount %">
                                    <PropertiesTextEdit DisplayFormatString="p">
                                    </PropertiesTextEdit>
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="12_Rebate" Name="12_Rebate" Caption="Rebate">
                                    <Settings AllowAutoFilter="False" AllowGroup="False" AllowHeaderFilter="False" AllowSort="True"
                                        ShowFilterRowMenu="False" />

                                    <CellStyle BackColor="Wheat">
                                    </CellStyle>
                                    <FooterCellStyle Font-Size="X-Small" HorizontalAlign="Center">
                                    </FooterCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="c" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:GridViewBandColumn>
                    </Columns>
                </dx:GridViewBandColumn>

            </Columns>
            <Settings ShowFooter="True" ShowHeaderFilterButton="True" ShowTitlePanel="True" ShowGroupFooter="VisibleIfExpanded"
                HorizontalScrollBarMode="Visible" />
        </dx:ASPxGridView>

        <br />
        <br />
    </div>
    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"
        PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>

