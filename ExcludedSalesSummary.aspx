<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ExcludedSalesSummary.aspx.vb" Inherits="ExcludedSalesSummary" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <div style="position: relative; font-family: Calibri; text-align: left;" >
        
       <div id="divTitle" style="position:relative; top:0px">
            <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Excluded Sales Summary" />
       </div>

       <br />

        <table id="trSelectionStuff" Width="40%" style="position:relative; margin-bottom: 10px;">
            <tr id="Tr2" runat="server" >
   
                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblFrom"
                            runat="server" 
                            Text="Month From">
                    </dx:ASPxLabel>
                </td>

                <td align="left" width="50%" >
                     <dx:ASPxLabel 
                            id="lblTo"
                            runat="server" 
                            Text="Month To">
                    </dx:ASPxLabel>
                </td>


                </tr>

                <tr id="Tr1" runat="server" >

                    <td align="left" width="50%" valign="top" >
                         <dx:ASPxComboBox
                        ID="ddlFrom" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                     </td>

                    <td align="left" width="50%" valign="top">
                         <dx:ASPxComboBox
                        ID="ddlTo" 
                        runat="server" 
                        AutoPostBack="True">
                    </dx:ASPxComboBox>
                    </td>
                
             </tr>
        </table>
        
        <dx:ASPxGridView 
            ID="gridExcluded" 
            runat="server" 
            onload="gridStyles"
            cssclass="grid_styles"
            AutoGenerateColumns="False" 
            DataSourceID="dsExcluded" 
            Width="100%"> 

            <SettingsDetail  ShowDetailRow="false"/>
            
            <Settings 
                ShowHeaderFilterBlankItems="false"
                ShowFilterRow="False" 
                ShowFooter="True" 
                ShowGroupedColumns="False"
                ShowGroupFooter="VisibleIfExpanded" 
                ShowGroupPanel="False" 
                ShowHeaderFilterButton="False"
                ShowStatusBar="Hidden" 
                ShowTitlePanel="False" 
                UseFixedTableLayout="True" />
            
            <SettingsBehavior 
                AllowSort="true" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" />
            
            <SettingsPager  Mode="ShowPager" PageSize="16" >
                <AllButton Visible="True">
                </AllButton>
            </SettingsPager>
            
            <Styles Footer-HorizontalAlign ="Center" />

            <Columns>

                <dx:GridViewBandColumn Caption="Dealer Information"  HeaderStyle-HorizontalAlign="Center">
                    <Columns>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="0"
                    FieldName="zone"
                    Name="zone"
                    caption="Zone"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth ="150"
                    Width="10%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="1"
                    FieldName="dealername"
                    Name="dealername"
                    caption="Dealer"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="20%" >
                </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn 
                    VisibleIndex="2"
                    FieldName="dealercode"
                    Name="dealercode"
                    caption="Dealer Code"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-HeaderFilterMode="CheckedList"
                    Settings-AllowHeaderFilter="True"
                    ExportWidth="150"
                    Width="10%" >
                </dx:GridViewDataTextColumn>

                    </Columns>
                </dx:GridViewBandColumn>

                <dx:GridViewBandColumn Caption="Sales Information"  HeaderStyle-HorizontalAlign="Center">
                    <Columns>
                        <dx:GridViewDataTextColumn 
                    VisibleIndex="4"
                    FieldName="salevalue_i"
                    Name="salevalue_i"
                    caption="Included Sales"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="10%" >
                   <PropertiesTextEdit DisplayFormatString="C2"/>
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    VisibleIndex="5"
                    FieldName="salevalue_x"
                    Name="salevalue_x"
                    caption="Excluded Sales"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="10%" >
                   <PropertiesTextEdit DisplayFormatString="C2"/>
                </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn 
                    VisibleIndex="6"
                    FieldName="salevalue_c"
                    Name="salevalue_c"
                    caption="Centrally Supported Sales"
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    Width="10%" >
                   <PropertiesTextEdit DisplayFormatString="C2"/>
                </dx:GridViewDataTextColumn>

               <dx:GridViewDataTextColumn 
                    VisibleIndex="6"
                    FieldName="validpc"
                    Name="validpc"
                    caption="Valid Sales % "
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    ToolTip="Shows Valid sales as a % of Total Sales AFTER centrally supported sales have been removed."
                    Width="10%" >
                   <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    VisibleIndex="7"
                    FieldName="excludedpc"
                    Name="excludedpc"
                    caption="Excluded Sales % "
                    CellStyle-Wrap="False"
                    CellStyle-HorizontalAlign="Center"
                    HeaderStyle-Wrap="False"
                    HeaderStyle-HorizontalAlign="Center"
                    Settings-AllowHeaderFilter="False"
                    ExportWidth="150"
                    ToolTip="Shows Excluded sales as a % of Total Sales AFTER centrally supported sales have been removed."
                    Width="10%" >
                   <PropertiesTextEdit DisplayFormatString="0.00%"/>
                </dx:GridViewDataTextColumn>

                    </Columns>
                </dx:GridViewBandColumn>

            </Columns>

            <TotalSummary>
                <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="salevalue_i" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="salevalue_x" SummaryType="Sum" />
                <dx:ASPxSummaryItem DisplayFormat="C2" FieldName="salevalue_c" SummaryType="Sum" />
<%--                <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="validpc" SummaryType="Custom" />
                <dx:ASPxSummaryItem DisplayFormat="0.00%" FieldName="excludedpc" SummaryType="Custom" />--%>
             </TotalSummary>
  
        </dx:ASPxGridView>
         
    </div>
    
    <asp:SqlDataSource ID="dsExcluded" runat="server" SelectCommand="sp_ExcludedSalesSummary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="nFrom" SessionField="MonthFrom" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nTo" SessionField="MonthTo" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    

    <dx:ASPxGridViewExporter 
        ID="ASPxGridViewExporter1" 
        runat="server" 
        GridViewID="gridExcluded" 
        PreserveGroupRowStates="True">
    </dx:ASPxGridViewExporter>

</asp:Content>
