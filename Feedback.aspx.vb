﻿Imports System.Net.Mail
Imports System.Data
Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text

Partial Class Feedback

    Inherits System.Web.UI.Page

    Dim da As New DatabaseAccess, ds As DataSet, sErr, sSql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Contact Us"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Contact Us Page", "Viewing Contact Us Page")
            txtEmail.Text = Session("UserEmailAddress")
            txtFeedBack.Text = ""
        End If

        '/ All buttons are visible when the page loads. You only need to hide the ones you don't want.
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

    End Sub

    Protected Sub dsTPSMs_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsTPSMs.Init
        dsTPSMs.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        If txtFeedBack.Text <> "" And txtEmail.Text <> "" Then
            Call SendMail(txtEmail.Text, txtFeedBack.Text)
            txtEmail.Text = Session("UserEmailAddress")
            txtFeedBack.Text = ""
            lblSendMsg.Text = "Your comments have been emailed to the website support team."
        Else
            lblSendMsg.Text = "The Email Address and Comments must be entered."
        End If
        PopSent.ShowOnPageLoad = True

    End Sub

    Sub SendMail(ByVal sUserEmail As String, ByVal sComments As String)

        Dim MailObj As New SmtpClient
        Dim mm As New MailMessage
        Dim basicAuthenticationInfo As New System.Net.NetworkCredential("username", "password")
        basicAuthenticationInfo.UserName = "support@toyotavaluechain.net"
        basicAuthenticationInfo.Password = "4AhK56DmpGvn"
        MailObj.Host = "mail.toyotavaluechain.net"
        MailObj.UseDefaultCredentials = False
        MailObj.Credentials = basicAuthenticationInfo

        mm.To.Add("dick@qubedata.net")
        '        mm.CC.Add("josie@qubedata.net")
        mm.From = New MailAddress(sUserEmail)
        mm.Subject = "NissanTrade Site - Comment from the Contact Us page."

        mm.Body = "Senders Email address - " & sUserEmail & vbCrLf & vbCrLf
        mm.Body += "Comments:" & vbCrLf & vbCrLf & sComments
        MailObj.Send(mm)

    End Sub

    
End Class
