﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PFCPerformance.aspx.vb" Inherits="PFCPerformance2012" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"    Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; font-family: Calibri; text-align: left;" >
    
        <div id="divTitle" style="position:relative;">
            <table width="100%">
                <tr>
                    <td style="width:40%" align="left" valign="middle" > 
                          <dx:ASPxLabel 
        Font-Size="Larger"
        ID="lblPageTitle" 
        runat ="server" 
        Text="Description Code Performance" />
                    </td>
                    <td style="width:60%" align="right" valign="middle" > 
                          <dx:ASPxButton 
                            ID="btnClearFilter" 
                            runat="server" 
                            visible="false"
                            Text="Clear Filters" 
                            AutoPostBack="true">
                        </dx:ASPxButton>
                        <dx:ASPxComboBox
                                runat="server" 
                                ID="ddlYear"
                                AutoPostBack="True">
                          </dx:ASPxComboBox>
                    </td>
                </tr>
            </table>
        </div>

        <br/>

        <dx:ASPxGridView 
            ID="gridPFCPerf" 
            runat="server" 
            Width="100%" 
            OnLoad="GridStyles" style="position:relative;"
            DataSourceID="dsPFCPerf"  
            AutoGenerateColumns="False" 
            KeyFieldName="DealerCode" >
       
            <SettingsPager AllButton-Visible="true" AlwaysShowPager="False" FirstPageButton-Visible="true" LastPageButton-Visible="true" PageSize="15" />
            
            <Settings
                ShowHeaderFilterBlankItems="false"
                ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden"
                ShowGroupFooter="VisibleIfExpanded" 
                UseFixedTableLayout="True" 
                ShowFilterRow="False" />

            <SettingsBehavior 
                AllowSort="False" 
                AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" 
                AllowDragDrop="False" 
                AllowGroup="False" />
            
            <Columns>
                
                <dx:GridViewDataTextColumn 
                    Caption="Desc Code" 
                    FieldName="PFC"
                    Name="PFC"
                    VisibleIndex="0" 
                    Width="10%" 
                    ExportWidth="100">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" AllowSort="True"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Description" 
                    FieldName="PFCDescription"
                    Name="PFCDescription"
                    VisibleIndex="1" 
                    Width="16%" 
                    ExportWidth="200">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList" AllowSort="True"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"/>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn 
                    Caption="Service Description" 
                    FieldName="level4_code"
                    Name="level4_code"
                    VisibleIndex="2" 
                    Width="16%" 
                    ExportWidth="200">
                    <Settings AllowGroup="False" AllowHeaderFilter="True" HeaderFilterMode="CheckedList"  AllowSort="True"/>
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Units" 
                        FieldName="Units_1"
                        Name="Units_1"
                        Width="9%" 
                        ToolTip="Units Sold in Year Shown. Click on Column Header to sort by this value (ascending or descending)"
                        VisibleIndex="3" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="#,##0">
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Sales" 
                        FieldName="Value_1"
                        Width="9%" 
                        Name="Value_1"
                        ToolTip="Invoiced Sales in Year Shown. Click on Column Header to sort by this value (ascending or descending)"
                        VisibleIndex="4" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Units" 
                        FieldName="Units_2"
                        Width="9%" 
                        ToolTip="Units Sold in Year Shown. Click on Column Header to sort by this value (ascending or descending)"
                        Name="Units_2"
                        VisibleIndex="5" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="#,##0">
                        </PropertiesTextEdit>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Sales" 
                        FieldName="Value_2"
                        Width="9%" 
                        Name="Value_2"
                        ToolTip="Invoiced Sales in Year Shown. Click on Column Header to sort by this value (ascending or descending)"
                        VisibleIndex="6" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0"/>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Units Uplift" 
                        FieldName="Units_Diff" 
                        Width="9%" 
                        Name="Units_Diff"
                        ToolTip="Annual increase/decrease in Units Sold in 2 Years Shown. Click on Column Header to sort by this value (ascending or descending)"
                        VisibleIndex="7" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="#,##0"/>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn 
                        Caption="Sales Uplift" 
                        FieldName="Value_Diff"
                        Name="Value_Diff"
                        Width="9%" 
                        ToolTip="Annual increase/decrease in Invoiced Sales in 2 Years Shown. Click on Column Header to sort by this value (ascending or descending)"
                        VisibleIndex="8" 
                        ExportWidth="100">
                        <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="True"/>
                        <HeaderStyle Wrap="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <PropertiesTextEdit DisplayFormatString="&#163;#,##0"/>
                        <CellStyle HorizontalAlign="Center"/>
                    </dx:GridViewDataTextColumn>

            </Columns>

        </dx:ASPxGridView>
        
    </div>
    
    <asp:SqlDataSource ID="dsPFCPerf" runat="server" SelectCommand="p_PFC_Comparison" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="PFCPerfYear" Type="Int16" Size="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridPFCPerf" PreserveGroupRowStates="False">
    </dx:ASPxGridViewExporter>

</asp:Content>



