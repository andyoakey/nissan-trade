﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Cookies.aspx.vb" Inherits="Cookies" %>

<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dxm" %>
<%@ Register assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dxe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style2
        {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }
        .style3
        {
            line-height: normal;
            font-size: 13.5pt;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
        }
        .style8
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: large;
        }
        .style10
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        </style>
</head>
<body>

    <form id="form1" runat="server" style="width:976px">

        <div id="Page" style="width: 976px">
            <div id="PageHeader" style="height: 75px; width: 976px">
                <table class="style2">
                    <tr>
                        <td class="style3" align="left" valign="top" style="width: 75%">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images2014/PartsHeader.jpg" Width="488px" Height="68px" />
                        </td>
                        <td class="style3" align="right" valign="top">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images2014/Toyota.jpg" Width="96px" Height="68px" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="MenuDiv" style="height: 35px; width: 976px; text-align: left; border-bottom-style: solid;
                border-bottom-width: medium; border-bottom-color: #000000;">
            </div>
        </div>


        <div id="MainDiv" style="width:976px;">

            <h2 class="style11">Cookies</h2>
            
            <h3><span class="style8">What is a cookie?</span><o:p></h3>
            <p>
                <span class="style10">
                    Cookies are text files containing information downloaded to 
                    your PC, mobile or other device when you visit a website. They are then sent 
                    back to the originating website per visit or to another website that recognises 
                    that cookie; this allows a website to recognise a user&#39;s device.
                </span>
            </p>
            
            <h3><span class="style8">Persistent cookies</span></h3>
            <p>
                <span class="style10">
                    These remain on a user&#39;s device for a time period specified in the cookie. They 
                    are activated each time the user visits the website that created that particular 
                    cookie.
                </span>
            </p>
            
            <h3><span class="style8">Session cookies</span></h3>
            <p>
                <span class="style10">
                    These cookies allow website operators to link the actions of a user during a 
                    browser session. The session starts when a user opens the browser window and 
                    finishes when they close the browser window. Session cookies are created 
                    temporarily - once the browser is closed, all session 
                    cookies are deleted. Cookies do many different jobs, like navigating between 
                    pages efficiently, remembering your preferences, and improving the user experience.
                    You can find more information about cookies at 
                       <a href="http://www.allaboutcookies.org" target="_blank" title="This link opens in a new window">www.allaboutcookies.org</a>
                    and
                       <a href="http://www.youronlinechoices.eu" target="_blank" title="This link opens in a new window">www.youronlinechoices.eu</a>            
                </span>
            </p>

            <h3><span class="style8">Cookies used on the website</span></h3>
            <p>
                <span class="style10">
                    A list of the cookies used on the website by category is set out below.            
                </span>
            </p>

            <h3><span class="style8">Strictly necessary cookies</span></h3>
            <p>
                <span class="style10">
                    These cookies enable services you specifically ask for. For 
                    those types of cookies that are strictly necessary, no consent is required. 
                    These cookies are essential in order to enable you to move around the website 
                    and use its features, such as accessing secure areas. Without these cookies, the 
                    services you ask for cannot be provided.
                </span>
            </p>

            <h3><span class="style8">Functionality cookies</span></h3>
            <p>
                <span class="style10">
                    These cookies remember choices you make to improve your experience.<o:p></o:p>They 
                    allow the website to remember choices you made (such as your name or the region 
                    you&#39;re in) and provide enhanced, more personal features. These cookies can also 
                    be used to remember changes made to text size, fonts and other parts of web 
                    pages that can be customised. The information these cookies collect may not 
                    reveal the person’s identity and they cannot track browsing activity on other 
                    websites.            
                </span>
            </p>

            <h3><span class="style8">Using browser settings to manage cookies</span></h3>
            <p>
                <span class="style10">
                    The help function on the menu bar of most browsers will inform you how to stop accepting 
                    new cookies, how they can notify you when you receive a new cookie and how to 
                    disable them altogether. Similar data can also be disabled or deleted as used by 
                    browser add-ons, such as Flash cookies, by changing the add-on&#39;s settings or 
                    visiting the website of its manufacturer.<o:p></o:p>However, because cookies 
                    allow you to take advantage of some of the website&#39;s essential features they can 
                    be left enabled. For example, if you block or otherwise reject cookies you may 
                    not be able to use our products, services and other facilities. If you leave 
                    cookies turned on, remember to sign off when you finish using a shared computer.            
                </span>
            </p>

            <h3><span class="style8">Other websites</span></h3>
            <p>
                <span class="style10">
                    The website may contain links to other sites which are 
                    outside our control and not covered by this policy. Operators of these sites may 
                    collect information from you that will be used by them in accordance with their 
                    policy, which may differ from ours.            
                </span>
            </p>
            <p>
            </p>

            <dxe:ASPxButton ID="btnBack" runat="server" Text="Back to Login" 
                runat="server"  Width="120px" style="text-align: left">
            </dxe:ASPxButton>

        </div>

    </form> 
    
</body>
</html>
