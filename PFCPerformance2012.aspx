﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PFCPerformance2012.aspx.vb" Inherits="PFCPerformance2012" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; margin: 0 auto; left: 0px; width:976px; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="PFC Performance Report" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />

        <dxwgv:ASPxGridView ID="gridPFCPerf" runat="server" width="976px" 
            DataSourceID="dsPFCPerf"  AutoGenerateColumns="False" KeyFieldName="DealerCode" >
       
            <Styles >
                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                </Header>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowHeaderFilterButton="True"
                ShowStatusBar="Hidden"
                ShowGroupFooter="VisibleIfExpanded" 
                UseFixedTableLayout="True" VerticalScrollableHeight="300" 
                ShowFilterRow="True" ShowVerticalScrollBar="True" />

            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <Images >
                <CollapsedButton Height="12px" 
                    Width="11px" />
                <ExpandedButton Height="12px" 
                    Width="11px" />
                <DetailCollapsedButton Height="12px" 
                    Width="11px" />
                <DetailExpandedButton Height="12px" 
                    Width="11px" />
                <FilterRowButton Height="13px" Width="13px" />
            </Images>
            
            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="PFC" FieldName="PFC"
                    VisibleIndex="1" Width="10%">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="True" ShowFilterRowMenu="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="PFCDescription"
                    VisibleIndex="2" Width="25%">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="True" ShowFilterRowMenu="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Product Cat." FieldName="TradeAnalysis"
                    VisibleIndex="3" Width="10%">
                    <Settings AllowGroup="False" AllowAutoFilter="True" AllowHeaderFilter="True" 
                        AllowSort="True" ShowFilterRowMenu="True" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" />
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Units 2013" FieldName="Units_1"
                    VisibleIndex="4">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Sales 2013" FieldName="Value_1"
                    VisibleIndex="5">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Units 2014" FieldName="Units_2"
                    VisibleIndex="6">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>


                <dxwgv:GridViewDataTextColumn Caption="Sales 2014" FieldName="Value_2"
                    VisibleIndex="7">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Diff Units" FieldName="Units_Diff"
                    VisibleIndex="8">
                    <Settings AllowGroup="False" AllowAutoFilter="False" AllowHeaderFilter="False" 
                        AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="#,##0">
                    </PropertiesTextEdit>
                    <CellStyle HorizontalAlign="Center" Font-Size="Small">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Diff Sales" FieldName="Value_Diff"
                    VisibleIndex="9">
                    <Settings AllowAutoFilter="False" AllowHeaderFilter="False" AllowSort="False" />
                    <HeaderStyle Wrap="True" HorizontalAlign="Center" Font-Size="X-Small" 
                        VerticalAlign="Middle" />
                    <PropertiesTextEdit DisplayFormatString="&#163;#,##0">
                    </PropertiesTextEdit>
                    <CellStyle Font-Size="Small" HorizontalAlign="Center">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />

        <br />
        
    </div>
    
    <asp:SqlDataSource ID="dsPFCPerf" runat="server" SelectCommand="p_PFC_Comparison_2013" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridPFCPerf" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>



