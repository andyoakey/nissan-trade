﻿Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports DevExpress.Web


Partial Class ProcessNewAccounts

    Inherits System.Web.UI.Page

    Dim Total_i As Decimal = 0
    Dim Total_x As Decimal = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Process New Accounts"
        btnProcessed.ToolTip = "If ALL the selected A/Cs Appear valid Click here.  These A/Cs will NOT appear on this report again."
        btnExclude.ToolTip = "If ALL the selected A/Cs Appear to be invalid Click here.  These A/Cs will be removed from the website. These A/Cs will NOT appear on this report again."
    End Sub

    Protected Sub dsDealerBonus_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsProcessNew.Init
        dsProcessNew.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Not IsPostBack Then
            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
            Session("AnalysisType") = 1
            Session("MonthFrom") = GetDataLong("Select Min(Id) FROM DataPeriods WHERE PeriodStatus <> 'C'")
            Session("MonthTo") = Session("MonthFrom")
            Session("FocusType") = 0
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Excluded Sales Summary", "Excluded Sales Summary")
        End If

        grid.DataBind()

    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "New Accounts"
        ASPxGridViewExporter1.FileName = sFileName
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Private Sub btn_Click(sender As Object, e As EventArgs) Handles btnProcessed.Click, btnExclude.Click

        Dim nLoop As Integer = 0
        Dim nItemCount As Integer = selList.Items.Count()
        Dim sAccountString As String = ""
        Dim nExcludeFlag As Integer = 0

        If nItemCount = 0 Then
            Exit Sub
        End If

        If sender.id = "btnProcessed" Then
            nExcludeFlag = 0
        Else
            nExcludeFlag = 1
        End If

        For nLoop = 0 To (nItemCount - 1)   ' Base 0
            Call RunNonQueryCommand("exec sp_ProcessNewAccount '" & Trim(selList.Items(nLoop).Text) & "'," & nExcludeFlag & "," & Session("UserID"))
        Next

        selList.Items.Clear()

        grid.DataBind()



    End Sub


    Protected Sub BusinessType_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sCustId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "customerid").ToString()
        Dim sBTypeId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "businesstypeid").ToString()
        Dim sBTypeDesc As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "businesstypedescription").ToString()
        Dim sContentUrl As String = String.Format("{0}?Id={1}&BType={2}", "ViewCompaniesBT.aspx", sCustId, sBTypeId)

        link.Text = ShortBusinessType(sBTypeDesc)
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewCompaniesBT('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to update the business type."
        link.ForeColor = System.Drawing.Color.Black
        link.Font.Underline = True

    End Sub

    Private Function ShortBusinessType(ByVal sBT As String) As String
        Dim sShort As String
        Select Case Trim(sBT)
            Case "Bodyshop"
                sShort = "Bodyshop"
            Case "Breakdown Recovery Services"
                sShort = "Breakdown"
            Case "Car Dealer New/Used"
                sShort = "Dealer "
            Case "Independent Motor Trader"
                sShort = "IMT"
            Case "Mechanical Specialist"
                sShort = "Mech Spec"
            Case "Motor Factor"
                sShort = "Factor"
            Case "Parts Supplier"
                sShort = "Part Supp"
            Case "Taxi & Private Hire"
                sShort = "Taxi P/H"
            Case "Windscreens"
                sShort = "Windscreen"
            Case Else
                sShort = "Other"
        End Select
        ShortBusinessType = sShort
    End Function


    Protected Sub grid_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles grid.RowUpdating

        Dim nCustomerDealerID As Long = Convert.ToString(e.OldValues("id"))
        Dim sBusinessName As String = Trim(Convert.ToString(e.NewValues("businessname")))
        Dim sAddress1 As String = Trim(Convert.ToString(e.NewValues("address1")))
        Dim sAddress2 As String = Trim(Convert.ToString(e.NewValues("address2")))
        Dim sAddress3 As String = Trim(Convert.ToString(e.NewValues("address3")))
        Dim sAddress4 As String = Trim(Convert.ToString(e.NewValues("address4")))
        Dim sAddress5 As String = Trim(Convert.ToString(e.NewValues("address5")))
        Dim sPostCode As String = Trim(Convert.ToString(e.NewValues("postcode")))

        Dim sSql As String = "exec sp_HOUpdateNewCustomerDealer " _
            & nCustomerDealerID & "," _
            & "'" & sBusinessName & "'," _
            & "'" & sAddress1 & "'," _
            & "'" & sAddress2 & "'," _
            & "'" & sAddress3 & "'," _
            & "'" & sAddress4 & "'," _
            & "'" & sAddress5 & "'," _
            & "'" & sPostCode & "'"

        Call RunNonQueryCommand(sSql)

        grid.CancelEdit()

    End Sub




End Class