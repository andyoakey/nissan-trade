﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MCChallBreakdown.aspx.vb" Inherits="MCChall2011Breakdown" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width:976px; margin: 0 auto; text-align: left;" >
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblCentreDetails" runat="server" Text="" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
            <br />
        </div>

        <dxwgv:ASPxGridView ID="gridCampaigns" runat="server" width="966px"
            DataSourceID="dsCampaigns"  AutoGenerateColumns="False" KeyFieldName="DealerCode" 
            style="position:relative; top:20px; left:5px">
       
            <SettingsPager PageSize="120" Mode="ShowAllRecords">
            </SettingsPager>
            
            <Settings
                ShowFooter="True"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                ShowGroupFooter="VisibleIfExpanded" ShowVerticalScrollBar="True" 
                UseFixedTableLayout="True" VerticalScrollableHeight="300"  />
            
            <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />
            
            <Styles>
                <Header Wrap="True" HorizontalAlign="Center" />
                <Cell Font-Size="Small" HorizontalAlign="Center" />
                <Footer HorizontalAlign="Center" VerticalAlign="Middle" />
            </Styles>

            <Columns>
                
                <dxwgv:GridViewDataTextColumn Caption="PFC" FieldName="PFC" VisibleIndex="0" Width="5%" ExportWidth="100" />

                <dxwgv:GridViewDataTextColumn Caption="Description" FieldName="PFCDescription" VisibleIndex="1" Width="25%" ExportWidth="200" />

                <dxwgv:GridViewDataTextColumn Caption="Jul" FieldName="Month1" VisibleIndex="2" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Aug" FieldName="Month2" VisibleIndex="3" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Sep" FieldName="Month3" VisibleIndex="4" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Oct" FieldName="Month4" VisibleIndex="5" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Nov" FieldName="Month5" VisibleIndex="6" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Dec" FieldName="Month6" VisibleIndex="7" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn Caption="Total" FieldName="TotalSpend" VisibleIndex="8" ExportWidth="120">
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <TotalSummary>
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month1" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month2" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month3" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month4" SummaryType="Sum" />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month5" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="Month6" SummaryType="Sum"  />
                <dxwgv:ASPxSummaryItem DisplayFormat="&#163;##,##0" FieldName="TotalSpend" SummaryType="Sum"  />
            </TotalSummary>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>

            <SettingsDetail ShowDetailButtons="False" />

        </dxwgv:ASPxGridView>
        <br />

        <table width="976px" style="position:relative;top:10px">
            <tr>
                <td align="left" valign="top" style="width:50%">
                </td>
                <td align="right" valign="top" >

                    <dxwgv:ASPxGridView ID="gridCampaignsSummary" runat="server" width="376px" 
                        OnCustomColumnDisplayText="Summary_OnCustomColumnDisplayText" 
                        DataSourceID="dsCampaignsSummary"  AutoGenerateColumns="False">
                   
                        <SettingsPager PageSize="120" Mode="ShowAllRecords">
                        </SettingsPager>
                        
                        <Settings
                            ShowFooter="False"  ShowStatusBar="Hidden" ShowTitlePanel="True"
                            ShowGroupFooter="VisibleIfExpanded" VerticalScrollableHeight="350" 
                            ShowColumnHeaders="False"  />
                        
                        <SettingsBehavior AllowSort="False" AutoFilterRowInputDelay="12000" 
                            ColumnResizeMode="Control" AllowDragDrop="False" AllowGroup="False" />

                        <Styles>
                            <Header Wrap="True" HorizontalAlign="Center" />
                            <Cell Font-Size="Small" HorizontalAlign="Center" />
                            <Footer HorizontalAlign="Center" VerticalAlign="Middle" />
                        </Styles>

                       <Columns>
                            
                            <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowDescription" VisibleIndex="0" Width="60%" />

                            <dxwgv:GridViewDataTextColumn Caption="" FieldName="RowValue" VisibleIndex="1" />

                        </Columns>
                        
                        <StylesEditors>
                            <ProgressBar Height="25px">
                            </ProgressBar>
                        </StylesEditors>

                        <SettingsDetail ShowDetailButtons="False" />

                    </dxwgv:ASPxGridView>
                </td>
            </tr>
        
        </table>
        <br />
    </div>
    
    <asp:SqlDataSource ID="dsCampaigns" runat="server" SelectCommand="p_MCChallenge2015_DealerDetail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="MCDealerCode" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCampaignsSummary" runat="server" SelectCommand="p_MCChallenge2015_DealerDetail_Summary" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sDealerCode" SessionField="MCDealerCode" Type="String" Size="6" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridCampaigns" PreserveGroupRowStates="False">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>


