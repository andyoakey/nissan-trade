﻿Imports System.Data
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class ServiceKitYOYDetail

    Inherits System.Web.UI.Page

    Dim dsInvoices As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim htin As New Hashtable
    Dim nTotalSale As Double
    Dim nTotalCost As Double

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.lblPageTitle.Text = "Service Kit Invoices for " & Left(Request.QueryString(0), 4) & "/" & Right(Request.QueryString(0), 2)
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Service Kit YOY - Invoice List", "Viewing List of Service Kit Invoices")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        '  Handle Master Page Button Clicks
        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

        Dim master_btnBack As DevExpress.Web.ASPxButton
        master_btnBack = CType(Master.FindControl("btnMasterBack"), DevExpress.Web.ASPxButton)
        master_btnBack.Visible = False

        If Session("BackClicked") = True Then
            Session("BackClicked") = False
            Response.Redirect("ServiceKitYOY.aspx")
        End If
        '  End Handle Master Page Button Clicks


        Session("ReturnProgram") = "ProductReports.aspx"

        Call LoadInvoices()

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If



    End Sub

    Sub LoadInvoices()

        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nYear", Left(Request.QueryString(0), 4))
        htin.Add("@nMonth", Right(Request.QueryString(0), 2))

        dsInvoices = da.Read(sErrorMessage, "p_ServiceKitData_Detail", htin)
        gridInvoices.DataSource = dsInvoices.Tables(0)
        gridInvoices.DataBind()

    End Sub

    Protected Sub gridInvoices_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.0") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub gridInvoices_CustomSummaryCalculate(ByVal sender As Object, ByVal e As DevExpress.Data.CustomSummaryEventArgs) Handles gridInvoices.CustomSummaryCalculate

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Start Then
            nTotalSale = 0
            nTotalCost = 0
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Calculate Then
            nTotalSale = nTotalSale + e.GetValue("SaleValue")
            nTotalCost = nTotalCost + e.GetValue("CostValue")
        End If

        If e.SummaryProcess = DevExpress.Data.CustomSummaryProcess.Finalize Then
            If nTotalSale <> 0 And nTotalCost <> 0 Then
                e.TotalValue = ((nTotalSale - nTotalCost) / nTotalSale)
            Else
                e.TotalValue = 0
            End If
        End If

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    'Protected Sub btnExcel_Click()
    '    ASPxGridViewExporter1.FileName = "ServiceKitInvoices_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
    '    ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
    'End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = Replace(KillSpaces(Me.lblPageTitle.Text), "/", "-")
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)

    End Sub


    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim dealerCode = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "DealerCode").ToString()

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub


End Class
