﻿Imports DevExpress.Web

Partial Class ChallengeCup

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sDate As String = ""
        Me.Title = "Nissan Trade Site - Campaign Challenge Cup"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not Page.IsPostBack Then
            sDate = GetMaxDate(Session("SelectionLevel"), Session("SelectionId"))
            lblPageTitle.Text = "Campaign Challenge Cup 2011 Q4 - As at " & sDate
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Campaign Challenge Cup", "Viewing Campaign Challenge Cup")
        End If
    End Sub

    Protected Sub dsCampaigns_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaigns.Init
        dsCampaigns.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dscampaignDetails_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCampaignDetails.Init
        dsCampaignDetails.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub



    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub gridCampaigns_DetailRowGetButtonVisibility(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewDetailRowButtonEventArgs) Handles gridCampaigns.DetailRowGetButtonVisibility
        If (e.VisibleIndex > -1) Then
            Dim gv As ASPxGridView = CType(sender, ASPxGridView)
            Dim sDealerCode As String
            sDealerCode = gv.GetRowValues(e.VisibleIndex, "DealerCode")
            If Not CanViewDealer(Session("SelectionLevel"), Session("SelectionId"), sDealerCode) Then
                e.ButtonState = GridViewDetailRowButtonState.Hidden
            End If
        End If
    End Sub

    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCampaigns.HtmlDataCellPrepared
        If e.DataColumn.Index >= 2 And e.DataColumn.Index <= 7 Then
            If e.CellValue = 0 Then
                e.Cell.BackColor = Drawing.Color.Red
                e.Cell.ForeColor = Drawing.Color.White
            Else
                e.Cell.ForeColor = Drawing.Color.Green
            End If
        End If
    End Sub

    Protected Sub gridDetail_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)
        If e.GetValue("Description") = "Total Points" Then
            e.Cell.BackColor = Drawing.Color.Red
            e.Cell.ForeColor = Drawing.Color.White
        End If
    End Sub

    Protected Sub AllGrids_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.Index >= 2 And e.Column.Index <= 7 Then
            If e.Value > 0 Then
                e.DisplayText = "√"
            Else
                e.DisplayText = "X"
            End If
        End If
    End Sub

    Protected Sub gridDetail_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("DealerCode") = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
    End Sub

End Class
