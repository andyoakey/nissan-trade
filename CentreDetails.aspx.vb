﻿Imports System.Data
Imports DevExpress.Export
Imports DevExpress.Web
Imports DevExpress.XtraPrinting

Partial Class CentreDetails

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Nissan Trade Site - Dealer Details"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Dealer List", "Viewing Dealer List")
        End If
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If


    End Sub

    Protected Sub dsZones_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsZones.Init
        dsZones.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsTPSMs_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsTPSMs.Init
        dsTPSMs.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCentres_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCentres.Init
        dsCentres.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridCentres_HeaderFilterFillItems(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewHeaderFilterEventArgs) Handles gridCentres.HeaderFilterFillItems

        Dim da As New DatabaseAccess
        Dim dsTPSMs As DataSet
        Dim dsZones As DataSet
        Dim sErrTPSMs As String = ""
        Dim sSQL As String = ""

        If e.Column.FieldName = "Zone" Then

            e.Values.Clear()

            sSQL = "SELECT TPSM, Id FROM Toyota_TPSMs ORDER BY Id"
            dsTPSMs = da.ExecuteSQL(sErrTPSMs, sSQL)
            If dsTPSMs.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsTPSMs.Tables(0).Rows.Count - 1
                    e.AddValue("TPSM : " & dsTPSMs.Tables(0).Rows(i).Item("TPSM"), "", "[TPSMId]=" & dsTPSMs.Tables(0).Rows(i).Item("ID"))
                Next
            End If

            sSQL = "SELECT DISTINCT Zone FROM CentreMaster ORDER BY Zone"
            dsZones = da.ExecuteSQL(sErrTPSMs, sSQL)
            If dsZones.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsZones.Tables(0).Rows.Count - 1
                    e.AddValue("Zone : " & dsZones.Tables(0).Rows(i).Item("Zone"), "", "[Zone]='" & dsZones.Tables(0).Rows(i).Item("Zone") & "'")
                Next
            End If

        End If

    End Sub

    Protected Sub gridCentres_RowUpdating(ByVal sender As Object, ByVal e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gridCentres.RowUpdating

        Dim da As New DatabaseAccess
        Dim sErr As String = ""
        Dim htIn As New Hashtable
        Dim nRes As Long

        htIn.Add("@sDealerCode", Convert.ToString(e.OldValues("DealerCode")))
        htIn.Add("@sZone", Convert.ToString(e.NewValues("Zone")))
        htIn.Add("@sArea", Convert.ToString(e.NewValues("Area")))
        htIn.Add("@sCentreName", Convert.ToString(e.NewValues("CentreName")))
        htIn.Add("@sMarketingName", Convert.ToString(e.NewValues("MarketingName")))
        htIn.Add("@sMarketAreaDescription", Convert.ToString(e.NewValues("MarketAreaDescription")))
        htIn.Add("@sTelephoneNumber", Convert.ToString(e.NewValues("TelephoneNumber")))
        htIn.Add("@sFaxNumber", Convert.ToString(e.NewValues("FaxNumber")))
        htIn.Add("@sContact", Convert.ToString(e.NewValues("Contact")))
        htIn.Add("@sPosition", Convert.ToString(e.NewValues("Position")))
        htIn.Add("@sEmailAddress", Convert.ToString(e.NewValues("Email")))
        htIn.Add("@sAddress1", Convert.ToString(e.NewValues("Address1")))
        htIn.Add("@sAddress2", Convert.ToString(e.NewValues("Address2")))
        htIn.Add("@sAddress3", Convert.ToString(e.NewValues("Address3")))
        htIn.Add("@sAddress4", Convert.ToString(e.NewValues("Address4")))
        htIn.Add("@sPostCode", Convert.ToString(e.NewValues("Postcode")))
        htIn.Add("@nBand1", Convert.ToString(e.NewValues("MileageBand1")))
        htIn.Add("@nBand2", Convert.ToString(e.NewValues("MileageBand2")))
        htIn.Add("@sUF1", Convert.ToString(e.NewValues("Userfield1")))
        htIn.Add("@sUF2", Convert.ToString(e.NewValues("Userfield2")))
        htIn.Add("@sUF3", Convert.ToString(e.NewValues("Userfield3")))
        htIn.Add("@sUF4", Convert.ToString(e.NewValues("Userfield4")))

        nRes = da.Update(sErr, "p_Update_CentreDetails", htIn)

        e.Cancel = True
        If sErr.Length = 0 Then
            gridCentres.CancelEdit()
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Dealer List", "Edit Dealer Details for Dealer : " & e.OldValues("DealerCode"))
        Else
            Session("ErrorToDisplay") = sErr
            Response.Redirect("dbError.aspx")
        End If

    End Sub

    Protected Sub ExpGridDealer_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ExpGridDealer.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()
        Dim sFileName As String = "DealerListing"

        Dim nCols As Integer
        Dim nRows As Integer

        nCols = gridCentres.Columns.Count - 1
        nRows = gridCentres.VisibleRowCount

        gridCentres.Columns("MarketingName").Visible = True
        gridCentres.Columns("MarketAreaDescription").Visible = True
        gridCentres.Columns("FaxNumber").Visible = True
        gridCentres.Columns("Contact").Visible = True
        gridCentres.Columns("Position").Visible = True
        gridCentres.Columns("Address1").Visible = True
        gridCentres.Columns("Address2").Visible = True
        gridCentres.Columns("Address3").Visible = True
        gridCentres.Columns("Address4").Visible = True
        gridCentres.Columns("Postcode").Visible = True

        ExpGridDealer.FileName = sFileName
        ExpGridDealer.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        gridCentres.Columns("MarketingName").Visible = False
        gridCentres.Columns("MarketAreaDescription").Visible = False
        gridCentres.Columns("FaxNumber").Visible = False
        gridCentres.Columns("Contact").Visible = False
        gridCentres.Columns("Position").Visible = False
        gridCentres.Columns("Address1").Visible = False
        gridCentres.Columns("Address2").Visible = False
        gridCentres.Columns("Address3").Visible = False
        gridCentres.Columns("Address4").Visible = False
        gridCentres.Columns("Postcode").Visible = False

    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Private Sub gridCentres_CommandButtonInitialize(sender As Object, e As ASPxGridViewCommandButtonEventArgs) Handles gridCentres.CommandButtonInitialize
        If isQubeUser(Session("UserEmailAddress")) = False Then
            If (e.ButtonType = ColumnCommandButtonType.Edit) Then
                e.Visible = False
            End If
        End If
    End Sub
End Class
