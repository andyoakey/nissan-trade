﻿Imports System.Data
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class VisitActions

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Toyota PARTS website - Visit Actions"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        ' Hide Excel Button ( on masterpage) 
        Dim myexcelbutton As DevExpress.Web.ASPxButton
        myexcelbutton = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        If Not myexcelbutton Is Nothing Then
            myexcelbutton.Visible = False
        End If

        ' Hide PDFl Button ( on masterpage) 
        Dim mypdfbutton As DevExpress.Web.ASPxButton
        mypdfbutton = CType(Master.FindControl("btnPDF"), DevExpress.Web.ASPxButton)
        If Not mypdfbutton Is Nothing Then
            mypdfbutton.Visible = False
        End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Visit Action List", "Viewing Visit Action List")
            ddlYear.Items.Add("2016")
            ddlYear.Items.Add("2015")
            ddlYear.SelectedIndex = 0
            Session("VARYear") = 2016
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("UserLevel") = "C" Or Session("UserLevel") = "G" Then
            gridVAR.Columns(6).Visible = False
            gridVAR.Columns(7).Visible = False
            btnNewVisit.Visible = False
        Else
            gridVAR.Columns(6).Visible = True
            gridVAR.Columns(7).Visible = True
            btnNewVisit.Visible = True
        End If
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If
    End Sub

    Protected Sub dsVAR_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVAR.Init
        dsVAR.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsVARDetail_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsVarDetail.Init
        dsVarDetail.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ExpGridDealer_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()

        Dim sFileName As String = "VisitActions"

        Dim nCols As Integer
        Dim nRows As Integer

        nCols = gridVAR.Columns.Count - 1
        nRows = gridVAR.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            gridVAR.Columns(6).Visible = False
            gridVAR.Columns(7).Visible = False
            ASPxGridViewExporter1.FileName = sFileName
            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
            gridVAR.Columns(6).Visible = True
            gridVAR.Columns(7).Visible = True


        End If

    End Sub

    Protected Sub gridVAR_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs) Handles gridVAR.CustomColumnDisplayText

        If e.Column.FieldName = "Status" Then
            Select Case e.Value
                Case 0
                    e.DisplayText = "Open"
                Case 1
                    e.DisplayText = "Partially complete"
                Case 2
                    If e.GetFieldValue("OutstandingItems") = 1 Then
                        e.DisplayText = "Awaiting Confirmation"
                    Else
                        e.DisplayText = "Completed late"
                    End If
                Case 3
                    If e.GetFieldValue("OutstandingItems") = 1 Then
                        e.DisplayText = "Awaiting Confirmation"
                    Else
                        e.DisplayText = "Completed on time"
                    End If
            End Select
        End If
        If e.Column.FieldName = "VisitDate" Then
            e.DisplayText = Format(CDate(e.Value), "dd-MMM-yyyy")
        End If

    End Sub

    Protected Sub gridVARDetail_CustomColumnDisplayText(sender As Object, e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)

        If e.Column.FieldName = "DetailStatus" Then
            Select Case e.Value
                Case 0
                    e.DisplayText = "Outstanding"
                Case 1
                    If IsDBNull(e.GetFieldValue("ConfirmedDate")) Then
                        e.DisplayText = "Awaiting TPSM approval"
                    Else
                        e.DisplayText = "Completed late"
                    End If
                Case 2
                    If IsDBNull(e.GetFieldValue("ConfirmedDate")) Then
                        e.DisplayText = "Awaiting TPSM approval"
                    Else
                        e.DisplayText = "Completed on time"
                    End If
            End Select
        End If
        If e.Column.FieldName = "TargetDate" Then
            e.DisplayText = Format(CDate(e.Value), "dd-MMM-yyyy")
        End If

    End Sub

    Protected Sub gridVAR_FilterControlCustomValueDisplayText(sender As Object, e As FilterControlCustomValueDisplayTextEventArgs) Handles gridVAR.FilterControlCustomValueDisplayText

        Select Case e.Value
            Case 0
                e.DisplayText = "Outstanding"
            Case 1
                e.DisplayText = "Completed late"
            Case 2
                e.DisplayText = "Completed on time"
        End Select

    End Sub

    Protected Sub gridVAR_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridVAR.HtmlDataCellPrepared

        If e.DataColumn.FieldName = "Status" Then
            Select Case e.CellValue
                Case 0
                    e.Cell.BackColor = GlobalVars.g_Color_White
                Case 1
                    e.Cell.BackColor = GlobalVars.g_Color_Yellow
                Case 2
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Red
                Case 3
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Green
            End Select
        End If

    End Sub

    Protected Sub gridVARDetail_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs)

        If e.DataColumn.FieldName = "Status" Then
            Select Case e.CellValue
                Case 0
                    e.Cell.BackColor = GlobalVars.g_Color_LightGray
                Case 1
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Red
                Case 2
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Green
            End Select
        End If

    End Sub

    Protected Sub gridVARDetail_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        Dim trxgrid As ASPxGridView = CType(sender, ASPxGridView)
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("VARId") = sKey
        If Session("SelectionLevel") = "C" Or Session("SelectionLevel") = "G" Then
            trxgrid.Columns(0).Visible = False
            trxgrid.Columns(6).Visible = False
            trxgrid.Columns(8).Visible = False
        End If
    End Sub

    Protected Sub CompletionDate_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sVarDetailsId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sComplDate As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "CompletionDate").ToString()
        Dim sConfDate As String = IIf(IsDBNull(templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ConfirmedDate").ToString()), "", templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ConfirmedDate").ToString())
        Dim sContentUrl As String = String.Format("{0}?id=1{1}", "VisitActionsDate.aspx", sVarDetailsId)

        If sComplDate = "" Then
            link.ImageUrl = "~/Images2014/date_add.png"
        Else
            link.Text = Format(CDate(sComplDate), "dd-MMM-yyyy")
        End If
        If sConfDate = "" Then
            link.ClientSideEvents.Click = String.Format("function(s, e) {{ VisitActionDate('{0}'); }}", sContentUrl)
            link.ToolTip = "Click here to add update the date."
        Else
            link.Font.Underline = False
        End If

    End Sub

    Protected Sub ConfirmedDate_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sVarDetailsId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sComplDate As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "ConfirmedDate").ToString()
        Dim sContentUrl As String = String.Format("{0}?id=2{1}", "VisitActionsDate.aspx", sVarDetailsId)

        If sComplDate = "" Then
            link.ImageUrl = "~/Images2014/date_add.png"
        Else
            link.Text = Format(CDate(sComplDate), "dd-MMM-yyyy")
        End If
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ VisitActionDate('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to add update the date."

    End Sub

    Protected Sub RemoveActivity_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sVarDetailsId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sContentUrl As String = String.Format("{0}?id=1{1}", "VisitActionRemove.aspx", sVarDetailsId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/user_block.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ActivityWindow('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to remove this Activity line."

    End Sub

    Protected Sub RemoveVAR_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim sVarDetailsId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "Id").ToString()
        Dim sContentUrl As String = String.Format("{0}?id=0{1}", "VisitActionRemove.aspx", sVarDetailsId)

        link.NavigateUrl = "javascript:void(0);"
        link.ImageUrl = "~/Images2014/user_block.png"
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ActivityWindow('{0}'); }}", sContentUrl)
        link.ToolTip = "Click here to remove this Activity line."

    End Sub

    Protected Sub btnNewVisit_Click(sender As Object, e As EventArgs) Handles btnNewVisit.Click
        Response.Redirect("VisitActionAddNew.aspx")
    End Sub

    Protected Sub gridVAR_DataBound(sender As Object, e As EventArgs) Handles gridVAR.DataBound
        Dim grid As ASPxGridView = TryCast(sender, ASPxGridView)
        Dim nId As Integer
        For i As Integer = 0 To grid.VisibleRowCount - 1
            nId = grid.GetRowValues(i, "Id").ToString()
            If GetDataLong("SELECT COUNT(*) FROM Web_Var_Details WHERE VARId = " & Trim(Str(nId)) & " AND ConfirmedDate IS NULL") > 0 Then
                grid.DetailRows.ExpandRow(i)
            End If
        Next i
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("VARYear") = ddlYear.SelectedItem.Text
    End Sub

    Protected Sub btnMeetings_Click(sender As Object, e As EventArgs) Handles btnMeetings.Click
        Response.Redirect("VisitMeetings.aspx")
    End Sub
End Class

