﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RewardProcessing201601.aspx.vb" Inherits="RewardProcessing201601" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register assembly="DevExpress.Web.v14.2" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div style="position: relative; top: 5px; font-family: Calibri; left: 0px; width: 976px; margin: 0 auto; text-align: left;">
    
        <div id="divTitle" style="position:relative; left:5px">
            <asp:Label ID="lblPageTitle" runat="server" Text="Reward Processing 2016 Q1" 
                style="font-weight: 700; font-size: large">
            </asp:Label>
        </div>
        <br />
        
        <table width="976px">
            <tr>
                <td align="left" style="width:5%" valign="middle">
                    <dxe:ASPxTextBox ID="txtDate" runat="server" Width="100px" MaxLength="30">
                    </dxe:ASPxTextBox>
                </td>
                <td align="left" style="width:5%" valign="middle">
                    <dxe:ASPxButton ID="btnRefresh" runat="server" Text="Refresh" Width="100px" Visible="True">
                    </dxe:ASPxButton>
                </td>
                <td align="left" style="width:5%" valign="middle">
                    <dxe:ASPxButton ID="btnProcess" runat="server" Text="Process" Width="100px" Visible="True">
                    </dxe:ASPxButton>
                </td>
                <td align="left" style="width:5%" valign="middle">
                    <dxe:ASPxButton ID="btnExportToExcel" runat="server" Text="Excel" Width="100px" Visible="True">
                    </dxe:ASPxButton>
                </td>
                <td align="left" valign="middle" style="width:50%">
                    <dxe:ASPxLabel ID="lblError" runat="server" Text="" Visible="true" Font-Size="Small">
                    </dxe:ASPxLabel>
                </td>
                <td align="right" style="width:5%" valign="middle">
                    <dxe:ASPxButton ID="btnOK" runat="server" Text="Confirm" Width="100px" Visible="False">
                    </dxe:ASPxButton>
                </td>
                <td align="right" style="width:5%" valign="middle">
                    <dxe:ASPxButton ID="btnCancel" runat="server" Text="Cancel" Width="100px" Visible="False">
                    </dxe:ASPxButton>
                </td>
            </tr>
        </table>

        <dxwgv:ASPxGridView ID="gridOrders" runat="server" Width="976px" AutoGenerateColumns="False" KeyFieldName="DealerCode">
            
            <Styles>
                <Header ImageSpacing="5px" SortingImageSpacing="5px" Wrap="True" HorizontalAlign="Center"  >
                </Header>
                <Cell HorizontalAlign="Center">
                </Cell>
                <LoadingPanel ImageSpacing="10px">
                </LoadingPanel>
            </Styles>
            
            <Settings ShowHeaderFilterButton="False" UseFixedTableLayout="True" 
                VerticalScrollableHeight="300" ShowFilterRow="False" ShowFooter="true"
                ShowVerticalScrollBar="True" />

            <SettingsBehavior AllowSort="true" />
            
            <SettingsPager PageSize="0" Mode="ShowAllRecords" Visible="False">
                <AllButton Visible="True">
                </AllButton>
                <FirstPageButton Visible="True">
                </FirstPageButton>
                <LastPageButton Visible="True">
                </LastPageButton>
            </SettingsPager>

            <Columns>
            
                <dxwgv:GridViewDataTextColumn 
                    FieldName="Contact" 
                    Caption="Contact" 
                    width="10%"
                    ExportWidth="200"
                    VisibleIndex="0"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="DealerCode" 
                    Caption="DealerCode" 
                    width="8%"
                    ExportWidth="120"
                    VisibleIndex="1">  
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="CentreName" 
                    Caption="CentreName" 
                    width="12%"
                    ExportWidth="250"
                    VisibleIndex="2"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address1" 
                    Caption="Address1" 
                    ExportWidth="300"
                    VisibleIndex="3" Visible="false"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address2" 
                    Caption="Address2" 
                    ExportWidth="300"
                    VisibleIndex="4" Visible="false"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address3" 
                    Caption="Address3" 
                    ExportWidth="300"
                    VisibleIndex="5" Visible="false"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Address4" 
                    ExportWidth="300"
                    Caption="Address4" 
                    VisibleIndex="6" Visible="false"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="PostCode" 
                    Caption="PostCode" 
                    ExportWidth="120"
                    VisibleIndex="7" Visible="false"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="CustomerName" 
                    Caption="CustomerName" 
                    width="16%"
                    ExportWidth="300"
                    VisibleIndex="8"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="InvoiceNumber" 
                    Caption="InvoiceNumber" 
                    width="10%"
                    ExportWidth="120"
                    VisibleIndex="9"> 
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="SuperMorri" 
                    Caption="Morrisons"
                    ExportWidth="120"
                    VisibleIndex="10"> 
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="SuperMarks" 
                    Caption="M&S" 
                    ExportWidth="120"
                    VisibleIndex="11"> 
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="SuperTesco" 
                    Caption="Tesco" 
                    ExportWidth="120"
                    VisibleIndex="12"> 
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="SuperAsda" 
                    Caption="Asda" 
                    ExportWidth="120"
                    VisibleIndex="13"> 
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="SuperLoveToShop" 
                    Caption="Love To Shop" 
                    ExportWidth="120"
                    VisibleIndex="14"> 
                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Jacket1" 
                    Caption="Small" 
                    ExportWidth="120"
                    VisibleIndex="15"> 
                    <PropertiesTextEdit DisplayFormatString="#,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Jacket2" 
                    ExportWidth="120"
                    Caption="Medium" 
                    VisibleIndex="16"> 
                    <PropertiesTextEdit DisplayFormatString="#,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Jacket3" 
                    ExportWidth="120"
                    Caption="Large" 
                    VisibleIndex="17"> 
                    <PropertiesTextEdit DisplayFormatString="#,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Jacket4" 
                    ExportWidth="120"
                    Caption="XL" 
                    VisibleIndex="18"> 
                    <PropertiesTextEdit DisplayFormatString="#,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn 
                    FieldName="Jacket5" 
                    ExportWidth="120"
                    Caption="XXL" 
                    VisibleIndex="19"> 
                    <PropertiesTextEdit DisplayFormatString="#,##0" />
                    <CellStyle HorizontalAlign="Left">
                    </CellStyle>
                </dxwgv:GridViewDataTextColumn>

            </Columns>
            
            <TotalSummary>
                <dxwgv:ASPxSummaryItem FieldName="InvoiceNumber" DisplayFormat="Total Invoices: {0}" SummaryType="Count" ShowInColumn="Contact" />
            </TotalSummary>

            <StylesEditors>
                <ProgressBar Height="25px">
                </ProgressBar>
            </StylesEditors>
            
        </dxwgv:ASPxGridView>
        <br />
        <br />
        
    </div>

    <dxwgv:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridOrders">
    </dxwgv:ASPxGridViewExporter>

</asp:Content>







