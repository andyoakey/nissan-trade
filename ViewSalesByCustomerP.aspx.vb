﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Partial Class ViewSalesByCustomerP

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Key PFC / Customer Pivot Table"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Call GetMonths(ddlFrom)
            Call GetMonths(ddlTo)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales by Customer (Pivot)", "Viewing Sales by Customer Pivot Table")
        End If

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

    End Sub

    Protected Sub dsPivotData_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsPivotData.Init
        If Session("TradeClubType") Is Nothing Then
            Session("TradeClubType") = 0
        End If
        If Session("PivotFrom") Is Nothing Then
            Session("PivotFrom") = GetDataLong("SELECT MAX(Id) FROM DataPeriods WHERE PeriodStatus = 'C'")
        End If
        If Session("PivotTo") Is Nothing Then
            Session("PivotTo") = Session("PivotFrom")
        End If
        dsPivotData.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("PivotFrom") = ddlFrom.SelectedValue
        gridKeyPFC.DataBind()
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("PivotTo") = ddlTo.SelectedValue
        gridKeyPFC.DataBind()
    End Sub

    Protected Sub gridKeyPFC_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridKeyPFC.DataBound
        If Not Page.IsPostBack Then
            gridKeyPFC.CollapseAllColumns()
        End If
    End Sub

    Protected Sub ASPXPivotResults_CustomCellDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxPivotGrid.PivotCellDisplayTextEventArgs) Handles gridKeyPFC.CustomCellDisplayText
        Select Case e.DataField.FieldName
            Case "SaleValue"
                e.DisplayText = Format(e.Value, "£#,##0")
            Case "Quantity"
                e.DisplayText = Format(e.Value, "#,##0")
            Case Else
        End Select
    End Sub



    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Session("TradeClubType") = ddlType.SelectedIndex
    End Sub

    Protected Sub btnExcel_Click()
       ASPxPivotGridExporter1.ASPxPivotGridID = "gridKeyPFC"
        ASPxPivotGridExporter1.ExportXlsToResponse("Results", True)
    End Sub

End Class
