﻿
Partial Class LatestData
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        'Dim master_lblPageTitle As DevExpress.Web.ASPxLabel
        'master_lblPageTitle = CType(Master.FindControl("lblPageTitle"), DevExpress.Web.ASPxLabel)
        'master_lblPageTitle.Text = "Nissan Cup Sales"
        'Me.Page.Title = master_lblPageTitle.Text

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call ExportToExcel()
        End If

        Dim master_btnExcel As DevExpress.Web.ASPxButton
        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = True

    End Sub

    Protected Sub ExportToExcel()
        Dim sFileName As String = "LatestData"
        Dim linkResults1 As New DevExpress.Web.Export.GridViewLink(ASPxGridViewExporter1)
        Dim composite As New DevExpress.XtraPrintingLinks.CompositeLink(New DevExpress.XtraPrinting.PrintingSystem())
        composite.Links.AddRange(New Object() {linkResults1})
        composite.CreateDocument()
        Dim stream As New System.IO.MemoryStream()
        composite.PrintingSystem.ExportToXlsx(stream)
        WriteToResponse(Page, sFileName, True, "xlsx", stream)
    End Sub

    Protected Sub gridExport_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub grid_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles grid.HtmlDataCellPrepared

        If e.DataColumn.Name = "daysoverdue" Then
            If Not IsDBNull(e.CellValue) Then
                If e.CellValue > 0 Then
                    e.Cell.Font.Bold = True
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_Red
                End If

                If e.CellValue = 0 Then
                    e.Cell.Font.Bold = False
                    e.Cell.ForeColor = GlobalVars.g_Color_White
                    e.Cell.BackColor = GlobalVars.g_Color_White
                End If

            End If
        End If

    End Sub



End Class
