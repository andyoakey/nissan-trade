﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NewCustomers.aspx.vb" Inherits="NewCustomers" %>

<%@ Register Assembly="DevExpress.XtraCharts.v14.2.Web, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraCharts" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content2" runat="Server" ContentPlaceHolderID="HeaderCSSJS">
    <script type="text/javascript" language="javascript">
        function ViewInvoice(contentUrl, invoiceNumber) {
            popInvoice.SetContentUrl(contentUrl);
            popInvoice.SetHeaderText(' ');
            popInvoice.SetSize(800, 600);
            popInvoice.Show();
        }
    </script>
    <style>
        .dxpcLite_Kia .dxpc-content, .dxdpLite_Kia .dxpc-content {
            white-space: normal;
            padding: 0 !important;
        }
    </style>
</asp:Content>


    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <dx:ASPxPopupControl ID="popInvoice" runat="server" ShowOnPageLoad="False" ClientInstanceName="popInvoice" Modal="True" CloseAction="CloseButton" AllowDragging="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" CloseOnEscape="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="popInvoiceContent" runat="server" CssClass="invoice"></dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


    <div style="position: relative; font-family: Calibri; text-align: left;" >
        
        <div id="divTitle" style="position: relative;">
            <table width="100%">
                <tr>

                    <td style="width:13%" align="left" valign="middle" > 
                        <dx:ASPxLabel 
                Font-Size="Larger"
                ID="lblPageTitle" runat="server" 
                Text="New Customers" />     
                    </td>
          
                    <td style="width:87%" align="left" valign="middle" > 
                        <dx:ASPxComboBox
                            ID="ddlYear" 
                            runat="server" 
                            SelectedIndex="0"
                            AutoPostBack="True"   
                            Width="120px"/>
                    </td>
                </tr>
            </table>
        </div>        

        <br />

        <dx:ASPxPageControl 
            ID="ASPxPageControl1" 
            runat="server" 
            ActiveTabIndex="0" 
            Width="100%" 
            CssClass="page_tabs">

            <TabPages>
            
                <%------------------------------------------------------------------------------------------
                ' TAB 1
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Chart">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl2" runat="server">
                        
                            <table width="100%">
                                <tr>
                                    <td style="width:50%">
                                        <dxchartsui:WebChartControl 
                                            CssClass="graph_respond" 
                                            ID="WebChartControl1" 
                                            runat="server" 
                                            AppearanceName="Origin" 
                                            ClientInstanceName="chart" 
                                            Height="450px" 
                                            PaletteName="Origin" 
                                            Width="624px">
                                        </dxchartsui:WebChartControl>
                                        <br />
                                    </td>
                                    <td>
                                        <dxchartsui:WebChartControl 
                                            CssClass="graph_respond" 
                                            ID="WebChartControl2" 
                                            runat="server" 
                                            AppearanceName="Origin" 
                                            ClientInstanceName="chart" 
                                            Height="450px" 
                                            PaletteName="Origin" 
                                            Width="624px" 
                                            PaletteBaseColorNumber="2">
                                        </dxchartsui:WebChartControl>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 2
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Monthly Detail">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl3" runat="server">
                      
                            <table id="trSelectionStuff" width="100%">
                                <tr id="Tr3" runat="server" style="height:30px;">
                                    <td align="left" >
                                        <dx:ASPxComboBox
                                            ID="ddlMonth" 
                                            runat="server" 
                                            AutoPostBack="True" 
                                            Width="180px"   >
                                            <Items>
                                                <dx:ListEditItem  Text="January" Value="Jan"/>
                                                <dx:ListEditItem  Text="February" Value="Feb" />
                                                <dx:ListEditItem  Text="March" Value="Mar" />
                                                <dx:ListEditItem  Text="April" Value="Apr" />
                                                <dx:ListEditItem  Text="May" Value="May" />
                                                <dx:ListEditItem  Text="June" Value="Jun" />
                                                <dx:ListEditItem  Text="July" Value="Jul"/>
                                                <dx:ListEditItem  Text="August" Value="Aug" />
                                                <dx:ListEditItem  Text="September" Value="Sep" />
                                                <dx:ListEditItem  Text="October" Value="Oct" />
                                                <dx:ListEditItem  Text="November" Value="Nov" />
                                                <dx:ListEditItem  Text="December" Value="Dec" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>

                                    <td>
                                    </td>

                                </tr>
                            </table>
                            <br />
                            
                            <dx:ASPxGridView 
                                ID="gridCustList" 
                                runat="server" 
                                onload="gridStyles"
                                CssClass="grid_styles"
                                Styles-Header-Wrap="True"
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Cell-Wrap="False"
                                Styles-Cell-HorizontalAlign="Center"
                                Styles-Footer-HorizontalAlign="Center"
                                AutoGenerateColumns="False" 
                                DataSourceID="dsCustList" 
                                KeyFieldName="CustomerId"                                
                                Width="100%">
                                
                                <SettingsPager PageSize="14" Mode="ShowPager" Visible="True"/>
                                
                                <Settings 
                                    ShowGroupButtons="False" 
                                    ShowStatusBar="Hidden" 
                                    ShowTitlePanel="True" 
                                    ShowFooter="true"  
                                    UseFixedTableLayout="True"  
                                    ShowVerticalScrollBar="False"/>

                                          
                                <Columns>
                                
                                    <dx:GridViewDataTextColumn 
                                        VisibleIndex="0" 
                                        FieldName="BusinessName" 
                                        CellStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Left"
                                        Width="20%" 
                                        Caption="Business Name">
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        VisibleIndex="1" 
                                        FieldName="fulladdress" 
                                        CellStyle-HorizontalAlign="Left"
                                        HeaderStyle-HorizontalAlign="Left"
                                        Width="50%" 
                                        Caption="Address">
                                    </dx:GridViewDataTextColumn>
                             
                                    <dx:GridViewDataTextColumn 
                                        Caption="Postcode" 
                                        FieldName="PostCode" 
                                        VisibleIndex="2" 
                                        Width="10%">
                                    </dx:GridViewDataTextColumn>
                                    
                                    <dx:GridViewDataTextColumn 
                                        Caption="Telephone" 
                                        FieldName="TelephoneNumber" 
                                        VisibleIndex="3" 
                                        Visible="false" 
                                        Width="10%">
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="First Spend" 
                                        FieldName="FirstSpend" 
                                        Width="10%"
                                        VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>

                                    <dx:GridViewDataTextColumn 
                                        Caption="Initial Spend" 
                                        FieldName="InitialSpend" 
                                        Width="10%"
                                        VisibleIndex="5">
                                        <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>
                                    
                                </Columns>
                                
                                <TotalSummary>
                                    <dx:ASPxSummaryItem FieldName="CustomerId" DisplayFormat="Total Customers: {0}" SummaryType="Count" ShowInColumn="BusinessName" />
                                    <dx:ASPxSummaryItem FieldName="InitialSpend" DisplayFormat="&#163;##,##0" SummaryType="Sum" ShowInColumn="InitialSpend" />
                                </TotalSummary>

                                <Templates>
                                
                                    <DetailRow>
                                          <br />
                                          <dx:ASPxGridView 
                                              ID="gridCustListDrillDown" 
                                              runat="server" 
                                             Styles-Footer-HorizontalAlign ="Center"
                                              Styles-Header-Wrap="True"
                                              Styles-Header-HorizontalAlign="Center"
                                              Styles-Cell-Wrap="False"
                                              Styles-Cell-HorizontalAlign="Center"
      
                                              AutoGenerateColumns="False"
                                              DataSourceID="dsCustListDrillDown" 
                                              onload="gridStyles"
                                              CssClass="grid_styles"
                                              OnCustomColumnDisplayText="gridCustListDrillDown_OnCustomColumnDisplayText" 
                                              OnBeforePerformDataSelect="gridCustListDrillDown_BeforePerformDataSelect"  
                                              Width="100%">
                                            
                                            <SettingsPager PageSize="16" Mode="ShowAllRecords" Visible="True"/>
                                            
                                            <Settings 
                                                ShowFooter="False" 
                                                ShowGroupedColumns="False" 
                                                ShowGroupFooter="Hidden"
                                                ShowGroupPanel="False" 
                                                ShowHeaderFilterButton="False"  
                                                ShowFilterRow="False"
                                                ShowStatusBar="Hidden" 
                                                ShowTitlePanel="False" 
                                                UseFixedTableLayout="True"/>
                                            
                                            <SettingsBehavior AllowSort="True" ColumnResizeMode="Control"/>

                                            <Columns>
                                            
                                                <dx:GridViewDataTextColumn 
                                                    Caption="Dealer" 
                                                    FieldName="DealerCode" 
                                                    ToolTip=""
                                                    VisibleIndex="0" 
                                                    Width="5%">
                                                 </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn 
                                                    Caption="Name" 
                                                    FieldName="CentreName" 
                                                    ToolTip=""
                                                    VisibleIndex="1" 
                                                    CellStyle-Wrap="False"
                                                    CellStyle-HorizontalAlign="Left"
                                                    HeaderStyle-Wrap="False"
                                                    HeaderStyle-HorizontalAlign="Left"
                                                     Width="15%">
                                                 </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn 
                                                    Caption="Date" 
                                                    FieldName="InvoiceDate" 
                                                    ToolTip=""
                                                    VisibleIndex="2" 
                                                    Width="10%">
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn Caption="Invoice" FieldName="Id" VisibleIndex="2" ExportWidth="150">
                                                    <DataItemTemplate>
                                                        <dx:ASPxHyperLink ID="hyperLink" runat="server" OnInit="ViewInvoice_Init"></dx:ASPxHyperLink>
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>

                                                <dx:GridViewDataTextColumn 
                                                    Caption="Units" 
                                                    FieldName="Quantity"
                                                    ToolTip="The number of units sold within this PFC" 
                                                    VisibleIndex="4">
                                                     </dx:GridViewDataTextColumn>
                                                
                                                <dx:GridViewDataTextColumn 
                                                    Caption="Sales Value" 
                                                    FieldName="SaleValue" 
                                                    ToolTip="The total value of all parts sold in this PFC/Month/Year"
                                                    VisibleIndex="5">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                    </PropertiesTextEdit>
                                                </dx:GridViewDataTextColumn>
                                                
                                                <dx:GridViewDataTextColumn 
                                                    Caption="Cost Value" 
                                                     FieldName="CostValue" 
                                                    ToolTip="The total cost of all parts purchased in this PFC/Month/Year"
                                                    VisibleIndex="6">
                                                    <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                    </PropertiesTextEdit>
                                                  </dx:GridViewDataTextColumn>
                                                
                                                <dx:GridViewDataTextColumn 
                                                    Caption="Margin (%)"
                                                    FieldName="Margin" 
                                                    ToolTip="The total percentage margin made on sales of all parts in this PFC/Month/Year"
                                                    VisibleIndex="7">
                                                    <PropertiesTextEdit DisplayFormatString="0.0%">
                                                    </PropertiesTextEdit>
                                                 </dx:GridViewDataTextColumn>

                                            </Columns>
                                            
                                            <SettingsDetail ExportMode="Expanded" IsDetailGrid="True" />
                                            
                                        </dx:ASPxGridView>
                          
                                        <br />

                                    </DetailRow>
                                    
                                </Templates>

                                <SettingsDetail ShowDetailRow="True" ExportMode="Expanded" IsDetailGrid="False" />

                            </dx:ASPxGridView>
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>

                <%------------------------------------------------------------------------------------------
                ' TAB 3
                ------------------------------------------------------------------------------------------%>
                <dx:TabPage Text="Dealer Summary">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server">

                            
                            <dx:ASPxGridView 
                                ID="gridNewCustomers" 
                                runat="server" 
                                onload="gridStyles"
                                CssClass="grid_styles"
                                AutoGenerateColumns="False"                              
                                Styles-Header-Wrap="True"
                                Styles-Header-HorizontalAlign="Center"
                                Styles-Cell-Wrap="False"
                                Styles-Cell-HorizontalAlign="Center"
                                Width="100%">
                                
                                <SettingsPager PageSize="16" Mode="ShowPager" Visible="True" AlwaysShowPager="true"/>
                                
                                <Settings 
                                    ShowGroupButtons="False" 
                                    ShowStatusBar="Hidden" 
                                    ShowTitlePanel="True" 
                                    UseFixedTableLayout="True" /> 
                                          
                                <Columns>
                                
                                    <dx:GridViewDataTextColumn 
                                        VisibleIndex="0" 
                                        FieldName="DealerCode" 
         
                                        Width="4%" 
                                        Caption="Dealer" 
                                        ExportWidth="150">
                                    </dx:GridViewDataTextColumn>
                            
                                    <dx:GridViewDataTextColumn 
                                        VisibleIndex="1" 
                                        FieldName="CentreName" 
         
                                        Width="12%" 
                                        Caption="Name" 
                                        HeaderStyle-HorizontalAlign="Left"
                                        CellStyle-HorizontalAlign="Left"
                                        ExportWidth="200">
                                    </dx:GridViewDataTextColumn>
                            
                                    <dx:GridViewBandColumn Caption="January" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="2" 
                                                FieldName="Month_1a" 
                 
                                                Caption="New Cust." 
                                                Width="3%" 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="3" 
                                                FieldName="Month_1b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="February" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="4" 
                                                FieldName="Month_2a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="5" 
                                                FieldName="Month_2b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="March" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="5" 
                                                FieldName="Month_3a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="6" 
                                                FieldName="Month_3b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>
       
                                    <dx:GridViewBandColumn Caption="April" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="7" 
                                                FieldName="Month_4a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="8" 
                                                FieldName="Month_4b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="May" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="9" 
                                                FieldName="Month_5a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="10" 
                                                FieldName="Month_5b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="June" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="11" 
                                                FieldName="Month_6a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="12" 
                                                FieldName="Month_6b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="July" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="13" 
                                                FieldName="Month_7a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="14" 
                                                FieldName="Month_7b" 
                                                Width="4%" 
                 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="August" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="15" 
                                                FieldName="Month_8a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="16" 
                                                FieldName="Month_8b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="September" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="17" 
                                                FieldName="Month_9a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="18" 
                                                FieldName="Month_9b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="October" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="19" 
                                                FieldName="Month_10a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="20" 
                                                FieldName="Month_10b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="November" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="21" 
                                                FieldName="Month_11a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="22" 
                                                FieldName="Month_11b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>

                                    <dx:GridViewBandColumn Caption="December" HeaderStyle-HorizontalAlign="Center">
                                        <Columns>
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="23" 
                                                FieldName="Month_12a" 
                 
                                                Width="3%" 
                                                Caption="New Cust." 
                                                Tooltip="Total No. of New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                            </dx:GridViewDataTextColumn>
                                            
                                            <dx:GridViewDataTextColumn 
                                                VisibleIndex="24" 
                                                FieldName="Month_12b" 
                 
                                                Width="4%" 
                                                Caption="Total Sales" 
                                                Tooltip="Total Value of Sales to New Customers in this month for this Dealer"
                                                ExportWidth="200">
                                                <PropertiesTextEdit DisplayFormatString="&#163;##,##0">
                                                </PropertiesTextEdit>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:GridViewBandColumn>


                                </Columns>
                                
                            </dx:ASPxGridView>
                            
                            <br />
                            <br />
                            
                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
                
            </TabPages>
            
        </dx:ASPxPageControl>

    </div>
    
    <asp:SqlDataSource ID="dsCustList" runat="server" SelectCommand="p_NewCustomerDataDetail" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="NCYear" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="NCDetailMonth" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="dsCustListDrillDown" runat="server" SelectCommand="p_CustomerSpends" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="sSelectionLevel" SessionField="SelectionLevel" Type="String" Size="1" />
            <asp:SessionParameter DefaultValue="" Name="sSelectionId" SessionField="SelectionId" Type="String" Size="6" />
            <asp:SessionParameter DefaultValue="" Name="nCustomerId" SessionField="CustomerId" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nYear" SessionField="NCYear" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="nMonth" SessionField="NCDetailMonth" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gridNewCustomers">
    </dx:ASPxGridViewExporter>

    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="gridCustList">
    </dx:ASPxGridViewExporter>

</asp:Content>


