﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" EnableTheming="false" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.12.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head id="Head1" runat="server">

    <meta charset="utf-8">
    <title>Nissan Trade Parts Login</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/bootstrap.min.css" />

    <link rel="stylesheet" href="assets/css/normalize.min.css" />
    <link rel="stylesheet" href="styles/font-awesome.min.css" />
    <link id="mainsplash" runat="server" rel="stylesheet" href="assets/css/nissan/splash.css" />
    <link rel="shortcut icon" href="assets/images/nissan/favicon.ico?v=2" />

    <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script>window.html5 || document.write('<script src="assets/js/html5shiv.js"><\/script>')</script>
    <![endif]-->
    <style type="text/css">
        .alert-danger {
            color: #f00000;
        }

        form.login .form-group button, form .form-group input.alt,
        button.alt {
            background-color: grey;
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        function resetPasswordClick() {
            document.getElementById("btnSendPassword").style.display = 'none';
            document.getElementById("lblResetClicked").innerHTML = 'Please wait, we are locating your user account...';
            return true;
        }
    </script>

</head>

<body id="body" runat="server">

    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
    <form id="page_form" class="login" runat="server" autocomplete="false">
        <div class="container-fluid">
            <div class="header">
                <div class="row">
                    <div class="col-md-3">
                        <img src="Images2014/new_logo_dark.png" class="img-responsive" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-4">
                    <!-- error msg boxes -->
                    <div class="alert alert-info fade in" role="alert" style="margin-bottom: 20px; padding: 10px; font-size: 14px; line-height: 18px;" runat="server" id="divPwdChangedNotice" visible="false">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        You are required to login again due to resetting your password.
                    </div>
                    <div class="alert alert-danger fade in" role="alert" style="margin-bottom: 20px; padding: 10px; font-size: 14px; line-height: 18px;" runat="server" id="divErrorMessage" visible="false">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <asp:Label class="errors" ID="lblErrorMessage" runat="server"></asp:Label>
                    </div>

                    <asp:Login ID="login" runat="server" Width="100%">
                        <LayoutTemplate>
                            <asp:Panel runat="server" DefaultButton="btnLogin">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                        <asp:TextBox runat="server" ID="UserName" placeholder="Username" CssClass="form-control required" TabIndex="1"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                        <asp:TextBox runat="server" ID="Password" placeholder="Passsword" CssClass="form-control required" TextMode="Password" TabIndex="2"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Button runat="server" ID="btnLogin" CssClass="form-control login_button" Text="Login" CommandName="Login" OnClick="btnLogin_Click" tabindex="3" />
                                </div>
                                <div class="form-group alt_buttons">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="form-control alt" data-toggle="modal" data-target="#forgottenPassword" id="btnForgotPwd">Reset Password</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="form-control alt" data-toggle="modal" data-target="#privacyPolicy" text="Privacy Policy">Privacy Policy</button>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                        </LayoutTemplate>
                    </asp:Login>

                </div>
            </div>
        </div>

        <!-- Reset Password Modal -->
        <div class="modal modal--small modal--grey fade" id="forgottenPassword" tabindex="-1" role="dialog" runat="server">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="panel_box">
                        <div class="panel_box__header">
                            <h3>Reset Your Password</h3>
                            <div class="panel_box__header__add">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                        <div class="panel_box__content">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                            <asp:TextBox runat="server" ID="txtEmailAddress" CssClass="form-control" placeholder="Email Address"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Button runat="server" ID="btnSendPassword" ClientIDMode="static" CssClass="form-control send_button" Text="Send Reset Link" UseSubmitBehaviour="False" OnClick="btnSendPassword_Click" OnClientClick="resetPasswordClick(this)" />
                                        <div runat="server" id="lblResetClicked" ClientIDMode="Static"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblErrors" CssClass="errors"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PRIVACY POLICY Modal -->
        <div class="modal modal--medium modal--grey fade" id="privacyPolicy" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="panel_box">
                        <div class="panel_box__header">
                            <h3>Privacy Policy</h3>
                            <div class="panel_box__header__add">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                        </div>
                        <div class="panel_box__content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="privacyContentHolder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                sessionStorage.clear();

                $('#privacyContentHolder').load('DocumentLibrary/PrivacyPolicy.html');
                $('#termsContentHolder').load('DocumentLibrary/TermsConditions.html');
            });
        </script>

    </form>
</body>

</html>
