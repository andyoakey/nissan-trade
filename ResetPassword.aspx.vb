﻿Imports System.Data
Imports QubeSecurity

Partial Class ResetPassword
    Inherits System.Web.UI.Page


    Private Property UserId As String
        Get
            Return ViewState("UserId").ToString()
        End Get

        Set(ByVal value As String)
            ViewState("UserId") = value.ToString()
        End Set
    End Property


    Dim errorMessage As String = ""
    ReadOnly _da As New DatabaseAccess
    Dim da As New DatabaseAccess
    Dim ds As DataSet
    Dim sErr As String = ""
    Dim htIn As New Hashtable
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'no password reset
        if Membership.EnablePasswordReset = false Then
            FormsAuthentication.RedirectToLoginPage()
        End If

        lblErrorMessage.Text = ""

        If IsSystemOpen() Then
            If TestDBConnection() = False Then
                btnSave.Text = "CLOSED FOR MAINTENANCE"
                btnSave.Enabled = false
            End If
        Else
            btnSave.Text = "SYSTEM UNAVAILABLE!"
            btnSave.Enabled = false
        End If
        

        If Not IsPostBack Then
            mvResetPassword.ActiveViewIndex = 1

            If(Request.QueryString("T") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString("T").ToString())) AndAlso (Request.QueryString("U") IsNot Nothing AndAlso Not String.IsNullOrEmpty(Request.QueryString("U").ToString())) Then
                Dim sErrorMessage As String = ""
                Me.UserId = Authentication.DecryptResetLink(HttpContext.Current, sErrorMessage)
                If String.IsNullOrEmpty(Me.UserId) Then
                    If sErrorMessage.Length > 0 Then
                        lblErrorMessage.Text = GeneralUtilities.HTMLEscape(sErrorMessage)
                    Else
                        mvResetPassword.ActiveViewIndex = 1
                    End If
                else
                    mvResetPassword.ActiveViewIndex = 0
                End If
            End If

            lblRequirements.Text = String.Format("<ul class=""passwordrequirements""><li>{0} characters in length</li><li>1 UPPER case character</li><li>1 number</li><li>{1} of the following special characters (<em>*[#!@$%^</em>)</li></ul>", Membership.Provider.MinRequiredPasswordLength, Membership.Provider.MinRequiredNonAlphanumericCharacters)
        End If

    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim sPassword1 As String = txtPassword1.Text
        Dim sPassword2 As String = txtPassword2.Text
        Dim sErrorMsg As String = ""

        If String.IsNullOrEmpty(sPassword1) OR String.IsNullOrEmpty(sPassword2) Then
            sErrorMsg = sErrorMsg & "<li>You must enter your new password in both fields</li>"
        End If

        If (sPassword1.Length > 0 And sPassword2.Length > 0) And (sPassword1 <> sPassword2) Then
            sErrorMsg = sErrorMsg & "<li>Passwords entered do not match. Please check them and try again</li>"
        End If

        'If Not chkTermsConditionsRead.Checked Then
        '    sErrorMsg = sErrorMsg &  "<li>Please confirm you have read our Terms & Conditions of Use</li>"
        'End If

        If sErrorMsg.Length > 0 Then
            lblErrorMessage.Text = "<ul class=""passwordrequirements"">" & sErrorMsg & "</ul>"
            txtPassword1.Text = ""
            txtPassword2.Text = ""
        Else
            If Authentication.CheckPasswordComplexity(Membership.Provider, sPassword2) Then
                Dim oUser As MembershipUser = Membership.GetUser(Guid.Parse(UserId))
                If oUser IsNot Nothing Then
                    Dim sPassword As String = oUser.ResetPassword("blue")
                    If oUser.ChangePassword(sPassword, sPassword2) Then
                        Authentication.PasswordResetCleanup(oUser.UserName)

                        mvResetPassword.Visible = False

                        lblSuccessMessage.Text = "Your password has been updated!" & Environment.NewLine + Environment.NewLine & "Please click <a href=""Login.aspx"">here</a> to login."
                        divSuccessMessage.Visible = True
                        divErrorMessage.Visible = FALSE
                    Else
                        lblErrorMessage.Text = "Password could not be changed at this time."
                    End If
                Else
                    lblErrorMessage.Text = "Password could not be changed at this time."
                End If
            Else
                lblErrorMessage.Text = "New password does not meet the minimum requirements above!"
            End If
            
        End If

        If lblErrorMessage.Text.Length > 0 Then
            divErrorMessage.Visible = True
        End If

    End Sub
    
    Protected Sub btnSendResetEmail_OnClick(sender As Object, e As EventArgs)
        Dim sUsername As String = txtUsername.Text

        Dim oUser As MembershipUser = Membership.GetUser(sUsername, False)

        If oUser IsNot Nothing Then
            Dim sGUID As String = Guid.NewGuid().ToString().Replace("-", "")
            Dim sResetLink As String = Authentication.GetResetLink(oUser, sGUID, "Please click here to reset your password")

            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Login", "Forgotten password email sent.")

            Call Authentication.UpdateUserForPwdGuid(sGUID, oUser.UserName)
            Call Authentication.SendResetPwdEmail(oUser, sResetLink)

        End If

        lblSuccessMessage.Text = "If we find a user account with that email address you will receive a password reset link."
        divSuccessMessage.Visible = True
    End Sub


    Private Function TestDBConnection() As Boolean
        Dim da As New DatabaseAccess
        Dim ds As DataSet
        Dim sErr As String = ""
        Try
            ds = da.ExecuteSQL(sErr, "SELECT * FROM System")
        Catch ex As Exception
            sErr = ex.Message
        End Try
        If sErr.Length <> 0 Then
            TestDBConnection = False
        Else
            TestDBConnection = True
        End If
    End Function


    Private Function IsSystemOpen() As Boolean
        Dim objFSO = Server.CreateObject("Scripting.FileSystemObject")
        Dim objTextStream = objFSO.OpenTextFile(Request.ServerVariables("APPL_PHYSICAL_PATH") & "SysStatus.txt", 1)
        Dim sSysUpOrDown As String = UCase(Trim(objTextStream.Readline))
        Dim sMessage As String = Trim(objTextStream.Readline)
        If sSysUpOrDown = "CLOSED" Then
            IsSystemOpen = False
        Else
            IsSystemOpen = True
        End If
    End Function


End Class
