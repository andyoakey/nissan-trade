﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.XtraCharts
Imports DevExpress.XtraCharts.Web
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting


Partial Class NewCustomers

    Inherits System.Web.UI.Page

    Dim ds As DataSet
    Dim ds2 As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim htin As New Hashtable
    Dim htin2 As New Hashtable
    Dim nMaxMonth As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim i As Integer

        Me.Title = "Nissan Trade Site - New Customers"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If

        If Not IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "New Customers", "Viewing New Customers")

            Session("NCYear") = GetDataDecimal("Select year(max(enddate)) FROM dataperiods where periodstatus = 'C'")
            ddlYear.Items.Add(Session("NCYear"))
            ddlYear.Items.Add(Session("NCYear") - 1)
            ddlYear.Items.Add(Session("NCYear") - 2)
            ddlYear.Items.Add(Session("NCYear") - 3)
            ddlYear.Items.Add(Session("NCYear") - 4)

            Session("NCMonth") = GetDataDecimal("SELECT MAX(CalendarMonth) FROM DataPeriods WHERE CalendarYear = " & Session("NCYear") & " AND PeriodStatus = 'C'")
            Session("NCDetailMonth") = Session("NCMonth")
            ddlMonth.SelectedIndex = Int32.Parse(Session("NCDetailMonth")) - 1
            ddlYear.SelectedIndex = 0
        End If

    End Sub

    Protected Sub dsCustList_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCustList.Init
        dsCustList.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsCustListDrillDown_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsCustListDrillDown.Init
        dsCustListDrillDown.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Sub ChartLoad1()

        Dim i As Integer
        Dim nThisValue As Integer

        While WebChartControl1.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl1.Series(0)
            WebChartControl1.Series.Remove(oldseries)
        End While

        Dim series1 As New Series
        Dim view1 As New SideBySideBarSeriesView

        nMaxMonth = Session("NCMonth")

        series1.ArgumentScaleType = ScaleType.Qualitative
        series1.Label.Visible = True
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.FixedPoint
        series1.PointOptions.ValueNumericOptions.Precision = 0

        series1.View = view1
        series1.LegendText = "No. New Customers"

        htin.Clear()
        htin.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin.Add("@sSelectionId", Session("SelectionId"))
        htin.Add("@nYear", Session("NCYear"))
        htin.Add("@nType", 1)
        ds = da.Read(sErrorMessage, "p_NewCustomerData", htin)

        With ds.Tables(0).Rows(0)
            For i = 1 To nMaxMonth
                nThisValue = Val(.Item("Month_" & Trim(Str(i)) & "a"))
                series1.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
            Next i
        End With

        WebChartControl1.Series.Add(series1)

        Dim diagram As XYDiagram = CType(WebChartControl1.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Month"
        diagram.AxisX.Title.Visible = True
        diagram.AxisX.Range.SideMarginsEnabled = True

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Number of new Customers"
        diagram.AxisY.Title.Visible = True
        diagram.AxisY.Range.SideMarginsEnabled = True
        diagram.AxisY.NumericOptions.Format = NumericFormat.FixedPoint
        diagram.AxisY.NumericOptions.Precision = 0

        WebChartControl1.Legend.Visible = False

        htin2.Clear()
        htin2.Add("@sSelectionLevel", Session("SelectionLevel"))
        htin2.Add("@sSelectionId", Session("SelectionId"))
        htin2.Add("@nYear", Session("NCYear"))
        htin2.Add("@nType", 0)
        ds2 = da.Read(sErrorMessage, "p_NewCustomerData", htin2)
        gridNewCustomers.DataSource = ds2.Tables(0)
        gridNewCustomers.DataBind()

    End Sub

    Sub ChartLoad2()

        Dim i As Integer
        Dim nThisValue As Integer
        While WebChartControl2.Series.Count > 0
            Dim oldseries As New Series
            oldseries = WebChartControl2.Series(0)
            WebChartControl2.Series.Remove(oldseries)
        End While

        Dim series1 As New Series
        Dim view1 As New LineSeriesView

        series1.ArgumentScaleType = ScaleType.Qualitative
        series1.Label.Visible = True
        series1.PointOptions.ValueNumericOptions.Format = NumericFormat.Currency
        series1.PointOptions.ValueNumericOptions.Precision = 0

        series1.View = view1
        series1.LegendText = "Value New Customers (£)"

        With ds.Tables(0).Rows(0)
            For i = 1 To nMaxMonth
                nThisValue = Val(.Item("Month_" & Trim(Str(i)) & "b"))
                series1.Points.Add(New SeriesPoint(GetShortMonthName(i), nThisValue))
            Next i
        End With

        WebChartControl2.Series.Add(series1)

        Dim diagram As XYDiagram = CType(WebChartControl2.Diagram, XYDiagram)

        diagram.AxisX.Tickmarks.Visible = False
        diagram.AxisX.Tickmarks.MinorVisible = False
        diagram.AxisX.Title.Antialiasing = False
        diagram.AxisX.Title.Text = "Month"
        diagram.AxisX.Title.Visible = True
        diagram.AxisX.Range.SideMarginsEnabled = True

        diagram.AxisY.Tickmarks.Visible = False
        diagram.AxisY.Tickmarks.MinorVisible = False
        diagram.AxisY.Title.Antialiasing = False
        diagram.AxisY.Title.Text = "Value of new Customers (£)"
        diagram.AxisY.Title.Visible = True
        diagram.AxisY.Range.SideMarginsEnabled = True
        diagram.AxisY.NumericOptions.Format = NumericFormat.Currency
        diagram.AxisY.NumericOptions.Precision = 0

        WebChartControl2.Legend.Visible = False

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Call ChartLoad1()
        Call ChartLoad2()
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Session("ReturnProgram") = "NewCustomers.aspx"

    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub btnExcel_Click()

        Dim nCol As Integer

        Call ChartLoad1()
        Call ChartLoad2()

        For i = 1 To gridNewCustomers.Columns.Count - 1
            gridNewCustomers.Columns(i).Visible = True
        Next

        nCol = 1
        For i = 1 To gridNewCustomers.Columns.Count - 1
            If gridNewCustomers.Columns(i).Visible Then
                gridNewCustomers.Columns(i).VisibleIndex = nCol
                nCol = nCol + 1
            End If
        Next

        ASPxGridViewExporter1.FileName = "New Customers"
        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        nCol = 1
        For i = 1 To gridNewCustomers.Columns.Count - 1
            If gridNewCustomers.Columns(i).Visible Then
                gridNewCustomers.Columns(i).VisibleIndex = nCol
                nCol = nCol + 1
            End If
        Next

    End Sub

    Protected Sub ddlMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMonth.SelectedIndexChanged
        Session("NCDetailMonth") = ddlMonth.SelectedIndex + 1
    End Sub

    Protected Sub gridCustList_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCustList.HtmlDataCellPrepared
        If Not IsDBNull(e.GetValue("BusinessURN")) Then
            If e.GetValue("BusinessURN") <> "" Then
                e.Cell.Font.Bold = True
            End If
        End If

        '    If e.GetValue("Campaign") = "Y" Then
        '        e.Cell.BackColor = System.Drawing.Color.Gold
        ''    End If
    End Sub

    Protected Sub gridCustListDrillDown_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub

    Protected Sub gridCustListDrillDown_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("CustomerId") = sKey
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        Session("NCYear") = ddlYear.SelectedItem.Text
        Session("NCMonth") = GetDataDecimal("SELECT MAX(CalendarMonth) FROM DataPeriods WHERE CalendarYear = " & Session("NCYear") & " AND PeriodStatus = 'C'")
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "InvoiceNumber").ToString()
        Dim dealerCode = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "DealerCode").ToString()

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub

End Class

