﻿Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting
Imports DevExpress.Web.GridViewDataTextColumn

Partial Class ViewSalesbyCustomer

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Spend Analysis by Customer"
        If Session("UserId") = Nothing Then
            Response.Redirect("Login.aspx")
        End If
        If Not IsPostBack Then
            Session("FocusType") = 0
            Call GetMonthsDevX(ddlFrom)
            Call GetMonthsDevX(ddlTo)
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Sales by Customer", "Viewing Sales by Customer")
        End If

    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        Session("ReturnProgram") = "ViewSalesbyCustomer.aspx"

    End Sub

    Protected Sub dsViewSalesLevel0_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel0.Init
        If Session("MonthFrom") Is Nothing Then
            Session("MonthFrom") = GetDataLong("SELECT MIN(Id) FROM DataPeriods WHERE PeriodStatus <> 'C'")
        End If
        If Session("MonthTo") Is Nothing Then
            Session("MonthTo") = Session("MonthFrom")
        End If
        dsViewSalesLevel0.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsViewSalesLevel1_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel1.Init
        dsViewSalesLevel1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub dsViewSalesLevel2_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles dsViewSalesLevel2.Init
        dsViewSalesLevel2.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("databaseconnectionstring").ConnectionString
    End Sub

    Protected Sub gridSalesLevel1_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("CustomerId") = sKey
    End Sub

    Protected Sub gridSalesLevel2_BeforePerformDataSelect(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sKey As String
        sKey = CType(sender, DevExpress.Web.ASPxGridView).GetMasterRowKeyValue()
        Session("CustomerDealerId") = sKey
    End Sub

    Protected Sub ddlFrom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFrom.SelectedIndexChanged
        Session("MonthFrom") = ddlFrom.Value
    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        Session("MonthTo") = ddlTo.Value
    End Sub

    Protected Sub gridDrillDown2_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "Margin" Then
            e.DisplayText = Format(IIf(IsDBNull(e.Value), 0, e.Value), "0.00") & IIf(IIf(IsDBNull(e.Value), 0, e.Value) <> 0, "%", "")
        End If
    End Sub
    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick
        Call GlobalRenderBrick(e)
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFocusType.SelectedIndexChanged
        Session("FocusType") = ddlFocusType.SelectedIndex
    End Sub

    Protected Sub btnExcel_Click()

        Dim i As Integer
        Dim nCol As Integer
        Dim sFileName As String

        gridSales.DataBind()
        gridSales.SettingsDetail.ExportMode = GridViewDetailExportMode.Expanded

        'For i = 3 To 9
        '    gridSales.Columns(i).Visible = True
        'Next
        'nCol = 0
        'For i = 0 To gridSales.Columns.Count - 1
        '    If gridSales.Columns(i).Visible Then
        '        gridSales.Columns(i).VisibleIndex = nCol
        '        nCol = nCol + 1
        '    End If
        'Next

        sFileName = "SalesByCustomer_" & Format(Now, "ddMMMyyhhmmss") & ".xls"
        ASPxGridViewExporter1.FileName = sFileName

        ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})

        For i = 3 To 9
            gridSales.Columns(i).Visible = False
        Next
        nCol = 0
        For i = 0 To gridSales.Columns.Count - 1
            If gridSales.Columns(i).Visible Then
                gridSales.Columns(i).VisibleIndex = nCol
                nCol = nCol + 1
            End If
        Next
    End Sub

    Private Sub ShowHideDetails(ByRef grid As DevExpress.Web.ASPxGridView, ByVal sBandName As String, ByVal bOn As Boolean)

        Dim band1 As DevExpress.Web.GridViewBandColumn = CType(grid.Columns(sBandName), DevExpress.Web.GridViewBandColumn)
        band1.Visible = bOn

    End Sub
    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub

    Protected Sub GridStylesFalse(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, False)
    End Sub

    Protected Sub ViewInvoice_Init(ByVal sender As Object, ByVal e As EventArgs)

        Dim link As ASPxHyperLink = CType(sender, ASPxHyperLink)
        Dim templateContainer As GridViewDataItemTemplateContainer = CType(link.NamingContainer, GridViewDataItemTemplateContainer)

        Dim nRowVisibleIndex As Integer = templateContainer.VisibleIndex
        Dim invoiceId As String = templateContainer.Grid.GetRowValues(nRowVisibleIndex, "invoicenumber").ToString()
        Dim dealerCode = Left(templateContainer.Grid.GetRowValues(nRowVisibleIndex, "keyfield").ToString(), 4)

        link.NavigateUrl = "javascript:void(0);"
        link.Text = invoiceId
        link.ClientSideEvents.Click = String.Format("function(s, e) {{ ViewInvoice('{0}','{1}'); }}", String.Format("/popups/Invoice.aspx?dc={0}&inv={1}", dealerCode, invoiceId), invoiceId)

    End Sub

End Class
