﻿Imports System.Data
Imports DevExpress.Web
Imports DevExpress.Export
Imports DevExpress.XtraPrinting

Partial Class CustomerYOYReport

    Inherits System.Web.UI.Page

    Dim dsResults As DataSet
    Dim da As New DatabaseAccess
    Dim sErrorMessage As String = ""
    Dim master_btnExcel As DevExpress.Web.ASPxButton

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Title = "Nissan Trade Site - Year on Year Customer Performance"

        If Not Page.IsPostBack Then
            Call AddToActivityLog(Session("UserID"), Session("SelectionLevel"), Session("SelectionId"), "Customer YOY Report", "Viewing Customer YOY Report")
        End If


        master_btnExcel = CType(Master.FindControl("btnExcel"), DevExpress.Web.ASPxButton)
        master_btnExcel.Visible = False


    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If Session("ExcelClicked") = True Then
            Session("ExcelClicked") = False
            Call btnExcel_Click()
        End If

        If Session("SelectionLevel") <> "C" Then
            lblCentreError.Visible = True
            gridCustomers.Visible = False
            'btnClearFilter.Visible = False
            ddlTA.Visible = False
            ddlType.Visible = False
            master_btnExcel.Visible = False
        Else
            lblCentreError.Visible = False
            gridCustomers.Visible = True
            'btnClearFilter.Visible = True
            ddlTA.Visible = True
            ddlType.Visible = True
            master_btnExcel.Visible = True
            LoadReport()
        End If

    End Sub

    Sub LoadReport()

        Session("CallingModule") = "CustomerYOYReport.aspx - LoadReport"

        Dim htIn As New Hashtable

        If Session("Focus") Is Nothing Then
            Session("Focus") = 0
        End If

        Session("YOYType") = ddlTA.SelectedItem.Value

        htIn.Add("@sDealerCode", Session("SelectionId"))
        htIn.Add("@nType", Session("YOYType"))
        htIn.Add("@nFocus", Session("Focus"))

        dsResults = da.Read(sErrorMessage, "p_CustomerYOYReport2", htIn)
        gridCustomers.DataSource = dsResults.Tables(0)
        gridCustomers.DataBind()
    End Sub

    Protected Sub ASPxGridViewExporter1_RenderBrick(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewExportRenderingEventArgs) Handles ASPxGridViewExporter1.RenderBrick

        Call GlobalRenderBrick(e)

        If e.RowType = DevExpress.Web.GridViewRowType.Header Or e.RowType = DevExpress.Web.GridViewRowType.Footer Then


        Else

            e.BrickStyle.ForeColor = System.Drawing.Color.Black
            e.BrickStyle.BackColor = System.Drawing.Color.White

            If e.Column.Name = "AccountStatus" Then

                Select Case CStr(e.Value)
                    Case "Lost"
                        e.BrickStyle.BackColor = System.Drawing.Color.Red

                    Case "New"
                        e.BrickStyle.BackColor = System.Drawing.Color.Green
                    Case Is = "Better"
                        e.BrickStyle.BackColor = System.Drawing.Color.LightGreen
                    Case "Worse"
                        e.BrickStyle.BackColor = System.Drawing.Color.Orange
                    Case Else
                        e.BrickStyle.BackColor = System.Drawing.Color.Wheat
                End Select

            End If

        End If

    End Sub

    Protected Sub gridCampaigns_HtmlDataCellPrepared(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridCustomers.HtmlDataCellPrepared
        Dim sThisValue As String = ""
        If IsDBNull(e.CellValue) = True Then
            sThisValue = ""
        Else
            sThisValue = Trim(e.CellValue)
        End If
        If e.DataColumn.FieldName = "AccountStatus" Then
            If sThisValue = "Lost" Then
                e.Cell.BackColor = System.Drawing.Color.Red
            ElseIf sThisValue = "New" Then
                e.Cell.BackColor = System.Drawing.Color.Green
            ElseIf sThisValue = "Better" Then
                e.Cell.BackColor = System.Drawing.Color.LightGreen
            ElseIf sThisValue = "Worse" Then
                e.Cell.BackColor = System.Drawing.Color.Orange
            Else
                e.Cell.BackColor = System.Drawing.Color.Wheat
            End If
        End If
    End Sub

    Protected Sub btnClearFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClearFilter.Click
        gridCustomers.FilterEnabled = False
        gridCustomers.FilterExpression = ""
    End Sub

    Protected Sub ddlTASelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTA.SelectedIndexChanged, ddlType.SelectedIndexChanged

        Session("YOYType") = ddlTA.SelectedItem.Value
        Session("Focus") = ddlType.SelectedItem.Value

        Select Case ddlTA.SelectedItem.Value
            Case 0
                gridCustomers.Columns(5).Caption = "Total Spend This Year"
                gridCustomers.Columns(6).Caption = "Total Spend Last Year"
            Case 1
                gridCustomers.Columns(5).Caption = "Accessories This Year"
                gridCustomers.Columns(6).Caption = "Accessories Last Year"
            Case 2
                gridCustomers.Columns(5).Caption = "Additional Product This Year"
                gridCustomers.Columns(6).Caption = "Additional Product Last Year"
            Case 3
                gridCustomers.Columns(5).Caption = "Damage This Year"
                gridCustomers.Columns(6).Caption = "Damage Last Year"
            Case 4
                gridCustomers.Columns(5).Caption = "Extended Maintenance This Year"
                gridCustomers.Columns(6).Caption = "Extended Maintenance Last Year"
            Case 5
                gridCustomers.Columns(5).Caption = "Mechanical Repair This Year"
                gridCustomers.Columns(6).Caption = "Mechanical Repair Last Year"
            Case 6
                gridCustomers.Columns(5).Caption = "Routine Maintenance This Year"
                gridCustomers.Columns(6).Caption = "Routine Maintenance Last Year"
        End Select
        gridCustomers.ClearSort()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("TradeClubType") = Nothing
        Session("YOYType") = Nothing
    End Sub

    Protected Sub gridCustomers_OnCustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "BusinessName" Then
            If IsDBNull(e.Value) Then
                If e.GetFieldValue("CustomerId") = 0 Then
                    e.DisplayText = "Excluded Customers"
                End If
            ElseIf Trim(e.Value) = "" Then
                If e.GetFieldValue("CustomerId") = 0 Then
                    e.DisplayText = "Excluded Customers"
                End If
            End If
        End If
    End Sub

    Protected Sub btnExcel_Click()
        Dim nCols As Integer
        Dim nRows As Integer
        Dim sFileName As String = ""

        nCols = gridCustomers.Columns.Count - 1
        nRows = gridCustomers.VisibleRowCount

        If nRows > 50000 Or nCols > 250 Then

            Dim strMessage As String
            strMessage = "This grid is too large to export."

            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If

        Else

            sFileName = "YOYCustomerPerformance"

            LoadReport()

            For i = 7 To 16
                gridCustomers.Columns(i).Visible = True
            Next
            ASPxGridViewExporter1.FileName = sFileName

            ASPxGridViewExporter1.WriteXlsxToResponse(New XlsxExportOptionsEx() With {.ExportType = ExportType.WYSIWYG})
            For i = 7 To 16
                gridCustomers.Columns(i).Visible = False
            Next
            For i = 0 To gridCustomers.Columns.Count - 1
                gridCustomers.Columns(i).VisibleIndex = i
            Next

        End If
    End Sub

    Protected Sub GridStyles(sender As Object, e As EventArgs)
        Call Grid_Styles(sender, True)
    End Sub


End Class
