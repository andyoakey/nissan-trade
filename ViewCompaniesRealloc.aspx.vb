﻿Imports System
Imports System.Data
Imports System.Net.Mail
Imports System.Collections.Generic
Imports Microsoft.VisualBasic
Imports System.Threading

Partial Class ViewCompaniesRealloc

    Inherits System.Web.UI.Page

    Dim nId As Integer
    Dim nCustomerId As Long

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        nId = Val(Request.QueryString(0))
        If Not Page.IsPostBack Then
            If nId <> 0 Then
                Call FetchDetails()
            End If
        End If

    End Sub

    Private Sub FetchDetails()

        Dim da As New DatabaseAccess, ds As DataSet, sErr As String = "", sSQL As String = ""
        nCustomerId = GetDataLong("SELECT CustomerId FROM CustomerDealer WHERE Id = " & Trim(Str(nId)))

        sSQL = "SELECT * FROM dbo.MatchCustomerList(" & Trim(Str(nId)) & ") ORDER BY Confidence DESC"
        ds = da.ExecuteSQL(sErr, sSQL)
        gridNewCustomers.DataSource = ds.Tables(0)
        gridNewCustomers.DataBind()

    End Sub

    Protected Sub gridNewCustomers_HtmlDataCellPrepared(sender As Object, e As DevExpress.Web.ASPxGridViewTableDataCellEventArgs) Handles gridNewCustomers.HtmlDataCellPrepared
        If e.GetValue("Id") = nCustomerId Then
            e.Cell.BackColor = GlobalVars.g_Color_LightBlue
        End If
    End Sub

    Protected Sub btnNewCustomer_Click(sender As Object, e As EventArgs) Handles btnNewCustomer.Click
        Session("CustAction") = 1
        SelChoice.Visible = False
        ConfChoice.Visible = True
    End Sub

    Protected Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        Dim startUpScript As String = String.Format("window.parent.HideReAlloc();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)
    End Sub

    Protected Sub btnSelectCustomer_Click(sender As Object, e As EventArgs) Handles btnSelectCustomer.Click

        Dim selectedValues As List(Of Object)
        Dim i As Integer
        Dim fieldNames As List(Of String) = New List(Of String)()

        fieldNames.Add("Id")
        selectedValues = gridNewCustomers.GetSelectedFieldValues(fieldNames.ToArray())
        If selectedValues.Count = 1 Then
            Session("CustAction") = 0
            Session("NewCustId") = selectedValues.Item(0).ToString
            SelChoice.Visible = False
            ConfChoice.Visible = True
        End If

    End Sub

    Protected Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click

        If Session("CustAction") = 1 Then
            Call RunNonQueryCommand("EXEC p_CustomerDealer_Realloc_NewCust " & Trim(Str(nId)) & "," & Session("UserId"))
        Else
            Call RunNonQueryCommand("EXEC p_CustomerDealer_Realloc " & Trim(Str(nId)) & "," & Session("NewCustId") & "," & Session("UserId"))
        End If
        Dim startUpScript As String = String.Format("window.parent.HideReAlloc();")
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "ANY_KEY", startUpScript, True)

    End Sub

End Class
