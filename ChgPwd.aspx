﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ChgPwd.aspx.vb" Inherits="ChgPwd" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        form.login .form-group button, 
        form .form-group input.alt,
        button.alt {
            background-color: grey;
            color: #fff;
        }

        .form-control {
            border-radius: 0px;
        }
        .input-group-addon:first-child {
            border-radius: 0px;
        }
        .login_button {
            background-color: #363b3f;
            color: #fff;
            border-radius: 0;
        }

        .modal--medium  .modal-dialog{
             width: 800px !important;
             margin: 30px auto;
         }

        /*#termsContentHolder, #privacyContentHolder {
            max-height: 640px !important;
            overflow-y: scroll !important;
            padding: 0 5px 10px;
        }*/

        
        .panel_box {
            font-family: 'Open Sans', sans-serif;
            margin-bottom: calc(3vh - 8px);
            box-shadow: 5px 5px 15px rgba(0, 0, 0, 0.15);
        }

        .panel_box:before, .panel_box:after {
            display: table;
            content: " ";
        }

        .panel_box:after {
            clear: both;
        }

        .panel_box > .panel_box__header {
            padding: 0.4vh 0.5vw;
            background: #363b3f;
            color: #fff;
            position: relative;
        }

        .panel_box > .panel_box__header > h3 {
            font-size: 2vh;
            margin: 0;
            font-weight: bold;
            display: inline;
        }

        .panel_box > .panel_box__header > h3 > span {
            font-size: 11px;
        }

        .panel_box > .panel_box__header > .panel_box__header__add {
            float: right;
            font-family: 'Open Sans', sans-serif;
            font-weight: 600;
            margin-left: 12px;
            margin-top: -0.4vh;
            font-size: 1.5vh;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > span {
            background: #fff;
            color: #000;
            padding: 1px 14px;
            margin-left: 6px;
            font-weight: 700;
            line-height: 1.7em;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > span.blue {
            color: #307bbb;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > span.red {
            color: #dd2222;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > span.orange {
            color: #ff8f0e;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > span.green {
            color: #4db53c;
        }

        .glyphicon.green {
            color: #4db53c;
        }

        .panel_box > .panel_box__content {
            padding: 12px;
            background: #fff;
        }

        .panel_box .panel_box__content__report {
            display: none;
            overflow: hidden;
        }

        .panel_box .panel_box__content__report.open {
            animation: pop_in;
        }

         .modal-content .panel_box {
             margin-bottom: 0;
         }

        .modal-content .panel_box__content {
            position: relative;
            min-height: 100px;
            max-height: calc(100vh - 300px);
            overflow-x: hidden;
            overflow-y: scroll;
            width: 100%;
        }

        .panel_box > .panel_box__header > .panel_box__header__add > button {
            color: #fff;
            opacity: 1;
            outline: 0;
        }

        .modal.modal--grey .panel_box__content {
            background-color: #e7e7e7;
        }
    </style>

    <div style="position: relative; font-family: Calibri; text-align: left;">
        <h2>Update your password</h2>
        <br />
        <br />
        <asp:Label runat="server" ID="lblMessage" Text="Please enter your old and new passwords below and then click the 'Save' button."></asp:Label>
        <br />
        <br />
        <div class="row">
            <div class="col-md-4">
                <asp:Panel runat="server" id="pnlChangePassword" DefaultButton="btnSave">
                    <div class="form-group" id="pnlCurrentPassword" runat="server">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                        <asp:TextBox runat="server" ID="txtCurrentPassword" placeholder="Current password" CssClass="form-control required" TextMode="Password" TabIndex="1"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                        <asp:TextBox runat="server" ID="txtPassword1" placeholder="New password" CssClass="form-control required" TextMode="Password" TabIndex="2"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                        <asp:TextBox runat="server" ID="txtPassword2" placeholder="Confirm your new password" CssClass="form-control required" TextMode="Password" TabIndex="3"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button runat="server" ID="btnSave" CssClass="form-control login_button" Text="Save" CommandName="Save" TabIndex="4"/>
                </div>
                <div class="form-group alt_buttons">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="form-control alt" data-toggle="modal" data-target="#privacyPolicy" id="btnPrivacyPolicy">Privacy Policy</button>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger fade in" role="alert" style="margin-bottom: 20px; padding: 10px; font-size: 14px; line-height: 18px;" runat="server" id="divErrorMessage" visible="false">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <asp:Label class="errors" ID="lblErrorMessage" runat="server" Style="text-align: left;"></asp:Label>
                </div>
                </asp:Panel>
                
            </div>
        </div>
        
    </div>
    
    <!-- TERMS CONDITIONS Modal -->
    <div class="modal modal--medium fade" id="termsConditions" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="panel_box">
                    <div class="panel_box__header">
                        <h3>Terms & Conditions</h3>
                        <div class="panel_box__header__add">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    <div class="panel_box__content">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="termsContentHolder"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- PRIVACY POLICY Modal -->
    <div class="modal modal--medium fade" id="privacyPolicy" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="panel_box">
                    <div class="panel_box__header">
                        <h3>Privacy Policy</h3>
                        <div class="panel_box__header__add">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    <div class="panel_box__content">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="privacyContentHolder"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#privacyContentHolder').load('DocumentLibrary/PrivacyPolicy.html');
            $('#termsContentHolder').load('DocumentLibrary/TermsConditions.html');
        });
    </script>
</asp:Content>

